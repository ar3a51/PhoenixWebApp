﻿using Microsoft.Graph;
using Microsoft.Identity.Client;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Phoenix.ApiExtension;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;


namespace Phoenix.Data.MsGraphExtention
{
    public class MsGraph
    {
        string[] scopes = { "https://graph.microsoft.com/.default" };

        private string ClientId { get; set; }
        private string TenantId{ get; set; }
        private string Authority { get; set; }

        public MsGraph(string clientId, string tenantId)
        {
            ClientId = clientId;
            TenantId = tenantId;
            Authority = "https://login.microsoftonline.com/" + tenantId;
        }
        public GraphServiceClient CreateGraphServiceClient(string token)
        {
            var graphClient = new GraphServiceClient("https://graph.microsoft.com/v1.0",
                new DelegateAuthenticationProvider(
                    async (requestMessage) =>
                    {
                        await Task.Delay(10);
                        requestMessage.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
                    }));

            return graphClient;
        }

        public async Task<Microsoft.Identity.Client.AuthenticationResult> GetAuthentication(string email, string password)
        {
            IPublicClientApplication app = PublicClientApplicationBuilder.Create(ClientId)
                                              .WithAuthority(Authority)
                                              .WithTenantId(TenantId)
                                              .Build();

            //var app = ConfidentialClientApplicationBuilder.Create(clientId)
            //    .WithClientSecret(clientSc)
            //    .WithTenantId(tenantId)                
            //    .WithAuthority(new Uri(authority))
            //    .Build();

            //var result = await app.AcquireTokenForClient(scopes).ExecuteAsync();

            var accounts = await app.GetAccountsAsync();

            Microsoft.Identity.Client.AuthenticationResult result = null;
            if (accounts.Any())
            {
                result = await app.AcquireTokenSilent(scopes, accounts.FirstOrDefault())
                                  .ExecuteAsync();

                return result;
            }
            else
            {
                try
                {
                    var securePassword = new SecureString();
                    foreach (char c in password)        // you should fetch the password keystroke
                        securePassword.AppendChar(c);  // by keystroke

                    result = await app.AcquireTokenByUsernamePassword(scopes, email, securePassword).ExecuteAsync();
                    return result;
                }
                catch (MsalUiRequiredException ex) when (ex.Message.Contains("AADSTS65001"))
                {
                    throw;
                    // Here are the kind of error messages you could have, and possible mitigations

                    // ------------------------------------------------------------------------
                    // MsalUiRequiredException: AADSTS65001: The user or administrator has not consented to use the application
                    // with ID '{appId}' named '{appName}'. Send an interactive authorization request for this user and resource.

                    // Mitigation: you need to get user consent first. This can be done either statically (through the portal), 
                    /// or dynamically (but this requires an interaction with Azure AD, which is not possible with 
                    // the username/password flow)
                    // Statically: in the portal by doing the following in the "API permissions" tab of the application registration:
                    // 1. Click "Add a permission" and add all the delegated permissions corresponding to the scopes you want (for instance
                    // User.Read and User.ReadBasic.All)
                    // 2. Click "Grant/revoke admin consent for <tenant>") and click "yes".
                    // Dynamically, if you are not using .NET Core (which does not have any Web UI) by 
                    // calling (once only) AcquireTokenInteractive.
                    // remember that Username/password is for public client applications that is desktop/mobile applications.
                    // If you are using .NET core or don't want to call AcquireTokenInteractive, you might want to:
                    // - use device code flow (See https://aka.ms/msal-net-device-code-flow)
                    // - or suggest the user to navigate to a URL to consent: https://login.microsoftonline.com/common/oauth2/v2.0/authorize?client_id={clientId}&response_type=code&scope=user.read
                    // ------------------------------------------------------------------------

                    // ------------------------------------------------------------------------
                    // ErrorCode: invalid_grant
                    // SubError: basic_action
                    // MsalUiRequiredException: AADSTS50079: The user is required to use multi-factor authentication.
                    // The tenant admin for your organization has chosen to oblige users to perform multi-factor authentication.
                    // Mitigation: none for this flow
                    // Your application cannot use the Username/Password grant.
                    // Like in the previous case, you might want to use an interactive flow (AcquireTokenInteractive()), 
                    // or Device Code Flow instead.
                    // Note this is one of the reason why using username/password is not recommended;
                    // ------------------------------------------------------------------------

                    // ------------------------------------------------------------------------
                    // ex.ErrorCode: invalid_grant
                    // subError: null
                    // Message = "AADSTS70002: Error validating credentials.
                    // AADSTS50126: Invalid username or password
                    // In the case of a managed user (user from an Azure AD tenant opposed to a
                    // federated user, which would be owned
                    // in another IdP through ADFS), the user has entered the wrong password
                    // Mitigation: ask the user to re-enter the password
                    // ------------------------------------------------------------------------

                    // ------------------------------------------------------------------------
                    // ex.ErrorCode: invalid_grant
                    // subError: null
                    // MsalServiceException: ADSTS50034: To sign into this application the account must be added to 
                    // the {domainName} directory.
                    // or The user account does not exist in the {domainName} directory. To sign into this application, 
                    // the account must be added to the directory.
                    // The user was not found in the directory
                    // Explanation: wrong username
                    // Mitigation: ask the user to re-enter the username.
                    // ------------------------------------------------------------------------
                }
                catch (MsalServiceException ex) when (ex.ErrorCode == "invalid_request")
                {
                    throw;
                    // ------------------------------------------------------------------------
                    // AADSTS90010: The grant type is not supported over the /common or /consumers endpoints. 
                    // Please use the /organizations or tenant-specific endpoint.
                    // you used common.
                    // Mitigation: as explained in the message from Azure AD, the authority you use in the application needs 
                    // to be tenanted or otherwise "organizations". change the
                    // "Tenant": property in the appsettings.json to be a GUID (tenant Id), or domain name (contoso.com) 
                    // if such a domain is registered with your tenant
                    // or "organizations", if you want this application to sign-in users in any Work and School accounts.
                    // ------------------------------------------------------------------------

                }
                catch (MsalServiceException ex) when (ex.ErrorCode == "unauthorized_client")
                {
                    throw;
                    // ------------------------------------------------------------------------
                    // AADSTS700016: Application with identifier '{clientId}' was not found in the directory '{domain}'.
                    // This can happen if the application has not been installed by the administrator of the tenant or consented 
                    // to by any user in the tenant.
                    // You may have sent your authentication request to the wrong tenant
                    // Cause: The clientId in the appsettings.json might be wrong
                    // Mitigation: check the clientId and the app registration
                    // ------------------------------------------------------------------------
                }
                catch (MsalServiceException ex) when (ex.ErrorCode == "invalid_client")
                {
                    throw;
                    // ------------------------------------------------------------------------
                    // AADSTS70002: The request body must contain the following parameter: 'client_secret or client_assertion'.
                    // Explanation: this can happen if your application was not registered as a public client application in Azure AD
                    // Mitigation: in the Azure portal, edit the manifest for your application and set the `allowPublicClient` to `true`
                    // ------------------------------------------------------------------------
                }
                catch (MsalServiceException)
                {
                    throw;
                }

                catch (MsalClientException ex) when (ex.ErrorCode == "unknown_user_type")
                {
                    // Message = "Unsupported User Type 'Unknown'. Please see https://aka.ms/msal-net-up"
                    // The user is not recognized as a managed user, or a federated user. Azure AD was not
                    // able to identify the IdP that needs to process the user
                    throw new ArgumentException("U/P: Wrong username", ex);
                }
                catch (MsalClientException ex) when (ex.ErrorCode == "user_realm_discovery_failed")
                {
                    // The user is not recognized as a managed user, or a federated user. Azure AD was not
                    // able to identify the IdP that needs to process the user. That's for instance the case
                    // if you use a phone number
                    throw new ArgumentException("U/P: Wrong username", ex);
                }
                catch (MsalClientException ex) when (ex.ErrorCode == "unknown_user")
                {
                    // the username was probably empty
                    // ex.Message = "Could not identify the user logged into the OS. See http://aka.ms/msal-net-iwa for details."
                    throw new ArgumentException("U/P: Wrong username", ex);
                }
                catch (MsalClientException ex) when (ex.ErrorCode == "parsing_wstrust_response_failed")
                {
                    throw;
                    // ------------------------------------------------------------------------
                    // In the case of a Federated user (that is owned by a federated IdP, as opposed to a managed user owned in an Azure AD tenant)
                    // ID3242: The security token could not be authenticated or authorized.
                    // The user does not exist or has entered the wrong password
                    // ------------------------------------------------------------------------
                }
            }
        }
    }

    public static class MsGraphExtension
    {
        public static async Task<Stream> DownloadFile(this GraphServiceClient graphClient, string fileName, bool uploadToSharePoint)
        {
            if (uploadToSharePoint)
                return await graphClient.Sites["root"].Drive.Root.ItemWithPath(fileName).Content.Request().GetAsync();
            else
                return await graphClient.Me.Drive.Root.ItemWithPath(fileName).Content.Request().GetAsync();
        }

        public static async Task<DriveItem> UploadSmallFile(this GraphServiceClient graphClient, Stream fileStream, string fileName, bool uploadToSharePoint)
        {
            if (uploadToSharePoint)
                return await graphClient.Sites["root"].Drive.Root.ItemWithPath(fileName).Content.Request().PutAsync<DriveItem>(fileStream);
            else
                return await graphClient.Me.Drive.Root.ItemWithPath(fileName).Content.Request().PutAsync<DriveItem>(fileStream);
        }

        public static async Task<DriveItem> CreateFolder(this GraphServiceClient graphClient, string folderName, string pathFolder,bool isRoot)
        {
            var driveItem = new DriveItem
            {
                Name = folderName,
                Folder = new Folder { }
            };
            if(isRoot)
                return await graphClient.Me.Drive.Root.Children.Request().AddAsync(driveItem);
            else
                return await graphClient.Me.Drive.Root.ItemWithPath("/" + pathFolder).Children.Request().AddAsync(driveItem);
        }

        /// <summary>
        /// Created By M.Salih
        /// Source Folder bisa berupa ID Item Folder atau Path ,contoh : "ROOT/SubFOlder
        /// </summary>
        /// <param name="graphClient">authentication msGraph</param>
        /// <param name="sourceFolder">Id Item Folder atau PAth Folder</param>
        /// <param name="usingId">true : menggunakan Id Item Folder, false : menggunakan Path</param>
        /// <returns></returns>
        public static async Task RemoveFolder(this GraphServiceClient graphClient, string sourceFolder, bool usingId)
        {
            if (usingId)
                await graphClient.Me.Drive.Items[sourceFolder].Request().DeleteAsync();
            else
                await graphClient.Me.Drive.Root.ItemWithPath("/"+sourceFolder).Request().DeleteAsync();
        }

         public static async Task<DriveItem> UploadLargeFile(this GraphServiceClient graphClient, Stream fileStream, string fileName, bool uploadToSharePoint)
        {
            //Memory<Stream> stream;
            DriveItem uploadedFile = null;
            UploadSession uploadSession = null;

            // Do we want OneDrive for Business/Consumer or do we want a SharePoint Site? 
            if (uploadToSharePoint)
            {
                uploadSession = await graphClient.Sites["root"].Drive.Root.ItemWithPath(fileName).CreateUploadSession().Request().PostAsync();
            }
            else
            {
                uploadSession = await graphClient.Me.Drive.Root.ItemWithPath(fileName).CreateUploadSession().Request().PostAsync();
            }

            if (uploadSession != null)
            {
                // Chunk size must be divisible by 320KiB, our chunk size will be slightly more than 1MB 
                int maxSizeChunk = (320 * 1024) * 4;
                ChunkedUploadProvider uploadProvider = new ChunkedUploadProvider(uploadSession, graphClient, fileStream, maxSizeChunk);
                var chunkRequests = uploadProvider.GetUploadChunkRequests();
                var exceptions = new List<Exception>();
                var readBuffer = new byte[maxSizeChunk];

                foreach (var request in chunkRequests)
                {
                    var result = await uploadProvider.GetChunkRequestResponseAsync(request, readBuffer, exceptions);

                    if (result.UploadSucceeded)
                    {
                        uploadedFile = result.ItemResponse;
                    }
                }
            }
            return uploadedFile;
        }
    }
}
