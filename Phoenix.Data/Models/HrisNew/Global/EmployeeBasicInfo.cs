﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Phoenix.Data.Models
{
    [Table("employee_basic_info", Schema = "hr")]
    public class EmployeeBasicInfo
    {
        [Key, Required]
        [MaxLength(50)]
        [Column(nameof(Id)), StringLength(50)]
        public string Id { get; set; }

        [MaxLength(50)]
        [Column("employee_code"), StringLength(50)]
        public string EmployeeCode { get; set; }

        [MaxLength(255)]
        [Column("name_employee"), StringLength(255)]
        public string NameEmployee { get; set; }

        [MaxLength(50)]
        [Column("kitas_number"), StringLength(50)]
        public string KitasNumber { get; set; }

        [Column("kitas_expiry")]
        public DateTime? KitasExpiry { get; set; }

        [MaxLength(50)]
        [Column("religion_id"), StringLength(50)]
        public string ReligionId { get; set; }

        [MaxLength(50)]
        [Column("passport_number"), StringLength(50)]
        public string PassportNumber { get; set; }

        [Column("passport_expiry")]
        public DateTime? PassportExpiry { get; set; }

        [MaxLength(50)]
        [Column("npwp_number"), StringLength(50)]
        public string NpwpNumber { get; set; }

        [MaxLength(150)]
        [Column("npwp_expiry"), StringLength(150)]
        public string NpwpExpiry { get; set; }

        [MaxLength(50)]
        [Column(nameof(Blood)), StringLength(50)]
        public string Blood { get; set; }

        [MaxLength(50)]
        [Column("identity_card"), StringLength(50)]
        public string IdentityCard { get; set; }

        [MaxLength(500)]
        [Column("kk_no"), StringLength(500)]
        public string KkNo { get; set; }

        [Column("address_identity_card"), StringLength(2147483647)]
        public string AddressIdentityCard { get; set; }

        [MaxLength(50)]
        [Column(nameof(Photo)), StringLength(50)]
        public string Photo { get; set; }

        [Column("home_address"), StringLength(2147483647)]
        public string HomeAddress { get; set; }

        [Column("join_date")]
        public DateTime? JoinDate { get; set; }

        [MaxLength(50)]
        [Column("bpjs_healthy"), StringLength(50)]
        public string BpjsHealthy { get; set; }

        [MaxLength(50)]
        [Column("payroll_methods"), StringLength(50)]
        public string PayrollMethods { get; set; }

        [MaxLength(50)]
        [Column("central_resource_id"), StringLength(50)]
        public string CentralResourceId { get; set; }

        [MaxLength(50)]
        [Column("status_employee_id"), StringLength(50)]
        public string StatusEmployeeId { get; set; }

        [MaxLength(50)]
        [Column("business_unit_id"), StringLength(50)]
        public string BusinessUnitId { get; set; }

        [MaxLength(50)]
        [Column("employement_status_id"), StringLength(50)]
        public string EmployementStatusId { get; set; }

        [MaxLength(50)]
        [Column("city_id"), StringLength(50)]
        public string CityId { get; set; }

        [MaxLength(50)]
        [Column("account_no"), StringLength(50)]
        public string AccountNo { get; set; }

        [MaxLength(50)]
        [Column("bpjs_tk"), StringLength(50)]
        public string BpjsTk { get; set; }

        [MaxLength(50)]
        [Column("married_status_id"), StringLength(50)]
        public string MarriedStatusId { get; set; }

        [MaxLength(50)]
        [Column("last_education"), StringLength(50)]
        public string LastEducation { get; set; }

        [MaxLength(50)]
        [Column(nameof(Probation)), StringLength(50)]
        public string Probation { get; set; }

        [MaxLength(50)]
        [Column(nameof(Approval)), StringLength(50)]
        public string Approval { get; set; }

        [MaxLength(50)]
        [Column("process_salary"), StringLength(50)]
        public string ProcessSalary { get; set; }

        [MaxLength(50)]
        [Column("job_grade_id"), StringLength(50)]
        public string JobGradeId { get; set; }

        [MaxLength(50)]
        [Column("type_tax"), StringLength(50)]
        public string TypeTax { get; set; }

        [Column("warning_date")]
        public DateTime? WarningDate { get; set; }

        [MaxLength(50)]
        [Column("working_status"), StringLength(50)]
        public string WorkingStatus { get; set; }

        [Column("date_termination")]
        public DateTime? DateTermination { get; set; }

        [MaxLength(500)]
        [Column("termination_reason"), StringLength(500)]
        public string TerminationReason { get; set; }

        [MaxLength(50)]
        [Column("flag_diageo"), StringLength(50)]
        public string FlagDiageo { get; set; }

        [MaxLength(500)]
        [Column("personal_files"), StringLength(500)]
        public string PersonalFiles { get; set; }

        [MaxLength(50)]
        [Column("first_name"), StringLength(50)]
        public string FirstName { get; set; }

        [MaxLength(50)]
        [Column("last_name"), StringLength(50)]
        public string LastName { get; set; }

        [MaxLength(50)]
        [Column("nick_name"), StringLength(50)]
        public string NickName { get; set; }

        [MaxLength(50)]
        [Column(nameof(Gender)), StringLength(50)]
        public string Gender { get; set; }

        [MaxLength(50)]
        [Column(nameof(Email)), StringLength(50)]
        public string Email { get; set; }

        [MaxLength(50)]
        [Column("birth_place"), StringLength(50)]
        public string BirthPlace { get; set; }

        [Column("date_birth")]
        public DateTime? DateBirth { get; set; }

        [MaxLength(50)]
        [Column("mobile_number"), StringLength(50)]
        public string MobileNumber { get; set; }

        [MaxLength(500)]
        [Column(nameof(Portfolio)), StringLength(500)]
        public string Portfolio { get; set; }

        [MaxLength(500)]
        [Column("pay_slip"), StringLength(500)]
        public string PaySlip { get; set; }

        [MaxLength(50)]
        [Column("location_id"), StringLength(50)]
        public string LocationId { get; set; }

        [MaxLength(50)]
        [Column("job_title_id"), StringLength(50)]
        public string JobTitleId { get; set; }

        [MaxLength(50)]
        [Column("bank_id"), StringLength(50)]
        public string BankId { get; set; }

        [MaxLength(50)]
        [Column("bank_branch"), StringLength(50)]
        public string BankBranch { get; set; }

        [MaxLength(150)]
        [Column("bank_address"), StringLength(150)]
        public string BankAddress { get; set; }

        [MaxLength(50)]
        [Column("account_name"), StringLength(50)]
        public string AccountName { get; set; }

        [MaxLength(50)]
        [Column("bank_city"), StringLength(50)]
        public string BankCity { get; set; }

        [MaxLength(255)]
        [Column("identity_address"), StringLength(255)]
        public string IdentityAddress { get; set; }

        [MaxLength(50)]
        [Column("post_code"), StringLength(50)]
        public string PostCode { get; set; }

        [MaxLength(50)]
        [Column("nationality_id"), StringLength(50)]
        public string NationalityId { get; set; }

        [MaxLength(50)]
        [Column("phone_number"), StringLength(50)]
        public string PhoneNumber { get; set; }

        [MaxLength(50)]
        [Column("legal_entity_id"), StringLength(50)]
        public string LegalEntityId { get; set; }

        [MaxLength(50)]
        [Column("ptkp_setting_id"), StringLength(50)]
        public string PtkpSettingId { get; set; }

        [MaxLength(50)]
        [Column("created_by"), StringLength(50)]
        public string CreatedBy { get; set; }

        [Column("created_on")]
        public DateTime? CreatedOn { get; set; }

        [MaxLength(50)]
        [Column("modified_by"), StringLength(50)]
        public string ModifiedBy { get; set; }

        [Column("modified_on")]
        public DateTime? ModifiedOn { get; set; }

        [MaxLength(50)]
        [Column("approved_by"), StringLength(50)]
        public string ApprovedBy { get; set; }

        [Column("approved_on")]
        public DateTime? ApprovedOn { get; set; }

        [Column("is_active")]
        public bool? IsActive { get; set; }

        [Column("is_locked")]
        public bool? IsLocked { get; set; }

        [Column("is_default")]
        public bool? IsDefault { get; set; }

        [Column("is_deleted")]
        public bool? IsDeleted { get; set; }

        [MaxLength(50)]
        [Column("owner_id"), StringLength(50)]
        public string OwnerId { get; set; }

        [MaxLength(50)]
        [Column("reports_to"), StringLength(50)]
        public string ReportsTo { get; set; }

        [MaxLength(500)]
        [Column("position_id"), StringLength(500)]
        public string PositionId { get; set; }

        [MaxLength(50)]
        [Column("deleted_by"), StringLength(50)]
        public string DeletedBy { get; set; }

        [Column("deleted_on")]
        public DateTime? DeletedOn { get; set; }

        [MaxLength(50)]
        [Column("business_unit_job_level_id"), StringLength(50)]
        public string BusinessUnitJobLevelId { get; set; }
    }
}