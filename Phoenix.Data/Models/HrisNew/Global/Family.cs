﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Phoenix.Data.Models
{
    [Table(nameof(Family), Schema = "hr")]
    public class Family
    {
        [Key, Required]
        [MaxLength(50)]
        [Column(nameof(Id)), StringLength(50)]
        public string Id { get; set; }

        [Required]
        [MaxLength(50)]
        [Column("employee_basic_info_id"), StringLength(50)]
        public string EmployeeBasicInfoId { get; set; }

        [Required]
        [MaxLength(50)]
        [Column("family_status_id"), StringLength(50)]
        public string FamilyStatusId { get; set; }

        [Required]
        [MaxLength(50)]
        [Column(nameof(Name)), StringLength(50)]
        public string Name { get; set; }

        [MaxLength(50)]
        [Column("phone_no"), StringLength(50)]
        public string PhoneNo { get; set; }

        [MaxLength(50)]
        [Column("other_no"), StringLength(50)]
        public string OtherNo { get; set; }

        [MaxLength(100)]
        [Column(nameof(Address)), StringLength(100)]
        public string Address { get; set; }

        [MaxLength(50)]
        [Column("place_birth"), StringLength(50)]
        public string PlaceBirth { get; set; }

        [Column(nameof(Birthday))]
        public DateTime? Birthday { get; set; }

        [MaxLength(50)]
        [Column(nameof(Gender)), StringLength(50)]
        public string Gender { get; set; }

        [MaxLength(50)]
        [Column("religion_id"), StringLength(50)]
        public string ReligionId { get; set; }

        [MaxLength(50)]
        [Column("education_level_id"), StringLength(50)]
        public string EducationLevelId { get; set; }

        [MaxLength(50)]
        [Column("institution_id"), StringLength(50)]
        public string InstitutionId { get; set; }

        [Column(nameof(Description)), StringLength(2147483647)]
        public string Description { get; set; }

        [MaxLength(50)]
        [Column("married_status_id"), StringLength(50)]
        public string MarriedStatusId { get; set; }

        [MaxLength(50)]
        [Column("created_by"), StringLength(50)]
        public string CreatedBy { get; set; }

        [Column("created_on")]
        public DateTime? CreatedOn { get; set; }

        [MaxLength(50)]
        [Column("modified_by"), StringLength(50)]
        public string ModifiedBy { get; set; }

        [Column("modified_on")]
        public DateTime? ModifiedOn { get; set; }

        [MaxLength(50)]
        [Column("approved_by"), StringLength(50)]
        public string ApprovedBy { get; set; }

        [Column("approved_on")]
        public DateTime? ApprovedOn { get; set; }

        [Column("is_active")]
        public bool? IsActive { get; set; }

        [Column("is_locked")]
        public bool? IsLocked { get; set; }

        [Column("is_default")]
        public bool? IsDefault { get; set; }

        [Column("is_deleted")]
        public bool? IsDeleted { get; set; }

        [MaxLength(50)]
        [Column("owner_id"), StringLength(50)]
        public string OwnerId { get; set; }

        [MaxLength(50)]
        [Column("deleted_by"), StringLength(50)]
        public string DeletedBy { get; set; }

        [Column("deleted_on")]
        public DateTime? DeletedOn { get; set; }

        [MaxLength(50)]
        [Column("business_unit_division_id"), StringLength(50)]
        public string BusinessUnitDivisionId { get; set; }

        [MaxLength(50)]
        [Column("business_unit_departement_id"), StringLength(50)]
        public string BusinessUnitDepartementId { get; set; }

        public FamilyStatus FamilyStatus { get; set; }
    }
}