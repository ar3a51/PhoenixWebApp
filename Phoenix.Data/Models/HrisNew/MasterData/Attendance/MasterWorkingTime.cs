using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Phoenix.Data.Models
{
    [Table("master_working_time", Schema = "hr")]
    public class MasterWorkingTime : HrisEntityBase
    {
        [Key]
        [MaxLength(100)]
        [Column(nameof(Id)), StringLength(100)]
        public string Id { get; set; }

        [MaxLength(50)]
        [Column(nameof(Name)), StringLength(50)]
        public string Name { get; set; }

        [Column("time_in")]
        public TimeSpan? TimeIn { get; set; }

        [Column("time_out")]
        public TimeSpan? TimeOut { get; set; }
    }
}