using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Phoenix.Data.Models
{
    [Table(nameof(Overtime), Schema = "hr")]
    public class Overtime : HrisEntityBase
    {
        [Key]
        [MaxLength(50)]
        [Column(nameof(Id)), StringLength(50)]
        public string Id { get; set; }

        [MaxLength(50)]
        [Column("employee_basic_info_id"), StringLength(50)]
        public string EmployeeBasicInfoId { get; set; }

        [Required]
        [Column("overtime_date")]
        public DateTime OvertimeDate { get; set; }

        [Required]
        [Column("hour_overtime")]
        public TimeSpan HourOvertime { get; set; }

        [Column("reason_overtime"), StringLength(2147483647)]
        public string ReasonOvertime { get; set; }
                
        [Column("request_date")]
        public DateTime? RequestDate { get; set; }

        [MaxLength(50)]
        [Column("request_code"), StringLength(50)]
        public string RequestCode { get; set; }

        [MaxLength(50)]
        [Column("leave_balance_id"), StringLength(50)]
        public string LeaveBalanceId { get; set; }

        [NotMapped]
        public InfoEmployee requestEmployee { get; set; }

        [NotMapped]
        public int? Status { get; set; }

        [NotMapped]
        public string StatusDesc { get; set; }

        [NotMapped]
        public string RemarkRejected { get; set; }
    }
}