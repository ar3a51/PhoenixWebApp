﻿using System;
using System.Text;

namespace Phoenix.Data.Models.ViewModel
{
    public class ListItem
    {
        public string Text { get; set; }

        public string Value { get; set; }
    }
}
