﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace Phoenix.Data.Models.ViewModel
{
    public class ChatAndCommentVm
    {
        [Required]
        public string ChatText { get; set; }
        public string FileExt { get; set; }
        [Required]
        public string CollaborateCode { get; set; }
        [Required]
        public string MemberName { get; set; }

        [Required]
        public string Type { get; set; }
    }

    public class ChatAndCommentFileVm
    {
        [Required]
        public IFormFile File { get; set; }
        public string FileExt { get; set; }
        [Required]
        public string CollaborateCode { get; set; }
        [Required]
        public string MemberName { get; set; }
    }

    public class ContactMemberChatVm
    {
        [Required]
        public string MemberCode { get; set; }
        [Required]
        public string MemberName { get; set; }
        [Required]
        public string MemberToCode { get; set; }
        [Required]
        public string MemberToName { get; set; }
    }


    public class ChatGroupInfoVm
    {
        [Required]
        public string ChatText { get; set; }
        public string FileExt { get; set; }
        [Required]
        public string CollaborateCode { get; set; }
        [Required]
        public string MemberCode { get; set; }
        [Required]
        public string MemberName { get; set; }
    }

    public class TeamInfoVm
    {
        public string Id { get; set; }
        public string JobName { get; set; }
        public string TeamName { get; set; }
    }
    public class ListTeamMember
    {
        public string MemberCode { get; set; }
    }

    public class NotificationVm
    {
        public string TemplateNotify { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int Tipe { get; set; }
    }
    public class CodeNotifyVm
    {
        public string Codex { get; set; }
    }
}
