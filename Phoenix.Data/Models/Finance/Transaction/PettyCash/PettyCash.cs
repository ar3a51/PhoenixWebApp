﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Phoenix.Data.Models.Finance
{
    [Table("petty_cash_old", Schema = "fn")]
    public class PettyCash : FinanceEntityBase
    {
        [Key]
        [MaxLength(50)]
        [Column(nameof(Id)), StringLength(50)]
        public string Id { get; set; }

        [Column("amount")]
        public decimal? Amount { get; set; }
        
        //[Required]
        [MaxLength(50)]
        [Column("settlement_number"), StringLength(50)]
        public string SettlementNumber { get; set; }

        [Column("settlement_date")]
        public DateTime? SettlementDate { get; set; }

        //[Required]
        [MaxLength(50)]
        [Column("legal_entity_id"), StringLength(50)]
        public string LegalEntityId { get; set; }

        //[Required]
        [MaxLength(50)]
        [Column("business_unit_id"), StringLength(50)]
        public string BusinessUnitId { get; set; }

        [MaxLength(50)]
        [Column("affiliation_id"), StringLength(50)]
        public string AffiliationId { get; set; }

        //[Required]
        [MaxLength(50)]
        [Column("cash_holder_id"), StringLength(50)]
        public string CashHolderId { get; set; }
        
        [Column("start_date")]
        public DateTime StartDate { get; set; }

        [Column("end_date")]
        public DateTime EndDate { get; set; }

        //[Required]
        [MaxLength(50)]
        [Column("currency_id"), StringLength(50)]
        public string CurrencyId { get; set; }

        [Column("exchange_rate")]
        public decimal? ExchangeRate { get; set; }

        [MaxLength(450)]
        [Column(nameof(Remarks)), StringLength(450)]
        public string Remarks { get; set; }

        [MaxLength(50)]
        [Column("account_id"), StringLength(50)]
        public string AccountId { get; set; }

        [MaxLength(50)]
        [Column("account_payable_id"), StringLength(50)]
        public string AccountPayableId { get; set; }

        [MaxLength(50)]
        [Column("approval_id"), StringLength(50)]
        public string ApprovalId { get; set; }

        [MaxLength(50)]
        [Column("status_petty_cash"), StringLength(50)]
        public string Status { get; set; }        

        //diisi oleh jurna entry Id
        [NotMapped]
        public string RefNumber { get; set; }

        [NotMapped]
        public string LegalEntity { get; set; }

        [NotMapped]
        public string BusinessUnit { get; set; }

        [NotMapped]
        public string Affiliation { get; set; }

        [NotMapped]
        public string RequestorName { get; set; }
        
        [NotMapped]
        public string BankAccountName { get; set; }

        [NotMapped]
        public List<PettyCashDetail> Detail { get; set; }

        //public LegalEntity LegalEntity { get; set; }
        //public BusinessUnit BusinessUnit { get; set; }
        //public ApplicationUser ApplicationUser { get; set; }
        //public Currency Currency { get; set; }
        //public Account Account { get; set; }
        //public Account Account { get; set; }
        //public Approval Approval { get; set; }
    }
}
