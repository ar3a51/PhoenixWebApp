﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Phoenix.Data.Models.Finance.Transaction
{
	[Table("petty_cash", Schema = "fn")]
	public class NewPettyCash : FinanceEntityBase
	{
		[Key]
		[MaxLength(50)]
		[Column(nameof(Id)), StringLength(50)]
		public string Id { get; set; }

		[Column("settlement_date")]
		public DateTime? SettlementDate { get; set; }

		[MaxLength(50)]
		[Column("business_unit_id"), StringLength(50)]
		public string BusinessUnitId { get; set; }

		[MaxLength(50)]
		[Column("affiliation_id"), StringLength(50)]
		public string AffiliationId { get; set; }

		//[Required]
		[MaxLength(50)]
		[Column("cash_holder_id"), StringLength(50)]
		public string CashHolderId { get; set; }

		[Column("exchange_rate")]
		public decimal? ExchangeRate { get; set; }

		[MaxLength(450)]
		[Column(nameof(Remarks)), StringLength(450)]
		public string Remarks { get; set; }

		[MaxLength(50)]
		[Column("approval_id"), StringLength(50)]
		public string ApprovalId { get; set; }

		[Column("status")]
		public int Status { get; set; }

		[Column("status_petty_cash")]
		public int? TrnStatus { get; set; }

		//diisi oleh id master petty cash
		[MaxLength(50)]
		[Column("refference_no"), StringLength(50)]
		public string PettyCashNumber { get; set; }

		[Column("ending_balance")]
		public decimal? EndingBalance { get; set; }

		/// <summary>
		/// NotMapped
		/// </summary>
		[NotMapped]
		public string BankAccountNumber { get; set; }

		/// <summary>
		/// NotMapped
		/// </summary>
		[NotMapped]
		public string LegalEntityId { get; set; }

		/// <summary>
		/// NotMapped
		/// </summary>
		[NotMapped]
		public string CurrencyId { get; set; }

		/// <summary>
		/// NotMapped
		/// </summary>
		[NotMapped]
		public string LegalEntity { get; set; }

		/// <summary>
		/// NotMapped
		/// </summary>
		[NotMapped]
		public string BusinessUnit { get; set; }

		/// <summary>
		/// NotMapped
		/// </summary>
		[NotMapped]
		public string Affiliation { get; set; }

		/// <summary>
		/// NotMapped
		/// </summary>
		[NotMapped]
		public string RequestorName { get; set; }

		/// <summary>
		/// NotMapped
		/// </summary>
		[NotMapped]
		public string BankName { get; set; }

		/// <summary>
		/// NotMapped
		/// </summary>
		[NotMapped]
		public List<PettyCashDetail> Detail { get; set; }

		/// <summary>
		/// NotMapped
		/// </summary>
		[NotMapped]
		public string StatusName { get; set; }

		/// <summary>
		/// NotMapped
		/// </summary>
		[NotMapped]
		public bool IsOVerDueSettlement { get; set; }

		/// <summary>
		/// NotMapped
		/// </summary>
		[NotMapped]
		public DateTime? MsPettyCashDate { get; set; }

		/// <summary>
		/// NotMapped
		/// </summary>
		[NotMapped]
		public string GroupName { get; set; }

		/// <summary>
		/// Not Mapped
		/// </summary>
		[NotMapped]
		public string DescJournalEndingBalance { get; set; }
	}
}
