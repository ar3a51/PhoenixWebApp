﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Phoenix.Data.Models.Finance.Transaction.PaymentRequest
{
   
    public class PaymentRequestDTO
    {
        public PaymentRequest Header { get; set; }
        public List<PaymentRequestDetail> Details { get; set; }
    }
}
