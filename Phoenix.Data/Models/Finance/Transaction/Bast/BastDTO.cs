﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Data.Models.Finance
{
    public class BastDTO
    {
        public Bast Header;
        public List<BastDetail> Details;
    }
}
