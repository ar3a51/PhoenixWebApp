﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Data.Models.Finance.Transaction.InvoiceReceivable
{
    public class ClientData
    {
        public string CompanyName { get; set; }
        public string TaxRegistrationNumber { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string ContactName { get; set; }
        public string Phone { get; set; }
    }
}
