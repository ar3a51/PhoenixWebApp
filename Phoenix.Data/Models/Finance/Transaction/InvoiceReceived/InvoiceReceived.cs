﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Phoenix.Data.Models.Finance.Transaction.InvoiceReceivable
{
    [Table("invoice_receivable", Schema = "fn")]
    public class InvoiceReceived
    {
        [Key]
        [MaxLength(50)]
        [Column(nameof(Id)), StringLength(50)]
        public string Id { get; set; }

        [Column("invoice_date")]
        public DateTime? InvoiceDate { get; set; }
        
        [MaxLength(50)]
        [Column("invoice_number"), StringLength(50)]
        public string InvoiceNumber { get; set; }

        [MaxLength(50)]
        [Column("client_id"), StringLength(50)]
        public string ClientId { get; set; }

        [MaxLength(50)]
        [Column("po_id"), StringLength(50)]
        public string PoId { get; set; }

        [MaxLength(50)]
        [Column("job_id"), StringLength(50)]
        public string JobId { get; set; }

        [MaxLength(50)]
        [Column("pce_id"), StringLength(50)]
        public string PceId { get; set; }

        [MaxLength(50)]
        [Column("invoice_vendor"), StringLength(50)]
        public string InvoiceVendor { get; set; }

        [Column("invoice_vendor_date")]
        public DateTime? InvoiceVendorDate { get; set; }

        [MaxLength(500)]
        [Column("description"), StringLength(500)]
        public string Description { get; set; }

        [Column("value")]
        public decimal Value { get; set; }
        [Column("total_cost_vendor")]
        public decimal? TotalCostVendor { get; set; }

        [MaxLength(50)]
        [Column("currency_id"), StringLength(50)]
        public string CurrencyId { get; set; }

        [Column("exchange_rate")]
        public decimal ExchangeRate { get; set; }

        [Column("amount")]
        public decimal Amount { get; set; }

        [Column("vat")]
        public decimal Vat { get; set; }

        [Column("total_amount")]
        public decimal TotalAmount { get; set; }

        [MaxLength(50)]
        [Column("tax_number"), StringLength(50)]
        public string TaxNumber { get; set; }

        [Column("tax_date")]
        public DateTime? TaxDate { get; set; }

        [Column("po_check_list")]
        public bool? PoCheckList { get; set; }
        
        [MaxLength(50)]
        [Column("po_object"), StringLength(50)]
        public string PoObject { get; set; }

        [Column("tax_check_list")]
        public bool? TaxCheckList { get; set; }
        
        [MaxLength(50)]
        [Column("tax_object"), StringLength(50)]
        public string TaxObject { get; set; }

        [Column("contract_check_list")]
        public bool? ContractCheckList { get; set; }

        [MaxLength(50)]
        [Column("contract_object"), StringLength(50)]
        public string ContractObject { get; set; }

        [Column("do_check_list")]
        public bool? DoCheckList { get; set; }

        [MaxLength(50)]
        [Column("do_object"), StringLength(50)]
        public string DoObject { get; set; }

        [Column("gr_check_list")]
        public bool? GrCheckList { get; set; }

        [MaxLength(50)]
        [Column("gr_object"), StringLength(50)]
        public string GrObject { get; set; }

        [Column("approval_front_desk")]
        public bool? ApprovalFrontDesk { get; set; }

        [MaxLength(450)]
        [Column("front_desk_notes"), StringLength(450)]
        public string FrontDeskNotes { get; set; }

        [Column("approval_admin_desk")]
        public bool? ApprovalAdminDesk { get; set; }

        [MaxLength(450)]
        [Column("admin_notes"), StringLength(450)]
        public string AdminNotes { get; set; }

        [MaxLength(50)]
        [Column("mainservice_category_id"), StringLength(50)]
        public string MainServiceCategoryId { get; set; }
      
        [Column("GoodReceivedNumber")]
        public string GoodReceivedNumber { get; set; }

        [MaxLength(50)]
        [Column("service_center_id"), StringLength(50)]
        public string ServiceCenterId { get; set; }

        [MaxLength(50)]
        [Column("division_id"), StringLength(50)]
        public string DivisionId { get; set; }

        [MaxLength(50)]
        [Column("legal_id"), StringLength(50)]
        public string LegalId { get; set; }

        [MaxLength(50)]
        [Column("affiliation_id"), StringLength(50)]
        public string AffiliationId { get; set; }

        [MaxLength(50)]
        [Column("invoice_status_id"), StringLength(50)]
        public string InvoiceStatusId { get; set; }

        [MaxLength(50)]
        [Column("created_by"), StringLength(50)]
        public string CreatedBy { get; set; }

        [Column("created_on")]
        public DateTime? CreatedOn { get; set; }

        [MaxLength(50)]
        [Column("modified_by"), StringLength(50)]
        public string ModifiedBy { get; set; }

        [Column("modified_on")]
        public DateTime? ModifiedOn { get; set; }

        [MaxLength(50)]
        [Column("approved_by"), StringLength(50)]
        public string Approvedy { get; set; }

        [Column("approved_on")]
        public DateTime? ApprovedOn { get; set; }

        [Column("is_deleted")]
        public bool? IsDeleted { get; set; }

        [Column("is_active")]
        public bool? IsActive { get; set; }
        [Column("DueDays")]
        public decimal? DueDays { get; set; }
        [Column("due_date")]
        public DateTime? DueDate { get; set; }
        [Column("Periode")]
        public DateTime? Periode { get; set; }
        [Column("inv_check_list")]
        public bool? InvCheckList { get; set; }
        [NotMapped]
        public bool? InvCheckListData { get; set; }
        [Column("inv_object")]
        public string InvObject { get; set; }
        [Column("diagio")]
        public bool? diagio { get; set; }

        [MaxLength(50)]
        [Column("media_order_number"), StringLength(50)]
        public string MediaOrderNumber { get; set; }

        [NotMapped]
        public string ClientName { get; set; }

        [NotMapped]
        public string StatusName { get; set; }

        [NotMapped]
        public string CurrencyName { get; set; }

        [NotMapped]
        public string PoNumber { get; set; }

        [NotMapped]
        public Pce PCE { get; set; }

        [NotMapped]
        public string purchase_request_number { get; set; }
        [NotMapped]
        public string JobName { get; set; }
        [NotMapped]
        public PurchaseOrder purchaseOrder { get; set; }
        [NotMapped]
        public string checklistString { get; set; }
        //tambahan untuk inv rec detail
        [Column("NetAmount")]
        public decimal? NetAmount { get; set; }
        [Column("PromptPayment")]
        public decimal? PromptPayment { get; set; }
        [Column("PromptPayment2")]
        public decimal? PromptPayment2 { get; set; }
        [Column("OtherDiscount")]
        public decimal? OtherDiscount { get; set; }
        [Column("ValueAddedTax")]
        public decimal? ValueAddedTax { get; set; }
        [Column("ValueAddedTaxPercent")]
        public decimal? ValueAddedTaxPercent { get; set; }
        [NotMapped]
        public decimal? ValueAddedTaxOn { get; set; }
        [Column("AdvertisingTax")]
        public decimal? AdvertisingTax { get; set; }
    
        [Column("TaxPayable")]
        public decimal? TaxPayable { get; set; }
        [Column("TaxPayablePercent")]
        public decimal? TaxPayablePercent { get; set; }
        [NotMapped]
        public decimal? TaxPayableOn { get; set; }
        [Column("AllocationRatio")]
        public decimal? AllocationRatio { get; set; }
        [Column("organization_id")]
        public string organization_id { get; set; }
        [NotMapped]
        public bool? PoCheckListData { get; set; }
        [NotMapped]
        public bool? ContractCheckListData { get; set; }
        [NotMapped]
        public bool? TaxCheckListData { get; set; }
        [NotMapped]
        public bool? GrCheckListData { get; set; }
        [NotMapped]
        public bool? DoCheckListData { get; set; }
        [NotMapped]
        public string DivisionName { get; set; }
        [NotMapped]
        public string LegalEntityId { get; set; }
        [NotMapped]
        public string LegalEntityName { get; set; }

    }
}
