﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Phoenix.Data.Models.Finance.Transaction.InvoiceReceivable
{
    [Table("invoice_receivable_detail", Schema = "fn")]
    public class InvoiceReceivedDetail : FinanceEntityBase
    {
        [Key]
        [MaxLength(50)]
        [Column(nameof(Id)), StringLength(50)]
        public string Id { get; set; }
        [MaxLength(50)]
        [Column("invoice_receivable_id"), StringLength(50)]
        public string InvoiceReceivableId { get; set; }
        [MaxLength(50)]
        [Column("pce_id"), StringLength(50)]
        public string pce_id { get; set; }
        [Column("category")]
        public string category { get; set; }
        [Column("company_brand_id")]
        public string company_brand_id { get; set; }
        [Column("task_detail")]
        public string TaskDetail { get; set; }
        [Column("pce_value")]
        public decimal? pce_value { get; set; }
        [Column("balance")]
        public decimal? balance { get; set; }
        [Column("actual_cost")]
        public decimal? actual_cost { get; set; }
        [Column("invoice_value")]
        public decimal? invoice_value { get; set; }
        [Column("amount_paid")]
        public decimal? AmountPaid { get; set; }
        [Column("discount")]
        public decimal? Discount { get; set; }
        [Column("tax")]
        public decimal? Tax { get; set; }

        [Column("row_index")]
        public int? RowIndex { get; set; }
        [Column("ItemName")]
        public string ItemName { get; set; }
        [Column("ItemDescription")]
        public string ItemDescription { get; set; }
        [Column("qty")]
        public decimal? qty { get; set; }
        [Column("unit_price")]
        public decimal? unit_price { get; set; }
        [Column("amount")]
        public decimal? amountTotal { get; set; }
        [Column("uom_id")]
        public string UomId { get; set; }
        [Column("Item_type_id")]
        public string ItemTypeId { get; set; }
        [Column("d_k")]
        public string d_k { get; set; }
        [Column("account_id")]
        public string account_id { get; set; }
        [Column("line_code")]
        public string LineCode { get; set; }
        [Column("organization_id")]
        public string organization_id { get; set; }
        [Column("item_id")]
        public string ItemId { get; set; }
        [Column("tax_pay_coa_id")]
        public string TaxPayableCoaId { get; set; }
        [Column("tax_payable_pct")]
        public decimal? TaxPayablePercent { get; set; }
        [NotMapped]
        public string TaxPayableCoaName { get; set; }
        [NotMapped]
        public string ItemTypeName { get; set; }
        [NotMapped]
        public string uom_name { get; set; }
        [NotMapped]
        public string ItemCode { get; set; }
        [NotMapped]
        public string AccountId { get; set; }
        [NotMapped]
        public string AccountName { get; set; }
        [NotMapped]
        public string AccountCode { get; set; }

    }
}
