﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Phoenix.Data.Models.Finance.Transaction.InvoiceReceivable
{
   
    public class InvoiceReceiveableDTO
    {
        public InvoiceReceived Header { get; set; }
        public List<InvoiceReceivedDetail> Details { get; set; }
    }
}
