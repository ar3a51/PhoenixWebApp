﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace Phoenix.Data.Models.Finance.Transaction.AccReceivable
{
    public class StatementAR
    {
        public string Id { get; set; }
        
        public string CompanyName { get; set; }
        
        public string CompanyBrand { get; set; }
        
        public string InvoiceNumber { get; set; }
        
        public DateTime? TransactionDate { get; set; }

        public string JobId { get; set; }
        public string JobName { get; set; }

        public string Division { get; set; }
        
        public string Description { get; set; }
        
        public string MainServiceCategory { get; set; }
        
        public decimal? Debet { get; set; }
        
        public decimal? Credit { get; set; }
        
        public decimal? Balance { get; set; }

    }
}
