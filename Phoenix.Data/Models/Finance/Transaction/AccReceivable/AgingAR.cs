﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Phoenix.Data.Models.Finance.Transaction.AccReceivable
{
    public class AgingAR
    {
        public string Id { get; set; }
        
        public string CompanyName { get; set; }
        
        public string CompanyBrand { get; set; }
        
        public string InvoiceNumber { get; set; }

        [DisplayName("Date")]
        public DateTime? TransactionDate { get; set; }

        [DisplayName("Due Date")]
        public DateTime? DueDate { get; set; }

        [DisplayName("Day")]
        public Int32 Day { get; set; }

        [DisplayName("PO Number")]
        public string PONumber { get; set; }

        [DisplayName("Job Id")]
        public string JobId { get; set; }

        [DisplayName("Job Name")]
        public string JobName { get; set; }

        [DisplayName("Division")]
        public string Division { get; set; }

        [DisplayName("Legal Entity")]
        public string LegalEntity { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Coa Code")]
        public string CoaCode { get; set; }

        [DisplayName("Aging")]
        public int Aging { get; set; }

        //public string I01_30 { get { return ((Aging >= 1 && Aging <= 30) ? 0 : Aging).ToString(); } }
        //public string I31_60 { get { return ((Aging >= 31 && Aging <= 60) ? 0 : Aging).ToString(); } }
        //public string I61_90 { get { return ((Aging >= 61 && Aging <= 90) ? 0 : Aging).ToString(); } }
        //public string IGt90 { get { return (Aging > 90 ? 0 : Aging).ToString(); } }

        public string LegalEntityId { get; set; }
        public string DivisionId { get; set; }
        public string CompanyId { get; set; }
    }
}
