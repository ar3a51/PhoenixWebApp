﻿using Phoenix.Data.Models.Finance.Transaction.AR;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Phoenix.Data.Models
{
    [Table("invoice_receivable_detail", Schema = "fn")]
    public class InvoiceReceivableDtl : FinanceEntityBase
    {
        [Key]
        [MaxLength(50)]
        [Column(nameof(Id)), StringLength(50)]
        public string Id { get; set; }

        [MaxLength(50)]
        [Column("invoice_receivable_id"), StringLength(50)]
        public string InvoiceReceivableId { get; set; }

        public string owner_id { get; set; }
        public string pce_id { get; set; }

        public string category { get; set; }
        public string company_brand_id { get; set; }
        public string task_detail { get; set; }
        public decimal? pce_value { get; set; }
        public decimal? balance { get; set; }
        public decimal? actual_cost { get; set; }
        public decimal? invoice_value { get; set; }
        public InvoiceReceivable InvoiceReceivable { get; set; }
        //public Organization Organization { get; set; }
        //public CompanyBrand CompanyBrand { get; set; }
    }
}