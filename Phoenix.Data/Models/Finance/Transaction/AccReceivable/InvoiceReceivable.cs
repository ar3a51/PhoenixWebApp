﻿
using Phoenix.Shared.Core.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Phoenix.Data.Models.Finance.Transaction.AR
{
    [Table("invoice_receivable", Schema = "fn")]
    public class InvoiceReceivable : FinanceEntityBase
    {
        [Key]
        [MaxLength(50)]
        [Column(nameof(Id)), StringLength(50)]
        public string Id { get; set; }

        [Column("invoice_date")]
        public DateTime? InvoiceDate { get; set; }

        [Required]
        [MaxLength(50)]
        [Column("invoice_number"), StringLength(50)]
        public string InvoiceNumber { get; set; }

        [MaxLength(50)]
        [Column("client_id"), StringLength(50)]
        public string ClientId { get; set; }

        [MaxLength(50)]
        [Column("po_id"), StringLength(50)]
        public string PoId { get; set; }

        [MaxLength(50)]
        [Column("job_id"), StringLength(50)]
        public string JobId { get; set; }

        [MaxLength(50)]
        [Column("pce_id"), StringLength(50)]
        public string PceId { get; set; }

        [MaxLength(50)]
        [Column("invoice_vendor"), StringLength(50)]
        public string InvoiceVendor { get; set; }

        [Column("invoice_vendor_date")]
        public DateTime? InvoiceVendorDate { get; set; }

        [MaxLength(500)]
        [Column(nameof(Description)), StringLength(500)]
        public string Description { get; set; }

        [Column(nameof(Value))]
        public decimal? Value { get; set; }

        [MaxLength(50)]
        [Column("currency_id"), StringLength(50)]
        public string CurrencyId { get; set; }

        [Column("exchange_rate")]
        public decimal? ExchangeRate { get; set; }

        [Column(nameof(Amount))]
        public decimal? Amount { get; set; }

        [Column(nameof(Vat))]
        public decimal? Vat { get; set; }

        [Column("total_amount")]
        public decimal? TotalAmount { get; set; }

        [MaxLength(50)]
        [Column("tax_number"), StringLength(50)]
        public string TaxNumber { get; set; }

        [Column("tax_date")]
        public DateTime? TaxDate { get; set; }

        [Column("po_check_list")]
        public bool? PoCheckList { get; set; }

        [MaxLength(50)]
        [Column("po_object"), StringLength(50)]
        public string PoObject { get; set; }

        [Column("tax_check_list")]
        public bool? TaxCheckList { get; set; }

        [MaxLength(50)]
        [Column("tax_object"), StringLength(50)]
        public string TaxObject { get; set; }

        [Column("contract_check_list")]
        public bool? ContractCheckList { get; set; }

        [MaxLength(50)]
        [Column("contract_object"), StringLength(50)]
        public string ContractObject { get; set; }

        [Column("do_check_list")]
        public bool? DoCheckList { get; set; }

        [MaxLength(50)]
        [Column("do_object"), StringLength(50)]
        public string DoObject { get; set; }

        [Column("gr_check_list")]
        public bool? GrCheckList { get; set; }

        [MaxLength(50)]
        [Column("gr_object"), StringLength(50)]
        public string GrObject { get; set; }

        [Column("approval_front_desk")]
        public bool? ApprovalFrontDesk { get; set; }

        [MaxLength(450)]
        [Column("front_desk_notes"), StringLength(450)]
        public string FrontDeskNotes { get; set; }

        [Column("approval_admin_desk")]
        public bool? ApprovalAdminDesk { get; set; }

        [MaxLength(450)]
        [Column("admin_notes"), StringLength(450)]
        public string AdminNotes { get; set; }

        [MaxLength(50)]
        [Column("main_service_id"), StringLength(50)]
        public string MainServiceId { get; set; }

        [MaxLength(50)]
        [Column("service_center_id"), StringLength(50)]
        public string ServiceCenterId { get; set; }

        [MaxLength(50)]
        [Column("division_id"), StringLength(50)]
        public string DivisionId { get; set; }

        [MaxLength(50)]
        [Column("legal_id"), StringLength(50)]
        public string LegalId { get; set; }

        [MaxLength(50)]
        [Column("affiliation_id"), StringLength(50)]
        public string AffiliationId { get; set; }

        [MaxLength(50)]
        [Column("invoice_status_id"), StringLength(50)]
        public string InvoiceStatusId { get; set; }

        public Finance.Master.Currency Currency { get; set; }
        public LegalEntity LegalEntity { get; set; }
        public Affiliation Affiliation { get; set; }
        public Organization Organization { get; set; }
    }
}
