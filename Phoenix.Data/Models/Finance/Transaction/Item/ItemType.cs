﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Phoenix.Data.Models
{
    [Table("item_type", Schema = "dbo")]
    public class ItemType : FinanceEntityBase
    {
        [Key]
        [MaxLength(50)]
        [Column(nameof(Id)), StringLength(50)]
        public string Id { get; set; }

        [Required]
        [MaxLength(450)]
        [Column("type_name"), StringLength(450)]
        public string TypeName { get; set; }

        [MaxLength(50)]
        [Column("organization_id"), StringLength(50)]
        public string OrganizationId { get; set; }
    }
}