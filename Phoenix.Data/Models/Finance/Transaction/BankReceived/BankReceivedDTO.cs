﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Data.Models.Finance.Transaction
{
    public class BankReceivedDTO
    {
        public BankReceived Header { get; set; }
        public List<BankReceivedDetail> Details { get; set; }
    }
}
