﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Phoenix.Data.Models.Finance.Transaction
{
    [Table("bank_received_type", Schema = "fn")]
    public class BankReceivedType : FinanceEntityBase
    {
        [Key]
        [MaxLength(50)]
        [Column(nameof(Id)), StringLength(50)]
        public string Id { get; set; }

        [Column("organization_id")]
        public string organization_id { get; set; }

        [Column("received_type")]
        public string ReceivedType { get; set; }


    }
}
