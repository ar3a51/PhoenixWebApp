﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Phoenix.Data.Models
{
    [Table("fixed_asset_depreciation", Schema = "fn")]
    public class FixedAssetDepreciation : FinanceEntityBase
    {
        [MaxLength(50)]
        [Column(nameof(Id)), StringLength(50)]
        public string Id { get; set; }
        [Column("fixed_asset_id")]
        public string FixedAssetID { get; set; }
        [Column("depreciation_date")]
        public DateTime? DepreciationDate { get; set; }
        [Column("depreciation_value")]
        public decimal? DepreciationValue { get; set; }
        [Column("accumulated_depreciation")]
        public decimal? AccumulatedDepreciation { get; set; }
        [Column("book_value")]
        public decimal? BookValue { get; set; }
        [Column("residual")]
        public decimal? Residual { get; set; }
        [Column("status")]
        public string Status { get; set; }
        [Column("depreciation_name")]
        public string DepreciationName { get; set; }
        [Column("depreciation_id")]
        public string DepreciationId { get; set; }
    }
}
