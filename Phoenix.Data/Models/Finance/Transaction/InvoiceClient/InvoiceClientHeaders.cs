﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Phoenix.Data.Models.Finance.Transaction
{
    [Table("invoice_client_headers", Schema = "fn")]
    public class InvoiceClientHeaders : HrisEntityBase
    {
        [Key]
        [MaxLength(50)]
        [Column(nameof(Id)), StringLength(50)]
        public string Id { get; set; }

        [MaxLength(50)]
        [Column("pce_id"), StringLength(50)]
        public string PceId { get; set; }

        [MaxLength(50)]
        [Column("brand_id"), StringLength(50)]
        public string BrandId { get; set; }

        [MaxLength(50)]
        [Column("client_id"), StringLength(50)]
        public string ClientId { get; set; }

        [MaxLength(50)]
        [Column("currency_id"), StringLength(50)]
        public string CurrencyId { get; set; }

        [Column("invoice_date")]
        public DateTime? InvoiceDate { get; set; }

        [MaxLength(50)]
        [Column("invoice_number"), StringLength(50)]
        public string InvoiceNumber { get; set; }

        [Required]
        [Column("due_date")]
        public DateTime DueDate { get; set; }

        [MaxLength(50)]
        [Column("alternate_client_id"), StringLength(50)]
        public string AlternateClientId { get; set; }

        [Column(nameof(Amount))]
        public decimal? Amount { get; set; }

        [Column(nameof(Vat))]
        public decimal? Vat { get; set; }

        [Column("exchange_rate")]
        public decimal? ExchangeRate { get; set; }

        [Column("grand_total")]
        public decimal? GrandTotal { get; set; }

        [MaxLength(500)]
        [Column(nameof(Remarks)), StringLength(500)]
        public string Remarks { get; set; }

        [MaxLength(50)]
        [Column(nameof(Status)), StringLength(50)]
        public string Status { get; set; }
    }
}