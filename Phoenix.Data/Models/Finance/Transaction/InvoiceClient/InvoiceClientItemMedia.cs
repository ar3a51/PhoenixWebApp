﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Phoenix.Data.Models.Finance.Transaction
{
    [Table("invoice_client_item_media", Schema = "fn")]
    public class InvoiceClientItemMedia
    {
        [Key]
        [MaxLength(50)]
        [Column(nameof(Id)), StringLength(50)]
        public string Id { get; set; }

        [MaxLength(50)]
        [Column("parent_id"), StringLength(50)]
        public string ParentId { get; set; }

        [Required]
        [MaxLength(50)]
        [Column("invoice_id"), StringLength(50)]
        public string InvoiceId { get; set; }

        [Column("amount")]
        public decimal Amount { get; set; }

        [Column("sequence")]
        public short? Sequence { get; set; }

        [MaxLength(50)]
        [Column("media"), StringLength(50)]
        public string Media { get; set; }

        [MaxLength(50)]
        [Column("version"), StringLength(50)]
        public string Version { get; set; }

        [MaxLength(50)]
        [Column("date"), StringLength(50)]
        public DateTime? Date { get; set; }

        [MaxLength(50)]
        [Column("description"), StringLength(50)]
        public string Description { get; set; }
        
        [Column("rate")]
        public decimal? Rate { get; set; }

        [Column("ins")]
        public short? Ins { get; set; }

        [Column("gross")]
        public decimal? Gross { get; set; }

        [Column("disc")]
        public short? Disc { get; set; }

        [Column("total")]
        public decimal? Total { get; set; }
    }
}
