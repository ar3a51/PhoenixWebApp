﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Phoenix.Data.Models.Finance.Transaction
{
    [Table("invoice_client_item_other", Schema = "fn")]
    public class InvoiceClientItemOther
    {
        [Key]
        [MaxLength(50)]
        [Column(nameof(Id)), StringLength(50)]
        public string Id { get; set; }

        [MaxLength(50)]
        [Column("parent_id"), StringLength(50)]
        public string ParentId { get; set; }
        
        [MaxLength(50)]
        [Column("invoice_id"), StringLength(50)]
        public string InvoiceId { get; set; }

        [MaxLength(250)]
        [Column(nameof(Description)), StringLength(250)]
        public string Description { get; set; }

        [MaxLength(250)]
        [Column(nameof(Remark)), StringLength(250)]
        public string Remark { get; set; }
        
        [Column("amount_percentage")]
        public decimal AmountPercentage { get; set; }

        [Required]
        [Column(nameof(Amount))]
        public decimal Amount { get; set; }

        [Column("job_id")]
        public string JobID { get; set; }
        
        [Column("pce_id")]
        public string PceID { get; set; }

        [Column("account_id")]
        public string AccountID { get; set; }
        
        [MaxLength(50)]
        [Column("task_id"), StringLength(50)]
        public string TaskId { get; set; }

        [Column("quantity_other")]
        public int? Quantity { get; set; }

        [MaxLength(50)]
        [Column("task_name_other"), StringLength(50)]
        public string TaskName { get; set; }

        [Column("unit_price_other")]
        public decimal? UnitPrice { get; set; }
        [Column("amount_billed_other")]
        public decimal? AmountBilled { get; set; }



        [NotMapped]
        public string AccountName { get; set; }

        //----PCE TASK----
        //[NotMapped]
        //[MaxLength(255)]
        //[Column("task_name"), StringLength(255)]
        //public string TaskName { get; set; }

        //[NotMapped]
        //[Column(nameof(Quantity))]
        //public int? Quantity { get; set; }

        [NotMapped]
        [MaxLength(50)]
        [Column("uom_id"), StringLength(50)]
        public string UomId { get; set; }

        [NotMapped]
        [Column(nameof(Price))]
        public decimal? Price { get; set; }

        [NotMapped]
        [Column(nameof(Asf))]
        public decimal? Asf { get; set; }

        //[NotMapped]
        //[Column("unit_price")]
        //public decimal? UnitPrice { get; set; }

        [NotMapped]
        [Column(nameof(Total))]
        public decimal? Total { get; set; }

        [NotMapped]
        public string UomSymbol { get; set; }

        //[NotMapped]
        //public decimal? AmountBilled { get; set; }

        [NotMapped]
        public decimal? Balance { get; set; }
        [NotMapped]
        public decimal? TempBalance { get; set; }

        [NotMapped]
        public bool IsSelected { get; set; }

        [NotMapped]
        public decimal TotalBillPercent { get; set; }

        [NotMapped]
        public decimal? GrandTotal { get; set; }

        [NotMapped]
        public DateTime? InvoiceDate { get; set; }

        [NotMapped]
        public decimal? AsfPercent { get; set; }
        [NotMapped]
        public decimal? Vat { get; set; }
        [NotMapped]
        public decimal? VatPercent { get; set; }
    }
}
