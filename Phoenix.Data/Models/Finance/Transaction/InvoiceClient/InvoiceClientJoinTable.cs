﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Data.Models.Finance.Transaction
{
    public class InvoiceClientJoinTable : InvoiceClient
    {
        public string PceId { get; set; }
        public string PceCode { get; set; }
        public DateTime? PoDate { get; set; }
        public string ClientId { get; set; }
        public string ClientName { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public string CurrencyName { get; set; }
        public string CurrencyId { get; set; }

        public string BrandName { get; set; }
        public string BrandId { get; set; }

        public string InvoiceCode { get; set; }
        public string InvoiceId { get; set; }
        public DateTime? due_date { get; set; }
        public string remarks { get; set; }
        public decimal? GrandTotal { get; set; }
    }
}
