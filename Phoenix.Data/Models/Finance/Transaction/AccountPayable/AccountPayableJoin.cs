﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Data.Models.Finance.Transaction
{
    public class AccountPayableJoin : AccountPayable
    {
        public string VendorName { get; set; }
        public string CurrencyName { get; set; }
        public string REF { get; set; }
        public string YourREF { get; set; }
        public string Product { get; set; }
        public string InvoiceStatusId { get; set; }
        public string BusinessUnitName { get; set; }
        public string LegalEntityName { get; set; }
        public string InvoiceNumber { get; set; }
    }
}
