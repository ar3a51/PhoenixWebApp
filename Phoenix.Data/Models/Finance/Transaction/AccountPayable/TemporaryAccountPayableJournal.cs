﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Phoenix.Data.Models.Finance.Transaction
{
    [Table("journal_temp", Schema = "fn")]
    public class TemporaryAccountPayableJournal :FinanceEntityBase
    {
        [MaxLength(50)]
        [Column(nameof(Id)), StringLength(50)]
        public string Id { get; set; }

        [Column("transaction_date")]
        public DateTime? transaction_date { get; set; }
        [MaxLength(50)]
        [Column("reference_id"), StringLength(50)]
        public string reference_id { get; set; }
       
        [Column("referencedate")]
        public DateTime? referencedate { get; set; }
        [MaxLength(50)]
        [Column("referenceto"), StringLength(50)]
        public string referenceto { get; set; }

        [MaxLength(50)]
        [Column("account_id"), StringLength(50)]
        public string AccountId { get; set; }
        [NotMapped]
        public string AccountName { get; set; }
        [Column("description")]
        public string Description { get; set; }
        [MaxLength(50)]
        [Column("currency_id"), StringLength(50)]
        public string currency_id { get; set; }
        [Column("qty")]
        public decimal? qty { get; set; }
        [Column("unit")]
        public string unit { get; set; }

        [Column("exchange_rate")]
        public decimal? exchange_rate { get; set; }
        [Column("price")]
        public decimal? price { get; set; }
        [MaxLength(50)]
        [Column("pce_id"), StringLength(50)]
        public string pce_id { get; set; }
        [Column("job_id")]
        public string JobId { get; set; }
        [NotMapped]
        public string JobIdName { get; set; }
        [Column("external_id")]
        public string external_id { get; set; }
        [Column("internal_id")]
        public string internal_id { get; set; }
       
        [Column("invoice_id")]
        public string invoice_id { get; set; }
        [Column("po_id")]
        public string po_id { get; set; }
        [Column("budgetdebit")]
        public decimal? budgetdebit { get; set; }       
        [Column("budgetcredit")]
        public decimal? budgetcredit { get; set; }
        [Column("budgetgballance")]
        public decimal? budgetgballance { get; set; }
        [Column("beginningdebit")]
        public decimal? beginningdebit { get; set; }
        [Column("beginningcredit")]
        public decimal? beginningcredit { get; set; }
        [Column("beginningballance")]
        public decimal? beginningballance { get; set; }
        [Column("debit")]
        public decimal? Debit { get; set; }
        [Column("credit")]
        public decimal? Credit { get; set; }
        [Column("balance")]
        public decimal? balance { get; set; }
        [Column("endingdebet")]
        public decimal? endingdebet { get; set; }
        [Column("endingcredit")]
        public decimal? endingcredit { get; set; }
        [Column("endingbalance")]
        public decimal? endingbalance { get; set; }
        [Column("rcondition")]
        public string rcondition { get; set; }
        [Column("flag")]
        public bool? flag { get; set; }
        [Column("ftime")]
        public DateTime? ftime { get; set; }
        [Column("mainservice_category_id")]
        public string mainservice_category_id { get; set; }
        [Column("shareservice_id")]
        public string shareservice_id { get; set; }
        [Column("business_unit_id")]
        public string DivisiId { get; set; }
        [NotMapped]
        public string DivisiName { get; set; }
        [Column("legal_id")]
        public string legal_id { get; set; }
        [Column("affiliate_id")]
        public string affiliate_id { get; set; }
        [Column("is_posted")]
        public string is_posted { get; set; }
        [Column("account_code")]
        public string AccountCode { get; set; }
        [Column("line_code")]
        public string LineCode { get; set; }
  
    }
}
