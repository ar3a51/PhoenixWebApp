﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Data.Models.Finance.Transaction
{
    public class TemporaryAccountPayableJoinTable : TemporaryAccountPayable
    {
        public string VendorName { get; set; }
        public string InvoiceReceivedId { get; set; }
        public string InvoiceReceivedNumber { get; set; }
        public DateTime? InvoiceReceivedDate { get; set; }
        public string PONumber { get; set; }
        public DateTime? Period { get; set; }
        public string DivisionId { get; set; }
        public string DivisionName { get; set; }
        public string CurrencyName { get; set; }
        public DateTime? ReceivedDate { get; set; }
        public decimal? AllocationRatioId { get; set; }
        public decimal? VATNumber { get; set; }
        public decimal? Value { get; set; }
        public string StatusName { get; set; }
    }
}
