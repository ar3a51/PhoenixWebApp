﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Phoenix.Data.Models.Finance.Transaction
{
   
    public class TemporaryAccountPayableDTO
    {
        public TemporaryAccountPayable Header { get; set; }
        public List<TemporaryAccountPayableDetail> Details { get; set; }
    }
}
