﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Data.Models.Finance.Transaction
{
    public class AccountPayableDTO
    {
        public AccountPayable Header { get; set; }
        public List<AccountPayableDetail> Details { get; set; }
    }
}
