﻿using System.Collections.Generic;

namespace Phoenix.Data.Models.Finance.Transaction
{
    public class FixedAssetDTO
    {
        public FixedAsset Header { get; set; }
        public List<FixedAssetMovement> Details { get; set; }
    }
}
