﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Phoenix.Data.Models
{
    [Table("fixed_asset", Schema ="fn")]
    public class FixedAsset : FinanceEntityBase
    {
        [MaxLength(50)]
        [Column(nameof(Id)), StringLength(50)]
        public string Id { get; set; }
        [Column("item_id")]
        public string ItemId { get; set; }
        [Column("fixed_asset_name")]
        public string FixedAssetName { get; set; }
        [Column("fixed_asset_number")]
        public string FixedAssetNumber { get; set; }
        [Column("unit_count")]
        public decimal? UnitCount { get; set; }
        [Column("purchase_order_id")]
        public string PurchaseOrderId { get; set; }
        [Column("invoice_id")]
        public string InvoiceId { get; set; }
        [Column("evident_number")]
        public string EvidentNumber { get; set; }
        [Column("current_price")]
        public decimal? CurrentPrice { get; set; }
        [Column("currency_id")]
        public string CurrencyId { get; set; }
        [Column("exchange_rate")]
        public decimal? ExchangeRate { get; set; }
        [Column("current_location")]
        public string CurrentLocation { get; set; }
        [Column("account_id")]
        public string AccountId { get; set; }
        [Column("commercial_depreciation_id")]
        public string CommercialDepreciationId { get; set; }
        [Column("economic_period")]
        public int? EconomicPeriod { get; set; }
        [Column("description")]
        public string Description { get; set; }
        [Column("registration_date")]
        public DateTime? RegistrationDate { get; set; }
        [Column("item_type_id")]
        public string ItemTypeId { get; set; }
        [Column("item_group_id")]
        public string ItemGroupId { get; set; }
        [Column("item_category_id")]
        public string ItemCategoryId { get; set; }
        [Column("item_condition_id")]
        public string ItemConditionId { get; set; }
        [Column("serial_number")]
        public string SerialNumber { get; set; }
        [Column("gross_value")]
        public decimal? GrossValue { get; set; }
        [Column("residue")]
        public decimal? Residue { get; set; }
        [Column("acquisition_cost")]
        public decimal? AcquisitionCost { get; set; }
        [Column("rounding")]
        public string Rounding { get; set; }
        [Column("coa_id_akumulasi")]
        public string CoaIdAkumulasi { get; set; }
        [Column("coa_id_depresiasi")]
        public string CoaIdDepresiai { get; set; }
        [Column("good_received_id")]
        public string GoodReceiptId { get; set; }
        [Column("good_received_detail_id")]
        public string GoodReceiptDetailId { get; set; }
        [Column("manufacture")]
        public string Manufacture { get; set; }
        [Column("brand")]
        public string Brand { get; set; }
        [Column("model")]
        public string Model { get; set; }
        [Column("product_number")]
        public string ProductNumber { get; set; }
        [Column("sku_code")]
        public string SkuCode { get; set; }
        [Column("color")]
        public string Color { get; set; }
        [Column("qty")]
        public decimal? Qty { get; set; }
        [Column("uom")]
        public string Uom { get; set; }
        [Column("condition")]
        public string Condition { get; set; }
        [Column("business_unit")]
        public string BusinessUnit { get; set; }
        [Column("legal_entity")]
        public string LegalEntity { get; set; }
        [Column("affiliation")]
        public string Affiliation { get; set; }
        [Column("lifetime")]
        public decimal? Lifetime { get; set; }
        [Column("master_depreciation_id")]
        public string MasterDepreciationId { get; set; }
        [Column("status_depreciation")]
        public string StatusDepreciation { get; set; }
        [Column("remarks_depreciation")]
        public string RemarksDepreciation { get; set; }
        [NotMapped]
        public string Status { get; set; }
        [NotMapped]
        public string ConditionName { get; set; }
        [NotMapped]
        public string LocationName { get; set; }
    }
}
