﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Phoenix.Data.Models
{
    [Table("vw_general_ledger", Schema = "fn")]
    public class GeneralLedgerDetail
    {
        [Key]
        [Column(nameof(Id))]
        public Guid? Id { get; set; }

        [MaxLength(15)]
        [Column(nameof(Report)), StringLength(15)]
        public string Report { get; set; }

        [MaxLength(20)]
        [Column(nameof(Rpotype)), StringLength(20)]
        public string Rpotype { get; set; }

        [MaxLength(50)]
        [Column(nameof(Desc1)), StringLength(50)]
        public string Desc1 { get; set; }

        [MaxLength(50)]
        [Column(nameof(Fdesc1)), StringLength(50)]
        public string Fdesc1 { get; set; }

        [MaxLength(50)]
        [Column(nameof(Accname)), StringLength(50)]
        public string Accname { get; set; }

        [MaxLength(80)]
        [Column(nameof(Accdesc)), StringLength(80)]
        public string Accdesc { get; set; }

        [Column("client_id")]
        public int? ClientId { get; set; }

        [MaxLength(2000)]
        [Column(nameof(Description)), StringLength(2000)]
        public string Description { get; set; }

        [Column("periode_from")]
        public DateTime? PeriodeFrom { get; set; }

        [Column("periode_to")]
        public DateTime? PeriodeTo { get; set; }

        [Column("your_reff")]
        public int? YourReff { get; set; }

        [Required]
        [MaxLength(50)]
        [Column("evd_no"), StringLength(50)]
        public string EvdNo { get; set; }

        [Column(nameof(Referencedate))]
        public DateTime? Referencedate { get; set; }

        [MaxLength(100)]
        [Column("job_id"), StringLength(100)]
        public string JobId { get; set; }

        [MaxLength(50)]
        [Column("mainservice_category"), StringLength(50)]
        public string MainserviceCategory { get; set; }

        [MaxLength(100)]
        [Column("pce_id"), StringLength(100)]
        public string PceId { get; set; }

        [MaxLength(50)]
        [Column("pca_id"), StringLength(50)]
        public string PcaId { get; set; }

        [MaxLength(2000)]
        [Column("trx_description"), StringLength(2000)]
        public string TrxDescription { get; set; }

        [MaxLength(350)]
        [Column(nameof(Division)), StringLength(350)]
        public string Division { get; set; }

        [MaxLength(5)]
        [Column("curr_code"), StringLength(5)]
        public string CurrCode { get; set; }

        [Column("exchange_rate")]
        public decimal? ExchangeRate { get; set; }

        [Column(nameof(Debit))]
        public decimal? Debit { get; set; }

        [Column(nameof(Credit))]
        public decimal? Credit { get; set; }

        [Required]
        [Column(nameof(Adjustment))]
        public decimal? Adjustment { get; set; }

        [MaxLength(50)]
        [Column("currency_id"), StringLength(50)]
        public string CurrencyId { get; set; }

        [MaxLength(50)]
        [Column("legal_entity_id"), StringLength(50)]
        public string LegalEntityId { get; set; }
    }

    public class GeneralLedgerDetailSearch
    {
        public string LegalEntityId { get; set; }
        public DateTime? PeriodeFrom { get; set; }
        public DateTime? PeriodeTo { get; set; }
        public string EvdNo { get; set; }
        public string CurrencyId { get; set; }
        public decimal? ExchangeRate { get; set; }
        public string Description { get; set; }
        public decimal? Adjustment { get; set; }
    }
}
