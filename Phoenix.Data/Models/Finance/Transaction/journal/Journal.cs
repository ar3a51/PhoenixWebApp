﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Phoenix.Data.Models
{
	[Table(nameof(Journal), Schema = "fn")]
	public class Journal : FinanceEntityBase
	{
		[Key]
		[MaxLength(50)]
		[Column(nameof(Id)), StringLength(50)]
		public string Id { get; set; }

		[Column("transaction_date")]
		public DateTime? TransactionDate { get; set; }

		[MaxLength(100)]
		[Column("reference_id"), StringLength(100)]
		public string ReferenceId { get; set; }

		[MaxLength(100)]
		[Column("reference_number"), StringLength(100)]
		public string ReferenceNumber { get; set; }

		[Column("referencedate")]
		public DateTime? ReferenceDate { get; set; }

		[MaxLength(100)]
		[Column("referenceto"), StringLength(100)]
		public string ReferenceTo { get; set; }

		[Column(nameof(Description)), StringLength(2147483647)]
		public string Description { get; set; }

		[MaxLength(50)]
		[Column("currency_id"), StringLength(50)]
		public string CurrencyId { get; set; }

		[Column(nameof(Qty))]
		public decimal? Qty { get; set; }

		[Column("exchange_rate")]
		public decimal? ExchangeRate { get; set; }

		[MaxLength(20)]
		[Column(nameof(Unit)), StringLength(20)]
		public string Unit { get; set; }

		[Column(nameof(Price))]
		public int? Price { get; set; }

		[MaxLength(100)]
		[Column("pce_id"), StringLength(100)]
		public string PceId { get; set; }

		[MaxLength(100)]
		[Column("pca_id"), StringLength(100)]
		public string PcaId { get; set; }

		[MaxLength(100)]
		[Column("job_id"), StringLength(100)]
		public string JobId { get; set; }

		[MaxLength(50)]
		[Column("external_id"), StringLength(50)]
		public string ExternalId { get; set; }

		[MaxLength(50)]
		[Column("internal_id"), StringLength(50)]
		public string InternalId { get; set; }

		[MaxLength(40)]
		[Column("account_id"), StringLength(40)]
		public string AccountId { get; set; }

		[MaxLength(50)]
		[Column("invoice_id"), StringLength(50)]
		public string InvoiceId { get; set; }

		[MaxLength(50)]
		[Column("po_id"), StringLength(50)]
		public string PoId { get; set; }

		[MaxLength(50)]
		[Column("pca_task_id"), StringLength(50)]
		public string PcaTaskId { get; set; }


		[Column(nameof(Budgetdebit))]
		public decimal? Budgetdebit { get; set; }

		[Column(nameof(Budgetcredit))]
		public decimal? Budgetcredit { get; set; }

		[Column(nameof(Budgetgballance))]
		public decimal? Budgetgballance { get; set; }

		[Column(nameof(Beginningdebit))]
		public decimal? Beginningdebit { get; set; }

		[Column(nameof(Beginningcredit))]
		public decimal? Beginningcredit { get; set; }

		[Column(nameof(Beginningballance))]
		public decimal? Beginningballance { get; set; }

		[Column(nameof(Debit))]
		public decimal? Debit { get; set; }

		[Column(nameof(Credit))]
		public decimal? Credit { get; set; }

		[Column(nameof(Balance))]
		public decimal? Balance { get; set; }

		[Column(nameof(Endingdebet))]
		public decimal? Endingdebet { get; set; }

		[Column(nameof(Endingcredit))]
		public decimal? Endingcredit { get; set; }

		[Column(nameof(Endingbalance))]
		public decimal? Endingbalance { get; set; }

		[MaxLength(20)]
		[Column(nameof(Rcondition)), StringLength(20)]
		public string Rcondition { get; set; }

		[Column(nameof(Flag))]
		public bool? Flag { get; set; }

		[Column(nameof(Fdate))]
		public DateTime? Fdate { get; set; }

		[Column(nameof(Ftime))]
		public TimeSpan? Ftime { get; set; }

		[MaxLength(50)]
		[Column("mainservice_category_id"), StringLength(50)]
		public string MainserviceCategoryId { get; set; }

		[Column("shareservice_id"), StringLength(2147483647)]
		public string ShareserviceId { get; set; }

		[MaxLength(50)]
		[Column("business_unit_id"), StringLength(50)]
		public string DivisiId { get; set; }

		[MaxLength(50)]
		[Column("legal_id"), StringLength(50)]
		public string LegalId { get; set; }

		[MaxLength(50)]
		[Column("affiliate_id"), StringLength(50)]
		public string AffiliateId { get; set; }

		[MaxLength(50)]
		[Column("is_posted"), StringLength(50)]
		public string IsPosted { get; set; }

		[MaxLength(50)]
		[Column("account_code"), StringLength(50)]
		public string AccountCode { get; set; }

		[MaxLength(50)]
		[Column("line_code"), StringLength(50)]
		public string LineCode { get; set; }

		[MaxLength(50)]
		[Column("type_of_expense_id"), StringLength(50)]
		public string TypeOfExpenseId { get; set; }

		[MaxLength(1)]
		[Column("is_job_closed"), StringLength(1)]
		public string IsJobClosed { get; set; }

		[NotMapped]
		public string AccountName { get; set; }

		[NotMapped]
		public string JobIdName { get; set; }

		[NotMapped]
		public string DivisiName { get; set; }

		[NotMapped]
		public string AfiliationName { get; set; }

		[NotMapped]
		public string LegalEntityName { get; set; }

		[NotMapped]
		public string CodeRec { get; set; }

		[NotMapped]
		public string PcaTaskName { get; set; }
	}
}