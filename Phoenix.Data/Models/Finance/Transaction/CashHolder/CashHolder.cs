﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Phoenix.Data.Models.Finance.Transaction.CashHolder
{
    [Table("cash_holder", Schema = "fn")]
    public class CashHolder : FinanceEntityBase
    {
        [Key]
        [MaxLength(50)]
        [Column(nameof(Id)), StringLength(50)]
        public string Id { get; set; }

        [MaxLength(50)]
        [Column("employee_id"), StringLength(50)]
        public string EmployeeId { get; set; }

        [MaxLength(50)]
        [Column("bank_id"), StringLength(50)]
        public string BankId { get; set; }

        [MaxLength(50)]
        [Column("account_number"), StringLength(50)]
        public string AccountNumber { get; set; }

        [MaxLength(50)]
        [Column("job_level_id"), StringLength(50)]
        public string JobLevelId { get; set; }

        [Column("ending_balance")]
        public decimal? EndingBalance { get; set; }
    }
}