﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Phoenix.Data.Models.Finance.Transaction.InventoryGoodsReceipt
{
    [Table("inventory_goods_receipt_detail", Schema = "fn")]
    public class InventoryGoodsReceiptDetail : FinanceEntityBase
    {
        [MaxLength(50)]
        [Column(nameof(Id)), StringLength(50)]
        public string Id { get; set; }
        [Column("inventory_goods_receipt_id")]//,<, nvarchar(50),>
        public string InventoryGoodsReceiptId { get; set; }
        [Column("item_id")]   //,<, nvarchar(50),>
        public string ItemId { get; set; }
        [Column("item_name")]   // ,<, nvarchar(450),>
        public string ItemName { get; set; }
        [Column("item_type_id")]   //,<, nvarchar(50),>
        public string ItemTypeId { get; set; }
        [Column("item_type_name")]   // ,<, nvarchar(450),>
        public string ItemTypeName { get; set; }
        [Column("item_category_id")]  // ,<, nvarchar(50),>
        public string CategoryId { get; set; }
        [Column("item_category_name")]   //,<, nvarchar(450),>
        public string CategoryName { get; set; }
        [Column("uom_id")]  // ,<, nvarchar(50),>
        public string UOMId { get; set; }
        [Column("unit_name")]  //,<, nvarchar(450),>
        public string UnitName { get; set; }
        [Column("brand")]  // ,<, nvarchar(450),>
        public string Brannd { get; set; }
        [Column("subbrand")]  // ,<, nvarchar(450),>
        public string SubBrand { get; set; }
        [Column("description")]  // ,<, nvarchar(450),>
        public string Description { get; set; }
        [Column("received_qty")]   // ,<, decimal (18,5),>
        public decimal?  ReceivedQty{ get; set; }
        [Column("purchased_qty")]   // ,<, decimal (18,5),>
        public decimal? PurchasedQty { get; set; }
        [Column("total_received_qty")]  // ,<, decimal (18,5),>
        public decimal? TotalReceivedQty { get; set; }
        [Column("remark")]   // ,<, nvarchar(450),>
        public string Remark { get; set; }
        [Column("file_attachment")] //  ,<, nvarchar(50),>
        public string FileAttachment { get; set; }
        [Column("purchase_order_detail_id")] // ,<, nvarchar(50),>
        public string PurchaseOrderDetailId { get; set; }
        [Column("item_code")]   //,<, nvarchar(50),>)
        public string ItemCode { get; set; }
        [Column("remaining_qty")]
        public decimal? RemainingQty { get; set; }
        [Column("task_id")]
        public string TaskId { get; set; }
        [Column("task")]
        public string Task { get; set; }
        [Column("inventory_id")]
        public string InventoryId { get; set; }
        [Column("inventory_name")]
        public string InventoryName { get; set; }
        [NotMapped]
        public decimal? UnitPrice { get; set; }
        [NotMapped]
        public decimal? Amount { get; set; }
        [NotMapped]
        public string Location { get; set; }
    }
}
