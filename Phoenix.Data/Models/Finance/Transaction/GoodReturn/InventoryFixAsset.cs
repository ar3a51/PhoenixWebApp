﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Data.Models.Finance.Transaction.GoodReturn
{
   public class InventoryFixAsset
    {
        public string Inv_Fa_Number { get; set; }
        public string Inv_Fa_Name { get; set; }
    }
}
