﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Phoenix.Data.Models.Finance
{
    [Table("ca_settlement_detail", Schema = "fn")]
    public class CashAdvanceSettlementDetail : FinanceEntityBase
    {
        [Key]
        [MaxLength(50)]
        [Column(nameof(Id)), StringLength(50)]
        public string Id { get; set; }


        [MaxLength(50)]
        [Column("ca_settlement_id"), StringLength(50)]
        public string CashAdvanceSettleId { get; set; }

        [MaxLength(50)]
        [Column("ca_detail_id"), StringLength(50)]
        public string CashAdvanceDetailId { get; set; }

        [Column("Qty")]
        public decimal? ActualQty { get; set; }

        [Column("actual_unit_price")]
        public decimal? ActualUnitPrice { get; set; }

        [Column("actual_amount")]
        public decimal? ActualAmount { get; set; }

        [Column("account_id")]
        public string AccountId { get; set; }

        [MaxLength(500)]
        [Column("file_receipt"), StringLength(500)]
        public string Receipt { get; set; }

        [Column("taxpayable")]
        public decimal? TaxPayable { get; set; }

        [Column("taxpayable_percent")]
        public decimal? TaxPayablePercent { get; set; }

        [Column("tax_pay_coa_id")]
        public string TaxPayableCoaId { get; set; }

        [Column("task_id")]
        public string TaskId { get; set; }

        [Column("task_name")]
        public string TaskName { get; set; }
        

        [NotMapped]
        public decimal? Qty { get; set; }
        [NotMapped]
        public string AccountCode { get; set; }
        [Column("purchase_date")]
        public DateTime? PurchaseDate { get; set; }

        [NotMapped]
        public string AccountName { get; set; }

        [NotMapped]
        public string AccountIdView { get; set; }

        [NotMapped]
        public string FileReceiptNameDetail { get; set; }

        [NotMapped]
        public string TempFileIdDetail { get; set; }
    }
}
