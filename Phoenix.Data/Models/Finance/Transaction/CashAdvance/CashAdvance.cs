﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Phoenix.Data.Models.Finance
{
    [Table("cash_advance", Schema = "fn")]
    public class CashAdvance : FinanceEntityBase
    {
        [Key]
        [MaxLength(50)]
        [Column(nameof(Id)), StringLength(50)]
        public string Id { get; set; }

        [Required]
        [MaxLength(50)]
        [Column("ca_number"), StringLength(50)]
        public string CaNumber { get; set; }

        [Required]
        [Column("ca_date")]
        public DateTime CaDate { get; set; }

        [Column(nameof(Revision))]
        public int? Revision { get; set; }

        [MaxLength(50)]
        [Column("requested_by"), StringLength(50)]
        public string RequestedBy { get; set; }

		[MaxLength(50)]
        [Column("requested_on"), StringLength(50)]
        public string RequestedOn { get; set; }

        [MaxLength(50)]
        [Column("business_unit_id"), StringLength(50)]
        public string BusinessUnitId { get; set; }

        [MaxLength(50)]
        [Column("legal_entity_id"), StringLength(50)]
        public string LegalEntityId { get; set; }

        [Column("is_project_cost")]
        public bool? IsProjectCost { get; set; }

        [MaxLength(50)]
        [Column("purchase_order_id"), StringLength(50)]
        public string PurchaseOrderId { get; set; }

        [MaxLength(50)]
        [Column("purchase_order_number"), StringLength(50)]
        public string PurchaseOrderNumber { get; set; }

        [MaxLength(50)]
        [Column("job_id"), StringLength(50)]
        public string JobId { get; set; }

        [MaxLength(50)]
        [Column("job_number"), StringLength(50)]
        public string JobNumber { get; set; }

        [MaxLength(450)]
        [Column("client_name"), StringLength(450)]
        public string ClientName { get; set; }

        [MaxLength(450)]
        [Column("project_activity"), StringLength(450)]
        public string ProjectActivity { get; set; }

        [MaxLength(450)]
        [Column("project_area"), StringLength(450)]
        public string ProjectArea { get; set; }

        [MaxLength(50)]
        [Column("vendor_id"), StringLength(50)]
        public string VendorId { get; set; }

        [MaxLength(450)]
        [Column("vendor_name"), StringLength(450)]
        public string VendorName { get; set; }

        [MaxLength(50)]
        [Column("employee_id"), StringLength(50)]
        public string EmployeeId { get; set; }

        [MaxLength(450)]
        [Column(nameof(Notes)), StringLength(450)]
        public string Notes { get; set; }

        [MaxLength(50)]
        [Column("payee_bank_id"), StringLength(50)]
        public string PayeeBankId { get; set; }

        [MaxLength(50)]
        [Column("payee_account_number"), StringLength(50)]
        public string PayeeAccountNumber { get; set; }

        [MaxLength(450)]
        [Column("payee_account_name"), StringLength(450)]
        public string PayeeAccountName { get; set; }

        [MaxLength(450)]
        [Column("payee_bank_branch"), StringLength(450)]
        public string PayeeBankBranch { get; set; }

        [Column("ca_period_from")]
        public DateTime? CaPeriodFrom { get; set; }

        [Column("ca_period_to")]
        public DateTime? CaPeriodTo { get; set; }

        [MaxLength(50)]
        [Column("approval_id"), StringLength(50)]
        public string ApprovalId { get; set; }

        [MaxLength(50)]
        [Column("ca_status_id"), StringLength(50)]
        public string CaStatusId { get; set; }

        [Column("request_employee_id")]
        public string RequestEmployeeId { get; set; }
        [Column("request_employee_name")]
        public string RequestEmployeeName { get; set; }
        [Column("request_job_title_id")]
        public string RequestJobTitleId { get; set; }
        [Column("request_job_title_name")]
        public string RequestJobTitleName { get; set; }
        [Column("request_job_grade_id")]
        public string RequestJobGradeId { get; set; }
        [Column("request_job_grade_name")]
        public string RequestJobGradeName { get; set; }
        [Column("amount")]
        public decimal? Amount { get; set; }

        [Column("request_business_unit_id")]
        public string RequestBusinessUnitId { get; set; }

        [Column("share_service_id")]
        public string ShareservicesId { get; set; }

        [Column("pce_type_id")]
        public string PCETypeId { get; set; }

        [Column("client_brand_id")]
        public string ClientBrandId { get; set; }

        [NotMapped]
        public string MainServiceCategoryName { get; set; }

        [NotMapped]
        public string VendorPIC { get; set; }

        [NotMapped]
        public string VendorAddress { get; set; }

        [NotMapped]
        public string VendorTaxNumber { get; set; }
        

        [NotMapped]
		public string StatusDesc { get; set; }
        [NotMapped]
        public string MainServiceCategoryId { get; set; }

        //public ApplicationUser ApplicationUser { get; set; }
        public BusinessUnit BusinessUnit { get; set; }

		[NotMapped]
		public EmployeeBasicInfo RequestEmployee { get; set; }

		[NotMapped]
		public Um.TmUserApp Owner { get; set; }

		[NotMapped]
		public List<CashAdvanceDetail> CashAdvanceDetails { get; set; }
        [NotMapped]
        public string RemarkRejected { get; set; }

        [NotMapped]
        public string BusinessUnitName { get; set; }

        [NotMapped]
        public string RequestBusinessUnitName { get; set; }

        [NotMapped]
        public string LegalEntityName { get; set; }
        [NotMapped]
        public string LogDescription { get; set; }

        [NotMapped]
        public string JobName { get; set; }

        //public LegalEntity LegalEntity { get; set; }
        //public PurchaseOrder PurchaseOrder { get; set; }
        //public JobPa JobPa { get; set; }
        //public Company Company { get; set; }
        //public Bank Bank { get; set; }
        //public CashAdvanceStatus CashAdvanceStatus { get; set; }
    }
}
