﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Phoenix.Data.Models.Finance.Transaction
{
   
    public class CashAdvanceSattlementDTO
    {
        public CashAdvanceSettlement Header { get; set; }
        public List<CashAdvanceSettlementDetail> Details { get; set; }
        public IFormFile fileUpload { get; set; }
    }
}
