﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Phoenix.Data.Models.Finance
{
    [Table("ca_settlement", Schema = "fn")]
    public class CashAdvanceSettlement : FinanceEntityBase
    {
        [Key]
        [MaxLength(50)]
        [Column(nameof(Id)), StringLength(50)]
        public string Id { get; set; }

        [Required]
        [MaxLength(50)]
        [Column("cash_advance_id"), StringLength(50)]
        public string CashAdvanceId { get; set; }

        [Required]
        [Column("ca_settlement_date")]
        public DateTime CashAdvanceSettleDate { get; set; }

		[Column("total_amount")]
		public decimal? TotalAmount { get; set; }

		[Column("ca_balance")]
		public decimal? Balance { get; set; }		

        [Column("amount")]
        public decimal? Amount { get; set; }

		[MaxLength(500)]
		[Column("file_receipt"), StringLength(500)]
		public string FileReceipt { get; set; }

        [Column("job_id")]
        public string JobId { get; set; }

        [Column("job_number")]
        public string JobNumber { get; set; }

        [Column("mainservice_category_id")]
        public string MainServiceCategoryId { get; set; }

        [Column("vat")]
        public decimal? Vat { get; set; }

        [Column("vatpercent")]
        public double? VatPercent { get; set; }

        [Column("taxpayable")]
        public decimal? TaxPayable { get; set; }

        [Column("taxpayablepercent")]
        public decimal? TaxPayablePercent { get; set; }

        [Column("tax_pay_coa_id")]
        public string TaxPayCoaId { get; set; }

        [Column("request_employee_name")]
        public string RequestEmployeeName { get; set; }

        [Column("request_employee_id")]
        public string RequestEmployeeId { get; set; }

        [Column("request_job_title_id")]
        public string RequestJobTitleId { get; set; }

        [Column("request_job_title_name")]
        public string RequestJobTitleName { get; set; }

        [Column("request_job_grade_id")]
        public string RequestJobGradeId { get; set; }

        [Column("request_job_grade_name")]
        public string RequestJobGradeName { get; set; }

        [Column("bank_id")]
        public string BankId { get; set; }

        [Column("account_number")]
        public string AccountNumber { get; set; }

        [Column("sattlement_number")]
        public string SattlementNumber { get; set; }

        [Column("ca_status_id")]
        public string CaStatusId { get; set; }

        [Column("to_account")]
        public string ToAccount { get; set; }

        [Column("to_account_number")]
        public string ToAccountNumber { get; set; }

        [NotMapped]
        public string FileReceiptName { get; set; }

        [NotMapped]
        public string RemarkRejected { get; set; }

        [NotMapped]
        public string ClientName { get; set; }

        [NotMapped]
        public string CaNumber { get; set; }

        [NotMapped]
        public DateTime CaDate { get; set; }

        [NotMapped]
        public string LegalEntityId { get; set; }

        [NotMapped]
        public string RequestBusinessUnitId { get; set; }

        [NotMapped]
        public string StatusDesc { get; set; }

        [NotMapped]
        public string ProjectActivity { get; set; }

        [NotMapped]
        public string LogDescription { get; set; }

        [NotMapped]
        public string VendorId { get; set; }
        [NotMapped]
        public string VendorPIC { get; set; }
        [NotMapped]
        public string VendorTaxNumber { get; set; }
        [NotMapped]
        public string VendorAddress { get; set; }

        [NotMapped]
        public string JobName { get; set; }

        [NotMapped]
		public List<CashAdvanceSettlementDetail> CashAdvanceSettlementDetail { get; set; }

        [NotMapped]
        public string BusinessUnitName { get; set; }
        [NotMapped]
        public string RequestorDivision { get; set; }
        [NotMapped]
        public string LegalEntityName { get; set; }
        [NotMapped]
        public string MainServiceVategoryName { get; set; }
        [NotMapped]
        public DateTime? CaPeriodTo { get; set; }
    }
}
