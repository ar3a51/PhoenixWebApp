﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Phoenix.Data.Models.Finance.Master
{
    [Table("currency", Schema = "dbo")]
    public class Currency : FinanceEntityBase
    {
        [Key]
        [MaxLength(50)]
        [Column(nameof(Id)), StringLength(50)]
        public string Id { get; set; }

        [MaxLength(400)]
        [Column("currency_name"), StringLength(400)]
        public string CurrencyName { get; set; }

        [MaxLength(5)]
        [Column("currency_code"), StringLength(5)]
        public string CurrencyCode { get; set; }

        [MaxLength(5)]
        [Column("currency_symbol"), StringLength(5)]
        public string CurrencySymbol { get; set; }
    }
}
