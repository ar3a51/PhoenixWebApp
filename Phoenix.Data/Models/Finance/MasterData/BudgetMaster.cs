﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Phoenix.Data.Models.Finance.MasterData
{
    [Table("budget_master", Schema = "fn")]
    public class BudgetMaster : FinanceEntityBase
    {
        [Key]
        [MaxLength(50)]
        [Column(nameof(Id)), StringLength(50)]
        public string Id { get; set; }

        [MaxLength(50)]
        [Column("organization_id"), StringLength(50)]
        public string OrganizationId { get; set; }

        [MaxLength(50)]
        [Column("budget_id"), StringLength(50)]
        public string BudgetId { get; set; }

        [MaxLength(50)]
        [Column("financial_year_id"), StringLength(50)]
        public string FinancialYearId { get; set; }

        [MaxLength(50)]
        [Column("legal_entity_id"), StringLength(50)]
        public string LegalEntityId { get; set; }

        [MaxLength(50)]
        [Column("division_id"), StringLength(50)]
        public string DivisionId { get; set; }

        [MaxLength(50)]
        [Column("business_unit_id"), StringLength(50)]
        public string BusinessUnitId { get; set; }

        [MaxLength(50)]
        [Column("account_id"), StringLength(50)]
        public string AccountId { get; set; }

        [MaxLength(50)]
        [Column("currency_id"), StringLength(50)]
        public string CurrencyId { get; set; }
        
        [Column("exchange_rate")]
        public decimal? ExchangeRate { get; set; }

        [MaxLength(50)]
        [Column("budget_allocation_ratio_id"), StringLength(50)]
        public string BudgetAllocationRatioId { get; set; }

        [Column("budget_version")]
        public int? BudgetVersion { get; set; }

        [MaxLength(50)]
        [Column("budget_owner_id"), StringLength(50)]
        public string BudgetOwnerId { get; set; }

        [MaxLength(50)]
        [Column("budget_status"), StringLength(50)]
        public string BudgetStatus { get; set; }

        [Column("total_budget")]
        public decimal? TotalBudget { get; set; }

        [Column("budget_01")]
        public decimal? Budget01 { get; set; }

        [Column("budget_02")]
        public decimal? Budget02 { get; set; }

        [Column("budget_03")]
        public decimal? Budget03 { get; set; }

        [Column("budget_04")]
        public decimal? Budget04 { get; set; }

        [Column("budget_05")]
        public decimal? Budget05 { get; set; }

        [Column("budget_06")]
        public decimal? Budget06 { get; set; }

        [Column("budget_07")]
        public decimal? Budget07 { get; set; }

        [Column("budget_08")]
        public decimal? Budget08 { get; set; }

        [Column("budget_09")]
        public decimal? Budget09 { get; set; }

        [Column("budget_10")]
        public decimal? Budget10 { get; set; }

        [Column("budget_11")]
        public decimal? Budget11 { get; set; }

        [Column("budget_12")]
        public decimal? Budget12 { get; set; }
    }
}
