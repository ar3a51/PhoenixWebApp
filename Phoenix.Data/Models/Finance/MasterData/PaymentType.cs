﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Phoenix.Data.Models.Finance.Master
{
    [Table("payment_type", Schema = "fn")]
    public class PaymentType : FinanceEntityBase
    {
        [Key]
        [MaxLength(50)]
        [Column(nameof(Id)), StringLength(50)]
        public string Id { get; set; }
        
        [MaxLength(50)]
        [Column("type_name"), StringLength(50)]
        public string TypeName { get; set; }

 
    }
}
