using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Phoenix.Data.Models
{
    [Table("master_allocation_ratio", Schema = "fn")]
    public class MasterAllocationRatio : FinanceEntityBase
    {
        [Key]
        [MaxLength(50)]
        [Column(nameof(Id)), StringLength(50)]
        public string Id { get; set; }

        [MaxLength(50)]
        [Column("cost_allocation_name"), StringLength(50)]
        public string CostAllocationName { get; set; }

        [MaxLength(50)]
        [Column("vat_full_charged"), StringLength(50)]
        public string VatFullCharged { get; set; }

        [Column(nameof(Year))]
        public DateTime? Year { get; set; }

        [MaxLength(50)]
        [Column("chart_account_name"), StringLength(50)]
        public string ChartAccountName { get; set; }

        [MaxLength(50)]
        [Column("chart_account_number"), StringLength(50)]
        public string ChartAccountNumber { get; set; }

        [MaxLength(50)]
        [Column("business_unit_1"), StringLength(50)]
        public string BusinessUnit1 { get; set; }

        [MaxLength(50)]
        [Column("business_unit_2"), StringLength(50)]
        public string BusinessUnit2 { get; set; }

        [MaxLength(50)]
        [Column("business_unit_3"), StringLength(50)]
        public string BusinessUnit3 { get; set; }

        [MaxLength(50)]
        [Column("business_unit_4"), StringLength(50)]
        public string BusinessUnit4 { get; set; }

        [MaxLength(50)]
        [Column("business_unit_5"), StringLength(50)]
        public string BusinessUnit5 { get; set; }

        [MaxLength(50)]
        [Column("business_unit_6"), StringLength(50)]
        public string BusinessUnit6 { get; set; }

        [MaxLength(50)]
        [Column("business_unit_7"), StringLength(50)]
        public string BusinessUnit7 { get; set; }

        [MaxLength(50)]
        [Column("percentage_1"), StringLength(50)]
        public string Percentage1 { get; set; }

        [MaxLength(50)]
        [Column("percentage_2"), StringLength(50)]
        public string Percentage2 { get; set; }

        [MaxLength(50)]
        [Column("percentage_3"), StringLength(50)]
        public string Percentage3 { get; set; }

        [MaxLength(50)]
        [Column("percentage_4"), StringLength(50)]
        public string Percentage4 { get; set; }

        [MaxLength(50)]
        [Column("percentage_5"), StringLength(50)]
        public string Percentage5 { get; set; }

        [MaxLength(50)]
        [Column("percentage_6"), StringLength(50)]
        public string Percentage6 { get; set; }

        [MaxLength(50)]
        [Column("percentage_7"), StringLength(50)]
        public string Percentage7 { get; set; }

        [MaxLength(50)]
        [Column(nameof(Status)), StringLength(50)]
        public string Status { get; set; }

        [MaxLength(50)]
        [Column("legal_entity_id"), StringLength(50)]
        public string LegalEntityId { get; set; }
    }
}