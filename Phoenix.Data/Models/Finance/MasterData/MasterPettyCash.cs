﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Phoenix.Data.Models.Finance.MasterData
{
    [Table("master_petty_cash", Schema = "fn")]
    public class MasterPettyCash : FinanceEntityBase
    {
        [Key]
        [MaxLength(50)]
        [Column(nameof(Id)), StringLength(50)]
        public string Id { get; set; }

        [MaxLength(50)]
        [Column("bank_id"), StringLength(50)]
        public string BankId { get; set; }

        [MaxLength(50)]
        [Column("bank_account_name"), StringLength(50)]
        public string BankAccountName { get; set; }

        [Column("bank_account_number")]
        public string BankAccountNumber { get; set; }

        [Column("currency_id")]
        public string CurrencyId { get; set; }

        [MaxLength(50)]
        [Column("chart_account_id"), StringLength(50)]
        public string ChartAccountId { get; set; }

        [MaxLength(50)]
        [Column("chart_account_name"), StringLength(50)]
        public string ChartAccountName { get; set; }

        [MaxLength(50)]
        [Column("chart_account_number"), StringLength(50)]
        public string ChartAccountNumber { get; set; }

        [MaxLength(50)]
        [Column(nameof(Description)), StringLength(50)]
        public string Description { get; set; }

        [MaxLength(50)]
        [Column(nameof(Status)), StringLength(50)]
        public string Status { get; set; }

        [MaxLength(50)]
        [Column("action_id"), StringLength(50)]
        public string ActionId { get; set; }

        [MaxLength(50)]
        [Column("legal_entity_id"), StringLength(50)]
        public string LegalEntityId { get; set; }

        [Column("is_settle")]
        public bool IsSettle { get; set; }

        [Column("begining_balance")]
        public decimal? BeginingBalance { get; set; }


        [Column("amount_insused")]
        public decimal? AmountInsused { get; set; }

        [NotMapped]
        public string CurrencyCode { get; set; }
        [NotMapped]
        public string BankCode { get; set; }
        [NotMapped]
        public string LegalEntity { get; set; }
        [NotMapped]
        public string BussinessUnitId { get; set; }
        [NotMapped]
        public string BussinessUnit { get; set; }
        [NotMapped]
        public string AffiliationId { get; set; }
        [NotMapped]
        public string Affiliation { get; set; }
    }
}