﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Data.Models
{
    public class TrialBalanceFilter
    {
        public string LegalEntityId { get; set; }
        public string BusinessUnitId { get; set; }
        public string FiscalYearsId { get; set; }
        public string StartPeriodId { get; set; }
        public string EndPeriodId { get; set; }
    }
}
