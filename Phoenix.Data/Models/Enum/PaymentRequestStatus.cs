﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.Data.Models
{
    public class PaymentRequestStatus
    {
        public const string Open = "1";
        public const string WaitingApproval = "2";
        public const string Approved = "3";
        public const string Reject = "4";
        public const string PayBalance = "5";
        public const string Complete = "6";
        public const string Reconsiliation = "7";


        public static string GetStatus(string value)
        {
            string result = "";
            switch (value)
            {
                case Open:
                    result = "Open";
                    break;
                case WaitingApproval:
                    result = "Waiting Approval";
                    break;
                case Approved:
                    result = "Approved";
                    break;
                case Reject:
                    result = "Reject";
                    break;
                case PayBalance:
                    result = "PayBalance";
                    break;
                case Complete:
                    result = "Complete";
                    break;
                case Reconsiliation:
                    result = "Reconsiliation";
                    break;
                default:
                    result = "Open";
                    break;
            }
            return result;
        }

    }
}
