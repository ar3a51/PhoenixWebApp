﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.Data.Models
{
    public class CashAdvanceStatus
    {
        public const string Open = "1";
        public const string WaitingApproval = "2";
        public const string Approved = "3";
        public const string Reject = "4";
        public const string SattlementCheck = "5";
        public const string Complete = "6";


        public static string GetStatus(string value)
        {
            string result = "";
            switch (value)
            {
                case Open:
                    result = "Open";
                    break;
                case WaitingApproval:
                    result = "WaitingApproval";
                    break;
                case Approved:
                    result = "Approved";
                    break;
                case Reject:
                    result = "Reject";
                    break;
                case SattlementCheck:
                    result = "SattlementCheck";
                    break;
                case Complete:
                    result = "Complete";
                    break;
                default:
                    result = "Open";
                    break;
            }
            return result;
        }

    }
}
