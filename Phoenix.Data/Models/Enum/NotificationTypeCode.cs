﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Data.Models
{
    public static class NotificationTypeCode
    {
        public const string CashAdvanceApprovalNotification = "fn.ca.need-approval";
        public const string BankPaymentApprovalNotification = "fn.bp.need-approval";
        public const string PromotionRequestApproval = "hris.promotion.request-approval";
        public const string RotationRequestApproval = "hris.rotation.request-approval";
        public const string MediaPlanRequestApproval = "media.media.request-approval";
        public const string MediaOrderRequestApproval = "media.mediaorder.request-approval";
        public const string InvoiceClientMultiple = "fn.ar-invoiceclientmultiple.need-approval";
        public const string InvoiceClient = "fn.ar-invoiceclien.need-approval";
    }

    public static class NotificationModuleType
    {
        public const string ModuleFinance = "FINANCE";
        public const string ModuleHRIS = "HRIS";
        public const string ModulePM = "PROJECT MANAGEMENT";
        public const string ModuleMEDIA = "MEDIA";
    }
}
