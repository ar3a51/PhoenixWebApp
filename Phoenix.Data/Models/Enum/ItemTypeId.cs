﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Data.Models.Enum
{
    public static class ItemTypeId
    {
        public static string FixedAsset = "27d89bcd-5d43-8e76-61dd-cb392a22c8db";
        public static string Inventory = "d8d3c623-e1de-721d-6d3c-9e52099b7d02";
        public static string NonInventory = "0777eedd-8325-dd42-857a-9618726a5040";
    }
}
