﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Data.Models
{
    public static class TerminationType
    {
        public const string Resignation = "1";
        public const string EndOfContract = "2";
        public const string EndOfProbation = "3";
        public const string Death = "4";
        public const string ProlongedIllness = "5";
        public const string Sanction = "6";
        public const string Retired = "7";
    }
}
