﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Data.Models.Enum
{
    public static class BscCategory
    {
        public static string FinancialPerspective = "Financial Perspective";
        public static string CustomerPerspective = "Customer Perspective";
        public static string OperationalExcellenceAndInnovation = "Operational Excellence And Innovation";
        public static string LearningAndGrowthPerspective = "Learning And Growth Perspective";
    }
}
