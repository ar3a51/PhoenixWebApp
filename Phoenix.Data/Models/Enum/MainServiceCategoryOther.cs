﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Data.Models
{
    public static class MainServiceCategoryId
    {
        public const string Production = "1105194186501656222";
        public const string Media = "2225194186501656576";
        public const string Others = "3335194186501656444";
    }

    public static class MainServiceCategoryOther
    {
        //Ambil data other pada table fn.mainservice_category
        public const string MainServiceCategoryId = "3335194186501656444";
        public const string MainServiceCategoryName = "Others";
    }

}
