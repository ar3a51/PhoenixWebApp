﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Data.Models.Enum
{
    public enum FinancePeriodCode
    {
        AP, AR, CAS, PCS, BP, BR, JE, JA, AMOR, DEP, SA
    }
}
