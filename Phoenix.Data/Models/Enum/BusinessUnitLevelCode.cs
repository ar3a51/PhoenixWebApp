﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Data.Models
{
    public static class BusinessUnitLevelCode
    {
        //Business Unit Level from business_unit_type
        public const int HoldingGroup = 1000;
        public const int BusinessUnitSubgroup = 900;
        public const int Division = 800;
        public const int Department = 700;
    }
}
