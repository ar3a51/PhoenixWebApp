﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Data.Models
{
    public static class MenuUnique
    {
        public const string Rotation = "ROTATIONREQ";
        public const string Promotion = "PROMOTIONREQ";
        public const string Demotion = "DEMOTIONREQ";
        //public const string LegalDocument = "DOCUMENTLEGALREQ";
        public const string LegalDocumentFullfilment = "DOCUMENTLEGALFULLFILMENTREQ";
        public const string LegalSharingRequest = "DOCLEGALSHARINGREQ";
        public const string Sanction = "SNCREQ";
        public const string Retired = "RTDREQ";
        public const string Resignation = "TEREMPRESREQ";
        public const string MassLeave = "MASSLEAVEREQ";
        public const string Leave = "LEAVEREQ";
        public const string Overtime = "OVERTIMEREQ";
        public const string PdrPerformance = "PDRPERFORMANCEREQ";
        public const string PCA = "PCAREQ";
        public const string PCE = "PCEREQ";
        public const string CashAdvance = "ProcCashAdvance";
        public const string CashAdvanceSettlement = "ProcCashAdvanceSattlement";
        public const string Payroll = "PAYROLLREQ";
        public const string TrainingRequisition = "TRNREQ";

        #region Finance
        public const string PurchaseRequest = "ProcPurchaseRequest";
        public const string RFQ = "ProcRFQ";
        public const string PO = "ProcPurchaseOrder";
        public const string PaymentRequest = "ProcPaymentReq";
        public const string FinancialYear = "FinancialYearReq";
        public const string FinancePeriod = "FinancePeriodReq";
        public const string JobClosing = "FNJOBCLOSING";
        public const string GoodRequest = "GOODREQUEST";
        public const string PettyCash = "ProcPettyCash";
        public const string InvoiceClient = "FININVCLNT";
        public const string InvoiceClientMultiplePce = "FININVCLNTMTLP";
        public const string FixedAssetDepreciation = "FADEPRECIATION";
        public const string BankReceived = "ProcBankReceived";
        #endregion

        #region Media
        public const string MediaPlan = "MEDPLANESTMREQ";
        public const string MediaOrder = "MEDIAORDER";
        #endregion

        public const string TimeSheet = "TMSHT";
    }
}
