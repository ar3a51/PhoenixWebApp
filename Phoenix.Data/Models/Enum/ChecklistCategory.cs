﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Data.Models
{
    public static class ChecklistCategory
    {
        //sumber datanya di table employee_checklist_category
        //SELECT id, checklist_category FROM hr.employee_checklist_category
        public const string JobHandover = "109804497641212121";
        public const string OfficeManagement = "109804497641313131";
        public const string Finance = "222804497641000000";
        public const string InformationTechnology = "222804497641313542";
        public const string HumanResouce = "222804497641434343";
        public const string Others = "222804497641555555";
        public const string FinalHandover = "222804497641888888";
    }
}
