using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Models;
using Phoenix.Data.RestApi;
using Phoenix.Shared.Core.Contexts;

namespace Phoenix.Data
{
    public interface IAllowanceComponentService : IDataService<AllowanceComponent>
    {
    }

    public class AllowanceComponentService : IAllowanceComponentService
    {
        readonly DataContext context;

        /// <summary>
        /// And endpoint to manage AllowanceComponent
        /// </summary>
        /// <param name="context">Database context</param>
        public AllowanceComponentService(DataContext context)
        {
            this.context = context;
        }

        public async Task<int> AddAsync(AllowanceComponent entity)
        {
            entity.Id = Guid.NewGuid().ToString();
            await context.PhoenixAddAsync(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> AddRangeAsync(params AllowanceComponent[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteAsync(AllowanceComponent entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteRageAsync(params AllowanceComponent[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> DeleteSoftAsync(AllowanceComponent entity)
        {
            context.PhoenixDelete(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> DeleteSoftRangeAsync(params AllowanceComponent[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> EditAsync(AllowanceComponent entity)
        {
            context.PhoenixEdit(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> EditRangeAsync(AllowanceComponent entity)
        {
            throw new NotImplementedException();
        }

        public async Task<List<AllowanceComponent>> Get()
        {
            var result = await (from a in context.AllowanceComponents
                                join b in context.JobGrades on a.JobGradeId equals b.Id
                                where !(a.IsDeleted ?? false)
                                select new AllowanceComponent
                                {
                                    Id = a.Id,
                                    SubComponentName = a.SubComponentName,
                                    Description = a.Description,
                                    Value = a.Value,
                                    JobGradeId = a.JobGradeId,
                                    JobGradeName = b.GradeName,
                                    CreatedOn = a.CreatedOn
                                }).OrderBy(x => x.CreatedOn).ToListAsync();
            return result;
        }

        public Task<List<AllowanceComponent>> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public async Task<AllowanceComponent> Get(string Id) => await context.AllowanceComponents.AsNoTracking().Where(x => !(x.IsDeleted ?? false) && x.Id == Id).FirstOrDefaultAsync();
    }
}