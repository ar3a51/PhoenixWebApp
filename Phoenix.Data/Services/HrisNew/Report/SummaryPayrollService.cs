using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Models;
using Phoenix.Data.RestApi;
using Phoenix.Shared.Core.Contexts;

namespace Phoenix.Data
{
    public interface ISummaryPayrollService
    {
        Task<DataTable> Get(int month, int year, string legalEntityId);
    }

    public class SummaryPayrollService : ISummaryPayrollService
    {
        readonly DataContext context;

        /// <summary>
        /// And endpoint to manage LeaveBalance
        /// </summary>
        /// <param name="context">Database context</param>
        public SummaryPayrollService(DataContext context)
        {
            this.context = context;
        }
        public async Task<DataTable> Get(int month, int year, string legalEntityId)
        {
            var paramType = new SqlParameter[] {
                                new SqlParameter("@Month", month),
                                new SqlParameter("@Year", year),
                                new SqlParameter("@LegalEntityId", legalEntityId)
                            };

            var result = await context.ExecuteStoredProcedure("uspGetSummaryPayrollList", paramType);
            return result;
        }
    }
}