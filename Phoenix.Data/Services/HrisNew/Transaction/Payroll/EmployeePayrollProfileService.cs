using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Models;
using Phoenix.Data.Models.ViewModel;
using Phoenix.Data.RestApi;
using Phoenix.Shared.Core.Contexts;

namespace Phoenix.Data
{
    public interface IEmployeePayrollProfileService
    {
        Task<int> AddAsync(EmployeePayrollProfile entity);
        Task<List<InfoEmployee>> Get();
        Task<InfoEmployee> Get(string employeeId);
        Task<List<EmployeeSubcomponentAdditional>> GetAdditionalList(string employeeId);
        Task<List<EmployeeSubcomponentDeduction>> GetDeductionList(string employeeId);
    }

    public class EmployeePayrollProfileService : IEmployeePayrollProfileService
    {
        readonly DataContext context;

        /// <summary>
        /// And endpoint to manage EmployeeSubcomponentSalary
        /// </summary>
        /// <param name="context">Database context</param>
        public EmployeePayrollProfileService(DataContext context)
        {
            this.context = context;
        }

        public async Task<int> AddAsync(EmployeePayrollProfile entity)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var additional = await context.EmployeeSubcomponentAdditionals.AsNoTracking().Where(x => x.AdditionalEmployeeBasicInfoId == entity.EmployeeId).ToListAsync();
                    if (additional.Count > 0)
                    {
                        context.EmployeeSubcomponentAdditionals.RemoveRange(additional);
                    }

                    var deductions = await context.EmployeeSubcomponentDeductions.AsNoTracking().Where(x => x.EmployeeBasicInfoId == entity.EmployeeId).ToListAsync();
                    if (deductions.Count > 0)
                    {
                        context.EmployeeSubcomponentDeductions.RemoveRange(deductions);
                    }

                    if (entity.EmployeeSubcomponentAdditionals != null)
                    {
                        Parallel.ForEach(entity.EmployeeSubcomponentAdditionals, (item) =>
                        {
                            item.Id = Guid.NewGuid().ToString();
                            item.AdditionalEmployeeBasicInfoId = entity.EmployeeId;
                            context.PhoenixAddAsync(item);
                        });
                    }


                    if (entity.EmployeeSubcomponentDeductions != null)
                    {
                        Parallel.ForEach(entity.EmployeeSubcomponentDeductions, (item) =>
                        {
                            item.Id = Guid.NewGuid().ToString();
                            item.EmployeeBasicInfoId = entity.EmployeeId;
                            context.PhoenixAddAsync(item);
                        });
                    }

                    var employee = await context.EmployeeBasicInfos.AsNoTracking().Where(x => x.Id == entity.EmployeeId).FirstOrDefaultAsync();
                    if (employee != null)
                    {
                        employee.PtkpSettingId = entity.PtkpSettingId;
                        context.EmployeeBasicInfos.Update(employee);
                    }

                    var save = await context.SaveChangesAsync();
                    transaction.Commit();
                    return save;

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<List<InfoEmployee>> Get()
        {
            return await (from b in context.EmployeeBasicInfos
                          where !(b.IsDeleted ?? false)
                          select new InfoEmployee
                          {
                              EmployeeId = b.Id,
                              EmployeeName = b.NameEmployee,
                              DateEmployed = b.JoinDate,
                              //Phone = b.PhoneNumber,
                              //Email = b.Email,
                              //DateBirth = b.DateBirth,
                              //Age = Convert.ToDateTime(b.DateBirth).AddYears(DateTime.Now.Year - Convert.ToDateTime(b.DateBirth).Year) > DateTime.Now ?
                              //                                 (DateTime.Now.Year - Convert.ToDateTime(b.DateBirth).Year) - 1 : DateTime.Now.Year - Convert.ToDateTime(b.DateBirth).Year,
                              JobTitle = b.BusinessUnitJobLevelId == null ? "" :
                                                                           (from r in context.BusinessUnitJobLevels
                                                                            join s in context.JobTitles on r.JobTitleId equals s.Id
                                                                            where r.Id == b.BusinessUnitJobLevelId
                                                                            select s.TitleName).FirstOrDefault(),
                              Grade = b.BusinessUnitJobLevelId == null ? "" :
                                                                           (from r in context.BusinessUnitJobLevels
                                                                            join s in context.JobGrades on r.JobGradeId equals s.Id
                                                                            where r.Id == b.BusinessUnitJobLevelId
                                                                            select s.GradeName).FirstOrDefault(),
                              EmployementStatus = context.EmployementStatuss.Where(x => x.Id == b.EmployementStatusId).Select(x => x.EmploymentStatusName).FirstOrDefault(),
                              Division = b.BusinessUnitJobLevelId == null ? "" :
                                                                        (from r in context.BusinessUnitJobLevels
                                                                         join s in context.BusinessUnits on r.BusinessUnitId equals s.Id
                                                                         join t in context.BusinessUnitTypes on s.BusinessUnitTypeId equals t.Id
                                                                         where t.BusinessUnitLevel == BusinessUnitLevelCode.Division && r.Id == b.BusinessUnitJobLevelId
                                                                         select s.UnitName).FirstOrDefault()
                          }).ToListAsync();
        }

        public async Task<InfoEmployee> Get(string employeeId)
        {
            return await (from b in context.EmployeeBasicInfos
                          where !(b.IsDeleted ?? false) && b.Id == employeeId
                          select new InfoEmployee
                          {
                              EmployeeId = b.Id,
                              EmployeeName = b.NameEmployee,
                              //DateEmployed = b.JoinDate,
                              Phone = b.PhoneNumber,
                              Email = b.Email,
                              //DateBirth = b.DateBirth,
                              //Age = Convert.ToDateTime(b.DateBirth).AddYears(DateTime.Now.Year - Convert.ToDateTime(b.DateBirth).Year) > DateTime.Now ?
                              //                                 (DateTime.Now.Year - Convert.ToDateTime(b.DateBirth).Year) - 1 : DateTime.Now.Year - Convert.ToDateTime(b.DateBirth).Year,
                              //JobTitle = b.BusinessUnitJobLevelId == null ? "" :
                              //                                             (from r in context.BusinessUnitJobLevels
                              //                                              join s in context.JobTitles on r.JobTitleId equals s.Id
                              //                                              where r.Id == b.BusinessUnitJobLevelId
                              //                                              select s.TitleName).FirstOrDefault(),
                              GradeId = b.BusinessUnitJobLevelId == null ? "" :
                                                                           (from r in context.BusinessUnitJobLevels
                                                                            join s in context.JobGrades on r.JobGradeId equals s.Id
                                                                            where r.Id == b.BusinessUnitJobLevelId
                                                                            select s.Id).FirstOrDefault(),
                              Grade = b.BusinessUnitJobLevelId == null ? "" :
                                                                           (from r in context.BusinessUnitJobLevels
                                                                            join s in context.JobGrades on r.JobGradeId equals s.Id
                                                                            where r.Id == b.BusinessUnitJobLevelId
                                                                            select s.GradeName).FirstOrDefault(),
                              //EmployementStatus = context.EmployementStatuss.Where(x => x.Id == b.EmployementStatusId).Select(x => x.EmploymentStatusName).FirstOrDefault(),
                              Mobile = b.MobileNumber,
                              Gender = b.Gender,
                              PtkpSettingId = b.PtkpSettingId
                          }).FirstOrDefaultAsync();
        }

        public async Task<List<EmployeeSubcomponentAdditional>> GetAdditionalList(string employeeId)
        {
            var result = new List<EmployeeSubcomponentAdditional>();
            var salaryComponent = await (from a in context.EmployeeSubcomponentAdditionals
                                         join b in context.SalaryMainComponents on a.AdditionalComponentId equals b.Id
                                         where a.AdditionalSalaryComponent == SalaryComponentTypeAdditional.SalaryComponent && !(a.IsDeleted ?? false) && a.AdditionalEmployeeBasicInfoId == employeeId
                                         select new EmployeeSubcomponentAdditional
                                         {
                                             Id = a.Id,
                                             AdditionalEmployeeBasicInfoId = a.AdditionalEmployeeBasicInfoId,
                                             AdditionalSalaryComponent = a.AdditionalSalaryComponent,
                                             AdditionalComponentId = a.AdditionalComponentId,
                                             AdditionalPercent = a.AdditionalPercent,
                                             AdditionalAmount = a.AdditionalAmount,
                                             CreatedBy = a.CreatedBy,
                                             CreatedOn = a.CreatedOn,
                                             ModifiedBy = a.ModifiedBy,
                                             ModifiedOn = a.ModifiedOn,
                                             ApprovedBy = a.ApprovedBy,
                                             ApprovedOn = a.ApprovedOn,
                                             IsActive = a.IsActive,
                                             IsLocked = a.IsLocked,
                                             IsDefault = a.IsDefault,
                                             IsDeleted = a.IsDeleted,
                                             OwnerId = a.OwnerId,
                                             DeletedBy = a.DeletedBy,
                                             DeletedOn = a.DeletedOn,
                                             BusinessUnitDivisionId = a.BusinessUnitDivisionId,
                                             BusinessUnitDepartementId = a.BusinessUnitDepartementId,
                                             AdditionalComponentTypeName = b.SubComponentName
                                         }).OrderByDescending(x => x.CreatedOn).ToListAsync();

            var allowance = await (from a in context.EmployeeSubcomponentAdditionals
                                   join b in context.AllowanceComponents on a.AdditionalComponentId equals b.Id
                                   where a.AdditionalSalaryComponent == SalaryComponentTypeAdditional.Allowance && !(a.IsDeleted ?? false) && a.AdditionalEmployeeBasicInfoId == employeeId
                                   select new EmployeeSubcomponentAdditional
                                   {
                                       Id = a.Id,
                                       AdditionalEmployeeBasicInfoId = a.AdditionalEmployeeBasicInfoId,
                                       AdditionalSalaryComponent = a.AdditionalSalaryComponent,
                                       AdditionalComponentId = a.AdditionalComponentId,
                                       AdditionalPercent = a.AdditionalPercent,
                                       AdditionalAmount = a.AdditionalAmount,
                                       CreatedBy = a.CreatedBy,
                                       CreatedOn = a.CreatedOn,
                                       ModifiedBy = a.ModifiedBy,
                                       ModifiedOn = a.ModifiedOn,
                                       ApprovedBy = a.ApprovedBy,
                                       ApprovedOn = a.ApprovedOn,
                                       IsActive = a.IsActive,
                                       IsLocked = a.IsLocked,
                                       IsDefault = a.IsDefault,
                                       IsDeleted = a.IsDeleted,
                                       OwnerId = a.OwnerId,
                                       DeletedBy = a.DeletedBy,
                                       DeletedOn = a.DeletedOn,
                                       BusinessUnitDivisionId = a.BusinessUnitDivisionId,
                                       BusinessUnitDepartementId = a.BusinessUnitDepartementId,
                                       AdditionalComponentTypeName = b.SubComponentName
                                   }).OrderByDescending(x => x.CreatedOn).ToListAsync();

            var bpjs = await (from a in context.EmployeeSubcomponentAdditionals
                              join b in context.BpjsComponents on a.AdditionalComponentId equals b.Id
                              where a.AdditionalSalaryComponent == SalaryComponentTypeAdditional.BPJS && b.Type == BPJSType.Additional && !(a.IsDeleted ?? false) && a.AdditionalEmployeeBasicInfoId == employeeId
                              select new EmployeeSubcomponentAdditional
                              {
                                  Id = a.Id,
                                  AdditionalEmployeeBasicInfoId = a.AdditionalEmployeeBasicInfoId,
                                  AdditionalSalaryComponent = a.AdditionalSalaryComponent,
                                  AdditionalComponentId = a.AdditionalComponentId,
                                  AdditionalPercent = a.AdditionalPercent,
                                  AdditionalAmount = a.AdditionalAmount,
                                  CreatedBy = a.CreatedBy,
                                  CreatedOn = a.CreatedOn,
                                  ModifiedBy = a.ModifiedBy,
                                  ModifiedOn = a.ModifiedOn,
                                  ApprovedBy = a.ApprovedBy,
                                  ApprovedOn = a.ApprovedOn,
                                  IsActive = a.IsActive,
                                  IsLocked = a.IsLocked,
                                  IsDefault = a.IsDefault,
                                  IsDeleted = a.IsDeleted,
                                  OwnerId = a.OwnerId,
                                  DeletedBy = a.DeletedBy,
                                  DeletedOn = a.DeletedOn,
                                  BusinessUnitDivisionId = a.BusinessUnitDivisionId,
                                  BusinessUnitDepartementId = a.BusinessUnitDepartementId,
                                  AdditionalComponentTypeName = b.SubComponentName
                              }).OrderByDescending(x => x.CreatedOn).ToListAsync();

            result.AddRange(salaryComponent);
            result.AddRange(allowance);
            result.AddRange(bpjs);

            return result;
        }

        public async Task<List<EmployeeSubcomponentDeduction>> GetDeductionList(string employeeId)
        {
            var result = new List<EmployeeSubcomponentDeduction>();
            var deduction = await (from a in context.EmployeeSubcomponentDeductions
                                   join b in context.DeductionComponents on a.ComponentId equals b.Id
                                   where a.SalaryComponent == SalaryComponentTypeDeduction.Deduction &&
                                   !(a.IsDeleted ?? false) && a.EmployeeBasicInfoId == employeeId
                                   select new EmployeeSubcomponentDeduction
                                   {
                                       Id = a.Id,
                                       EmployeeBasicInfoId = a.EmployeeBasicInfoId,
                                       SalaryComponent = a.SalaryComponent,
                                       ComponentId = a.ComponentId,
                                       Percent = a.Percent,
                                       Amount = a.Amount,
                                       CreatedBy = a.CreatedBy,
                                       CreatedOn = a.CreatedOn,
                                       ModifiedBy = a.ModifiedBy,
                                       ModifiedOn = a.ModifiedOn,
                                       ApprovedBy = a.ApprovedBy,
                                       ApprovedOn = a.ApprovedOn,
                                       IsActive = a.IsActive,
                                       IsLocked = a.IsLocked,
                                       IsDefault = a.IsDefault,
                                       IsDeleted = a.IsDeleted,
                                       OwnerId = a.OwnerId,
                                       DeletedBy = a.DeletedBy,
                                       DeletedOn = a.DeletedOn,
                                       BusinessUnitDivisionId = a.BusinessUnitDivisionId,
                                       BusinessUnitDepartementId = a.BusinessUnitDepartementId,
                                       ComponentTypeName = b.SubComponentName
                                   }).OrderByDescending(x => x.CreatedOn).ToListAsync();

            var bpjs = await (from a in context.EmployeeSubcomponentDeductions
                              join b in context.BpjsComponents on a.ComponentId equals b.Id
                              where a.SalaryComponent == SalaryComponentTypeDeduction.BPJS && b.Type == BPJSType.Deduction &&
                              !(a.IsDeleted ?? false) && a.EmployeeBasicInfoId == employeeId
                              select new EmployeeSubcomponentDeduction
                              {
                                  Id = a.Id,
                                  EmployeeBasicInfoId = a.EmployeeBasicInfoId,
                                  SalaryComponent = a.SalaryComponent,
                                  ComponentId = a.ComponentId,
                                  Percent = a.Percent,
                                  Amount = a.Amount,
                                  CreatedBy = a.CreatedBy,
                                  CreatedOn = a.CreatedOn,
                                  ModifiedBy = a.ModifiedBy,
                                  ModifiedOn = a.ModifiedOn,
                                  ApprovedBy = a.ApprovedBy,
                                  ApprovedOn = a.ApprovedOn,
                                  IsActive = a.IsActive,
                                  IsLocked = a.IsLocked,
                                  IsDefault = a.IsDefault,
                                  IsDeleted = a.IsDeleted,
                                  OwnerId = a.OwnerId,
                                  DeletedBy = a.DeletedBy,
                                  DeletedOn = a.DeletedOn,
                                  BusinessUnitDivisionId = a.BusinessUnitDivisionId,
                                  BusinessUnitDepartementId = a.BusinessUnitDepartementId,
                                  ComponentTypeName = b.SubComponentName
                              }).OrderByDescending(x => x.CreatedOn).ToListAsync();

            result.AddRange(deduction);
            result.AddRange(bpjs);
            return result;
        }
    }
}