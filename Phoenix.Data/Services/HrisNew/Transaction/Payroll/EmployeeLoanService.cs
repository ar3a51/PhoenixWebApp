using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Models;
using Phoenix.Data.RestApi;
using Phoenix.Shared.Core.Contexts;

namespace Phoenix.Data
{
    public interface IEmployeeLoanService : IDataServiceHris<EmployeeLoan>
    {
        Task<List<EmployeeLoanPaid>> GetLoanPaidList(string Id);
    }

    public class EmployeeLoanService : IEmployeeLoanService
    {
        readonly DataContext context;

        /// <summary>
        /// And endpoint to manage EmployeeLoan
        /// </summary>
        /// <param name="context">Database context</param>
        public EmployeeLoanService(DataContext context)
        {
            this.context = context;
        }

        public async Task<int> AddAsync(EmployeeLoan entity)
        {
            entity.Id = Guid.NewGuid().ToString();
            await context.PhoenixAddAsync(entity);

            var interestValue = ((entity.Value ?? 0) + ((((entity.Value ?? 0) * entity.PayrollDeduction)) / 100));
            for (int i = 0; i < entity.PayrollDeduction; i++)
            {
                EmployeeLoanPaid paid = new EmployeeLoanPaid();
                paid.Id = Guid.NewGuid().ToString();
                paid.EmployeeLoanId = entity.Id;
                paid.EmployeeBasicInfoId = entity.EmployeeBasicInfoId;
                paid.DatePaid = Convert.ToDateTime(entity.RepaymentStartDate).AddMonths(i);
                paid.Value = interestValue / entity.PayrollDeduction;
                paid.Status = LoanStatus.NotPaid;
                await context.PhoenixAddAsync(paid);
            }

            return await context.SaveChangesAsync();
        }

        public async Task<int> EditAsync(EmployeeLoan entity)
        {
            context.PhoenixEdit(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> ApproveAsync(EmployeeLoan entity)
        {
            throw new NotImplementedException();
        }

        public async Task<int> DeleteSoftAsync(EmployeeLoan entity)
        {
            context.PhoenixDelete(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<List<EmployeeLoan>> Get()
        {
            var result = await (from a in context.EmployeeLoans
                                join b in context.EmployeeBasicInfos on a.EmployeeBasicInfoId equals b.Id
                                join c in context.LoanCategorys on a.LoanCategoryId equals c.Id
                                where !(a.IsDeleted ?? false)
                                select new EmployeeLoan
                                {
                                    Id = a.Id,
                                    EmployeeBasicInfoId = a.EmployeeBasicInfoId,
                                    LoanCategoryId = a.LoanCategoryId,
                                    DateLoan = a.DateLoan,
                                    Value = a.Value,
                                    PayrollDeduction = a.PayrollDeduction,
                                    RepaymentStartDate = a.RepaymentStartDate,
                                    LoanCategoryName = c.Name,
                                    Remaining = (a.Value ?? 0),
                                    isPaid = context.EmployeeLoanPaids.Where(x => x.EmployeeLoanId == a.Id && x.Status == LoanStatus.Paid).Count() > 0,
                                    CreatedOn = a.CreatedOn,
                                    infoEmployee = new InfoEmployee
                                    {
                                        EmployeeId = b.Id,
                                        EmployeeName = b.NameEmployee,
                                        JobTitle = b.BusinessUnitJobLevelId == null ? "" :
                                                                          (from r in context.BusinessUnitJobLevels
                                                                           join s in context.JobTitles on r.JobTitleId equals s.Id
                                                                           where r.Id == b.BusinessUnitJobLevelId
                                                                           select s.TitleName).FirstOrDefault(),
                                        Grade = b.BusinessUnitJobLevelId == null ? "" :
                                                                          (from r in context.BusinessUnitJobLevels
                                                                           join s in context.JobGrades on r.JobGradeId equals s.Id
                                                                           where r.Id == b.BusinessUnitJobLevelId
                                                                           select s.GradeName).FirstOrDefault(),
                                    }
                                }).OrderByDescending(x => x.CreatedOn).ToListAsync();
            return result;
        }

        public async Task<EmployeeLoan> Get(string Id)
        {
            var result = await (from a in context.EmployeeLoans
                                join b in context.EmployeeBasicInfos on a.EmployeeBasicInfoId equals b.Id
                                join c in context.LoanCategorys on a.LoanCategoryId equals c.Id
                                where !(a.IsDeleted ?? false) && a.Id == Id
                                select new EmployeeLoan
                                {
                                    Id = a.Id,
                                    EmployeeBasicInfoId = a.EmployeeBasicInfoId,
                                    LoanCategoryId = a.LoanCategoryId,
                                    DateLoan = a.DateLoan,
                                    Value = a.Value,
                                    PayrollDeduction = a.PayrollDeduction,
                                    RepaymentStartDate = a.RepaymentStartDate,
                                    CreatedBy = a.CreatedBy,
                                    CreatedOn = a.CreatedOn,
                                    ModifiedBy = a.ModifiedBy,
                                    ModifiedOn = a.ModifiedOn,
                                    ApprovedBy = a.ApprovedBy,
                                    ApprovedOn = a.ApprovedOn,
                                    IsActive = a.IsActive,
                                    IsLocked = a.IsLocked,
                                    IsDefault = a.IsDefault,
                                    IsDeleted = a.IsDeleted,
                                    OwnerId = a.OwnerId,
                                    DeletedBy = a.DeletedBy,
                                    DeletedOn = a.DeletedOn,
                                    BusinessUnitDivisionId = a.BusinessUnitDivisionId,
                                    BusinessUnitDepartementId = a.BusinessUnitDepartementId,
                                    LoanCategoryName = c.Name,
                                    InterestMonth = c.InterestMonth,
                                    InterestValue = (a.Value ?? 0) + ((((a.Value ?? 0) * a.PayrollDeduction)) / 100),
                                    infoEmployee = new InfoEmployee
                                    {
                                        EmployeeId = b.Id,
                                        EmployeeName = b.NameEmployee,
                                        JobTitle = b.BusinessUnitJobLevelId == null ? "" :
                                                                          (from r in context.BusinessUnitJobLevels
                                                                           join s in context.JobTitles on r.JobTitleId equals s.Id
                                                                           where r.Id == b.BusinessUnitJobLevelId
                                                                           select s.TitleName).FirstOrDefault(),
                                        Grade = b.BusinessUnitJobLevelId == null ? "" :
                                                                          (from r in context.BusinessUnitJobLevels
                                                                           join s in context.JobGrades on r.JobGradeId equals s.Id
                                                                           where r.Id == b.BusinessUnitJobLevelId
                                                                           select s.GradeName).FirstOrDefault(),
                                    }
                                }).FirstOrDefaultAsync();
            return result;
        }

        public async Task<List<EmployeeLoanPaid>> GetLoanPaidList(string Id)
        {
            var result = await (from a in context.EmployeeLoans
                                join c in context.EmployeeLoanPaids on a.Id equals c.EmployeeLoanId
                                where !(a.IsDeleted ?? false) && !(c.IsDeleted ?? false) && a.Id == Id
                                select new EmployeeLoanPaid
                                {
                                    Id = c.Id,
                                    EmployeeLoanId = c.EmployeeLoanId,
                                    EmployeeBasicInfoId = c.EmployeeBasicInfoId,
                                    DatePaid = c.DatePaid,
                                    Value = c.Value,
                                    Status = c.Status,
                                    EarlyPaidDate = c.EarlyPaidDate,
                                    CreatedBy = c.CreatedBy,
                                    CreatedOn = c.CreatedOn,
                                    ModifiedBy = c.ModifiedBy,
                                    ModifiedOn = c.ModifiedOn,
                                    ApprovedBy = c.ApprovedBy,
                                    ApprovedOn = c.ApprovedOn,
                                    IsActive = c.IsActive,
                                    IsLocked = c.IsLocked,
                                    IsDefault = c.IsDefault,
                                    IsDeleted = c.IsDeleted,
                                    OwnerId = c.OwnerId,
                                    DeletedBy = c.DeletedBy,
                                    DeletedOn = c.DeletedOn,
                                }).OrderBy(x => x.DatePaid).ToListAsync();
            return result;
        }

    }
}