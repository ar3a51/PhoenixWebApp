using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Models;
using Phoenix.Data.RestApi;
using Phoenix.Shared.Core.Contexts;

namespace Phoenix.Data
{
    public interface IAttendanceService
    {
        Task<int> CheckIn(Attendance entity);
        Task<int> CheckOut(Attendance entity);
        Task<dynamic> GetList();
        Task<dynamic> ReportList();
        Task<Attendance> GetById();
        Task<Attendance> Get(string Id);
        Task<dynamic> GetReportById(string employeeId, int month, int year);
    }

    public class AttendanceService : IAttendanceService
    {
        readonly DataContext context;

        /// <summary>ReportList
        /// And endpoint to manage Attendance
        /// </summary>
        /// <param name="context">Database context</param>
        public AttendanceService(DataContext context)
        {
            this.context = context;
        }

        public async Task<int> CheckIn(Attendance entity)
        {
            return await CheckInOut(entity, true);

        }

        public async Task<int> CheckOut(Attendance entity)
        {
            return await CheckInOut(entity, false);
        }

        private async Task<int> CheckInOut(Attendance entity, bool IsCheckIn)
        {
            var workingHour = await context.MasterWorkingTimes.AsNoTracking().Where(x => x.IsDefault == true).FirstOrDefaultAsync();
            var edit = await context.Attendances.AsNoTracking().Where(x => Convert.ToDateTime(x.DateAttendance).Date == DateTime.Now.Date && x.Type == (IsCheckIn ? "ClockIn" : "ClockOut")).FirstOrDefaultAsync();
            var datecheckin = DateTime.Now;

            //Untuk Checkin Selalu mengambil jam yg pertama kali click
            //Untuk CheckOut Selalu mengambil jam yg terakhir kali click
            if (IsCheckIn && edit != null)
            {
                entity.DateAttendance = edit.DateAttendance;
                entity.TimeAttendance = edit.TimeAttendance;

            }
            else
            {
                entity.DateAttendance = datecheckin.Date;
                entity.TimeAttendance = datecheckin.TimeOfDay;
            }

            if (IsCheckIn)
            {
                entity.Type = "ClockIn";

                if (entity.TimeAttendance > workingHour.TimeIn)
                {
                    entity.Late = DateTime.Parse(((TimeSpan)entity.TimeAttendance).ToString(@"hh\:mm\:ss")).Subtract(DateTime.Parse(((TimeSpan)workingHour.TimeIn).ToString(@"hh\:mm\:ss")));
                }
            }
            else
            {
                entity.Type = "ClockOut";
                if (entity.TimeAttendance > workingHour.TimeOut)
                {
                    entity.OverTime = DateTime.Parse(((TimeSpan)entity.TimeAttendance).ToString(@"hh\:mm\:ss")).Subtract(DateTime.Parse(((TimeSpan)workingHour.TimeOut).ToString(@"hh\:mm\:ss")));
                }
            }

            if (edit != null)
            {
                entity.Id = edit.Id;
                context.PhoenixEdit(entity);
            }
            else
            {
                entity.Id = Guid.NewGuid().ToString();
                await context.PhoenixAddAsync(entity);
            }

            if (context.EmployeeLocations.AsNoTracking().Where(x => x.LocationId == MasterLocationDefault.LocationNava).Count() == 0)
            {
                var employeeLocation = new EmployeeLocation()
                {
                    Id = Guid.NewGuid().ToString(),
                    EmployeeBasicInfoId = entity.EmployeeBasicInfoId,
                    LocationId = MasterLocationDefault.LocationNava
                };
                await context.PhoenixAddAsync(employeeLocation);
            }

            return await context.SaveChangesAsync();
        }

        public async Task<Attendance> Get(string Id) => await context.Attendances.AsNoTracking().Where(x => !(x.IsDeleted ?? false) && x.Id == Id).FirstOrDefaultAsync();

        public async Task<Attendance> GetById()
        {
            var employeeId = context.GetEmployeeId();
            RequestorService request = new RequestorService(context);
            var attendance = await GetAttendance(employeeId);
            attendance.requestEmployee = request.GetEmployeeInfo(attendance.requestEmployee);
            return attendance;
        }

        public async Task<Attendance> GetAttendance(string employeeId)
        {
            var attandanceOut = await context.Attendances.Where(z => !(z.IsDeleted ?? false) && z.Type == "ClockOut" && z.EmployeeBasicInfoId == employeeId && Convert.ToDateTime(z.DateAttendance).Date == DateTime.Now.Date).Select(x => x.TimeAttendance).FirstOrDefaultAsync();
            var attendance = await (from a in context.EmployeeBasicInfos
                                    join c in context.MasterLocations on MasterLocationDefault.LocationNava equals c.Id
                                    join d in context.Attendances on a.Id equals d.EmployeeBasicInfoId
                                    where !(a.IsDeleted ?? false) && a.Id == employeeId && Convert.ToDateTime(d.DateAttendance).Date == DateTime.Now.Date &&
                                    c.Id == MasterLocationDefault.LocationNava && d.Type == "ClockIn"
                                    select new Attendance
                                    {
                                        Id = d.Id,
                                        EmployeeBasicInfoId = a.Id,
                                        Lat = c.MapLat,
                                        Lng = c.MapLong,
                                        LocationName = c.LocationName,
                                        DateAttendance = d.DateAttendance,
                                        TimeAttendance = d.TimeAttendance,
                                        Type = d.Type,
                                        CreatedOn = d.CreatedOn,
                                        ModifiedBy = d.ModifiedBy,
                                        ModifiedOn = d.ModifiedOn,
                                        ApprovedBy = d.ApprovedBy,
                                        ApprovedOn = d.ApprovedOn,
                                        IsActive = d.IsActive,
                                        IsLocked = d.IsLocked,
                                        IsDefault = d.IsDefault,
                                        IsDeleted = d.IsDeleted,
                                        OwnerId = d.OwnerId,
                                        DeletedBy = d.DeletedBy,
                                        DeletedOn = d.DeletedOn,
                                        TimeAttendanceOut = attandanceOut,
                                        requestEmployee = new InfoEmployee
                                        {
                                            EmployeeId = a.Id,
                                            EmployeeName = a.NameEmployee,
                                            JobTitle = a.BusinessUnitJobLevelId == null ? "" :
                                                                          (from r in context.BusinessUnitJobLevels
                                                                           join s in context.JobTitles on r.JobTitleId equals s.Id
                                                                           where r.Id == a.BusinessUnitJobLevelId
                                                                           select s.TitleName).FirstOrDefault(),
                                            Grade = a.BusinessUnitJobLevelId == null ? "" :
                                                                          (from r in context.BusinessUnitJobLevels
                                                                           join s in context.JobGrades on r.JobGradeId equals s.Id
                                                                           where r.Id == a.BusinessUnitJobLevelId
                                                                           select s.GradeName).FirstOrDefault()
                                        }
                                    }).OrderByDescending(x => (x.ModifiedOn ?? x.CreatedOn)).FirstOrDefaultAsync();

            if (attendance == null)
            {
                attendance = await (from a in context.EmployeeBasicInfos
                                    join c in context.MasterLocations on MasterLocationDefault.LocationNava equals c.Id
                                    where !(a.IsDeleted ?? false) && a.Id == employeeId && c.Id == MasterLocationDefault.LocationNava
                                    select new Attendance
                                    {
                                        EmployeeBasicInfoId = a.Id,
                                        Lat = c.MapLat,
                                        Lng = c.MapLong,
                                        LocationName = c.LocationName,
                                        requestEmployee = new InfoEmployee
                                        {
                                            EmployeeId = a.Id,
                                            EmployeeName = a.NameEmployee,
                                            JobTitle = a.BusinessUnitJobLevelId == null ? "" :
                                                                          (from r in context.BusinessUnitJobLevels
                                                                           join s in context.JobTitles on r.JobTitleId equals s.Id
                                                                           where r.Id == a.BusinessUnitJobLevelId
                                                                           select s.TitleName).FirstOrDefault(),
                                            Grade = a.BusinessUnitJobLevelId == null ? "" :
                                                                          (from r in context.BusinessUnitJobLevels
                                                                           join s in context.JobGrades on r.JobGradeId equals s.Id
                                                                           where r.Id == a.BusinessUnitJobLevelId
                                                                           select s.GradeName).FirstOrDefault()
                                        }
                                    }).FirstOrDefaultAsync();
            }
            return attendance;
        }

        public async Task<dynamic> GetList()
        {
            var employeeId = context.GetEmployeeId();
            var clockIn = await ListCheckIn(employeeId);
            var clockOut = await ListCheckOut(employeeId);

            return (from a in clockIn
                    join b in clockOut on new { a.EmployeeBasicInfoId, a.DateAttendance } equals new { b.EmployeeBasicInfoId, b.DateAttendance } into g
                    from c in g.DefaultIfEmpty()
                    select new
                    {
                        EmployeeBasicInfoId = a.EmployeeBasicInfoId,
                        ClockIn = Convert.ToDateTime(a.DateAttendance).Add((TimeSpan)a.TimeAttendance),
                        LocationIn = a.LocationName,
                        Clockout = (c == null) ? (DateTime?)null : Convert.ToDateTime(c.DateAttendance).Add((TimeSpan)c.TimeAttendance),
                        LocationOut = (c == null) ? "" : c.LocationName,
                        Late = a.Late,
                        OverTime = (c == null) ? (TimeSpan?)null : c.OverTime
                    }).ToList();
        }

        public async Task<dynamic> ReportList()
        {
            var groupAttandance = await context.Attendances.Where(x => !(x.IsDeleted ?? false)).GroupBy(x => new { x.EmployeeBasicInfoId, x.DateAttendance }).Select(x => new { x.Key.EmployeeBasicInfoId, x.Key.DateAttendance }).ToListAsync();

            var groupAttandancePerMonth = groupAttandance.GroupBy(x => new
            {
                x.EmployeeBasicInfoId,
                Convert.ToDateTime(x.DateAttendance).Month,
                Convert.ToDateTime(x.DateAttendance).Year,
                MonthName = Convert.ToDateTime(x.DateAttendance).ToString("MMMM")
            }).Select(x => new
            {
                x.Key.EmployeeBasicInfoId,
                x.Key.Month,
                x.Key.Year,
                x.Key.MonthName
            }).ToList();

            var attandance = await (from a in context.EmployeeBasicInfos
                                    join b in groupAttandancePerMonth on a.Id equals b.EmployeeBasicInfoId
                                    where !(a.IsDeleted ?? false)
                                    select new Attendance
                                    {
                                        Id = Guid.NewGuid().ToString(),
                                        EmployeeBasicInfoId = b.EmployeeBasicInfoId,
                                        TotalRecord = groupAttandance.Where(x => x.EmployeeBasicInfoId == b.EmployeeBasicInfoId &&
                                                                                 Convert.ToDateTime(x.DateAttendance).Month == b.Month &&
                                                                                 Convert.ToDateTime(x.DateAttendance).Year == b.Year).Count(),
                                        MonthName = b.MonthName,
                                        Month = b.Month,
                                        Year = b.Year,
                                        requestEmployee = new InfoEmployee
                                        {
                                            EmployeeId = a.Id,
                                            EmployeeName = a.NameEmployee,
                                            JobTitle = a.BusinessUnitJobLevelId == null ? "" :
                                                                            (from r in context.BusinessUnitJobLevels
                                                                             join s in context.JobTitles on r.JobTitleId equals s.Id
                                                                             where r.Id == a.BusinessUnitJobLevelId
                                                                             select s.TitleName).FirstOrDefault(),
                                            Grade = a.BusinessUnitJobLevelId == null ? "" :
                                                                            (from r in context.BusinessUnitJobLevels
                                                                             join s in context.JobGrades on r.JobGradeId equals s.Id
                                                                             where r.Id == a.BusinessUnitJobLevelId
                                                                             select s.GradeName).FirstOrDefault()
                                        }
                                    }).ToListAsync();
            return attandance;
        }

        public async Task<dynamic> GetReportById(string employeeId, int month, int year)
        {
            var workingHour = await context.MasterWorkingTimes.AsNoTracking().Where(x => x.IsDefault == true).FirstOrDefaultAsync();
            var clockIn = await ListCheckIn(employeeId);
            var clockOut = await ListCheckOut(employeeId);

            return (from b in clockIn
                    join a in clockOut on new { b.EmployeeBasicInfoId, b.DateAttendance } equals new { a.EmployeeBasicInfoId, a.DateAttendance } into g
                    from c in g.DefaultIfEmpty()
                    select new
                    {
                        Id = Guid.NewGuid().ToString(),
                        Day = Convert.ToDateTime(b.DateAttendance).ToString("ddd"),
                        Date = b.DateAttendance,
                        WorkingHour = (c == null) ? ((TimeSpan)b.TimeAttendance).ToString(@"hh\:mm") + " - " : ((TimeSpan)b.TimeAttendance).ToString(@"hh\:mm") + " - " + ((TimeSpan)c.TimeAttendance).ToString(@"hh\:mm"),
                        EarlyIn = (workingHour.TimeIn > b.TimeAttendance) ? DateTime.Parse(((TimeSpan)workingHour.TimeIn).ToString(@"hh\:mm\:ss")).Subtract(DateTime.Parse(((TimeSpan)b.TimeAttendance).ToString(@"hh\:mm\:ss"))).ToString(@"hh\:mm") : "",
                        earlyout = (c == null) ? "" : (workingHour.TimeOut > c.TimeAttendance) ? DateTime.Parse(((TimeSpan)workingHour.TimeOut).ToString(@"hh\:mm\:ss")).Subtract(DateTime.Parse(((TimeSpan)c.TimeAttendance).ToString(@"hh\:mm\:ss"))).ToString(@"hh\:mm") : "",
                        LateIn = (b.TimeAttendance > workingHour.TimeIn) ? DateTime.Parse(((TimeSpan)b.TimeAttendance).ToString(@"hh\:mm\:ss")).Subtract(DateTime.Parse(((TimeSpan)workingHour.TimeIn).ToString(@"hh\:mm\:ss"))).ToString(@"hh\:mm") : "",
                        LateOut = (c == null) ? "" : (c.TimeAttendance > workingHour.TimeOut) ? DateTime.Parse(((TimeSpan)c.TimeAttendance).ToString(@"hh\:mm\:ss")).Subtract(DateTime.Parse(((TimeSpan)workingHour.TimeOut).ToString(@"hh\:mm\:ss"))).ToString(@"hh\:mm") : "",
                        TotalHour = (c == null) ? "" : DateTime.Parse(((TimeSpan)c.TimeAttendance).ToString(@"hh\:mm\:ss")).Subtract(DateTime.Parse(((TimeSpan)b.TimeAttendance).ToString(@"hh\:mm\:ss"))).ToString(@"hh\:mm")
                    }).ToList();
        }

        private async Task<List<Attendance>> ListCheckIn(string employeeId)
        {
            var data = await context.Attendances.Where(x => !(x.IsDeleted ?? false) && x.EmployeeBasicInfoId == employeeId && x.Type == "ClockIn")
                .GroupBy(x => new { x.EmployeeBasicInfoId, x.DateAttendance })
                .Select(x => new { x.Key.EmployeeBasicInfoId, x.Key.DateAttendance, TimeAttendance = x.Min(y => y.TimeAttendance) }).ToListAsync();

            var result = (from a in context.Attendances
                          join b in data on new { a.EmployeeBasicInfoId, a.DateAttendance, a.TimeAttendance } equals new { b.EmployeeBasicInfoId, b.DateAttendance, b.TimeAttendance }
                          select a).ToList();
            return result;
        }

        private async Task<List<Attendance>> ListCheckOut(string employeeId)
        {
            var data = await context.Attendances.Where(x => !(x.IsDeleted ?? false) && x.EmployeeBasicInfoId == employeeId && x.Type == "ClockOut")
                .GroupBy(x => new { x.EmployeeBasicInfoId, x.DateAttendance })
                .Select(x => new { x.Key.EmployeeBasicInfoId, x.Key.DateAttendance, TimeAttendance = x.Max(y => y.TimeAttendance) }).ToListAsync();

            var result = (from a in context.Attendances
                          join b in data on new { a.EmployeeBasicInfoId, a.DateAttendance, a.TimeAttendance } equals new { b.EmployeeBasicInfoId, b.DateAttendance, b.TimeAttendance }
                          select a).ToList();
            return result;
        }
    }
}