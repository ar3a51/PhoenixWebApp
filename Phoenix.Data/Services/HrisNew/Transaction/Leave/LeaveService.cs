using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Attributes;
using Phoenix.Data.Models;
using Phoenix.Data.Models.ViewModel;
using Phoenix.Data.RestApi;
using Phoenix.Shared.Core.Contexts;

namespace Phoenix.Data
{
    public interface ILeaveService : IDataServiceHris<Leave>
    {
        Task<List<Leave>> GetApprovalList();
        Task<List<InfoEmployee>> GetAssignList(string id);
        //Task<Leave> GetLeaveBalanceInfoById(string employeeId, DateTime endDate);
    }

    public class LeaveService : ILeaveService
    {
        readonly DataContext context;
        readonly GlobalFunctionApproval globalFcApproval;

        string idTemplate = "";

        /// <summary>
        /// And endpoint to manage Leave
        /// </summary>
        /// <param name="context">Database context</param>
        public LeaveService(DataContext context, GlobalFunctionApproval globalFcApproval)
        {
            this.context = context;
            this.globalFcApproval = globalFcApproval;
        }

        public async Task<int> AddAsync(Leave entity)
        {
            entity.Id = await TransID.GetTransId(context, Code.Leave, DateTime.Today);
            return await globalFcApproval.SubmitApproval<Leave>(false, entity.Id, entity.Status, $"{ApprovalLink.Leave}?Id={entity.Id}&isApprove=true", idTemplate, MenuUnique.Leave, entity);
        }

        public async Task<int> EditAsync(Leave entity)
        {
            return await globalFcApproval.SubmitApproval<Leave>(true, entity.Id, entity.Status, $"{ApprovalLink.Leave}?Id={entity.Id}&isApprove=true", idTemplate, MenuUnique.Leave, entity);
        }

        public async Task<int> DeleteSoftAsync(Leave entity)
        {
            context.PhoenixDelete(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> ApproveAsync(Leave entity)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    context.PhoenixApprove(entity);

                    var vm = new TrUpdateStatusApprovalViewModel()
                    {
                        RefId = entity.Id,
                        StatusApproved = entity.Status ?? 0,
                        Remarks = entity.RemarkRejected
                    };

                    var employeeId = Phoenix.Shared.Core.PrincipalHelpers.ClainJwtPrincipalHelper.GetEmployeeId(context.HttpContext);
                    var isApprove = await globalFcApproval.UpdateStatusApproval(vm, employeeId);

                    var template = context.TrTemplateApprovals.Where(x => x.RefId == entity.Id).FirstOrDefault();

                    if (template != null)
                    {
                        if (template.StatusApproved.GetHashCode() == StatusTransaction.Approved)
                        {
                            var leaveBalance = await context.LeaveBalances.AsNoTracking().Where(x => x.EmployeeBasicInfoId == entity.EmployeeBasicInfoId).ToListAsync();
                            int sisaCuti = 0;
                            foreach (var item in leaveBalance.OrderBy(x => x.ExpiredDate).ThenByDescending(x => x.Type))
                            {
                                if ((entity.TotalDays - sisaCuti) > item.Balance)
                                {
                                    sisaCuti = ((entity.TotalDays ?? 0) - sisaCuti) - (item.Balance ?? 0);
                                    item.Balance = 0;
                                }
                                else

                                {
                                    if (sisaCuti > 0)
                                    {
                                        item.Balance = (item.Balance ?? 0) - sisaCuti;
                                    }
                                    else
                                    {
                                        item.Balance = (item.Balance ?? 0) - (entity.TotalDays ?? 0);
                                    }
                                    break;
                                }
                            }
                            context.PhoenixEditRange(leaveBalance.ToArray());
                        }
                    }

                    if (isApprove)
                    {
                        var save = await context.SaveChangesAsync();
                        transaction.Commit();
                        return save;
                    }
                    else
                    {
                        transaction.Rollback();
                        throw new Exception();
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<List<Leave>> Get()
        {
            var result = await (from a in context.Leaves
                                join b in context.EmployeeBasicInfos on a.EmployeeBasicInfoId equals b.Id
                                join c in context.LeaveRequestTypes on a.LeaveRequestTypeId equals c.Id
                                join d in context.TrTemplateApprovals on a.Id equals d.RefId
                                where !(a.IsDeleted ?? false) && a.EmployeeBasicInfoId == context.GetEmployeeId()
                                select new Leave
                                {
                                    Id = a.Id,
                                    EmployeeBasicInfoId = a.EmployeeBasicInfoId,
                                    StartDate = a.StartDate,
                                    EndDate = a.EndDate,
                                    Reason = a.Reason,
                                    StatusDesc = d.StatusApprovedDescription,
                                    TotalDays = a.TotalDays,
                                    LeaveRequestTypeId = a.LeaveRequestTypeId,
                                    AssistedJob = a.AssistedJob,
                                    OtherLeave = a.OtherLeave,
                                    Reached = a.Reached,
                                    RequestDate = a.RequestDate,
                                    LeaveRequestType = c,
                                    requestEmployee = new InfoEmployee
                                    {
                                        EmployeeId = b.Id,
                                        EmployeeName = b.NameEmployee,
                                        //DateEmployed = b.JoinDate,
                                        //Phone = b.PhoneNumber,
                                        //Email = b.Email,
                                        //DateBirth = b.DateBirth,
                                        //Age = Convert.ToDateTime(b.DateBirth).AddYears(DateTime.Now.Year - Convert.ToDateTime(b.DateBirth).Year) > DateTime.Now ?
                                        //                      (DateTime.Now.Year - Convert.ToDateTime(b.DateBirth).Year) - 1 : DateTime.Now.Year - Convert.ToDateTime(b.DateBirth).Year,
                                        JobTitle = b.BusinessUnitJobLevelId == null ? "" :
                                                                          (from r in context.BusinessUnitJobLevels
                                                                           join s in context.JobTitles on r.JobTitleId equals s.Id
                                                                           where r.Id == b.BusinessUnitJobLevelId
                                                                           select s.TitleName).FirstOrDefault(),
                                        Grade = b.BusinessUnitJobLevelId == null ? "" :
                                                                          (from r in context.BusinessUnitJobLevels
                                                                           join s in context.JobGrades on r.JobGradeId equals s.Id
                                                                           where r.Id == b.BusinessUnitJobLevelId
                                                                           select s.GradeName).FirstOrDefault(),
                                        //EmployementStatus = context.EmployementStatuss.Where(x => x.Id == b.EmployementStatusId).Select(x => x.EmploymentStatusName).FirstOrDefault()
                                    }
                                }).OrderByDescending(x => x.CreatedOn).ToListAsync();


            var result2 = await (from a in context.Leaves
                                 join b in context.EmployeeBasicInfos on a.EmployeeBasicInfoId equals b.Id
                                 join c in context.LeaveRequestTypes on a.LeaveRequestTypeId equals c.Id
                                 where !(a.IsDeleted ?? false) && a.EmployeeBasicInfoId == context.GetEmployeeId() && !context.TrTemplateApprovals.Any(x => x.RefId == a.Id)
                                 select new Leave
                                 {
                                     Id = a.Id,
                                     EmployeeBasicInfoId = a.EmployeeBasicInfoId,
                                     StartDate = a.StartDate,
                                     EndDate = a.EndDate,
                                     Reason = a.Reason,
                                     StatusDesc = StatusTransaction.StatusName(StatusTransaction.Draft),
                                     TotalDays = a.TotalDays,
                                     LeaveRequestTypeId = a.LeaveRequestTypeId,
                                     AssistedJob = a.AssistedJob,
                                     OtherLeave = a.OtherLeave,
                                     Reached = a.Reached,
                                     RequestDate = a.RequestDate,
                                     LeaveRequestType = c,
                                     requestEmployee = new InfoEmployee
                                     {
                                         EmployeeId = b.Id,
                                         EmployeeName = b.NameEmployee,
                                         //DateEmployed = b.JoinDate,
                                         //Phone = b.PhoneNumber,
                                         //Email = b.Email,
                                         //DateBirth = b.DateBirth,
                                         //Age = Convert.ToDateTime(b.DateBirth).AddYears(DateTime.Now.Year - Convert.ToDateTime(b.DateBirth).Year) > DateTime.Now ?
                                         //                      (DateTime.Now.Year - Convert.ToDateTime(b.DateBirth).Year) - 1 : DateTime.Now.Year - Convert.ToDateTime(b.DateBirth).Year,
                                         JobTitle = b.BusinessUnitJobLevelId == null ? "" :
                                                                          (from r in context.BusinessUnitJobLevels
                                                                           join s in context.JobTitles on r.JobTitleId equals s.Id
                                                                           where r.Id == b.BusinessUnitJobLevelId
                                                                           select s.TitleName).FirstOrDefault(),
                                         Grade = b.BusinessUnitJobLevelId == null ? "" :
                                                                          (from r in context.BusinessUnitJobLevels
                                                                           join s in context.JobGrades on r.JobGradeId equals s.Id
                                                                           where r.Id == b.BusinessUnitJobLevelId
                                                                           select s.GradeName).FirstOrDefault(),
                                         //EmployementStatus = context.EmployementStatuss.Where(x => x.Id == b.EmployementStatusId).Select(x => x.EmploymentStatusName).FirstOrDefault()
                                     }
                                 }).OrderByDescending(x => x.CreatedOn).ToListAsync();


            if (result == null) result = new List<Leave>();
            result.AddRange(result2);
            return result;
        }

        public async Task<List<Leave>> GetApprovalList()
        {
            var result = await (from a in context.Leaves
                                join b in context.EmployeeBasicInfos on a.EmployeeBasicInfoId equals b.Id
                                join c in context.LeaveRequestTypes on a.LeaveRequestTypeId equals c.Id
                                join e in context.TrTemplateApprovals on a.Id equals e.RefId
                                join d in context.TrUserApprovals on e.Id equals d.TrTempApprovalId
                                where !(a.IsDeleted ?? false) && d.StatusApproved == Models.Um.StatusApproved.CurrentApproval && d.EmployeeBasicInfoId == context.GetEmployeeId()
                                select new Leave
                                {
                                    Id = a.Id,
                                    EmployeeBasicInfoId = a.EmployeeBasicInfoId,
                                    StartDate = a.StartDate,
                                    EndDate = a.EndDate,
                                    Reason = a.Reason,
                                    StatusDesc = e.StatusApprovedDescription,
                                    TotalDays = a.TotalDays,
                                    LeaveRequestTypeId = a.LeaveRequestTypeId,
                                    AssistedJob = a.AssistedJob,
                                    OtherLeave = a.OtherLeave,
                                    Reached = a.Reached,
                                    RequestDate = a.RequestDate,
                                    LeaveRequestType = c,
                                    requestEmployee = new InfoEmployee
                                    {
                                        EmployeeId = b.Id,
                                        EmployeeName = b.NameEmployee,
                                        //DateEmployed = b.JoinDate,
                                        //Phone = b.PhoneNumber,
                                        //Email = b.Email,
                                        //DateBirth = b.DateBirth,
                                        //Age = Convert.ToDateTime(b.DateBirth).AddYears(DateTime.Now.Year - Convert.ToDateTime(b.DateBirth).Year) > DateTime.Now ?
                                        //                      (DateTime.Now.Year - Convert.ToDateTime(b.DateBirth).Year) - 1 : DateTime.Now.Year - Convert.ToDateTime(b.DateBirth).Year,
                                        JobTitle = b.BusinessUnitJobLevelId == null ? "" :
                                                                          (from r in context.BusinessUnitJobLevels
                                                                           join s in context.JobTitles on r.JobTitleId equals s.Id
                                                                           where r.Id == b.BusinessUnitJobLevelId
                                                                           select s.TitleName).FirstOrDefault(),
                                        Grade = b.BusinessUnitJobLevelId == null ? "" :
                                                                          (from r in context.BusinessUnitJobLevels
                                                                           join s in context.JobGrades on r.JobGradeId equals s.Id
                                                                           where r.Id == b.BusinessUnitJobLevelId
                                                                           select s.GradeName).FirstOrDefault(),
                                        //EmployementStatus = context.EmployementStatuss.Where(x => x.Id == b.EmployementStatusId).Select(x => x.EmploymentStatusName).FirstOrDefault()
                                    }
                                }).OrderByDescending(x => x.CreatedOn).ToListAsync();
            return result;
        }

        public async Task<Leave> Get(string Id)
        {
            var result = await (from a in context.Leaves
                                join b in context.EmployeeBasicInfos on a.EmployeeBasicInfoId equals b.Id
                                join c in context.LeaveRequestTypes on a.LeaveRequestTypeId equals c.Id
                                join d in context.TrTemplateApprovals on a.Id equals d.RefId
                                where !(a.IsDeleted ?? false) && a.Id == Id
                                select new Leave
                                {
                                    Id = a.Id,
                                    EmployeeBasicInfoId = a.EmployeeBasicInfoId,
                                    StartDate = a.StartDate,
                                    EndDate = a.EndDate,
                                    Reason = a.Reason,
                                    Status = d.StatusApproved.GetHashCode(),
                                    StatusDesc = d.StatusApprovedDescription,
                                    TotalDays = a.TotalDays,
                                    LeaveRequestTypeId = a.LeaveRequestTypeId,
                                    AssistedJob = a.AssistedJob,
                                    OtherLeave = a.OtherLeave,
                                    Reached = a.Reached,
                                    RequestDate = a.RequestDate,
                                    CreatedBy = a.CreatedBy,
                                    CreatedOn = a.CreatedOn,
                                    ModifiedBy = a.ModifiedBy,
                                    ModifiedOn = a.ModifiedOn,
                                    ApprovedBy = a.ApprovedBy,
                                    ApprovedOn = a.ApprovedOn,
                                    IsActive = a.IsActive,
                                    IsLocked = a.IsLocked,
                                    IsDefault = a.IsDefault,
                                    IsDeleted = a.IsDeleted,
                                    OwnerId = a.OwnerId,
                                    DeletedBy = a.DeletedBy,
                                    DeletedOn = a.DeletedOn,
                                    BusinessUnitDivisionId = a.BusinessUnitDivisionId,
                                    BusinessUnitDepartementId = a.BusinessUnitDepartementId,
                                    //Annual = 0,
                                    //Compensatory = 0,
                                    requestEmployee = new InfoEmployee
                                    {
                                        EmployeeId = b.Id,
                                        EmployeeName = b.NameEmployee,
                                        //DateEmployed = b.JoinDate,
                                        //Phone = b.PhoneNumber,
                                        //Email = b.Email,
                                        //DateBirth = b.DateBirth,
                                        //Age = Convert.ToDateTime(b.DateBirth).AddYears(DateTime.Now.Year - Convert.ToDateTime(b.DateBirth).Year) > DateTime.Now ?
                                        //                      (DateTime.Now.Year - Convert.ToDateTime(b.DateBirth).Year) - 1 : DateTime.Now.Year - Convert.ToDateTime(b.DateBirth).Year,
                                        JobTitle = b.BusinessUnitJobLevelId == null ? "" :
                                                                          (from r in context.BusinessUnitJobLevels
                                                                           join s in context.JobTitles on r.JobTitleId equals s.Id
                                                                           where r.Id == b.BusinessUnitJobLevelId
                                                                           select s.TitleName).FirstOrDefault(),
                                        Grade = b.BusinessUnitJobLevelId == null ? "" :
                                                                          (from r in context.BusinessUnitJobLevels
                                                                           join s in context.JobGrades on r.JobGradeId equals s.Id
                                                                           where r.Id == b.BusinessUnitJobLevelId
                                                                           select s.GradeName).FirstOrDefault(),
                                        //EmployementStatus = context.EmployementStatuss.Where(x => x.Id == b.EmployementStatusId).Select(x => x.EmploymentStatusName).FirstOrDefault()
                                    }
                                }).FirstOrDefaultAsync();


            if (result == null)
            {
                result = await (from a in context.Leaves
                                join b in context.EmployeeBasicInfos on a.EmployeeBasicInfoId equals b.Id
                                join c in context.LeaveRequestTypes on a.LeaveRequestTypeId equals c.Id
                                where !(a.IsDeleted ?? false) && a.Id == Id
                                select new Leave
                                {
                                    Id = a.Id,
                                    EmployeeBasicInfoId = a.EmployeeBasicInfoId,
                                    StartDate = a.StartDate,
                                    EndDate = a.EndDate,
                                    Reason = a.Reason,
                                    Status = StatusTransaction.Draft,//DRAFT
                                    StatusDesc = StatusTransaction.StatusName(StatusTransaction.Draft),
                                    TotalDays = a.TotalDays,
                                    LeaveRequestTypeId = a.LeaveRequestTypeId,
                                    AssistedJob = a.AssistedJob,
                                    OtherLeave = a.OtherLeave,
                                    Reached = a.Reached,
                                    RequestDate = a.RequestDate,
                                    CreatedBy = a.CreatedBy,
                                    CreatedOn = a.CreatedOn,
                                    ModifiedBy = a.ModifiedBy,
                                    ModifiedOn = a.ModifiedOn,
                                    ApprovedBy = a.ApprovedBy,
                                    ApprovedOn = a.ApprovedOn,
                                    IsActive = a.IsActive,
                                    IsLocked = a.IsLocked,
                                    IsDefault = a.IsDefault,
                                    IsDeleted = a.IsDeleted,
                                    OwnerId = a.OwnerId,
                                    DeletedBy = a.DeletedBy,
                                    DeletedOn = a.DeletedOn,
                                    BusinessUnitDivisionId = a.BusinessUnitDivisionId,
                                    BusinessUnitDepartementId = a.BusinessUnitDepartementId,
                                    //Annual = 0,
                                    //Compensatory = 0,
                                    requestEmployee = new InfoEmployee
                                    {
                                        EmployeeId = b.Id,
                                        EmployeeName = b.NameEmployee,
                                        //DateEmployed = b.JoinDate,
                                        //Phone = b.PhoneNumber,
                                        //Email = b.Email,
                                        //DateBirth = b.DateBirth,
                                        //Age = Convert.ToDateTime(b.DateBirth).AddYears(DateTime.Now.Year - Convert.ToDateTime(b.DateBirth).Year) > DateTime.Now ?
                                        //                      (DateTime.Now.Year - Convert.ToDateTime(b.DateBirth).Year) - 1 : DateTime.Now.Year - Convert.ToDateTime(b.DateBirth).Year,
                                        JobTitle = b.BusinessUnitJobLevelId == null ? "" :
                                                                          (from r in context.BusinessUnitJobLevels
                                                                           join s in context.JobTitles on r.JobTitleId equals s.Id
                                                                           where r.Id == b.BusinessUnitJobLevelId
                                                                           select s.TitleName).FirstOrDefault(),
                                        Grade = b.BusinessUnitJobLevelId == null ? "" :
                                                                          (from r in context.BusinessUnitJobLevels
                                                                           join s in context.JobGrades on r.JobGradeId equals s.Id
                                                                           where r.Id == b.BusinessUnitJobLevelId
                                                                           select s.GradeName).FirstOrDefault(),
                                        //EmployementStatus = context.EmployementStatuss.Where(x => x.Id == b.EmployementStatusId).Select(x => x.EmploymentStatusName).FirstOrDefault()
                                    }
                                }).FirstOrDefaultAsync();
            }
            return result;
        }

        public async Task<List<InfoEmployee>> GetAssignList(string id)
        {
            var result = await (from a in context.Leaves
                                join b in context.EmployeeBasicInfos on a.AssistedJob equals b.Id
                                where a.Id == id
                                select new InfoEmployee
                                {
                                    EmployeeId = b.Id,
                                    EmployeeName = b.NameEmployee,
                                    //DateEmployed = b.JoinDate,
                                    //Phone = b.PhoneNumber,
                                    //Email = b.Email,
                                    //DateBirth = b.DateBirth,
                                    //Age = Convert.ToDateTime(b.DateBirth).AddYears(DateTime.Now.Year - Convert.ToDateTime(b.DateBirth).Year) > DateTime.Now ?
                                    //                          (DateTime.Now.Year - Convert.ToDateTime(b.DateBirth).Year) - 1 : DateTime.Now.Year - Convert.ToDateTime(b.DateBirth).Year,
                                    JobTitle = b.BusinessUnitJobLevelId == null ? "" :
                                                                          (from r in context.BusinessUnitJobLevels
                                                                           join s in context.JobTitles on r.JobTitleId equals s.Id
                                                                           where r.Id == b.BusinessUnitJobLevelId
                                                                           select s.TitleName).FirstOrDefault(),
                                    Grade = b.BusinessUnitJobLevelId == null ? "" :
                                                                          (from r in context.BusinessUnitJobLevels
                                                                           join s in context.JobGrades on r.JobGradeId equals s.Id
                                                                           where r.Id == b.BusinessUnitJobLevelId
                                                                           select s.GradeName).FirstOrDefault(),
                                    //EmployementStatus = context.EmployementStatuss.Where(x => x.Id == b.EmployementStatusId).Select(x => x.EmploymentStatusName).FirstOrDefault(),
                                    Division = b.BusinessUnitJobLevelId == null ? "" :
                                                                        (from r in context.BusinessUnitJobLevels
                                                                         join s in context.BusinessUnits on r.BusinessUnitId equals s.Id
                                                                         join t in context.BusinessUnitTypes on s.BusinessUnitTypeId equals t.Id
                                                                         where t.BusinessUnitLevel == BusinessUnitLevelCode.Division && r.Id == b.BusinessUnitJobLevelId
                                                                         select s.UnitName).FirstOrDefault()
                                }).ToListAsync();
            return result;
        }
    }
}