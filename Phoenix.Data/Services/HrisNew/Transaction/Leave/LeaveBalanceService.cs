using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Models;
using Phoenix.Data.RestApi;
using Phoenix.Shared.Core.Contexts;

namespace Phoenix.Data
{
    public interface ILeaveBalanceService
    {
        Task<List<LeaveBalanceView>> Get();
        Task<int> Generate(int Type);
        //Task<dynamic> GetById(string employeeId);
        Task<dynamic> GetEmployeeLeaveBalaceInfoList(string employeeId, DateTime? endDate);
        //Task<dynamic> GetLeaveHistoryList(string employeeId);
    }

    public class LeaveBalanceService : ILeaveBalanceService
    {
        readonly DataContext context;

        /// <summary>
        /// And endpoint to manage LeaveBalance
        /// </summary>
        /// <param name="context">Database context</param>
        public LeaveBalanceService(DataContext context)
        {
            this.context = context;
        }

        public async Task<List<LeaveBalanceView>> Get() => await context.ExecuteStoredProcedure<LeaveBalanceView>("uspGetLeaveBalanceList");

        public async Task<int> Generate(int Type)
        {
            var paramType = new SqlParameter("@Type", Type);
            var paramEmployeeId = new SqlParameter("@RequestBy", context.GetEmployeeId());

            var result = await context.Database.ExecuteSqlCommandAsync("EXEC uspGenerateLeaveBalance @Type,  @RequestBy", paramType, paramEmployeeId);
            return result;
        }

        public async Task<dynamic> GetEmployeeLeaveBalaceInfoList(string employeeId, DateTime? endDate)
        {
            return await context.LeaveBalances
              .Where(x => x.EmployeeBasicInfoId == employeeId && !(x.IsDeleted ?? false) && x.ExpiredDate >= (endDate ?? DateTime.Now))
              .Select(x => new
              {
                  x.Id,
                  x.EmployeeBasicInfoId,
                  LeaveType = LeaveType.LeaveTypeName(x.Type),
                  x.Balance,
                  x.ExpiredDate
              }).AsNoTracking().ToListAsync();
        }

        //public async Task<dynamic> GetById(string employeeId)
        //{
        //    return await context.LeaveBalances
        //      .Where(x => x.EmployeeBasicInfoId == employeeId && !(x.IsDeleted ?? false))
        //      .Select(x => new
        //      {
        //          x.Id,
        //          LeaveType = LeaveType.LeaveTypeName(x.Type),
        //          x.Balance,
        //          Default = x.Total
        //      }).FirstOrDefaultAsync();
        //}


        //public async Task<dynamic> GetLeaveHistoryList(string employeeId)
        //{
        //    return await (from a in context.LeaveTransactions
        //                  join b in context.Leaves on a.LeaveId equals b.Id
        //                  where b.EmployeeBasicInfoId == employeeId
        //                  select new
        //                  {
        //                      a.Id,
        //                      Date = a.TakenDate,
        //                      Reason = "",
        //                      Days = a.Total
        //                  }).AsNoTracking().ToListAsync();
        //}
    }
}