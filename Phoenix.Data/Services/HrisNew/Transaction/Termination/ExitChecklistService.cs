using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Phoenix.ApiExtension.Extensions;
using Phoenix.Data.Models;
using Phoenix.Data.RestApi;
using Phoenix.Shared.Core.Contexts;

namespace Phoenix.Data
{
    public interface IExitChecklistService : IDataServiceHris<Termination>
    {
        Task<List<ExitChecklist>> GetJobHandoverList(string terminationId, string employeeBasicInfoId);
        Task<List<ExitChecklist>> GetOfficeManagementList(string terminationId, string employeeBasicInfoId);
        Task<List<ExitChecklist>> GetInformationTechnologyList(string terminationId, string employeeBasicInfoId);
        Task<List<ExitChecklist>> GetFinanceList(string terminationId, string employeeBasicInfoId);
        Task<List<ExitChecklist>> GetHumanResourceList(string terminationId, string employeeBasicInfoId);
        Task<List<ExitChecklist>> GetOtherList(string terminationId, string employeeBasicInfoId);
        Task<List<ExitChecklist>> GetFinalHandoverList(string terminationId, string employeeBasicInfoId);
    }

    public class ExitChecklistService : IExitChecklistService
    {
        readonly DataContext context;
        string[] terminationType = new string[] {TerminationType.Resignation,
            TerminationType.EndOfContract, TerminationType.EndOfProbation,
            TerminationType.Death, TerminationType.ProlongedIllness,
            TerminationType.Sanction, TerminationType.Retired};

        /// <summary>
        /// And endpoint to manage Termination
        /// </summary>
        /// <param name="context">Database context</param>
        public ExitChecklistService(DataContext context)
        {
            this.context = context;
        }

        public Task<int> AddAsync(Termination entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> ApproveAsync(Termination entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteSoftAsync(Termination entity)
        {
            throw new NotImplementedException();
        }

        public async Task<int> EditAsync(Termination entity)
        {
            List<ExitChecklist> list = new List<ExitChecklist>();
            if (entity.JobHandover != null) list.AddRange(entity.JobHandover);
            if (entity.OfficeManagement != null) list.AddRange(entity.OfficeManagement);
            if (entity.InformationTechnology != null) list.AddRange(entity.InformationTechnology);
            if (entity.Finance != null) list.AddRange(entity.Finance);
            if (entity.HumanResource != null) list.AddRange(entity.HumanResource);
            if (entity.Other != null) list.AddRange(entity.Other);
            if (entity.FinalHandover != null) list.AddRange(entity.FinalHandover);

            var exitChecklistCount = list.Where(x => x.TerminationId == entity.Id && !(x.IsDeleted ?? false)).Count();
            var doneCount = list.Where(x => x.TerminationId == entity.Id && !(x.IsDeleted ?? false) && x.IsDone == true).Count();
            if (exitChecklistCount == 0)
            {
                throw new Exception("nothing is processed in the exit checklist");
            }

            if (doneCount == exitChecklistCount)
            {
                entity.StatusChecklist = TerminationExitChecklistStatus.Done;
                entity.IsChecklist = true;
            }
            else if (doneCount > 0)
            {
                entity.StatusChecklist = TerminationExitChecklistStatus.OnProgress;
            }
            else
            {
                entity.StatusChecklist = TerminationExitChecklistStatus.Unfinished;
            }

            if (context.ExitChecklists.Where(x => x.TerminationId == entity.Id && !(x.IsDeleted ?? false)).Count() > 0)
            {
                context.PhoenixEditRange(list.ToArray());
            }
            else
            {
                context.PhoenixAddRange(list.ToArray());
            }

            context.PhoenixEdit(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<List<Termination>> Get()
        {
            TerminationService termination = new TerminationService(context);
            var result = await termination.GetTerminationHC(terminationType, false);
            return result;
        }

        public async Task<Termination> Get(string terminationId)
        {
            TerminationService termination = new TerminationService(context);
            var data = await termination.GetTerminationById(null, terminationType, terminationId);
            var file = await context.Filemasters.Where(x => x.Id == data.ExitInterviewFiles).FirstOrDefaultAsync();
            if (file != null) data.ExitInterviewFilesName = file.Name;
            return data;
        }

        public async Task<List<ExitChecklist>> GetJobHandoverList(string terminationId, string employeeBasicInfoId)
        {
            return await GetExitChecklist(terminationId, employeeBasicInfoId, ChecklistCategory.JobHandover);
        }

        public async Task<List<ExitChecklist>> GetOfficeManagementList(string terminationId, string employeeBasicInfoId)
        {
            return await GetExitChecklist(terminationId, employeeBasicInfoId, ChecklistCategory.OfficeManagement);
        }

        public async Task<List<ExitChecklist>> GetInformationTechnologyList(string terminationId, string employeeBasicInfoId)
        {
            return await GetExitChecklist(terminationId, employeeBasicInfoId, ChecklistCategory.InformationTechnology);
        }

        public async Task<List<ExitChecklist>> GetFinanceList(string terminationId, string employeeBasicInfoId)
        {
            return await GetExitChecklist(terminationId, employeeBasicInfoId, ChecklistCategory.Finance);
        }

        public async Task<List<ExitChecklist>> GetHumanResourceList(string terminationId, string employeeBasicInfoId)
        {
            return await GetExitChecklist(terminationId, employeeBasicInfoId, ChecklistCategory.HumanResouce);
        }

        public async Task<List<ExitChecklist>> GetOtherList(string terminationId, string employeeBasicInfoId)
        {
            return await GetExitChecklist(terminationId, employeeBasicInfoId, ChecklistCategory.Others);
        }

        public async Task<List<ExitChecklist>> GetFinalHandoverList(string terminationId, string employeeBasicInfoId)
        {
            return await GetExitChecklist(terminationId, employeeBasicInfoId, ChecklistCategory.FinalHandover);
        }

        private async Task<List<ExitChecklist>> GetExitChecklist(string terminationId, string employeeId, string employeeChecklistCategoryId)
        {
            var result = await (from b in context.EmployeeChecklists
                                join c in context.BusinessUnits on b.BusinessUnitPicId equals c.Id
                                join d in context.ExitChecklists on b.Id equals d.EmployeeChecklistId
                                where d.TerminationId == terminationId && b.EmployeeBasicInfoId == employeeId &&
                                b.EmployeeChecklistCategoryId == employeeChecklistCategoryId
                                select new ExitChecklist
                                {
                                    Id = d.Id,
                                    TerminationId = d.TerminationId,
                                    EmployeeChecklistId = d.EmployeeChecklistId,
                                    Notes = d.Notes,
                                    IsDone = d.IsDone,
                                    EndDate = d.EndDate,
                                    CreatedBy = d.CreatedBy,
                                    CreatedOn = d.CreatedOn,
                                    ModifiedBy = d.ModifiedBy,
                                    ModifiedOn = d.ModifiedOn,
                                    ApprovedBy = d.ApprovedBy,
                                    ApprovedOn = d.ApprovedOn,
                                    IsActive = d.IsActive,
                                    IsLocked = d.IsLocked,
                                    IsDefault = d.IsDefault,
                                    IsDeleted = d.IsDeleted,
                                    OwnerId = d.OwnerId,
                                    DeletedBy = d.DeletedBy,
                                    DeletedOn = d.DeletedOn,
                                    Checklistitem = b.Checklistitem,
                                    InCharge = c.UnitName
                                }).ToListAsync();

            if (result.Count() == 0)
            {
                result = await (from b in context.EmployeeChecklists
                                join c in context.BusinessUnits on b.BusinessUnitPicId equals c.Id
                                where b.EmployeeBasicInfoId == employeeId &&
                                b.EmployeeChecklistCategoryId == employeeChecklistCategoryId
                                select new ExitChecklist
                                {
                                    Id = Guid.NewGuid().ToString(),
                                    TerminationId = terminationId,
                                    EmployeeChecklistId = b.Id,
                                    Checklistitem = b.Checklistitem,
                                    InCharge = c.UnitName
                                }).ToListAsync();
            }
            return result;
        }
    }
}