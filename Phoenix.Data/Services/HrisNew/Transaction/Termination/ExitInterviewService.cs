using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Phoenix.ApiExtension.Extensions;
using Phoenix.Data.Models;
using Phoenix.Data.RestApi;
using Phoenix.Shared.Core.Contexts;

namespace Phoenix.Data
{
    public interface IExitInterviewService : IDataServiceHris<Termination>
    {
    }

    public class ExitInterviewService : IExitInterviewService
    {
        readonly DataContext context;
        string[] terminationType = new string[] {TerminationType.Resignation,
            TerminationType.EndOfContract, TerminationType.EndOfProbation,
            TerminationType.Death, TerminationType.ProlongedIllness,
            TerminationType.Sanction, TerminationType.Retired};

        /// <summary>
        /// And endpoint to manage Termination
        /// </summary>
        /// <param name="context">Database context</param>
        public ExitInterviewService(DataContext context)
        {
            this.context = context;
        }

        public Task<int> AddAsync(Termination entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> ApproveAsync(Termination entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteSoftAsync(Termination entity)
        {
            throw new NotImplementedException();
        }

        public async Task<int> EditAsync(Termination entity)
        {
            context.PhoenixEdit(entity);
            var exit = context.ExitInterviews.AsNoTracking().Where(x => x.TerminationId == entity.Id).ToList();
            if (exit.Count > 0)
            {
                context.ExitInterviews.RemoveRange(exit);
            }
            context.PhoenixAddRange(entity.ExitInterviews.ToArray());
            return await context.SaveChangesAsync();
        }

        public async Task<List<Termination>> Get()
        {
            TerminationService termination = new TerminationService(context);
            var result = await termination.GetTerminationHC(terminationType, false);
            return result;
        }

        public async Task<Termination> Get(string terminationId)
        {
            TerminationService termination = new TerminationService(context);
            var data = await termination.GetTerminationById(null, terminationType, terminationId);
            data.ExitInterviews = await GetExitInterviewItemsList(terminationId);
            data.CategoryExitInterviews = await context.CategoryExitInterviews.Where(x => !(x.IsDeleted ?? false)).ToListAsync();
            return data;
        }

        private async Task<List<ExitInterview>> GetExitInterviewItemsList(string terminationId)
        {
            var result = await (from a in context.ExitInterviewItems
                                join b in context.ExitInterviews on a.Id equals b.ExitInterviewItemId
                                where b.TerminationId == terminationId
                                select new ExitInterview
                                {
                                    Id = b.Id,
                                    TerminationId = b.TerminationId,
                                    ExitInterviewItemId = b.ExitInterviewItemId,
                                    CategoryExitInterviewId = b.CategoryExitInterviewId,
                                    Result = b.Result,
                                    TextItem = a.TextItem,
                                    Type = a.Type,
                                    No = a.No,
                                    CreatedBy = b.CreatedBy,
                                    CreatedOn = b.CreatedOn,
                                    ModifiedBy = b.ModifiedBy,
                                    ModifiedOn = b.ModifiedOn,
                                    ApprovedBy = b.ApprovedBy,
                                    ApprovedOn = b.ApprovedOn,
                                    IsActive = b.IsActive,
                                    IsLocked = b.IsLocked,
                                    IsDefault = b.IsDefault,
                                    IsDeleted = b.IsDeleted,
                                    OwnerId = b.OwnerId,
                                    DeletedBy = b.DeletedBy,
                                    DeletedOn = b.DeletedOn
                                }).ToListAsync();

            if (result.Count() == 0)
            {
                result = await context.ExitInterviewItems.Where(x => !(x.IsDeleted ?? false)).Select(x => new ExitInterview
                {
                    TerminationId = terminationId,
                    ExitInterviewItemId = x.Id,
                    CategoryExitInterviewId = x.CategoryExitInterviewId,
                    TextItem = x.TextItem,
                    Type = x.Type,
                    No = x.No
                }).ToListAsync();
            }
            return result;
        }
    }
}