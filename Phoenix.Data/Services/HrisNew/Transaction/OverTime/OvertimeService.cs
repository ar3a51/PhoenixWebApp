using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Attributes;
using Phoenix.Data.Models;
using Phoenix.Data.RestApi;
using Phoenix.Data.Services.Um;
using Phoenix.Shared.Core.Contexts;

namespace Phoenix.Data
{
    public interface IOvertimeService : IDataServiceHris<Overtime>
    {
        Task<List<Overtime>> GetApprovalList();
        //bool IsRequest();
    }

    public class OvertimeService : IOvertimeService
    {
        readonly DataContext context;
        readonly GlobalFunctionApproval globalFcApproval;
        private readonly IManageUserAppService userApp;

        string idTemplate = "";

        /// <summary>
        /// And endpoint to manage Overtime
        /// </summary>
        /// <param name="context">Database context</param>
        public OvertimeService(DataContext context, GlobalFunctionApproval globalFcApproval, IManageUserAppService userApp)
        {
            this.context = context;
            this.globalFcApproval = globalFcApproval;
            this.userApp = userApp;
        }

        public async Task<int> AddAsync(Overtime entity)
        {
            var id = await TransID.GetTransId(context, Code.Overtime, DateTime.Today);
            entity.Id = id;
            entity.RequestCode = id;
            return await globalFcApproval.SubmitApproval<Overtime>(false, entity.Id, entity.Status, $"{ApprovalLink.Overtime}?Id={entity.Id}&isApprove=true", idTemplate, MenuUnique.Overtime, entity);
        }

        public async Task<int> EditAsync(Overtime entity)
        {
            if (string.IsNullOrEmpty(entity.RequestCode))
            {
                entity.RequestCode = (context.Overtimes.Where(x => x.Id == entity.Id).FirstOrDefault() ?? new Overtime()).RequestCode;
            }
            return await globalFcApproval.SubmitApproval<Overtime>(true, entity.Id, entity.Status, $"{ApprovalLink.Overtime}?Id={entity.Id}&isApprove=true", idTemplate, MenuUnique.Overtime, entity);
        }

        public async Task<int> ApproveAsync(Overtime entity)
        {
            return await globalFcApproval.ProcessApproval<Overtime>(entity.Id, entity.Status, entity.RemarkRejected, entity);
        }

        public async Task<int> DeleteSoftAsync(Overtime entity)
        {
            context.PhoenixDelete(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<List<Overtime>> Get()
        {
            var group = await userApp.GetByUserLogin();
            var HCAdmin = group.Where(x => x.GroupId == GroupAcess.HCAdmin).Count() > 0;
            var employeeId = context.GetEmployeeId();

            var result = await (from a in context.Overtimes
                                join w in context.EmployeeBasicInfos on a.EmployeeBasicInfoId equals w.Id
                                join c in context.TrTemplateApprovals on a.Id equals c.RefId
                                where !(a.IsDeleted ?? false) && (HCAdmin ? true : a.CreatedBy == employeeId)
                                select new Overtime
                                {
                                    Id = a.Id,
                                    OvertimeDate = a.OvertimeDate,
                                    HourOvertime = a.HourOvertime,
                                    StatusDesc = c.StatusApprovedDescription,
                                    RequestDate = a.RequestDate,
                                    CreatedOn = a.CreatedOn,
                                    requestEmployee = new InfoEmployee
                                    {
                                        EmployeeId = w.Id,
                                        EmployeeName = w.NameEmployee,
                                        //DateEmployed = w.JoinDate,
                                        //Phone = w.PhoneNumber,
                                        //Email = w.Email,
                                        //DateBirth = w.DateBirth,
                                        JobTitle = w.BusinessUnitJobLevelId == null ? "" :
                                                               (from r in context.BusinessUnitJobLevels
                                                                join s in context.JobTitles on r.JobTitleId equals s.Id
                                                                where r.Id == w.BusinessUnitJobLevelId
                                                                select s.TitleName).FirstOrDefault(),
                                        //Grade = w.BusinessUnitJobLevelId == null ? "" :
                                        //                       (from r in context.BusinessUnitJobLevels
                                        //                        join s in context.JobGrades on r.JobGradeId equals s.Id
                                        //                        where r.Id == w.BusinessUnitJobLevelId
                                        //                        select s.GradeName).FirstOrDefault()
                                    }
                                }).OrderByDescending(x => x.CreatedOn).ToListAsync();

            var result2 = await (from a in context.Overtimes
                                 join w in context.EmployeeBasicInfos on a.EmployeeBasicInfoId equals w.Id
                                 where !(a.IsDeleted ?? false) && !context.TrTemplateApprovals.Any(x => x.RefId == a.Id)
                                 && (HCAdmin ? true : a.CreatedBy == employeeId)
                                 select new Overtime
                                 {
                                     Id = a.Id,
                                     OvertimeDate = a.OvertimeDate,
                                     HourOvertime = a.HourOvertime,
                                     StatusDesc = StatusTransaction.StatusName(StatusTransaction.Draft),
                                     RequestDate = a.RequestDate,
                                     CreatedOn = a.CreatedOn,
                                     requestEmployee = new InfoEmployee
                                     {
                                         EmployeeId = w.Id,
                                         EmployeeName = w.NameEmployee,
                                         //DateEmployed = w.JoinDate,
                                         //Phone = w.PhoneNumber,
                                         //Email = w.Email,
                                         //DateBirth = w.DateBirth,
                                         JobTitle = w.BusinessUnitJobLevelId == null ? "" :
                                                                (from r in context.BusinessUnitJobLevels
                                                                 join s in context.JobTitles on r.JobTitleId equals s.Id
                                                                 where r.Id == w.BusinessUnitJobLevelId
                                                                 select s.TitleName).FirstOrDefault(),
                                         //Grade = w.BusinessUnitJobLevelId == null ? "" :
                                         //                       (from r in context.BusinessUnitJobLevels
                                         //                        join s in context.JobGrades on r.JobGradeId equals s.Id
                                         //                        where r.Id == w.BusinessUnitJobLevelId
                                         //                        select s.GradeName).FirstOrDefault()
                                     }
                                 }).OrderByDescending(x => x.CreatedOn).ToListAsync();

            if (result == null) result = new List<Overtime>();
            result.AddRange(result2);
            return result.OrderByDescending(x => x.CreatedOn).ToList();
        }

        public async Task<List<Overtime>> GetApprovalList()
        {
            var result = await (from a in context.Overtimes
                                join w in context.EmployeeBasicInfos on a.EmployeeBasicInfoId equals w.Id
                                join c in context.TrTemplateApprovals on a.Id equals c.RefId
                                join d in context.TrUserApprovals on c.Id equals d.TrTempApprovalId
                                where !(a.IsDeleted ?? false) && d.StatusApproved == Models.Um.StatusApproved.CurrentApproval && d.EmployeeBasicInfoId == context.GetEmployeeId()
                                select new Overtime
                                {
                                    Id = a.Id,
                                    OvertimeDate = a.OvertimeDate,
                                    HourOvertime = a.HourOvertime,
                                    StatusDesc = c.StatusApprovedDescription,
                                    RequestDate = a.RequestDate,
                                    CreatedOn = a.CreatedOn,
                                    requestEmployee = new InfoEmployee
                                    {
                                        EmployeeId = w.Id,
                                        EmployeeName = w.NameEmployee,
                                        //DateEmployed = w.JoinDate,
                                        //Phone = w.PhoneNumber,
                                        //Email = w.Email,
                                        //DateBirth = w.DateBirth,
                                        JobTitle = w.BusinessUnitJobLevelId == null ? "" :
                                                               (from r in context.BusinessUnitJobLevels
                                                                join s in context.JobTitles on r.JobTitleId equals s.Id
                                                                where r.Id == w.BusinessUnitJobLevelId
                                                                select s.TitleName).FirstOrDefault(),
                                        //Grade = w.BusinessUnitJobLevelId == null ? "" :
                                        //                       (from r in context.BusinessUnitJobLevels
                                        //                        join s in context.JobGrades on r.JobGradeId equals s.Id
                                        //                        where r.Id == w.BusinessUnitJobLevelId
                                        //                        select s.GradeName).FirstOrDefault()
                                    }
                                }).OrderByDescending(x => x.CreatedOn).ToListAsync();
            return result;
        }

        public async Task<Overtime> Get(string Id)
        {
            var result = await (from a in context.Overtimes
                                join w in context.EmployeeBasicInfos on a.EmployeeBasicInfoId equals w.Id
                                join d in context.TrTemplateApprovals on a.Id equals d.RefId
                                where !(a.IsDeleted ?? false) && a.Id == Id
                                select new Overtime
                                {
                                    Id = a.Id,
                                    EmployeeBasicInfoId = a.EmployeeBasicInfoId,
                                    RequestCode = a.RequestCode,
                                    RequestDate = a.RequestDate,
                                    OvertimeDate = a.OvertimeDate,
                                    HourOvertime = a.HourOvertime,
                                    ReasonOvertime = a.ReasonOvertime,
                                    Status = d.StatusApproved.GetHashCode(),
                                    StatusDesc = d.StatusApprovedDescription,
                                    CreatedBy = a.CreatedBy,
                                    CreatedOn = a.CreatedOn,
                                    ModifiedBy = a.ModifiedBy,
                                    ModifiedOn = a.ModifiedOn,
                                    ApprovedBy = a.ApprovedBy,
                                    ApprovedOn = a.ApprovedOn,
                                    IsActive = a.IsActive,
                                    IsLocked = a.IsLocked,
                                    IsDefault = a.IsDefault,
                                    IsDeleted = a.IsDeleted,
                                    OwnerId = a.OwnerId,
                                    DeletedBy = a.DeletedBy,
                                    DeletedOn = a.DeletedOn,
                                    requestEmployee = new InfoEmployee
                                    {
                                        EmployeeId = w.Id,
                                        EmployeeName = w.NameEmployee,
                                        //DateEmployed = w.JoinDate,
                                        //Phone = w.PhoneNumber,
                                        //Email = w.Email,
                                        //DateBirth = w.DateBirth,
                                        JobTitle = w.BusinessUnitJobLevelId == null ? "" :
                                                          (from r in context.BusinessUnitJobLevels
                                                           join s in context.JobTitles on r.JobTitleId equals s.Id
                                                           where r.Id == w.BusinessUnitJobLevelId
                                                           select s.TitleName).FirstOrDefault(),
                                        Grade = w.BusinessUnitJobLevelId == null ? "" :
                                                               (from r in context.BusinessUnitJobLevels
                                                                join s in context.JobGrades on r.JobGradeId equals s.Id
                                                                where r.Id == w.BusinessUnitJobLevelId
                                                                select s.GradeName).FirstOrDefault()
                                    }
                                }).FirstOrDefaultAsync();


            if (result == null)
            {
                result = await (from a in context.Overtimes
                                join w in context.EmployeeBasicInfos on a.EmployeeBasicInfoId equals w.Id
                                where !(a.IsDeleted ?? false) && a.Id == Id && !context.TrTemplateApprovals.Any(x => x.RefId == a.Id)
                                select new Overtime
                                {
                                    Id = a.Id,
                                    EmployeeBasicInfoId = a.EmployeeBasicInfoId,
                                    RequestCode = a.RequestCode,
                                    RequestDate = a.RequestDate,
                                    OvertimeDate = a.OvertimeDate,
                                    HourOvertime = a.HourOvertime,
                                    ReasonOvertime = a.ReasonOvertime,
                                    Status = StatusTransaction.Draft,//DRAFT
                                    StatusDesc = StatusTransaction.StatusName(StatusTransaction.Draft),
                                    CreatedBy = a.CreatedBy,
                                    CreatedOn = a.CreatedOn,
                                    ModifiedBy = a.ModifiedBy,
                                    ModifiedOn = a.ModifiedOn,
                                    ApprovedBy = a.ApprovedBy,
                                    ApprovedOn = a.ApprovedOn,
                                    IsActive = a.IsActive,
                                    IsLocked = a.IsLocked,
                                    IsDefault = a.IsDefault,
                                    IsDeleted = a.IsDeleted,
                                    OwnerId = a.OwnerId,
                                    DeletedBy = a.DeletedBy,
                                    DeletedOn = a.DeletedOn,
                                    requestEmployee = new InfoEmployee
                                    {
                                        EmployeeId = w.Id,
                                        EmployeeName = w.NameEmployee,
                                        //DateEmployed = w.JoinDate,
                                        //Phone = w.PhoneNumber,
                                        //Email = w.Email,
                                        //DateBirth = w.DateBirth,
                                        JobTitle = w.BusinessUnitJobLevelId == null ? "" :
                                                          (from r in context.BusinessUnitJobLevels
                                                           join s in context.JobTitles on r.JobTitleId equals s.Id
                                                           where r.Id == w.BusinessUnitJobLevelId
                                                           select s.TitleName).FirstOrDefault(),
                                        Grade = w.BusinessUnitJobLevelId == null ? "" :
                                                               (from r in context.BusinessUnitJobLevels
                                                                join s in context.JobGrades on r.JobGradeId equals s.Id
                                                                where r.Id == w.BusinessUnitJobLevelId
                                                                select s.GradeName).FirstOrDefault()
                                    }
                                }).FirstOrDefaultAsync();
            }

            return result;
        }
    }
}