using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Phoenix.ApiExtension.Extensions;
using Phoenix.Data.Models;
using Phoenix.Data.RestApi;
using Phoenix.Shared.Core.Contexts;

namespace Phoenix.Data
{
    public interface IRequestorService
    {
        Task<InfoEmployee> GetByUserLogin();
        Task<InfoEmployee> GetEmployeeinfoById(string employeeId);
        Task<InfoEmployee> GetEmployeeinfoByName(string employeeName);
    }

    public class RequestorService : IRequestorService
    {
        readonly DataContext context;

        /// <summary>
        /// And endpoint to manage Termination
        /// </summary>
        /// <param name="context">Database context</param>
        public RequestorService(DataContext context)
        {
            this.context = context;
        }

        public async Task<InfoEmployee> GetByUserLogin()
        {
            var employeeId = context.GetEmployeeId();
            var requestor = (from b in context.EmployeeBasicInfos
                             where b.Id == employeeId
                             select new InfoEmployee
                             {
                                 EmployeeId = b.Id,
                                 EmployeeName = b.NameEmployee,
                                 DateEmployed = b.JoinDate,
                                 Phone = b.PhoneNumber,
                                 Email = b.Email
                             }).FirstOrDefault();

            if (requestor != null) requestor = GetEmployeeInfo(requestor);
            return requestor;
        }

        public async Task<InfoEmployee> GetEmployeeinfoById(string employeeId)
        {
            var requestor = (from b in context.EmployeeBasicInfos
                             where b.Id == employeeId
                             select new InfoEmployee
                             {
                                 EmployeeId = b.Id,
                                 EmployeeName = b.NameEmployee,
                                 DateEmployed = b.JoinDate,
                                 Phone = b.PhoneNumber,
                                 Email = b.Email
                             }).FirstOrDefault();

            if (requestor != null)
            {
                requestor = GetEmployeeInfo(requestor);
            }

            return requestor;
        }

        public async Task<InfoEmployee> GetEmployeeinfoByName(string employeeName)
        {
            var requestor = (from b in context.EmployeeBasicInfos
                             where b.NameEmployee.Trim().Replace(" ", "").ToLower() == employeeName.Trim().Replace(" ", "").ToLower()
                             select new InfoEmployee
                             {
                                 EmployeeId = b.Id,
                                 EmployeeName = b.NameEmployee,
                                 DateEmployed = b.JoinDate,
                                 Phone = b.PhoneNumber,
                                 Email = b.Email
                             }).FirstOrDefault();

            if (requestor != null)
            {
                requestor = GetEmployeeInfo(requestor);
            }

            return requestor;

        }

        public InfoEmployee GetEmployeeInfo(InfoEmployee employee)
        {
            var basicInfo = context.EmployeeBasicInfos.Where(x => x.Id == employee.EmployeeId).FirstOrDefault();
            int years = DateTime.Now.Year - Convert.ToDateTime(basicInfo.DateBirth).Year;
            DateTime birthdayThisYear = Convert.ToDateTime(basicInfo.DateBirth).AddYears(years);

            employee.DateBirth = basicInfo.DateBirth;
            employee.Age = birthdayThisYear > DateTime.Now ? years - 1 : years;

            if (basicInfo.BusinessUnitJobLevelId != null)
            {
                var businessUnit = context.BusinessUnitJobLevels.Where(x => x.Id == basicInfo.BusinessUnitJobLevelId).FirstOrDefault();
                if (businessUnit != null)
                {
                    employee.JobTitle = context.JobTitles.Where(x => x.Id == businessUnit.JobTitleId).Select(x => x.TitleName).FirstOrDefault();
                    employee.JobTitleId = businessUnit.JobTitleId;
                    employee.Grade = context.JobGrades.Where(x => x.Id == businessUnit.JobGradeId).Select(x => x.GradeName).FirstOrDefault();
                    employee.GradeId = businessUnit.JobGradeId;
                    employee.Division = context.BusinessUnits.Where(x => x.Id == businessUnit.BusinessUnitDivisionId).Select(x => x.UnitName).FirstOrDefault();
                    employee.BusinessUnitId = businessUnit.BusinessUnitId; //context.BusinessUnits.Where(x => x.Id == businessUnit.BusinessUnitDivisionId).Select(x => x.Id).FirstOrDefault();
                }
            }

            var contract = (from r in context.EmployeeContracts
                            join s in context.Filemasters on r.FilemasterId equals s.Id
                            where r.EmployeeBasicInfoId == employee.EmployeeId
                            select new { r.FilemasterId, s.Name }).FirstOrDefault();

            if (contract != null)
            {
                employee.EmployeeContractDocument = contract.FilemasterId;
                employee.EmployeeContractDocumentName = contract.Name;
            }

            employee.MarriedStatusName = context.MarriedStatuss.Where(x => x.Id == basicInfo.MarriedStatusId).Select(x => x.MarriedName).FirstOrDefault();
            employee.EmployementStatusId = basicInfo.EmployementStatusId;
            employee.EmployementStatus = context.EmployementStatuss.Where(x => x.Id == basicInfo.EmployementStatusId).Select(x => x.EmploymentStatusName).FirstOrDefault();
            employee.EmployeeJobDescription = "";
            employee.EmployeeJobDescriptionName = "";

            return employee;
        }


    }
}