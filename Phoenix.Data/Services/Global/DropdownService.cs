﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Phoenix.Data.Models;
using Phoenix.Data.Models.Enum;
using Phoenix.Data.Services.Um;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Data
{
    public interface IDropdownService
    {
        #region Global
        Task<dynamic> GetUom();
        Task<dynamic> GetJobGrades();
        Task<dynamic> GetJobTitles();
        Task<dynamic> GetJobLevels();
        Task<dynamic> GetSubGroups();
        Task<dynamic> GetDivision();
        Task<dynamic> GetDivisionHris();
        Task<dynamic> GetDivisionFinance();
        Task<dynamic> GetLegalEntity();
        Task<dynamic> GetDivisions(string subGroupId);
        Task<dynamic> GetDepartments(string divisionId);
        Task<dynamic> GetLocations();
        Task<dynamic> GetFamily(string employeeId);
        Task<dynamic> GetItemType();
        Task<dynamic> GetDepartments();
        //Task<dynamic> GetMasterDepartments();
        Task<dynamic> GetPaymentType();
        Task<dynamic> GetItemCode(string ItemTypeId);
        Task<dynamic> GetItemInventoryAndFixedAsset(string ItemTypeId);
        Task<dynamic> GetGroups();
        Task<dynamic> GetSubGroupsHris(string GroupId);
        Task<dynamic> GetDivisionsHris(string subGroupId);
        Task<dynamic> GetDepartmentsHris(string divisionId);
        #endregion

        #region HRIS
        Task<dynamic> GetMasterDivisionSelfservice();
        Task<dynamic> GetEmployeeBasicInfo();
        Task<dynamic> GetEmployeeBasicInfoByName(string name);
        Task<dynamic> GetTerminationEmployeeBasicInfoByName(string name, string terminationType);
        Task<dynamic> GetEmployeeBasicInfoByHead(string name);
        Task<dynamic> GetEmployeeResignationType();
        Task<dynamic> GetEmployeeTerminationType();
        Task<dynamic> GetEmployeeDeathType();
        Task<dynamic> FaultCategoryList();
        Task<dynamic> ServiceNameList();
        Task<dynamic> CategoryExitInterviewList();
        Task<dynamic> LeaveRequestTypeList();
        Task<dynamic> LoanCategoryList();
        Task<dynamic> SalaryComponentList();
        Task<dynamic> AllowanceComponentList(string gradeId);
        Task<dynamic> DeductionComponentList();
        Task<dynamic> BpjsComponentAdditionalList();
        Task<dynamic> BpjsComponentDeductionList();
        Task<dynamic> InsuranceCompanyList();
        Task<dynamic> PdrCategoryList();
        Task<dynamic> PdrGradeList();
        Task<dynamic> LegalCategoryList();
        Task<dynamic> GetDDLEmployeeLegalDoc(string category, string legalCategoryId, string text);
        Task<dynamic> GetDDLLegalDoc(string category, string CounterpartId);
        Task<dynamic> GetDDLCorporateObjective();
        Task<dynamic> GetDDLCorporateObjectiveName(string bscCategory);
        Task<dynamic> GetDDLObjectiveSetting();
        Task<dynamic> GetDDLEmployeePayrollGroup();
        Task<dynamic> GetDDLPTKPSetting();
        Task<dynamic> GetDDLTrainingSetting();
        Task<dynamic> GetDDLTrainingCategory();
        Task<dynamic> GetDDLTrainingSubject();
        Task<dynamic> GetDDLTrainingType();
        #endregion

        #region Media
        Task<dynamic> GetMediaType();
        Task<dynamic> GetMediaPlanRequestType();
        Task<dynamic> GetMediaOrderStatus();
        Task<dynamic> GetMediaPlanStatus();
        Task<dynamic> GetProgramCategory();
        Task<dynamic> GetProgramPosition();
        Task<dynamic> GetProgramType();
        Task<dynamic> GetCascadeFromJob(string jobid, string caseData);
        Task<dynamic> GetBrandASF();
        #endregion

        #region Finance
        #region Master Data
        Task<dynamic> OrganizationList();
        Task<dynamic> GetAffiliation();
        Task<dynamic> ChartofaccountReportList();
        Task<dynamic> GetCOACodeByNameLv1();
        Task<dynamic> GetCOACodeByNameLv2(string CodeRec);
        Task<dynamic> GetCOACodeByNameLv3(string CodeRec);
        Task<dynamic> GetCOACodeByNameLv4(string CodeRec);
        Task<dynamic> GetCOACodeByNameLv5(string CodeRec);
        Task<dynamic> CountryList();
        Task<dynamic> ProvincesList(string countryId);
        Task<dynamic> CityList(string ProvinceId);
        Task<dynamic> GetClientlist();
        Task<dynamic> GetVendorList();
        Task<dynamic> GetMasterConditionProcurement();
        Task<dynamic> CostSharingAllocationList();
        Task<dynamic> GetFixedAssetMovementStatus();
        Task<dynamic> GetAllocationRatio();
        #endregion
        Task<dynamic> GetCompanys();
        Task<dynamic> GetClients(string id);
        Task<dynamic> GetVendors(string id);
        Task<dynamic> GetRFQVendors(string rfqId, string vendorId);
        Task<dynamic> GetBrands();
        Task<dynamic> GetMediaPlanNo();
        Task<dynamic> GetCurrencies();
        Task<dynamic> GetTAPDDL();
        Task<dynamic> GetPurchaseOrderByClientId(string clietnId, string invoiceId);
        Task<dynamic> GetPurchaseOrderByVendorId(string vendorId);
        Task<dynamic> GetInvoiceVendorByPoId(string poId);
        Task<dynamic> GetChartOfAccount(string code_rec_1, string level);
        Task<dynamic> GetCOAByNameLv5();
        Task<dynamic> GetChartOfAccountLv5();
        Task<dynamic> GetChartOfAccountLv5Like(string search);
        Task<dynamic> GetMainServiceCategory();
        Task<dynamic> GetPurchaseRequestNumber(string purchaseId);
        Task<dynamic> GetJobDetailInRFQTask();
        Task<dynamic> GetRFQOpenPO();
        Task<dynamic> GetSpecialRequestPOType();
        Task<dynamic> GetBrands(string company_id);
        Task<dynamic> GetInventoryGoodsReceipt(string poId);
        Task<dynamic> GetServiceReceipt(string poId);
        Task<dynamic> GetGoodRequest();
        Task<dynamic> GetCoaInvoice();
        Task<dynamic> GetCoaInvoiceTaxPayable();
        Task<dynamic> GetBanks();
        Task<dynamic> GetCoaInvoiceClient();
        Task<dynamic> GetVendorsForInvRecevied(string id);
        Task<dynamic> GetClientInv();
        Task<dynamic> GetBankReceivedType();
        Task<dynamic> GetPCA();
        Task<dynamic> GetPCE();
        Task<dynamic> GetFiscalYear();
        Task<dynamic> GetFiscalYearByLegalId(string legalId);
        Task<dynamic> GetFiscalPeriod(string Id);
        Task<dynamic> GetMasterLocationProcurement();
        Task<dynamic> GetBankVendor();
        Task<dynamic> GetFixedAsset();
        Task<dynamic> GetMasterDepreciation();
        Task<dynamic> GetInventory();
        Task<dynamic> GetOrderReceipt(string id);
        Task<dynamic> GetPettyCash();
        #endregion

        #region PM
        Task<dynamic> GetJobNotExistsInPCAandJobId(string Status, string JobId);
        Task<dynamic> GetJobNotExistsInPCEandJobId(string Status, string JobId);
        Task<dynamic> GetMotherPca();
        Task<dynamic> GetRateCard();
        Task<dynamic> GetTaskRateCard(string rateCardId);
        Task<dynamic> GetSubTaskRateCard(string rateCardId, string taskRateCardName);
        Task<dynamic> GetTypeOfExpense();
        Task<dynamic> GetMainserviceCategory();
        Task<dynamic> GetMainserviceCategoryPCA();
        Task<dynamic> GetShareservices();
        Task<dynamic> GetShareservices(string MainserviceCategoryId);
        Task<dynamic> GetJobId();
        Task<dynamic> GetJobByPCEApproved();
        Task<dynamic> GetPCEReference(string pceId);
        Task<dynamic> MasterOutletList(string clientId);
        Task<dynamic> GetPurchaceOrderByClientIdInGoodReceipt(string clientId);
        #endregion
    }
    public class DropdownService : IDropdownService
    {
        readonly DataContext context;
        private readonly IManageUserAppService userApp;

        /// <summary>
        /// And endpoint to manage MasterDivisionSelfservice
        /// </summary>
        /// <param name="context">Database context</param>
        public DropdownService(DataContext context, IManageUserAppService userApp)
        {
            this.context = context;
            this.userApp = userApp;
        }

        #region GLOBAL

        public async Task<dynamic> GetUom() => await context.Uom.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.unit_symbol, Value = x.Id }).ToListAsync();
        public async Task<dynamic> GetJobGrades() => await context.JobGrades.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.GradeName, Value = x.Id }).OrderBy(x => x.Text).ToListAsync();
        public async Task<dynamic> GetJobTitles() => await context.JobTitles.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.TitleName, Value = x.Id }).ToListAsync();
        public async Task<dynamic> GetJobLevels() => await context.JobLevels.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.JobName, Value = x.Id }).ToListAsync();

        public async Task<dynamic> GetSubGroups()
        {
            var result = await (from x in context.BusinessUnits
                                join y in context.BusinessUnitTypes on x.BusinessUnitTypeId equals y.Id
                                where y.BusinessUnitLevel == BusinessUnitLevelCode.BusinessUnitSubgroup && !(x.IsDeleted ?? false) && !(x.Isfinance ?? false)
                                select new
                                {
                                    Text = x.UnitName,
                                    Value = x.Id
                                }).ToListAsync();
            return result;
        }

        public async Task<dynamic> GetDivision()
        {
            var result = await (from x in context.BusinessUnits
                                join y in context.BusinessUnitTypes on x.BusinessUnitTypeId equals y.Id
                                where y.BusinessUnitLevel == BusinessUnitLevelCode.Division && !(x.IsDeleted ?? false)
                                select new
                                {
                                    Text = x.UnitName,
                                    Value = x.Id
                                }).ToListAsync();
            return result;
        }

        public async Task<dynamic> GetDivisionHris()
        {
            var result = await (from x in context.BusinessUnits
                                join y in context.BusinessUnitTypes on x.BusinessUnitTypeId equals y.Id
                                where y.BusinessUnitLevel == BusinessUnitLevelCode.Division && !(x.IsDeleted ?? false) && (x.Ishris ?? false)
                                select new
                                {
                                    Text = x.UnitName,
                                    Value = x.Id
                                }).ToListAsync();
            return result;
        }

        public async Task<dynamic> GetDivisionFinance()
        {
            var result = await context.BusinessUnits.Where(x => !(x.IsDeleted ?? false) && (x.Isfinance ?? false))
                                .Select(x => new
                                {
                                    Text = x.UnitName,
                                    Value = x.Id
                                }).ToListAsync();
            return result;
        }

        public async Task<dynamic> GetDivisions(string subGroupId)
        {
            var result = await (from x in context.BusinessUnits
                                join y in context.BusinessUnitTypes on x.BusinessUnitTypeId equals y.Id
                                where y.BusinessUnitLevel == BusinessUnitLevelCode.Division && !(x.IsDeleted ?? false) && x.ParentUnit == subGroupId && !(x.Isfinance ?? false)
                                select new
                                {
                                    Text = x.UnitName,
                                    Value = x.Id
                                }).ToListAsync();
            return result;
        }

        public async Task<dynamic> GetDepartments()
        {
            var result = await (from x in context.BusinessUnits
                                join y in context.BusinessUnitTypes on x.BusinessUnitTypeId equals y.Id
                                where y.BusinessUnitLevel == BusinessUnitLevelCode.Department && !(x.IsDeleted ?? false) && !(x.Isfinance ?? false)
                                select new
                                {
                                    Text = x.UnitName,
                                    Value = x.Id
                                }).ToListAsync();
            return result;
        }

        //public async Task<dynamic> GetMasterDepartments()
        //{
        //    var result = await context.MasterDepartments.Where(x => !(x.IsDeleted ?? false)).
        //        Select(x => new
        //        {
        //            Text = x.Name,
        //            Value = x.Id
        //        }).ToListAsync();
        //    return result;
        //}

        public async Task<dynamic> GetDepartments(string divisionId)
        {
            var result = await (from x in context.BusinessUnits
                                join y in context.BusinessUnitTypes on x.BusinessUnitTypeId equals y.Id
                                where y.BusinessUnitLevel == BusinessUnitLevelCode.Department && x.ParentUnit == divisionId && !(x.IsDeleted ?? false)
                                select new
                                {
                                    Text = x.UnitName,
                                    Value = x.Id
                                }).ToListAsync();
            return result;
        }

        public async Task<dynamic> GetLocations() => await context.Locations.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.LocationName, Value = x.Id }).ToListAsync();
        public async Task<dynamic> GetFamily(string employeeId) => await context.Familys.Where(x => x.EmployeeBasicInfoId == employeeId).Select(x => new { Text = x.Name, Value = x.Id }).ToListAsync();

        public async Task<dynamic> GetItemType()
        {
            return await context.ItemType.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.TypeName, Value = x.Id }).ToListAsync();
        }
        public async Task<dynamic> GetLegalEntity()
        {
            var result = await (from x in context.LegalEntity
                                where !(x.IsDeleted ?? false)
                                select new
                                {
                                    Text = x.LegalEntityName,
                                    Value = x.Id
                                }).ToListAsync();
            return result;
        }
        public async Task<dynamic> GetPaymentType()
        {
            var result = await (from x in context.PaymentType
                                where !(x.IsDeleted ?? false)
                                select new
                                {
                                    Text = x.TypeName,
                                    Value = x.Id
                                }).ToListAsync();
            return result;
        }
        public async Task<dynamic> GetItemCode(string ItemTypeId)
        {
            return await context.Item.Where(x => !(x.IsDeleted ?? false) && x.ItemTypeId == ItemTypeId).Select(x => new { Text = x.ItemCode, Value = x.Id }).ToListAsync();
        }
        public async Task<dynamic> GetItemInventoryAndFixedAsset(string itemTypeId)
        {
            if (itemTypeId == ItemTypeId.Inventory)
            {
                return await context.Inventory.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Value = x.Id, Text = x.SKUCode + " - " + x.InventoryName }).ToListAsync();
            }
            else if (itemTypeId == ItemTypeId.FixedAsset)
            {
                return await context.MasterBudgetCodes.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Value = x.Id, Text = x.BudgetCode + " - " + x.BudgetName }).ToListAsync();
            }
            return null;
        }
        public async Task<dynamic> GetGroups()
        {
            var bulevelid = await context.BusinessUnitTypes.Where(x => x.BusinessUnitLevel == BusinessUnitLevelCode.HoldingGroup).FirstOrDefaultAsync();
            return await context.BusinessUnits.Where(x => !(x.IsDeleted ?? false) && x.BusinessUnitTypeId == bulevelid.Id).Select(x => new { Text = x.UnitName, Value = x.Id }).ToListAsync();
        }
        public async Task<dynamic> GetSubGroupsHris(string GroupId)
        {
            var bulevelid = await context.BusinessUnitTypes.Where(x => x.BusinessUnitLevel == BusinessUnitLevelCode.BusinessUnitSubgroup).FirstOrDefaultAsync();
            return await context.BusinessUnits.Where(x => !(x.IsDeleted ?? false) && x.BusinessUnitTypeId == bulevelid.Id && x.ParentUnit == GroupId).Select(x => new { Text = x.UnitName, Value = x.Id }).ToListAsync();
        }
        public async Task<dynamic> GetDivisionsHris(string subGroupId)
        {
            var bulevelid = await context.BusinessUnitTypes.Where(x => x.BusinessUnitLevel == BusinessUnitLevelCode.Division).FirstOrDefaultAsync();
            return await context.BusinessUnits.Where(x => !(x.IsDeleted ?? false) && x.BusinessUnitTypeId == bulevelid.Id && x.ParentUnit == subGroupId).Select(x => new { Text = x.UnitName, Value = x.Id }).ToListAsync();
        }
        public async Task<dynamic> GetDepartmentsHris(string divisionId)
        {
            var bulevelid = await context.BusinessUnitTypes.Where(x => x.BusinessUnitLevel == BusinessUnitLevelCode.Department).FirstOrDefaultAsync();
            return await context.BusinessUnits.Where(x => !(x.IsDeleted ?? false) && x.BusinessUnitTypeId == bulevelid.Id && x.ParentUnit == divisionId).Select(x => new { Text = x.UnitName, Value = x.Id }).ToListAsync();
        }
        #endregion

        #region HRIS
        public async Task<dynamic> GetMasterDivisionSelfservice() => await context.MasterDivisionSelfservices.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.BusinessUnitName, Value = x.Id }).ToListAsync();

        public async Task<dynamic> GetEmployeeBasicInfo() => await context.EmployeeBasicInfos.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.NameEmployee, Value = x.Id }).ToListAsync();


        public async Task<dynamic> GetEmployeeBasicInfoByName(string name)
        {
            var result = await context.EmployeeBasicInfos
                                .Where(x => !(x.IsDeleted ?? false) && x.NameEmployee.Trim().Replace(" ", "").ToLower().Contains(name.Trim().Replace(" ", "").ToLower()))
                                .Select(x => new { Text = x.NameEmployee, Value = x.Id }).ToListAsync();
            return result;
        }

        public async Task<dynamic> GetTerminationEmployeeBasicInfoByName(string name, string terminationType)
        {
            var employeeId = context.GetEmployeeId();
            var status = new int[] { StatusTransaction.Draft, StatusTransaction.Rejected };
            var terminationTypes = new string[] { TerminationType.ProlongedIllness, TerminationType.Sanction };
            var isContract = terminationType == TerminationType.EndOfProbation || terminationType == TerminationType.EndOfContract;

            var employeeStatus = "";
            if (terminationType == TerminationType.EndOfProbation) employeeStatus = EmployeeStatus.Probation;
            else if (terminationType == TerminationType.EndOfContract) employeeStatus = EmployeeStatus.Contract;

            var group = await userApp.GetByUserLogin();
            var HCAdmin = group.Where(x => x.GroupId == GroupAcess.HCAdmin).Count() > 0;
            var terminationEmployeeId = context.Terminations.Where(x => !status.Any(y => y == x.Status) && !terminationTypes.Any(y => y == x.TerminationType)).Select(x => x.EmployeeBasicInfoId).ToList();

            if (HCAdmin)
            {
                var result = await context.EmployeeBasicInfos.Where(x => !(x.IsDeleted ?? false) &&
                                    x.NameEmployee.Trim().Replace(" ", "").ToLower().Contains(name.Trim().Replace(" ", "").ToLower()) &&
                                    !terminationEmployeeId.Any(y => y == x.Id) &&
                                    (!isContract ? true : x.EmployementStatusId == employeeStatus))
                                    .Select(x => new { Text = x.NameEmployee, Value = x.Id }).Take(50).ToListAsync();
                return result;
            }
            else
            {
                var parentId = (from b in context.EmployeeBasicInfos
                                join c in context.BusinessUnitJobLevels on b.BusinessUnitJobLevelId equals c.Id
                                where b.Id == employeeId
                                select c.Id).FirstOrDefault();

                var result = (from b in context.EmployeeBasicInfos
                              join c in context.BusinessUnitJobLevels on b.BusinessUnitJobLevelId equals c.Id
                              where b.NameEmployee.Trim().Replace(" ", "").ToLower().Contains(name.Trim().Replace(" ", "").ToLower()) &&
                              !terminationEmployeeId.Any(y => y == b.Id) && c.ParentId.Trim() == parentId &&
                              (!isContract ? true : b.EmployementStatusId == employeeStatus)
                              select new { Text = b.NameEmployee, Value = b.Id }).Take(50).ToList();
                return result;
            }
        }

        public async Task<dynamic> GetEmployeeBasicInfoByHead(string name)
        {
            var employeeId = context.GetEmployeeId();
            var group = await userApp.GetByUserLogin();
            var HCAdmin = group.Where(x => x.GroupId == GroupAcess.HCAdmin).Count() > 0;

            if (HCAdmin)
            {
                return await context.EmployeeBasicInfos
                                    .Where(x => !(x.IsDeleted ?? false) && x.NameEmployee.Trim().Replace(" ", "").ToLower().Contains(name.Trim().Replace(" ", "").ToLower()))
                                    .Select(x => new { Text = x.NameEmployee, Value = x.Id }).ToListAsync();
            }
            else
            {
                var parentId = (from b in context.EmployeeBasicInfos
                                join c in context.BusinessUnitJobLevels on b.BusinessUnitJobLevelId equals c.Id
                                where b.Id == employeeId
                                select c.Id).FirstOrDefault();

                var result = (from b in context.EmployeeBasicInfos
                              join c in context.BusinessUnitJobLevels on b.BusinessUnitJobLevelId equals c.Id
                              where b.NameEmployee.Trim().Replace(" ", "").ToLower().Contains(name.Trim().Replace(" ", "").ToLower()) && c.ParentId.Trim() == parentId
                              select new { Text = b.NameEmployee, Value = b.Id }).Take(50).ToList();
                return result;
            }
        }

        public async Task<dynamic> GetEmployeeResignationType() => await GetTerminationType(new string[] { TerminationType.Resignation });
        public async Task<dynamic> GetEmployeeTerminationType() => await GetTerminationType(new string[] { TerminationType.EndOfContract, TerminationType.EndOfProbation });
        public async Task<dynamic> GetEmployeeDeathType() => await GetTerminationType(new string[] { TerminationType.Death, TerminationType.ProlongedIllness, TerminationType.Sanction, TerminationType.Retired });
        private async Task<dynamic> GetTerminationType(string[] id) => await context.TmTerminationTypes.Where(x => !(x.IsDeleted ?? false) && id.Any(z => z == x.Id)).Select(x => new { Text = x.TypeName, Value = x.Id }).ToListAsync();
        public async Task<dynamic> FaultCategoryList() => await context.FaultCategorys.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.FaultName, Value = x.Id }).ToListAsync();
        public async Task<dynamic> ServiceNameList() => await context.MasterMappingRequests.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.ServiceName, Value = x.Id }).ToListAsync();
        public async Task<dynamic> CategoryExitInterviewList() => await context.CategoryExitInterviews.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.Name, Value = x.Id }).ToListAsync();
        public async Task<dynamic> LeaveRequestTypeList() => await context.LeaveRequestTypes.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.Name, Value = x.Id }).ToListAsync();
        public async Task<dynamic> LoanCategoryList() => await context.LoanCategorys.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.Name, Value = x.Id }).ToListAsync();
        public async Task<dynamic> SalaryComponentList() => await context.SalaryMainComponents.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.SubComponentName, Value = x.Id }).ToListAsync();
        public async Task<dynamic> AllowanceComponentList(string gradeId) => await context.AllowanceComponents.Where(x => !(x.IsDeleted ?? false) && x.JobGradeId == gradeId).Select(x => new { Text = x.SubComponentName, Value = x.Id }).ToListAsync();
        public async Task<dynamic> DeductionComponentList() => await context.DeductionComponents.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.SubComponentName, Value = x.Id }).ToListAsync();
        public async Task<dynamic> BpjsComponentAdditionalList() => await context.BpjsComponents.Where(x => !(x.IsDeleted ?? false) && x.Type == BPJSType.Additional).Select(x => new { Text = x.SubComponentName, Value = x.Id }).ToListAsync();
        public async Task<dynamic> BpjsComponentDeductionList() => await context.BpjsComponents.Where(x => !(x.IsDeleted ?? false) && x.Type == BPJSType.Deduction).Select(x => new { Text = x.SubComponentName, Value = x.Id }).ToListAsync();
        public async Task<dynamic> InsuranceCompanyList() => await context.InsuranceCompanys.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.NameInsurance, Value = x.Id }).ToListAsync();
        public async Task<dynamic> PdrCategoryList() => await context.PdrCategorys.Where(x => !(x.IsDeleted ?? false)).OrderBy(x => x.No).Select(x => new { Text = x.Name, Value = x.Id }).ToListAsync();
        public async Task<dynamic> PdrGradeList() => await context.PdrGrades.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.Result, Value = x.Id }).ToListAsync();
        public async Task<dynamic> LegalCategoryList() => await context.LegalCategorys.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.Category, Value = x.Id }).ToListAsync();
        public async Task<dynamic> GetDDLCorporateObjective() => await context.BscCorporateObjectives.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.BscName, Value = x.Id }).ToListAsync();
        public async Task<dynamic> GetDDLCorporateObjectiveName(string bscCategory) => await context.BscCorporateObjectives.Where(x => !(x.IsDeleted ?? false) && x.BscCategory.Trim() == bscCategory.Trim()).Select(x => new { Text = x.BscName, Value = x.Id }).ToListAsync();
        public async Task<dynamic> GetDDLObjectiveSetting() => await context.PdrObjectiveSettings.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.ObjectiveItem, Value = x.Id }).ToListAsync();
        public async Task<dynamic> GetDDLEmployeePayrollGroup() => await context.EmployeePayrollGroups.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.GroupName, Value = x.Id }).ToListAsync();
        public async Task<dynamic> GetDDLPTKPSetting() => await context.PtkpSettings.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.Name, Value = x.Id }).ToListAsync();

        public async Task<dynamic> GetDDLEmployeeLegalDoc(string category, string legalCategoryId, string text)
        {
            if (category == CategoryLegalDoc.Employee)
            {
                return await context.EmployeeBasicInfos.Where(x => !(x.IsDeleted ?? false) && x.NameEmployee.Contains(text)).Select(x => new { Text = x.NameEmployee, Value = x.Id }).ToListAsync();
            }
            else
            {

                var result = await (from a in context.EmployeeBasicInfos
                                    join b in context.LegalDocumentMasterCounterparts on a.Id equals b.EmployeeBasicInfoId
                                    where (a.IsDeleted ?? false) == false && b.LegalCategoryId == legalCategoryId && a.NameEmployee.Contains(text)
                                    select new { Text = a.NameEmployee, Value = a.Id }
                                     ).ToListAsync();
                return result;
            }
        }

        public async Task<dynamic> GetDDLLegalDoc(string category, string CounterpartId)
        {
            return await (from a in context.LegalDocuments
                          join b in context.LegalDocumentCounterparts on a.Id equals b.LegalDocumentId
                          where (a.IsDeleted ?? false) == false && (b.IsDeleted ?? false) == false &&
                          a.Category == category && b.CounterpartId == CounterpartId
                          select new { Text = a.Label, Value = a.Id }).ToListAsync();
        }

        public async Task<dynamic> GetDDLTrainingSetting()
        {
            return await context.Training.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.Name, Value = x.Id }).ToListAsync();
        }
        public async Task<dynamic> GetDDLTrainingCategory()
        {
            return await context.TrainingCategory.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.Category, Value = x.Category }).ToListAsync();
        }

        public async Task<dynamic> GetDDLTrainingSubject()
        {
            return await context.TrainingSubject.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.Subject, Value = x.Subject }).ToListAsync();
        }

        public async Task<dynamic> GetDDLTrainingType()
        {
            return await context.TrainingType.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.Type, Value = x.Type }).ToListAsync();
        }
        #endregion

        #region Media

        public async Task<dynamic> GetMediaType()
        {
            var result = await context.MediaTypes.Select(x => new { Text = x.MediaTypeName, Value = x.Id }).ToListAsync();
            return result;
        }

        public async Task<dynamic> GetMediaPlanRequestType()
        {
            var result = await context.TmMediaPlanRequestTypes.Select(x => new { Text = x.RequestTypeName, Value = x.Id }).ToListAsync();
            return result;
        }

        public async Task<dynamic> GetMediaPlanNo()
        {
            var result = await context.TrMediaPlanTVs
                .Where(w => w.TmMediaPlanStatusId == MediaPlanStatusName.Approved)
                .Select(x => new { Text = x.MediaPlanNo, Value = x.Id }).ToListAsync();
            return result;
        }

        public async Task<dynamic> GetMediaOrderStatus()
        {
            var result = await context.TmMediaOrderStatuses
                .Select(x => new { Text = x.MediaOrderStatusName, Value = x.Id }).ToListAsync();
            return result;
        }

        public async Task<dynamic> GetMediaPlanStatus()
        {
            var result = await context.TmMediaPlanStatuses
                .Select(x => new { Text = x.MediaPlanStatusName, Value = x.Id }).ToListAsync();
            return result;
        }

        public async Task<dynamic> GetProgramCategory()
        {
            var result = await context.TmProgramCategoryTypes
               .Select(x => new { Text = x.ProgramCategoryName, Value = x.Id }).ToListAsync();
            return result;
        }

        public async Task<dynamic> GetProgramPosition()
        {
            var result = await context.TmProgramPositions
               .Select(x => new { Text = x.ProgramPositionName, Value = x.Id }).ToListAsync();
            return result;
        }

        public async Task<dynamic> GetProgramType()
        {
            var result = await context.TmProgramTypes
               .Select(x => new { Text = x.ProgramTypeName, Value = x.Id }).ToListAsync();
            return result;
        }
        public async Task<dynamic> GetCascadeFromJob(string jobid, string caseData)
        {

            if (caseData.ToLower() == "mainservicecategory")
            {
                return await (from a in context.JobDetail
                              join b in context.MainServiceCategorys on a.MainServiceCategory equals b.Id
                              where !(a.IsDeleted ?? false) && a.Id == jobid
                              select new { Text = b.Name, Value = b.Id }).ToListAsync();
            }
            else if (caseData.ToLower() == "division")
            {
                return await (from a in context.JobDetail
                              join b in context.BusinessUnits on a.BusinessUnitDivisionId equals b.Id
                              where !(a.IsDeleted ?? false) && a.Id == jobid
                              select new { Text = b.UnitName, Value = b.Id }).ToListAsync();
            }
            else if (caseData.ToLower() == "legalentity")
            {
                return await (from a in context.JobDetail
                              join b in context.LegalEntity on a.LegalEntityId equals b.Id
                              where !(a.IsDeleted ?? false) && a.Id == jobid
                              select new { Text = b.LegalEntityName, Value = b.Id }).ToListAsync();
            }
            else if (caseData.ToLower() == "affiliation")
            {
                return await (from a in context.JobDetail
                              join b in context.Affiliations on a.AffiliationId equals b.Id
                              where !(a.IsDeleted ?? false) && a.Id == jobid
                              select new { Text = b.AffiliationName, Value = b.Id }).ToListAsync();
            }
            else if (caseData.ToLower() == "clientname")
            {
                return await (from a in context.JobDetail
                              join b in context.Companys on a.CompanyId equals b.Id
                              where !(a.IsDeleted ?? false) && a.Id == jobid
                              select new { Text = b.CompanyName, Value = b.Id }).ToListAsync();
            }
            else if (caseData.ToLower() == "clientbrand")
            {
                return await (from a in context.JobDetail
                              join b in context.CompanyBrands on a.BrandId equals b.Id
                              where !(a.IsDeleted ?? false) && a.Id == jobid
                              select new { Text = b.BrandName, Value = b.Id }).ToListAsync();
            }
            return new List<SelectListItem>();

        }

        public async Task<dynamic> GetBrandASF()
        {
            var result = await context.CompanyBrands.Where(a => !string.IsNullOrEmpty(a.ASFValue.ToString()))
               .Select(x => new { Text = x.ASFValue, Value = x.ASFValue }).Distinct().ToListAsync();
            return result;
        }
        #endregion

        #region Finance
        #region Master Data
        public async Task<dynamic> OrganizationList() => await context.Organizations.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.OrganizationName, Value = x.Id }).ToListAsync();
        public async Task<dynamic> GetAffiliation() => await context.Affiliations.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.AffiliationName, Value = x.Id }).ToListAsync();
        public async Task<dynamic> ChartofaccountReportList() => await context.ChartofaccountReports.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.ReportName, Value = x.Id }).ToListAsync();
        public async Task<dynamic> GetCOACodeByNameLv1() => await context.ChartOfAccounts.Where(coa => !(coa.IsDeleted ?? false) && coa.Level == "1").OrderBy(x => x.CodeRec).Select(x => new { Text = x.Name1, Value = x.CodeRec }).ToListAsync();
        public async Task<dynamic> GetCOACodeByNameLv2(string CodeRec) => await context.ChartOfAccounts.Where(coa => !(coa.IsDeleted ?? false) && coa.Level == "2" && CoaCodeLevel(CodeRec, coa.CodeRec, 2)).OrderBy(x => x.CodeRec).GroupBy(x => new { x.Name1, x.CodeRec }).Select(x => new { Text = x.Key.Name1, Value = x.Key.CodeRec }).ToListAsync();
        public async Task<dynamic> GetCOACodeByNameLv3(string CodeRec) => await context.ChartOfAccounts.Where(coa => !(coa.IsDeleted ?? false) && coa.Level == "3" && CoaCodeLevel(CodeRec, coa.CodeRec, 3)).OrderBy(x => x.CodeRec).GroupBy(x => new { x.Name1, x.CodeRec }).Select(x => new { Text = x.Key.Name1, Value = x.Key.CodeRec }).ToListAsync();
        public async Task<dynamic> GetCOACodeByNameLv4(string CodeRec) => await context.ChartOfAccounts.Where(coa => !(coa.IsDeleted ?? false) && coa.Level == "4" && CoaCodeLevel(CodeRec, coa.CodeRec, 4)).OrderBy(x => x.CodeRec).GroupBy(x => new { x.Name1, x.CodeRec }).Select(x => new { Text = x.Key.Name1, Value = x.Key.CodeRec }).ToListAsync();
        public async Task<dynamic> GetCOACodeByNameLv5(string CodeRec) => await context.ChartOfAccounts.Where(coa => !(coa.IsDeleted ?? false) && coa.Level == "5" && CoaCodeLevel(CodeRec, coa.CodeRec, 5)).OrderBy(x => x.CodeRec).GroupBy(x => new { x.Name1, x.CodeRec }).Select(x => new { Text = x.Key.Name1, Value = x.Key.CodeRec }).ToListAsync();
        public async Task<dynamic> CountryList() => await context.Countrys.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.CountryName, Value = x.Id }).ToListAsync();
        public async Task<dynamic> ProvincesList(string countryId) => await context.Provinces.Where(x => !(x.IsDeleted ?? false) && x.CountryId == countryId).Select(x => new { Text = x.ProvinceName, Value = x.Id }).ToListAsync();
        public async Task<dynamic> CityList(string ProvinceId) => await context.Citys.Where(x => !(x.IsDeleted ?? false) && x.ProvinceId == ProvinceId).Select(x => new { Text = x.CityName, Value = x.Id }).ToListAsync();
        public async Task<dynamic> GetClientlist() => await context.Companys.Where(x => !(x.IsDeleted ?? false) && (x.IsClient ?? false)).Select(x => new { Text = x.CompanyName, Value = x.Id }).ToListAsync();
        public async Task<dynamic> GetVendorList() => await context.Companys.Where(x => !(x.IsDeleted ?? false) && (x.IsVendor ?? false)).Select(x => new { Text = x.CompanyName, Value = x.Id }).ToListAsync();
        public async Task<dynamic> GetMasterConditionProcurement() => await context.MasterConditionProcurement.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.ConditionName, Value = x.Id }).ToListAsync();
        public async Task<dynamic> CostSharingAllocationList() => await context.CostSharingAllocations.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.CostSharingAllocationName, Value = x.Id }).ToListAsync();
        private bool CoaCodeLevel(string codeRec, string codeRecNew, int level)
        {
            var result = "";
            var resultNew = "";
            var array = codeRec.Split(".");
            var arrayNew = codeRecNew.Split(".");
            if (array.First() == arrayNew.First())
            {
                for (int i = 1; i <= array.Length; i++)
                {
                    if (i >= level) break;
                    result += array[i - 1];
                    resultNew += arrayNew[i - 1];
                };
            }
            else
            {
                return false;
            }
            return (result == resultNew);
        }
        #endregion

        public async Task<dynamic> GetCompanys()
        {
            var result = await context.Companys.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.CompanyName, Value = x.Id }).ToListAsync();
            return result;
        }

        public async Task<dynamic> GetClients(string id)
        {
            var result = await context.Companys.Where(x => !(x.IsDeleted ?? false) && (x.IsClient ?? false) && string.IsNullOrEmpty(id) ? true : x.Id == id).Select(x => new { Text = x.CompanyName, Value = x.Id }).ToListAsync();
            return result;
        }

        public async Task<dynamic> GetVendors(string id)
        {
            var result = await context.Companys.Where(x => !(x.IsDeleted ?? false) && (x.IsVendor ?? false) && string.IsNullOrEmpty(id) ? true : x.Id == id).Select(x => new { Text = x.CompanyName, Value = x.Id }).ToListAsync();
            return result;
        }

        public async Task<dynamic> GetRFQVendors(string rfqId, string vendorId)
        {
            var vendorDetail = await context.RequestForQuotationVendor.Where(y => !(y.IsDeleted ?? false) && y.RequestForQuotationId == rfqId && y.VendorId != vendorId).ToListAsync();
            var result = await context.Companys.Where(x => !(x.IsDeleted ?? false) && (x.IsVendor ?? false) && !vendorDetail.Any(y => y.VendorId == x.Id)).Select(x => new { Text = x.CompanyName, Value = x.Id }).ToListAsync();
            return result;
        }

        public async Task<dynamic> GetBrands()
        {
            var result = await context.CompanyBrands.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.BrandName, Value = x.Id }).ToListAsync();
            return result;
        }

        public async Task<dynamic> GetCurrencies()
        {
            var result = await context.Currencies.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.CurrencyName, Value = x.Id }).ToListAsync();
            return result;
        }

        public async Task<dynamic> GetPurchaseOrderByClientId(string clietnId, string invoiceId)
        {
            var poId = await context.InvoiceReceivables.Where(x => x.Id == invoiceId).Select(x => x.PoId).FirstOrDefaultAsync();
            var result = await context.PurchaseOrders.Where(x => x.CompanyId == clietnId &&
               (string.IsNullOrEmpty(invoiceId) ? validatePO(context.InvoiceReceivables.Where(y => y.PoId == x.Id && y.InvoiceStatusId != InvoiceReceivedStatus.Rejected).Count(), x.PercentPaymentTerm1, x.PercentPaymentTerm2, x.PercentPaymentTerm3) : x.Id == poId) &&
                !(x.IsDeleted ?? false) && x.Status == StatusTransactionName.Approved)
                .Select(x => new { Text = x.PurchaseOrderNumber, Value = x.Id }).ToListAsync();
            return result;
        }

        private bool validatePO(int paymentto, decimal? term1, decimal? term2, decimal? term3)
        {
            var totalpayment = 0;
            if ((term1 ?? 0) != 0) totalpayment++;
            if ((term2 ?? 0) != 0) totalpayment++;
            if ((term3 ?? 0) != 0) totalpayment++;
            return paymentto < totalpayment;
        }

        public async Task<dynamic> GetPurchaseOrderByVendorId(string vendorId)
        {
            var result = await context.PurchaseOrders.Where(x => x.CompanyId == vendorId && !(x.IsDeleted ?? false) && x.Status == StatusTransactionName.Approved).Select(x => new { Text = x.PurchaseOrderNumber, Value = x.Id }).ToListAsync();
            return result;
        }

        public async Task<dynamic> GetTAPDDL()
        {
            var result = await context.TemporaryAccountPayables.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.TapNumber, Value = x.Id }).ToListAsync();
            return result;
        }

        public async Task<dynamic> GetInvoiceVendorByPoId(string poId)
        {
            var result = await context.InvoiceReceivables.Where(x => !(x.IsDeleted ?? false) && x.PoId == poId && !x.IsDeleted.GetValueOrDefault()).Select(x => new { Text = x.InvoiceVendor, Value = x.InvoiceVendor }).ToListAsync();
            return result;
        }

        public async Task<dynamic> GetChartOfAccount(string code_rec_1, string level)
        {
            var result = await context.ChartOfAccounts.Where(coa => !(coa.IsDeleted ?? false) && coa.CodeRec.Substring(0, 1) == code_rec_1 && coa.Level == level).Select(x => new { Text = x.Name3, Value = x.Id }).ToListAsync();
            return result;
        }

        public async Task<dynamic> GetCOAByNameLv5() => await context.ChartOfAccounts.Where(coa => !(coa.IsDeleted ?? false) && coa.Level.Equals("5")).OrderBy(x => x.CodeRec).Select(x => new { Text = $"({x.CodeRec}) {x.Name5}", Value = x.Id }).ToListAsync();

        public async Task<dynamic> GetChartOfAccountLv5()
        {
            var result = await context.ChartOfAccounts.Where(coa => !(coa.IsDeleted ?? false) && coa.Level.Equals("5")).OrderBy(x => x.CodeRec).Select(x => new { Text = x.CodeRec, Value = x.Id }).ToListAsync();
            return result;
        }

        public async Task<dynamic> GetChartOfAccountLv5Like(string search) => await context.ChartOfAccounts.Where(coa => !(coa.IsDeleted ?? false) && coa.Level.Equals("5") && EF.Functions.Like(coa.CodeRec, "%" + search + "%")).OrderBy(x => x.CodeRec).Select(x => new { Text = x.CodeRec, Value = x.Id }).ToListAsync();

        public async Task<dynamic> GetMainServiceCategory() => await context.MainServiceCategorys.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.Name, Value = x.Id }).ToListAsync();

        public async Task<dynamic> GetMainserviceCategoryPCA()
        {
            var result = await context.MainServiceCategorys.Where(x => !(x.IsDeleted ?? false) && x.Id != MainServiceCategoryOther.MainServiceCategoryId).Select(x => new { Text = x.Name, Value = x.Id }).ToListAsync();
            return result;
        }

        public async Task<dynamic> GetPurchaseRequestNumber(string purchaseId)
        {
            var result = await context.PurchaseRequest.Where(x => !(x.IsDeleted ?? false) &&
            !string.IsNullOrEmpty(purchaseId) ? true : !context.RequestForQuotation.Any(y => y.PurchaseRequestId == x.Id))
            .Select(x => new { Text = x.RequestNumber, Value = x.Id }).ToListAsync();
            return result;
        }

        public async Task<dynamic> GetJobDetailInRFQTask()
        {
            var result = await context.RequestForQuotationTask.Where(p => !(p.IsDeleted ?? false) && !p.JobId.Equals(null)).ToListAsync();
            return result.Distinct().Select(x => new { Text = x.JobId, Value = x.JobId });
        }

        public async Task<dynamic> GetRFQOpenPO()
        {
            var result = await context.RequestForQuotation.Where(p => !(p.IsDeleted ?? false) && p.Status.Equals(StatusTransaction.StatusName(StatusTransaction.Approved))).ToListAsync();
            return result.Select(x => new { Text = x.RequestForQuotationNumber, Value = x.Id });
        }

        public async Task<dynamic> GetSpecialRequestPOType()
        {
            var result = await context.SpecialRequests.Where(p => !(p.IsDeleted ?? false)).ToListAsync();
            return result.Select(x => new { Text = x.SpecialName, Value = x.Id });
        }

        public async Task<dynamic> GetBrands(string company_id)
        {
            var result = await context.CompanyBrands.Where(p => !(p.IsDeleted ?? false) && p.CompanyId == company_id).ToListAsync();
            return result.Select(x => new { Text = x.BrandName, Value = x.Id });
        }

        public async Task<dynamic> GetInventoryGoodsReceipt(string poId) => await context.InventoryGoodsReceipts.Where(x => !(x.IsDeleted ?? false) && x.PurchaseOrderId == poId).Select(x => new { Text = x.GoodReceiptNumber, Value = x.Id }).ToListAsync();

        public async Task<dynamic> GetServiceReceipt(string poId) => await context.ServiceReceipt.Where(x => !(x.IsDeleted ?? false) && x.PurchaseOrderId == poId).Select(x => new { Text = x.ServiceReceiptNumber, Value = x.Id }).ToListAsync();

        public async Task<dynamic> GetCoaInvoice()
        {
            var result = await context.CoaInvoice.ToListAsync();
            return result.Select(x => new { Text = x.Name5, Value = x.Id });
        }
        public async Task<dynamic> GetCoaInvoiceTaxPayable()
        {
            var result = await context.CoaInvoiceTaxpayable.ToListAsync();
            return result.Select(x => new { Text = x.Name5, Value = x.Id });
        }
        public async Task<dynamic> GetBanks()
        {
            return await (from a in context.Banks
                          join b in context.LegalEntity on a.LegalEntityId equals b.Id
                          where !(a.IsDeleted ?? false)
                          select new { Text = a.BankName + "-" + b.LegalEntityName, Value = a.Id }).ToListAsync();
        }

        public async Task<dynamic> GetPettyCash()
        {
            return await (from a in context.MasterPettyCashs
                          join b in context.LegalEntity on a.LegalEntityId equals b.Id
                          where !(a.IsDeleted ?? false)
                          select new { Text = a.BankAccountName + "-" + b.LegalEntityName, Value = a.Id }).ToListAsync();
        }

        public async Task<dynamic> GetCoaInvoiceClient()
        {
            var result = await context.CoaInvoiceClients.ToListAsync();
            return result.Select(x => new { Text = x.Name5, Value = x.Id });
        }
        public async Task<dynamic> GetVendorsForInvRecevied(string id) => await context.Companys.Where(x => !(x.IsDeleted ?? false) && x.IsVendor.Equals(true) && string.IsNullOrEmpty(id) ? true : x.Id == id).Select(x => new { Text = x.CompanyName, Value = x.Id }).ToListAsync();

        public async Task<dynamic> GetClientInv() => await context.Companys.Where(x => !(x.IsDeleted ?? false) && x.IsClient.Equals(true) && x.IsDeleted.Equals(false)).Select(x => new { Text = x.CompanyName, Value = x.Id }).ToListAsync();

        public async Task<dynamic> GetBankReceivedType() => await context.BankReceivedType.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.ReceivedType, Value = x.Id }).ToListAsync();


        public async Task<dynamic> GetPCA() => await context.Pcas.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.Code, Value = x.Id }).ToListAsync();


        public async Task<dynamic> GetPCE() => await context.Pces.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.Code, Value = x.Id }).ToListAsync();


        public async Task<dynamic> GetFiscalYear() => await context.FinancialYears.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.FinancialYearName, Value = x.Id }).ToListAsync();
        public async Task<dynamic> GetFiscalYearByLegalId(string legalId)
        {
            if (legalId == "-")
                return await context.FinancialYears.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.FinancialYearName, Value = x.Id }).ToListAsync();
            else
                return await context.FinancialYears.Where(x => !(x.IsDeleted ?? false) && x.LegalEntityId == legalId).Select(x => new { Text = x.FinancialYearName, Value = x.Id }).ToListAsync();
        }

        public async Task<dynamic> GetFiscalPeriod(string Id) => await context.FinancePeriods.Where(x => !(x.IsDeleted ?? false) && x.FiscalYearId == Id).Select(x => new { Text = x.FinancePeriodName, Value = x.Id }).ToListAsync();

        public async Task<dynamic> GetMasterLocationProcurement() => await context.MasterLocationProcurement.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.LocationStore, Value = x.Id }).ToListAsync();

        public async Task<dynamic> GetOrderReceipt(string id) => await context.OrderReceipt.Where(x => x.IsDeleted.Equals(false) && x.GoodRequestNumber == id).Select(x => new { Text = x.ReceiptOrderNumber, Value = x.Id }).ToListAsync();

        #endregion

        #region PM
        public async Task<dynamic> GetJobNotExistsInPCAandJobId(string Status, string JobId)
        {
            if (Status == StatusTransactionName.Draft || Status == StatusTransactionName.Rejected)
            {
                var userId = context.GetUserId();
                return await (from a in context.PmTeams
                              join b in context.PmTeamMembers on a.Id equals b.TeamId
                              join c in context.JobDetail on a.JobId equals c.Id
                              where !(a.IsDeleted ?? false) && !(b.IsDeleted ?? false) && !(c.IsDeleted ?? false) &&
                                    b.ApplicationUserId == userId && !context.Pcas.Any(y => y.JobId == c.Id && y.JobId != JobId && !(y.IsDeleted ?? false) && y.Status != StatusTransactionName.Rejected)
                              select new { Text = c.JobName, Value = c.Id }).ToListAsync();
            }
            else
            {
                return await context.JobDetail.Where(x => !(x.IsDeleted ?? false) && !context.Pcas.Any(y => y.JobId == x.Id && y.JobId != JobId && (y.IsDeleted ?? false) == false)).Select(x => new { Text = x.JobName, Value = x.Id }).ToListAsync();
            }
        }

        public async Task<dynamic> GetJobNotExistsInPCEandJobId(string Status, string JobId)
        {
            if (Status == StatusTransactionName.Draft || Status == StatusTransactionName.Rejected)
            {
                var userId = context.GetUserId();
                return await (from a in context.PmTeams
                              join b in context.PmTeamMembers on a.Id equals b.TeamId
                              join c in context.JobDetail on a.JobId equals c.Id
                              where !(a.IsDeleted ?? false) && !(b.IsDeleted ?? false) && !(c.IsDeleted ?? false) &&
                                    b.ApplicationUserId == userId && !context.Pces.Any(y => y.JobId == c.Id && y.JobId != JobId && (y.IsDeleted ?? false) == false && y.Status != StatusTransactionName.Rejected)
                              select new { Text = c.JobName, Value = c.Id }).ToListAsync();
            }
            else
            {
                return await context.JobDetail.Where(x => !(x.IsDeleted ?? false) && !context.Pces.Any(y => y.JobId == x.Id && y.JobId != JobId && (y.IsDeleted ?? false) == false)).Select(x => new { Text = x.JobName, Value = x.Id }).ToListAsync();
            }
        }

        public async Task<dynamic> GetMotherPca() => await context.MotherPcas.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.Name, Value = x.Id }).ToListAsync();

        public async Task<dynamic> GetRateCard() => await context.RateCards.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.Name, Value = x.Id }).ToListAsync();

        public async Task<dynamic> GetTaskRateCard(string rateCardId)
        {
            var model = await context.RateCards.Where(x => !(x.IsDeleted ?? false) && x.Id == rateCardId).FirstOrDefaultAsync();
            if (model != null)
            {
                var detail = model.Detail;
                if (!string.IsNullOrEmpty(detail))
                {
                    var list = JsonConvert.DeserializeObject<List<TaskRateCard>>(detail);
                    return list.Select(x => new { Text = x.task_name, Value = x.task_name });
                }
            }
            return new List<SelectListItem>();
        }
        public async Task<dynamic> GetSubTaskRateCard(string rateCardId, string taskRateCardName)
        {
            var model = await context.RateCards.Where(x => !(x.IsDeleted ?? false) && x.Id == rateCardId).FirstOrDefaultAsync();
            if (model != null)
            {
                var detail = model.Detail;
                if (!string.IsNullOrEmpty(detail))
                {
                    var list = JsonConvert.DeserializeObject<List<TaskRateCard>>(detail);
                    var task = list.Where(x => x.task_name == taskRateCardName).FirstOrDefault();
                    if (task != null)
                    {
                        return task.sub_Task.Select(x => new { Text = x.subtask, Value = x.subtask });
                    }
                }
            }
            return new List<SelectListItem>();
        }

        public async Task<dynamic> GetTypeOfExpense() => await context.TypeOfExpenses.Where(x => !(x.IsDeleted ?? false) && (x.IsActive ?? true)).Select(x => new { Text = x.Name, Value = x.Id }).ToListAsync();

        public async Task<dynamic> GetMainserviceCategory() => await context.MainServiceCategorys.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.Name, Value = x.Id }).ToListAsync();

        public async Task<dynamic> GetShareservices() => await context.ShareServicess.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.ShareserviceName, Value = x.Id }).ToListAsync();

        public async Task<dynamic> GetShareservices(string MainserviceCategoryId) => await context.ShareServicess.Where(x => !(x.IsDeleted ?? false) && x.MainserviceCategoryId == MainserviceCategoryId).Select(x => new { Text = x.ShareserviceName, Value = x.Id }).ToListAsync();

        public async Task<dynamic> GetJobId() => await context.JobDetail.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.Id + " - " + x.JobName, Value = x.Id }).ToListAsync();

        public async Task<dynamic> GetPCEReference(string pceId)
        {
            var result = await context.Pces.Where(x => !(x.IsDeleted ?? false) && x.Id != pceId).Select(x => new { Text = x.Code, Value = x.Id }).ToListAsync();
            return result;
        }

        public async Task<dynamic> MasterOutletList(string clientId) => await context.MasterOutlets.Where(x => !(x.IsDeleted ?? false) && x.ClientId == clientId).Select(x => new { Text = x.OutletName, Value = x.Id }).ToListAsync();

        public async Task<dynamic> GetJobByPCEApproved()
        {
            var result = await (from a in context.Pces
                                join b in context.JobDetail on a.JobId equals b.Id
                                where !(a.IsDeleted ?? false) && a.Status == StatusTransactionName.Approved
                                select new { Text = b.Id + " - " + b.JobName, Value = b.Id }).ToListAsync();
            return result;
        }

        public async Task<dynamic> GetFixedAssetMovementStatus()
        {
            var result = await context.FixedAssetMovementStatus.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.StatusName, Value = x.Id }).ToListAsync();
            return result;
        }

        public async Task<dynamic> GetBankVendor()
        {
            var result = await (from a in context.Banks
                                join c in context.Companys on a.Id equals c.BankId
                                where !(a.IsDeleted ?? false)
                                select new { Text = a.BankName, Value = a.Id }).Distinct().ToListAsync();
            //result = result.Distinct();
            return result;
        }

        public async Task<dynamic> GetGoodRequest() => await context.GoodRequest.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.GoodRequestNumber, Value = x.Id }).ToListAsync();

        public async Task<dynamic> GetFixedAsset() => await context.FixedAsset.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.FixedAssetName, Value = x.Id }).ToListAsync();

        public async Task<dynamic> GetInventory() => await context.Inventory.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.Id + " - " + x.InventoryName, Value = x.Id }).ToListAsync();

        public async Task<dynamic> GetMasterDepreciation() => await context.MasterDepreciation.Where(x => !(x.IsDeleted ?? false) && x.status == "Active").Select(x => new { Text = x.DepreciationName, Value = x.Id }).ToListAsync();
        public async Task<dynamic> GetAllocationRatio() => await context.MasterAllocationRatios.Where(x => !(x.IsDeleted ?? false)).Select(x => new { Text = x.CostAllocationName, Value = x.Id }).ToListAsync();

        public async Task<dynamic> GetPurchaceOrderByClientIdInGoodReceipt(string clientId)
        {
            var gr = await (from a in context.InventoryGoodsReceipts
                            join b in context.InventoryGoodsReceiptsDetail on a.Id equals b.InventoryGoodsReceiptId
                            where a.IsDeleted.Equals(false) && b.RemainingQty == 0
                            select a.PurchaseOrderNumber).ToListAsync();
            var po = await context.PurchaseOrders.Where(x => x.IsDeleted.Equals(false) && x.CompanyId == clientId && !gr.Contains(x.PurchaseOrderNumber) && x.Status.Equals("Approved"))
                            .Select(x => new { Text = x.PurchaseOrderNumber, Value = x.Id }).AsNoTracking().ToListAsync();
            return po;
        }

        #endregion
    }
}
