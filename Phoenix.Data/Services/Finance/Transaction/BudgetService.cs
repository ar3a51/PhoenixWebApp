﻿using Phoenix.Data.Models.Finance.Transaction;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Data;
using Microsoft.AspNetCore.Http;
using Phoenix.ApiExtension.Extensions;
using Phoenix.Data.Models.Finance;
using System.Data.SqlClient;
using Phoenix.Data.Models;

namespace Phoenix.Data.Services.Finance.Transaction
{
    public interface IBudgetService : IDataService<Budget>
    {
        Task<DataSet> ReadExcel(IFormFile file);
        Task<dynamic> GetMasterDivisionSelfservice();
        Task<dynamic> GetMasterFinancialyear();
        Task<int> AddDetailFileAsync(Budget entity, DataSet data);
        Task<int> InsertBudget(Budget entity, DataSet data,string id);
        Task<int> InsertBudgettoTmpTable(Budget entity, DataSet data);
        Task<List<TypeBudgetHdr>> GetWithName();
        Task<TypeBudgetHdr> GetIdWithName(string Id);
        Task<List<TypeBudget>> GetTypeBugetWithHeaderId(string Id);
        Task<int> DeleteSoftBudgetHdrAsync(TypeBudgetHdr entity);
        Task<int> InsertTmpTabletoBudgetSp(string finalcial_year_id, string bulan, int? revisi, string type_budget_hdr_id);
    }
    public class BudgetService : IBudgetService
    {
        readonly DataContext context;
        public BudgetService(DataContext context) {
            this.context = context;
        }
        public async Task<int> AddAsync(Budget entity)
        {
            entity.Id = Guid.NewGuid().ToString();
            await context.PhoenixAddAsync(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> InsertBudgettoTmpTable(Budget entity, DataSet data)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                Random rnd = new Random();
                try
                {
                    TypeBudgetHdr headerdata = new TypeBudgetHdr();
                    List<TypeBudget> typebudgets = new List<TypeBudget>();
                    

                    headerdata.financialyear = entity.FinancialYearName;
                    headerdata.employee_basic_info_id = entity.employee_basic_info_id;
                    headerdata.bulan = entity.FinancialMonth;
                    headerdata.revision = entity.revision;
                    headerdata.organization_id = "9EE4834225E334380667DDCF2F60F6DA";
                    headerdata.upload_date = DateTime.Now;
                    headerdata.Id = Guid.NewGuid().ToString();
                    
                   

                    var ExcelTab = data.Tables["YTD EXPENSE 2018"];

                    for (int col = 11; col < 25; col++)
                    {
                        for (int row = 2; row < 96; row++)
                        {
                            if (col != 21 && col != 23)
                            {
                                TypeBudget typeBudget = new TypeBudget();
                                //string RandomString = "BGT" + DateTime.Now.ToString("HHmmss") + rnd.Next(1, 9999);
                                typeBudget.financialyear = entity.FinancialYearName;
                                typeBudget.type_budget_hdr_id = headerdata.Id;
                                typeBudget.bulan = entity.FinancialMonth;
                                typeBudget.revisi = entity.revision;
                                typeBudget.organization_id = headerdata.organization_id;
                                typeBudget.skor = 0;
                                typeBudget.Interface = 0;
                                typeBudget.tm_advis = 0;
                                typeBudget.brandcoma = 0;
                                typeBudget.brandcomb = 0;
                                typeBudget.marketingservicerm = 0;
                                typeBudget.marketingservicesampling = 0;
                                typeBudget.pr = 0;
                                typeBudget.pathfinder = 0;
                                typeBudget.totaliris = 0;
                                typeBudget.totalgroup = 0;
                                typeBudget.bodcentral = 0;
                                typeBudget.Id = Guid.NewGuid().ToString();
                                //-----
                                typeBudget.accountname = ExcelTab.Rows[row][0].ToString();
                                //---
                                if (typeBudget.accountname != string.Empty && typeBudget.accountname != "-" && typeBudget.accountname != "" && typeBudget.accountname != null)
                                {
                                    var dataaccount = typebudgets.Where(pr => typeBudget.accountname.Equals(ExcelTab.Rows[row][0].ToString())).FirstOrDefault();
                                    if (dataaccount != null)
                                    {
                                        dataaccount.brandcoma = 10000;
                                      
                                    }
                                    else
                                    {
                                        if (ExcelTab.Rows[row][col].ToString() != string.Empty || ExcelTab.Rows[row][col].ToString() != "-" || ExcelTab.Rows[row][col].ToString() != null || ExcelTab.Rows[row][col].ToString() != "")
                                        {
                                            if (col == 11)
                                            {
                                                typeBudget.skor = ExcelTab.Rows[row][col] is DBNull ? 0 : decimal.Parse(Convert.ToString(ExcelTab.Rows[row][col]));
                                            }
                                            else if (col == 12)
                                            {
                                                typeBudget.Interface = ExcelTab.Rows[row][col] is DBNull ? 0 : decimal.Parse(Convert.ToString(ExcelTab.Rows[row][col]));
                                            }
                                            else if (col == 13)
                                            {
                                                typeBudget.tm_advis = ExcelTab.Rows[row][col] is DBNull ? 0 : decimal.Parse(Convert.ToString(ExcelTab.Rows[row][col]));
                                            }
                                            else if (col == 14)
                                            {
                                                typeBudget.brandcoma = ExcelTab.Rows[row][col] is DBNull ? 0 : decimal.Parse(Convert.ToString(ExcelTab.Rows[row][col]));
                                            }
                                            else if (col == 15)
                                            {
                                                typeBudget.brandcomb = ExcelTab.Rows[row][col] is DBNull ? 0 : decimal.Parse(Convert.ToString(ExcelTab.Rows[row][col]));
                                            }
                                            else if (col == 16)
                                            {
                                                typeBudget.marketingservicesampling = ExcelTab.Rows[row][col] is DBNull ? 0 : decimal.Parse(Convert.ToString(ExcelTab.Rows[row][col]));
                                            }
                                            else if (col == 17)
                                            {
                                                typeBudget.marketingservicerm = ExcelTab.Rows[row][col] is DBNull ? 0 : decimal.Parse(Convert.ToString(ExcelTab.Rows[row][col]));
                                            }
                                            else if (col == 18)
                                            {
                                                typeBudget.pr = ExcelTab.Rows[row][col] is DBNull ? 0 : decimal.Parse(Convert.ToString(ExcelTab.Rows[row][col]));
                                            }
                                            else if (col == 19)
                                            {
                                                typeBudget.pathfinder = ExcelTab.Rows[row][col] is DBNull ? 0 : decimal.Parse(Convert.ToString(ExcelTab.Rows[row][col]));
                                            }
                                            else if (col == 20)
                                            {
                                                typeBudget.totaliris = ExcelTab.Rows[row][col] is DBNull ? 0 : decimal.Parse(Convert.ToString(ExcelTab.Rows[row][col]));
                                            }
                                            else if (col == 22)
                                            {
                                                typeBudget.bodcentral = ExcelTab.Rows[row][col] is DBNull ? 0 : decimal.Parse(Convert.ToString(ExcelTab.Rows[row][col]));
                                            }
                                            else if (col == 24)
                                            {
                                                typeBudget.totalgroup = ExcelTab.Rows[row][col] is DBNull ? 0 : decimal.Parse(Convert.ToString(ExcelTab.Rows[row][col]));
                                            }
                                        }
                                        else
                                        {

                                            if (col == 11)
                                            {
                                                typeBudget.skor = 0;
                                            }
                                            else if (col == 12)
                                            {
                                                typeBudget.Interface = 0;
                                            }
                                            else if (col == 13)
                                            {
                                                typeBudget.tm_advis = 0;
                                            }
                                            else if (col == 14)
                                            {
                                                typeBudget.brandcoma = 0;
                                            }
                                            else if (col == 15)
                                            {
                                                typeBudget.brandcomb = 0;
                                            }
                                            else if (col == 16)
                                            {
                                                typeBudget.marketingservicesampling = 0;
                                            }
                                            else if (col == 17)
                                            {
                                                typeBudget.marketingservicerm = 0;
                                            }
                                            else if (col == 18)
                                            {
                                                typeBudget.pr = 0;
                                            }
                                            else if (col == 19)
                                            {
                                                typeBudget.pathfinder = 0;
                                            }
                                            else if (col == 20)
                                            {
                                                typeBudget.totaliris = 0;
                                            }
                                            else if (col == 22)
                                            {
                                                typeBudget.bodcentral = 0;
                                            }
                                            else if (col == 24)
                                            {
                                                typeBudget.totalgroup = 0;
                                            }
                                        }
                                        typebudgets.Add(typeBudget);
                                
                                    }
                                }
                            }
                        }
                    }
                    TypeBudget[] typebugetsarray = typebudgets.ToArray();
                    foreach (var detail in typebugetsarray)
                    {
                        detail.Id = Guid.NewGuid().ToString();
                        detail.type_budget_hdr_id = headerdata.Id;
                    }

                    await context.PhoenixAddAsync(headerdata);
                    await context.PhoenixAddRangeAsync(typebugetsarray);

                    //foreach (var sp in typebugetsarray)
                    //{
                    //    string BudgetCode = await TransID.GetTransId(context, Code.Budget, DateTime.Today);
                    //    await InsertTmpTabletoBudgetSp(BudgetCode, sp.financialyear, sp.bulan, sp.revisi, sp.type_budget_hdr_id);
                    //}
                    var save = await context.SaveChangesAsync();
                    transaction.Commit();
                    context.Dispose();
                    return save;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    context.Dispose();
                    throw new Exception(ex.Message.ToString());
                }    
            }
        }

        public async Task<int> InsertBudget(Budget entity, DataSet data,string id)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                Random rnd = new Random();
                try
                {
                    TypeBudgetHdr headerdata = new TypeBudgetHdr();
                    List<TypeBudget> typebudgets = new List<TypeBudget>();


                    headerdata.financialyear = entity.FinancialYearName;
                    headerdata.employee_basic_info_id = entity.employee_basic_info_id;
                    headerdata.bulan = entity.FinancialMonth;
                    headerdata.revision = entity.revision;
                    headerdata.organization_id = "9EE4834225E334380667DDCF2F60F6DA";
                    headerdata.upload_date = DateTime.Now;
                    headerdata.financial_year_id = entity.FinancialYearId;
                    headerdata.Id = id;



                    var ExcelTab = data.Tables["YTD EXPENSE 2018"];

                    for (int row = 2; row < 96; row++)
                    {
                        TypeBudget typeBudget = new TypeBudget();
                        //string RandomString = "BGT" + DateTime.Now.ToString("HHmmss") + rnd.Next(1, 9999);
                        typeBudget.financialyear = entity.FinancialYearName;
                        typeBudget.bulan = entity.FinancialMonth;
                        typeBudget.revisi = entity.revision;
                        typeBudget.organization_id = headerdata.organization_id;
                        typeBudget.skor = 0;
                        typeBudget.Interface = 0;
                        typeBudget.tm_advis = 0;
                        typeBudget.brandcoma = 0;
                        typeBudget.brandcomb = 0;
                        typeBudget.is_upload = "Y";
                        typeBudget.marketingservicerm = 0;
                        typeBudget.marketingservicesampling = 0;
                        typeBudget.pr = 0;
                        typeBudget.pathfinder = 0;
                        typeBudget.totaliris = 0;
                        typeBudget.totalgroup = 0;
                        typeBudget.bodcentral = 0;
                        typeBudget.accountname = ExcelTab.Rows[row][0].ToString();

                        for (int col = 11; col < 25; col++)
                        {
                            if (col != 21 && col != 23)
                            {           
                                //---
                                if (typeBudget.accountname != string.Empty && typeBudget.accountname != "-" && typeBudget.accountname != "" && typeBudget.accountname != null)
                                { 
                                    if (ExcelTab.Rows[row][col].ToString() != string.Empty || ExcelTab.Rows[row][col].ToString() != "-" || ExcelTab.Rows[row][col].ToString() != null || ExcelTab.Rows[row][col].ToString() != "")
                                    {
                                        if (col == 11)
                                        {
                                            typeBudget.skor = ExcelTab.Rows[row][col] is DBNull ? 0 : decimal.Parse(Convert.ToString(ExcelTab.Rows[row][col]));
                                        }
                                        else if (col == 12)
                                        {
                                            typeBudget.Interface = ExcelTab.Rows[row][col] is DBNull ? 0 : decimal.Parse(Convert.ToString(ExcelTab.Rows[row][col]));
                                        }
                                        else if (col == 13)
                                        {
                                            typeBudget.tm_advis = ExcelTab.Rows[row][col] is DBNull ? 0 : decimal.Parse(Convert.ToString(ExcelTab.Rows[row][col]));
                                        }
                                        else if (col == 14)
                                        {
                                            typeBudget.brandcoma = ExcelTab.Rows[row][col] is DBNull ? 0 : decimal.Parse(Convert.ToString(ExcelTab.Rows[row][col]));
                                        }
                                        else if (col == 15)
                                        {
                                            typeBudget.brandcomb = ExcelTab.Rows[row][col] is DBNull ? 0 : decimal.Parse(Convert.ToString(ExcelTab.Rows[row][col]));
                                        }
                                        else if (col == 16)
                                        {
                                            typeBudget.marketingservicesampling = ExcelTab.Rows[row][col] is DBNull ? 0 : decimal.Parse(Convert.ToString(ExcelTab.Rows[row][col]));
                                        }
                                        else if (col == 17)
                                        {
                                            typeBudget.marketingservicerm = ExcelTab.Rows[row][col] is DBNull ? 0 : decimal.Parse(Convert.ToString(ExcelTab.Rows[row][col]));
                                        }
                                        else if (col == 18)
                                        {
                                            typeBudget.pr = ExcelTab.Rows[row][col] is DBNull ? 0 : decimal.Parse(Convert.ToString(ExcelTab.Rows[row][col]));
                                        }
                                        else if (col == 19)
                                        {
                                            typeBudget.pathfinder = ExcelTab.Rows[row][col] is DBNull ? 0 : decimal.Parse(Convert.ToString(ExcelTab.Rows[row][col]));
                                        }
                                        else if (col == 20)
                                        {
                                            typeBudget.totaliris = ExcelTab.Rows[row][col] is DBNull ? 0 : decimal.Parse(Convert.ToString(ExcelTab.Rows[row][col]));
                                        }
                                        else if (col == 22)
                                        {
                                            typeBudget.bodcentral = ExcelTab.Rows[row][col] is DBNull ? 0 : decimal.Parse(Convert.ToString(ExcelTab.Rows[row][col]));
                                        }
                                        else if (col == 24)
                                        {
                                            typeBudget.totalgroup = ExcelTab.Rows[row][col] is DBNull ? 0 : decimal.Parse(Convert.ToString(ExcelTab.Rows[row][col]));
                                        }
                                    }
                                    else
                                    {
                                        if (col == 11)
                                        {
                                            typeBudget.skor = 0;
                                        }
                                        else if (col == 12)
                                        {
                                            typeBudget.Interface = 0;
                                        }
                                        else if (col == 13)
                                        {
                                            typeBudget.tm_advis = 0;
                                        }
                                        else if (col == 14)
                                        {
                                            typeBudget.brandcoma = 0;
                                        }
                                        else if (col == 15)
                                        {
                                            typeBudget.brandcomb = 0;
                                        }
                                        else if (col == 16)
                                        {
                                            typeBudget.marketingservicesampling = 0;
                                        }
                                        else if (col == 17)
                                        {
                                            typeBudget.marketingservicerm = 0;
                                        }
                                        else if (col == 18)
                                        {
                                            typeBudget.pr = 0;
                                        }
                                        else if (col == 19)
                                        {
                                            typeBudget.pathfinder = 0;
                                        }
                                        else if (col == 20)
                                        {
                                            typeBudget.totaliris = 0;
                                        }
                                        else if (col == 22)
                                        {
                                            typeBudget.bodcentral = 0;
                                        }
                                        else if (col == 24)
                                        {
                                            typeBudget.totalgroup = 0;
                                        }
                                    }
                                }
                            }
                        }

                        if (typeBudget.accountname != string.Empty && typeBudget.accountname != "-" && typeBudget.accountname != "" && typeBudget.accountname != null)
                        {
                            typebudgets.Add(typeBudget);
                        }
                    }
                    TypeBudget[] typebugetsarray = typebudgets.ToArray();
                    foreach (var detail in typebugetsarray)
                    {
                        detail.Id = Guid.NewGuid().ToString();
                        detail.type_budget_hdr_id = headerdata.Id;
                    }
                    //insert header
                    await context.PhoenixAddAsync(headerdata);
                    //insert detail
                    await context.PhoenixAddRangeAsync(typebugetsarray);
                    //exec sp insert into table budget
                    
                    var save = await context.SaveChangesAsync();
                            
                    transaction.Commit();
                    return save;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message.ToString());
                }
            }
        }

        public async Task<int> InsertTmpTabletoBudgetSp(string finalcial_year_id,string bulan,int? revisi,string type_budget_hdr_id)
        {
            try
            {
                string BudgetCode = await TransID.GetTransId(context, Code.Budget, DateTime.Today);
                //string CV = await TransID.GetTransId(context, Code.PaymentRequest, DateTime.Today);

                //var budgetNumber = new SqlParameter("@p_budget_number", BudgetCode);
                //var financalyearId = new SqlParameter("@p_financial_year_id", finalcial_year_id);
                //var _bulan = new SqlParameter("@p_bulan", bulan);
                //var _revisi = new SqlParameter("@p_revisi", revisi);
                //var typeBudgetHdrId = new SqlParameter("@p_type_budget_hdr_id", type_budget_hdr_id);

                //context.
                var result = await context.Database.ExecuteSqlCommandAsync("EXECUTE fn.sp_entry_budget @p_budget_number,@p_financial_year_id,@p_bulan, @p_revisi, @p_type_budget_hdr_id", parameters: new[]{ new SqlParameter("@p_budget_number", BudgetCode), new SqlParameter("@p_financial_year_id", finalcial_year_id),  new SqlParameter("@p_bulan", bulan), new SqlParameter("@p_revisi", revisi), new SqlParameter("@p_type_budget_hdr_id", type_budget_hdr_id) });
                //var result =  await context.Database.ExecuteSqlCommandAsync("EXECUTE fn.sp_entry_budget '@p_budget_number','@p_financial_year_id','@p_bulan', @p_revisi, '@p_type_budget_hdr_id'", budgetNumber, financalyearId, _bulan, _revisi, typeBudgetHdrId);
                await context.SaveChangesAsync();
                return result;
            }
            catch (Exception ex) {
                throw new Exception(ex.Message.ToString());
            }
        }

        public async Task<int> AddDetailFileAsync(Budget entity, DataSet data)
        {
            
                Random rnd = new Random();
                var ExcelTab = data.Tables["YTD EXPENSE 2018"];
                entity.exchange_rate = 1;
                entity.currency_id = "46e4d57e-5ce2-4c31-82be-f81382dc243a";


           for (int col = 12; col < 21; col++)
           {
                   


              for (int row = 2; row < 96; row++)
              {
                    Budget newBudget = new Budget();
                    newBudget.currency_id = entity.currency_id;
                    newBudget.exchange_rate = entity.exchange_rate;
                    newBudget.FinancialYearId = entity.FinancialYearId;
                    newBudget.FinancialMonth = entity.FinancialMonth;
                    newBudget.organization_id = "9EE4834225E334380667DDCF2F60F6DA";
                    string RandomString = "BGT" + DateTime.Now.ToString("HHmmss") + rnd.Next(1, 9999);
                    newBudget.code = RandomString;
                    newBudget.revision = entity.revision;
                    newBudget.BusinessUnitId = ExcelTab.Rows[0][col].ToString();
                    newBudget.account_id = ExcelTab.Rows[row][0].ToString();

                    if (newBudget.account_id != string.Empty && newBudget.account_id != "-" && newBudget.account_id != "" && newBudget.account_id != null)
                    {
                            
                            newBudget.BudgetNumber = RandomString;
                            //newBudget.account_id = ExcelTab.Rows[row][0].ToString();
                            if (ExcelTab.Rows[row][col].ToString() != string.Empty || ExcelTab.Rows[row][col].ToString() != "-" || ExcelTab.Rows[row][col].ToString() != null || ExcelTab.Rows[row][col].ToString() != "")
                            {
                                
                                newBudget.budget_value = ExcelTab.Rows[row][col] is DBNull ? 0 : double.Parse(Convert.ToString(ExcelTab.Rows[row][col]));
                            }
                            else {

                                newBudget.budget_value = 0;
                            }
                                
                            newBudget.budget_usage = 0;

                        newBudget.Id = Guid.NewGuid().ToString();
                        await context.PhoenixAddAsync(newBudget);
                    }
                    
              }
                  
            }                    
          
            return await context.SaveChangesAsync(); ;
        }

        public async Task<int> AddRangeAsync(params Budget[] entities)
        {
            await context.PhoenixAddRangeAsync(entities);
            return await context.SaveChangesAsync();
        }

     
        public async Task<int> DeleteAsync(Budget entity)
        {
            context.PhoenixApprove(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> DeleteRageAsync(params Budget[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> DeleteSoftAsync(Budget entity)
        {
            context.PhoenixDelete(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteSoftBudgetHdrAsync(TypeBudgetHdr entity)
        {
            context.PhoenixDelete(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> DeleteSoftRangeAsync(params Budget[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> EditAsync(Budget entity)
        {
            context.PhoenixEdit(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> EditRangeAsync(Budget entity)
        {
            throw new NotImplementedException();
        }

        public async Task<List<Budget>> Get()
        {
            return await context.Budget.AsNoTracking().Where(x => !(x.IsDeleted ?? false)).ToListAsync();
        }

        public async Task<List<TypeBudgetHdr>> GetWithName()
        {
        var data =  from tb1 in context.TypeBudgetHdr
                    join tb2 in await context.EmployeeBasicInfos.AsNoTracking().ToListAsync() on tb1.employee_basic_info_id equals tb2.Id
                    
                    select new TypeBudgetHdr()
                    {
                            Id = tb1.Id,
                            employee_basic_info_id = tb1.employee_basic_info_id,
                            employee_name = tb2.NameEmployee,
                            financialyear = tb1.financialyear,
                            financial_year_id = tb1.financial_year_id,
                            bulan = tb1.bulan,
                            revision = tb1.revision,
                            upload_date = tb1.upload_date
                    };
            return data.ToList();
        }

        public Task<List<Budget>> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public async Task<TypeBudgetHdr> GetIdWithName(string id)
        {
            var data = from tb1 in context.TypeBudgetHdr
                       join tb2 in await context.EmployeeBasicInfos.AsNoTracking().ToListAsync() on tb1.employee_basic_info_id equals tb2.Id
                       where tb1.Id ==id
                       select new TypeBudgetHdr()
                       {
                           Id = tb1.Id,
                           employee_basic_info_id = tb1.employee_basic_info_id,
                           employee_name = tb2.NameEmployee,
                           financialyear = tb1.financialyear,
                           financial_year_id = tb1.financial_year_id,
                           bulan = tb1.bulan,
                           revision = tb1.revision,
                           upload_date = tb1.upload_date
                       };
            return await data.FirstOrDefaultAsync();
            //return  context.Budget.AsNoTracking().Where(x => !(x.IsDeleted ?? false) && x.Id == id).FirstOrDefaultAsync();
        }
        public async Task<List<TypeBudget>> GetTypeBugetWithHeaderId(string Id)
        {
            try
            {
                var data = from tb1 in context.TypeBudget
                           where tb1.type_budget_hdr_id == Id
                           select new TypeBudget
                           {
                               Id = tb1.Id,
                               financialyear = tb1.financialyear,
                               bulan = tb1.bulan,
                               revisi = tb1.revisi,
                               type_budget_hdr_id = tb1.type_budget_hdr_id,
                               accountname = tb1.accountname,
                               skor = tb1.skor,
                               Interface = tb1.Interface,
                               tm_advis = tb1.tm_advis,
                               brandcoma = tb1.brandcoma,
                               brandcomb = tb1.brandcomb,
                               marketingservicerm = tb1.marketingservicerm,
                               marketingservicesampling = tb1.marketingservicesampling,
                               pr = tb1.pr,
                               pathfinder = tb1.pathfinder,
                               totaliris = tb1.totaliris,
                               totalgroup = tb1.totalgroup,
                               bodcentral = tb1.bodcentral
                           };
                return await data.ToListAsync();
            }
            catch (Exception ex) {
                throw new Exception(ex.Message.ToString());
            }
        }

        #region "Combobox / Dropdown"
        public async Task<dynamic> GetMasterDivisionSelfservice()
        {
            return await context.MasterDivisionSelfservices.Where(x => (x.IsDeleted ?? false) == false).Select(x => new { Text = x.BusinessUnitName, Value = x.Id }).ToListAsync();
        }

        public async Task<dynamic> GetMasterFinancialyear()
        {
            var data = await context.FinancialYears.Where(x => (x.IsDeleted ?? false) == false).Select(x => new { Text = x.FinancialYearName, Value = x.Id }).ToListAsync();
            return data;

        }

       
        #endregion


        public async Task<DataSet> ReadExcel(IFormFile file)
        {
            using (var reader = file.OpenReadStream())
            {
                var dataSet = ExcelConverter.ExcelToDataSet(reader);
                return dataSet;
            }
        }

        public Task<Budget> Get(string id)
        {
            throw new NotImplementedException();
        }

       
    }
}
