﻿using Phoenix.Data.Models.Finance;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Phoenix.Data.Services.Finance
{
    public interface IBankPaymentService : IDataService<BankPayment>
    {
        Task<List<PaymentInformationData>> GetPaymentInformationData();
    }

    public class BankPaymentService : IBankPaymentService
    {
        readonly DataContext context;
        readonly FinancePeriodService financePeriod;

        /// <summary>
        /// And endpoint to manage PettyCash
        /// </summary>
        /// <param name="context">Database context</param>
        public BankPaymentService(DataContext context, FinancePeriodService financePeriod)
        {
            this.context = context;
            this.financePeriod = financePeriod;
        }

        public async Task<int> AddAsync(BankPayment entity)
        {
            await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.DueDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.BP);
            entity.Id = Guid.NewGuid().ToString();
            await context.PhoenixAddAsync(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> AddRangeAsync(params BankPayment[] entities)
        {
            foreach (var entity in entities) await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.DueDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.BP);
            await context.PhoenixAddRangeAsync(entities);
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteAsync(BankPayment entity)
        {
            await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.DueDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.BP);
            context.PhoenixDelete(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteRageAsync(params BankPayment[] entities)
        {
            foreach (var entity in entities) await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.DueDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.BP);
            context.PhoenixDeleteRange(entities);
            return await context.SaveChangesAsync();

        }

        public async Task<int> DeleteSoftAsync(BankPayment entity)
        {
            await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.DueDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.BP);
            context.PhoenixDelete(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteSoftRangeAsync(params BankPayment[] entities)
        {
            foreach (var entity in entities) await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.DueDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.BP);
            context.PhoenixDeleteRange(entities);
            return await context.SaveChangesAsync();
        }

        public async Task<int> EditAsync(BankPayment entity)
        {
            await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.DueDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.BP);
            context.PhoenixEdit(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> EditRangeAsync(BankPayment entity)
        {
            await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.DueDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.BP);
            context.PhoenixEditRange(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<List<BankPayment>> Get()
        {
            return await context.BankPayment.Where(x => x.IsDeleted.Equals(false)).ToListAsync();
        }


        public Task<List<BankPayment>> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public async Task<BankPayment> Get(string Id) => await context.BankPayment.AsNoTracking().Where(x => !(x.IsDeleted ?? false) && x.Id == Id).FirstOrDefaultAsync();

        public async Task<List<PaymentInformationData>> GetPaymentInformationData()
        {
            var model = await (from tb in context.BankPayment
                               where tb.IsDeleted.Equals(false)
                               select new PaymentInformationData
                               {
                                   Id = tb.Id,
                                   EvidenceNo = tb.ReferenceNumber,
                                   Amount = tb.Amount,
                                   Discount = 0,
                                   Charges = 0,
                                   Vat = tb.Vat,
                                   VatPercent = tb.VatPercent,
                                   TaxPayable = tb.TaxPayable,
                                   TaxPayablePercent = tb.TaxPayablePercent,
                                   Recon = false,
                                   Billing = tb.TotalAmount,
                                   Paid = 0

                               }).ToListAsync();
            return model;
        }
    }
}
