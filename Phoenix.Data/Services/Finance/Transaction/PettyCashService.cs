﻿using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Attributes;
using Phoenix.Data.Models;
using Phoenix.Data.Models.Finance;
using Phoenix.Data.Models.Finance.MasterData;
using Phoenix.Data.Models.Finance.Transaction;
using Phoenix.Data.Services.Finance.Transaction.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.Data.Services.Finance
{
	public interface IPettyCashService : IDataService<NewPettyCash>
	{
		//: IDataService<NewPettyCash>
		Task<NewPettyCash> Get(string Id, string ReffNum);
		Task<List<NewPettyCash>> ReadIndex(NewPettyCash model);
		Task<List<PettyCashDetail>> GetDetails(string Id, string ReffNum);
		Task<JobDetail> GetInfoPceByJobId(string jobid);
		Task<List<Journal>> GetJournal(string Id, string rfn);
		Task<int> CreateJournal(NewPettyCash entity);
		Task<int> EditJournal(NewPettyCash entity);
		Task<dynamic> GetPcaTasks(string pcaId);
		Task<dynamic> GetGroupName();
	}

	public class PettyCashService : IPettyCashService
	{
		readonly DataContext context;
		readonly IJournalEntryService journalEntryService;
		readonly FinancePeriodService financePeriod;
		readonly GlobalFunctionApproval globalFcApproval;
		readonly Services.Um.IManageMenuService menuService;
		readonly FileService uploadFile;
		/// <summary>
		/// And endpoint to manage NewPettyCash
		/// </summary>
		/// <param name="context">Database context</param>
		public PettyCashService(DataContext context, IJournalEntryService journalEntryService, FinancePeriodService financePeriod,
			GlobalFunctionApproval globalFcApproval, Services.Um.IManageMenuService menuService, FileService uploadFile)
		{
			this.context = context;
			this.journalEntryService = journalEntryService;
			this.financePeriod = financePeriod;
			this.globalFcApproval = globalFcApproval;
			this.menuService = menuService;
			this.uploadFile = uploadFile;
		}

		public async Task<dynamic> GetGroupName()
		{
			string groupid = context.GetSubGroupAccess();
			string groupAccessName = (await context.TmGroups.FirstOrDefaultAsync(x => x.Id == groupid) ?? new Models.Um.TmGroup()).GroupName;
			return groupAccessName;
		}

		public async Task<dynamic> GetPcaTasks(string pcaId)
		{
			return await context.PcaTasks.Where(x => x.PcaId == pcaId).Select(x => new { Text = x.TaskName, Value = x.Id }).ToListAsync();
		}

		public async Task<JobDetail> GetInfoPceByJobId(string jobid)
		{
			var info = await (
				from a in context.JobDetail
				join b in context.Pces on a.Id equals b.JobId
				where a.Id == jobid
				select new JobDetail()
				{
					JobPeId = b.Code,
					JobPaId = b.PcaId
				}).FirstOrDefaultAsync();
			return info;
		}

		public async Task<List<NewPettyCash>> ReadIndex(NewPettyCash model)
		{
			//DateTime comingSunday = DateTime.Now.NextDayOfWeek(DayOfWeek.Monday);
			//DateTime thisWeekMonday = DateTime.Now.PreviousDayOfWeek(DayOfWeek.Monday);

			var initialList = await (
				from a in context.PettyCashs
				join reff in context.MasterPettyCashs on a.PettyCashNumber equals reff.Id
				join b in context.LegalEntity on reff.LegalEntityId equals b.Id
				join c in context.Affiliations on a.AffiliationId equals c.Id into cj
				from aff in cj.DefaultIfEmpty()
				join d in context.BusinessUnits on a.BusinessUnitId equals d.Id into db
				from bu in db.DefaultIfEmpty()
				where !(a.IsDeleted ?? false)
				&& a.PettyCashNumber == (model.PettyCashNumber ?? a.PettyCashNumber)
				&& reff.LegalEntityId == (model.LegalEntityId ?? reff.LegalEntityId)
				&& a.BusinessUnitId == (model.BusinessUnitId ?? a.BusinessUnitId)
				&& reff.BankAccountNumber == (model.BankAccountNumber ?? reff.BankAccountNumber)
				select new NewPettyCash()
				{
					Id = a.Id,
					PettyCashNumber = a.PettyCashNumber,
					LegalEntityId = reff.LegalEntityId,
					BusinessUnitId = a.BusinessUnitId,
					AffiliationId = a.AffiliationId,
					LegalEntity = b.LegalEntityName,
					Affiliation = aff.AffiliationName,
					BusinessUnit = bu.UnitName,
					BankAccountNumber = reff.BankAccountNumber,
					BankName = reff.BankAccountName,
					Status = a.Status > 0 ? a.Status : PettyCashStatus.Open,
					StatusName = PettyCashStatus.StatusName((a.Status > 0) ? a.Status : PettyCashStatus.Open)
				}).ToListAsync();

			return initialList;
		}

		public async Task<NewPettyCash> Get(string Id, string ReffNum)
		{
			if (Id == "0")
			{
				string fullName = null;
				var employeeId = context.GetEmployeeId();
				EmployeeBasicInfo employeeinfo = new EmployeeBasicInfo();
				employeeinfo = await context.EmployeeBasicInfos.Where(x => x.Id == employeeId).FirstOrDefaultAsync();
				if (employeeinfo != null)
				{
					fullName = employeeinfo.NameEmployee;
				}

				string cahsHolderId = string.Empty;
				var cashHolder = await context.CashHolders.Where(x => x.Id == employeeinfo.Id).FirstOrDefaultAsync();
				if (cashHolder != null)
				{
					cahsHolderId = cashHolder.Id;
				}

				string groupid = context.GetSubGroupAccess();
				string groupAccessName = (context.TmGroups.FirstOrDefault(x => x.Id == groupid) ?? new Models.Um.TmGroup()).GroupName;

				return new NewPettyCash()
				{
					RequestorName = fullName,
					SettlementDate = DateTime.Today,
					CashHolderId = cahsHolderId,
					Status = PettyCashStatus.Draft,
					StatusName = PettyCashStatus.StatusName(PettyCashStatus.Draft),
					TrnStatus = StatusTransaction.Draft,
					GroupName = groupAccessName
				};
			}
			else
			{
				var result = await (
				from a in context.PettyCashs
				join b in context.MasterPettyCashs on a.PettyCashNumber equals b.Id
				join c in context.LegalEntity on b.LegalEntityId equals c.Id
				join d in context.Affiliations on a.AffiliationId equals d.Id into cj
				from aff in cj.DefaultIfEmpty()
				join e in context.BusinessUnits on a.BusinessUnitId equals e.Id into db
				from bu in db.DefaultIfEmpty()
				where !(a.IsDeleted ?? false) && a.Id == Id
				select new NewPettyCash()
				{
					Id = a.Id,
					CashHolderId = a.CashHolderId,
					PettyCashNumber = a.PettyCashNumber,
					BankAccountNumber = b.BankAccountNumber,
					BankName = b.BankAccountName,
					LegalEntityId = b.LegalEntityId,
					BusinessUnitId = a.BusinessUnitId,
					AffiliationId = a.AffiliationId,
					Affiliation = aff.AffiliationName,
					CurrencyId = b.CurrencyId,
					ExchangeRate = a.ExchangeRate,
					Status = a.Status,
					SettlementDate = a.SettlementDate,
					EndingBalance = a.EndingBalance,
					StatusName = PettyCashStatus.StatusName((a.Status > 0) ? a.Status : PettyCashStatus.Draft),
					TrnStatus = a.TrnStatus
				}).FirstOrDefaultAsync();

				if (result != null)
				{
					EmployeeBasicInfo holder = new EmployeeBasicInfo();
					if (result.CashHolderId != null)
					{
						var cashHolder = await context.CashHolders.Where(x => x.Id == result.CashHolderId).FirstOrDefaultAsync();
						holder = await context.EmployeeBasicInfos.Where(x => x.Id == cashHolder.EmployeeId).FirstOrDefaultAsync();
						if (holder != null) result.RequestorName = holder.NameEmployee;
					}

					var employee = await context.EmployeeBasicInfos.Where(x => x.Id == result.CreatedBy).FirstOrDefaultAsync();
					if (employee != null) result.RequestorName = employee.NameEmployee;

					string groupid = context.GetSubGroupAccess();
					string groupAccessName = (context.TmGroups.FirstOrDefault(x => x.Id == groupid) ?? new Models.Um.TmGroup()).GroupName;
					result.GroupName = groupAccessName;
				}
				return result;
			}
		}

		public async Task<List<PettyCashDetail>> GetDetails(string Id, string ReffNum)
		{
			if (Id.Equals("0"))
			{
				var masterPetty = context.MasterPettyCashs.FirstOrDefault(x => x.Id == ReffNum && !(x.IsDeleted ?? false)) ?? new MasterPettyCash();
				var jurnalentry = new PettyCashDetail()
				{
					TransactionDate = DateTime.Today,
					TransactionDetail = "Begining Balance",
					Debit = masterPetty.BeginingBalance,
					Credit = 0,
					IsFromJE = true,
				};
				List<PettyCashDetail> data = new List<PettyCashDetail>();
				data.Add(jurnalentry);

				return data;
			}
			else
				{
				var result = await (
				from pd in context.PettyCashDetails
				join job in context.JobDetail on pd.JobId equals job.Id into jb
				from j in jb.DefaultIfEmpty()
				join tp in context.PcaTasks on pd.PcaTaskId equals tp.Id into tp1
				from pct in tp1.DefaultIfEmpty()
				join ca in context.ChartOfAccounts on new { Id = pd.AccountId, Level = "5"} equals new { ca.Id, ca.Level } into caa
				from coa in caa.DefaultIfEmpty()
				select new PettyCashDetail
				{
					AccountId = pd.AccountId,
					Id = pd.Id,
					TransactionDetail = pd.TransactionDetail,
					Debit = pd.Debit,
					Credit = pd.Credit,
					TransactionDate = pd.TransactionDate,
					FileMasterId = pd.FileMasterId,
					FileName = pd.FileName,
					JobId = pd.JobId,
					JobName = j.JobName,
					PcaId = pd.PcaId,
					PceId = pd.PceId,
					PcaTaskId = pd.PcaTaskId,
					PcaTask = pct.TaskName,
					AccountName = coa.Name5,
					CodeRec = coa.CodeRec
				}).ToListAsync();

				return result;
				//return await context.PettyCashDetails.Where(x => !(x.IsDeleted ?? false) && x.PettyCashId == Id).OrderByDescending(x => (x.Debit ?? 0)).ToListAsync();
			}
		}

		public async Task<int> AddAsync(NewPettyCash entity)
		{
			return await AddEdit(entity, false, false);
		}

		public async Task<int> EditAsync(NewPettyCash entity)
		{
			return await AddEdit(entity, true, false);
		}

		public async Task<int> CreateJournal(NewPettyCash entity)
		{
			return await AddEdit(entity, false, true);
		}

		public async Task<int> EditJournal(NewPettyCash entity)
		{
			return await AddEdit(entity, true, true);
		}

		private async Task<int> AddEdit(NewPettyCash entity, bool isEdit, bool isJournal)
		{
			await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.SettlementDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.PCS);
			using (var transaction = await context.Database.BeginTransactionAsync())
			{
				try
				{
					if (isEdit)
					{
						context.PhoenixEdit(entity);
					}
					else
					{
						var employeeId = context.GetEmployeeId();

						string ID = await TransID.GetTransId(context, Code.PettyCash, DateTime.Today);
						entity.Id = ID;

						var cash_holder = await context.CashHolders.Where(x => x.EmployeeId == employeeId).FirstOrDefaultAsync();
						if (cash_holder != null)
						{
							entity.CashHolderId = cash_holder.Id;
						}
						await context.PhoenixAddAsync(entity);
						await context.SaveChangesAsync();
					}

					if (!string.IsNullOrEmpty(entity.Id))
					{
						var detail = context.PettyCashDetails.AsNoTracking().Where(x => x.PettyCashId == entity.Id).ToList();
						if (detail.Count > 0)
						{
							context.PettyCashDetails.RemoveRange(detail);
						}
					}

					if (entity.Detail != null)
					{
						Parallel.ForEach(entity.Detail, (item) =>
						{
							item.PettyCashId = entity.Id;
							context.PhoenixAddAsync(item);
						});
					}

					//Create Journal
					if (isJournal == true)
					{
						if (!string.IsNullOrEmpty(entity.Id))
						{
							var journal = context.Journal.AsNoTracking().Where(x => x.ReferenceId == entity.Id).ToList();
							if (journal.Count > 0)
							{
								context.Journal.RemoveRange(journal);
							}
						}

						var createJournal = entity.Detail.Where(x => x.TransactionDetail != "Begining Balance")
							.Select(x => new Journal
							{
								Id = Guid.NewGuid().ToString(),
								Description = x.TransactionDetail,
								Debit = x.Credit,
								Credit = x.Debit,
								ReferenceId = entity.Id,
								ReferenceNumber = entity.Id,
								AccountId = x.AccountId,
								ReferenceDate = x.TransactionDate,
								TransactionDate = x.TransactionDate,
								CurrencyId = entity.CurrencyId,
								ExchangeRate = entity.ExchangeRate,
								LegalId = entity.LegalEntityId,
								AffiliateId = entity.AffiliationId,
								DivisiId = entity.BusinessUnitId,
								LineCode = "1",
								MainserviceCategoryId = "3335194186501656444",
								PcaId = x.PcaId,
								PceId = x.PceId,
								JobId = x.JobId,
								PcaTaskId = x.PcaTaskId
							});

						context.Journal.AddRange(createJournal);

						decimal? totalPetty = createJournal.Sum(x => x.Debit);
						var summary = new Journal
						{
							Id = Guid.NewGuid().ToString(),
							Description = entity.DescJournalEndingBalance,
							Debit = 0,
							Credit = totalPetty,
							ReferenceId = entity.Id,
							ReferenceNumber = entity.Id,
							AccountId = "2923999166262540",
							ReferenceDate = DateTime.Now,
							TransactionDate = DateTime.Now,
							CurrencyId = entity.CurrencyId,
							ExchangeRate = entity.ExchangeRate,
							LegalId = entity.LegalEntityId,
							AffiliateId = entity.AffiliationId,
							DivisiId = entity.BusinessUnitId,
							LineCode = "1",
							MainserviceCategoryId = "3335194186501656444"
						};
						context.Journal.Add(summary);
					}

					var isApprove = true;
					if (!(entity.TrnStatus == StatusTransaction.Draft))
					{
						var approval = await context.TrTemplateApprovals.AsNoTracking().Where(x => x.RefId == entity.Id).ToListAsync();
						if (approval.Count > 0)
						{
							var userApproval = await context.TrUserApprovals.AsNoTracking().Where(x => approval.Any(y => y.Id == x.TrTempApprovalId)).ToListAsync();
							var conditionUserApproval = await context.TrConditionUserApprovals.AsNoTracking().Where(x => userApproval.Any(y => y.Id == x.TrUserApprovalId)).ToListAsync();

							context.TrConditionUserApprovals.RemoveRange(conditionUserApproval);
							context.TrUserApprovals.RemoveRange(userApproval);
							context.TrTemplateApprovals.RemoveRange(approval);
						}


						var dataMenu = await menuService.GetByUniqueName(MenuUnique.PettyCash).ConfigureAwait(false);
						var dataTempate = context.TmTemplateApprovals.Where(x => x.MenuId == dataMenu.Id).FirstOrDefault();
						var vm = new Models.ViewModel.TransApprovalHrisVm()
						{
							IdTemplate = dataTempate.Id,
							MenuId = dataMenu.Id,
							RefId = entity.Id,
							DetailLink = $"{ApprovalLink.PettyCash}?Id={entity.Id}"
						};
						var subGroupId = Phoenix.Shared.Core.PrincipalHelpers.ClainJwtPrincipalHelper.GetBusinessSubGroup(context.HttpContext);
						var divisionId = Phoenix.Shared.Core.PrincipalHelpers.ClainJwtPrincipalHelper.GetBusinessUnitDivisi(context.HttpContext);
						var employeeId = Phoenix.Shared.Core.PrincipalHelpers.ClainJwtPrincipalHelper.GetEmployeeId(context.HttpContext);
						isApprove = await globalFcApproval.Save(vm, subGroupId, divisionId, employeeId);
						if (isApprove)
						{
							var save = await context.SaveChangesAsync();
							transaction.Commit();
							return save;
						}
						else
						{
							transaction.Rollback();
							throw new Exception("please check the approval template that will be processed");
						}
					}
					else
					{
						context.PettyCashs.Update(entity);
						var save = await context.SaveChangesAsync();
						transaction.Commit();
						return save;
					}

					//var save = await context.SaveChangesAsync();
					//transaction.Commit();
					//return save;
				}
				catch (Exception ex)
				{
					transaction.Rollback();
					throw new Exception(ex.Message);
				}
			}
		}

		public async Task<List<Journal>> GetJournal(string Id, string rfn)
		{
			//var result = await (
			//	from a in context.Journal
			//	join b in context.LegalEntity on a.LegalId equals b.Id
			//	join c in context.Affiliations on a.AffiliateId equals c.Id
			//	join d in context.BusinessUnits on a.DivisiId equals d.Id
			//	join e in context.ChartOfAccounts on a.AccountId equals e.Id into co from coa in co.DefaultIfEmpty()
			//	join f in context.JobDetail on a.JobId equals f.Id into jd from job in jd.DefaultIfEmpty()
			//	where !(a.IsDeleted ?? false) && a.ReferenceId == Id
			//	join g in context.PcaTasks
			//	select new Journal
			//	{
			//		AccountId = a.AccountId,
			//		AccountName = coa.Name5,
			//		ReferenceNumber = a.ReferenceNumber,
			//		Description = a.Description,
			//		Debit = a.Debit,
			//		Credit = a.Credit,
			//		LegalEntityName = b.LegalEntityName,
			//		DivisiName = d.UnitName,
			//		AfiliationName = c.AffiliationName,
			//		CodeRec = coa.CodeRec,
			//		CreatedOn = a.CreatedOn,
			//		PceId = a.PceId,
			//		PcaId = a.PcaId,
			//		JobId = a.JobId,
			//		JobIdName = job.JobName
			//	}).OrderByDescending(x => x.CreatedOn).ToListAsync();
			//return result;

			var result = await (
				from a in context.Journal
				join e in context.ChartOfAccounts on a.AccountId equals e.Id into co
				from coa in co.DefaultIfEmpty()
				join f in context.JobDetail on a.JobId equals f.Id into jd
				from job in jd.DefaultIfEmpty()
				where !(a.IsDeleted ?? false) && a.ReferenceId == Id
				join g in context.PcaTasks on a.PcaTaskId equals g.Id into pt
				from pc in pt.DefaultIfEmpty()
				select new Journal
				{
					TransactionDate = a.TransactionDate,
					AccountId = a.AccountId,
					AccountName = coa.Name5,
					ReferenceNumber = a.ReferenceNumber,
					Description = a.Description,
					Debit = a.Debit,
					Credit = a.Credit,
					CodeRec = coa.CodeRec,
					CreatedOn = a.CreatedOn,
					PceId = a.PceId,
					PcaId = a.PcaId,
					JobId = a.JobId,
					JobIdName = job.JobName,
					PcaTaskName = pc.TaskName
				}).OrderByDescending(x => x.CreatedOn).ToListAsync();
			return result;
		}

		public async Task<int> ApproveAsync(NewPettyCash entity)
        {
            await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.SettlementDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.PCS);
            using (var transaction = await context.Database.BeginTransactionAsync())
			{
				try
				{
					var save = await globalFcApproval.UnsafeProcessApproval(entity.Id, entity.TrnStatus, entity.Remarks, entity);
					var approval = context.TrTemplateApprovals.Where(x => x.RefId == entity.Id && !(x.IsDeleted ?? false)).FirstOrDefault();
					if (approval != null)
					{
						entity.Status = PettyCashStatus.Settle;
					}
					else
					{
						entity.Status = PettyCashStatus.Open;
					}
					context.PettyCashs.Update(entity);

					save = await context.SaveChangesAsync();
					transaction.Commit();
					return save;
				}
				catch (Exception ex)
				{
					transaction.Rollback();
					throw new Exception(ex.Message);
				}
			}
		}

		#region not_implemented
		public Task<int> AddRangeAsync(params NewPettyCash[] entities)
		{
			throw new NotImplementedException();
		}

		public Task<int> DeleteAsync(NewPettyCash entity)
		{
			throw new NotImplementedException();
		}

		public Task<int> DeleteRageAsync(params NewPettyCash[] entities)
		{
			throw new NotImplementedException();
		}

		public async Task<int> DeleteSoftAsync(NewPettyCash entity)
		{
			await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.SettlementDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.PCS);
			context.PhoenixDelete(entity);
			return await context.SaveChangesAsync();
		}

		public Task<int> DeleteSoftRangeAsync(params NewPettyCash[] entities)
		{
			throw new NotImplementedException();
		}

		public Task<int> EditRangeAsync(NewPettyCash entity)
		{
			throw new NotImplementedException();
		}

		public Task<List<NewPettyCash>> Get(int skip, int take)
		{
			throw new NotImplementedException();
		}

		public Task<List<NewPettyCash>> Get()
		{
			throw new NotImplementedException();
		}

		Task<List<NewPettyCash>> IDataService<NewPettyCash>.Get(int skip, int take)
		{
			throw new NotImplementedException();
		}

		public Task<NewPettyCash> Get(string id)
		{
			throw new NotImplementedException();
		}

		#endregion not_implemented
	}
}
