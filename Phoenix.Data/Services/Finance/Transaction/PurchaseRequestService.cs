﻿using Phoenix.Data.Models.Finance;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Phoenix.Data.Models;
using Phoenix.Data.Attributes;
using Phoenix.Data.Services.Um;

namespace Phoenix.Data.Services.Finance.Transaction
{
    public interface IPurchaseRequestService
    {
        Task<int> AddAsync(PurchaseRequest entity);
        Task<int> EditAsync(PurchaseRequest entity);
        Task<int> DeleteSoftAsync(PurchaseRequest entity);
        Task<int> ApproveAsync(PurchaseRequest entity);
        Task<List<PurchaseRequest>> GetPRList(string status);
        Task<PurchaseRequest> Get(string Id);
        Task<List<PurchaseRequestDetail>> GetDetail(string Id);
        Task<List<Budget>> GetBudgetByDivisionId(string BusinessUnitId, string CoaId);
    }

    public class PurchaseRequestService : IPurchaseRequestService
    {
        readonly GlobalFunctionApproval globalFcApproval;
        readonly IManageMenuService menuService;
        readonly DataContext context;
        readonly StatusLogService log;

        public PurchaseRequestService(DataContext context, GlobalFunctionApproval globalFcApproval, IManageMenuService menuService, StatusLogService log)
        {
            this.context = context;
            this.globalFcApproval = globalFcApproval;
            this.menuService = menuService;
            this.log = log;
        }

        public async Task<int> AddAsync(PurchaseRequest entity)
        {
            return await AddEdit(entity, false);
        }

        public async Task<int> EditAsync(PurchaseRequest entity)
        {
            return await AddEdit(entity, true);
        }

        private async Task<int> AddEdit(PurchaseRequest entity, bool isEdit)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var AffiliationId = await context.BusinessUnits.AsNoTracking().Where(x => !(x.IsDeleted ?? false) && (x.Isfinance ?? false) && x.Id == entity.BusinessUnitId).Select(x => x.AffiliationId).FirstOrDefaultAsync();

                    entity.AffiliationId = AffiliationId;
                    entity.Status = StatusTransaction.StatusName(entity.StatusApproval);

                    if (isEdit)
                    {
                        context.PhoenixEdit(entity);
                    }
                    else
                    {
                        entity.Id = await TransID.GetTransId(context, Code.PR, DateTime.Today);
                        entity.RequestNumber = entity.Id;
                        await context.PhoenixAddAsync(entity);
                        await context.SaveChangesAsync();
                    }

                    if (!string.IsNullOrEmpty(entity.Id))
                    {
                        var detail = context.PurchaseRequestDetail.AsNoTracking().Where(x => x.PurchaseRequestId == entity.Id).ToList();
                        if (detail.Count > 0)
                        {
                            context.PurchaseRequestDetail.RemoveRange(detail);
                        }
                    }

                    if (entity.PurchaseRequestDetails != null)
                    {
                        Parallel.ForEach(entity.PurchaseRequestDetails, (item) =>
                        {
                            item.Id = Guid.NewGuid().ToString();
                            item.PurchaseRequestId = entity.Id;
                            context.PhoenixAddAsync(item);
                        });
                    }

                    var isApprove = true;
                    if (entity.Status != StatusTransactionName.Draft)
                    {
                        var dataMenu = await menuService.GetByUniqueName(MenuUnique.PurchaseRequest).ConfigureAwait(false);
                        var vm = new Models.ViewModel.TransApprovalHrisVm()
                        {
                            MenuId = dataMenu.Id,
                            RefId = entity.Id,
                            DetailLink = $"{ApprovalLink.PurchaseRequest}?Id={entity.Id}"
                        };
                        var subGroupId = Phoenix.Shared.Core.PrincipalHelpers.ClainJwtPrincipalHelper.GetBusinessSubGroup(context.HttpContext);
                        var divisionId = Phoenix.Shared.Core.PrincipalHelpers.ClainJwtPrincipalHelper.GetBusinessUnitDivisi(context.HttpContext);
                        var employeeId = Phoenix.Shared.Core.PrincipalHelpers.ClainJwtPrincipalHelper.GetEmployeeId(context.HttpContext);
                        isApprove = await globalFcApproval.Save(vm, subGroupId, divisionId, employeeId);
                        if (isApprove)
                        {
                            await log.AddAsync(new StatusLog() { TransactionId = entity.Id, Status = entity.Status, Description = entity.RemarksProcess });
                            var save = await context.SaveChangesAsync();
                            transaction.Commit();
                            return save;
                        }
                        else
                        {
                            transaction.Rollback();
                            throw new Exception("please check the approval template that will be processed");
                        }
                    }
                    else
                    {
                        entity.Status = StatusTransactionName.Draft;
                        context.PurchaseRequest.Update(entity);
                        await log.AddAsync(new StatusLog() { TransactionId = entity.Id, Status = entity.Status, Description = entity.RemarksProcess });
                        var save = await context.SaveChangesAsync();
                        transaction.Commit();
                        return save;
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<int> ApproveAsync(PurchaseRequest entity)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var save = await globalFcApproval.UnsafeProcessApproval(entity.Id, entity.StatusApproval, entity.RemarksProcess, entity);
                    var approval = context.TrTemplateApprovals.Where(x => x.RefId == entity.Id && !(x.IsDeleted ?? false)).FirstOrDefault();
                    if (approval != null)
                    {
                        entity.Status = StatusTransaction.StatusName(approval.StatusApproved.GetHashCode());
                        if (approval.StatusApproved == Models.Um.StatusApproved.Approved)
                        {
                            if (context.RequestForQuotation.AsNoTracking().Where(x => x.PurchaseRequestId == entity.Id).Count() == 0)
                            {
                                var rfqId = await TransID.GetTransId(context, Code.RFQ, DateTime.Today);
                                var RFQ = new RequestForQuotation()
                                {
                                    Id = rfqId,
                                    RequestForQuotationNumber = rfqId,
                                    PurchaseRequestId = entity.Id,
                                    AffiliationId = entity.AffiliationId,
                                    LegalEntityId = entity.LegalEntityId,
                                    BusinessUnitId = entity.BusinessUnitId,
                                    RequestForQuotationDate = DateTime.Now,
                                    Status = StatusTransactionName.Draft,
                                    MainserviceCategoryId = MainServiceCategoryOther.MainServiceCategoryId,
                                    //IsPreferedVendor = entity.IsPrefered,
                                    DueDate = entity.DueDate,
                                    CurrencyId = CurrencyIDR.CurrencyId,
                                    //TypeOfExpenseId = entity.TypeOfExpenseId,
                                    Remarks = entity.RemarksProcess,
                                    CreatedBy = entity.CreatedBy,
                                    CreatedOn = DateTime.Now,
                                    ModifiedBy = context.GetEmployeeId(),
                                    ModifiedOn = DateTime.Now,
                                    InternalOrProject = "Internal"
                                };
                                await context.RequestForQuotation.AddAsync(RFQ);
                                await context.SaveChangesAsync();


                                var detail = await context.PurchaseRequestDetail.AsNoTracking().Where(x => x.PurchaseRequestId == entity.Id).ToListAsync();
                                Parallel.ForEach(detail, (item) =>
                                {
                                    var RFQTask = new RequestForQuotationTask
                                    {
                                        Id = Guid.NewGuid().ToString(),
                                        RequestForQuotationId = RFQ.Id,
                                        Task = item.Name,
                                        TaskId = item.Id,
                                        //JobId = entity.JobId,
                                        ItemTypeId = item.ItemTypeId,
                                        ItemId = item.ItemId,
                                        ItemCode = item.ItemCode,
                                        ItemName = item.ItemName,
                                        Quantity = item.Qty,
                                        UomId = item.UomId,
                                        UnitPrice = item.UnitPrice,
                                        TotalPrice = item.UnitPrice * item.Qty,
                                        CreatedBy = entity.CreatedBy,
                                        CreatedOn = DateTime.Now,
                                        ModifiedBy = context.GetEmployeeId(),
                                        ModifiedOn = DateTime.Now
                                    };
                                    context.RequestForQuotationTask.Add(RFQTask);
                                });
                            }
                        }
                    }
                    else
                    {
                        entity.Status = StatusTransactionName.Draft;
                    }
                    context.PurchaseRequest.Update(entity);

                    await log.AddAsync(new StatusLog() { TransactionId = entity.Id, Status = entity.Status, Description = entity.RemarksProcess });
                    save = await context.SaveChangesAsync();
                    transaction.Commit();
                    return save;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<int> DeleteSoftAsync(PurchaseRequest entity)
        {
            context.PhoenixDelete(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<List<PurchaseRequest>> GetPRList(string status)
        {
            var businessUnit = await (from buj in context.BusinessUnitJobLevels
                                      join bu in context.BusinessUnits on buj.BusinessUnitId equals bu.Id
                                      join but in context.BusinessUnitTypes on bu.BusinessUnitTypeId equals but.Id
                                      where but.BusinessUnitLevel == BusinessUnitLevelCode.Division && (bu.Ishris ?? false)
                                      select new { BusinessUnitJobLevelId = buj.Id, bu.UnitName }).ToListAsync();

            var model = await (from tb in context.PurchaseRequest
                               join bu in context.BusinessUnits on tb.BusinessUnitId equals bu.Id
                               join but in context.BusinessUnitTypes on bu.BusinessUnitTypeId equals but.Id
                               join usr in context.EmployeeBasicInfos on tb.CreatedBy equals usr.Id
                               where !(tb.IsDeleted ?? false) && but.BusinessUnitLevel == BusinessUnitLevelCode.Division && (bu.Isfinance ?? false) &&
                               (string.IsNullOrEmpty(status) ? true : (tb.Status ?? StatusTransactionName.Draft) == status)
                               select new PurchaseRequest
                               {
                                   Id = tb.Id,
                                   RequestNumber = tb.RequestNumber,
                                   RequestDatetime = tb.RequestDatetime,
                                   BusinessUnitName = bu.UnitName,
                                   Requestor = usr.NameEmployee,
                                   Status = tb.Status,
                                   RequestorDivisionName = businessUnit.Where(x => x.BusinessUnitJobLevelId == usr.BusinessUnitJobLevelId).Select(x => x.UnitName).FirstOrDefault(),
                                   CreatedOn = tb.CreatedOn
                               }).OrderByDescending(x => x.CreatedOn).ToListAsync();
            return model;
        }

        public async Task<PurchaseRequest> Get(string Id)
        {
            var result = await context.PurchaseRequest.AsNoTracking().Where(x => !(x.IsDeleted ?? false) && x.Id == Id).FirstOrDefaultAsync();
            var employee = await context.EmployeeBasicInfos.Where(x => x.Id == result.CreatedBy).FirstOrDefaultAsync();
            if (employee != null)
            {
                result.Requestor = employee.NameEmployee;
                var businessUnitId = await (from buj in context.BusinessUnitJobLevels
                                            join bu in context.BusinessUnits on buj.BusinessUnitId equals bu.Id
                                            join but in context.BusinessUnitTypes on bu.BusinessUnitTypeId equals but.Id
                                            where but.BusinessUnitLevel == BusinessUnitLevelCode.Division &&
                                            buj.Id == employee.BusinessUnitJobLevelId && (bu.Ishris ?? false)
                                            select bu.Id).Select(x => x).FirstOrDefaultAsync();
                result.RequestorDivisionId = businessUnitId;
            }
            return result;
        }

        public async Task<List<PurchaseRequestDetail>> GetDetail(string Id)
        {
            var result = await (from a in context.PurchaseRequestDetail
                                join b in context.Uom on a.UomId equals b.Id
                                join c in context.ItemType on a.ItemTypeId equals c.Id
                                where a.PurchaseRequestId == Id && !(a.IsDeleted ?? false)
                                select new PurchaseRequestDetail
                                {
                                    Id = a.Id,
                                    PurchaseRequestId = a.PurchaseRequestId,
                                    ItemTypeId = a.ItemTypeId,
                                    ItemId = a.ItemId,
                                    ItemCode = a.ItemCode,
                                    ItemName = a.ItemName,
                                    Name = a.Name,
                                    UomId = a.UomId,
                                    Qty = a.Qty,
                                    UnitPrice = a.UnitPrice,
                                    Amount = a.Amount,
                                    ItemTypeName = c.TypeName,
                                    UomName = b.unit_symbol,
                                    CreatedBy = a.CreatedBy,
                                    CreatedOn = a.CreatedOn
                                }).ToListAsync();
            return result;
        }

        public async Task<List<Budget>> GetBudgetByDivisionId(string BusinessUnitId, string CoaId)
        {
            var model = await (from tb in context.Budget
                               join coa in context.ChartOfAccounts on tb.account_id equals coa.Id
                               where tb.BusinessUnitId == BusinessUnitId && (string.IsNullOrEmpty(CoaId) ? true : tb.account_id == CoaId)
                               orderby tb.revision descending
                               select new Budget
                               {
                                   Id = tb.Id,
                                   CodeRec = coa.CodeRec,
                                   AccountName = coa.Name5,
                                   TotalBudget = tb.budget_01 + tb.budget_02 + tb.budget_03 + tb.budget_04 + tb.budget_05 + tb.budget_06 + tb.budget_07 + tb.budget_08 + tb.budget_09 + tb.budget_10 + tb.budget_11 + tb.budget_12,
                                   Actual = 0,
                                   Balance = tb.budget_01 + tb.budget_02 + tb.budget_03 + tb.budget_04 + tb.budget_05 + tb.budget_06 + tb.budget_07 + tb.budget_08 + tb.budget_09 + tb.budget_10 + tb.budget_11 + tb.budget_12
                               }).ToListAsync();
            return model;
        }
    }
}
