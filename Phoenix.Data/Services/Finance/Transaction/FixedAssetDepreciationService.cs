﻿using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Phoenix.Data.Attributes;

namespace Phoenix.Data.Services.Finance.Transaction
{
    public interface IFixedAssetDepreciationService : IDataService<FixedAssetDepreciation>
    {
        Task<List<FixedAsset>> GetFixedAsset();
        Task<List<FixedAssetDepreciation>> GetByFA(string faId);
        Task<int> Generate(string faId);
        Task<int> EditApprovalAsync(FixedAsset model);
        Task<int> ApproveAsync(FixedAsset model);
    }
    public class FixedAssetDepreciationService : IFixedAssetDepreciationService
    {
        readonly DataContext context;
        readonly GlobalFunctionApproval globalFcApproval;
        string idTemplate = "";

        public FixedAssetDepreciationService(DataContext context, GlobalFunctionApproval globalFcApproval)
        {
            this.context = context;
            this.globalFcApproval = globalFcApproval;
        }

        public async Task<int> AddAsync(FixedAssetDepreciation entity)
        {
            entity.Id = Guid.NewGuid().ToString();
            await context.PhoenixAddAsync(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> AddRangeAsync(params FixedAssetDepreciation[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> ApproveAsync(FixedAsset model)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var save = await globalFcApproval.UnsafeProcessApproval(model.Id, int.Parse(model.StatusDepreciation), model.RemarksDepreciation, model);
                    var approval = context.TrTemplateApprovals.Where(x => x.RefId == model.Id && !(x.IsDeleted ?? false)).FirstOrDefault();
                    if (approval != null)
                    {
                        model.StatusDepreciation = StatusTransaction.StatusName(approval.StatusApproved.GetHashCode());
                        if (approval.StatusApproved == Models.Um.StatusApproved.Approved)
                        {
                            //if ((model.IsClose ?? false))
                            //{
                                model.StatusDepreciation = StatusTransaction.Approved.ToString();
                            //}
                            //else
                            //{
                            //    model.Status = StatusTransactionName.Draft;
                            //}
                        }
                        else if (approval.StatusApproved == Models.Um.StatusApproved.Rejected)
                        {
                            model.StatusDepreciation = StatusTransaction.Draft.ToString();
                        }
                    }
                    else
                    {
                        model.StatusDepreciation = StatusTransaction.Draft.ToString();
                    }
                    context.PhoenixEdit(model);
                    save = await context.SaveChangesAsync();
                    transaction.Commit();
                    return save;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        public Task<int> DeleteAsync(FixedAssetDepreciation entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteRageAsync(params FixedAssetDepreciation[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteSoftAsync(FixedAssetDepreciation entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteSoftRangeAsync(params FixedAssetDepreciation[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> EditApprovalAsync(FixedAsset model)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    if (model.StatusDepreciation != StatusTransaction.Draft.ToString())
                    {
                        await globalFcApproval.UnsafeSubmitApproval<FixedAsset>(true, model.Id, int.Parse(model.Status), $"{ApprovalLink.FixedAssetDepreciation}?Id={model.Id}&isApprove=true", idTemplate, MenuUnique.FixedAssetDepreciation, model);
                        var save = await context.SaveChangesAsync();
                        transaction.Commit();
                        return save;
                    }
                    else
                    {
                        model.StatusDepreciation = StatusTransaction.Draft.ToString();
                        context.PhoenixEdit(model);
                        var save = await context.SaveChangesAsync();
                        transaction.Commit();
                        return save;
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        public Task<int> EditAsync(FixedAssetDepreciation entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> EditRangeAsync(FixedAssetDepreciation entity)
        {
            throw new NotImplementedException();
        }

        public async Task<int> Generate(string faId)
        {
            try
            {
                //await context.ExecuteStoredProcedure("EXECUTE prc_generate_depresiasi @p_fa_id", parameters: new[] { new SqlParameter("@p_fa_id", faId) });
                await context.Database.ExecuteSqlCommandAsync("EXECUTE fn.prc_generate_depresiasi @p_fa_id", parameters: new[] { new SqlParameter("@p_fa_id", faId) });

                var save = await context.SaveChangesAsync();
                return save;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<FixedAssetDepreciation>> Get()
        {
            var query = await context.FixedAssetDepreciation.ToListAsync();
            return query;
        }

        public Task<List<FixedAssetDepreciation>> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public Task<FixedAssetDepreciation> Get(string id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<FixedAssetDepreciation>> GetByFA(string faId)
        {
            return await context.FixedAssetDepreciation.Where(x => x.IsDeleted.Equals(false) && x.FixedAssetID == faId).ToListAsync();
        }

        public async Task<List<FixedAsset>> GetFixedAsset()
        {
            var query = await (from fa in context.FixedAsset
                               join fam in (from f in context.FixedAssetMovement orderby f.FaaDateTime descending select f).Take(1) on fa.Id equals fam.FixedAssetId into left
                               from fam in left.DefaultIfEmpty()
                               join loc in context.MasterLocationProcurement on fam.FromLocationID equals loc.Id into famloc
                               from loc in famloc.DefaultIfEmpty()
                               join con in context.MasterConditionProcurement on fam.Condition equals con.Id into famcon
                               from con in famcon.DefaultIfEmpty()
                               join emp in context.EmployeeBasicInfos on fam.Employee equals emp.Id into famemp
                               from emp in famemp.DefaultIfEmpty()
                               join bu in context.BusinessUnits on fam.BusinessUnitID equals bu.Id into fambu
                               from bu in fambu.DefaultIfEmpty()
                               join le in context.LegalEntity on fam.LegalEntityID equals le.Id into famle
                               from le in famle.DefaultIfEmpty()
                               join fad in context.FixedAssetDepreciation on fa.Id equals fad.FixedAssetID into fafad
                               from fad in fafad.DefaultIfEmpty()
                               select new FixedAsset
                               {
                                   Id = fa.Id,
                                   FixedAssetName = fa.FixedAssetName,
                                   Brand = fa.Brand,
                                   SkuCode = fa.SkuCode,
                                   UnitCount = fa.UnitCount,
                                   CurrentLocation = loc.LocationStore,
                                   Condition = con.ConditionName,
                                   OwnerId = emp.NameEmployee,
                                   BusinessUnit = bu.UnitName,
                                   LegalEntity = le.LegalEntityName,
                                   Status = fad.Status,
                                   StatusDepreciation=fa.StatusDepreciation
                               }).ToListAsync();
            return query;
        }
    }
}
