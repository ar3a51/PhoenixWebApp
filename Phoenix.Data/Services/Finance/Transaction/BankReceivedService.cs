﻿using Phoenix.Data.Models.Finance;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Phoenix.Data.Models;
using Phoenix.Data.Services.Um;
using Phoenix.Data.Attributes;
using Phoenix.Data.Models.Finance.Transaction;
using System.Data.SqlClient;
using Phoenix.Shared.Core.PrincipalHelpers;
using Phoenix.Data.Models.Finance.Transaction.PaymentRequest;
using Phoenix.Data.Models.Finance.MasterData;

namespace Phoenix.Data.Services.Finance
{
    public interface IBankReceivedService : IDataService<BankReceived>
    {
        Task<List<BankReceived>> GetWithFilterAP(PaymentRequestFilter Filter);
        Task<int> ApproveData(BankReceived entity);
        Task<BankReceived> Reject(BankReceived entity);
        Task<BankReceived> Reconsiliation(BankReceived entity);
        Task<Bank> GetAccountNumberData(string Id);
        Task<MasterPettyCash> GetPettycashData(string Id);
        Task<Bank> GetVendorData(string Id);

        Task<List<BankReceived>> GetWithFilterCA(PaymentRequestFilter Filter);
        Task<List<BankReceived>> GetWithFilter(PaymentRequestFilter Filter);
        Task<int> ApproveAsync(BankReceived entity);
        Task<List<PaymentInformationData>> GetPaymentInformationData(string referenceId);
        Task<List<Journal>> GetBankPaymentJournal(string BankPaymentId);
        Task<int> CreateJournalWithSP(string PaymentRequestId);
        Task<List<AccountReceivable>> GetListARPosted(PaymentRequestFilter Filter);
        Task<int> AddWithDetail(BankReceivedDTO entity);
        Task<int> EditWithDetail(BankReceivedDTO entity);
        Task<List<AccountReceivable>> GetListDetailPaymentRequest(string Id);
        Task<List<BankReceivedDetail>> GetDetail(string Id);
    }

    public class BankReceivedService : IBankReceivedService
    {
        readonly DataContext context;
        readonly GlobalFunctionApproval globalFcApproval;
        readonly IManageMenuService menuService;
        readonly FinancePeriodService financePeriod;
        readonly StatusLogService log;
        string idTemplate = "";
        /// <summary>
        /// And endpoint to manage PettyCash
        /// </summary>
        /// <param name="context">Database context</param>
        public BankReceivedService(DataContext context, GlobalFunctionApproval globalFcApproval, IManageMenuService menuService, FinancePeriodService financePeriod, StatusLogService log)
        {
            this.context = context;
            this.globalFcApproval = globalFcApproval;
            this.menuService = menuService;
            this.financePeriod = financePeriod;
            this.log = log;
        }

        public async Task<int> ApproveAsync(BankReceived entity)
        {
            return await globalFcApproval.ProcessApproval<BankReceived>(entity.Id, Int32.Parse(entity.Status), entity.RemarkRejected, entity);
        }


        public async Task<int> ApproveData(BankReceived entity)
        {
            try
            {
                BankReceived model = new BankReceived();
                model = entity;
                entity.RemarkRejected = entity.Remarks;
                if (string.IsNullOrEmpty(entity.RemarkRejected))
                {
                    entity.RemarkRejected = "";
                }
                await globalFcApproval.ProcessApproval<BankReceived>(model.Id, StatusTransaction.Approved, model.RemarkRejected, model);
                var modeltrapp = await context.TrTemplateApprovals.Where(x => x.RefId == entity.Id).FirstOrDefaultAsync();
                if (modeltrapp != null)
                {
                    if (modeltrapp.StatusApprovedDescription == StatusTransaction.StatusName(StatusTransaction.Approved))
                    {
                        var modelbp = await context.BankReceived.Where(x => x.IsDeleted.Equals(false) && x.Id == entity.Id).FirstOrDefaultAsync();
                        if (modelbp != null)
                        {
                            model.Status = PaymentRequestStatus.Complete;

                            var result = await context.Database.ExecuteSqlCommandAsync("EXECUTE fn.create_jurnal_bp @p_bank_payment_id", parameters: new[] { new SqlParameter("@p_bank_payment_id", entity.Id) });
                            if (entity.PayToType == "1")//PETTYCASH
                            {
                                var modelPettycah = await context.MasterPettyCashs.Where(x => x.IsDeleted.Equals(false) && x.Id == entity.PayToPettyCash).FirstOrDefaultAsync();
                                modelPettycah.AmountInsused = (modelPettycah.AmountInsused == null ? 0 : modelPettycah.AmountInsused) + entity.AmountPay;
                                context.PhoenixEdit(modelPettycah);
                            }

                        }
                        else
                        {
                            model.Status = PaymentRequestStatus.WaitingApproval;
                        }
                    }
                    else
                    {
                        model.Status = PaymentRequestStatus.WaitingApproval;
                    }
                }
                else
                {
                    model.Status = PaymentRequestStatus.WaitingApproval;
                }
                await log.AddAsync(new StatusLog() { TransactionId = entity.Id, Status = PaymentRequestStatus.GetStatus(model.Status), Description = entity.LogDescription });
                context.PhoenixEdit(model);
                var save = await context.SaveChangesAsync();
                return save;
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new Exception(ex.Message.ToString());
            }
            catch (DbUpdateException exc)
            {
                throw new Exception(exc.Message.ToString());
            }
        }


        public async Task<BankReceived> Reconsiliation(BankReceived entity)
        {
            try
            {
                entity.Status = PaymentRequestStatus.Reconsiliation;
                context.PhoenixEdit(entity);
                await log.AddAsync(new StatusLog() { TransactionId = entity.Id, Status = PaymentRequestStatus.GetStatus(PaymentRequestStatus.Reconsiliation), Description = entity.LogDescription });
                await context.SaveChangesAsync();
                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
        public async Task<BankReceived> Reject(BankReceived entity)
        {
            try
            {
                await globalFcApproval.ProcessApproval<BankReceived>(entity.Id, Int32.Parse(entity.Status), entity.RemarkRejected, entity);

                var modeltrapp = await context.TrTemplateApprovals.Where(x => x.RefId == entity.Id).FirstOrDefaultAsync();
                if (modeltrapp != null)
                {
                    if (modeltrapp.StatusApprovedDescription == StatusTransaction.StatusName(StatusTransaction.Rejected))
                    {
                        entity.Status = PaymentRequestStatus.Reject;
                        entity.ModifiedOn = DateTime.Now;
                        context.PhoenixEdit(entity);
                    }
                }
                else
                {
                    entity.Status = PaymentRequestStatus.Reject;
                    entity.ModifiedOn = DateTime.Now;
                    context.PhoenixEdit(entity);
                }
                await log.AddAsync(new StatusLog() { TransactionId = entity.Id, Status = PaymentRequestStatus.GetStatus(PaymentRequestStatus.Reject), Description = entity.RemarkRejected });
                await context.SaveChangesAsync();
                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public async Task<int> AddAsync(BankReceived entity)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    //await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.PaymentRequestDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.BP);
                    var isApprove = true;

                    entity.Id = Guid.NewGuid().ToString();
                    //entity.RemarkRejected = entity.Remarks;
                    //entity.TotalAmount = entity.AmountPay;// + entity.Vat;

                    if (string.IsNullOrEmpty(entity.PairingGroupId))
                    {
                        entity.PairingGroupId = Guid.NewGuid().ToString();
                    }

                    await context.PhoenixAddAsync(entity);

                    //approval process request to system

                    var dataMenu = await menuService.GetByUniqueName(MenuUnique.BankReceived).ConfigureAwait(false);
                    var vm = new Models.ViewModel.TransApprovalHrisVm()
                    {
                        MenuId = dataMenu.Id,
                        RefId = entity.Id,
                        DetailLink = $"{ApprovalLink.BankReceived}?Id={entity.Id}&isApprove=true",
                        //Tname = "fn.PurchaseRequest",//"SP#NAMA SP NYA",
                        IdTemplate = idTemplate
                    };
                    var subGroupId = ClainJwtPrincipalHelper.GetBusinessSubGroup(context.HttpContext);
                    var divisionId = ClainJwtPrincipalHelper.GetBusinessUnitDivisi(context.HttpContext);
                    var employeeId = ClainJwtPrincipalHelper.GetEmployeeId(context.HttpContext);
                    isApprove = await globalFcApproval.Save(vm, subGroupId, divisionId, employeeId);


                    if (isApprove)
                    {
                        var save = await context.SaveChangesAsync();
                        transaction.Commit();
                        return save;
                    }
                    else
                    {
                        transaction.Rollback();
                        throw new Exception("please check the approval template that will be processed");
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message.ToString());
                }
            }

        }

        public async Task<int> AddRangeAsync(params BankReceived[] entities)
        {
            await context.PhoenixAddRangeAsync(entities);
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteAsync(BankReceived entity)
        {
            context.PhoenixDelete(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteRageAsync(params BankReceived[] entities)
        {
            context.PhoenixDeleteRange(entities);
            return await context.SaveChangesAsync();

        }

        public async Task<int> DeleteSoftAsync(BankReceived entity)
        {
            context.PhoenixDelete(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteSoftRangeAsync(params BankReceived[] entities)
        {
            context.PhoenixDeleteRange(entities);
            return await context.SaveChangesAsync();
        }

        public async Task<int> EditAsync(BankReceived entity)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.PaymentRequestDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.BP);
                    //entity.Remarks = entity.RemarkRejected;
                    //entity.TotalAmount = entity.AmountPay;// + entity.Vat;
                    context.PhoenixEdit(entity);

                    var trapprovalmodel = await context.TrTemplateApprovals.Where(x => x.RefId == entity.Id).FirstOrDefaultAsync();
                    if (trapprovalmodel == null)
                    {
                        var isApprove = true;
                        var dataMenu = await menuService.GetByUniqueName(MenuUnique.BankReceived).ConfigureAwait(false);
                        var vm = new Models.ViewModel.TransApprovalHrisVm()
                        {
                            MenuId = dataMenu.Id,
                            RefId = entity.Id,
                            DetailLink = $"{ApprovalLink.BankReceived}?Id={entity.Id}&isApprove=true",
                            //Tname = "fn.PurchaseRequest",//"SP#NAMA SP NYA",
                            IdTemplate = idTemplate
                        };
                        var subGroupId = ClainJwtPrincipalHelper.GetBusinessSubGroup(context.HttpContext);
                        var divisionId = ClainJwtPrincipalHelper.GetBusinessUnitDivisi(context.HttpContext);
                        var employeeId = ClainJwtPrincipalHelper.GetEmployeeId(context.HttpContext);
                        isApprove = await globalFcApproval.Save(vm, subGroupId, divisionId, employeeId);


                        if (isApprove)
                        {
                            var save = await context.SaveChangesAsync();
                            transaction.Commit();
                            return save;
                        }
                        else
                        {
                            transaction.Rollback();
                            throw new Exception("please check the approval template that will be processed");
                        }

                    }
                    else
                    {

                        var save = await context.SaveChangesAsync();
                        transaction.Commit();
                        return save;
                    }

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message.ToString());
                }
            }

        }

        public async Task<int> EditRangeAsync(BankReceived entity)
        {
            context.PhoenixEditRange(entity);
            return await context.SaveChangesAsync();

        }

        public async Task<List<BankReceived>> Get()
        {
            var model = await (from tb in context.BankReceived
                               join inv in context.InvoiceReceivables on tb.InvoiceReceivableId equals inv.Id
                               join bu in context.BusinessUnits on tb.BusinessUnitId equals bu.Id
                               join le in context.LegalEntity on tb.LegalEntityId equals le.Id into leg
                               from legg in leg.DefaultIfEmpty()
                               where tb.IsDeleted.Equals(false)
                               select new BankReceived
                               {
                                   Id = tb.Id,
                                   InvoiceReceivableId = tb.InvoiceReceivableId,
                                   InvoiceReceivableNumber = inv.InvoiceNumber,
                                   AccountId = tb.AccountId,
                                   AdvertisingTax = tb.AdvertisingTax,
                                   AffilitionId = tb.AffilitionId,
                                   BusinessUnitId = tb.BusinessUnitId,
                                   BusinessUnitName = bu.UnitName,
                                   Amount = tb.Amount,
                                   BankName = tb.BankName,
                                   CurrencyId = tb.CurrencyId,
                                   Discount = tb.Discount,
                                   DueDate = tb.DueDate,
                                   ExchangeRate = tb.ExchangeRate,
                                   LegalEntityId = tb.LegalEntityId,
                                   LegalEntityName = legg.LegalEntityName,
                                   PaymentRequestDate = tb.PaymentRequestDate,
                                   PaymentRequestNumber = tb.PaymentRequestNumber,
                                   PaymentTypeId = tb.PaymentTypeId,
                                   TaxPayable = tb.TaxPayable,
                                   TaxPayablePercent = tb.TaxPayablePercent,
                                   Status = tb.Status,
                                   MainServiceCategoryId = tb.MainServiceCategoryId,
                                   SourceDoc = tb.SourceDoc,
                                   PayToVendor = tb.PayToVendor,
                                   BankId = tb.BankId,
                                   AccountNumber = tb.AccountNumber,
                                   AmountPay = tb.AmountPay,
                                   ReferenceNumberId = string.IsNullOrEmpty(tb.ReferenceNumberId) ? "" : tb.ReferenceNumberId,
                                   Vat = tb.Vat,
                                   VatPercent = tb.VatPercent,
                                   TotalAmount = tb.TotalAmount,
                                   PaytoName = tb.PaytoName,
                                   JobName = tb.JobId == null ? tb.PurchaseRequestId : tb.JobId,
                                   IsBalance = tb.IsBalance
                               }).ToListAsync();
            return model;
        }


        public Task<List<BankReceived>> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public async Task<BankReceived> Get(string Id)
        {
            try
            {
                return await (from tb in context.BankReceived
                              join b in context.LegalEntity on tb.LegalEntityId equals b.Id into gb
                              from bb in gb.DefaultIfEmpty()
                              where tb.IsDeleted.Equals(false) && tb.Id == Id
                              select new BankReceived
                              {
                                  Id = tb.Id,
                                  InvoiceReceivableId = tb.InvoiceReceivableId,
                                  InvoiceReceivableNumber = tb.InvoiceReceivableNumber,
                                  AccountId = tb.AccountId,
                                  AdvertisingTax = tb.AdvertisingTax,
                                  AffilitionId = tb.AffilitionId,
                                  BusinessUnitId = tb.BusinessUnitId,
                                  Amount = tb.Amount,
                                  BankName = tb.BankName,
                                  CurrencyId = tb.CurrencyId,
                                  Discount = tb.Discount,
                                  DueDate = tb.DueDate,
                                  BankId = tb.BankId,
                                  MainServiceCategoryId = tb.MainServiceCategoryId,
                                  SourceDoc = tb.SourceDoc,
                                  PayToVendor = tb.PayToVendor,
                                  AccountNumber = tb.AccountNumber,
                                  ExchangeRate = tb.ExchangeRate,
                                  LegalEntityId = tb.LegalEntityId,
                                  LegalEntityName = bb.LegalEntityName,
                                  PaymentRequestDate = tb.PaymentRequestDate,
                                  PaymentRequestNumber = tb.PaymentRequestNumber,
                                  PaymentTypeId = tb.PaymentTypeId,
                                  TaxPayable = tb.TaxPayable,
                                  TaxPayablePercent = tb.TaxPayablePercent,
                                  Status = tb.Status,
                                  AmountPay = tb.AmountPay,
                                  ReferenceNumberId = tb.ReferenceNumberId,
                                  Vat = tb.Vat,
                                  VatPercent = tb.VatPercent,
                                  TotalAmount = tb.TotalAmount,
                                  PaytoName = tb.PaytoName,
                                  JobName = tb.JobId == null ? tb.PurchaseRequestId : tb.JobId,
                                  PairingGroupId = tb.PairingGroupId,
                                  Balance = tb.Balance,
                                  BankChargesCoaId = tb.BankChargesCoaId,
                                  Charges = tb.Charges,
                                  Remarks = tb.Remarks,
                                  JobId = tb.JobId,
                                  ApprovedBy = tb.ApprovedBy,
                                  ApprovedOn = tb.ApprovedOn,
                                  CreatedBy = tb.CreatedBy,
                                  CreatedOn = tb.CreatedOn,
                                  DeletedBy = tb.DeletedBy,
                                  DeletedOn = tb.DeletedOn,
                                  IsActive = tb.IsActive,
                                  IsDefault = tb.IsDefault,
                                  IsDeleted = tb.IsDeleted,
                                  IsLocked = tb.IsLocked,
                                  IsMultiple = tb.IsMultiple,
                                  ModifiedBy = tb.ModifiedBy,
                                  ModifiedOn = tb.ModifiedOn,
                                  OwnerId = tb.OwnerId,
                                  PaytoBank = tb.PaytoBank,
                                  PaytoCoaId = tb.PaytoCoaId,
                                  PurchaseRequestId = tb.PurchaseRequestId,
                                  Recon = tb.Recon,
                                  RemarkRejected = tb.RemarkRejected,
                                  TaxPayCoaId = tb.TaxPayCoaId,
                                  PayToAccountNumber = tb.PayToAccountNumber,
                                  PayToPettyCash = tb.PayToPettyCash,
                                  PayToType = tb.PayToType,
                                  TotalAmountPay = tb.TotalAmountPay,
                                  TaxPayableCoa = tb.TaxPayableCoa
                              }).FirstOrDefaultAsync();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
        }

        public async Task<List<BankReceived>> GetWithFilterAP(PaymentRequestFilter Filter)
        {
            var model = await (from tb in context.BankReceived
                               join inv in context.InvoiceReceivables on tb.InvoiceReceivableId equals inv.Id
                               join bu in context.BusinessUnits on tb.BusinessUnitId equals bu.Id
                               join le in context.LegalEntity on tb.LegalEntityId equals le.Id into leg
                               from legg in leg.DefaultIfEmpty()
                               where tb.IsDeleted.Equals(false) && !string.IsNullOrEmpty(tb.InvoiceReceivableId)
                               select new BankReceived
                               {
                                   Id = tb.Id,
                                   InvoiceReceivableId = tb.InvoiceReceivableId,
                                   InvoiceReceivableNumber = inv.InvoiceNumber,
                                   AccountId = tb.AccountId,
                                   AdvertisingTax = tb.AdvertisingTax,
                                   AffilitionId = tb.AffilitionId,
                                   BusinessUnitId = tb.BusinessUnitId,
                                   BusinessUnitName = bu.UnitName,
                                   Amount = tb.Amount,
                                   BankName = tb.BankName,
                                   CurrencyId = tb.CurrencyId,
                                   Discount = tb.Discount,
                                   DueDate = tb.DueDate,
                                   BankId = tb.BankId,
                                   MainServiceCategoryId = tb.MainServiceCategoryId,
                                   SourceDoc = tb.SourceDoc,
                                   PayToVendor = tb.PayToVendor,
                                   AccountNumber = tb.AccountNumber,
                                   ExchangeRate = tb.ExchangeRate,
                                   LegalEntityId = tb.LegalEntityId,
                                   LegalEntityName = legg.LegalEntityName,
                                   PaymentRequestDate = tb.PaymentRequestDate,
                                   PaymentRequestNumber = tb.PaymentRequestNumber,
                                   PaymentTypeId = tb.PaymentTypeId,
                                   TaxPayable = tb.TaxPayable,
                                   TaxPayablePercent = tb.TaxPayablePercent,
                                   Status = tb.Status,
                                   AmountPay = tb.AmountPay,
                                   ReferenceNumberId = tb.ReferenceNumberId,
                                   Vat = tb.Vat,
                                   VatPercent = tb.VatPercent,
                                   TotalAmount = tb.TotalAmount,
                                   PaytoName = tb.PaytoName,
                                   JobName = tb.JobId == null ? tb.PurchaseRequestId : tb.JobId

                               }).ToListAsync();

            List<BankReceived> pay = new List<BankReceived>();
            pay = model;

            if (Filter.isBankPayment)
            {
                pay = model.Where(x => Convert.ToInt32(x.Status) >= Convert.ToInt32(PaymentRequestStatus.Approved)).ToList();
            }
            else
            {
                pay = model.Where(x => Convert.ToInt32(x.Status) < Convert.ToInt32(PaymentRequestStatus.Approved)).ToList();
            }

            if (!string.IsNullOrEmpty(Filter.FilterStatus))
            {
                pay = model.Where(x => x.Status == Filter.FilterStatus).ToList();
            }

            if (!string.IsNullOrEmpty(Filter.FilterBusinessUnitId))
            {
                pay = model.Where(x => x.BusinessUnitId == Filter.FilterBusinessUnitId).ToList();
            }


            if (!string.IsNullOrEmpty(Filter.FilterLegalEntity))
            {
                pay = model.Where(x => x.LegalEntityId == Filter.FilterLegalEntity).ToList();
            }

            if (!string.IsNullOrEmpty(Filter.FilterReferenceNumber))
            {
                pay = model.Where(x => x.ReferenceNumberId == Filter.FilterReferenceNumber).ToList();
            }

            if (!string.IsNullOrEmpty(Filter.FilterInvoice))
            {
                var datainv = await context.InvoiceReceivables.Where(y => y.InvoiceNumber == Filter.FilterInvoice).FirstOrDefaultAsync();
                if (!string.IsNullOrEmpty(datainv.Id))
                {
                    pay = model.Where(x => x.InvoiceReceivableId == datainv.Id).ToList();
                }
            }

            if (Filter.FilterStartDate != null && Filter.FilterEndDate != null)
            {

                pay = model.Where(y => y.PaymentRequestDate >= Convert.ToDateTime(Filter.FilterStartDate) && y.PaymentRequestDate <= Convert.ToDateTime(Filter.FilterEndDate)).ToList();
            }

            return pay;
        }

        public async Task<List<BankReceived>> GetWithFilterCA(PaymentRequestFilter Filter)
        {
            var model = await (from tb in context.BankReceived
                               join bu in context.BusinessUnits on tb.BusinessUnitId equals bu.Id
                               join le in context.LegalEntity on tb.LegalEntityId equals le.Id
                               where tb.IsDeleted.Equals(false) && string.IsNullOrEmpty(tb.InvoiceReceivableId)
                               select new BankReceived
                               {
                                   Id = tb.Id,
                                   InvoiceReceivableId = tb.InvoiceReceivableId,
                                   InvoiceReceivableNumber = "",
                                   AccountId = tb.AccountId,
                                   AdvertisingTax = tb.AdvertisingTax,
                                   AffilitionId = tb.AffilitionId,
                                   BusinessUnitId = tb.BusinessUnitId,
                                   BusinessUnitName = bu.UnitName,
                                   Amount = tb.Amount,
                                   BankName = tb.BankName,
                                   CurrencyId = tb.CurrencyId,
                                   Discount = tb.Discount,
                                   BankId = tb.BankId,
                                   MainServiceCategoryId = tb.MainServiceCategoryId,
                                   SourceDoc = tb.SourceDoc,
                                   PayToVendor = tb.PayToVendor,
                                   AccountNumber = tb.AccountNumber,
                                   DueDate = tb.DueDate,
                                   Status = tb.Status,
                                   ExchangeRate = tb.ExchangeRate,
                                   LegalEntityId = tb.LegalEntityId,
                                   LegalEntityName = le.LegalEntityName,
                                   PaymentRequestDate = tb.PaymentRequestDate,
                                   PaymentRequestNumber = tb.PaymentRequestNumber,
                                   PaymentTypeId = tb.PaymentTypeId,
                                   TaxPayable = tb.TaxPayable,
                                   TaxPayablePercent = tb.TaxPayablePercent,
                                   ReferenceNumberId = tb.ReferenceNumberId,
                                   Vat = tb.Vat,
                                   AmountPay = tb.AmountPay,
                                   VatPercent = tb.VatPercent,
                                   TotalAmount = tb.TotalAmount,
                                   PaytoName = tb.PaytoName,
                                   JobName = tb.JobId == null ? tb.PurchaseRequestId : tb.JobId
                               }).ToListAsync();

            List<BankReceived> pay = new List<BankReceived>();
            pay = model;

            if (!string.IsNullOrEmpty(Filter.FilterStatus))
            {
                pay = model.Where(x => x.Status == Filter.FilterStatus).ToList();
            }

            if (!string.IsNullOrEmpty(Filter.FilterBusinessUnitId))
            {
                pay = model.Where(x => x.BusinessUnitId == Filter.FilterBusinessUnitId).ToList();
            }

            if (!string.IsNullOrEmpty(Filter.FilterLegalEntity))
            {
                pay = model.Where(x => x.LegalEntityId == Filter.FilterLegalEntity).ToList();
            }

            if (!string.IsNullOrEmpty(Filter.FilterReferenceNumber))
            {
                pay = model.Where(x => x.ReferenceNumberId == Filter.FilterReferenceNumber).ToList();
            }

            if (!string.IsNullOrEmpty(Filter.FilterInvoice))
            {
                var datainv = await context.InvoiceReceivables.Where(y => y.InvoiceNumber == Filter.FilterInvoice).FirstOrDefaultAsync();
                if (!string.IsNullOrEmpty(datainv.Id))
                {
                    pay = model.Where(x => x.InvoiceReceivableId == datainv.Id).ToList();
                }
            }

            if (Filter.FilterStartDate != null && Filter.FilterEndDate != null)
            {

                pay = model.Where(y => y.PaymentRequestDate >= Convert.ToDateTime(Filter.FilterStartDate) && y.PaymentRequestDate <= Convert.ToDateTime(Filter.FilterEndDate)).ToList();
            }

            return model.ToList();
        }

        public async Task<List<PaymentInformationData>> GetPaymentInformationData(string referenceId)
        {
            var model = await (from tb in context.BankReceived
                               join su in context.InvoiceReceivables on tb.InvoiceReceivableId equals su.Id
                               where tb.IsDeleted.Equals(false) && tb.ReferenceNumberId == referenceId && tb.Status == PaymentRequestStatus.PayBalance
                               select new PaymentInformationData
                               {
                                   Id = tb.Id,
                                   Amount = tb.TotalAmountPay,
                                   Billing = su.TotalCostVendor,
                                   Charges = tb.Charges == null ? 0 : tb.Charges,
                                   EvidenceNo = tb.ReferenceNumberId,
                                   Discount = tb.Discount == null ? 0 : tb.Discount,
                                   TaxPayable = tb.TaxPayable == null ? 0 : tb.TaxPayable,
                                   TaxPayablePercent = tb.TaxPayablePercent == null ? 0 : tb.TaxPayablePercent,
                                   Vat = tb.Vat == null ? 0 : tb.Vat,
                                   VatPercent = tb.VatPercent == null ? 0 : tb.VatPercent,
                                   Paid = su.TotalCostVendor - tb.AmountPay,
                                   Recon = tb.Recon == null ? false : tb.Recon
                               }).ToListAsync();

            if (model == null)
            {
                model = new List<PaymentInformationData>();
                var model2 = await (from tb in context.BankReceived
                                    join su in context.CashAdvances on tb.ReferenceNumberId equals su.Id
                                    join de in context.CashAdvanceDetails on su.Id equals de.CashAdvanceId
                                    where tb.IsDeleted.Equals(false) && tb.ReferenceNumberId == referenceId && tb.Status == PaymentRequestStatus.PayBalance
                                    select new PaymentInformationData
                                    {
                                        Id = tb.Id,
                                        Amount = tb.TotalAmountPay,
                                        Billing = de.UnitPrice * de.Qty,
                                        Charges = tb.Charges == null ? 0 : tb.Charges,
                                        EvidenceNo = tb.ReferenceNumberId,
                                        Discount = tb.Discount == null ? 0 : tb.Discount,
                                        TaxPayable = tb.TaxPayable == null ? 0 : tb.TaxPayable,
                                        TaxPayablePercent = tb.TaxPayablePercent == null ? 0 : tb.TaxPayablePercent,
                                        Vat = tb.Vat == null ? 0 : tb.Vat,
                                        VatPercent = tb.VatPercent == null ? 0 : tb.VatPercent,
                                        Paid = (de.UnitPrice * de.Qty) - tb.AmountPay,
                                        Recon = tb.Recon == null ? false : tb.Recon
                                    }).ToListAsync();
                model.AddRange(model2);
            }
            return model.OrderByDescending(x => x.CreatedOn).ToList();
        }

        public async Task<List<Journal>> GetBankPaymentJournal(string BankPaymentId)
        {
            try
            {
                var bp = await context.BankReceived.Where(x => x.IsDeleted.Equals(false) && x.Id == BankPaymentId).FirstOrDefaultAsync();
                var tapname = "";

                tapname = bp.JobId;

                var data = await (from tb in context.JournalTemps
                                  join coa in context.ChartOfAccounts on tb.AccountId equals coa.Id
                                  join bu in context.BusinessUnits on tb.DivisiId equals bu.Id
                                  where tb.ReferenceId == BankPaymentId
                                  orderby tb.Credit descending
                                  orderby tb.Debit descending
                                  select new Journal
                                  {
                                      AccountId = coa.Id,
                                      AccountCode = coa.CodeRec,
                                      AccountName = coa.Name5,
                                      JobId = tb.JobId,
                                      Description = tb.Description == null ? coa.Name5 : tb.Description,
                                      JobIdName = tapname,
                                      DivisiId = tb.DivisiId,
                                      DivisiName = bu.UnitName,
                                      Credit = tb.Credit,
                                      Debit = tb.Debit,
                                      LineCode = tb.LineCode,
                                  }).ToListAsync();
                return data.OrderByDescending(x => x.CreatedOn).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public async Task<int> CreateJournalWithSP(string PaymentRequestId)
        {
            try
            {
                string legalId = "";
                var dataPayment = context.BankReceived.Where(x => x.IsDeleted.Equals(false) && x.Id == PaymentRequestId).FirstOrDefault();
                var legalentity = context.Banks.Where(x => x.IsDeleted.Equals(false) && x.Id == dataPayment.BankId).FirstOrDefault();
                if (dataPayment.PayToType == "1")//petty
                    legalId = dataPayment.LegalEntityId;
                else
                    legalId = legalentity.LegalEntityId;

                await financePeriod.IsClosePeriod(Convert.ToDateTime(dataPayment.PaymentRequestDate), legalId, Models.Enum.FinancePeriodCode.BR);

                var result = await context.Database.ExecuteSqlCommandAsync("EXECUTE fn.create_jurnal_bank_received @p_bank_received_id", parameters: new[] { new SqlParameter("@p_bank_received_id", PaymentRequestId) });
                var save = await context.SaveChangesAsync();
                return save;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public async Task<Bank> GetAccountNumberData(string Id)
        {
            //return await context.Banks.Where(x => x.IsDeleted.Equals(false) && x.Id == Id).FirstOrDefaultAsync();
            return await (from bank in context.Banks
                          join leg in context.LegalEntity on bank.LegalEntityId equals leg.Id
                          where bank.IsDeleted.Equals(false) && bank.Id == Id
                          select new Bank
                          {
                              AccountNumber = bank.AccountNumber,
                              LegalEntityId = bank.LegalEntityId,
                              LegalEntityName = leg.LegalEntityName,
                              SwiftCode = bank.SwiftCode,
                              CoaId = bank.CoaId,
                              CurrencyId = bank.CurrencyId
                          }).FirstOrDefaultAsync();
        }

        public async Task<MasterPettyCash> GetPettycashData(string Id)
        {
            return await (from pet in context.MasterPettyCashs
                          join leg in context.LegalEntity on pet.LegalEntityId equals leg.Id
                          join bank in context.Banks on pet.BankId equals bank.Id into bg
                          from bankg in bg.DefaultIfEmpty()
                          where pet.IsDeleted.Equals(false) && pet.Id == Id
                          select new MasterPettyCash
                          {
                              BankAccountNumber = pet.BankAccountNumber,
                              LegalEntityId = pet.LegalEntityId,
                              ChartAccountId = pet.ChartAccountId,
                              BankCode = bankg.BankName,
                              CurrencyId = pet.CurrencyId
                          }).FirstOrDefaultAsync();
        }

        public async Task<Bank> GetVendorData(string Id)
        {
            return await (from pet in context.Companys
                          join leg in context.Banks on pet.BankId equals leg.Id
                          where pet.IsDeleted.Equals(false) && pet.Id == Id
                          select new Bank
                          {
                              AccountNumber = pet.AccountNumber,
                              LegalEntityId = leg.LegalEntityId,
                              CoaId = leg.CoaId,
                              BankName = leg.BankName
                          }).FirstOrDefaultAsync();
        }

        public async Task<List<BankReceived>> GetWithFilter(PaymentRequestFilter Filter)
        {
            return await (from tb in context.BankReceived
                          join bu in context.BusinessUnits on tb.BusinessUnitId equals bu.Id
                          join le in context.LegalEntity on tb.LegalEntityId equals le.Id into gle
                          from leg in gle.DefaultIfEmpty()
                          join job in context.JobDetail on tb.JobId equals job.Id into gjob
                          from jobs in gjob.DefaultIfEmpty()
                          where tb.IsDeleted.Equals(false) //&& (tb.Status == "5"? tb.IsBalance.Equals(true) : tb.IsBalance.Equals(false))
                          select new BankReceived
                          {
                              Id = tb.Id,
                              InvoiceReceivableId = tb.InvoiceReceivableId,
                              InvoiceReceivableNumber = tb.InvoiceReceivableNumber,
                              AccountId = tb.AccountId,
                              AdvertisingTax = tb.AdvertisingTax,
                              AffilitionId = tb.AffilitionId,
                              BusinessUnitId = tb.BusinessUnitId,
                              BusinessUnitName = bu.UnitName,
                              Amount = tb.Amount,
                              BankName = tb.BankName,
                              CurrencyId = tb.CurrencyId,
                              Discount = tb.Discount,
                              BankId = tb.BankId,
                              MainServiceCategoryId = tb.MainServiceCategoryId,
                              SourceDoc = tb.SourceDoc,
                              PayToVendor = tb.PayToVendor,
                              AccountNumber = tb.AccountNumber,
                              DueDate = tb.DueDate,
                              Status = tb.Status,
                              ExchangeRate = tb.ExchangeRate,
                              LegalEntityId = tb.LegalEntityId,
                              LegalEntityName = leg.LegalEntityName,
                              PaymentRequestDate = tb.PaymentRequestDate,
                              PaymentRequestNumber = tb.PaymentRequestNumber,
                              PaymentTypeId = tb.PaymentTypeId,
                              TaxPayable = tb.TaxPayable,
                              TaxPayablePercent = tb.TaxPayablePercent,
                              ReferenceNumberId = tb.ReferenceNumberId,
                              Vat = tb.Vat,
                              AmountPay = tb.AmountPay,
                              VatPercent = tb.VatPercent,
                              TotalAmount = tb.TotalAmount,
                              PaytoName = tb.PaytoName,
                              JobName = tb.JobId == null ? tb.PurchaseRequestId : tb.JobId + " - " + jobs.JobName,
                              IsBalance = tb.IsBalance,
                              TotalAmountPay = tb.TotalAmountPay,
                              JobId = tb.JobId,
                              Remarks = tb.Remarks
                          }).ToListAsync();

        }

        public async Task<List<AccountReceivable>> GetListARPosted(PaymentRequestFilter Filter)
        {
            var journals = (
                       from a in context.Journal
                       where a.Debit > 0
                       group a by a.ReferenceId into b
                       select b.First()).ToList();


            var model = await (from ar in context.AccountReceivables
                               join bu in context.BusinessUnits on ar.BusinessUnitId equals bu.Id
                               join le in context.LegalEntity on ar.LegalEntityId equals le.Id
                               join job in context.JobDetail on ar.JobId equals job.Id
                               join client in context.Companys on ar.ClientId equals client.Id
                               join jou in journals on ar.Id equals jou.ReferenceId
                               where ar.IsDeleted.Equals(false) && ar.Status.ToLower() == "posted"
                               select new AccountReceivable
                               {
                                   Id = ar.Id,
                                   AccountReceivableNumber = ar.AccountReceivableNumber,
                                   TransactionDate = ar.TransactionDate,
                                   BankAccountDestination = ar.BankAccountDestination,
                                   Amount = ar.Amount,
                                   LegalEntityId = ar.LegalEntityId,
                                   LegalEntity = le.LegalEntityName,
                                   ClientId = ar.ClientId,
                                   ClientName = client.CompanyName,
                                   Status = "",
                                   JobId = ar.JobId,
                                   CurrencyId = ar.CurrencyId,
                                   JobName = job.Id + " - " + job.JobName,
                                   BusinessUnitId = ar.BusinessUnitId,
                                   BusinessUnitName = bu.UnitName,
                                   BrandId = ar.BrandId,
                                   AffilitionId = ar.AffilitionId,
                                   AccountId = jou.AccountId
                               }).ToListAsync();

            if (Filter.FilterBusinessUnitId != "-")
            {
                model = model.Where(x => x.BusinessUnitId == Filter.FilterBusinessUnitId).ToList();
            }

            if (Filter.FilterLegalEntity != "-")
            {
                model = model.Where(x => x.LegalEntityId == Filter.FilterLegalEntity).ToList();
            }

            if (Filter.VendorId != "-")
            {
                model = model.Where(x => x.ClientId == Filter.VendorId).ToList();
            }

            if (Filter.CurrencyId != "-")
            {
                model = model.Where(x => x.CurrencyId == Filter.CurrencyId).ToList();
            }

            if (Filter.FilterARNumber != "-")
            {
                model = model.Where(x => x.AccountReceivableNumber.Contains(Filter.FilterARNumber)).ToList();
            }

            return model;
        }

        public async Task<int> AddWithDetail(BankReceivedDTO entity)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    //var isApprove = true;
                    await globalFcApproval.UnsafeSubmitApproval<BankReceived>(false, entity.Header.Id, int.Parse(entity.Header.Status), $"{ApprovalLink.BankReceived}?Id={entity.Header.Id}&isApprove=true", idTemplate, MenuUnique.BankReceived, entity.Header);
                    //await context.PhoenixAddAsync(entity.Header);
                    List<BankReceivedDetail> ad = new List<BankReceivedDetail>();

                    if (entity.Details.Count > 0)
                    {
                        if (entity.Details != null)
                        {
                            Parallel.ForEach(entity.Details, (item) =>
                            {
                                item.Id = Guid.NewGuid().ToString();
                                item.BankReceivedId = entity.Header.Id;
                                context.PhoenixAddAsync(item);
                            });
                        }
                    }

                    await log.AddAsync(new StatusLog() { TransactionId = entity.Header.Id, Status = PaymentRequestStatus.GetStatus(entity.Header.Status), Description = entity.Header.LogDescription });
                    var save = await context.SaveChangesAsync();
                    transaction.Commit();
                    return save;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message.ToString());
                }
            }
        }

        public async Task<int> EditWithDetail(BankReceivedDTO entity)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var trapprovalmodel = await context.TrTemplateApprovals.Where(x => x.RefId == entity.Header.Id).FirstOrDefaultAsync();
                    if (trapprovalmodel == null)
                    {
                        await globalFcApproval.UnsafeSubmitApproval<BankReceived>(true, entity.Header.Id, int.Parse(entity.Header.Status), $"{ApprovalLink.BankReceived}?Id={entity.Header.Id}&isApprove=true", idTemplate, MenuUnique.BankReceived, entity.Header);
                    }
                    else
                    {
                        context.PhoenixEdit(entity.Header);
                    }
                    var delmod = await context.BankReceivedDetail.Where(x => x.IsDeleted.Equals(false) && x.BankReceivedId == entity.Header.Id).ToListAsync();
                    if (delmod.Count > 0)
                    {
                        context.BankReceivedDetail.RemoveRange(delmod);
                    }
                    if (entity.Details.Count > 0)
                    {
                        //remove existing data

                        //validate again, if still exist additional data after removerange

                        List<BankReceivedDetail> dt = new List<BankReceivedDetail>();
                        foreach (BankReceivedDetail data in entity.Details)
                        {
                            //data.Id = Guid.NewGuid().ToString();
                            data.BankReceivedId = entity.Header.Id;
                            dt.Add(data);
                        }

                        BankReceivedDetail[] dtg = dt.ToArray();
                        await context.PhoenixAddRangeAsync(dtg);

                    }


                    //var trapprovalmodel = await context.TrTemplateApprovals.Where(x => x.RefId == entity.Header.Id).FirstOrDefaultAsync();
                    //if (trapprovalmodel == null)
                    //{
                    //    var isApprove = true;
                    //    var dataMenu = await menuService.GetByUniqueName(MenuUnique.BankReceived).ConfigureAwait(false);
                    //    var vm = new Models.ViewModel.TransApprovalHrisVm()
                    //    {
                    //        MenuId = dataMenu.Id,
                    //        RefId = entity.Header.Id,
                    //        DetailLink = $"{ApprovalLink.BankReceived}?Id={entity.Header.Id}&isApprove=true",
                    //        //Tname = "fn.PurchaseRequest",//"SP#NAMA SP NYA",
                    //        IdTemplate = idTemplate
                    //    };
                    //    var subGroupId = ClainJwtPrincipalHelper.GetBusinessSubGroup(context.HttpContext);
                    //    var divisionId = ClainJwtPrincipalHelper.GetBusinessUnitDivisi(context.HttpContext);
                    //    var employeeId = ClainJwtPrincipalHelper.GetEmployeeId(context.HttpContext);
                    //    isApprove = await globalFcApproval.Save(vm, subGroupId, divisionId, employeeId);


                    //    if (isApprove)
                    //    {
                    //        var save = await context.SaveChangesAsync();
                    //        transaction.Commit();
                    //        return save;
                    //    }
                    //    else
                    //    {
                    //        transaction.Rollback();
                    //        throw new Exception("please check the approval template that will be processed");
                    //    }
                    //}
                    //else
                    //{
                    await log.AddAsync(new StatusLog() { TransactionId = entity.Header.Id, Status = PaymentRequestStatus.GetStatus(entity.Header.Status), Description = entity.Header.LogDescription });
                    var save = await context.SaveChangesAsync();
                    transaction.Commit();
                    return save;
                    //}

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message.ToString());
                }
            }
        }


        public async Task<List<AccountReceivable>> GetListDetailPaymentRequest(string Id)
        {
            var model = await (from tb in context.BankReceivedDetail
                               join ar in context.AccountReceivables on tb.AccountReceivebleNumber equals ar.Id
                               //join inv in context.InvoiceReceivables on ta.InvoiceReceivableId equals inv.Id
                               join bu in context.BusinessUnits on ar.BusinessUnitId equals bu.Id
                               join job in context.JobDetail on tb.JobId equals job.Id
                               join le in context.LegalEntity on ar.LegalEntityId equals le.Id into leg
                               from le in leg.DefaultIfEmpty()
                               join ve in context.Companys on ar.ClientId equals ve.Id
                               //join coa5 in context.ChartOfAccounts on inv.TaxPayableCoaId equals coa5.Id into gcoa
                               //from gcoa5 in gcoa.DefaultIfEmpty()
                               join bank in context.Banks on ve.BankId equals bank.Id into gbank
                               from banks in gbank.DefaultIfEmpty()
                               join brand in context.CompanyBrands on tb.BrandId equals brand.Id into gbrand
                               from brands in gbrand.DefaultIfEmpty()
                               where tb.IsDeleted.Equals(false) && tb.BankReceivedId == Id
                               select new AccountReceivable
                               {
                                   Id = ar.Id,
                                   AccountReceivableNumber = ar.AccountReceivableNumber,
                                   TransactionDate = ar.TransactionDate,
                                   BankAccountDestination = ar.BankAccountDestination,
                                   Amount = ar.Amount,
                                   LegalEntityId = ar.LegalEntityId,
                                   LegalEntity = le.LegalEntityName,
                                   ClientId = ar.ClientId,
                                   ClientName = ve.CompanyName,
                                   Status = "",
                                   JobId = ar.JobId,
                                   CurrencyId = ar.CurrencyId,
                                   JobName = job.Id + " - " + job.JobName,
                                   BusinessUnitId = ar.BusinessUnitId,
                                   BusinessUnitName = bu.UnitName,
                                   BrandId = ar.BrandId,
                                   ClientBrand = brands.BrandName,
                                   AffilitionId = ar.AffilitionId
                               }).ToListAsync();

            if (model == null) model = new List<AccountReceivable>();
            return model.OrderBy(x => x.CreatedOn).ToList();
        }

        public async Task<List<BankReceivedDetail>> GetDetail(string Id)
        {
            return await context.BankReceivedDetail.Where(x => x.IsDeleted.Equals(false) && x.BankReceivedId == Id).ToListAsync();
        }


    }
}
