﻿using Phoenix.Data.Models.Finance.Transaction;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Phoenix.Data.Models.Finance.Transaction.InvoiceReceivable;
using Phoenix.Data.Models;

namespace Phoenix.Data.Services.Finance.Transaction
{
    public interface IAccountPayableService : IDataService<AccountPayable>
    {
        Task<List<AccountPayableJoin>> GetJoin();
        Task<List<AccountPayableJoin>> GetJoinCustom(string status, string businessUnit, string legalEntity, string ApNumber);
        Task<int> AddRangeDetailAsync(params AccountPayableDetail[] entities);
        Task<TemporaryAccountPayable> GetAccountPayableData(string referenceId);
        Task<int> InsertAccountPaybleDTO(AccountPayable model);
        Task<List<AccountPayableDetail>> GetDetailAccountPayable(string Id);
        Task<AccountPayable> GetAccountPayableByAccountNumber(string AccountNumber);
    }
    public class AccountPayableService : IAccountPayableService
    {
        readonly DataContext context;
        public AccountPayableService(DataContext context)
        {
            this.context = context;
        }
        public async Task<int> AddAsync(AccountPayable entity)
        {
            entity.Id = Guid.NewGuid().ToString();
            await context.PhoenixAddAsync(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> AddRangeAsync(params AccountPayable[] entities)
        {
            await context.PhoenixAddRangeAsync(entities);
            return await context.SaveChangesAsync();
        }

        public async Task<int> AddRangeDetailAsync(params AccountPayableDetail[] entities)
        {
            await context.PhoenixAddRangeAsync(entities);
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteAsync(AccountPayable entity)
        {
            context.PhoenixApprove(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteRageAsync(params AccountPayable[] entities)
        {
            context.PhoenixDeleteRange(entities);
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteSoftAsync(AccountPayable entity)
        {
            context.PhoenixDelete(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteSoftRangeAsync(params AccountPayable[] entities)
        {
            context.PhoenixDeleteRange(entities);
            return await context.SaveChangesAsync();
        }

        public async Task<int> EditAsync(AccountPayable entity)
        {
            context.PhoenixEdit(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> EditRangeAsync(AccountPayable entity)
        {
            context.PhoenixEditRange(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<List<AccountPayable>> Get() => await context.AccountPayables.AsNoTracking().Where(x => !(x.IsDeleted ?? false)).ToListAsync();

        public async Task<List<AccountPayable>> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public async Task<AccountPayable> Get(string id) => await context.AccountPayables.AsNoTracking().SingleOrDefaultAsync(x => !(x.IsDeleted ?? false) && x.Id == id);

        public async Task<AccountPayable> GetAccountPayableByAccountNumber(string AccountNumber)
        {
            return await context.AccountPayables.Where(x => x.IsDeleted.Equals(false) && x.AccountPayableNumber == AccountNumber).FirstOrDefaultAsync();
        }

        public async Task<TemporaryAccountPayable> GetAccountPayableData(string referenceId)
        {
            var query = await (from tap in context.TemporaryAccountPayables
                               where tap.TapNumber.Equals(referenceId)
                               select new TemporaryAccountPayable
                               {
                                   Id = tap.Id,
                                   TapNumber = tap.TapNumber,
                                   LegalEntityId = tap.LegalEntityId,
                                   BusinessUnitId = tap.BusinessUnitId,
                                   AffiliationId = tap.AffiliationId,
                                   AccountId = tap.AccountId,
                                   VendorId = tap.VendorId,
                                   JobId = tap.JobId,
                                   TransactionDate = tap.TransactionDate,
                                   DueDays = tap.DueDays,
                                   CurrencyId = tap.CurrencyId,
                                   Amount = tap.Amount,
                                   ExchangeRate = tap.ExchangeRate,
                                   BankAccountDestination = tap.BankAccountDestination,
                                   Charges = tap.Charges,
                                   Remarks = tap.Remarks,
                                   //invoiceReceivable = (from invr in context.InvoiceReceivables
                                   //                     join comp in context.Companys on invr.ClientId equals comp.Id
                                   //                     join curr in context.Currencies on invr.CurrencyId equals curr.Id
                                   //                     where tap.InvoiceReceivableId == invr.Id
                                   //                     select new InvoiceReceived
                                   //                     {
                                   //                         Id = invr.Id,
                                   //                         Description = invr.Description,
                                   //                         DivisionId = invr.DivisionId,
                                   //                         InvoiceVendor = invr.InvoiceVendor,
                                   //                         InvoiceVendorDate = invr.InvoiceVendorDate,
                                   //                         InvoiceNumber = invr.InvoiceNumber,
                                   //                         InvoiceDate = invr.InvoiceDate,
                                   //                         InvoiceStatusId = invr.InvoiceStatusId,
                                   //                         ClientName = invr.ClientName,
                                   //                         TotalAmount = invr.TotalAmount ?? 0,
                                   //                         PoNumber = invr.PoId,
                                   //                         CurrencyId = invr.CurrencyId,
                                   //                         CurrencyName = curr.CurrencyName,
                                   //                         ExchangeRate = invr.ExchangeRate ?? 0,
                                   //                         ClientId = invr.ClientId,
                                   //                         ContractCheckList = invr.ContractCheckList,
                                   //                         ContractObject = invr.ContractObject,
                                   //                         ApprovalFrontDesk = invr.ApprovalFrontDesk,
                                   //                         FrontDeskNotes = invr.FrontDeskNotes,
                                   //                         AdminNotes = invr.AdminNotes,
                                   //                         ApprovalAdminDesk = invr.ApprovalAdminDesk,
                                   //                         AffiliationId = invr.AffiliationId,
                                   //                         Amount = invr.Amount ?? 0,
                                   //                         JobId = invr.JobId,
                                   //                         LegalId = invr.LegalEntityId,
                                   //                         MainServiceCategoryId = invr.MainServiceCategoryId,
                                   //                         GrCheckList = invr.GrCheckList,
                                   //                         GrObject = invr.GrObject,
                                   //                         DoCheckList = invr.DoCheckList,
                                   //                         DoObject = invr.DoObject, 
                                   //                         PoCheckList = invr.PoCheckList,
                                   //                         PoObject = invr.PoObject,
                                   //                         PoId = invr.PoId,
                                   //                         ServiceCenterId = invr.ServiceCenterId,
                                   //                         //StatusName = invr.StatusName,
                                   //                         TaxCheckList = invr.TaxCheckList,
                                   //                         TaxDate = invr.TaxDate,
                                   //                         TaxNumber = invr.TaxNumber,
                                   //                         TaxObject = invr.TaxObject,
                                   //                         Value = invr.Value ?? 0,
                                   //                         Vat = invr.Vat ?? 0,
                                   //                         PCE = (from jd in context.JobDetail
                                   //                                join tpce in context.Pces on jd.Id equals tpce.JobId
                                   //                                join ms in context.MainServiceCategorys on tpce.MainserviceCategoryId equals ms.Id
                                   //                                into tms1
                                   //                                from tljms in tms1.DefaultIfEmpty()
                                   //                                join cb in context.ClientBrief on jd.ClientBriefId equals cb.Id
                                   //                                into tcb1
                                   //                                from tljcb in tcb1.DefaultIfEmpty()
                                   //                                join cbr in context.CompanyBrands on tljcb.ExternalBrandId equals cbr.Id
                                   //                                into tcbr1
                                   //                                from tljcbr in tcbr1.DefaultIfEmpty()
                                   //                                where invr.JobId == jd.Id
                                   //                                select new Pce
                                   //                                {
                                   //                                    Id = jd.Id,
                                   //                                    external_brand_id = tljcb.ExternalBrandId,
                                   //                                    BrandName = tljcbr.BrandName,
                                   //                                    MainserviceCategoryId = tpce.MainserviceCategoryId,
                                   //                                    MainServicesName = tljms.Name
                                   //                                }).FirstOrDefault()
                                   //                     }).FirstOrDefault(),

                               }).FirstOrDefaultAsync();

            return query;
        }

        public async Task<List<AccountPayableDetail>> GetDetailAccountPayable(string Id)
        {
            var model = await (from tb in context.AccountPayableDetails
                               where tb.AccountPayableId == Id
                               select new AccountPayableDetail
                               {

                               }).ToListAsync();
            return model;
        }



        public async Task<List<AccountPayableJoin>> GetJoin()
        {
            var query = await (from ap in context.AccountPayables.AsNoTracking()
                               join comp in await context.Companys.AsNoTracking().ToListAsync() on ap.VendorId equals comp.Id
                               join curr in await context.Currencies.AsNoTracking().ToListAsync() on ap.CurrencyId equals curr.Id
                               join inv in await context.InvoiceReceivables.AsNoTracking().ToListAsync() on ap.OurRefId equals inv.Id
                               into tinv1
                               from tljinv in tinv1.DefaultIfEmpty()
                               join jd in await context.JobDetail.AsNoTracking().ToListAsync() on tljinv.JobId equals jd.Id
                               into tjd1
                               from tjdi in tjd1.DefaultIfEmpty()
                               join cb in context.ClientBrief.ToList() on tjdi.ClientBriefId equals cb.Id
                               into tcb1
                               from tljcb in tcb1.DefaultIfEmpty()
                               join cbr in context.CompanyBrands.ToList() on tljcb.ExternalBrandId equals cbr.Id
                               into tcbr1
                               from tljcbr in tcbr1.DefaultIfEmpty()
                               join bu in await context.BusinessUnits.ToListAsync() on ap.BusinessUnitId equals bu.Id
                               join le in await context.LegalEntity.ToListAsync() on ap.LegalEntityId equals le.Id
                               select new AccountPayableJoin
                               {
                                   Id = ap.Id,
                                   AccountPayableNumber = ap.AccountPayableNumber,
                                   LegalEntityId = ap.LegalEntityId,
                                   BusinessUnitId = ap.BusinessUnitId,
                                   AffiliationId = ap.AffiliationId,
                                   AccountId = ap.AccountId,
                                   VendorId = ap.VendorId,
                                   JobId = ap.JobId,
                                   REF = tljinv.InvoiceNumber,
                                   YourREF = tljinv.InvoiceVendor,
                                   Product = tljcbr.BrandName,
                                   TransactionDate = ap.TransactionDate,
                                   DueDays = ap.DueDays,
                                   Periode = ap.Periode,
                                   InvoiceVendorNumber = ap.InvoiceVendorNumber,
                                   InvoiceReceivedDate = ap.InvoiceReceivedDate,
                                   CurrencyId = ap.CurrencyId,
                                   Amount = ap.Amount,
                                   ExchangeRate = ap.ExchangeRate,
                                   BankAccountDestination = ap.BankAccountDestination,
                                   InvoiceStatusId = tljinv.InvoiceStatusId,
                                   Charges = ap.Charges,
                                   Remarks = tljinv.Description,
                                   InvoiceReceivableId = tljinv.Id,
                                   VendorName = comp.CompanyName,
                                   CurrencyName = curr.CurrencyName,
                                   TemporaryAccountPayableId = ap.TemporaryAccountPayableId,
                                   BusinessUnitName = bu.UnitName,
                                   LegalEntityName = le.LegalEntityName
                               }).ToListAsync();

            return query;
        }

        public async Task<List<AccountPayableJoin>> GetJoinCustom(string status, string businessUnit, string legalEntity, string ApNumber)
        {
            var data = await (from ap in context.AccountPayables.AsNoTracking()
                              join comp in await context.Companys.AsNoTracking().ToListAsync() on ap.VendorId equals comp.Id
                              join curr in await context.Currencies.AsNoTracking().ToListAsync() on ap.CurrencyId equals curr.Id
                              join inv in await context.InvoiceReceivables.AsNoTracking().ToListAsync() on ap.OurRefId equals inv.Id
                              into tinv1
                              from tljinv in tinv1.DefaultIfEmpty()
                              join jd in await context.JobDetail.AsNoTracking().ToListAsync() on tljinv.JobId equals jd.Id
                              into tjd1
                              from tjdi in tjd1.DefaultIfEmpty()
                              join cb in context.ClientBrief.ToList() on tjdi.ClientBriefId equals cb.Id
                              into tcb1
                              from tljcb in tcb1.DefaultIfEmpty()
                              join cbr in context.CompanyBrands.ToList() on tljcb.ExternalBrandId equals cbr.Id
                              into tcbr1
                              from tljcbr in tcbr1.DefaultIfEmpty()
                              join bu in await context.BusinessUnits.ToListAsync() on ap.BusinessUnitId equals bu.Id
                              join le in await context.LegalEntity.ToListAsync() on ap.LegalEntityId equals le.Id
                              select new AccountPayableJoin
                              {
                                  Id = ap.Id,
                                  AccountPayableNumber = ap.AccountPayableNumber,
                                  LegalEntityId = ap.LegalEntityId,
                                  BusinessUnitId = ap.BusinessUnitId,
                                  AffiliationId = ap.AffiliationId,
                                  AccountId = ap.AccountId,
                                  VendorId = ap.VendorId,
                                  JobId = ap.JobId,
                                  REF = tljinv.InvoiceNumber,
                                  YourREF = tljinv.InvoiceVendor,
                                  Product = tljcbr.BrandName,
                                  TransactionDate = ap.TransactionDate,
                                  DueDays = ap.DueDays,
                                  Periode = ap.Periode,
                                  InvoiceVendorNumber = ap.InvoiceVendorNumber,
                                  InvoiceReceivedDate = ap.InvoiceReceivedDate,
                                  CurrencyId = ap.CurrencyId,
                                  Amount = ap.Amount,
                                  ExchangeRate = ap.ExchangeRate,
                                  BankAccountDestination = ap.BankAccountDestination,
                                  InvoiceStatusId = tljinv.InvoiceStatusId,
                                  Charges = ap.Charges,
                                  Remarks = tljinv.Description,
                                  InvoiceReceivableId = tljinv.Id,
                                  VendorName = comp.CompanyName,
                                  CurrencyName = curr.CurrencyName,
                                  TemporaryAccountPayableId = ap.TemporaryAccountPayableId,
                                  BusinessUnitName = bu.UnitName,
                                  LegalEntityName = le.LegalEntityName,
                                  InvoiceNumber = tljinv.InvoiceNumber
                              }).ToListAsync();

            if (status != "0")
            {
                data = data.Where(x => x.InvoiceStatusId == status).ToList();
            }
            if (businessUnit != "0")
            {

                data = data.Where(x => x.BusinessUnitId == businessUnit).ToList();
            }
            if (legalEntity != "0")
            {

                data = data.Where(x => x.LegalEntityId == legalEntity).ToList();
            }
            if (ApNumber != "0")
            {

                data = data.Where(x => x.AccountPayableNumber == ApNumber).ToList();
            }

            return data;
        }

        public async Task<int> InsertAccountPaybleDTO(AccountPayable model)
        {
            try
            {
                var details = model.Details.ToArray();
                var now = DateTime.Now;
                model.AccountPayableNumber = $"AP{now.ToString("yyyy")}{now.ToString("M").ToUpper()}{now.ToString("ddHHmmss")}";
                model.TransactionDate = DateTime.Now;
                await context.AddAsync(model);

                foreach (var detail in details)
                {
                    detail.Id = Guid.NewGuid().ToString();
                    detail.AccountPayableId = model.Id;
                    detail.EvidentNumber = model.AccountPayableNumber;

                }

                await context.AddRangeAsync(details);
                return await context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
    }


}
