﻿using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Attributes;
using Phoenix.Data.Models;
using Phoenix.Data.Models.Finance.Transaction;
using Phoenix.Data.Models.ViewModel;
using Phoenix.Data.Services.Um;
using Phoenix.Shared.Core.PrincipalHelpers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.Data.Services.Finance.Transaction
{
    public interface IInvoiceClientMultiPceService : IDataService<InvoiceClient>
    {
        Task<object> GetListIndex(string status, string legal);
        Task<InvoiceClient> GetPceInfo(string pceno);
        Task<dynamic> ReadMultiPceList(string multipceid);
        Task<object> GetPceChoiceList(string multipceid, string legalid, string divid, string mservice, string clientt, string currency, string brandid);
        Task<dynamic> ReadInvHistory(string multipceid);
        Task<InvoiceClient> EditAsyncData(InvoiceClient model);
        Task<InvoiceClient> InsertInvoiceClient(InvoiceClient model);
        Task<int> InsertAndSubmitInvoiceClient(InvoiceClient model);
        Task<dynamic> GetDDLBank(string org);
        Task<string> GetInfoBank(string idbank);
        Task<InvoiceClient> Approve(InvoiceClient entity);
        Task<InvoiceClient> Reject(InvoiceClient entity);
        Task<int> EditAndSubmitAsync(InvoiceClient entity);
        Task<int> CreateJournalWithSP(string Id);
        Task<List<Journal>> GetDataJournal(string Id);
        Task<List<InvoiceClientItemOther>> GetInvoiceProductionDetail(string pceid, string invNo);
    }
    public class InvoiceClientMultiPceService : IInvoiceClientMultiPceService
    {
        readonly DataContext context;
        readonly GlobalFunctionApproval globalFcApproval;
        readonly IManageMenuService menuService;
        readonly FileService uploadFile;
        readonly StatusLogService log;
        public InvoiceClientMultiPceService(DataContext context, GlobalFunctionApproval globalFcApproval, IManageMenuService menuService, FileService uploadFile, StatusLogService log)
        {
            this.context = context;
            this.globalFcApproval = globalFcApproval;
            this.menuService = menuService;
            this.uploadFile = uploadFile;
            this.log = log;
        }

        //Read
        public async Task<object> GetListIndex(string status, string legal)
        {
            var indexlst = await (
                from a in context.InvoiceClients
                join pce in context.Pces on a.PceId equals pce.Id
                join b in context.JobDetail on pce.JobId equals b.Id
                join c in context.LegalEntity on b.LegalEntityId equals c.Id
                join d in context.MainServiceCategorys on b.MainServiceCategory equals d.Id
                join f in context.CompanyBrands on b.BrandId equals f.Id
                join g in context.Companys on f.CompanyId equals g.Id
                join h in context.BusinessUnits on b.BusinessUnitDivisionId equals h.Id
                join i in context.Banks on a.AccountId equals i.Id
                where a.MultiPceId != null
                    && c.Id == (legal == "0" ? c.Id : legal)
                // && a.Status == (status == "0" ? a.Status : status)
                select new
                {
                    a.MultiPceId,
                    Pceid = a.PceId,
                    a.InvoiceDate,
                    JobId = b.Id,
                    b.JobName,
                    ClientName = g.CompanyName,
                    a.GrandTotal,
                    a.TotalAmount,
                    AccountBankName = i.BankName,
                    AccountBankNumber = i.AccountNumber,
                    LegalEntity = c.LegalEntityName,
                    Division = h.UnitName,
                    a.Status,
                    StatusName = SingleInvoiceClientStatus.StatusName(Convert.ToInt32(a.Status))
                }).ToListAsync();

            //var result = (
            //    from rows in indexlst
            //    group rows by new { rows.MultiPceId, rows.Status, rows.ServiceType, rows.LegalEntity, rows.ClientName, rows.TotalAmount } into gr
            //    select new
            //    {
            //        gr.Key.MultiPceId,
            //        gr.Key.Status,
            //        gr.Key.ServiceType,
            //        gr.Key.LegalEntity,
            //        gr.Key.ClientName,
            //        gr.Key.TotalAmount,
            //        Pceid = string.Join(",", gr.Select(i => i.Pceid))
            //    }).ToList();
            //return result;
            if (indexlst.Count > 0)
                return indexlst.OrderByDescending(x => decimal.Parse(x.MultiPceId.Substring(4)));
            return indexlst;
        }

        public async Task<dynamic> GetDDLBank(string org)
        {
            return await (from a in context.Banks
                          where //a.OrganizationId == (string.IsNullOrWhiteSpace(org) ? a.OrganizationId : org) && 
                          a.IsActive == true
                          select new
                          {
                              Text = a.BankName + " - " + a.AccountNumber,
                              Value = a.Id
                          }).AsNoTracking().ToListAsync();
        }

        public async Task<InvoiceClient> GetPceInfo(string multipceid)
        {
            //join divisi in (
            //    from x in context.BusinessUnits
            //    join y in context.BusinessUnitTypes on x.BusinessUnitTypeId equals y.Id
            //    where y.BusinessUnitLevel == BusinessUnitLevelCode.Division
            //    select x
            //) on b.BusinessUnitDivisionId equals divisi.Id
            //var total = (
            //  from a in context.InvoiceClients
            //  where a.MultiPceId == multipceid
            //  //join b in context.Pces on a.PceId equals b.Id
            //  // group b by new { a.MultiPceId } into c
            //  //where c.Key.MultiPceId == multipceid
            //  select new
            //  {
            //      //Subtotal = c.Sum(d => d.Subtotal),
            //      Total = c.Sum(a.InvoiceBilled),
            //      //Vat = c.Sum(d => d.Vat),
            //      //OtherFee = c.Sum(d => d.OtherFee),
            //      //Subtotal = 
            //  }).FirstOrDefault() ?? new Pce();
            var Billed = (from a in context.InvoiceClients where a.MultiPceId == multipceid select new InvoiceClient {
                Asf = a.Asf,
                InvoiceBilled =a.InvoiceBilled
            }).ToList();

            decimal totalBilled = 0;
            foreach (var i in Billed)
            {
                totalBilled += ((Convert.ToDecimal(i.Asf) / Convert.ToDecimal("100")) * Convert.ToDecimal(i.InvoiceBilled)) + Convert.ToDecimal(i.InvoiceBilled);
            }
            
            var data = await (
                from a in context.InvoiceClients
                join bank in context.Banks on a.AccountId equals bank.Id
                join c in context.Currencies on a.CurrencyId equals c.Id
                join d in context.MainServiceCategorys on a.MainServiceCategory equals d.Id
                join e in context.LegalEntity on a.LegalId equals e.Id
                join h in context.CompanyBrands on a.BrandId equals h.Id
                join i in context.Companys on a.ClientId equals i.Id
                join divisi in context.BusinessUnits on a.BusinessUnitId equals divisi.Id
                // join p in context.AccountReceivables on a.MultiPceId equals p.Id into arc
                //from ar in arc.DefaultIfEmpty(new AccountReceivable())
                where a.MultiPceId == multipceid
                select new InvoiceClient
                {
                    Id = a.Id,
                    MultiPceId = a.MultiPceId,
                    AccountId = a.AccountId,
                    AccountBankName = bank.BankName,
                    InvoiceDate = a.InvoiceDate,
                    VatNo = a.VatNo,
                    PceId = a.PceId,
                    RefNumber = a.RefNumber,
                    YourRef = a.YourRef,
                    ExchangeRate = a.ExchangeRate,
                    GrandTotal = (Convert.ToDecimal("0.10") * totalBilled) + totalBilled,
                    Vat = Convert.ToDecimal("0.10") * totalBilled,
                    Asf = a.Asf,
                    AsfAmount = a.AsfAmount,
                    Status = a.Status,
                    Remarks = a.Remarks,
                    PayDescription = a.PayDescription,
                    AffiliationId = a.AffiliationId,
                    TotalAmount = totalBilled,
                    PoDocFileId = a.PoDocFileId,
                    VatDocFileId = a.VatDocFileId,
                    InvoiceNumber = a.MultiPceId,
                    AccountBankNumber = bank.AccountNumber,

                    CurrencyId = a.CurrencyId,
                    CurrencyCode = c.CurrencyCode,
                    MainServiceCategory = a.MainServiceCategory,
                    MainServiceCategoryName = d.Name,
                    JobId = a.JobId,
                    // JobName = b.JobName,
                    LegalId = a.LegalId,
                    LegalEntityName = e.LegalEntityName,
                    BrandId = h.Id,
                    TermOfPayment = h.TermOfPayment,
                    //OrganizationId = f.Id,
                    //Brand = f.OrganizationName,
                    POClientNo = a.POClientNo,
                    //JobStatus = b.JobStatusId,
                    DivisionName = divisi.UnitName,
                    BusinessUnitId = divisi.Id,
                    ClientId = i.Id,
                    ClientName = i.CompanyName,
                    ClientBrandName = h.BrandName,
                    ClientInvAddress = h.BrandAddress,
                    ClientTaxNumber = i.TaxRegistrationNumber,
                    ClientPIC = h.ContactName,
                    //Asf = a.OtherFeePercentage,
                    //AsfAmount = a.OtherFee,
                    //AffiliationName = m.AffiliationName,

                    ApprovedBy = a.ApprovedBy,
                    ApprovedOn = a.ApprovedOn,
                    CreatedBy = a.CreatedBy,
                    CreatedOn = a.CreatedOn,
                    DeletedBy = a.DeletedBy,
                    DeletedOn = a.DeletedOn,
                    IsActive = a.IsActive,
                    IsDefault = a.IsDefault,
                    IsDeleted = a.IsDeleted,
                    IsLocked = a.IsLocked,
                    ModifiedBy = a.ModifiedBy,
                    ModifiedOn = a.ModifiedOn,
                    OwnerId = a.OwnerId

                }).FirstOrDefaultAsync();

            //if (n == null)
            //    n = new InvoiceClient();

            ////var totalAmt = (from a in context.InvoiceClients where a.MultiPceId == multipceid select a.TotalAmount).ToList().Sum();


            //var pce = await (
            //    from a in context.Pces
            //    join b in context.JobDetail on a.JobId equals b.Id
            //    join c in context.Currencies on a.CurrencyId equals c.Id
            //    join d in context.MainServiceCategorys on a.MainserviceCategoryId equals d.Id
            //    join e in context.LegalEntity on b.LegalEntityId equals e.Id
            //    join h in context.CompanyBrands on b.BrandId equals h.Id
            //    join i in context.Companys on h.CompanyId equals i.Id
            //    join divisi in context.BusinessUnits on b.BusinessUnitDivisionId equals divisi.Id
            //    where a.Code == n.PceId //&& j.AddressType == 1
            //    select new
            //    {
            //        n.Id,
            //        InvoiceNumber = n.MultiPceId,
            //        n.AccountId,
            //        n.AccountBankName,
            //        n.InvoiceDate,
            //        n.VatNo,
            //        PceBalance = (total.Total ?? 0) - n.TotalAmount,
            //        //n.DueDate,
            //        PceId = a.Id,
            //        CurrencyId = c.Id,
            //        c.CurrencyCode,
            //        n.RefNumber,
            //        n.YourRef,
            //        n.ExchangeRate,
            //        TotalAmount = total.Total,
            //        n.GrandTotal,
            //        n.Vat,
            //        n.Asf,
            //        n.AsfAmount,

            //        BrandId = h.Id,
            //        ClientId = i.Id,
            //        LegalId = e.Id,
            //        DivisionId = divisi.Id,
            //        MainServiceCategory = d.Id,
            //        MainServiceCategoryName = d.Name,
            //        Status = "",
            //        n.MultiPceId,
            //        n.ARStatus,
            //        n.Remarks
            //    }).AsNoTracking().FirstOrDefaultAsync();

            //return pce;
            return await GetFile(data);
        }

        private async Task<InvoiceClient> GetFile(InvoiceClient data)
        {
            if (data.PoDocFileId != null)
            {
                var a = await context.Filemasters.Where(x => x.Id == data.PoDocFileId).FirstOrDefaultAsync();
                if (a != null) data.PoDocFileName = a.Name;
            }

            if (data.VatDocFileId != null)
            {
                var b = await context.Filemasters.Where(x => x.Id == data.VatDocFileId).FirstOrDefaultAsync();
                if (b != null) data.VatDocFileName = b.Name;
            }
            return data;
        }

        public async Task<object> ReadMultiPceList(string multipceid)
        {
            var indexlst = await (
                from a in context.InvoiceClients
                join b in context.Pces on a.PceId equals b.Id
                join c in context.JobDetail on b.JobId equals c.Id
                join d in context.LegalEntity on c.LegalEntityId equals d.Id
                join e in context.MainServiceCategorys on c.MainServiceCategory equals e.Id
                join divisi in (
                   from x in context.BusinessUnits
                   join y in context.BusinessUnitTypes on x.BusinessUnitTypeId equals y.Id
                   where y.BusinessUnitLevel == BusinessUnitLevelCode.Division
                   select x
               ) on c.BusinessUnitDivisionId equals divisi.Id
                join f in context.Currencies on b.CurrencyId equals f.Id
                join h in context.CompanyBrands on c.BrandId equals h.Id
                join i in context.Companys on h.CompanyId equals i.Id
                where b.Status == StatusTransactionName.Approved

                where a.MultiPceId != null && a.MultiPceId == multipceid
                select new
                {
                    a.Id,
                    a.PceId,
                    a.MultiPceId,
                    PceSubtotal = b.Subtotal,
                    PceOtherFee = b.OtherFee,
                    PceTotal = (b.Subtotal + b.OtherFee),
                    PceGrandTotal = (b.Subtotal + b.Vat),
                    JobId = c.Id,
                    JobName = c.Id + " - " + c.JobName,
                    BillPercent = 100,
                    a.InvoiceBilled,
                    PceBalance = 0,
                    JobStatus = c.JobStatusId,
                    LegalId = d.Id,
                    LegalEntity = d.LegalEntityName,
                    DivisionId = divisi.Id,
                    Division = divisi.UnitName,
                    MainServiceCategoryName = e.Name,
                    f.CurrencyCode,
                    ClientId = i.Id,
                    ClientName = i.CompanyName,
                    ClientBrandName = h.BrandName,
                    Asf = b.OtherFeePercentage,
                    SubtotalPlusAsf = (b.Subtotal + b.OtherFee),
                    BilledAsfPercent = (b.OtherFeePercentage /100) * a.InvoiceBilled,
                    Subtotals = ((b.OtherFeePercentage / 100) * a.InvoiceBilled) + a.InvoiceBilled
                }).ToListAsync();
            return indexlst;
        }

        public async Task<object> GetPceChoiceList(string multipceid, string legalid, string divid, string mservice, string clientt, string currency, string brandid)
        {
            List<string> pceid = new List<string>();
            //if (!(string.IsNullOrWhiteSpace(multipceid) || multipceid == "0"))
            //{
            //    pceid = await (from a in context.InvoiceClients where a.MultiPceId == multipceid select a.PceId).AsNoTracking().ToListAsync();
            //}
            //else
            //{
            //    pceid = await (from a in context.InvoiceClients where a.MultiPceId != null select a.PceId).AsNoTracking().ToListAsync();
            //}
            pceid = await (from a in context.InvoiceClients where !(a.IsDeleted ?? false) select a.PceId).AsNoTracking().ToListAsync();

            var pces = await (from b in context.Pces
                              join c in context.JobDetail on b.JobId equals c.Id
                              join d in context.LegalEntity on c.LegalEntityId equals d.Id
                              join e in context.MainServiceCategorys on c.MainServiceCategory equals e.Id
                              join divisi in (
                                 from x in context.BusinessUnits
                                 join y in context.BusinessUnitTypes on x.BusinessUnitTypeId equals y.Id
                                 where y.BusinessUnitLevel == BusinessUnitLevelCode.Division
                                 select x
                             ) on c.BusinessUnitDivisionId equals divisi.Id
                              join g in context.CompanyBrands on c.BrandId equals g.Id
                              join h in context.Companys on g.CompanyId equals h.Id
                              where b.Status == StatusTransactionName.Approved
                                  && c.LegalEntityId == (legalid == "0" ? c.LegalEntityId : legalid)
                                  && c.BusinessUnitDivisionId == (divid == "0" ? c.BusinessUnitDivisionId : divid)
                                  && c.MainServiceCategory == (mservice == "0" ? c.MainServiceCategory : mservice)
                                  && h.Id == (clientt == "0" ? h.Id : clientt)
                                  && b.CurrencyId == (currency == "0" ? b.CurrencyId : currency)
                                  && g.Id == (brandid == "0" ? g.Id : brandid)
                                  && b.PceReference == null
                              select new
                              {
                                  PceId = b.Id,
                                  PceCode = b.Code,
                                  PceSubtotal = b.Subtotal,
                                  PceOtherFee = b.OtherFee,
                                  PceVat = b.Vat,
                                  PceGrandTotal = b.Total,
                                  TotalAmount = (b.Subtotal + b.OtherFee),
                                  GrandTotal = b.Total,
                                  ClientName = h.CompanyName,
                                  ClientBrandName = g.BrandName,
                                  Status = "Open",
                                  Asf = b.OtherFeePercentage,
                                  BillPercent = 100,
                                  AffiliationId = divisi.AffiliationId,
                                  //InvoiceBilled = (b.Subtotal + b.OtherFee),
                                  //PceBalance = (b.Subtotal + b.OtherFee),
                                  JobId = c.Id,
                                  JobName = c.JobName,
                                  JobStatus = c.JobStatusId,
                                  LegalId = d.Id,
                                  LegalEntity = d.LegalEntityName,
                                  DivisionId = divisi.Id,
                                  Division = divisi.UnitName,
                                  ClientId = h.Id,
                                  BrandId = g.Id,
                                  b.CurrencyId,
                                  MainServiceCategory = e.Id,
                                  MainServiceCategoryName = e.Name,
                                  b.CreatedOn,
                                  TermOfPayment = g.TermOfPayment,
                                  ClientInvAddress = g.BrandAddress,
                                  ClientTaxNumber = h.TaxRegistrationNumber,
                                  ClientPIC = g.ContactName
                              }).AsNoTracking().ToListAsync();

            var return_data = pces.Where(x => !pceid.Any(y => y == x.PceId)).ToList();
            return return_data.OrderByDescending(x => x.CreatedOn);
        }

        public async Task<dynamic> ReadInvHistory(string multipceid)
        {
            var gg = await (from a in context.InvoiceClients
                            join b in context.Pces on a.PceId equals b.Id
                            join c in context.JobDetail on b.JobId equals c.Id
                            where a.MultiPceId != null && a.MultiPceId == multipceid
                            select new
                            {
                                a.Id,
                                a.PceId,
                                a.MultiPceId,
                                a.TotalAmount,
                                AsfPercent = a.Asf,
                                a.AsfAmount,
                                a.GrandTotal,
                                a.Vat,
                                c.JobName
                            }).ToListAsync();

            return gg.OrderBy(x => decimal.Parse((x.MultiPceId ?? "0").Substring(4)));
        }

        public Task<List<InvoiceClient>> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        private async Task<InvoiceClient> ProcessUpload(InvoiceClient entity)
        {
            var file1 = await uploadFile.Upload(entity.VatDocFile, null);
            if (!string.IsNullOrEmpty(file1))
            {
                entity.VatDocFileId = file1;
            }

            var file2 = await uploadFile.Upload(entity.PoDocFile, null);
            if (!string.IsNullOrEmpty(file2))
            {
                entity.PoDocFileId = file2;
            }
            return entity;
        }

        public async Task<InvoiceClient> InsertInvoiceClient(InvoiceClient model)
        {
            int savedCount = 0;
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var pces = from a in context.Pces join b in model.PceListMultiple on a.Code equals b.PceId select a;
                    List<AccountReceivable> listar = new List<AccountReceivable>();

                    string id = await TransID.GetTransId(context, Code.InvoiceClientCode, DateTime.Today);
                    model.MultiPceId = id;
                    string multiplepceid = Guid.NewGuid().ToString();
                    foreach (var _item in model.PceListMultiple)
                    {
                        var item = await ProcessUpload(model);
                        Pce pce = pces.Where(x => x.Id == _item.PceId).FirstOrDefault();
                        item.Id = Guid.NewGuid().ToString();
                        item.MultiPceId = id;
                        item.InvoiceNumber = id;
                        item.Status = SingleInvoiceClientStatus.Open.ToString();
                        item.TotalAmount = _item.PceSubtotal;
                        item.AsfAmount = _item.PceOtherFee;
                        item.Asf = pce.OtherFeePercentage;
                        item.Vat = model.Vat;
                        item.GrandTotal = _item.Subtotals;//model.GrandTotal;
                        item.PceId = _item.PceId;
                        item.BrandId = model.BrandId;
                        item.ClientId = model.ClientId;
                        item.BusinessUnitId = model.BusinessUnitId;
                        item.CurrencyId = model.CurrencyId;
                        item.LegalId = model.LegalId;
                        item.MainServiceCategory = model.MainServiceCategory;
                        item.POClientNo = model.POClientNo;
                        item.AccountId = model.AccountId;
                        item.PayDescription = model.PayDescription;
                        item.AffiliationId = model.AffiliationId;
                        item.VatNo = model.VatNo;
                        item.InvoiceDate = model.InvoiceDate;
                        item.YourRef = model.YourRef;
                        item.RefNumber = model.RefNumber;
                        item.Remarks = model.Remarks;
                        item.JobId = pce.JobId;
                        item.InvoiceBilled = _item.InvoiceBilled;
                        await context.PhoenixAddAsync(item);
                    }
                    //await context.PhoenixAddRangeAsync(model.PceListMultiple.ToArray());
                    savedCount = await context.SaveChangesAsync();

                    foreach (var item in model.InvoiceMultipleListOther)
                    {
                        var models = model.PceListMultiple.Where(a => a.PceId == item.PceID).FirstOrDefault();
                        item.Id = Guid.NewGuid().ToString();
                        item.InvoiceId = id;
                        item.PceID = models.PceId;
                        item.JobID = models.JobId;
                        item.ParentId = models.Id;
                        item.TaskId = item.TaskId;
                        await context.AddAsync(item);
                    }
                    savedCount += await context.SaveChangesAsync();

                    //var refc = model.PceListMultiple.FirstOrDefault();
                    #region multirowar
                    //foreach (var item in pces)
                    //{
                    //    AccountReceivable accountReceivable = new AccountReceivable()
                    //    {
                    //        //Id = id,
                    //        Id = Guid.NewGuid().ToString(),
                    //        AccountReceivableNumber = id,
                    //        Amount = item.Subtotal + item.OtherFee,
                    //        BrandId = refc.BrandId,
                    //        JobId = item.JobId,
                    //        LegalEntityId = refc.LegalId,
                    //        Status = ARStatus.Open,
                    //        BankAccountDestination = refc.AccountId,
                    //        ExchangeRate = refc.ExchangeRate == null ? (decimal?)1.0 : refc.ExchangeRate,
                    //        CurrencyId = refc.CurrencyId,
                    //        TransactionDate = refc.InvoiceDate,
                    //        //DueDate = refc.DueDate,
                    //        BusinessUnitId = refc.BusinessUnitId
                    //    };
                    //    await context.PhoenixAddAsync(accountReceivable);
                    //    //listar.Add(accountReceivable);
                    //}
                    #endregion
                    //await context.PhoenixAddRangeAsync(listar.ToArray());

                    await log.AddAsync(new StatusLog() { TransactionId = id, Status = SingleInvoiceClientStatus.StatusName(SingleInvoiceClientStatus.Open), Description = model.PayDescription  });
                    transaction.Commit();
                    return model;
                }
                catch (Exception ex)
                {
                    savedCount = 0;
                    transaction.Rollback();
                    throw new Exception(ex.Message.ToString());
                }
            }
        }


        public async Task<int> InsertAndSubmitInvoiceClient(InvoiceClient model)
        {
            int savedCount = 0;
            var isApprove = true;
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var pces = from a in context.Pces join b in model.PceListMultiple on a.Code equals b.PceId select a;
                    List<AccountReceivable> listar = new List<AccountReceivable>();

                    string id = await TransID.GetTransId(context, Code.InvoiceClientCode, DateTime.Today);
                    string multiplepceid = Guid.NewGuid().ToString();

                    foreach (var _item in model.PceListMultiple)
                    {
                        var item = await ProcessUpload(model);
                        Pce pce = pces.Where(x => x.Id == _item.PceId).FirstOrDefault();

                        item.Id = Guid.NewGuid().ToString();
                        item.MultiPceId = id;
                        item.InvoiceNumber = id;
                        item.Status = SingleInvoiceClientStatus.WaitingApproval.ToString();
                        item.TotalAmount = _item.PceSubtotal;
                        item.AsfAmount = _item.PceOtherFee;
                        item.Asf = pce.OtherFeePercentage;
                        item.Vat = model.Vat;
                        item.GrandTotal = _item.Subtotals;//model.GrandTotal;
                        item.PceId = _item.PceId;
                        item.BrandId = model.BrandId;
                        item.ClientId = model.ClientId;
                        item.BusinessUnitId = model.BusinessUnitId;
                        item.CurrencyId = model.CurrencyId;
                        item.LegalId = model.LegalId;
                        item.MainServiceCategory = model.MainServiceCategory;
                        item.POClientNo = model.POClientNo;
                        item.AccountId = model.AccountId;
                        item.PayDescription = model.PayDescription;
                        item.AffiliationId = model.AffiliationId;
                        item.VatNo = model.VatNo;
                        item.InvoiceDate = model.InvoiceDate;
                        item.YourRef = model.YourRef;
                        item.RefNumber = model.RefNumber;
                        item.Remarks = model.Remarks;
                        item.JobId = pce.JobId;
                        item.InvoiceBilled = _item.InvoiceBilled;
                        await context.PhoenixAddAsync(item);
                    }
                    savedCount += await context.SaveChangesAsync();

                    foreach (var item in model.InvoiceMultipleListOther)
                    {
                        var models = model.PceListMultiple.Where(a => a.PceId == item.PceID).FirstOrDefault();
                        item.Id = Guid.NewGuid().ToString();
                        item.InvoiceId = id;
                        item.PceID = models.PceId;
                        item.JobID = models.JobId;
                        item.ParentId = model.Id;
                        item.TaskId = item.TaskId;
                        await context.AddAsync(item);
                    }

                    savedCount += await context.SaveChangesAsync();

                    var refc = model.PceListMultiple.FirstOrDefault();

                    #region multirowar
                    //foreach (var item in pces)
                    //{
                    //    AccountReceivable accountReceivable = new AccountReceivable()
                    //    {
                    //        //Id = id,
                    //        Id = Guid.NewGuid().ToString(),
                    //        AccountReceivableNumber = id,
                    //        Amount = item.Subtotal + item.OtherFee,
                    //        BrandId = refc.BrandId,
                    //        JobId = item.JobId,
                    //        LegalEntityId = refc.LegalId,
                    //        Status = ARStatus.Open,
                    //        BankAccountDestination = refc.AccountId,
                    //        ExchangeRate = refc.ExchangeRate == null ? (decimal?)1.0 : refc.ExchangeRate,
                    //        CurrencyId = refc.CurrencyId,
                    //        TransactionDate = refc.InvoiceDate,
                    //        //DueDate = refc.DueDate,
                    //        BusinessUnitId = refc.BusinessUnitId
                    //    };
                    //    await context.PhoenixAddAsync(accountReceivable);
                    //    //listar.Add(accountReceivable);
                    //}
                    #endregion

                    //approval process request to system

                    var dataMenu = await menuService.GetByUniqueName(MenuUnique.InvoiceClientMultiplePce).ConfigureAwait(false);
                    var vm = new Models.ViewModel.TransApprovalHrisVm()
                    {
                        MenuId = dataMenu.Id,
                        RefId = id,
                        DetailLink = $"{ApprovalLink.InvoiceClientMultiplePce}?Id={id}&isApprove=true",
                        //Tname = "fn.PurchaseRequest",//"SP#NAMA SP NYA",
                        IdTemplate = ""
                    };
                    var subGroupId = ClainJwtPrincipalHelper.GetBusinessSubGroup(context.HttpContext);
                    var divisionId = ClainJwtPrincipalHelper.GetBusinessUnitDivisi(context.HttpContext);
                    var employeeId = ClainJwtPrincipalHelper.GetEmployeeId(context.HttpContext);
                    isApprove = await globalFcApproval.Save(vm, subGroupId, divisionId, employeeId);


                    if (isApprove)
                    {
                        savedCount += await context.SaveChangesAsync();
                        await log.AddAsync(new StatusLog() { TransactionId = id, Status = SingleInvoiceClientStatus.StatusName(SingleInvoiceClientStatus.WaitingApproval), Description = model.PayDescription });
                        transaction.Commit();
                        return savedCount;
                    }
                    else
                    {
                        transaction.Rollback();
                        throw new Exception("please check the approval template that will be processed");
                    }
                }
                catch (Exception ex)
                {
                    savedCount = 0;
                    transaction.Rollback();
                    throw new Exception(ex.Message.ToString());
                }
            }
        }

        public async Task<int> EditAndSubmitAsync(InvoiceClient model)
        {
            int savedCount = 0;
            var isApprove = true;
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var pces = from a in context.Pces join b in model.PceListMultiple on a.Code equals b.PceId select a;
                    List<AccountReceivable> listar = new List<AccountReceivable>();

                    model.Status = SingleInvoiceClientStatus.WaitingApproval.ToString();

                    var additional = await context.InvoiceClients.AsNoTracking().Where(x => x.MultiPceId == model.InvoiceNumber).ToListAsync();
                    if (additional.Count > 0)
                    {
                        context.InvoiceClients.RemoveRange(additional);
                    }

                    List<InvoiceClient> dt = new List<InvoiceClient>();
                    foreach (var _item in model.PceListMultiple)
                    {
                        _item.VatDocFile = model.VatDocFile;
                        _item.PoDocFile = model.PoDocFile;

                        var item = await ProcessUpload(_item);
                        if (string.IsNullOrEmpty(item.PoDocFileId))
                            item.PoDocFileId = model.PoDocFileId;

                        if (string.IsNullOrEmpty(item.VatDocFileId))
                            item.VatDocFileId = model.VatDocFileId;

                        Pce pce = pces.Where(x => x.Id == _item.PceId).FirstOrDefault();
                        item.Id = Guid.NewGuid().ToString();
                        item.InvoiceNumber = model.InvoiceNumber;
                        item.MultiPceId = model.InvoiceNumber;
                        item.Status = SingleInvoiceClientStatus.WaitingApproval.ToString();
                        item.TotalAmount = _item.PceSubtotal;
                        item.AsfAmount = _item.PceOtherFee;
                        item.Asf = pce.OtherFeePercentage;
                        item.Vat = model.Vat;
                        item.GrandTotal = _item.Subtotals;//model.GrandTotal;
                        item.PceId = _item.PceId;
                        item.BrandId = model.BrandId;
                        item.ClientId = model.ClientId;
                        item.BusinessUnitId = model.BusinessUnitId;
                        item.CurrencyId = model.CurrencyId;
                        item.LegalId = model.LegalId;
                        item.MainServiceCategory = model.MainServiceCategory;
                        item.POClientNo = model.POClientNo;
                        item.AccountId = model.AccountId;
                        item.PayDescription = model.PayDescription;
                        item.AffiliationId = model.AffiliationId;
                        item.VatNo = model.VatNo;
                        item.InvoiceDate = model.InvoiceDate;
                        item.YourRef = model.YourRef;
                        item.RefNumber = model.RefNumber;
                        item.Remarks = model.Remarks;
                        item.JobId = pce.JobId;
                        item.InvoiceBilled = _item.InvoiceBilled;
                        dt.Add(item);
                    }
                   
                    InvoiceClient[] dtg = dt.ToArray();
                    await context.PhoenixAddRangeAsync(dtg);
                    
                    var additionalOther = await context.InvoiceClientItemOthers.AsNoTracking().Where(x => x.InvoiceId == model.InvoiceNumber).ToListAsync();
                    if (additionalOther.Count > 0)
                    {
                        context.InvoiceClientItemOthers.RemoveRange(additionalOther);
                    }

                    List<InvoiceClientItemOther> dtO = new List<InvoiceClientItemOther>();
                    foreach (var _item in model.InvoiceMultipleListOther)
                    {
                        //var models = model.PceListMultiple.Where(a => a.PceId == itemD.PceID).FirstOrDefault();
                        //_item.Id = Guid.NewGuid().ToString();
                        _item.InvoiceId = model.InvoiceNumber;
                        //itemD.PceID = itemD.PceID;
                        //itemD.JobID = model.JobId;
                        _item.ParentId = model.Id;
                        //itemD.TaskId = itemD.TaskId;
                        dtO.Add(_item);

                        //var models = model.PceListMultiple.Where(a => a.PceId == _item.PceID).FirstOrDefault();
                        //_item.Id = Guid.NewGuid().ToString();
                        //_item.InvoiceId = models.MultiPceId;
                        //_item.PceID = models.PceId;
                        //_item.JobID = models.JobId;
                        //_item.ParentId = models.Id;
                        //_item.TaskId = _item.TaskId;
                        //dtO.Add(_item);
                    }
                    InvoiceClientItemOther[] dtgO = dtO.ToArray();
                    await context.PhoenixAddRangeAsync(dtgO);

                    //var refc = model.PceListMultiple.FirstOrDefault();
                    #region multirowar
                    //foreach (var item in pces)
                    //{
                    //    AccountReceivable accountReceivable = new AccountReceivable()
                    //    {
                    //        //Id = id,
                    //        Id = Guid.NewGuid().ToString(),
                    //        AccountReceivableNumber = id,
                    //        Amount = item.Subtotal + item.OtherFee,
                    //        BrandId = refc.BrandId,
                    //        JobId = item.JobId,
                    //        LegalEntityId = refc.LegalId,
                    //        Status = ARStatus.Open,
                    //        BankAccountDestination = refc.AccountId,
                    //        ExchangeRate = refc.ExchangeRate == null ? (decimal?)1.0 : refc.ExchangeRate,
                    //        CurrencyId = refc.CurrencyId,
                    //        TransactionDate = refc.InvoiceDate,
                    //        //DueDate = refc.DueDate,
                    //        BusinessUnitId = refc.BusinessUnitId
                    //    };
                    //    await context.PhoenixAddAsync(accountReceivable);
                    //    //listar.Add(accountReceivable);
                    //}
                    #endregion

                    //approval process request to system

                    var dataMenu = await menuService.GetByUniqueName(MenuUnique.InvoiceClientMultiplePce).ConfigureAwait(false);
                    var vm = new Models.ViewModel.TransApprovalHrisVm()
                    {
                        MenuId = dataMenu.Id,
                        RefId = model.MultiPceId,
                        DetailLink = $"{ApprovalLink.InvoiceClientMultiplePce}?Id={model.MultiPceId}&isApprove=true",
                        //Tname = "fn.PurchaseRequest",//"SP#NAMA SP NYA",
                        IdTemplate = ""
                    };
                    var subGroupId = ClainJwtPrincipalHelper.GetBusinessSubGroup(context.HttpContext);
                    var divisionId = ClainJwtPrincipalHelper.GetBusinessUnitDivisi(context.HttpContext);
                    var employeeId = ClainJwtPrincipalHelper.GetEmployeeId(context.HttpContext);
                    isApprove = await globalFcApproval.Save(vm, subGroupId, divisionId, employeeId);


                    if (isApprove)
                    {
                        await log.AddAsync(new StatusLog() { TransactionId = model.InvoiceNumber, Status = SingleInvoiceClientStatus.StatusName(SingleInvoiceClientStatus.Open), Description = model.PayDescription });
                        savedCount += await context.SaveChangesAsync();
                        transaction.Commit();
                        return savedCount;
                    }
                    else
                    {
                        transaction.Rollback();
                        throw new Exception("please check the approval template that will be processed");
                    }
                }
                catch (Exception ex)
                {
                    savedCount = 0;
                    transaction.Rollback();
                    throw new Exception(ex.Message.ToString());
                }
            }
        }


        public async Task<string> GetInfoBank(string idbank)
        {
            var aa = await (from a in context.Banks where a.Id == idbank select a.AccountNumber).FirstOrDefaultAsync();
            return aa;
        }

        public async Task<InvoiceClient> Get(string id) => await context.InvoiceClients.AsNoTracking().SingleOrDefaultAsync(x => !(x.IsDeleted ?? false) && x.Id == id);

        //CUD
        public async Task<int> AddAsync(InvoiceClient entity)
        {
            entity.Id = Guid.NewGuid().ToString();
            await context.PhoenixAddAsync(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteAsync(InvoiceClient entity)
        {
            context.PhoenixApprove(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteSoftAsync(InvoiceClient entity)
        {
            context.PhoenixDelete(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<InvoiceClient> EditAsyncData(InvoiceClient model)
        {
            int savedCount = 0;
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var pces = from a in context.Pces join b in model.PceListMultiple on a.Code equals b.PceId select a;
                    List<AccountReceivable> listar = new List<AccountReceivable>();

                    //model.Status = SingleInvoiceClientStatus.Open.ToString();

                    var additional = await context.InvoiceClients.AsNoTracking().Where(x => x.MultiPceId == model.InvoiceNumber).ToListAsync();
                    if (additional.Count > 0)
                    {
                        context.InvoiceClients.RemoveRange(additional);
                    }
                    List<InvoiceClient> dt = new List<InvoiceClient>();
                    foreach (var _item in model.PceListMultiple)
                    {
                        _item.VatDocFile = model.VatDocFile;
                        _item.PoDocFile = model.PoDocFile;
                        var item = await ProcessUpload(_item);

                        if (string.IsNullOrEmpty(item.PoDocFileId))
                            item.PoDocFileId = model.PoDocFileId;

                        if (string.IsNullOrEmpty(item.VatDocFileId))
                            item.VatDocFileId = model.VatDocFileId;

                        Pce pce = pces.Where(x => x.Id == _item.PceId).FirstOrDefault();
                        item.Id = _item.Id;
                        item.InvoiceNumber = model.InvoiceNumber;
                        item.MultiPceId = model.InvoiceNumber;
                        item.Status = SingleInvoiceClientStatus.Open.ToString();
                        item.TotalAmount = _item.PceSubtotal;
                        item.AsfAmount = _item.PceOtherFee;
                        item.Asf = pce.OtherFeePercentage;
                        item.Vat = model.Vat;
                        item.GrandTotal = _item.Subtotals;//model.GrandTotal;
                        item.PceId = _item.PceId;
                        item.BrandId = model.BrandId;
                        item.ClientId = model.ClientId;
                        item.BusinessUnitId = model.BusinessUnitId;
                        item.CurrencyId = model.CurrencyId;
                        item.LegalId = model.LegalId;
                        item.MainServiceCategory = model.MainServiceCategory;
                        item.POClientNo = model.POClientNo;
                        item.AccountId = model.AccountId;
                        item.PayDescription = model.PayDescription;
                        item.AffiliationId = model.AffiliationId;
                        item.VatNo = model.VatNo;
                        item.InvoiceDate = model.InvoiceDate;
                        item.YourRef = model.YourRef;
                        item.RefNumber = model.RefNumber;
                        item.Remarks = model.Remarks;
                        item.JobId = pce.JobId;
                        item.InvoiceBilled = _item.InvoiceBilled;
                        dt.Add(item);
                    }

                    InvoiceClient[] dtg = dt.ToArray();
                    await context.PhoenixAddRangeAsync(dtg);

                    var additionalOther = await context.InvoiceClientItemOthers.AsNoTracking().Where(x => x.InvoiceId == model.InvoiceNumber).ToListAsync();
                    if (additionalOther.Count > 0)
                    {
                        context.InvoiceClientItemOthers.RemoveRange(additionalOther);
                    }

                    List<InvoiceClientItemOther> dtO = new List<InvoiceClientItemOther>();
                    foreach (var itemD in model.InvoiceMultipleListOther)
                    {
                        //var models = model.PceListMultiple.Where(a => a.PceId == itemD.PceID).FirstOrDefault();
                        //itemD.Id = Guid.NewGuid().ToString();
                        itemD.InvoiceId = model.InvoiceNumber;
                        //itemD.PceID = itemD.PceID;
                        //itemD.JobID = model.JobId;
                        itemD.ParentId = model.Id;
                        //itemD.TaskId = itemD.TaskId;
                        dtO.Add(itemD);
                    }
                    InvoiceClientItemOther[] dtgO = dtO.ToArray();
                    await context.PhoenixAddRangeAsync(dtgO);

                    await log.AddAsync(new StatusLog() { TransactionId = model.InvoiceNumber, Status = SingleInvoiceClientStatus.StatusName(SingleInvoiceClientStatus.Open), Description = model.PayDescription });

                    savedCount += await context.SaveChangesAsync();
                    transaction.Commit();
                    return model;
                }
                catch (Exception ex)
                {
                    savedCount = 0;
                    transaction.Rollback();
                    throw new Exception(ex.Message.ToString());
                }
            }
        }

        public async Task<int> UnsafeProcessApproval(string Id, int? status, string remarks, InvoiceClient model)
        {
            //foreach (var _item in model.PceListMultiple)
            //{
            //    context.PhoenixApprove(_item);
            //}

            var vm = new TrUpdateStatusApprovalViewModel()
            {
                RefId = Id,
                StatusApproved = status ?? 0,
                Remarks = remarks
            };

            var employeeId = Phoenix.Shared.Core.PrincipalHelpers.ClainJwtPrincipalHelper.GetEmployeeId(context.HttpContext);
            var isApprove = await globalFcApproval.UpdateStatusApproval(vm, employeeId);

            if (isApprove)
            {
                return await context.SaveChangesAsync();
            }
            else
            {
                throw new Exception();
            }
        }

        public async Task<InvoiceClient> Approve(InvoiceClient model)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    model.Status = StatusTransaction.Approved.ToString();
                    //await globalFcApproval.ProcessApproval<InvoiceClient>(entity.Id, Convert.ToInt32(entity.Status), entity.RemarkRejected, entity);
                    var saveApprove = await UnsafeProcessApproval(model.MultiPceId, Convert.ToInt32(model.Status), model.RemarkRejected, model);

                    var modeltrapp = await context.TrTemplateApprovals.Where(x => x.RefId == model.MultiPceId).FirstOrDefaultAsync();
                    if (modeltrapp != null)
                    {
                        if (modeltrapp.StatusApprovedDescription == StatusTransaction.StatusName(StatusTransaction.Approved))
                        {
                            // todo final approval
                            //var invoices = await (from a in context.InvoiceClients where a.MultiPceId == null && a.PceId == entity.PceId select a.GrandTotal).ToListAsync();
                            //var tagihan = invoices.Sum();
                            //if (tagihan + entity.GrandTotal >= entity.PceGrandTotal)
                            //    entity.Status = SingleInvoiceClientStatus.Closed.ToString();
                            //else
                            //entity.Status = SingleInvoiceClientStatus.Balance.ToString();
                            model.Status = SingleInvoiceClientStatus.Closed.ToString();
                        }
                        else
                        {
                            model.Status = SingleInvoiceClientStatus.WaitingApproval.ToString();
                        }
                    }
                    else
                    {
                        model.Status = SingleInvoiceClientStatus.WaitingApproval.ToString();
                    }

                    //var pces = from a in context.Pces join b in model.PceListMultiple on a.Code equals b.PceId select a;

                    List<InvoiceClient> dtH = new List<InvoiceClient>();
                    //var existingdatas = context.InvoiceClients.Where(x => x.MultiPceId == model.MultiPceId).ToList();
                    foreach (var item in model.PceListMultiple)
                    {
                        //var existingdata = existingdatas.Where(a => a.PceId == item.PceId).FirstOrDefault();
                        var existingdata = await context.InvoiceClients.AsNoTracking().Where(x => x.PceId == item.PceId).FirstOrDefaultAsync();
                        //InvoiceClient existingdata = context.InvoiceClients.Where(x => x.PceId == item.PceId).FirstOrDefault();
                        //Pce pce = pces.Where(x => x.Id == item.PceId).FirstOrDefault();
                        //item.Id = model.Id;
                        //existingdata.MultiPceId = model.InvoiceNumber;
                        existingdata.Status = model.Status;
                        //existingdata.TotalAmount = pce.Subtotal;
                        //existingdata.AsfAmount = pce.OtherFee;
                        //existingdata.Vat = model.Vat;
                        //existingdata.GrandTotal = model.GrandTotal;
                        //existingdata.PceId = item.PceId;
                        //item.BrandId = existingdata.BrandId;
                        //item.ClientId = existingdata.ClientId;
                        //item.BusinessUnitId = existingdata.BusinessUnitId;
                        //item.CurrencyId = existingdata.CurrencyId;
                        //item.LegalId = existingdata.LegalId;
                        //item.MainServiceCategory = existingdata.MainServiceCategory;
                        //existingdata.POClientNo = model.POClientNo;
                        //item.AccountId = existingdata.AccountId;
                        //existingdata.PayDescription = model.PayDescription;
                        // item.AffiliationId = existingdata.AffiliationId;
                        //existingdata.VatNo = model.VatNo;
                        //item.InvoiceDate = model.InvoiceDate;
                        //item.YourRef = model.YourRef;
                        //item.RefNumber = model.RefNumber;
                        //item.Remarks = model.Remarks;
                        //item.JobId = pce.JobId;
                        //dtH.Add(existingdata);
                        context.PhoenixApprove(existingdata);
                    }
                    //InvoiceClient[] dtgO = dtH.ToArray();
                    //context.PhoenixEditRange(dtgO);

                    if (model.Status == SingleInvoiceClientStatus.Closed.ToString() || model.Status == SingleInvoiceClientStatus.Balance.ToString())
                    {
                        AccountReceivable accountReceivable = new AccountReceivable()
                        {
                            AccountReceivableNumber = model.MultiPceId,
                            Id = model.MultiPceId,
                            Amount = model.GrandTotal,
                            BrandId = model.BrandId,
                            JobId = model.JobId,
                            LegalEntityId = model.LegalId,
                            AffilitionId = model.AffiliationId,
                            Status = ARStatus.Open,
                            BankAccountDestination = model.AccountId,
                            ExchangeRate = model.ExchangeRate == null ? 0 : model.ExchangeRate,
                            CurrencyId = model.CurrencyId,
                            TransactionDate = model.InvoiceDate,
                            BusinessUnitId = model.BusinessUnitId,
                            VatNo = model.VatNo,
                            ClientId = model.ClientId
                        };

                        await context.PhoenixAddAsync(accountReceivable);
                    }

                    await log.AddAsync(new StatusLog() { TransactionId = model.MultiPceId, Status = SingleInvoiceClientStatus.StatusName(Convert.ToInt32(model.Status)), Description = model.PayDescription });
                    var save = await context.SaveChangesAsync();
                    transaction.Commit();
                    return model;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message.ToString());
                }
            }
        }

        public async Task<InvoiceClient> Reject(InvoiceClient entity)
        {
            try
            {
                entity.Status = SingleInvoiceClientStatus.Rejected.ToString();
                //await globalFcApproval.ProcessApproval<InvoiceClient>(entity.Id, Convert.ToInt32(entity.Status), entity.RemarkRejected, entity);
                var saveReject = await UnsafeProcessApproval(entity.MultiPceId, Convert.ToInt32(entity.Status), "", entity);

                var modeltrapp = await context.TrTemplateApprovals.AsNoTracking().Where(x => x.RefId == entity.Id).FirstOrDefaultAsync();
                if (modeltrapp != null)
                {
                    if (modeltrapp.StatusApprovedDescription == SingleInvoiceClientStatus.StatusName(SingleInvoiceClientStatus.Rejected))
                    {
                        entity.Status = SingleInvoiceClientStatus.Rejected.ToString();
                        entity.Remarks = entity.RemarkRejected;
                    }
                }
                else
                {
                    entity.Status = SingleInvoiceClientStatus.Rejected.ToString();
                }

                var pces = from a in context.Pces join b in entity.PceListMultiple on a.Code equals b.PceId select a;
                foreach (var item in entity.PceListMultiple)
                {
                    Pce pce = pces.Where(x => x.Id == item.PceId).FirstOrDefault();
                    item.Id = entity.Id;
                    item.MultiPceId = entity.InvoiceNumber;
                    item.Status = SingleInvoiceClientStatus.WaitingApproval.ToString();
                    item.TotalAmount = pce.Subtotal;
                    item.AsfAmount = pce.OtherFee;
                    item.Vat = entity.Vat;
                    item.GrandTotal = entity.GrandTotal;
                    item.PceId = item.PceId;
                    item.BrandId = entity.BrandId;
                    item.ClientId = entity.ClientId;
                    item.BusinessUnitId = entity.BusinessUnitId;
                    item.CurrencyId = entity.CurrencyId;
                    item.LegalId = entity.LegalId;
                    item.MainServiceCategory = entity.MainServiceCategory;
                    item.POClientNo = entity.POClientNo;
                    item.AccountId = entity.AccountId;
                    item.PayDescription = entity.PayDescription;
                    item.AffiliationId = entity.AffiliationId;
                    item.VatNo = entity.VatNo;
                    item.InvoiceDate = entity.InvoiceDate;
                    item.YourRef = entity.YourRef;
                    item.RefNumber = entity.RefNumber;
                    item.Remarks = entity.Remarks;
                    item.JobId = pce.JobId;
                    context.PhoenixEdit(item);
                }
                await log.AddAsync(new StatusLog() { TransactionId = entity.MultiPceId, Status = SingleInvoiceClientStatus.StatusName(SingleInvoiceClientStatus.Rejected), Description = entity.RemarkRejected });
                var save = await context.SaveChangesAsync();
                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public async Task<List<Journal>> GetDataJournal(string Id)
        {
            try
            {
                var bp = await context.InvoiceClients.Where(x => x.IsDeleted.Equals(false) && x.MultiPceId == Id).FirstOrDefaultAsync();
                var tapname = "";
                if (bp != null)
                {
                    if (string.IsNullOrEmpty(bp.JobId))
                    {
                        tapname = "";
                    }
                    else
                    {
                        tapname = "PROJECT - " + bp.JobId;
                    }
                }
                else
                {
                    tapname = "INTERNAL";
                }

                var data = await (from tb in context.JournalTemps
                                  join coa in context.ChartOfAccounts on tb.AccountId equals coa.Id
                                  join bu in context.BusinessUnits on tb.DivisiId equals bu.Id into arc
                                  from buu in arc.DefaultIfEmpty()
                                  where tb.ReferenceId == Id
                                  orderby tb.Credit descending
                                  orderby tb.Debit descending
                                  select new Journal
                                  {
                                      AccountId = coa.Id,
                                      AccountCode = coa.CodeRec,
                                      AccountName = coa.Name5,
                                      JobId = tb.JobId,
                                      Description = tb.Description == null ? coa.Name5 : tb.Description,
                                      JobIdName = tapname,
                                      DivisiId = tb.DivisiId,
                                      DivisiName = buu.UnitName,
                                      Credit = tb.Credit,
                                      Debit = tb.Debit,
                                      LineCode = tb.LineCode,
                                  }).ToListAsync();


                return data.OrderByDescending(x => x.CreatedOn).ToList();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public async Task<int> CreateJournalWithSP(string Id)
        {
            try
            {
                var modeldel = await context.JournalTemps.Where(x => x.IsDeleted.Equals(false) && x.ReferenceId == Id).ToListAsync();
                if (modeldel != null)
                {
                    List<JournalTemp> jnd = new List<JournalTemp>();
                    foreach (JournalTemp data in modeldel)
                    {
                        jnd.Add(data);
                    }
                    JournalTemp[] jndt = jnd.ToArray();
                    context.PhoenixDeleteRange(jndt);
                }
                var result = await context.Database.ExecuteSqlCommandAsync("EXECUTE fn.create_jurnal_ar_multi2_temp @p_account_receivable_id", parameters: new[] { new SqlParameter("@p_account_receivable_id", Id) });
                var save = await context.SaveChangesAsync();
                return save;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }


        public Task<List<InvoiceClient>> Get()
        {
            throw new NotImplementedException();
        }

        public Task<int> AddRangeAsync(params InvoiceClient[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> EditRangeAsync(InvoiceClient entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteRageAsync(params InvoiceClient[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteSoftRangeAsync(params InvoiceClient[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> EditAsync(InvoiceClient entity)
        {
            throw new NotImplementedException();
        }

        public async Task<List<InvoiceClientItemOther>> GetInvoiceProductionDetail(string pceid, string invNo)
        {
            List<InvoiceClientItemOther> retData = new List<InvoiceClientItemOther>();
            if (pceid != "0" && invNo != "0")
            {
                retData = await (
                    from a in context.PceTasks
                    join e in context.InvoiceClientItemOthers on a.Id equals e.TaskId
                    where a.PceId == pceid && e.InvoiceId == invNo
                    select new InvoiceClientItemOther()
                    {
                        Id = e.Id,
                        TaskId = a.Id,
                        TaskName = a.TaskName,
                        Quantity = a.Quantity,
                        UnitPrice = a.UnitPrice,
                        Total = a.Total,
                        AmountPercentage = e.AmountPercentage,
                        Amount = e.Amount,
                        AmountBilled = e.AmountBilled,
                        Balance = (e.Quantity * e.UnitPrice) - e.AmountBilled,
                        PceID = a.PceId,
                        TotalBillPercent = 0
                    }).AsNoTracking().ToListAsync();
            }
            else
            {
                if (invNo != "0")
                {
                    retData = await (
                        from a in context.InvoiceClientItemOthers
                        where a.InvoiceId == invNo
                        select new InvoiceClientItemOther()
                        {
                            Id = a.Id,
                            TaskId = a.Id,
                            TaskName = a.TaskName,
                            Quantity = a.Quantity,
                            UnitPrice = a.UnitPrice,
                            Total = a.Quantity * a.UnitPrice,
                            //AmountPercentage = a.AmountPercentage,
                            Amount = a.Quantity ?? 0 * a.UnitPrice ?? 0,
                            AmountBilled = a.AmountBilled,
                            Balance = (a.Quantity * a.UnitPrice) - a.AmountBilled,
                            PceID = a.PceID,
                        }).AsNoTracking().ToListAsync();
                }
                else if (pceid != "0")
                {
                    retData = await (
                        from a in context.PceTasks
                        where a.PceId == pceid
                        select new InvoiceClientItemOther()
                        {
                            Id = a.Id,
                            TaskId = a.Id,
                            TaskName = a.TaskName,
                            Quantity = a.Quantity,
                            UnitPrice = a.UnitPrice,
                            Total = a.Quantity * a.UnitPrice,
                            Amount = a.Quantity ?? 0 * a.UnitPrice ?? 0,
                            //AmountBilled = 0,
                            //Balance = 0,
                            PceID = a.PceId
                        }).AsNoTracking().ToListAsync();
                }
            }

            return retData;
        }
    }
}
