﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Attributes;
using Phoenix.Data.Models;
using Phoenix.Data.Models.Finance;
using Phoenix.Data.Models.Finance.Transaction;
using Phoenix.Data.Services.Um;
using Phoenix.Shared.Core.PrincipalHelpers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.Data.Services.Finance
{
    public interface ICashAdvanceSattlementService : IDataService<CashAdvanceSettlement>
    {
        Task<List<CashAdvanceSettlement>> Get(Dictionary<string, string> filters);
        Task<CashAdvanceSettlement> GetSattlementById(string Id);
        Task<List<CashAdvanceSettlementDetail>> GetSattlementDetailById(string Id);
        Task<List<Journal>> GetJournalDataBySattlementId(string Id);
        Task<int> AddSattlement(CashAdvanceSattlementDTO model);
        Task<int> EditSattlement(CashAdvanceSattlementDTO model);
        Task<CashAdvanceSettlement> ApproveSattlement(CashAdvanceSettlement entity);
        Task<CashAdvanceSettlement> Reject(CashAdvanceSettlement entity);
        Task<int> CreateAutomaticJournalFromSattlementId(string Id);
        Task<CashAdvanceSettlementDetail> UploadFile(IFormFile file);
    }
    public class CashAdvanceSattlementService : ICashAdvanceSattlementService
    {
        readonly DataContext context;
        readonly GlobalFunctionApproval globalFcApproval;
        readonly IManageMenuService menuService;
        readonly FinancePeriodService financePeriod;
        readonly FileService uploadFile;
        readonly StatusLogService log;
        string idTemplate = "";

        public CashAdvanceSattlementService(DataContext context, GlobalFunctionApproval globalFcApproval, IManageMenuService menuService, FinancePeriodService financePeriod, FileService uploadFile, StatusLogService log)
        {
            this.context = context;
            this.globalFcApproval = globalFcApproval;
            this.menuService = menuService;
            this.financePeriod = financePeriod;
            this.uploadFile = uploadFile;
            this.log = log;
        }

        public Task<int> AddAsync(CashAdvanceSettlement entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> AddRangeAsync(params CashAdvanceSettlement[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteAsync(CashAdvanceSettlement entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteRageAsync(params CashAdvanceSettlement[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteSoftAsync(CashAdvanceSettlement entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteSoftRangeAsync(params CashAdvanceSettlement[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> EditAsync(CashAdvanceSettlement entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> EditRangeAsync(CashAdvanceSettlement entity)
        {
            throw new NotImplementedException();
        }

        public Task<List<CashAdvanceSettlement>> Get()
        {
            throw new NotImplementedException();
        }

        public Task<List<CashAdvanceSettlement>> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public Task<CashAdvanceSettlement> Get(string id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<CashAdvanceSettlement>> Get(Dictionary<string, string> filters)
        {
            var result = from st in context.CashAdvanceSettlement
                         join ca in context.CashAdvances on st.CashAdvanceId equals ca.Id
                         join job in context.JobDetail on st.JobId equals job.Id
                         join legal in context.LegalEntity on ca.LegalEntityId equals legal.Id into lca
                         from legal in lca.DefaultIfEmpty()
                         join div in context.BusinessUnits on ca.BusinessUnitId equals div.Id into dca
                         from div in dca.DefaultIfEmpty()
                         join divReq in context.BusinessUnits on ca.RequestBusinessUnitId equals divReq.Id into drca
                         from divReq in drca.DefaultIfEmpty()
                         join comp in context.Companys on ca.ClientName equals comp.Id into compTemp
                         from comps in compTemp.DefaultIfEmpty()
                         join mservice in context.MainServiceCategorys on ca.ProjectActivity equals mservice.Id into gmservice
                         from mservice in gmservice.DefaultIfEmpty()
                         where !(st.IsDeleted ?? false)
                         select new CashAdvanceSettlement
                         {
                             Id = st.Id,
                             CaNumber = ca.CaNumber,
                             CashAdvanceId = ca.Id,
                             CaDate = st.CashAdvanceSettleDate,
                             JobId = st.JobId,
                             JobName =job.JobName,
                             //JobNumber = st.JobNumber,
                             //RequestEmployeeId = st.RequestEmployeeId,
                             RequestEmployeeName = st.RequestEmployeeName,
                             //RemarkRejected = ca.RemarkRejected,
                             //RequestJobGradeId = ca.RequestJobGradeId,
                             //RequestJobGradeName = ca.RequestJobGradeName,
                             //RequestJobTitleId = ca.RequestJobTitleId,
                             //RequestJobTitleName = ca.RequestJobTitleName,
                             CaStatusId = st.CaStatusId,
                             StatusDesc = StatusTransaction.StatusName(Convert.ToInt32(st.CaStatusId)),// c.StatusApprovedDescription,
                             LegalEntityId = ca.LegalEntityId,
                             ProjectActivity = ca.ProjectActivity,
                             BusinessUnitName = comps.CompanyName,
                             RequestorDivision = divReq.UnitName,
                             LegalEntityName = legal.LegalEntityName,
                             MainServiceVategoryName = mservice.Name,
                             CaPeriodTo = ca.CaPeriodTo
                         };
            if (filters != null && filters.Count > 0)
            {
                foreach (var filter in filters)
                {
                    switch (filter.Key)
                    {
                        case "legal":
                            result = result.Where(x => x.LegalEntityId == filter.Value);
                            break;
                        case "svc":
                            result = result.Where(x => x.ProjectActivity == filter.Value);
                            break;
                    }
                }
            }
            //return await result.Where(x => ( x.RequestEmployeeId == employeeId || approval.Any(y => y.RefId == x.Id))).OrderByDescending(x => x.CreatedOn).ToListAsync();
            return await result.OrderByDescending(x => x.CreatedOn).ToListAsync();
        }

        public async Task<CashAdvanceSettlement> GetSattlementById(string Id)
        {
            var result = await context.CashAdvanceSettlement.Where(x => x.IsDeleted.Equals(false) && x.Id == Id).FirstOrDefaultAsync();
            return await GetFile(result);
        }

        private async Task<CashAdvanceSettlement> GetFile(CashAdvanceSettlement data)
        {
            if (data.FileReceipt != null)
            {
                var filemaster = await context.Filemasters.Where(x => x.Id == data.FileReceipt).FirstOrDefaultAsync();
                if (filemaster != null) data.FileReceiptName = filemaster.Name;
            }
            return data;
        }

        public async Task<List<CashAdvanceSettlementDetail>> GetSattlementDetailById(string Id)
        {
            return await (from x in context.CashAdvanceDetails
                          join a in context.CashAdvanceSettlementDetail on x.Id equals a.CashAdvanceDetailId into ga
                          from a in ga.DefaultIfEmpty()
                          join b in context.ChartOfAccounts on a.AccountId equals b.Id into gb
                          from b in gb.DefaultIfEmpty()
                          join m in context.Filemasters on a.Receipt equals m.Id into ma
                          from m in ma.DefaultIfEmpty()
                          where x.CashAdvanceId == Id
                          select new CashAdvanceSettlementDetail
                          {
                              Id = a.Id,
                              AccountCode = a.AccountCode,
                              AccountId = a.AccountId,
                              AccountName = string.IsNullOrEmpty(b.Name5) ? "" : b.Name5,
                              ActualAmount = a.ActualAmount,
                              ActualQty = x.Qty,
                              ActualUnitPrice = a.ActualUnitPrice,
                              ApprovedBy = a.ApprovedBy,
                              ApprovedOn = a.ApprovedOn,
                              CashAdvanceDetailId = x.Id,
                              CashAdvanceSettleId = a.CashAdvanceSettleId,
                              CreatedBy = a.CreatedBy,
                              CreatedOn = a.CreatedOn,
                              DeletedBy = a.DeletedBy,
                              DeletedOn = a.DeletedOn,
                              Receipt = a.Receipt,
                              IsActive = a.IsActive,
                              IsDefault = a.IsDefault,
                              IsDeleted = a.IsDeleted,
                              IsLocked = a.IsLocked,
                              ModifiedBy = a.ModifiedBy,
                              ModifiedOn = a.ModifiedOn,
                              OwnerId = a.OwnerId,
                              PurchaseDate = a.PurchaseDate,
                              Qty = a.Qty,
                              TaskId = x.TaskId,
                              TaskName = x.TaskDetail,
                              TaxPayable = a.TaxPayable,
                              TaxPayableCoaId = a.TaxPayableCoaId,
                              TaxPayablePercent = a.TaxPayablePercent,
                              AccountIdView = a.AccountId,
                              FileReceiptNameDetail = string.IsNullOrEmpty(m.Name) ? "" : m.Name,
                              TempFileIdDetail = a.Receipt
                          }).ToListAsync();
        }

        public async Task<List<Journal>> GetJournalDataBySattlementId(string Id)
        {
            var data = await (from tmpj in context.JournalTemps
                              join tbu in context.BusinessUnits on tmpj.DivisiId equals tbu.Id
                              join st in context.CashAdvanceSettlement on tmpj.ReferenceId equals st.Id into stn
                              from stnj in stn.DefaultIfEmpty()
                              join coa in context.ChartOfAccounts on tmpj.AccountId equals coa.Id
                              where tmpj.ReferenceId == Id && tmpj.IsDeleted.Equals(false)
                              select new Journal
                              {
                                  AccountId = tmpj.AccountId,
                                  AccountCode = coa.CodeRec,
                                  JobId = tmpj.JobId,
                                  AccountName = coa.Name5,
                                  Description = coa.Name5,
                                  JobIdName = stnj.JobId == null ? "-" : "" + stnj.JobId,
                                  DivisiId = tmpj.DivisiId,
                                  DivisiName = tbu.UnitName,
                                  Credit = tmpj.Credit,
                                  Debit = tmpj.Debit,
                                  LineCode = tmpj.LineCode,
                              }).ToListAsync();

            return data;
        }

        public async Task<int> AddSattlement(CashAdvanceSattlementDTO model)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var modelca = await context.CashAdvances.AsNoTracking().Where(x => x.Id == model.Header.CashAdvanceId).FirstOrDefaultAsync();
                    //await financePeriod.IsClosePeriod(Convert.ToDateTime(modelca.CaDate), modelca.LegalEntityId, Models.Enum.FinancePeriodCode.CAS);

                    model.Header.Id = Guid.NewGuid().ToString();
                    model.Header.SattlementNumber = await TransID.GetTransId(context, Code.CashAdvanceSattlement, DateTime.Today);
                    await globalFcApproval.UnsafeSubmitApproval<CashAdvanceSettlement>(false, model.Header.Id, int.Parse(model.Header.CaStatusId), $"{ApprovalLink.CashAdvanceSettlement}?Id={model.Header.Id}&isApprove=true", idTemplate, MenuUnique.CashAdvanceSettlement, model.Header);

                    modelca.CaStatusId = model.Header.CaStatusId;
                    context.PhoenixEdit(modelca);

                    if (model.Details != null)
                    {
                        //remove existing data
                        var additional = await context.CashAdvanceSettlementDetail.AsNoTracking().Where(x => x.CashAdvanceSettleId == model.Header.Id).ToListAsync();
                        if (additional.Count > 0)
                        {
                            context.CashAdvanceSettlementDetail.RemoveRange(additional);
                        }


                        //validate again, if still exist additional data after removerange
                        if (model.Details != null)
                        {
                            List<CashAdvanceSettlementDetail> cast = new List<CashAdvanceSettlementDetail>();
                            foreach (CashAdvanceSettlementDetail data in model.Details)
                            {

                                data.CashAdvanceSettleId = model.Header.Id;
                                data.Qty = data.ActualQty;
                                data.AccountId = "4415002503333080";
                                data.AccountCode = "2.1.01.01.01";
                                data.ActualAmount = data.Qty * data.ActualUnitPrice;
                                data.Id = Guid.NewGuid().ToString();
                                cast.Add(data);
                            }

                            CashAdvanceSettlementDetail[] npo = cast.ToArray();
                            await context.PhoenixAddRangeAsync(npo);
                        }

                    }
                    await log.AddAsync(new StatusLog() { TransactionId = model.Header.Id, Status = StatusTransaction.StatusName(Convert.ToInt32(model.Header.CaStatusId)), Description = model.Header.LogDescription });
                    var save = await context.SaveChangesAsync();
                    transaction.Commit();
                    return save;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        private async Task<CashAdvanceSattlementDTO> ProcessUpload(CashAdvanceSattlementDTO entity)
        {
            var file1 = await uploadFile.Upload(entity.fileUpload, null);
            if (!string.IsNullOrEmpty(file1))
            {
                entity.Header.FileReceipt = file1;
            }

            return entity;
        }

        public async Task<int> EditSattlement(CashAdvanceSattlementDTO entity)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var model = await ProcessUpload(entity);
                    context.PhoenixEdit(model.Header);
                    
                    if (model.Details != null)
                    {

                        if (model.Details.Count > 0)
                        {
                            var additional = await context.CashAdvanceSettlementDetail.AsNoTracking().Where(x => x.CashAdvanceSettleId == model.Header.Id).ToListAsync();
                            if (additional.Count > 0)
                            {
                                context.CashAdvanceSettlementDetail.RemoveRange(additional);
                            }

                            List<CashAdvanceSettlementDetail> dt = new List<CashAdvanceSettlementDetail>();
                            foreach (CashAdvanceSettlementDetail data in model.Details)
                            {
                                //data.AccountId = "4415002503333080";
                                data.Id = Guid.NewGuid().ToString();
                                data.CashAdvanceSettleId = entity.Header.Id;
                                data.AccountCode = "2.1.01.01.01";
                                data.ActualAmount = data.ActualQty * data.ActualUnitPrice;
                                dt.Add(data);
                            }

                            CashAdvanceSettlementDetail[] dtg = dt.ToArray();
                            await context.PhoenixAddRangeAsync(dtg);
                        }
                    }

                    var trapprovalmodel = await context.TrTemplateApprovals.AsNoTracking().Where(x => x.RefId == model.Header.Id).FirstOrDefaultAsync();
                    if (trapprovalmodel == null)
                    {
                        var isApprove = true;
                        var dataMenu = await menuService.GetByUniqueName(MenuUnique.CashAdvanceSettlement).ConfigureAwait(false);
                        var vm = new Models.ViewModel.TransApprovalHrisVm()
                        {
                            MenuId = dataMenu.Id,
                            RefId = model.Header.Id,
                            DetailLink = $"{ApprovalLink.CashAdvanceSettlement}?Id={model.Header.Id}&isApprove=true",
                            //Tname = "fn.PurchaseRequest",//"SP#NAMA SP NYA",
                            IdTemplate = idTemplate
                        };
                        var subGroupId = ClainJwtPrincipalHelper.GetBusinessSubGroup(context.HttpContext);
                        var divisionId = ClainJwtPrincipalHelper.GetBusinessUnitDivisi(context.HttpContext);
                        var employeeId = ClainJwtPrincipalHelper.GetEmployeeId(context.HttpContext);
                        isApprove = await globalFcApproval.Save(vm, subGroupId, divisionId, employeeId);


                        if (isApprove)
                        {
                            await log.AddAsync(new StatusLog() { TransactionId = model.Header.Id, Status = StatusTransaction.StatusName(Convert.ToInt32(model.Header.CaStatusId)), Description = model.Header.LogDescription });
                            var save = await context.SaveChangesAsync();
                            transaction.Commit();
                            return save;
                        }
                        else
                        {
                            transaction.Rollback();
                            throw new Exception("please check the approval template that will be processed");
                        }

                    }
                    else
                    {

                        var save = await context.SaveChangesAsync();
                        transaction.Commit();
                        return save;
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message.ToString());
                }
            }
        }

        public async Task<CashAdvanceSettlement> ApproveSattlement(CashAdvanceSettlement entity)
        {
            try
            {
                var modelcab = await context.CashAdvances.AsNoTracking().Where(x => x.IsDeleted.Equals(false) && x.Id == entity.CashAdvanceId).FirstOrDefaultAsync();
                //await financePeriod.IsClosePeriod(Convert.ToDateTime(modelcab.CaDate), modelcab.LegalEntityId, Models.Enum.FinancePeriodCode.CAS);

                await globalFcApproval.ProcessApproval<CashAdvanceSettlement>(entity.Id, StatusTransaction.Approved, entity.RemarkRejected, entity);

                var modeltrapp = await context.TrTemplateApprovals.Where(x => x.RefId == entity.Id).FirstOrDefaultAsync();
                if (modeltrapp != null)
                {
                    if (modeltrapp.StatusApprovedDescription == StatusTransaction.StatusName(StatusTransaction.Approved))
                    {
                        entity.CaStatusId = CashAdvanceStatus.Complete;
                        entity.RemarkRejected = entity.RemarkRejected;

                        if (modelcab != null)
                        {
                            modelcab.CaStatusId = entity.CaStatusId;
                            modelcab.ModifiedOn = DateTime.Now;
                            //await context.Database.ExecuteSqlCommandAsync("EXECUTE fn.create_jurnal_cas @p_cas_id", parameters: new[] { new SqlParameter("@p_cas_id", entity.Id) });
                            context.PhoenixEdit(modelcab);
                        }
                    }
                    else
                    {
                        entity.CaStatusId = CashAdvanceStatus.WaitingApproval;
                    }
                }
                else
                {
                    entity.CaStatusId = CashAdvanceStatus.WaitingApproval;
                }
                await log.AddAsync(new StatusLog() { TransactionId = entity.Id, Status = StatusTransaction.StatusName(Convert.ToInt32(entity.CaStatusId)), Description = entity.LogDescription });
                context.PhoenixEdit(entity);
                var save = await context.SaveChangesAsync();
                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }

        }

        public async Task<CashAdvanceSettlement> Reject(CashAdvanceSettlement entity)
        {
            try
            {
                //await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.CaDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.CAS);
                entity.CaStatusId = StatusTransaction.CurrentApproval.ToString();
                await globalFcApproval.ProcessApproval<CashAdvanceSettlement>(entity.Id, Int32.Parse(entity.CaStatusId), entity.RemarkRejected, entity);

                var modeltrapp = await context.TrTemplateApprovals.AsNoTracking().Where(x => x.RefId == entity.Id).FirstOrDefaultAsync();
                if (modeltrapp != null)
                {
                    if (modeltrapp.StatusApprovedDescription == StatusTransaction.StatusName(StatusTransaction.Rejected))
                    {
                        entity.CaStatusId = StatusTransaction.CurrentApproval.ToString();
                        entity.RemarkRejected = entity.RemarkRejected;
                    }
                }
                else
                {
                    entity.CaStatusId = StatusTransaction.CurrentApproval.ToString();
                }
                await log.AddAsync(new StatusLog() { TransactionId = entity.Id, Status = StatusTransaction.StatusName(Convert.ToInt32(StatusTransaction.Rejected)), Description = entity.RemarkRejected });
                context.PhoenixEdit(entity);
                var save = await context.SaveChangesAsync();
                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }

        }

        public async Task<int> CreateAutomaticJournalFromSattlementId(string Id)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var modelca = await (from a in context.CashAdvances.AsNoTracking()
                                         join b in context.CashAdvanceSettlement.AsNoTracking() on a.Id equals b.CashAdvanceId
                                         where b.Id == Id
                                         select a).FirstOrDefaultAsync();
                    await financePeriod.IsClosePeriod(Convert.ToDateTime(modelca.CaDate), modelca.LegalEntityId, Models.Enum.FinancePeriodCode.CAS);
                   
                    await context.Database.ExecuteSqlCommandAsync("EXECUTE fn.create_jurnal_cas_temp @p_cas_id", parameters: new[] { new SqlParameter("@p_cas_id", Id) });
                    var save = await context.SaveChangesAsync();

                    transaction.Commit();
                    return save;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message.ToString());
                }
            }
        }

        public async Task<CashAdvanceSettlementDetail> UploadFile(IFormFile file)
        {
            CashAdvanceSettlementDetail model = new CashAdvanceSettlementDetail();
            TrMediaFileMaster FileMaster = new TrMediaFileMaster();
            string fileIdDetail = "";
            //Upload ke FileMaster
            var fileId = await uploadFile.Upload(file, null);
            if (!string.IsNullOrEmpty(fileId))
            {
                FileMaster.FileMasterId = fileId;
                model.Receipt = fileId;
            }
            return model;
        }


    }
}

