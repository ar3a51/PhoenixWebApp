﻿using Phoenix.Data.Models.Finance.Transaction;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Phoenix.Data.Models;
using Phoenix.Data.Models.Finance.Master;
using Phoenix.Data.Models.ViewModel;
using System.Data.SqlClient;
using Phoenix.Data.Models.Finance.Transaction.PaymentRequest;

namespace Phoenix.Data.Services.Finance.Transaction
{
    public interface ITemporaryAccountPayableService : IDataService<TemporaryAccountPayable>
    {
        Task<TemporaryAccountPayableJoinTable> GetJoin(string id);
        Task<TemporaryAccountPayable> GetTAPFromINV(string invoiceReceivedId, string referenceId);
        Task<List<TemporaryAccountPayableJournal>> GetTapJournalWithTAPId(string TAPId);
        Task<List<TemporaryAccountPayableJoinTable>> GetJoinList();
        Task<int> AddWithDetail(TemporaryAccountPayableDTO entity);
        Task<string> AddWithDetailSP(string InvoiceReceivedId);
        Task<string> AddWithDetailSPPO(string InvoiceReceivedId, string PoId);
        Task<ChartOfAccount> GetAccountData(string id);
        Task<List<ExecJournalAPViewDto>> GetJournalTmpAll(string referenceId, string invoiceReceivedId);
        Task<List<ExecJournalAPViewDto>> GetJournalTmp(string referenceId);
        Task<List<ExecJournalAPViewDto>> GetJournalTmpTaxOnly(string referenceId);
        Task<int> CreateAutomaticJournal(string TapId);
        Task<int> CreateAutomaticJournalFromTAPNumber(string TapNumber);
        Task<int> EditAutomaticJournal(TemporaryAccountPayableDTO TapDto);
        Task<int> CreateAPJournal(string TapId);
        Task<int> CreateAPData(string TapId, string APId);
        Task<int> ChangeStatusInvoiceReceived(string TapId, string Status);
        Task<List<ExecJournalAPViewDto>> GetJournalTmpAllFromTAP(string invoiceReceivedId);
        Task<List<ExecJournalAPViewDto>> GetJournalTmpAllFromTAPHeader(string invoiceReceivedId);
        Task<TemporaryAccountPayable> GetTapFromINV(string invoiceReceivedId);
    }
    public class TemporaryAccountPayableService : ITemporaryAccountPayableService
    {
        readonly DataContext context;
        public TemporaryAccountPayableService(DataContext context)
        {
            this.context = context;
        }

        public async Task<int> CreateAutomaticJournal(string TapId)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    await ChangeStatusInvoiceReceived(TapId, InvoiceReceivedStatus.OpenAP);
                    //context.
                    await context.Database.ExecuteSqlCommandAsync("EXECUTE fn.create_jurnal_temp_ap2 @p_account_payable_temp_id", parameters: new[] { new SqlParameter("@p_account_payable_temp_id", TapId) });
                    var save = await context.SaveChangesAsync();

                    transaction.Commit();
                    return save;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message.ToString());
                }
            }
        }

        public async Task<int> CreateAutomaticJournalFromTAPNumber(string TapNumber)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var tap = await context.TemporaryAccountPayables.Where(x => x.TapNumber == TapNumber).FirstOrDefaultAsync();
                    //await ChangeStatusInvoiceReceived(tap.Id, InvoiceReceivedStatus.OpenAP);
                    //context.
                    await context.Database.ExecuteSqlCommandAsync("EXECUTE fn.create_jurnal_temp_ap2 @p_account_payable_temp_id", parameters: new[] { new SqlParameter("@p_account_payable_temp_id", tap.Id) });
                    var save = await context.SaveChangesAsync();

                    transaction.Commit();
                    return save;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message.ToString());
                }
            }
        }

        public async Task<int> EditAutomaticJournal(TemporaryAccountPayableDTO TapDto)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    if (TapDto.Details.Count > 1)
                    {
                        var modeldetail = await context.TemporaryAccountPayableDetail.Where(x => x.AccountPayableTemporaryId == TapDto.Header.Id && x.IsDeleted.Equals(false)).ToListAsync();

                        List<TemporaryAccountPayableDetail> dtd = new List<TemporaryAccountPayableDetail>();
                        foreach (TemporaryAccountPayableDetail data in modeldetail)
                        {
                            dtd.Add(data);
                        }

                        TemporaryAccountPayableDetail[] dtdn = dtd.ToArray();
                        context.PhoenixDeleteRange(dtdn);
                    }

                    List<TemporaryAccountPayableDetail> dtc = new List<TemporaryAccountPayableDetail>();
                    foreach (TemporaryAccountPayableDetail doe in TapDto.Details)
                    {
                        doe.Id = Guid.NewGuid().ToString();
                        doe.AccountPayableTemporaryId = TapDto.Header.Id;
                        dtc.Add(doe);
                    }
                    TemporaryAccountPayableDetail[] dtcn = dtc.ToArray();
                    context.PhoenixAddRange(dtcn);

                    var save = await context.SaveChangesAsync();

                    transaction.Commit();
                    return save;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message.ToString());
                }
            }
        }

        public async Task<int> CreateAPJournal(string APId)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    await context.Database.ExecuteSqlCommandAsync("EXECUTE fn.create_jurnal_ap @p_account_payable_id", parameters: new[] { new SqlParameter("@p_account_payable_id", APId) });
                    var save = await context.SaveChangesAsync();

                    transaction.Commit();
                    return save;
                }
                catch (Exception ex)
                {

                    throw new Exception(ex.Message.ToString());
                }
            }

        }

        public async Task<int> AddAsync(TemporaryAccountPayable entity)
        {
            entity.Id = Guid.NewGuid().ToString();
            await context.PhoenixAddAsync(entity);
            return await context.SaveChangesAsync();
        }
        public async Task<int> AddWithDetail(TemporaryAccountPayableDTO entity)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var modelinv = await context.InvoiceReceivables.Where(x => x.Id == entity.Header.InvoiceReceivableId && x.IsDeleted.Equals(false)).FirstOrDefaultAsync();
                    modelinv.InvoiceStatusId = InvoiceReceivedStatus.TAP;
                    context.PhoenixEdit(modelinv);

                    var modelaccount_detail = await context.ExecJournalAPView.AsNoTracking().Where(x => x.purchase_order_id.Equals(entity.Header.PurchaseOrderId) && x.ap_level.Equals("D")).FirstOrDefaultAsync();
                    var AccountId = modelaccount_detail.coa_id;
                    var d_k_d = "k";
                    if (modelaccount_detail.condition.Equals("Credit"))
                    {
                        d_k_d = "k";
                    }
                    else
                    {
                        d_k_d = "d";
                    }

                    var modelaccount_header = await context.ExecJournalAPView.AsNoTracking().Where(x => x.purchase_order_id.Equals(entity.Header.PurchaseOrderId) && x.ap_level.Equals("H")).FirstOrDefaultAsync();
                    var d_k_h = "k";
                    if (modelaccount_header.condition.Equals("Credit"))
                    {
                        d_k_h = "k";
                    }
                    else
                    {
                        d_k_h = "d";
                    }

                    //var modelaccount_tax = await context.ExecJournalAPView.AsNoTracking().Where(x => x.purchase_order_id.Equals(entity.Header.POId) && x.ap_level.Equals("D") && x.line_code.Equals("TAX")).FirstOrDefaultAsync();

                    //var modelaccount_vat = await context.ExecJournalAPView.AsNoTracking().Where(x => x.purchase_order_id.Equals(entity.Header.POId) && x.ap_level.Equals("D") && x.line_code.Equals("VAT")).FirstOrDefaultAsync();

                    entity.Header.Id = Guid.NewGuid().ToString();
                    entity.Header.TapNumber = await TransID.GetTransId(context, Code.TAP, DateTime.Today);
                    entity.Header.TransactionDate = DateTime.Now;
                    entity.Header.Status = InvoiceReceivedStatus.TAP;
                    entity.Header.AccountId = modelaccount_header.coa_id;
                    entity.Header.DK = d_k_h;
                    entity.Header.LineCode = modelaccount_header.line_code;
                    entity.Header.IsJournal = "N";
                    await context.PhoenixAddAsync(entity.Header);

                    int i = 1;

                    foreach (TemporaryAccountPayableDetail data in entity.Details)
                    {
                        data.Id = Guid.NewGuid().ToString();
                        data.AccountPayableTemporaryId = entity.Header.Id;
                        data.AccountId = AccountId;
                        data.DK = d_k_d;
                        data.LineCode = modelaccount_detail.line_code;
                        data.RowIndex = i;
                        i = i + i;
                    }

                    //tax
                    //TemporaryAccountPayableDetail newd = new TemporaryAccountPayableDetail();
                    //newd.Id = Guid.NewGuid().ToString();
                    //newd.AccountPayableTemporaryId = entity.Header.Id;
                    //newd.account_id = modelaccount_tax.coa_id;
                    //if (modelaccount_tax.condition.Equals("Debit"))
                    //{
                    //    newd.d_k = "d";
                    //}
                    //else
                    //{
                    //    newd.d_k = "k";
                    //}
                    //newd.RowIndex = i;
                    //newd.ItemName = modelaccount_tax.name5;
                    //newd.AmountPaid = modelaccount_tax.Debit;
                    //newd.amountTotal = modelaccount_tax.Debit;
                    //newd.ItemDescription = modelaccount_tax.name5;
                    //newd.Item_type_id = "0777eedd-8325-dd42-857a-9618726a5040";
                    //newd.qty = 1;
                    //newd.LineCode = modelaccount_tax.line_code;
                    //newd.unit_price = modelaccount_tax.Debit;
                    //newd.uom_id = "62919a04-5f2e-44d4-89a6-c831dd3c32e4";
                    //newd.Discount = 0;
                    //i = i + i;
                    //entity.Details.Add(newd);

                    //vat

                    //TemporaryAccountPayableDetail vat = new TemporaryAccountPayableDetail();
                    //vat.Id = Guid.NewGuid().ToString();
                    //vat.AccountPayableTemporaryId = entity.Header.Id;
                    //vat.account_id = modelaccount_vat.coa_id;
                    //if (modelaccount_vat.condition.Equals("Debit"))
                    //{
                    //    vat.d_k = "d";
                    //}
                    //else
                    //{
                    //    vat.d_k = "k";
                    //}
                    //vat.RowIndex = i;
                    //vat.ItemName = modelaccount_vat.name5;
                    //vat.AmountPaid = modelaccount_vat.Debit;
                    //vat.amountTotal = modelaccount_vat.Debit;
                    //vat.ItemDescription = modelaccount_vat.name5;
                    //vat.Item_type_id = "0777eedd-8325-dd42-857a-9618726a5040";
                    //vat.qty = 1;
                    //vat.LineCode = modelaccount_vat.line_code;
                    //vat.unit_price = modelaccount_vat.Debit;
                    //vat.uom_id = "62919a04-5f2e-44d4-89a6-c831dd3c32e4";
                    //vat.Discount = 0;
                    //i = i + i;
                    //entity.Details.Add(vat);

                    await context.AddRangeAsync(entity.Details);
                    var save = await context.SaveChangesAsync();
                    transaction.Commit();
                    return save;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message.ToString());
                }
            }

        }

        public async Task<string> AddWithDetailSP(string InvoiceReceivedId)
        {
            try
            {
                var modeldel = await context.TemporaryAccountPayables.Where(x => x.IsDeleted.Equals(false) && x.InvoiceReceivableId == InvoiceReceivedId).FirstOrDefaultAsync();
                if (modeldel != null)
                {
                    var mn = new List<TemporaryAccountPayableDetail>();
                    var modeldeldetail = await context.TemporaryAccountPayableDetail.Where(x => x.IsDeleted.Equals(false) && x.AccountPayableTemporaryId == modeldel.Id).ToListAsync();

                    foreach (TemporaryAccountPayableDetail data in modeldeldetail)
                    {
                        data.AccountPayableTemporaryId = modeldel.Id;
                        mn.Add(data);
                    }
                    TemporaryAccountPayableDetail[] mnd = mn.ToArray();

                    context.PhoenixDeleteRange(mnd);
                }

                var inv = await context.InvoiceReceivables.Where(x => x.Id == InvoiceReceivedId).FirstOrDefaultAsync();
                if (inv.InvoiceStatusId == InvoiceReceivedStatus.TAXTAP)
                {
                    inv.InvoiceStatusId = InvoiceReceivedStatus.OpenAP;
                }
                context.PhoenixEdit(inv);

                string TAPNumber = await TransID.GetTransId(context, Code.TAP, DateTime.Today);
                var createdby = Shared.Core.PrincipalHelpers.ClainJwtPrincipalHelper.GetEmployeeId(context.HttpContext);
                //context.
                var result = await context.Database.ExecuteSqlCommandAsync("EXECUTE fn.prc_push_ap_temp @p_tap_number,@p_invoice_receivable_id,@p_created_by", parameters: new[] { new SqlParameter("@p_tap_number", TAPNumber), new SqlParameter("@p_invoice_receivable_id", InvoiceReceivedId), new SqlParameter("@p_created_by", createdby) });
                await context.SaveChangesAsync();
                return TAPNumber;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public async Task<string> AddWithDetailSPPO(string InvoiceReceivedId, string PoId)
        {
            try
            {
                var modeldel = await context.TemporaryAccountPayables.Where(x => x.IsDeleted.Equals(false) && x.InvoiceReceivableId == InvoiceReceivedId && x.PurchaseOrderId == PoId).FirstOrDefaultAsync();
                if (modeldel != null)
                {
                    var mn = new List<TemporaryAccountPayableDetail>();
                    //detail tar tambahin isdeleted false
                    var modeldeldetail = await context.TemporaryAccountPayableDetail.Where(x => x.AccountPayableTemporaryId == modeldel.Id).ToListAsync();

                    foreach (TemporaryAccountPayableDetail data in modeldeldetail)
                    {
                        data.AccountPayableTemporaryId = modeldel.Id;
                        mn.Add(data);
                    }
                    TemporaryAccountPayableDetail[] mnd = mn.ToArray();

                    var tmpjr = await context.TemporaryAccountPayableJournal.Where(x => x.reference_id == modeldel.Id).ToListAsync();

                    if (tmpjr.Count > 0)
                    {

                        List<TemporaryAccountPayableJournal> tt = new List<TemporaryAccountPayableJournal>();

                        foreach (TemporaryAccountPayableJournal jr in tmpjr)
                        {
                            tt.Add(jr);
                        }

                        TemporaryAccountPayableJournal[] ttd = tt.ToArray();
                        context.PhoenixDeleteRange(ttd);
                    }

                    context.PhoenixDeleteRange(mnd);
                    context.PhoenixDelete(modeldel);
                }

                var inv = await context.InvoiceReceivables.Where(x => x.Id == InvoiceReceivedId).FirstOrDefaultAsync();

                context.PhoenixEdit(inv);

                string TAPNumber = await TransID.GetTransId(context, Code.TAP, DateTime.Today);
                var createdby = Shared.Core.PrincipalHelpers.ClainJwtPrincipalHelper.GetEmployeeId(context.HttpContext);
                //context.
                var result = await context.Database.ExecuteSqlCommandAsync("EXECUTE fn.prc_push_ap_temp @p_tap_number,@p_invoice_receivable_id,@p_created_by", parameters: new[] { new SqlParameter("@p_tap_number", TAPNumber), new SqlParameter("@p_invoice_receivable_id", InvoiceReceivedId), new SqlParameter("@p_created_by", createdby) });
                await context.SaveChangesAsync();
                return TAPNumber;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public async Task<int> AddRangeAsync(params TemporaryAccountPayable[] entities)
        {
            context.PhoenixAddRange(entities);
            return await context.SaveChangesAsync();

        }

        public async Task<int> DeleteAsync(TemporaryAccountPayable entity)
        {
            context.PhoenixApprove(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteRageAsync(params TemporaryAccountPayable[] entities)
        {
            context.PhoenixDeleteRange(entities);
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteSoftAsync(TemporaryAccountPayable entity)
        {
            context.PhoenixDelete(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteSoftRangeAsync(params TemporaryAccountPayable[] entities)
        {
            context.PhoenixDeleteRange(entities);
            return await context.SaveChangesAsync();
        }

        public async Task<int> EditAsync(TemporaryAccountPayable entity)
        {
            context.PhoenixEdit(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> EditRangeAsync(TemporaryAccountPayable entity)
        {
            context.PhoenixEditRange(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<List<TemporaryAccountPayable>> Get() => await context.TemporaryAccountPayables.AsNoTracking().Where(x => !(x.IsDeleted ?? false)).ToListAsync();

        public Task<List<TemporaryAccountPayable>> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public async Task<TemporaryAccountPayable> Get(string id)
        {
            var model = await (from tab in context.TemporaryAccountPayables
                               join inv in context.InvoiceReceivables on tab.InvoiceReceivableId equals inv.Id
                               where tab.IsDeleted.Equals(false) && tab.Id == id
                               select new TemporaryAccountPayable
                               {
                                   Id = tab.Id,
                                   AccountId = tab.AccountId,
                                   AffiliationId = tab.AffiliationId,
                                   Amount = tab.Amount,
                                   APType = tab.APType,
                                   BankAccountDestination = tab.BankAccountDestination,
                                   CompanyBrandId = tab.CompanyBrandId,
                                   BusinessUnitId = tab.BusinessUnitId,
                                   ApprovedBy = tab.ApprovedBy,
                                   ApprovedOn = tab.ApprovedOn,
                                   IsActive = tab.IsActive,
                                   TapNumber = tab.TapNumber,
                                   Charges = tab.Charges,
                                   CreatedBy = tab.CreatedBy,
                                   CreatedOn = tab.CreatedOn,
                                   CurrencyId = tab.CurrencyId,
                                   DeletedBy = tab.DeletedBy,
                                   DeletedOn = tab.DeletedOn,
                                   //Divisi = tab.Divisi,
                                   DueDays = tab.DueDays,
                                   DK = tab.DK,
                                   ExchangeRate = tab.ExchangeRate,
                                   //invoiceReceivable = tab.invoiceReceivable,
                                   InvoiceReceivableId = tab.InvoiceReceivableId,
                                   Status = inv.InvoiceStatusId,
                                   InvoiceVendorNumber = tab.InvoiceVendorNumber,
                                   IsDefault = tab.IsDefault,
                                   IsDeleted = tab.IsDeleted,
                                   IsLocked = tab.IsLocked,
                                   IsJournal = tab.IsJournal,
                                   JobId = tab.JobId,
                                   LegalEntityId = tab.LegalEntityId,
                                   ModifiedBy = tab.ModifiedBy,
                                   ModifiedOn = tab.ModifiedOn,
                                   OurRef = tab.OurRef,
                                   OurRefId = tab.OurRefId,
                                   Periode = tab.Periode,
                                   OwnerId = tab.OwnerId,
                                   PurchaseOrderId = tab.PurchaseOrderId,
                                   InvoiceReceivedDate = tab.InvoiceReceivedDate,
                                   Remarks = tab.Remarks,
                                   //Status = tab.Status,
                                   TaxDate = tab.TaxDate,
                                   TaxNumber = tab.TaxNumber,
                                   TransactionDate = tab.TransactionDate,
                                   MainServiceCategoryId = tab.MainServiceCategoryId,
                                   VendorId = tab.VendorId,
                                   //NetAmount = inv.SubTotal,
                                   //PromptPayment = inv.PromptPayment,
                                   //PromptPayment2 = inv.PromptPayment2,
                                   //ValueAddedTax = inv.Vat,
                                   //ValueAddedTaxPercent = inv.VatPercent,
                                   //AdvertisingTax = inv.AdvertisingTax,
                                   //TaxPayable = inv.TaxPayable,
                                   //TaxPayablePercent = inv.TaxPayablePercent,
                                   //TotalCostVendor = inv.GrandTotal

                               }).SingleOrDefaultAsync();


            return model;
        }


        public async Task<TemporaryAccountPayableJoinTable> GetJoin(string id)
        {
            var query = from tap in await this.Get()
                        join ir in await context.InvoiceReceivables.AsNoTracking().Where(x => !(x.IsDeleted ?? false)).ToListAsync() on tap.InvoiceReceivableId equals ir.Id
                        join po in await context.PurchaseOrders.AsNoTracking().Where(x => !(x.IsDeleted ?? false)).ToListAsync() on tap.PurchaseOrderId equals po.Id
                        join comp in await context.Companys.AsNoTracking().ToListAsync() on tap.VendorId equals comp.Id
                        join curr in await context.Currencies.AsNoTracking().ToListAsync() on tap.CurrencyId equals curr.Id
                        join div in await context.BusinessUnits.AsNoTracking().Where(x => !(x.IsDeleted ?? false)).ToListAsync() on tap.BusinessUnitId equals div.Id
                        select new TemporaryAccountPayableJoinTable
                        {
                            Id = tap.Id,
                            TapNumber = tap.TapNumber,
                            LegalEntityId = po.LegalEntityId,
                            BusinessUnitId = po.BusinessUnitId,
                            AffiliationId = po.AffiliationId,
                            AccountId = tap.AccountId,
                            InvoiceReceivedId = tap.InvoiceReceivableId,
                            VendorId = ir.ClientId,
                            JobId = ir.JobId,
                            TransactionDate = tap.TransactionDate,
                            DueDays = tap.DueDays,
                            CurrencyId = ir.CurrencyId,
                            Amount = ir.TotalAmount ?? 0,
                            ExchangeRate = ir.ExchangeRate ?? 0,
                            BankAccountDestination = tap.BankAccountDestination,
                            Charges = tap.Charges,
                            Remarks = tap.Remarks,
                            JobName = tap.JobName,
                            OurRef = tap.OurRef,
                            OurRefId = tap.OurRefId,
                            CreatedBy = tap.CreatedBy,
                            CreatedOn = tap.CreatedOn,
                            ModifiedBy = tap.ModifiedBy,
                            ModifiedOn = tap.ModifiedOn,
                            ApprovedBy = tap.ApprovedBy,
                            ApprovedOn = tap.ApprovedOn,
                            IsActive = tap.IsActive,
                            IsLocked = tap.IsLocked,
                            IsDefault = tap.IsDefault,
                            IsDeleted = tap.IsDeleted,
                            OwnerId = tap.OwnerId,
                            DeletedBy = tap.DeletedBy,
                            DeletedOn = tap.DeletedOn,
                            //InvoiceStatusId = ir.InvoiceStatusId,
                            PurchaseOrderId = po.Id,
                            PONumber = po.PurchaseOrderNumber,
                            InvoiceReceivedNumber = ir.InvoiceNumber,
                            InvoiceReceivedDate = ir.InvoiceDate,
                            CompanyBrandId = po.CompanyBrandId,
                            Period = tap.Periode,
                            InvoiceVendorNumber = ir.InvoiceVendor,
                            DivisionId = ir.DivisionId,
                            DivisionName = div.UnitName,
                            CurrencyName = curr.CurrencyName,
                            ReceivedDate = tap.InvoiceReceivedDate,
                            TaxNumber = ir.TaxNumber,
                            TaxDate = ir.TaxDate,
                            //AllocationRatioId = tap.AllocationRatio,
                            //PromptPayment = null,
                            //OtherDiscount = po.DiscountAmount,
                            //ValueAddedTax = ir.Vat,
                            //AdvertisingTax = null,
                            //TotalCostVendor = null,
                            //TaxPayable = po.TaxAmount,
                            VATNumber = null,
                            //Divisi = div.UnitName,
                            StatusName = tap.Status == "1" ? "Draft" : tap.Status == "2" ? "ApproveFrontDesk" + " on " + tap.ModifiedOn :
                                              tap.Status == "3" ? "ApproveAdmin" + " on " + tap.ModifiedOn : tap.Status == "4" ? "Rejected" + " on " + tap.ModifiedOn : tap.Status == "5" ? "TAP" : "Completed"


                        };
            return query.SingleOrDefault(m => m.Id == id);
        }

        public async Task<List<TemporaryAccountPayableJoinTable>> GetJoinList()
        {
            var query = from tap in await context.TemporaryAccountPayables.Where(x => x.IsDeleted.Equals(false)).ToListAsync()
                        join ir in await context.InvoiceReceivables.Where(x => x.IsDeleted.Equals(false)).ToListAsync() on tap.InvoiceReceivableId equals ir.Id into irn
                        from irnw in irn.DefaultIfEmpty()
                        join po in await context.PurchaseOrders.Where(x => x.IsDeleted.Equals(false)).ToListAsync() on tap.PurchaseOrderId equals po.Id into pon
                        from ponw in pon.DefaultIfEmpty()
                        join comp in await context.Companys.Where(x => x.IsDeleted.Equals(false)).ToListAsync() on tap.VendorId equals comp.Id into compn
                        from compnw in compn.DefaultIfEmpty()
                        join curr in await context.Currencies.ToListAsync() on tap.CurrencyId equals curr.Id into currn
                        from currnw in currn.DefaultIfEmpty()
                        join div in await context.BusinessUnits.Where(x => x.IsDeleted.Equals(false)).ToListAsync() on tap.BusinessUnitId equals div.Id into divn
                        from dinvw in divn.DefaultIfEmpty()
                        select new TemporaryAccountPayableJoinTable
                        {
                            Id = tap.Id,
                            TapNumber = tap.TapNumber,
                            LegalEntityId = ponw.LegalEntityId,
                            BusinessUnitId = ponw.BusinessUnitId,
                            AffiliationId = ponw.AffiliationId,
                            AccountId = tap.AccountId,
                            InvoiceReceivedId = tap.InvoiceReceivableId,
                            VendorId = irnw.ClientId,
                            JobId = irnw.JobId,
                            TransactionDate = tap.TransactionDate,
                            DueDays = tap.DueDays,
                            CurrencyId = irnw.CurrencyId,
                            Amount = irnw.TotalAmount ?? 0,
                            ExchangeRate = irnw.ExchangeRate ?? 0,
                            BankAccountDestination = tap.BankAccountDestination,
                            Charges = tap.Charges,
                            Remarks = tap.Remarks,
                            JobName = tap.JobName,
                            OurRef = tap.OurRef,
                            OurRefId = tap.OurRefId,
                            CreatedBy = tap.CreatedBy,
                            CreatedOn = tap.CreatedOn,
                            ModifiedBy = tap.ModifiedBy,
                            ModifiedOn = tap.ModifiedOn,
                            ApprovedBy = tap.ApprovedBy,
                            ApprovedOn = tap.ApprovedOn,
                            IsActive = tap.IsActive,
                            IsLocked = tap.IsLocked,
                            IsDefault = tap.IsDefault,
                            IsDeleted = tap.IsDeleted,
                            OwnerId = tap.OwnerId,
                            DeletedBy = tap.DeletedBy,
                            DeletedOn = tap.DeletedOn,
                            //InvoiceStatusId = irnw.InvoiceStatusId,

                            Value = irnw.Value,
                            InvoiceReceivedNumber = irnw.InvoiceNumber,
                            InvoiceReceivedDate = irnw.InvoiceDate,
                            CompanyBrandId = ponw.CompanyBrandId,
                            Period = tap.Periode,
                            VendorName = compnw.CompanyName,
                            InvoiceVendorNumber = irnw.InvoiceVendor,
                            PurchaseOrderId = ponw.Id,
                            PONumber = ponw.PurchaseOrderNumber,
                            DivisionId = irnw.DivisionId,
                            DivisionName = dinvw.UnitName,
                            CurrencyName = currnw.CurrencyName,
                            ReceivedDate = tap.InvoiceReceivedDate,
                            TaxNumber = irnw.TaxNumber,
                            TaxDate = irnw.TaxDate,
                            ////AllocationRatioId = tap.AllocationRatio,
                            //PromptPayment = null,
                            //OtherDiscount = ponw.DiscountAmount,
                            //ValueAddedTax = null,
                            //AdvertisingTax = null,
                            //TotalCostVendor = null,
                            //TaxPayable = ponw.TaxAmount,
                            VATNumber = irnw.ValueAddedTax,
                            //Divisi = dinvw.UnitName,
                            StatusName = tap.Status == "1" ? "Draft" : tap.Status == "2" ? "ApproveFrontDesk" + " on " + tap.ModifiedOn :
                                              tap.Status == "3" ? "ApproveAdmin" + " on " + tap.ModifiedOn : tap.Status == "4" ? "Rejected" + " on " + tap.ModifiedOn : tap.Status == "5" ? "TAP" : "Completed"

                        };
            return query.ToList();
        }

        public async Task<ChartOfAccount> GetAccountData(string id)
        {
            return await context.ChartOfAccounts.Where(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<List<ExecJournalAPViewDto>> GetJournalTmp(string referenceId)
        {
            var data = await (from tmpj in context.ExecJournalAPView
                              join tbu in context.BusinessUnits on tmpj.business_unit_id equals tbu.Id
                              where tmpj.purchase_order_id == referenceId && tmpj.line_code.Equals("AP")
                              select new ExecJournalAPViewDto
                              {
                                  coa_id = tmpj.coa_id,
                                  code_rec = tmpj.code_rec,
                                  name5 = tmpj.name5,
                                  job_id = tmpj.job_id,
                                  AccountName = tmpj.name5,
                                  DescriptionJournal = tmpj.DescriptionJournal,
                                  JobIdName = tmpj.job_id == null ? "INTERNAL " + tmpj.purchase_request_id : "PROJECT " + tmpj.job_id,
                                  business_unit_id = tmpj.business_unit_id,
                                  DivisiName = tbu.UnitName,
                                  Credit = tmpj.Credit,
                                  Debit = tmpj.Debit,
                                  ap_level = tmpj.ap_level,
                                  line_code = tmpj.line_code,
                                  condition = tmpj.condition

                              }).ToListAsync();

            return data;
        }

        public async Task<List<ExecJournalAPViewDto>> GetJournalTmpTaxOnly(string referenceId)
        {
            var data = await (from tmpj in context.ExecJournalAPView
                              join tbu in context.BusinessUnits on tmpj.business_unit_id equals tbu.Id
                              where tmpj.purchase_order_id == referenceId && !tmpj.line_code.Equals("AP")
                              select new ExecJournalAPViewDto
                              {
                                  coa_id = tmpj.coa_id,
                                  code_rec = tmpj.code_rec,
                                  name5 = tmpj.name5,
                                  job_id = tmpj.job_id,
                                  AccountName = tmpj.name5,
                                  DescriptionJournal = tmpj.DescriptionJournal,
                                  JobIdName = tmpj.job_id == null ? "INTERNAL " + tmpj.purchase_request_id : "PROJECT " + tmpj.job_id,
                                  business_unit_id = tmpj.business_unit_id,
                                  DivisiName = tbu.UnitName,
                                  Credit = tmpj.Credit,
                                  Debit = tmpj.Debit,
                                  ap_level = tmpj.ap_level,
                                  line_code = tmpj.line_code,
                                  condition = tmpj.condition
                              }).ToListAsync();

            return data;
        }
        //public async Task<List<TemporaryAccountPayableJournal>> GetJournalTmpTaxOnly(string referenceId)
        //{
        //    var data = await (from tmpj in context.TemporaryAccountPayableJournal
        //                      join ttac in context.TemporaryAccountPayables on tmpj.reference_id equals ttac.Id
        //                      join tca in context.ChartOfAccounts on tmpj.AccountId equals tca.Id
        //                      join tbu in context.BusinessUnits on tmpj.DivisiId equals tbu.Id
        //                      where tmpj.reference_id == referenceId && !tmpj.LineCode.Equals("AP")
        //                      select new TemporaryAccountPayableJournal
        //                      {
        //                          Id = tmpj.Id,
        //                          AccountId = tmpj.AccountId,
        //                          AccountCode = tca.CodeRec,
        //                          AccountName = tca.Name5,
        //                          Description = tmpj.Description,
        //                          JobId = tmpj.JobId,
        //                          JobIdName = ttac.JobName,
        //                          DivisiId = tmpj.DivisiId,
        //                          DivisiName = tbu.UnitName,
        //                          Credit = tmpj.Credit,
        //                          Debit = tmpj.Debit
        //                      }).ToListAsync();

        //    return data;
        //}

        public async Task<int> ChangeStatusInvoiceReceived(string TapId, string Status)
        {
            try
            {
                var modeltap = await context.TemporaryAccountPayables.Where(x => x.Id == TapId && x.IsDeleted.Equals(false)).FirstOrDefaultAsync();

                var modelinv = await context.InvoiceReceivables.Where(x => x.Id == modeltap.InvoiceReceivableId && x.IsDeleted.Equals(false)).FirstOrDefaultAsync();
                if (Status == InvoiceReceivedStatus.Open)
                {
                    modelinv.InvoiceStatusId = InvoiceReceivedStatus.Open;
                }
                else if (Status == InvoiceReceivedStatus.TAXTAP)
                {
                    modelinv.InvoiceStatusId = InvoiceReceivedStatus.TAXTAP;
                }
                else if (Status == InvoiceReceivedStatus.TAP)
                {
                    modelinv.InvoiceStatusId = InvoiceReceivedStatus.TAP;
                }
                else if (Status == InvoiceReceivedStatus.OpenAP)
                {
                    modelinv.InvoiceStatusId = InvoiceReceivedStatus.OpenAP;
                }
                else if (Status == InvoiceReceivedStatus.Posted)
                {
                    modelinv.InvoiceStatusId = InvoiceReceivedStatus.Posted;
                }
                else
                {
                    modelinv.InvoiceStatusId = InvoiceReceivedStatus.Open;
                }

                context.PhoenixEdit(modelinv);
                return await context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public async Task<int> CreateAPData(string TapId, string APId)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var modeltapheader = await context.TemporaryAccountPayables.Where(x => x.IsDeleted.Equals(false) && x.Id == TapId).FirstOrDefaultAsync();

                    var modeltapdetail = await context.TemporaryAccountPayableDetail.Where(x => x.AccountPayableTemporaryId == TapId).ToListAsync();

                    var modelinv = await context.InvoiceReceivables.Where(x => x.Id == modeltapheader.InvoiceReceivableId && x.IsDeleted.Equals(false)).FirstOrDefaultAsync();

                    AccountPayable accountPayable = new AccountPayable();
                    accountPayable.Id = APId;
                    accountPayable.Status = InvoiceReceivedStatus.Posted;
                    accountPayable.TemporaryAccountPayableId = modeltapheader.Id;
                    //accountPayable.TemporaryAccountPayableNumber = modeltapheader.TapNumber;
                    accountPayable.AccountId = modeltapheader.AccountId;
                    accountPayable.AccountPayableNumber = await TransID.GetTransId(context, Code.AP, DateTime.Today);
                    accountPayable.AffiliationId = modeltapheader.AffiliationId;
                    //accountPayable.AlocationRatio = modeltapheader.AllocationRatio;
                    accountPayable.Amount = modeltapheader.Amount;
                    accountPayable.BankAccountDestination = modeltapheader.BankAccountDestination;
                    accountPayable.BusinessUnitId = modeltapheader.BusinessUnitId;
                    //accountPayable.company_brand_id = modeltapheader.CompanyBrandId;
                    accountPayable.DueDays = modeltapheader.DueDays;
                    accountPayable.DK = modeltapheader.DK;
                    accountPayable.InvoiceReceivableId = modeltapheader.InvoiceReceivableId;
                    accountPayable.InvoiceReceivedDate = modeltapheader.InvoiceReceivedDate;
                    accountPayable.InvoiceVendorNumber = modeltapheader.InvoiceVendorNumber;
                    accountPayable.JobId = modeltapheader.JobId;
                    accountPayable.JobName = modeltapheader.JobName;
                    accountPayable.LegalEntityId = modeltapheader.LegalEntityId;
                    accountPayable.LineCode = modeltapheader.LineCode;
                    accountPayable.MainserviceCategoryId = modeltapheader.MainServiceCategoryId;
                    accountPayable.OurRef = modeltapheader.OurRef;
                    accountPayable.OurRefId = modeltapheader.OurRefId;
                    accountPayable.Periode = modeltapheader.Periode;
                    accountPayable.PurchaseOrderId = modeltapheader.PurchaseOrderId;
                    accountPayable.Remarks = modeltapheader.Remarks;
                    accountPayable.Status = modeltapheader.Status;
                    accountPayable.TaxDate = modeltapheader.TaxDate;
                    accountPayable.TaxNumber = modeltapheader.TaxNumber;
                    accountPayable.VendorId = modeltapheader.VendorId;
                    accountPayable.CurrencyId = modeltapheader.CurrencyId;
                    accountPayable.ExchangeRate = modeltapheader.ExchangeRate;
                    accountPayable.TransactionDate = modeltapheader.TransactionDate;
                    accountPayable.IsPayment = false;


                    List<AccountPayableDetail> AccountPayableDetailData = new List<AccountPayableDetail>();
                    foreach (TemporaryAccountPayableDetail data in modeltapdetail)
                    {
                        AccountPayableDetail accountPayableDetail = new AccountPayableDetail();
                        accountPayableDetail.Id = Guid.NewGuid().ToString();
                        accountPayableDetail.AccountPayableId = accountPayable.Id;
                        accountPayableDetail.EvidentNumber = accountPayable.Id;
                        accountPayableDetail.DK = data.DK;
                        accountPayableDetail.Discount = data.Discount;
                        accountPayableDetail.EndingBalance = data.EndingBalance;
                        accountPayableDetail.ItemDescription = data.ItemDescription;
                        accountPayableDetail.ItemName = data.ItemName;
                        accountPayableDetail.ItemTypeId = data.ItemTypeId;
                        //accountPayableDetail.Item_type_name = data.Item_type_name;
                        accountPayableDetail.LineCode = data.LineCode;
                        accountPayableDetail.PayableBalance = data.PayableBalance;
                        accountPayableDetail.Qty = data.Qty;
                        accountPayableDetail.RowIndex = data.RowIndex;
                        accountPayableDetail.Tax = data.Tax;
                        accountPayableDetail.UnitPrice = data.UnitPrice;
                        accountPayableDetail.UomId = data.UomId;
                        //accountPayableDetail.uom_name = data.uom_name;
                        accountPayableDetail.AccountId = data.AccountId;
                        accountPayableDetail.AmountPaid = data.AmountPaid;
                        accountPayableDetail.Amount = data.Amount;

                        AccountPayableDetailData.Add(accountPayableDetail);
                    }
                    AccountPayableDetail[] accountPayableDetailData = AccountPayableDetailData.ToArray();
                    modelinv.InvoiceStatusId = InvoiceReceivedStatus.Posted;

                    //PaymentRequest pay = new PaymentRequest();
                    //pay.Id = Guid.NewGuid().ToString();
                    //pay.InvoiceReceivableId = modeltapheader.InvoiceReceivableId;
                    //pay.InvoiceReceivableNumber = modeltapheader.OurRef;
                    //pay.BusinessUnitId = modeltapheader.BusinessUnitId;
                    //pay.LegalEntityId = modeltapheader.LegalEntityId;
                    //pay.Amount = modeltapheader.Amount - modelinv.ValueAddedTax;
                    //pay.AdvertisingTax = modeltapheader.AdvertisingTax == null ? 0 : modeltapheader.AdvertisingTax;
                    //pay.Vat = modelinv.ValueAddedTax == null ? 0 : modelinv.ValueAddedTax;
                    //pay.VatPercent = modelinv.ValueAddedTaxPercent == null ? 0 : modelinv.ValueAddedTaxPercent;
                    //pay.TotalAmount = modeltapheader.Amount;
                    //pay.PaymentRequestDate = DateTime.Now;
                    //pay.PaymentRequestNumber = await TransID.GetTransId(context, Code.PaymentRequest, DateTime.Today);
                    //pay.ReferenceNumberId = accountPayable.AccountPayableNumber;
                    //pay.Status = PaymentRequestStatus.Open;
                    //pay.TaxPayable = modelinv.TaxPayable == null ? 0 : modelinv.TaxPayable;
                    //pay.TaxPayablePercent = modelinv.TaxPayablePercent == null ? 0 : modelinv.TaxPayablePercent;
                    //pay.JobId = modeltapheader.JobId == null ? "" : modeltapheader.JobId;
                    //pay.ExchangeRate = modeltapheader.ExchangeRate;
                    //pay.CurrencyId = modeltapheader.CurrencyId;
                    //pay.PaymentTypeId = "d459cb61-4005-4501-b6b4-a6e79c420a87";
                    //pay.DueDate = modelinv.DueDate;
                    //pay.SourceDoc = "AP";
                    //var modelpo = await context.PurchaseOrders.Where(x => x.IsDeleted.Equals(false) && x.Id == modelinv.PoId).FirstOrDefaultAsync();
                    //if (!string.IsNullOrEmpty(modelpo.Id))
                    //{
                    //    pay.PurchaseRequestId = modelpo.PurchaseRequestId == null ? "" : modelpo.PurchaseRequestId;
                    //    pay.MainServiceCategoryId = modelpo.MainserviceCategoryId;
                    //}
                    context.PhoenixEdit(modelinv);
                    await context.PhoenixAddAsync(accountPayable);
                    await context.PhoenixAddRangeAsync(accountPayableDetailData);
                    // await context.PhoenixAddAsync(pay);
                    var save = await context.SaveChangesAsync();
                    transaction.Commit();
                    return save;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message.ToString());
                }
            }

        }

        public async Task<List<ExecJournalAPViewDto>> GetJournalTmpAll(string referenceId, string invoiceReceivedId)
        {
            var data = await (from tmpj in context.ExecJournalAPView
                              join tbu in context.BusinessUnits on tmpj.business_unit_id equals tbu.Id
                              where tmpj.purchase_order_id == referenceId && tmpj.invoice_receivable_id == invoiceReceivedId
                              select new ExecJournalAPViewDto
                              {
                                  coa_id = tmpj.coa_id,
                                  code_rec = tmpj.code_rec,
                                  name5 = tmpj.name5,
                                  job_id = tmpj.job_id,
                                  AccountName = tmpj.name5,
                                  DescriptionJournal = tmpj.DescriptionJournal,
                                  JobIdName = tmpj.job_id == null ? "INTERNAL " + tmpj.purchase_request_id : "PROJECT " + tmpj.job_id,
                                  business_unit_id = tmpj.business_unit_id,
                                  DivisiName = tbu.UnitName,
                                  Credit = tmpj.Credit,
                                  Debit = tmpj.Debit,
                                  ap_level = tmpj.ap_level,
                                  line_code = tmpj.line_code,
                                  condition = tmpj.condition
                              }).ToListAsync();

            return data;
        }

        public async Task<List<ExecJournalAPViewDto>> GetJournalTmpAllFromTAP(string invoiceReceivedId)
        {
            var data = await (from tmpj in context.TemporaryAccountPayableDetail
                              join ta in context.TemporaryAccountPayables on tmpj.AccountPayableTemporaryId equals ta.Id
                              join tbu in context.BusinessUnits on ta.BusinessUnitId equals tbu.Id
                              join coa in context.ChartOfAccounts on tmpj.AccountId equals coa.Id
                              where ta.InvoiceReceivableId == invoiceReceivedId
                              select new ExecJournalAPViewDto
                              {
                                  coa_id = tmpj.AccountId,
                                  code_rec = coa.CodeRec,
                                  name5 = coa.Name5,
                                  job_id = ta.JobId,
                                  AccountName = coa.Name5,
                                  JobIdName = ta.JobId == null ? "INTERNAL " : "PROJECT " + ta.JobId,
                                  business_unit_id = ta.BusinessUnitId,
                                  DivisiName = tbu.UnitName,
                                  Credit = 0,
                                  Debit = (tmpj.UnitPrice * tmpj.Qty),
                                  line_code = tmpj.LineCode,
                                  DescriptionJournal = tmpj.ItemDescription
                              }).ToListAsync();

            return data;
        }

        public async Task<List<ExecJournalAPViewDto>> GetJournalTmpAllFromTAPHeader(string invoiceReceivedId)
        {
            var data = await (from tmpj in context.TemporaryAccountPayables
                              join tbu in context.BusinessUnits on tmpj.BusinessUnitId equals tbu.Id
                              join coa in context.ChartOfAccounts on tmpj.AccountId equals coa.Id
                              where tmpj.InvoiceReceivableId == invoiceReceivedId
                              select new ExecJournalAPViewDto
                              {
                                  coa_id = tmpj.AccountId,
                                  code_rec = coa.CodeRec,
                                  name5 = coa.Name5,
                                  job_id = tmpj.JobId,
                                  AccountName = coa.Name5,
                                  DescriptionJournal = coa.Name5,
                                  JobIdName = tmpj.JobId == null ? "INTERNAL " : "PROJECT " + tmpj.JobId,
                                  business_unit_id = tmpj.BusinessUnitId,
                                  DivisiName = tbu.UnitName,
                                  Credit = tmpj.Amount,
                                  Debit = 0,
                                  line_code = tmpj.LineCode,
                              }).ToListAsync();

            return data;
        }

        public async Task<TemporaryAccountPayable> GetTapFromINV(string invoiceReceivedId)
        {
            try
            {
                return await context.TemporaryAccountPayables.Where(x => x.IsDeleted.Equals(false) && x.InvoiceReceivableId == invoiceReceivedId).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }

        }

        public async Task<TemporaryAccountPayable> GetTAPFromINV(string invoiceReceivedId, string referenceId)
        {
            return await context.TemporaryAccountPayables.Where(x => x.IsDeleted.Equals(false) && x.InvoiceReceivableId == invoiceReceivedId && x.PurchaseOrderId == referenceId).FirstOrDefaultAsync();
        }

        public async Task<List<TemporaryAccountPayableJournal>> GetTapJournalWithTAPId(string TAPId)
        {
            try
            {
                var tap = await context.TemporaryAccountPayables.Where(x => x.IsDeleted.Equals(false) && x.Id == TAPId).FirstOrDefaultAsync();
                var tapname = "";
                if (string.IsNullOrEmpty(tap.JobId))
                {
                    tapname = "INTERNAL";
                }
                else
                {
                    tapname = "PROJECT - " + tap.JobId;
                }

                var data = await (from tb in context.TemporaryAccountPayableJournal
                                  join coa in context.ChartOfAccounts on tb.AccountId equals coa.Id
                                  join bu in context.MasterDivision on tb.DivisiId equals bu.Id
                                  where tb.reference_id == TAPId
                                  orderby tb.Credit descending
                                  orderby tb.Debit descending
                                  select new TemporaryAccountPayableJournal
                                  {
                                      AccountId = coa.Id,
                                      AccountCode = coa.CodeRec,
                                      AccountName = coa.Name5,
                                      JobId = tb.JobId,
                                      Description = tb.Description == null ? coa.Name5 : tb.Description,
                                      JobIdName = tapname,
                                      DivisiId = tb.DivisiId,
                                      DivisiName = bu.Name,
                                      Credit = tb.Credit,
                                      Debit = tb.Debit,
                                      LineCode = tb.LineCode,
                                  }).ToListAsync();

                if (data == null) data = new List<TemporaryAccountPayableJournal>();
                var data2 = await (from tb in context.TemporaryAccountPayableJournal
                                   join coa in context.ChartOfAccounts on tb.AccountId equals coa.Id
                                   join bu in context.BusinessUnits on tb.DivisiId equals bu.Id
                                   where tb.reference_id == TAPId
                                   orderby tb.Credit descending
                                   orderby tb.Debit descending
                                   select new TemporaryAccountPayableJournal
                                   {
                                       AccountId = coa.Id,
                                       AccountCode = coa.CodeRec,
                                       AccountName = coa.Name5,
                                       JobId = tb.JobId,
                                       Description = tb.Description == null ? coa.Name5 : tb.Description,
                                       JobIdName = tapname,
                                       DivisiId = tb.DivisiId,
                                       DivisiName = bu.UnitName,
                                       Credit = tb.Credit,
                                       Debit = tb.Debit,
                                       LineCode = tb.LineCode,
                                   }).ToListAsync();
                data.AddRange(data2);
                return data.OrderByDescending(x => x.CreatedOn).ToList();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }

        }


    }
}
