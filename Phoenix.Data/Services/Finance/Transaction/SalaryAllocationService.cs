using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Models;
using Phoenix.Data.RestApi;
using Phoenix.Shared.Core.Contexts;

namespace Phoenix.Data
{
    public interface ISalaryAllocationService : IDataService<SalaryAllocation>
    {
        Task<List<SalaryAllocation>> GetList(SalaryAllocation model);
        Task<List<SalaryAllocationDetail>> GetDetailJournal(string Id);
        Task<List<Journal>> GetJournal(string Id);
        Task<int> CreateJournal(SalaryAllocation entity);
        Task<int> EditJournal(SalaryAllocation entity);
    }

    public class SalaryAllocationService : ISalaryAllocationService
    {
        readonly DataContext context;
        readonly FinancePeriodService financePeriod;

        /// <summary>
        /// And endpoint to manage SalaryAllocation
        /// </summary>
        /// <param name="context">Database context</param>
        public SalaryAllocationService(DataContext context, FinancePeriodService financePeriod)
        {
            this.context = context;
            this.financePeriod = financePeriod;
        }

        public async Task<int> AddAsync(SalaryAllocation entity)
        {
            return await AddEdit(entity, false, false);
        }

        public Task<int> AddRangeAsync(params SalaryAllocation[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteAsync(SalaryAllocation entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteRageAsync(params SalaryAllocation[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> DeleteSoftAsync(SalaryAllocation entity)
        {
            await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.TransactionDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.SA);
            context.PhoenixDelete(entity);

            var journalDetailList = await context.SalaryAllocationDetails.AsNoTracking().Where(x => x.SalaryAllocationId == entity.Id).ToListAsync();
            context.PhoenixDeleteRange(journalDetailList.ToArray());

            var journalList = await context.Journal.AsNoTracking().Where(x => x.ReferenceId == entity.Id).ToListAsync();
            context.PhoenixDeleteRange(journalList.ToArray());
            return await context.SaveChangesAsync();
        }

        public Task<int> DeleteSoftRangeAsync(params SalaryAllocation[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> EditAsync(SalaryAllocation entity)
        {
            return await AddEdit(entity, true, false);
        }

        public Task<int> EditRangeAsync(SalaryAllocation entity)
        {
            throw new NotImplementedException();
        }

        public async Task<int> CreateJournal(SalaryAllocation entity)
        {
            return await AddEdit(entity, false, true);
        }

        public async Task<int> EditJournal(SalaryAllocation entity)
        {
            return await AddEdit(entity, true, true);
        }

        private async Task<int> AddEdit(SalaryAllocation entity, bool isEdit, bool isJournal)
        {
            await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.TransactionDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.SA);
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    if (isEdit)
                    {
                        context.PhoenixEdit(entity);
                    }
                    else
                    {
                        entity.Id = await TransID.GetTransId(context, Code.SalaryAllocation, DateTime.Today);
                        entity.Code = await TransID.GetTransId(context, Code.JournalVoucher, DateTime.Today);
                        await context.PhoenixAddAsync(entity);
                        await context.SaveChangesAsync();
                    }

                    if (!string.IsNullOrEmpty(entity.Id))
                    {
                        var detail = context.SalaryAllocationDetails.AsNoTracking().Where(x => x.SalaryAllocationId == entity.Id).ToList();
                        if (detail.Count > 0)
                        {
                            context.SalaryAllocationDetails.RemoveRange(detail);
                        }
                    }

                    if (entity.SalaryAllocationDetails != null)
                    {
                        Parallel.ForEach(entity.SalaryAllocationDetails, (item) =>
                        {
                            item.SalaryAllocationId = entity.Id;
                            context.PhoenixAddAsync(item);
                        });
                    }

                    //Create Journal
                    if (isJournal == true)
                    {
                        if (!string.IsNullOrEmpty(entity.Id))
                        {
                            var journal = context.Journal.AsNoTracking().Where(x => x.ReferenceId == entity.Id).ToList();
                            if (journal.Count > 0)
                            {
                                context.Journal.RemoveRange(journal);
                            }
                        }

                        var CurrencyCode = await context.Currencies.AsNoTracking().Where(x => x.Id == entity.CurrencyId).Select(x => x.CurrencyCode).FirstOrDefaultAsync();
                        var createJournal = entity.SalaryAllocationDetails
                            .Select(x => new Journal
                            {
                                Id = Guid.NewGuid().ToString(),
                                Description = entity.Description,
                                Debit = x.Debit,
                                Credit = x.Credit,
                                ReferenceId = entity.Id,
                                ReferenceNumber = entity.Code,
                                AccountId = x.CoaId,
                                ReferenceDate = entity.TransactionDate,
                                TransactionDate = entity.TransactionDate,
                                CurrencyId = entity.CurrencyId,
                                Unit = CurrencyCode,
                                ExchangeRate = entity.ExchangeRate,
                                LegalId = entity.LegalEntityId,
                                AffiliateId = entity.AffiliationId,
                                DivisiId = entity.BusinessUnitId,
                                LineCode = "1",
                                IsJobClosed = (entity.IsDraft ?? true) ? "N" : "Y"
                            });

                        await context.PhoenixAddRangeAsync(createJournal.ToArray());
                    }
                    else
                    {
                        if (!(entity.IsDraft ?? true))
                        {
                            var journal = await context.Journal.AsNoTracking().Where(x => x.ReferenceId == entity.Id).ToListAsync();
                            Parallel.ForEach(journal, (item) =>
                            {
                                item.IsJobClosed = "Y";
                                context.Journal.Update(item);
                            });
                        }
                    }

                    var save = await context.SaveChangesAsync();
                    transaction.Commit();
                    return save;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<List<SalaryAllocation>> GetList(SalaryAllocation model)
        {
            var result = await (from a in context.SalaryAllocations
                                join b in context.LegalEntity on a.LegalEntityId equals b.Id
                                join c in context.Affiliations on a.AffiliationId equals c.Id
                                join d in context.BusinessUnits on a.BusinessUnitId equals d.Id
                                where !(a.IsDeleted ?? false) &&
                                (string.IsNullOrEmpty(model.LegalEntityId) ? true : a.LegalEntityId == model.LegalEntityId) &&
                                ((model.TransactionDate != null && model.TransactionDateEnd != null) ? (a.TransactionDate >= model.TransactionDate && a.TransactionDate <= model.TransactionDateEnd) : true) &&
                                (string.IsNullOrEmpty(model.Code) ? true : a.Code == model.Code) &&
                                (string.IsNullOrEmpty(model.CurrencyId) ? true : a.CurrencyId == model.CurrencyId) &&
                                ((model.ExchangeRate == null) ? true : a.ExchangeRate == model.ExchangeRate) &&
                                (string.IsNullOrEmpty(model.Description) ? true : a.Description == model.Description)
                                select new SalaryAllocation
                                {
                                    Id = a.Id,
                                    TransactionDate = a.TransactionDate,
                                    Code = a.Code,
                                    //JvReferenceNo = a.JvReferenceNo,
                                    Description = a.Description,
                                    LegalEntityName = b.LegalEntityName,
                                    BusinessUnitName = d.UnitName,
                                    AfiliationName = c.AffiliationName,
                                    IsDraft = a.IsDraft,
                                    CreatedOn = a.CreatedOn
                                }).OrderByDescending(x => x.CreatedOn).ToListAsync();
            return result;
        }

        public Task<List<SalaryAllocation>> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public async Task<SalaryAllocation> Get(string Id) => await context.SalaryAllocations.AsNoTracking().Where(x => !(x.IsDeleted ?? false) && x.Id == Id).FirstOrDefaultAsync();

        public async Task<List<SalaryAllocationDetail>> GetDetailJournal(string Id)
        {
            return await (from a in context.SalaryAllocationDetails
                          join b in context.ChartOfAccounts on a.CoaId equals b.Id
                          where a.SalaryAllocationId == Id
                          select new SalaryAllocationDetail
                          {
                              Id = a.Id,
                              SalaryAllocationId = a.SalaryAllocationId,
                              CoaId = a.CoaId,
                              Description = a.Description,
                              Debit = a.Debit,
                              Credit = a.Credit,
                              CodeRec = b.CodeRec,
                              AccountName = b.Name5,
                              Condition = a.Condition,
                              CreatedBy = a.CreatedBy,
                              CreatedOn = a.CreatedOn
                          }).ToListAsync();
        }

        public async Task<List<Journal>> GetJournal(string Id)
        {
            var result = await (from a in context.Journal
                                join b in context.LegalEntity on a.LegalId equals b.Id
                                join c in context.Affiliations on a.AffiliateId equals c.Id
                                join d in context.BusinessUnits on a.DivisiId equals d.Id
                                join e in context.ChartOfAccounts on a.AccountId equals e.Id
                                where !(a.IsDeleted ?? false) && a.ReferenceId == Id
                                select new Journal
                                {
                                    AccountId = a.AccountId,
                                    AccountName = e.Name5,
                                    ReferenceNumber = a.ReferenceNumber,
                                    Description = a.Description,
                                    Debit = a.Debit,
                                    Credit = a.Credit,
                                    LegalEntityName = b.LegalEntityName,
                                    DivisiName = d.UnitName,
                                    AfiliationName = c.AffiliationName,
                                    CodeRec = e.CodeRec,
                                    CreatedOn = a.CreatedOn
                                }).OrderByDescending(x => x.CreatedOn).ToListAsync();
            return result;
        }

        public Task<List<SalaryAllocation>> Get()
        {
            throw new NotImplementedException();
        }
    }
}