﻿using System;
using System.Collections.Generic;
using System.Text;
using Phoenix.Data.Models.Finance;
using Phoenix.Data.Models.Finance.Transaction.InventoryIn;
using System.Threading.Tasks;
using Phoenix.Data.Models;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Phoenix.Data.Models.Finance.Transaction.InventoryIn;

namespace Phoenix.Data.Services.Finance.Transaction
{
    public interface IInventoryInService : IDataService<InventoryInModel>
    {
        Task<int> InsertInventoryInDTO(InventoryInModel model);
        Task<List<InventoryInJoin>> GetJoin();
    }
    public class InventoryInService : IInventoryInService
    {
        readonly DataContext context;

        public Task<int> AddAsync(InventoryInModel entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> AddRangeAsync(params InventoryInModel[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteAsync(InventoryInModel entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteRageAsync(params InventoryInModel[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteSoftAsync(InventoryInModel entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteSoftRangeAsync(params InventoryInModel[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> EditAsync(InventoryInModel entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> EditRangeAsync(InventoryInModel entity)
        {
            throw new NotImplementedException();
        }

        public async Task<List<InventoryInModel>> Get() => await context.InventoryIns.AsNoTracking().Where(x => !(x.IsDeleted ?? false)).ToListAsync();

        public Task<List<InventoryInModel>> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public async Task<InventoryInModel> Get(string id) => await context.InventoryIns.AsNoTracking().SingleOrDefaultAsync(x => !(x.IsDeleted ?? false) && x.Id == id);

        public async Task<List<InventoryInJoin>> GetJoin()
        {
            var list = Get();
            var query = from inn in Get().Result
                        join ig in await context.InventoryGoodsReceipts.AsNoTracking().ToListAsync() on
                        inn.InventoryGoodsReceiptid equals ig.Id
                        select new InventoryInJoin
                        {
                            Id = inn.Id,
                            InventoryInNumber = inn.InventoryInNumber,
                            InventoryInDatetime = inn.InventoryInDatetime,
                            InventoryGoodsReceiptid = inn.InventoryGoodsReceiptid,
                            GoodsReceiptNumber = ig.GoodReceiptNumber,
                            WarehouseId = inn.WarehouseId,
                            InventoryOutId = inn.InventoryOutId,
                            BusinessUnitId = inn.BusinessUnitId,
                            LegalEntityId = inn.LegalEntityId,
                            JobId = inn.JobId,
                            OrganizationId = inn.OrganizationId,
                            Remark = inn.Remark,
                        };

            return query.ToList();
            //throw new NotImplementedException();
        }

        public async Task<int> InsertInventoryInDTO(InventoryInModel model)
        {
            try
            {
                var details = model.Details.ToArray();
                var now = DateTime.Now;
                model.Id = Guid.NewGuid().ToString();
                model.InventoryInNumber = await TransID.GetTransId(context, Code.InventoryIn, DateTime.Today);
                model.InventoryInDatetime = now;
                await context.AddAsync(model);
                foreach (var detail in details)
                {
                    detail.Id = Guid.NewGuid().ToString();
                    detail.InventoryInId = model.Id;
                }
                await context.AddRangeAsync(details);
                return await context.SaveChangesAsync();

            }
            catch (System.Exception ex)
            {

                throw new Exception(ex.Message.ToString());
            }
        }
    }
}
