﻿using Phoenix.Data.Models.Finance.Transaction.InventoryGoodsReceipt;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Models;
using Phoenix.Shared.Core.PrincipalHelpers;
using System.Data.SqlClient;
using System.Data;
using Phoenix.Data.Models.Finance;

namespace Phoenix.Data.Services.Finance.Transaction
{
    public interface IInventoryGoodsReceiptService : IDataService<InventoryGoodsReceipt>
    {
        Task<InventoryGoodsReceipt> GetPO(string poId);
        Task<List<InventoryGoodsReceiptDetail>> GetPODetail(string poId);
        Task<dynamic> GetDDLPODetail(string poId);
        Task<PurchaseOrderDetail> GetPODData(string podId);
        Task<int> AddAsync(InventoryGoodsReceipt entity);
        Task<int> AddWithDetailAsync(InventoryGoodsReceipt Header, List<InventoryGoodsReceiptDetail> Details);
        Task<int> EditWithDetailAsync(InventoryGoodsReceipt Header, List<InventoryGoodsReceiptDetail> Details);
        Task<List<InventoryGoodsReceiptDetail>> GetDetail(string grId);
        Task<int> Generate(string grId);
        Task<List<InventoryGoodsReceiptDetail>> CekDetail(string poId);
        Task<List<InventoryDetail>> GetInventoryDetail(string grid);
        Task<List<FixedAsset>> GetFixedAsset(string grid);
        Task<List<InventoryGoodsReceiptDetail>> GetDetailHistory(string poId);
        Task<object> SubmitGoodReceipt(string inventory_good_receipt_id);
        Task<InventoryGoodsReceiptGetPODTO> GetPODTOById(string purchase_order_id);
    }

    public class InventoryGoodsReceiptService : IInventoryGoodsReceiptService
    {
        readonly DataContext context;

        public InventoryGoodsReceiptService(DataContext context)
        {
            this.context = context;
        }

        private async Task<int> AddEdit(InventoryGoodsReceipt entity, bool isEdit)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    //var isApprove = true;
                    entity.Id = Guid.NewGuid().ToString();
                    entity.GoodReceiptNumber = await TransID.GetTransId(context, Code.GoodsReceipt, DateTime.Today);
                    //entity.organization_id = "9EE4834225E334380667DDCF2F60F6DA";
                    await context.PhoenixAddAsync(entity);
                    var save = await context.SaveChangesAsync();
                    transaction.Commit();
                    return save;
                    //if (Details.Count > 0)
                    //{
                    //    List<PurchaseRequestDetail> tb = new List<PurchaseRequestDetail>();
                    //    foreach (PurchaseRequestDetail data in Details)
                    //    {
                    //        data.Id = Guid.NewGuid().ToString();
                    //        data.PurchaseRequestId = Header.Id;
                    //        data.CurrencyId = "46e4d57e-5ce2-4c31-82be-f81382dc243a";
                    //        data.ExchangeRate = 1;
                    //        tb.Add(data);
                    //    }

                    //    PurchaseRequestDetail[] tbn = tb.ToArray();
                    //    await context.PhoenixAddRangeAsync(tbn);
                    //}


                    //approval process request to system

                    //var dataMenu = await menuService.GetByUniqueName(MenuUnique.PurchaseRequest).ConfigureAwait(false);
                    //var vm = new Models.ViewModel.TransApprovalHrisVm()
                    //{
                    //    MenuId = dataMenu.Id,
                    //    RefId = Header.Id,
                    //    DetailLink = $"{ApprovalLink.PurchaseRequest}?Id={Header.Id}&isApprove=true",
                    //    //Tname = "fn.PurchaseRequest",//"SP#NAMA SP NYA",
                    //    IdTemplate = idTemplate
                    //};
                    //var subGroupId = ClainJwtPrincipalHelper.GetBusinessSubGroup(context.HttpContext);
                    //var divisionId = ClainJwtPrincipalHelper.GetBusinessUnitDivisi(context.HttpContext);
                    //var employeeId = ClainJwtPrincipalHelper.GetEmployeeId(context.HttpContext);
                    //isApprove = await globalFcApproval.Save(vm, subGroupId, divisionId, employeeId);

                    //if (isApprove)
                    //{
                    //    var save = await context.SaveChangesAsync();
                    //    transaction.Commit();
                    //    return save;
                    //}
                    //else
                    //{
                    //    transaction.Rollback();
                    //    throw new Exception("please check the approval template that will be processed");
                    //}
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message.ToString());
                }
            }
        }

        public async Task<int> AddAsync(InventoryGoodsReceipt entity)
        {
            entity.Id = Guid.NewGuid().ToString();
            entity.GoodReceiptNumber = await TransID.GetTransId(context, Code.GoodsReceipt, DateTime.Today);
            //entity.DeliveryOrderNumber = entity.ReferenceNumber;
            await context.PhoenixAddAsync(entity);
            return await context.SaveChangesAsync();
            //return await AddEdit(entity, false);
        }

        public Task<int> AddRangeAsync(params InventoryGoodsReceipt[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteAsync(InventoryGoodsReceipt entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteRageAsync(params InventoryGoodsReceipt[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteSoftAsync(InventoryGoodsReceipt entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteSoftRangeAsync(params InventoryGoodsReceipt[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> EditAsync(InventoryGoodsReceipt entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> EditRangeAsync(InventoryGoodsReceipt entity)
        {
            throw new NotImplementedException();
        }

        //public Task<List<InventoryGoodsReceipt>> Get()
        //{
        //    throw new NotImplementedException();
        //}

        public async Task<List<InventoryGoodsReceipt>> Get()
        {
            var result = await context.InventoryGoodsReceipts.ToListAsync();
            return result;
        }

        public Task<List<InventoryGoodsReceipt>> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public async Task<InventoryGoodsReceipt> Get(string id)
        {
            //var query = await context.InventoryGoodsReceipts.AsNoTracking().Where(x=>x.Id==id).FirstOrDefaultAsync();
            //return query;

            var data = await (from t1 in context.InventoryGoodsReceipts
                              join t2 in context.BusinessUnits on t1.BusinessUnitId equals t2.Id into t1t2
                              from t2 in t1t2.DefaultIfEmpty()
                              join t3 in context.LegalEntity on t1.LegalEntityId equals t3.Id into t1t3
                              from t3 in t1t3.DefaultIfEmpty()
                              join t4 in context.Companys on t1.CompanyId equals t4.Id into t1t4
                              from t4 in t1t4.DefaultIfEmpty()
                              join t5 in context.CompanyBrands on t1.BrandId equals t5.Id into t1t5
                              from t5 in t1t5.DefaultIfEmpty()
                              join t6 in context.Affiliations on t2.AffiliationId equals t6.Id into t2t6
                              from t6 in t2t6.DefaultIfEmpty()
                              where t1.Id == id && t1.IsDeleted.Equals(false)
                              select new InventoryGoodsReceipt
                              {
                                  Id = t1.Id,
                                  AffiliationId = t1.AffiliationId,
                                  GoodReceiptNumber = t1.GoodReceiptNumber,
                                  ReferenceNumber = t1.ReferenceNumber,
                                  VendorId = t1.VendorId,
                                  VendorName = t1.VendorName,
                                  GoodReceiptDatetime = t1.GoodReceiptDatetime,
                                  PurchaseOrderId = t1.PurchaseOrderId,
                                  PurchaseOrderNumber = t1.PurchaseOrderNumber,
                                  ReceivedBy = t1.ReceivedBy,
                                  ReceivedByName = t1.ReceivedByName,
                                  InvoicingTypeId = t1.InvoicingTypeId,
                                  InvoicingTypeName = t1.InvoicingTypeName,
                                  JobId = t1.JobId,
                                  JobName = t1.JobName,
                                  Term = t1.Term,
                                  CompanyId = t1.CompanyId,
                                  CompanyName = t4.CompanyName,
                                  BrandId = t1.BrandId,
                                  BrandName = t5.BrandName,
                                  BusinessUnitId = t1.BusinessUnitId,
                                  BusinessUnitName = t2.UnitName,
                                  LegalEntityId = t1.LegalEntityId,
                                  LegalEntityName = t3.LegalEntityName,
                                  LocationId = t1.LocationId,
                                  AffiliationName = t6.AffiliationName,
                                  IsGenerated = t1.IsGenerated,
                                  Status=t1.Status
                              }
                              ).FirstOrDefaultAsync();
            return data;
        }

        public async Task<InventoryGoodsReceipt> GetPO(string poId)
        {
            var queryAP = await (from ap in context.AccountPayables
                                 where ap.PurchaseOrderId == poId
                                 select new PurchaseOrder
                                 {
                                     Id = ap.Id,
                                     SubTotal = ap.Amount
                                 }).ToListAsync();



            var countAP = queryAP.Count();
            var totaldata = countAP;

            var total_termin = "";

            var potemp = await context.PurchaseOrders.Where(x => x.IsDeleted.Equals(false) && x.Id == poId).FirstOrDefaultAsync();
            //kalo langsung lunas 100%
            if (potemp.PercentPaymentTerm3 == 0 && potemp.PercentPaymentTerm2 == 0)
            {
                total_termin = "1 from 1 (" + potemp.PercentPaymentTerm1 + ")";
            }//kalau di bayar 2x
            else if (potemp.PercentPaymentTerm3 == 0 && potemp.PercentPaymentTerm1 != 0 && potemp.PercentPaymentTerm2 != 0)
            {
                if (totaldata == 0)
                {
                    total_termin = "1 from 2 (" + potemp.PercentPaymentTerm1 + ")";

                }
                else if (totaldata == 1)
                {
                    total_termin = "2 from 2 (" + potemp.PercentPaymentTerm2 + ")";
                }
                else
                {

                }
                total_termin = "More than 2";
            }
            else
            {

                if (totaldata == 0)
                {
                    total_termin = "1 from 3 (" + potemp.PercentPaymentTerm1 + ")";

                }
                else if (totaldata == 1)
                {
                    total_termin = "2 from 3 (" + potemp.PercentPaymentTerm2 + ")";
                }
                else if (totaldata == 2)
                {
                    total_termin = "3 from 3 (" + potemp.PercentPaymentTerm3 + ")";
                }
                else
                {
                    total_termin = "More than 3";
                }

            }


            var query = await (from po in context.PurchaseOrders
                               join pod in context.PurchaseOrderDetails on po.Id equals pod.PurchaseOrderId into p
                               from pod in p.DefaultIfEmpty()
                               join job in context.JobDetail on po.JobId equals job.Id into pj
                               from job in pj.DefaultIfEmpty()
                               join le in context.LegalEntity on po.LegalEntityId equals le.Id into l
                               from le in l.DefaultIfEmpty()
                               join company in context.Companys on po.CompanyId equals company.Id into c
                               from company in c.DefaultIfEmpty()
                               join companyBrand in context.CompanyBrands on po.CompanyBrandId equals companyBrand.Id into cb
                               from companyBrand in cb.DefaultIfEmpty()
                               join businessUnit in context.BusinessUnits on po.BusinessUnitId equals businessUnit.Id into b
                               from businessUnit in b.DefaultIfEmpty()
                               join aff in context.Affiliations on businessUnit.AffiliationId equals aff.Id into baff
                               from aff in baff.DefaultIfEmpty()
                               where po.Id == poId
                               select new InventoryGoodsReceipt
                               {
                                   PurchaseOrderId = po.Id,
                                   PurchaseOrderNumber = po.PurchaseOrderNumber,
                                   InvoicingTypeId = po.InvoicingType,
                                   InvoicingTypeName = po.InvoicingType,
                                   JobId = job.Id,
                                   JobName = job.JobName,
                                   CompanyId = company.Id,
                                   CompanyName = company.CompanyName,
                                   BrandId = companyBrand.Id,
                                   BrandName = companyBrand.BrandName,
                                   BusinessUnitId = po.BusinessUnitId,
                                   BusinessUnitName = businessUnit.UnitName,
                                   LegalEntityId = po.LegalEntityId,
                                   LegalEntityName = le.LegalEntityName,
                                   Term = total_termin,
                                   AffiliationId = job.AffiliationId,
                                   AffiliationName = aff.AffiliationName

                               }
                              ).FirstOrDefaultAsync();
            return query;
        }

        public async Task<List<InventoryGoodsReceiptDetail>> GetPODetail(string poId)
        {
            var query = await (from pod in context.PurchaseOrderDetails
                               join item in context.Item on pod.ItemId equals item.Id into t1
                               from item in t1.DefaultIfEmpty()
                               join itemType in context.ItemType on pod.ItemTypeId equals itemType.Id into t2
                               from itemType in t2.DefaultIfEmpty()
                               join uom in context.Uom on pod.UomId equals uom.Id into t3
                               from uom in t3.DefaultIfEmpty()
                               join igr in context.InventoryGoodsReceipts on pod.Id equals igr.PurchaseOrderId into t4
                               from igr in t4.DefaultIfEmpty()
                               join igrd in context.InventoryGoodsReceiptsDetail on igr.Id equals igrd.InventoryGoodsReceiptId into t5
                               from igrd in t5.DefaultIfEmpty()
                               where pod.PurchaseOrderId == poId && (itemType.TypeName == "Inventory" || itemType.TypeName == "Fixed Asset")
                               select new InventoryGoodsReceiptDetail
                               {
                                   //Id = pod.Id,
                                   PurchaseOrderDetailId = pod.Id,
                                   ItemId = pod.ItemId,
                                   ItemName = pod.ItemName,
                                   ItemTypeId = pod.ItemTypeId,
                                   UOMId = pod.UomId,
                                   PurchasedQty = pod.Qty,
                                   //UnitPrice = pod.UnitPrice,
                                   //Amount = pod.Amount,                                    
                                   ItemTypeName = itemType.TypeName,
                                   ItemCode = item.ItemCode,
                                   UnitName = uom.unit_name,
                                   ReceivedQty = igrd.ReceivedQty,
                                   RemainingQty = igrd.RemainingQty,
                                   TotalReceivedQty = igrd.TotalReceivedQty
                               }
                                ).ToListAsync();
            return query;
        }

        public async Task<int> AddWithDetailAsync(InventoryGoodsReceipt Header, List<InventoryGoodsReceiptDetail> Details)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    //Header.Id = Guid.NewGuid().ToString();
                    Header.GoodReceiptNumber = await TransID.GetTransId(context, Code.GoodsReceipt, DateTime.Today);
                    Header.Status = StatusTransaction.StatusName(100);
                    //Header.DeliveryOrderNumber = Header.ReferenceNumber;
                    //Header.OrganizationId = "9EE4834225E334380667DDCF2F60F6DA";
                    Header.ReceivedBy = ClainJwtPrincipalHelper.GetEmployeeId(context.HttpContext);
                    await context.PhoenixAddAsync(Header);

                    if (Details.Count > 0)
                    {
                        List<InventoryGoodsReceiptDetail> igrd = new List<InventoryGoodsReceiptDetail>();
                        foreach (InventoryGoodsReceiptDetail data in Details)
                        {
                            data.Id = Guid.NewGuid().ToString();
                            //data.TotalReceivedQty = data.TotalReceivedQty + data.ReceivedQty;
                            data.InventoryGoodsReceiptId = Header.Id;
                            igrd.Add(data);
                        }

                        InventoryGoodsReceiptDetail[] igrdArray = igrd.ToArray();
                        await context.PhoenixAddRangeAsync(igrdArray);
                    }
                    var save = await context.SaveChangesAsync();
                    transaction.Commit();
                    return save;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message.ToString());
                }
            }
        }

        public async Task<List<InventoryGoodsReceiptDetail>> GetDetail(string grId)
        {
            var query = await (from igrd in context.InventoryGoodsReceiptsDetail
                               where igrd.InventoryGoodsReceiptId == grId
                               select new InventoryGoodsReceiptDetail
                               {
                                   Id = igrd.Id,
                                   InventoryGoodsReceiptId = igrd.InventoryGoodsReceiptId,
                                   PurchaseOrderDetailId = igrd.Id,
                                   ItemId = igrd.ItemId,
                                   ItemName = igrd.ItemName,
                                   ItemTypeId = igrd.ItemTypeId,
                                   UOMId = igrd.UOMId,
                                   PurchasedQty = igrd.PurchasedQty,
                                   //UnitPrice = igrd.UnitPrice,
                                   //Amount = igrd.Amount,                                    
                                   ItemTypeName = igrd.ItemTypeName,
                                   ItemCode = igrd.ItemCode,
                                   UnitName = igrd.UnitName,
                                   ReceivedQty = igrd.ReceivedQty,
                                   RemainingQty = igrd.RemainingQty,
                                   TotalReceivedQty = igrd.TotalReceivedQty,
                                   Task = igrd.Task,
                                   TaskId = igrd.TaskId
                               }
                ).ToListAsync();
            //var result = await context.InventoryGoodsReceiptsDetail.Where(x => x.InventoryGoodsReceiptId == grId).ToListAsync();
            return query;
        }

        public async Task<int> EditWithDetailAsync(InventoryGoodsReceipt Header, List<InventoryGoodsReceiptDetail> Details)
        {
            try
            {
                //Header.Id = Guid.NewGuid().ToString();
                //Header.ServiceReceiptNumber = await TransID.GetTransId(context, Code.ServiceReceipt, DateTime.Today);
                //Header.DeliveryOrderNumber = Header.ReferenceNumber;
                //Header.OrganizationId = "9EE4834225E334380667DDCF2F60F6DA";
                Header.Status = StatusTransaction.StatusName(100);
                context.PhoenixEdit(Header);

                if (Details.Count > 0)
                {
                    //List<InventoryGoodsReceiptDetail> srdtemp = new List<InventoryGoodsReceiptDetail>();
                    //var checkAvailable = await context.InventoryGoodsReceiptsDetail.Where(x => x.InventoryGoodsReceiptId == Header.Id && x.IsDeleted.Equals(false)).ToListAsync();
                    //foreach (InventoryGoodsReceiptDetail data in checkAvailable)
                    //{
                    //    srdtemp.Add(data);
                    //}

                    //InventoryGoodsReceiptDetail[] srdArraytemp = srdtemp.ToArray();
                    //context.PhoenixDeleteRange(srdArraytemp);

                    List<InventoryGoodsReceiptDetail> srdnew = new List<InventoryGoodsReceiptDetail>();
                    foreach (InventoryGoodsReceiptDetail data in Details)
                    {
                        //data.Id = Guid.NewGuid().ToString();
                        //data.InventoryGoodsReceiptId = Header.Id;
                        data.TotalReceivedQty = data.TotalReceivedQty + data.ReceivedQty;
                        srdnew.Add(data);
                    }

                    InventoryGoodsReceiptDetail[] srdArraynew = srdnew.ToArray();
                    //context.PhoenixAddRange(srdArraynew);

                    context.PhoenixEditRange(srdArraynew);
                }
                //var save = await context.SaveChangesAsync();
                //transaction.Commit();
                return await context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                //transaction.Rollback();
                throw new Exception(ex.Message.ToString());
            }
        }

        public async Task<dynamic> GetDDLPODetail(string poId)
        {
            var query = await (from pod in context.PurchaseOrderDetails
                               join type in context.ItemType on pod.ItemTypeId equals type.Id into t1
                               from type in t1.DefaultIfEmpty()
                               where pod.IsDeleted == false && pod.PurchaseOrderId == poId && (type.TypeName == "Inventory" || type.TypeName == "Fixed Asset")
                               select new
                               {
                                   Text = pod.ItemName,
                                   Value = pod.Id
                               }).ToListAsync();
            return query;
            //return await context.PurchaseOrderDetails.Where(x => !(x.IsDeleted ?? false) && x.PurchaseOrderId == poId).Select(x => new { Text = x.ItemName, Value = x.Id }).ToListAsync();
        }

        public async Task<PurchaseOrderDetail> GetPODData(string podId)
        {
            var query = await (from pod in context.PurchaseOrderDetails
                               join item in context.Item on pod.ItemId equals item.Id into t1
                               from item in t1.DefaultIfEmpty()
                               join type in context.ItemType on pod.ItemTypeId equals type.Id into t2
                               from type in t2.DefaultIfEmpty()
                               join uom in context.Uom on pod.UomId equals uom.Id into t3
                               from uom in t3.DefaultIfEmpty()
                               where pod.Id == podId && pod.IsDeleted == false
                               select new PurchaseOrderDetail
                               {
                                   Id = pod.Id,
                                   ItemName = pod.ItemName,
                                   ItemId = pod.ItemId,
                                   ItemCode = item.ItemCode,
                                   ItemTypeId = pod.ItemTypeId,
                                   ItemTypeName = type.TypeName,
                                   Qty = pod.Qty,
                                   UomId = pod.UomId,
                                   UomName = uom.unit_name
                               }
                               ).FirstOrDefaultAsync();
            return query;
            //return await context.PurchaseOrderDetails.Where(x => !(x.IsDeleted ?? false) && x.Id == podId).FirstOrDefaultAsync();
        }

        public async Task<int> Generate(string grId)
        {
            try
            {
                var updateIGR = await context.InventoryGoodsReceipts.Where(x => x.Id == grId).FirstOrDefaultAsync();
                updateIGR.IsGenerated = true;
                context.PhoenixEdit(updateIGR);
                //await context.ExecuteStoredProcedure("EXECUTE fn.proc_generate_aset @p_gr_id", parameters: new[] { new SqlParameter("@p_gr_id", grId) });
                await context.Database.ExecuteSqlCommandAsync("EXECUTE fn.proc_generate_aset @p_gr_id", parameters: new[] { new SqlParameter("@p_gr_id", grId) });

                var save = await context.SaveChangesAsync();
                return save;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<InventoryGoodsReceiptDetail>> CekDetail(string poId)
        {
            var query = new List<InventoryGoodsReceiptDetail>();
            var poCount = await context.InventoryGoodsReceipts.Where(x => x.PurchaseOrderId == poId).CountAsync();
            if (poCount > 0)
            {
                var po = await context.InventoryGoodsReceipts.Where(x => x.PurchaseOrderId == poId).OrderByDescending(x => x.ModifiedOn).FirstOrDefaultAsync();
                query = await (from igrd in context.InventoryGoodsReceiptsDetail
                               where igrd.InventoryGoodsReceiptId == po.Id
                               select new InventoryGoodsReceiptDetail
                               {
                                   Id = igrd.Id,
                                   InventoryGoodsReceiptId = igrd.InventoryGoodsReceiptId,
                                   //PurchaseOrderDetailId = igrd.Id,
                                   //edit by mbeng
                                   PurchaseOrderDetailId = igrd.PurchaseOrderDetailId,
                                   ItemId = igrd.ItemId,
                                   ItemName = igrd.ItemName,
                                   ItemTypeId = igrd.ItemTypeId,
                                   UOMId = igrd.UOMId,
                                   PurchasedQty = igrd.PurchasedQty,
                                   UnitPrice = igrd.UnitPrice,
                                   Amount = igrd.Amount,
                                   ItemTypeName = igrd.ItemTypeName,
                                   ItemCode = igrd.ItemCode,
                                   UnitName = igrd.UnitName,
                                   ReceivedQty = 0,
                                   RemainingQty = igrd.RemainingQty,
                                   TotalReceivedQty = igrd.TotalReceivedQty,
                                   Task = igrd.Task,
                                   TaskId = igrd.TaskId
                               }
                ).ToListAsync();
            }
            else
            {
                query = await (from pod in context.PurchaseOrderDetails
                               join item in context.Item on pod.ItemId equals item.Id into t1
                               from item in t1.DefaultIfEmpty()
                               join itemType in context.ItemType on pod.ItemTypeId equals itemType.Id into t2
                               from itemType in t2.DefaultIfEmpty()
                               join uom in context.Uom on pod.UomId equals uom.Id into t3
                               from uom in t3.DefaultIfEmpty()
                               join igr in context.InventoryGoodsReceipts on pod.Id equals igr.PurchaseOrderId into t4
                               from igr in t4.DefaultIfEmpty()
                               join igrd in context.InventoryGoodsReceiptsDetail on igr.Id equals igrd.InventoryGoodsReceiptId into t5
                               from igrd in t5.DefaultIfEmpty()
                               where pod.PurchaseOrderId == poId //&& (itemType.TypeName == "Inventory" || itemType.TypeName == "Fixed Asset")
                               select new InventoryGoodsReceiptDetail
                               {
                                   //Id = pod.Id,
                                   PurchaseOrderDetailId = pod.Id,
                                   ItemId = pod.ItemId,
                                   ItemName = pod.ItemName,
                                   ItemTypeId = pod.ItemTypeId,
                                   UOMId = pod.UomId,
                                   PurchasedQty = pod.Qty,
                                   UnitPrice = pod.UnitPrice,
                                   Amount = pod.Amount,
                                   ItemTypeName = itemType.TypeName,
                                   ItemCode = pod.ItemCode,
                                   UnitName = uom.unit_name,
                                   ReceivedQty = igrd.ReceivedQty,
                                   RemainingQty = igrd.RemainingQty,
                                   TotalReceivedQty = igrd.TotalReceivedQty,
                                   Task = pod.Task,
                                   TaskId = pod.TaskId
                               }
                                ).ToListAsync();
            }
            //query = await context.InventoryGoodsReceiptsDetail.Where(x => x.InventoryGoodsReceiptId == po.Id).CountAsync();
            return query;
        }

        public async Task<List<InventoryDetail>> GetInventoryDetail(string grid)
        {
            var query = new List<InventoryDetail>();

            var getHeader = await context.Inventory.Where(x => x.IsDeleted.Equals(false) && x.InventoryGoodsReceiptId == grid).ToListAsync();
            foreach (var item in getHeader)
            {
                query.AddRange( await context.InventoryDetail.Where(x => x.IsDeleted.Equals(false) && x.InventoryId == item.Id).ToListAsync());
            }

            return query;
        }

        public async Task<List<FixedAsset>> GetFixedAsset(string grid)
        {
            var query = await context.FixedAsset.Where(x => x.IsDeleted.Equals(false) && x.GoodReceiptId == grid).ToListAsync();
            return query;
        }

        public async Task<List<InventoryGoodsReceiptDetail>> GetDetailHistory(string poId)
        {
            var query = await (from igrd in context.InventoryGoodsReceiptsDetail
                               join igr in context.InventoryGoodsReceipts on igrd.InventoryGoodsReceiptId equals igr.Id
                               where igr.PurchaseOrderId == poId && igrd.ReceivedQty != 0
                               select new InventoryGoodsReceiptDetail
                               {
                                   Id = igrd.Id,
                                   ModifiedOn = igrd.ModifiedOn,
                                   ItemName = igrd.ItemName,
                                   ItemCode = igrd.ItemCode,
                                   ItemTypeName = igrd.ItemTypeName,
                                   ReceivedQty = igrd.ReceivedQty,
                                   InventoryGoodsReceiptId = igr.GoodReceiptNumber,
                                   Task = igrd.Task,
                                   TaskId = igrd.TaskId
                               }
                               ).OrderByDescending(x => x.ModifiedOn).ToListAsync();
            return query;
        }

        public async Task<object> SubmitGoodReceipt(string inventory_good_receipt_id)
        {
            try
            {
                //var updateIGR = await context.InventoryGoodsReceipts.Where(x => x.Id == grId).FirstOrDefaultAsync();
                //updateIGR.IsGenerated = true;
                //context.PhoenixEdit(updateIGR);
                //await context.ExecuteStoredProcedure("EXECUTE fn.proc_generate_aset @p_gr_id", parameters: new[] { new SqlParameter("@p_gr_id", grId) });
                var _inventory_good_receipt_id = new SqlParameter("inventory_good_receipt_id", inventory_good_receipt_id);
                var message = new SqlParameter("messege", SqlDbType.NVarChar, 300)
                {
                    Direction = System.Data.ParameterDirection.Output
                };
                var status_code = new SqlParameter("status_code", SqlDbType.Int)
                {
                    Direction = System.Data.ParameterDirection.Output
                };
                await context.Database.ExecuteSqlCommandAsync("EXECUTE fn.submit_inventory_good_receipt @inventory_good_receipt_id,@messege out,@status_code out",
                    _inventory_good_receipt_id, message, status_code);
                await context.SaveChangesAsync();
                var data = new { StatusCode = (int)status_code.Value, Message = message.Value };

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<InventoryGoodsReceiptGetPODTO> GetPODTOById(string purchase_order_id)
        {
            var queryAP = await (from ap in context.AccountPayables
                                 where ap.PurchaseOrderId == purchase_order_id
                                 select new PurchaseOrder
                                 {
                                     Id = ap.Id,
                                     SubTotal = ap.Amount
                                 }).ToListAsync();



            var countAP = queryAP.Count();
            var totaldata = countAP;

            var total_termin = "";

            var potemp = await context.PurchaseOrders.Where(x => x.IsDeleted.Equals(false) && x.Id == purchase_order_id).FirstOrDefaultAsync();
            //kalo langsung lunas 100%
            if (potemp.PercentPaymentTerm3 == 0 && potemp.PercentPaymentTerm2 == 0)
            {
                total_termin = "1 from 1 (" + potemp.PercentPaymentTerm1 + ")";
            }//kalau di bayar 2x
            else if (potemp.PercentPaymentTerm3 == 0 && potemp.PercentPaymentTerm1 != 0 && potemp.PercentPaymentTerm2 != 0)
            {
                if (totaldata == 0)
                {
                    total_termin = "1 from 2 (" + potemp.PercentPaymentTerm1 + ")";

                }
                else if (totaldata == 1)
                {
                    total_termin = "2 from 2 (" + potemp.PercentPaymentTerm2 + ")";
                }
                else
                {

                }
                total_termin = "More than 2";
            }
            else
            {

                if (totaldata == 0)
                {
                    total_termin = "1 from 3 (" + potemp.PercentPaymentTerm1 + ")";

                }
                else if (totaldata == 1)
                {
                    total_termin = "2 from 3 (" + potemp.PercentPaymentTerm2 + ")";
                }
                else if (totaldata == 2)
                {
                    total_termin = "3 from 3 (" + potemp.PercentPaymentTerm3 + ")";
                }
                else
                {
                    total_termin = "More than 3";
                }

            }


            var query = await (from po in context.PurchaseOrders
                               join pod in context.PurchaseOrderDetails on po.Id equals pod.PurchaseOrderId into p
                               from pod in p.DefaultIfEmpty()
                               join job in context.JobDetail on po.JobId equals job.Id into pj
                               from job in pj.DefaultIfEmpty()
                               join le in context.LegalEntity on po.LegalEntityId equals le.Id into l
                               from le in l.DefaultIfEmpty()
                               join company in context.Companys on po.CompanyId equals company.Id into c
                               from company in c.DefaultIfEmpty()
                               join companyBrand in context.CompanyBrands on po.CompanyBrandId equals companyBrand.Id into cb
                               from companyBrand in cb.DefaultIfEmpty()
                               join businessUnit in context.BusinessUnits on po.BusinessUnitId equals businessUnit.Id into b
                               from businessUnit in b.DefaultIfEmpty()
                               join aff in context.Affiliations on businessUnit.AffiliationId equals aff.Id into baff
                               from aff in baff.DefaultIfEmpty()
                               join Employee in context.EmployeeBasicInfos on po.PicId equals Employee.Id
                               where po.Id == purchase_order_id
                               select new InventoryGoodsReceiptGetPODTO
                               {
                                   PurchaseOrderId = po.Id,
                                   PurchaseOrderNumber = po.PurchaseOrderNumber,
                                   InvoicingTypeId = po.InvoicingType,
                                   InvoicingTypeName = po.InvoicingType,
                                   JobId = job.Id,
                                   JobName = job.JobName,
                                   CompanyId = company.Id,
                                   CompanyName = company.CompanyName,
                                   BrandId = companyBrand.Id,
                                   BrandName = companyBrand.BrandName,
                                   BusinessUnitId = po.BusinessUnitId,
                                   BusinessUnitName = businessUnit.UnitName,
                                   LegalEntityId = po.LegalEntityId,
                                   LegalEntityName = le.LegalEntityName,
                                   Term = total_termin,
                                   AffiliationId = job.AffiliationId,
                                   AffiliationName = aff.AffiliationName,
                                   PicId = po.PicId,
                                   PicName = Employee.NameEmployee
                               }
                              ).FirstOrDefaultAsync();
            return query;
        }
    }


}
