﻿using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.Data.Services.Finance.Transaction
{
    public interface IInvoiceReceivableService
    {
        Task<InvoiceReceivable> Get(string Id);
        Task<InvoiceReceivable> GetPoById(string poId);
        Task<List<InvoiceReceivable>> GetList(string status, string businessUnit, string legalEntity, string invoiceNumber);
        Task<List<AccountPayable>> GetAPList(string status, string businessUnit, string legalEntity, string accountPayableNumber);
        Task<List<InvoiceReceivableDetail>> GetDetailInvoiceList(string poId, string paymentTo, string invId);
        Task<List<Journal>> GetJournal(string invId);
        Task<int> AddAsync(InvoiceReceivable entity);
        Task<int> EditAsync(InvoiceReceivable entity);
        Task<int> CreateJournalAsync(InvoiceReceivable entity);
    }

    public class InvoiceReceivableService : IInvoiceReceivableService
    {
        readonly DataContext context;
        readonly IPurchaseOrderService poService;
        readonly StatusLogService log;
        readonly FileService uploadFile;
        readonly FinancePeriodService financePeriod;

        public InvoiceReceivableService(DataContext context, IPurchaseOrderService poService, StatusLogService log, FileService uploadFile, FinancePeriodService financePeriod)
        {
            this.context = context;
            this.poService = poService;
            this.log = log;
            this.uploadFile = uploadFile;
            this.financePeriod = financePeriod;
        }

        public async Task<InvoiceReceivable> Get(string Id)
        {
            var data = await context.InvoiceReceivables.Where(x => !(x.IsDeleted ?? false) && x.Id == Id).FirstOrDefaultAsync();
            if (data != null)
            {
                data = await GetFile(data);
                data.PurchaseOrder = await poService.Get(data.PoId);
                if (data.PurchaseOrder != null) data.Company = await context.Companys.AsNoTracking().Where(x => !(x.IsDeleted ?? false) && x.Id == data.PurchaseOrder.CompanyId).FirstOrDefaultAsync();
            }
            return data;
        }

        public async Task<InvoiceReceivable> GetPoById(string poId)
        {
            var data = new InvoiceReceivable();
            var po = await poService.Get(poId);

            if (po != null)
            {

                var totalpayment = 0;
                if ((po.PercentPaymentTerm1 ?? 0) != 0) totalpayment++;
                if ((po.PercentPaymentTerm2 ?? 0) != 0) totalpayment++;
                if ((po.PercentPaymentTerm3 ?? 0) != 0) totalpayment++;

                var paymentTo = context.InvoiceReceivables.Where(y => y.PoId == poId && y.InvoiceStatusId != InvoiceReceivedStatus.Rejected).Count() + 1;
                var paymentPercentage = (paymentTo == 1 ? po.PercentPaymentTerm1 : paymentTo == 2 ? po.PercentPaymentTerm2 : paymentTo == 3 ? po.PercentPaymentTerm3 : 0);

                var payment = paymentTo.ToString() + " of " + totalpayment.ToString();
                var detail = await GetDetailInvoiceList(poId, payment, null);
                data.NetAmount = detail.Sum(x => x.InvoiceValue);
                data.DiscountPercent = (po.DiscountPercent ?? 0);
                data.DiscountAmount = data.NetAmount * ((po.DiscountPercent ?? 0) / 100);
                data.ValueAddedTaxPercent = po.VatPercent;
                data.ValueAddedTax = data.NetAmount * (data.ValueAddedTaxPercent / 100);
                data.TotalCostVendor = (data.NetAmount + data.ValueAddedTax) - data.DiscountAmount;
                data.PaymentTo = payment;
                data.PurchaseOrder = po;
                data.Company = await context.Companys.AsNoTracking().Where(x => !(x.IsDeleted ?? false) && x.Id == po.CompanyId).FirstOrDefaultAsync();
            }
            else
            {

                data.PurchaseOrder = new PurchaseOrder();
            }
            return data;
        }

        public async Task<List<InvoiceReceivable>> GetList(string status, string businessUnit, string legalEntity, string invoiceNumber)
        {
            return await (from inv in context.InvoiceReceivables
                          join client in context.Companys on inv.ClientId equals client.Id
                          join bu in context.BusinessUnits on inv.BusinessUnitId equals bu.Id
                          join lgl in context.LegalEntity on inv.LegalEntityId equals lgl.Id
                          where !(inv.IsDeleted ?? false) &&
                          ((status ?? "0") == "0" ? true : inv.InvoiceStatusId == status) &&
                          ((businessUnit ?? "0") == "0" ? true : inv.BusinessUnitId == businessUnit) &&
                          ((legalEntity ?? "0") == "0" ? true : inv.LegalEntityId == legalEntity) &&
                          ((invoiceNumber ?? "0") == "0" ? true : inv.InvoiceNumber == invoiceNumber)
                          select new InvoiceReceivable
                          {
                              Id = inv.Id,
                              InvoiceNumber = inv.InvoiceNumber,
                              InvoiceDate = inv.InvoiceDate,
                              InvoiceVendor = inv.InvoiceVendor,
                              InvoiceStatusId = inv.InvoiceStatusId == null ? "1" : inv.InvoiceStatusId,
                              ClientName = client.CompanyName,
                              BusinessUnitName = bu.UnitName,
                              LegalEntityName = lgl.LegalEntityName
                          }).ToListAsync();
        }

        public async Task<List<AccountPayable>> GetAPList(string status, string businessUnit, string legalEntity, string accountPayableNumber)
        {
            return await (from inv in context.InvoiceReceivables
                          join client in context.Companys on inv.ClientId equals client.Id
                          join bu in context.BusinessUnits on inv.BusinessUnitId equals bu.Id
                          join lgl in context.LegalEntity on inv.LegalEntityId equals lgl.Id
                          join ap in context.AccountPayables on inv.Id equals ap.OurRefId
                          join po in context.PurchaseOrders on ap.PurchaseOrderId equals po.Id
                          where !(inv.IsDeleted ?? false) &&
                          ((status ?? "0") == "0" ? true : inv.InvoiceStatusId == status) &&
                          ((businessUnit ?? "0") == "0" ? true : ap.BusinessUnitId == businessUnit) &&
                          ((legalEntity ?? "0") == "0" ? true : ap.LegalEntityId == legalEntity) &&
                          ((accountPayableNumber ?? "0") == "0" ? true : ap.AccountPayableNumber == accountPayableNumber)
                          select new AccountPayable
                          {
                              Id = inv.Id,
                              InvoiceReceivableId = ap.InvoiceReceivableId,
                              AccountPayableNumber = ap.AccountPayableNumber,
                              TransactionDate = ap.TransactionDate,
                              InvoiceVendorNumber = ap.InvoiceVendorNumber,
                              Status = inv.InvoiceStatusId == null ? "1" : inv.InvoiceStatusId,
                              ClientName = client.CompanyName,
                              BusinessUnitName = bu.UnitName,
                              LegalEntityName = lgl.LegalEntityName,
                              PurchaseOrderId = po.PurchaseOrderNumber
                          }).ToListAsync();
        }

        public async Task<List<InvoiceReceivableDetail>> GetDetailInvoiceList(string poId, string payment, string invId)
        {
            var list = new List<InvoiceReceivableDetail>();
            var isAdd = false;
            if (invId == null) isAdd = true; else isAdd = context.InvoiceReceivableDetails.Where(x => !(x.IsDeleted ?? false) && x.InvoiceReceivableId == invId).Count() == 0;
            if (isAdd)
            {
                var paymentTo = int.Parse(payment.Split("of").FirstOrDefault().Trim());
                list = await (from a in context.PurchaseOrderDetails
                              join b in context.Uom on a.UomId equals b.Id
                              join c in context.ItemType on a.ItemTypeId equals c.Id
                              join d in context.PurchaseOrders on a.PurchaseOrderId equals d.Id
                              where a.PurchaseOrderId == poId && !(a.IsDeleted ?? false)
                              select new InvoiceReceivableDetail
                              {
                                  Id = a.Id,
                                  TaskId = a.TaskId,
                                  Task = a.Task,
                                  ItemTypeId = a.ItemTypeId,
                                  ItemId = a.ItemId,
                                  ItemCode = a.ItemCode,
                                  ItemName = a.ItemName,
                                  UomId = a.UomId,
                                  Qty = a.Qty,
                                  UnitPrice = a.UnitPrice,
                                  Amount = a.Amount,
                                  PaymentTo = paymentTo,
                                  InvoiceValue = paymentTo == 1 ? a.Amount * (d.PercentPaymentTerm1 / 100) :
                                               paymentTo == 2 ? a.Amount * (d.PercentPaymentTerm2 / 100) :
                                               paymentTo == 3 ? a.Amount * (d.PercentPaymentTerm3 / 100) : 0,
                                  AmountPaid = paymentTo == 2 ? (a.Amount * (d.PercentPaymentTerm1 / 100)) :
                                               paymentTo == 3 ? (a.Amount * (d.PercentPaymentTerm1 / 100)) +
                                                                (a.Amount * (d.PercentPaymentTerm2 / 100)) : 0, //yang sudah di bayar / di proses
                                  Balance = paymentTo == 1 ? a.Amount * (d.PercentPaymentTerm1 / 100) :
                                               paymentTo == 2 ? (a.Amount * (d.PercentPaymentTerm1 / 100)) +
                                                                (a.Amount * (d.PercentPaymentTerm2 / 100)) :
                                               paymentTo == 3 ? (a.Amount * (d.PercentPaymentTerm1 / 100)) +
                                                                (a.Amount * (d.PercentPaymentTerm2 / 100)) +
                                                                (a.Amount * (d.PercentPaymentTerm3 / 100)) : 0,  //total yg sudah di bayarkan dan juga yg baru di buatkan invoice.
                                  ItemTypeName = c.TypeName,
                                  UomName = b.unit_symbol
                              }).ToListAsync();
            }
            else
            {
                var model = await context.InvoiceReceivables.Where(x => x.Id == invId).FirstOrDefaultAsync();
                if (model != null)
                {
                    var isAccount = false;
                    if (model.MainServiceCategoryId == MainServiceCategoryOther.MainServiceCategoryId) { isAccount = context.InvoiceReceivableDetails.Where(x => x.InvoiceReceivableId == invId && !string.IsNullOrEmpty(x.AccountId)).Count() > 0; }

                    if (isAccount)
                    {
                        list = await (from a in context.InvoiceReceivableDetails
                                      join b in context.Uom on a.UomId equals b.Id
                                      join c in context.ItemType on a.ItemTypeId equals c.Id
                                      join d in context.ChartOfAccounts on a.AccountId equals d.Id
                                      where a.InvoiceReceivableId == invId && !(a.IsDeleted ?? false)
                                      select new InvoiceReceivableDetail
                                      {
                                          Id = a.Id,
                                          InvoiceReceivableId = a.InvoiceReceivableId,
                                          TaskId = a.TaskId,
                                          Task = a.Task,
                                          ItemTypeId = a.ItemTypeId,
                                          ItemId = a.ItemId,
                                          ItemCode = a.ItemCode,
                                          ItemName = a.ItemName,
                                          UomId = a.UomId,
                                          Qty = a.Qty,
                                          UnitPrice = a.UnitPrice,
                                          Amount = a.Amount,
                                          PaymentTo = a.PaymentTo,
                                          InvoiceValue = a.InvoiceValue,
                                          AmountPaid = a.AmountPaid,
                                          Balance = a.Balance,
                                          AccountId = a.AccountId,
                                          AccountName = d.Name5,
                                          CodeRec = d.CodeRec,
                                          ItemTypeName = c.TypeName,
                                          UomName = b.unit_symbol,
                                          Pph = a.Pph,
                                          PphAmount = a.PphAmount
                                      }).ToListAsync();
                    }
                    else
                    {
                        list = await (from a in context.InvoiceReceivableDetails
                                      join b in context.Uom on a.UomId equals b.Id
                                      join c in context.ItemType on a.ItemTypeId equals c.Id
                                      where a.InvoiceReceivableId == invId && !(a.IsDeleted ?? false)
                                      select new InvoiceReceivableDetail
                                      {
                                          Id = a.Id,
                                          InvoiceReceivableId = a.InvoiceReceivableId,
                                          TaskId = a.TaskId,
                                          Task = a.Task,
                                          ItemTypeId = a.ItemTypeId,
                                          ItemId = a.ItemId,
                                          ItemCode = a.ItemCode,
                                          ItemName = a.ItemName,
                                          UomId = a.UomId,
                                          Qty = a.Qty,
                                          UnitPrice = a.UnitPrice,
                                          Amount = a.Amount,
                                          PaymentTo = a.PaymentTo,
                                          InvoiceValue = a.InvoiceValue,
                                          AmountPaid = a.AmountPaid,
                                          Balance = a.Balance,
                                          AccountId = a.AccountId,
                                          ItemTypeName = c.TypeName,
                                          UomName = b.unit_symbol,
                                          Pph = a.Pph,
                                          PphAmount = a.PphAmount
                                      }).ToListAsync();
                    }
                }
            }
            return list;
        }


        public async Task<List<Journal>> GetJournal(string invId)
        {
            return await (from tb in context.JournalTemps
                          join tap in context.TemporaryAccountPayables on tb.ReferenceId equals tap.Id
                          join coa in context.ChartOfAccounts on tb.AccountId equals coa.Id
                          join bu in context.BusinessUnits on tb.DivisiId equals bu.Id
                          where tap.InvoiceReceivableId == invId
                          select new Journal
                          {
                              AccountId = coa.Id,
                              AccountCode = coa.CodeRec,
                              AccountName = coa.Name5,
                              JobId = tb.JobId,
                              Description = tb.Description == null ? coa.Name5 : tb.Description,
                              JobIdName = tap.JobName,
                              DivisiId = tb.DivisiId,
                              DivisiName = bu.UnitName,
                              Credit = tb.Credit,
                              Debit = tb.Debit,
                              LineCode = tb.LineCode,
                          }).OrderByDescending(x => x.Debit).ThenByDescending(x => x.Credit).ToListAsync();
        }

        public async Task<int> AddAsync(InvoiceReceivable entity)
        {
            return await AddEdit(entity, false, false);
        }

        public async Task<int> EditAsync(InvoiceReceivable entity)
        {
            return await AddEdit(entity, true, false);
        }

        public async Task<int> CreateJournalAsync(InvoiceReceivable entity)
        {
            return await AddEdit(entity, true, true);
        }

        private async Task<int> AddEdit(InvoiceReceivable model, bool isEdit, bool isJournal)
        {
            await financePeriod.IsClosePeriod(Convert.ToDateTime(model.DueDate), model.LegalEntityId, Models.Enum.FinancePeriodCode.AP);
            var entity = await ProcessUpload(model);
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    if (isEdit)
                    {
                        context.PhoenixEdit(entity);
                        await context.SaveChangesAsync();
                    }
                    else
                    {
                        var invNumber = await TransID.GetTransId(context, Code.InvoiceReceived, DateTime.Today);
                        entity.InvoiceStatusId = InvoiceReceivedStatus.Open;
                        entity.Id = invNumber;
                        entity.InvoiceNumber = invNumber;
                        await context.PhoenixAddAsync(entity);
                        await context.SaveChangesAsync();
                    }

                    if (entity.InvoiceReceivableDetails != null)
                    {
                        if (!string.IsNullOrEmpty(entity.Id))
                        {
                            var detail = context.InvoiceReceivableDetails.AsNoTracking().Where(x => !(x.IsDeleted ?? false) && x.InvoiceReceivableId == entity.Id).ToList();
                            if (detail.Count > 0)
                            {
                                context.InvoiceReceivableDetails.RemoveRange(detail);
                            }
                        }

                        Parallel.ForEach(entity.InvoiceReceivableDetails, (item) =>
                        {
                            item.Id = Guid.NewGuid().ToString();
                            item.InvoiceReceivableId = entity.Id;
                            context.PhoenixAddAsync(item);
                        });
                    }

                    var tap = await context.TemporaryAccountPayables.AsNoTracking().Where(x => !(x.IsDeleted ?? false) && x.InvoiceReceivableId == entity.Id).FirstOrDefaultAsync();
                    //update status TAP jika data tap sudah ada.
                    if (!isJournal && tap != null)
                    {
                        tap.Status = entity.InvoiceStatusId;
                        context.PhoenixEdit(tap);
                    }

                    if (isJournal)
                    {
                        //sumber crud TAP = EXECUTE fn.prc_push_ap_temp;
                        string TAPNumber = "";
                        if (tap == null) TAPNumber = await TransID.GetTransId(context, Code.TAP, DateTime.Today); else TAPNumber = tap.TapNumber;

                        var tmp = await context.VwjournalAp.AsNoTracking()
                            .Where(x => x.InvoiceReceivableId == entity.Id)
                            .Select(x => new TemporaryAccountPayable
                            {
                                Id = TAPNumber,
                                TapNumber = TAPNumber,
                                InvoiceReceivableId = x.InvoiceReceivableId,
                                LegalEntityId = x.LegalEntityId,
                                BusinessUnitId = x.BusinessUnitId,
                                AffiliationId = x.AffiliationId,
                                AccountId = x.CoaId,
                                VendorId = x.VendorId,
                                JobId = x.JobId,
                                TransactionDate = DateTime.Now,
                                CurrencyId = x.CurrencyId,
                                Amount = x.Credit ?? 0,
                                ExchangeRate = x.ExchangeRate ?? 0,
                                BankAccountDestination = null,
                                Charges = null,
                                Remarks = null,
                                IsJournal = "N",
                                DK = "K",
                                Status = x.InvoiceStatusId,
                                MainServiceCategoryId = x.MainserviceCategoryId,
                                CompanyBrandId = x.CompanyBrandId,
                                TaxNumber = x.TaxNumber,
                                Periode = DateTime.Now,
                                TaxDate = x.TaxDate,
                                InvoiceVendorNumber = x.InvoiceVendor,
                                APType = "1",
                                AllocationRatioId = x.AllocationRatioId,
                                InvoiceReceivedDate = x.InvoiceDate,
                                OurRef = x.InvoiceNumber,
                                OurRefId = x.InvoiceReceivableId,
                                DueDays = 3,
                                PurchaseOrderId = x.PurchaseOrderId,
                                LineCode = x.LineCode
                            }).FirstOrDefaultAsync();

                        if (tmp != null)
                        {
                            tmp.JobName = await context.JobDetail.Where(x => x.Id == tmp.JobId).Select(x => x.JobName).FirstOrDefaultAsync();

                            if (tap == null)
                            {
                                await context.PhoenixAddAsync(tmp);
                                await context.SaveChangesAsync();
                            }
                            else
                            {
                                context.PhoenixEdit(tmp);
                                await context.SaveChangesAsync();

                                var detail = context.TemporaryAccountPayableDetail.AsNoTracking().Where(x => x.AccountPayableTemporaryId == tap.Id).ToList();
                                if (detail.Count > 0) context.TemporaryAccountPayableDetail.RemoveRange(detail);
                            }

                            var tmpDetail = await (from a in context.VwJournalApDetail
                                                   where a.InvoiceReceivableId == entity.Id &&
                                                   ((a.Debit ?? 0) != 0 || (a.Credit ?? 0) != 0)
                                                   select new TemporaryAccountPayableDetail
                                                   {
                                                       Id = Guid.NewGuid().ToString(),
                                                       AccountPayableTemporaryId = tmp.Id,
                                                       EvidentNumber = tmp.Id,
                                                       PayableBalance = null,
                                                       AmountPaid = a.DK.ToLower() == "d" ? a.Debit : a.Credit,
                                                       Discount = null,
                                                       Tax = null,
                                                       EndingBalance = null,
                                                       RowIndex = null,
                                                       ItemName = a.ItemName,
                                                       ItemDescription = a.Name5,
                                                       Qty = 1,
                                                       UnitPrice = (a.DK.ToLower() == "d" ? a.Debit : a.Credit) ?? 0,
                                                       Amount = (a.DK.ToLower() == "d" ? a.Debit : a.Credit) ?? 0,
                                                       UomId = a.UomId,
                                                       ItemTypeId = a.ItemTypeId,
                                                       DK = a.DK,
                                                       AccountId = a.CoaId,
                                                       LineCode = a.LineCode,
                                                       TaxPayCoaId = a.TaxPayCoaId,
                                                       TaxPayablePct = a.TaxPayablePct
                                                   }).ToListAsync();
                            await context.PhoenixAddRangeAsync(tmpDetail.ToArray());
                            await context.Database.ExecuteSqlCommandAsync("EXECUTE fn.create_jurnal_temp_ap2 @p_account_payable_temp_id", new SqlParameter("@p_account_payable_temp_id", TAPNumber));
                        }
                    }

                    if (entity.InvoiceStatusId == InvoiceReceivedStatus.Posted)
                    {
                        var APNumber = await TransID.GetTransId(context, Code.AP, DateTime.Today);
                        var ap = await context.TemporaryAccountPayables.AsNoTracking()
                            .Where(x => !(x.IsDeleted ?? false) && x.InvoiceReceivableId == entity.Id)
                            .Select(x => new AccountPayable
                            {
                                Id = APNumber,
                                AccountPayableNumber = APNumber,
                                LegalEntityId = x.LegalEntityId,
                                BusinessUnitId = x.BusinessUnitId,
                                AffiliationId = x.AffiliationId,
                                AccountId = x.AccountId,
                                VendorId = x.VendorId,
                                JobId = x.JobId,
                                TransactionDate = x.TransactionDate,
                                CurrencyId = x.CurrencyId,
                                Amount = x.Amount,
                                ExchangeRate = x.ExchangeRate,
                                BankAccountDestination = x.BankAccountDestination,
                                Charges = x.Charges,
                                Remarks = x.Remarks,
                                InvoiceReceivableId = x.InvoiceReceivableId,
                                IsJournal = x.IsJournal,
                                DK = x.DK,
                                Status = entity.InvoiceStatusId,
                                MainserviceCategoryId = x.MainServiceCategoryId,
                                CompanyBrandId = x.CompanyBrandId,
                                TaxNumber = x.TaxNumber,
                                Periode = x.Periode,
                                TaxDate = x.TaxDate,
                                InvoiceVendorNumber = x.InvoiceVendorNumber,
                                //YourRef = x.YourRef,
                                AllocationRatioId = x.AllocationRatioId,
                                InvoiceReceivedDate = x.InvoiceReceivedDate,
                                JobName = x.JobName,
                                OurRef = x.OurRef,
                                OurRefId = x.OurRefId,
                                DueDays = x.DueDays,
                                PurchaseOrderId = x.PurchaseOrderId,
                                LineCode = x.LineCode,
                                TemporaryAccountPayableId = x.Id,
                                IsPayment = false
                            }).FirstOrDefaultAsync();
                        await context.PhoenixAddAsync(ap);
                        await context.SaveChangesAsync();

                        var apDetail = await (from a in context.TemporaryAccountPayableDetail
                                              join b in context.TemporaryAccountPayables on a.AccountPayableTemporaryId equals b.Id
                                              where !(b.IsDeleted ?? false) && b.InvoiceReceivableId == entity.Id
                                              select new AccountPayableDetail
                                              {
                                                  Id = Guid.NewGuid().ToString(),
                                                  AccountPayableId = ap.Id,
                                                  EvidentNumber = ap.Id,
                                                  PayableBalance = a.PayableBalance,
                                                  AmountPaid = a.AmountPaid,
                                                  Discount = a.Discount,
                                                  Tax = a.Tax,
                                                  EndingBalance = a.EndingBalance,
                                                  RowIndex = a.RowIndex,
                                                  ItemName = a.ItemName,
                                                  ItemDescription = a.ItemDescription,
                                                  Qty = a.Qty,
                                                  UnitPrice = a.UnitPrice,
                                                  Amount = a.Amount,
                                                  UomId = a.UomId,
                                                  ItemTypeId = a.ItemTypeId,
                                                  DK = a.DK,
                                                  AccountId = a.AccountId,
                                                  LineCode = a.LineCode,
                                                  TaxPayCoaId = a.TaxPayCoaId,
                                                  TaxPayablePct = a.TaxPayablePct
                                              }).ToListAsync();

                        await context.PhoenixAddRangeAsync(apDetail.ToArray());
                        await context.Database.ExecuteSqlCommandAsync("EXECUTE fn.create_jurnal_ap @p_account_payable_id", new SqlParameter("@p_account_payable_id", APNumber));
                    }
                    if (!isJournal) await log.AddAsync(new StatusLog() { TransactionId = entity.Id, Status = InvoiceReceivedStatus.StatusName(entity.InvoiceStatusId), Description = entity.Description });
                    var save = await context.SaveChangesAsync();
                    transaction.Commit();
                    return save;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        private async Task<InvoiceReceivable> ProcessUpload(InvoiceReceivable entity)
        {
            var filePoObject = await uploadFile.Upload(entity.filePoObject, null);
            if (!string.IsNullOrEmpty(filePoObject))
            {
                entity.PoObject = filePoObject;
            }

            var fileTaxObject = await uploadFile.Upload(entity.fileTaxObject, null);
            if (!string.IsNullOrEmpty(fileTaxObject))
            {
                entity.TaxObject = fileTaxObject;
            }

            var fileContractObject = await uploadFile.Upload(entity.fileContractObject, null);
            if (!string.IsNullOrEmpty(fileContractObject))
            {
                entity.ContractObject = fileContractObject;
            }

            var fileDoObject = await uploadFile.Upload(entity.fileDoObject, null);
            if (!string.IsNullOrEmpty(fileDoObject))
            {
                entity.DoObject = fileDoObject;
            }

            var fileGrObject = await uploadFile.Upload(entity.fileGrObject, null);
            if (!string.IsNullOrEmpty(fileGrObject))
            {
                entity.GrObject = fileGrObject;
            }

            var fileInvObject = await uploadFile.Upload(entity.fileInvObject, null);
            if (!string.IsNullOrEmpty(fileInvObject))
            {
                entity.InvObject = fileInvObject;
            }
            return entity;
        }

        private async Task<InvoiceReceivable> GetFile(InvoiceReceivable data)
        {
            if (data.PoObject != null)
            {
                var PoObject = await context.Filemasters.Where(x => x.Id == data.PoObject).FirstOrDefaultAsync();
                if (PoObject != null) data.filePoObjectName = PoObject.Name;
            }

            if (data.TaxObject != null)
            {
                var TaxObject = await context.Filemasters.Where(x => x.Id == data.TaxObject).FirstOrDefaultAsync();
                if (TaxObject != null) data.fileTaxObjectName = TaxObject.Name;
            }

            if (data.ContractObject != null)
            {
                var ContractObject = await context.Filemasters.Where(x => x.Id == data.ContractObject).FirstOrDefaultAsync();
                if (ContractObject != null) data.fileContractObjectName = ContractObject.Name;
            }

            if (data.DoObject != null)
            {
                var DoObject = await context.Filemasters.Where(x => x.Id == data.DoObject).FirstOrDefaultAsync();
                if (DoObject != null) data.fileDoObjectName = DoObject.Name;
            }

            if (data.GrObject != null)
            {
                var GrObject = await context.Filemasters.Where(x => x.Id == data.GrObject).FirstOrDefaultAsync();
                if (GrObject != null) data.fileGrObjectName = GrObject.Name;
            }

            if (data.InvObject != null)
            {
                var InvObject = await context.Filemasters.Where(x => x.Id == data.InvObject).FirstOrDefaultAsync();
                if (InvObject != null) data.fileInvObjectName = InvObject.Name;
            }
            return data;
        }
    }
}