﻿using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Data.Services.Finance.Transaction
{
    public interface IFixedAssetService : IDataService<FixedAsset>
    {
        Task<int> AddWithDetailAsync(FixedAsset Header, List<FixedAssetMovement> Details);
        Task<int> EditWithDetailAsync(FixedAsset Header, List<FixedAssetMovement> Details);
        Task<int> EditListHeaderAsync(List<FixedAsset> model);
        Task<Affiliation> GetAffiliation(string buId);
        Task<int> EditCostAsync(FixedAsset model);
    }
    public class FixedAssetService : IFixedAssetService
    {
        readonly DataContext context;

        public FixedAssetService(DataContext context)
        {
            this.context = context;
        }

        public async Task<int> AddAsync(FixedAsset entity)
        {
            entity.Id = Guid.NewGuid().ToString();
            entity.FixedAssetNumber = await TransID.GetTransId(context, "FX", DateTime.Today);
            await context.PhoenixAddAsync(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> AddRangeAsync(params FixedAsset[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> AddWithDetailAsync(FixedAsset Header, List<FixedAssetMovement> Details)
        {
            Header.Id = Guid.NewGuid().ToString();
            Header.FixedAssetNumber = await TransID.GetTransId(context, "FX", DateTime.Today);
            await context.PhoenixAddAsync(Header);

            if (Details.Count > 0)
            {
                List<FixedAssetMovement> fam = new List<FixedAssetMovement>();
                foreach (FixedAssetMovement data in Details)
                {
                    data.Id = Guid.NewGuid().ToString();
                    data.FixedAssetId = Header.Id;
                    data.FaaNumber = await TransID.GetTransId(context, "FAM", DateTime.Today);
                    fam.Add(data);
                }

                FixedAssetMovement[] igrdArray = fam.ToArray();
                await context.PhoenixAddRangeAsync(igrdArray);
            }

            return await context.SaveChangesAsync();
        }

        public Task<int> DeleteAsync(FixedAsset entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteRageAsync(params FixedAsset[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteSoftAsync(FixedAsset entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteSoftRangeAsync(params FixedAsset[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> EditAsync(FixedAsset entity)
        {
            context.PhoenixEdit(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> EditCostAsync(FixedAsset model)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    context.PhoenixEdit(model);
                    var save = await context.SaveChangesAsync();
                    transaction.Commit();
                    return save;
                }
                catch(Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                
            }
        }

        public async Task<int> EditListHeaderAsync(List<FixedAsset> model)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    if (model.Count > 0)
                    {
                        List<FixedAsset> fa = new List<FixedAsset>();
                        foreach (FixedAsset data in model)
                        {   
                            fa.Add(data);
                        }

                        FixedAsset[] faArray = fa.ToArray();
                        context.PhoenixEditRange(faArray);
                    }
                    var save = await context.SaveChangesAsync();
                    transaction.Commit();
                    return save;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        public Task<int> EditRangeAsync(FixedAsset entity)
        {
            throw new NotImplementedException();
        }

        public async Task<int> EditWithDetailAsync(FixedAsset Header, List<FixedAssetMovement> Details)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    context.PhoenixEdit(Header);

                    if (Details.Count > 0)
                    {
                        List<FixedAssetMovement> famtemp = new List<FixedAssetMovement>();
                        var checkAvailable = await context.FixedAssetMovement.Where(x => x.FixedAssetId == Header.Id && x.IsDeleted.Equals(false)).ToListAsync();
                        foreach (FixedAssetMovement data in checkAvailable)
                        {
                            famtemp.Add(data);
                        }

                        FixedAssetMovement[] famArraytemp = famtemp.ToArray();
                        context.PhoenixDeleteRange(famArraytemp);

                        List<FixedAssetMovement> famnew = new List<FixedAssetMovement>();
                        foreach (FixedAssetMovement data in Details)
                        {
                            data.Id = Guid.NewGuid().ToString();
                            data.FixedAssetId = Header.Id;
                            //data.FaaNumber = await TransID.GetTransId(context, Code.FixedAssetMovement, DateTime.Today);
                            famnew.Add(data);
                        }

                        FixedAssetMovement[] famArraynew = famnew.ToArray();
                        context.PhoenixAddRange(famArraynew);
                    }
                    var save = await context.SaveChangesAsync();
                    transaction.Commit();
                    return save;
                }
                catch(Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
            
            
        }

        public async Task<List<FixedAsset>> Get()
        {
            var query = await (from fa in context.FixedAsset
                               join fam in (from f in context.FixedAssetMovement orderby f.FaaDateTime descending select f).Take(1) on fa.Id equals fam.FixedAssetId into left
                               from fam in left.DefaultIfEmpty()
                               join loc in context.MasterLocationProcurement on fam.FromLocationID equals loc.Id into famloc
                               from loc in famloc.DefaultIfEmpty()
                               join con in context.MasterConditionProcurement on fam.Condition equals con.Id into famcon
                               from con in famcon.DefaultIfEmpty()
                               join emp in context.EmployeeBasicInfos on fam.Employee equals emp.Id into famemp
                               from emp in famemp.DefaultIfEmpty()
                               join bu in context.BusinessUnits on fam.BusinessUnitID equals bu.Id into fambu
                               from bu in fambu.DefaultIfEmpty()
                               join le in context.LegalEntity on fam.LegalEntityID equals le.Id into famle
                               from le in famle.DefaultIfEmpty()
                               select new FixedAsset
                               {
                                   Id = fa.Id,
                                   FixedAssetName = fa.FixedAssetName,
                                   FixedAssetNumber=fa.FixedAssetNumber,
                                   Brand = fa.Brand,
                                   SkuCode = fa.SkuCode,
                                   UnitCount = fa.UnitCount,
                                   CurrentLocation = loc.LocationStore,
                                   Condition = con.ConditionName,
                                   OwnerId = emp.NameEmployee,
                                   BusinessUnit = bu.UnitName,
                                   LegalEntity = le.LegalEntityName
                               }).ToListAsync();
            //var query = await context.FixedAsset.ToListAsync();
            return query;
        }

        public Task<List<FixedAsset>> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public async Task<FixedAsset> Get(string Id)
        {
            var query = await (from fa in context.FixedAsset
                               join fam in (from f in context.FixedAssetMovement orderby f.FaaDateTime descending select f).Take(1) on fa.Id equals fam.FixedAssetId into left
                               from fam in left.DefaultIfEmpty()
                               join cond in context.MasterConditionProcurement on fam.Condition equals cond.Id into facond
                               from cond in facond.DefaultIfEmpty()
                               join loca in context.MasterLocationProcurement on fam.FromLocationID equals loca.Id into faloca
                               from loca in faloca.DefaultIfEmpty()
                               where fa.Id == Id && fa.IsDeleted.Equals(false)
                               select new FixedAsset
                               {
                                   Id = fa.Id,
                                   Manufacture = fa.Manufacture,
                                   Brand = fa.Brand,
                                   Model = fa.Model,
                                   ProductNumber = fa.ProductNumber,
                                   Color = fa.Color,
                                   Condition = fam.Condition,
                                   ConditionName = cond.ConditionName,
                                   CurrentLocation = fam.FromLocationID,
                                   LocationName = loca.LocationStore,
                                   Qty = fa.Qty,
                                   Uom = fa.Uom,
                                   UnitCount = fa.UnitCount,
                                   BusinessUnit = fa.BusinessUnit,
                                   LegalEntity = fa.LegalEntity,
                                   GrossValue = fa.GrossValue,
                                   RegistrationDate=fa.RegistrationDate,
                                   Residue = fa.Residue,
                                   Lifetime = fa.Lifetime,
                                   AcquisitionCost = fa.AcquisitionCost,
                                   Affiliation = fa.Affiliation,
                                   CoaIdAkumulasi = fa.CoaIdAkumulasi,
                                   CoaIdDepresiai = fa.CoaIdDepresiai,
                                   CommercialDepreciationId = fa.CommercialDepreciationId,
                                   CurrentPrice = fa.CurrentPrice,
                                   FixedAssetName = fa.FixedAssetName,
                                   GoodReceiptId = fa.GoodReceiptId,
                                   GoodReceiptDetailId = fa.GoodReceiptDetailId,
                                   CurrencyId = fa.CurrencyId,
                                   EconomicPeriod = fa.EconomicPeriod,
                                   FixedAssetNumber = fa.FixedAssetNumber,
                                   Rounding = fa.Rounding,
                                   SkuCode = fa.SkuCode,
                                   SerialNumber = fa.SerialNumber,
                                   PurchaseOrderId = fa.PurchaseOrderId,
                                   Status = fa.Status,
                                   Description = fa.Description,
                                   ApprovedBy = fa.ApprovedBy,
                                   ApprovedOn = fa.ApprovedOn,
                                   ExchangeRate = fa.ExchangeRate,
                                   ItemId = fa.ItemId,
                                   StatusDepreciation = fa.StatusDepreciation
                               }
                               ).FirstOrDefaultAsync();
            //var query = await context.FixedAsset.Where(x=>x.IsDeleted.Equals(false) && x.Id==Id).FirstOrDefaultAsync();
            return query;
        }

        public async Task<Affiliation> GetAffiliation(string buId)
        {
            var query = await context.BusinessUnits.Where(x => x.Id == buId).FirstOrDefaultAsync();
            var result = await context.Affiliations.Where(x => x.Id == query.AffiliationId).FirstOrDefaultAsync();
            return result;
        }
    }
}
