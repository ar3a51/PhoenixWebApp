﻿using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Data
{
    public interface IGeneralLedgerDetailService
    {
        Task<List<GeneralLedgerDetail>> GetList(GeneralLedgerDetailSearch model);
    }

    public class GeneralLedgerDetailService : IGeneralLedgerDetailService
    {
        readonly DataContext context;
        public GeneralLedgerDetailService(DataContext context)
        {
            this.context = context;
        }

        public async Task<List<GeneralLedgerDetail>> GetList(GeneralLedgerDetailSearch model)
        {
            return await context.GeneralLedgerDetails
                .Where(x =>
                        (string.IsNullOrEmpty(model.LegalEntityId) ? true : x.LegalEntityId == model.LegalEntityId) &&
                        ((model.PeriodeFrom == null) ? true : x.PeriodeFrom == model.PeriodeFrom) &&
                        ((model.PeriodeTo == null) ? true : x.PeriodeTo == model.PeriodeTo) &&
                        (string.IsNullOrEmpty(model.EvdNo) ? true : x.EvdNo == model.EvdNo) &&
                        (string.IsNullOrEmpty(model.CurrencyId) ? true : x.CurrencyId == model.CurrencyId) &&
                        ((model.ExchangeRate == null) ? true : x.ExchangeRate == model.ExchangeRate) &&
                        (string.IsNullOrEmpty(model.Description) ? true : x.Description == model.Description) &&
                        ((model.Adjustment == null) ? true : x.Adjustment == model.Adjustment)
                ).ToListAsync();
        }
    }
}
