﻿using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Attributes;
using Phoenix.Data.Models.Finance;
using Phoenix.Data.Models;
using Phoenix.Data.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.Data.Services.Finance.Transaction
{
    public interface IGoodRequestService : IDataService<GoodRequest>
    {
        Task<int> AddWithDetailAsync(GoodRequest Header, List<GoodRequestDetail> Details);
        Task<int> EditWithDetailAsync(GoodRequest Header, List<GoodRequestDetail> Details);
        Task<GoodRequestDetail> GetDetail(string Id);
        Task<List<GoodRequestDetail>> GetListDetail(string headerId);
        Task<GoodRequest> Approve(GoodRequest model);
        Task<GoodRequest> Complete(GoodRequest model);
        Task<GoodRequest> GetByNumber(string number);
    }

    public class GoodRequestService : IGoodRequestService
    {
        readonly DataContext context;
        readonly GlobalFunctionApproval globalFcApproval;
        string idTemplate = "";

        public GoodRequestService(DataContext context, GlobalFunctionApproval globalFcApproval)
        {
            this.context = context;
            this.globalFcApproval = globalFcApproval;
        }

        public Task<int> AddAsync(GoodRequest entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> AddRangeAsync(params GoodRequest[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> AddWithDetailAsync(GoodRequest Header, List<GoodRequestDetail> Details)
        {
            //using (var transaction = await context.Database.BeginTransactionAsync())
            //{
                Header.Id = Guid.NewGuid().ToString();
                Header.GoodRequestNumber = await TransID.GetTransId(context, "GREQ", DateTime.Today);
            //await globalFcApproval.UnsafeSubmitApproval<GoodRequest>(false, Header.Id, int.Parse(2), $"{ApprovalLink.GoodRequest}?Id={Header.Id}&isApprove=true", idTemplate, MenuUnique.GoodRequest, Header);
            //Header.OrganizationId = "9EE4834225E334380667DDCF2F60F6DA";
            //await context.PhoenixAddAsync(Header);
            await globalFcApproval.UnsafeSubmitApproval<GoodRequest>(false, Header.Id, int.Parse(Header.Status), $"{ApprovalLink.GoodRequest}?Id={Header.Id}&isApprove=true", idTemplate, MenuUnique.GoodRequest, Header);
            if (Details.Count > 0)
                {
                    List<GoodRequestDetail> igrd = new List<GoodRequestDetail>();
                    foreach (GoodRequestDetail data in Details)
                    {
                        data.Id = Guid.NewGuid().ToString();
                        data.GoodRequestId = Header.Id;
                        igrd.Add(data);
                    }

                    GoodRequestDetail[] igrdArray = igrd.ToArray();
                    await context.PhoenixAddRangeAsync(igrdArray);
                }

                return await context.SaveChangesAsync();
            //}
        }

        public Task<int> DeleteAsync(GoodRequest entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteRageAsync(params GoodRequest[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteSoftAsync(GoodRequest entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteSoftRangeAsync(params GoodRequest[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> EditAsync(GoodRequest entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> EditRangeAsync(GoodRequest entity)
        {
            throw new NotImplementedException();
        }

        public async Task<int> EditWithDetailAsync(GoodRequest Header, List<GoodRequestDetail> Details)
        {   
            //Header.OrganizationId = "9EE4834225E334380667DDCF2F60F6DA";
            context.PhoenixEdit(Header);

            if (Details.Count > 0)
            {
                List<GoodRequestDetail> igrd = new List<GoodRequestDetail>();
                foreach (GoodRequestDetail data in Details)
                {
                    //data.Id = Guid.NewGuid().ToString();
                    data.GoodRequestId = Header.Id;
                    igrd.Add(data);
                }

                GoodRequestDetail[] igrdArray = igrd.ToArray();
                context.PhoenixEditRange(igrdArray);
            }
            //else
            //{
            //    List<GoodRequestDetail> srdtemp = new List<GoodRequestDetail>();
            //    var checkAvailable = await context.GoodRequestDetail.Where(x => x.GoodRequestId == Header.Id && x.IsDeleted.Equals(false)).ToListAsync();
            //    foreach (GoodRequestDetail data in checkAvailable)
            //    {
            //        srdtemp.Add(data);
            //    }

            //    GoodRequestDetail[] srdArraytemp = srdtemp.ToArray();
            //    context.PhoenixDeleteRange(srdArraytemp);
            //}

            return await context.SaveChangesAsync();
        }

        public async Task<List<GoodRequest>> Get()
        {
            var query = await (from gr in context.GoodRequest
                               join grd in (from g in context.GoodRequestDetail orderby g.ModifiedOn descending select g).Take(1) on gr.Id equals grd.GoodRequestId into left
                               from grd in left.DefaultIfEmpty()
                               select new GoodRequest
                               {
                                   Id = gr.Id,
                                   GoodRequestNumber = gr.GoodRequestNumber,
                                   GoodRequestDatetime = gr.GoodRequestDatetime,
                                   Type=gr.Type,
                                   DUeDate = gr.DUeDate,
                                   InvFaId = gr.InvFaId,
                                   InvFaName = gr.InvFaName,
                                   JobId = gr.JobId,
                                   JobName = gr.JobName,
                                   BusinessUnitId = gr.BusinessUnitId,
                                   BusinessUnitName = gr.BusinessUnitName,
                                   LegalEntityId = gr.LegalEntityId,
                                   LegalEntityName = gr.LegalEntityName,
                                   EmployeeName = gr.EmployeeName,
                                   Status = gr.Status,
                                   LocationTransfer = grd.WarehouseIdTo,
                                   Purpose = grd.Purpose,
                                   ReturnDate=gr.ReturnDate
                               }
                               ).ToListAsync();
            return query;
            //return await context.GoodRequest.Where(x => x.IsDeleted.Equals(false)).ToListAsync();
        }

        public Task<List<GoodRequest>> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public async Task<GoodRequest> Get(string Id)
        {
            return await context.GoodRequest.Where(x => x.IsDeleted.Equals(false) && x.Id==Id).FirstOrDefaultAsync();
        }

        public async Task<List<GoodRequestDetail>> GetListDetail(string headerId)
        {
            return await context.GoodRequestDetail.Where(x => x.IsDeleted.Equals(false) && x.GoodRequestId==headerId).ToListAsync();
        }

        public async Task<GoodRequestDetail> GetDetail(string Id)
        {
            return await context.GoodRequestDetail.Where(x => x.IsDeleted.Equals(false) && x.Id == Id).FirstOrDefaultAsync();
        }

        public async Task<GoodRequest> Approve(GoodRequest model)
        {
            try
            {   
                model.Status = GoodRequestStatus.Approved.ToString();
                await globalFcApproval.ProcessApproval<GoodRequest>(model.Id, Int32.Parse(model.Status), model.Remark, model);

                var modeltrapp = await context.TrTemplateApprovals.Where(x => x.RefId == model.Id).FirstOrDefaultAsync();
                if (modeltrapp != null)
                {
                    if (modeltrapp.StatusApprovedDescription == GoodRequestStatus.StatusName(GoodRequestStatus.Approved))
                    {
                        model.Status = GoodRequestStatus.Approved.ToString();
                        model.Remark = model.Remark;
                    }
                    else
                    {
                        model.Status = GoodRequestStatus.WaitingApproval.ToString();
                    }
                }
                else
                {
                    model.Status = GoodRequestStatus.WaitingApproval.ToString();
                }

                context.PhoenixEdit(model);
                var save = await context.SaveChangesAsync();
                return model;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public async Task<GoodRequest> Complete(GoodRequest model)
        {
            try
            {
                model.Status = GoodRequestStatus.Completed.ToString();
                context.PhoenixEdit(model);
                var save = await context.SaveChangesAsync();
                return model;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public async Task<GoodRequest> GetByNumber(string number)
        {
            return await context.GoodRequest.Where(x => x.IsDeleted.Equals(false) && x.GoodRequestNumber == number).FirstOrDefaultAsync();
        }
    }
}
