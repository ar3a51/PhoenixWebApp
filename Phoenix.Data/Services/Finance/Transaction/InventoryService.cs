﻿using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Models.Finance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Data.Services.Finance.Transaction
{
    public interface IInventoryService : IDataService<Inventory>
    {
        Task<int> AddWithDetailAsync(Inventory Header, List<InventoryDetail> Details);
        Task<int> EditWithDetailAsync(Inventory Header, List<InventoryDetail> Details);
        Task<List<InventoryDetail>> GetDetailByGoodReceipt(string grId);
        Task<List<InventoryDetail>> GetDetail(string invId);
        Task<Inventory> GetInvAll(string invId);
        Task<int> AddDetail(InventoryDetail model);
        Task<int> EditDetail(InventoryDetail model);
    }
    public class InventoryService : IInventoryService
    {
        readonly DataContext context;
        readonly FileService uploadFile;

        public InventoryService(DataContext context, FileService uploadFile)
        {
            this.context = context;
            this.uploadFile = uploadFile;
        }

        public Task<int> AddAsync(Inventory entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> AddRangeAsync(params Inventory[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> AddWithDetailAsync(Inventory Header, List<InventoryDetail> Details)
        {   
            Header.Id = Guid.NewGuid().ToString();
            Header.InventoryInNumber = await TransID.GetTransId(context, "INV", DateTime.Today);
            await context.PhoenixAddAsync(Header);

            if (Details.Count > 0)
            {
                List<InventoryDetail> igrd = new List<InventoryDetail>();
                foreach (InventoryDetail data in Details)
                {
                    await ProcessUpload(data);
                    //data.Id = Guid.NewGuid().ToString();
                    data.InventoryId = Header.Id;
                    igrd.Add(data);
                }

                InventoryDetail[] igrdArray = igrd.ToArray();
                await context.PhoenixAddRangeAsync(igrdArray);
            }

            return await context.SaveChangesAsync();
        }

        public Task<int> DeleteAsync(Inventory entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteRageAsync(params Inventory[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteSoftAsync(Inventory entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteSoftRangeAsync(params Inventory[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> EditAsync(Inventory entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> EditRangeAsync(Inventory entity)
        {
            throw new NotImplementedException();
        }

        public async Task<int> EditWithDetailAsync(Inventory Header, List<InventoryDetail> Details)
        {            
            if (Header.InventoryInNumber == null)
            {
                Header.InventoryInNumber = await TransID.GetTransId(context, "INVT", DateTime.Today);
            }
            if(Header.InventoryName == null || Header.InventoryName == "")
            {
                Header.InventoryName = await TransID.GetTransId(context, "GR_INVENTORY", DateTime.Today);
            }
            
            context.PhoenixEdit(Header);

            if (Details.Count > 0)
            {
                List<InventoryDetail> igrd = new List<InventoryDetail>();
                foreach (InventoryDetail data in Details)
                {
                    await ProcessUpload(data);
                    data.InventoryId = Header.Id;
                    igrd.Add(data);
                }

                InventoryDetail[] igrdArray = igrd.ToArray();
                context.PhoenixEditRange(igrdArray);
            }          

            return await context.SaveChangesAsync();
        }

        public async Task<List<Inventory>> Get()
        {
            var query = await context.Inventory.Where(x => x.IsDeleted.Equals(false)).ToListAsync();
            return query;
        }

        public Task<List<Inventory>> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public async Task<Inventory> Get(string id)
        {
            var query = await context.Inventory.Where(x => x.IsDeleted.Equals(false) && x.Id==id).FirstOrDefaultAsync();
            return query;
        }

        public async Task<List<InventoryDetail>> GetDetail(string invId)
        {
            var query = await (from invtd in context.InventoryDetail
                               join con in context.MasterConditionProcurement on invtd.Condition equals con.Id
                               join loc in context.MasterLocationProcurement on invtd.Location equals loc.Id
                               where invtd.InventoryId == invId
                               select new InventoryDetail
                               {
                                   Id= invtd.Id,
                                   InventoryId = invtd.InventoryId,
                                   DateReceipt = invtd.DateReceipt,
                                   Manufacturer = invtd.Manufacturer,
                                   Brand = invtd.Brand,
                                   Model = invtd.Model,
                                   ProductNumber = invtd.ProductNumber,
                                   Color = invtd.Color,
                                   Condition = invtd.Condition,
                                   Qty = invtd.Qty,
                                   UnitName = invtd.UnitName,
                                   Location = invtd.Location,
                                   ConditionName = con.ConditionName,
                                   LocationName = loc.LocationStore,
                                   Remark = invtd.Remark,
                                   FileAttachment = invtd.FileAttachment
                               }).ToListAsync();
            //var query = await context.InventoryDetail.Where(x => x.IsDeleted.Equals(false) && x.InventoryId == invId).ToListAsync();
            return await GetFileList(query);
        }

        public async Task<List<InventoryDetail>> GetDetailByGoodReceipt(string grId)
        {
            var getInventoryId = await context.Inventory.Where(x => x.IsDeleted.Equals(false) && x.InventoryGoodsReceiptId == grId).FirstOrDefaultAsync();

            if (getInventoryId!=null)
            {
                return await context.InventoryDetail.Where(x => x.IsDeleted.Equals(false) && x.InventoryId == getInventoryId.Id).ToListAsync();
            }
            else
            {
                return new List<InventoryDetail>();
            }
            //var query = await context.InventoryDetail.Where(x => x.IsDeleted.Equals(false) && x.InventoryId==getInventoryId.Id).ToListAsync();

            
            //return query;
        }

        public async Task<Inventory> GetInvAll(string invId)
        {
            var query = await (from inv in context.Inventory
                               join job in context.JobDetail on inv.JobId equals job.Id into invjob
                               from job in invjob.DefaultIfEmpty()
                               select new Inventory
                               {
                                   Id=inv.Id,
                                   BusinessUnitId = inv.BusinessUnitId,
                                   BusinessUnit = inv.BusinessUnit,
                                   LegalEntityId = inv.LegalEntityId,
                                   LegalEntity = inv.LegalEntity,
                                   JobId = job.Id,
                                   InventoryDetail = inv.InventoryDetail,
                               }).FirstOrDefaultAsync();
            return query;
        }

        private async Task<InventoryDetail> ProcessUpload(InventoryDetail entity)
        {
            var file = await uploadFile.Upload(entity.FileUpload, null);
            if (!string.IsNullOrEmpty(file))
            {
                entity.FileAttachment = file;
            }

            return entity;
        }


        private async Task<InventoryDetail> GetFile(InventoryDetail data)
        {
            if (data.FileAttachment != null)
            {
                var file = await context.Filemasters.Where(x => x.Id == data.FileAttachment).FirstOrDefaultAsync();
                if (file != null) data.FileAttachmentName = file.Name;
            }
           
            return data;
        }

        private async Task<List<InventoryDetail>> GetFileList(List<InventoryDetail> datas)
        {
            foreach (InventoryDetail data in datas)
            {
                if (data.FileAttachment != null)
                {
                    var file = await context.Filemasters.AsNoTracking().Where(x => x.Id == data.FileAttachment).FirstOrDefaultAsync();
                    if (file != null) data.FileAttachmentName = file.Name;
                }
            }
            return datas;
        }

        public async Task<int> AddDetail(InventoryDetail model)
        {
            var data = await ProcessUpload(model);
            data.Id = Guid.NewGuid().ToString();
            await context.PhoenixAddAsync(data);
            return await context.SaveChangesAsync();
        }

        public async Task<int> EditDetail(InventoryDetail model)
        {
            var data = model;

            if (model.FileUpload != null)
            {
                data = await ProcessUpload(model);
            }
            
            context.PhoenixEdit(data);
            return await context.SaveChangesAsync();
        }
    }
}
