using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Models;
using Phoenix.Data.RestApi;
using Phoenix.Shared.Core.Contexts;

namespace Phoenix.Data
{
    public interface IJournalAdjustmentService : IDataService<JournalAdjustment>
    {
        Task<List<JournalAdjustment>> GetList(JournalAdjustment model);
        Task<List<JournalAdjustmentDetail>> GetDetailJournal(string Id);
        Task<List<Journal>> GetJournal(string Id);
        Task<int> EditJournal(JournalAdjustment entity);
        Task<int> CreateJournal(JournalAdjustment entity);
    }

    public class JournalAdjustmentService : IJournalAdjustmentService
    {
        readonly DataContext context;
        readonly FinancePeriodService financePeriod;

        /// <summary>
        /// And endpoint to manage JournalAdjustment
        /// </summary>
        /// <param name="context">Database context</param>
        public JournalAdjustmentService(DataContext context, FinancePeriodService financePeriod)
        {
            this.context = context;
            this.financePeriod = financePeriod;
        }

        public async Task<int> AddAsync(JournalAdjustment entity)
        {
            return await AddEdit(entity, false, false);
        }

        public Task<int> AddRangeAsync(params JournalAdjustment[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteAsync(JournalAdjustment entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteRageAsync(params JournalAdjustment[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> DeleteSoftAsync(JournalAdjustment entity)
        {
            await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.JvDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.JA);
            context.PhoenixDelete(entity);

            var journalDetailList = await context.JournalAdjustmentDetails.AsNoTracking().Where(x => x.JournalAdjustmentId == entity.Id).ToListAsync();
            context.PhoenixDeleteRange(journalDetailList.ToArray());

            var journalList = await context.Journal.AsNoTracking().Where(x => x.ReferenceId == entity.Id).ToListAsync();
            context.PhoenixDeleteRange(journalList.ToArray());
            return await context.SaveChangesAsync();
        }

        public Task<int> DeleteSoftRangeAsync(params JournalAdjustment[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> EditAsync(JournalAdjustment entity)
        {
            return await AddEdit(entity, true, false);
        }

        public Task<int> EditRangeAsync(JournalAdjustment entity)
        {
            throw new NotImplementedException();
        }

        public async Task<int> CreateJournal(JournalAdjustment entity)
        {
            return await AddEdit(entity, false, true);
        }

        public async Task<int> EditJournal(JournalAdjustment entity)
        {
            return await AddEdit(entity, true, true);
        }

        private async Task<int> AddEdit(JournalAdjustment entity, bool isEdit, bool isJournal)
        {
            await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.JvDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.JA);
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    if (isEdit)
                    {
                        context.PhoenixEdit(entity);
                    }
                    else
                    {
                        entity.Id = await TransID.GetTransId(context, Code.JournalAdjustment, DateTime.Today);
                        entity.Code = await TransID.GetTransId(context, Code.JournalVoucher, DateTime.Today);
                        await context.PhoenixAddAsync(entity);
                        await context.SaveChangesAsync();
                    }

                    if (!string.IsNullOrEmpty(entity.Id))
                    {
                        var detail = context.JournalAdjustmentDetails.AsNoTracking().Where(x => x.JournalAdjustmentId == entity.Id).ToList();
                        if (detail.Count > 0)
                        {
                            context.JournalAdjustmentDetails.RemoveRange(detail);
                        }
                    }

                    if (entity.JournalAdjustmentDetails != null)
                    {
                        Parallel.ForEach(entity.JournalAdjustmentDetails, (item) =>
                        {
                            item.JournalAdjustmentId = entity.Id;
                            context.PhoenixAddAsync(item);
                        });
                    }

                    //Create Journal
                    if (isJournal == true)
                    {
                        if (!string.IsNullOrEmpty(entity.Id))
                        {
                            var journal = context.Journal.AsNoTracking().Where(x => x.ReferenceId == entity.Id).ToList();
                            if (journal.Count > 0)
                            {
                                context.Journal.RemoveRange(journal);
                            }
                        }

                        var CurrencyCode = await context.Currencies.AsNoTracking().Where(x => x.Id == entity.CurrencyId).Select(x => x.CurrencyCode).FirstOrDefaultAsync();
                        var createJournal = entity.JournalAdjustmentDetails
                            .Select(x => new Journal
                            {
                                Id = Guid.NewGuid().ToString(),
                                Description = entity.JvDescription,
                                Debit = x.Debit,
                                Credit = x.Credit,
                                ReferenceId = entity.Id,
                                ReferenceNumber = entity.Code,
                                AccountId = x.CoaId,
                                ReferenceDate = entity.JvDate,
                                TransactionDate = entity.JvDate,
                                CurrencyId = entity.CurrencyId,
                                Unit = CurrencyCode,
                                ExchangeRate = entity.ExchangeRate,
                                LegalId = entity.LegalEntityId,
                                AffiliateId = entity.AffiliationId,
                                DivisiId = entity.BusinessUnitId,
                                MainserviceCategoryId = entity.MainServiceCategoryId,
                                LineCode = "1",
                                PcaId = entity.PcaId,
                                PceId = entity.PceId,
                                JobId = entity.JobId,
                                IsJobClosed = (entity.IsDraft ?? true) ? "N" : "Y"
                            });

                        await context.PhoenixAddRangeAsync(createJournal.ToArray());
                    }
                    else
                    {
                        if (!(entity.IsDraft ?? true))
                        {
                            var journal = await context.Journal.AsNoTracking().Where(x => x.ReferenceId == entity.Id).ToListAsync();
                            Parallel.ForEach(journal, (item) =>
                            {
                                item.IsJobClosed = "Y";
                                context.Journal.Update(item);
                            });
                        }
                    }

                    var save = await context.SaveChangesAsync();
                    transaction.Commit();
                    return save;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<List<JournalAdjustment>> GetList(JournalAdjustment model)
        {
            var result = await (from a in context.JournalAdjustments
                                join b in context.LegalEntity on a.LegalEntityId equals b.Id
                                join c in context.Affiliations on a.AffiliationId equals c.Id
                                join d in context.BusinessUnits on a.BusinessUnitId equals d.Id
                                where !(a.IsDeleted ?? false) &&
                                (string.IsNullOrEmpty(model.LegalEntityId) ? true : a.LegalEntityId == model.LegalEntityId) &&
                                ((model.JvDate != null && model.JvDateEnd != null) ? (a.JvDate >= model.JvDate && a.JvDate <= model.JvDateEnd) : true) &&
                                (string.IsNullOrEmpty(model.Code) ? true : a.Code == model.Code) &&
                                (string.IsNullOrEmpty(model.CurrencyId) ? true : a.CurrencyId == model.CurrencyId) &&
                                ((model.ExchangeRate == null) ? true : a.ExchangeRate == model.ExchangeRate) &&
                                (string.IsNullOrEmpty(model.JvDescription) ? true : a.JvDescription == model.JvDescription) &&
                                (string.IsNullOrEmpty(model.JobId) ? true : a.JobId == model.JobId)
                                select new JournalAdjustment
                                {
                                    Id = a.Id,
                                    JvDate = a.JvDate,
                                    Code = a.Code,
                                    //JvReferenceNo = a.JvReferenceNo,
                                    JvDescription = a.JvDescription,
                                    LegalEntityName = b.LegalEntityName,
                                    BusinessUnitName = d.UnitName,
                                    AfiliationName = c.AffiliationName,
                                    PcaId = a.PcaId,
                                    PceId = a.PceId,
                                    JobId = a.JobId,
                                    IsDraft = a.IsDraft,
                                    CreatedOn = a.CreatedOn
                                }).OrderByDescending(x => x.CreatedOn).ToListAsync();
            return result;
        }

        public Task<List<JournalAdjustment>> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public async Task<JournalAdjustment> Get(string Id) => await context.JournalAdjustments.AsNoTracking().Where(x => !(x.IsDeleted ?? false) && x.Id == Id).FirstOrDefaultAsync();

        public async Task<List<JournalAdjustmentDetail>> GetDetailJournal(string Id)
        {
            return await (from a in context.JournalAdjustmentDetails
                          join b in context.ChartOfAccounts on a.CoaId equals b.Id
                          where a.JournalAdjustmentId == Id
                          select new JournalAdjustmentDetail
                          {
                              Id = a.Id,
                              JournalAdjustmentId = a.JournalAdjustmentId,
                              CoaId = a.CoaId,
                              Description = a.Description,
                              Debit = a.Debit,
                              Credit = a.Credit,
                              CodeRec = b.CodeRec,
                              AccountName = b.Name5,
                              Condition = a.Condition,
                              CreatedBy = a.CreatedBy,
                              CreatedOn = a.CreatedOn
                          }).ToListAsync();
        }

        public async Task<List<Journal>> GetJournal(string Id)
        {
            var result = await (from a in context.Journal
                                join b in context.LegalEntity on a.LegalId equals b.Id
                                join c in context.Affiliations on a.AffiliateId equals c.Id
                                join d in context.BusinessUnits on a.DivisiId equals d.Id
                                join e in context.ChartOfAccounts on a.AccountId equals e.Id
                                where !(a.IsDeleted ?? false) && a.ReferenceId == Id
                                select new Journal
                                {
                                    AccountId = a.AccountId,
                                    AccountName = e.Name5,
                                    ReferenceNumber = a.ReferenceNumber,
                                    Description = a.Description,
                                    Debit = a.Debit,
                                    Credit = a.Credit,
                                    LegalEntityName = b.LegalEntityName,
                                    DivisiName = d.UnitName,
                                    AfiliationName = c.AffiliationName,
                                    JobId = a.JobId,
                                    PceId = a.PceId,
                                    PcaId = a.PcaId,
                                    CodeRec = e.CodeRec,
                                    CreatedOn = a.CreatedOn
                                }).OrderByDescending(x => x.CreatedOn).ToListAsync();
            return result;
        }

        public Task<List<JournalAdjustment>> Get()
        {
            throw new NotImplementedException();
        }
    }
}