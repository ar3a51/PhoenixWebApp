﻿using Phoenix.Data.Models.Finance;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Phoenix.Data.Models.Finance.Transaction.PaymentRequest;
using Phoenix.Data.Attributes;
using Phoenix.Data.Services.Um;
using Phoenix.Data.Models;
using Phoenix.Data.Models.Finance.Transaction;
using System.Data.SqlClient;
using Phoenix.Data.Models.Um;
using Phoenix.Shared.Core.PrincipalHelpers;
using Phoenix.Data.Models.Finance.MasterData;

namespace Phoenix.Data.Services.Finance
{
    public interface IPaymentRequestService : IDataService<PaymentRequest>
    {
        Task<List<PaymentRequest>> GetWithFilterAP(PaymentRequestFilter Filter);
        Task<int> ApproveData(PaymentRequest entity);
        Task<PaymentRequest> Reject(PaymentRequest entity);
        Task<PaymentRequest> Reconsiliation(PaymentRequest entity);
        Task<Bank> GetAccountNumberData(string Id);
        Task<MasterPettyCash> GetPettycashData(string Id);
        Task<Bank> GetVendorData(string Id);

        Task<List<PaymentRequest>> GetWithFilterCA(PaymentRequestFilter Filter);
        Task<List<PaymentRequest>> GetWithFilter(PaymentRequestFilter Filter);
        Task<List<PaymentRequest>> GetHistoryPayment(string Id);
        Task<int> ApproveAsync(PaymentRequest entity);
        Task<List<PaymentInformationData>> GetPaymentInformationData(string referenceId);
        Task<List<Journal>> GetBankPaymentJournal(string BankPaymentId);
        Task<int> CreateJournalWithSP(string PaymentRequestId);
        Task<List<AccountPayable>> GetListAPPosted(PaymentRequestFilter Filter);
        Task<int> AddWithDetail(PaymentRequestDTO entity);
        Task<int> EditWithDetail(PaymentRequestDTO entity);
        Task<List<AccountPayable>> GetListDetailPaymentRequest(string Id);
        Task<List<AccountPayable>> GetListDetailPaymentRequestByPairingGroupId(string Id);
        Task<List<PaymentRequestDetail>> GetDetail(string Id);
    }

    public class PaymentRequestService : IPaymentRequestService
    {
        readonly DataContext context;
        readonly GlobalFunctionApproval globalFcApproval;
        readonly IManageMenuService menuService;
        readonly FinancePeriodService financePeriod;
        readonly StatusLogService log;
        string idTemplate = "";
        /// <summary>
        /// And endpoint to manage PettyCash
        /// </summary>
        /// <param name="context">Database context</param>
        public PaymentRequestService(DataContext context, GlobalFunctionApproval globalFcApproval, IManageMenuService menuService, FinancePeriodService financePeriod, StatusLogService log)
        {
            this.context = context;
            this.globalFcApproval = globalFcApproval;
            this.menuService = menuService;
            this.financePeriod = financePeriod;
            this.log = log;
        }

        public async Task<int> ApproveAsync(PaymentRequest entity)
        {
            return await globalFcApproval.ProcessApproval<PaymentRequest>(entity.Id, Int32.Parse(entity.Status), entity.RemarkRejected, entity);
        }

        public async Task<int> AddSettle(PaymentRequest entity)
        {
            var save = 0;
            if (entity.SourceDoc.ToLower() == "ca")
            {
                var modelca = await context.CashAdvances.Where(x => x.CaNumber == entity.ReferenceNumberId).FirstOrDefaultAsync();
                CashAdvanceSettlement caSettle = new CashAdvanceSettlement()
                {
                    Id = Guid.NewGuid().ToString(),
                    CashAdvanceId = modelca.Id,
                    CashAdvanceSettleDate = DateTime.Now,
                    AccountNumber = modelca.PayeeAccountNumber,
                    Amount = modelca.Amount,
                    BankId = modelca.PayeeBankId,
                    JobId = modelca.JobId,
                    JobNumber = modelca.JobNumber,
                    RequestEmployeeId = modelca.RequestEmployeeId,
                    RequestEmployeeName = modelca.RequestEmployeeName,
                    RequestJobGradeId = modelca.RequestJobGradeId,
                    RequestJobGradeName = modelca.RequestJobGradeName,
                    RequestJobTitleId = modelca.RequestJobTitleId,
                    RequestJobTitleName = modelca.RequestJobTitleName,
                    MainServiceCategoryId = modelca.ProjectActivity,
                    CaStatusId = "1",
                    SattlementNumber = await TransID.GetTransId(context, Code.CashAdvanceSattlement, DateTime.Today)
                };
                await context.PhoenixAddAsync(caSettle);

                save = await context.SaveChangesAsync();

            }
            return save;
        }



        public async Task<int> ApproveData(PaymentRequest entity)
        {
            try
            {
                PaymentRequest model = new PaymentRequest();
                model = entity;
                entity.RemarkRejected = entity.Remarks;
                if (string.IsNullOrEmpty(entity.RemarkRejected))
                {
                    entity.RemarkRejected = "";
                }
                await globalFcApproval.ProcessApproval<PaymentRequest>(model.Id, StatusTransaction.Approved, model.RemarkRejected, model);
                var modeltrapp = await context.TrTemplateApprovals.Where(x => x.RefId == entity.Id).FirstOrDefaultAsync();
                if (modeltrapp != null)
                {
                    if (modeltrapp.StatusApprovedDescription == StatusTransaction.StatusName(StatusTransaction.Approved))
                    {
                        var modelbp = await context.PaymentRequest.Where(x => x.IsDeleted.Equals(false) && x.Id == entity.Id).FirstOrDefaultAsync();
                        if (modelbp != null)
                        {
                            model.Status = PaymentRequestStatus.Complete;
                            //var result = await context.Database.ExecuteSqlCommandAsync("EXECUTE fn.create_jurnal_bp @p_bank_payment_id", parameters: new[] { new SqlParameter("@p_bank_payment_id", entity.Id) });

                            await AddSettle(entity);
                            if(entity.PayToType == "1")//PETTYCASH
                            {
                                var modelPettycah = await context.MasterPettyCashs.Where(x => x.IsDeleted.Equals(false) && x.Id == entity.PayToPettyCash).FirstOrDefaultAsync();
                                modelPettycah.AmountInsused = (modelPettycah.AmountInsused == null ? 0 : modelPettycah.AmountInsused) + entity.AmountPay;
                                context.PhoenixEdit(modelPettycah);
                            }
                        }
                        else
                        {
                            model.Status = PaymentRequestStatus.WaitingApproval;
                        }
                    }
                    else
                    {
                        model.Status = PaymentRequestStatus.WaitingApproval;
                    }
                }
                else
                {
                    model.Status = PaymentRequestStatus.WaitingApproval;
                }
                await log.AddAsync(new StatusLog() { TransactionId = entity.Id, Status = PaymentRequestStatus.GetStatus(model.Status), Description = entity.LogDescription });
                context.PhoenixEdit(model);
                var save = await context.SaveChangesAsync();
                return save;
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new Exception(ex.Message.ToString());
            }
            catch (DbUpdateException exc)
            {
                throw new Exception(exc.Message.ToString());
            }
        }


        public async Task<PaymentRequest> Reconsiliation(PaymentRequest entity)
        {
            try
            {
                entity.Status = PaymentRequestStatus.Reconsiliation;
                context.PhoenixEdit(entity);
                await log.AddAsync(new StatusLog() { TransactionId = entity.Id, Status = PaymentRequestStatus.GetStatus(PaymentRequestStatus.Reconsiliation), Description = entity.LogDescription });
                await context.SaveChangesAsync();
                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
        public async Task<PaymentRequest> Reject(PaymentRequest entity)
        {
            try
            {
                await globalFcApproval.ProcessApproval<PaymentRequest>(entity.Id, Int32.Parse(entity.Status), entity.RemarkRejected, entity);

                var modeltrapp = await context.TrTemplateApprovals.Where(x => x.RefId == entity.Id).FirstOrDefaultAsync();
                if (modeltrapp != null)
                {
                    if (modeltrapp.StatusApprovedDescription == StatusTransaction.StatusName(StatusTransaction.Rejected))
                    {
                        entity.Status = PaymentRequestStatus.Reject;
                        entity.ModifiedOn = DateTime.Now;
                        context.PhoenixEdit(entity);
                    }
                }
                else
                {
                    entity.Status = PaymentRequestStatus.Reject;
                    entity.ModifiedOn = DateTime.Now;
                    context.PhoenixEdit(entity);
                }
                await log.AddAsync(new StatusLog() { TransactionId = entity.Id, Status = PaymentRequestStatus.GetStatus(PaymentRequestStatus.Reject), Description = entity.RemarkRejected });
                await context.SaveChangesAsync();
                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public async Task<int> AddAsync(PaymentRequest entity)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    //await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.PaymentRequestDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.BP);
                    var isApprove = true;

                    entity.Id = Guid.NewGuid().ToString();
                    //entity.RemarkRejected = entity.Remarks;
                    //entity.TotalAmount = entity.AmountPay;// + entity.Vat;

                    if (string.IsNullOrEmpty(entity.PairingGroupId))
                    {
                        entity.PairingGroupId = Guid.NewGuid().ToString();
                    }

                    await context.PhoenixAddAsync(entity);

                    //approval process request to system

                    var dataMenu = await menuService.GetByUniqueName(MenuUnique.PaymentRequest).ConfigureAwait(false);
                    var vm = new Models.ViewModel.TransApprovalHrisVm()
                    {
                        MenuId = dataMenu.Id,
                        RefId = entity.Id,
                        DetailLink = $"{ApprovalLink.PaymentRequest}?Id={entity.Id}&isApprove=true",
                        //Tname = "fn.PurchaseRequest",//"SP#NAMA SP NYA",
                        IdTemplate = idTemplate
                    };
                    var subGroupId = ClainJwtPrincipalHelper.GetBusinessSubGroup(context.HttpContext);
                    var divisionId = ClainJwtPrincipalHelper.GetBusinessUnitDivisi(context.HttpContext);
                    var employeeId = ClainJwtPrincipalHelper.GetEmployeeId(context.HttpContext);
                    isApprove = await globalFcApproval.Save(vm, subGroupId, divisionId, employeeId);


                    if (isApprove)
                    {
                        var save = await context.SaveChangesAsync();
                        transaction.Commit();
                        return save;
                    }
                    else
                    {
                        transaction.Rollback();
                        throw new Exception("please check the approval template that will be processed");
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message.ToString());
                }
            }

        }

        public async Task<int> AddRangeAsync(params PaymentRequest[] entities)
        {
            await context.PhoenixAddRangeAsync(entities);
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteAsync(PaymentRequest entity)
        {
            context.PhoenixDelete(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteRageAsync(params PaymentRequest[] entities)
        {
            context.PhoenixDeleteRange(entities);
            return await context.SaveChangesAsync();

        }

        public async Task<int> DeleteSoftAsync(PaymentRequest entity)
        {
            context.PhoenixDelete(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteSoftRangeAsync(params PaymentRequest[] entities)
        {
            context.PhoenixDeleteRange(entities);
            return await context.SaveChangesAsync();
        }

        public async Task<int> EditAsync(PaymentRequest entity)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.PaymentRequestDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.BP);
                    //entity.Remarks = entity.RemarkRejected;
                    //entity.TotalAmount = entity.AmountPay;// + entity.Vat;
                    context.PhoenixEdit(entity);

                    var trapprovalmodel = await context.TrTemplateApprovals.Where(x => x.RefId == entity.Id).FirstOrDefaultAsync();
                    if (trapprovalmodel == null)
                    {
                        var isApprove = true;
                        var dataMenu = await menuService.GetByUniqueName(MenuUnique.PaymentRequest).ConfigureAwait(false);
                        var vm = new Models.ViewModel.TransApprovalHrisVm()
                        {
                            MenuId = dataMenu.Id,
                            RefId = entity.Id,
                            DetailLink = $"{ApprovalLink.PaymentRequest}?Id={entity.Id}&isApprove=true",
                            //Tname = "fn.PurchaseRequest",//"SP#NAMA SP NYA",
                            IdTemplate = idTemplate
                        };
                        var subGroupId = ClainJwtPrincipalHelper.GetBusinessSubGroup(context.HttpContext);
                        var divisionId = ClainJwtPrincipalHelper.GetBusinessUnitDivisi(context.HttpContext);
                        var employeeId = ClainJwtPrincipalHelper.GetEmployeeId(context.HttpContext);
                        isApprove = await globalFcApproval.Save(vm, subGroupId, divisionId, employeeId);


                        if (isApprove)
                        {
                            var save = await context.SaveChangesAsync();
                            transaction.Commit();
                            return save;
                        }
                        else
                        {
                            transaction.Rollback();
                            throw new Exception("please check the approval template that will be processed");
                        }

                    }
                    else
                    {

                        var save = await context.SaveChangesAsync();
                        transaction.Commit();
                        return save;
                    }

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message.ToString());
                }
            }

        }

        public async Task<int> EditRangeAsync(PaymentRequest entity)
        {
            context.PhoenixEditRange(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<List<PaymentRequest>> Get()
        {
            var model = await (from tb in context.PaymentRequest
                               join inv in context.InvoiceReceivables on tb.InvoiceReceivableId equals inv.Id
                               join bu in context.BusinessUnits on tb.BusinessUnitId equals bu.Id
                               join le in context.LegalEntity on tb.LegalEntityId equals le.Id into leg
                               from legg in leg.DefaultIfEmpty()
                               where tb.IsDeleted.Equals(false)
                               select new PaymentRequest
                               {
                                   Id = tb.Id,
                                   InvoiceReceivableId = tb.InvoiceReceivableId,
                                   InvoiceReceivableNumber = inv.InvoiceNumber,
                                   AccountId = tb.AccountId,
                                   AdvertisingTax = tb.AdvertisingTax,
                                   AffilicationId = tb.AffilicationId,
                                   BusinessUnitId = tb.BusinessUnitId,
                                   BusinessUnitName = bu.UnitName,
                                   Amount = tb.Amount,
                                   BankName = tb.BankName,
                                   CurrencyId = tb.CurrencyId,
                                   Discount = tb.Discount,
                                   DueDate = tb.DueDate,
                                   ExchangeRate = tb.ExchangeRate,
                                   LegalEntityId = tb.LegalEntityId,
                                   LegalEntityName = legg.LegalEntityName,
                                   PaymentRequestDate = tb.PaymentRequestDate,
                                   PaymentRequestNumber = tb.PaymentRequestNumber,
                                   PaymentTypeId = tb.PaymentTypeId,
                                   TaxPayable = tb.TaxPayable,
                                   TaxPayablePercent = tb.TaxPayablePercent,
                                   Status = tb.Status,
                                   MainServiceCategoryId = tb.MainServiceCategoryId,
                                   SourceDoc = tb.SourceDoc,
                                   PayToVendor = tb.PayToVendor,
                                   BankId = tb.BankId,
                                   AccountNumber = tb.AccountNumber,
                                   AmountPay = tb.AmountPay,
                                   ReferenceNumberId = string.IsNullOrEmpty(tb.ReferenceNumberId) ? "" : tb.ReferenceNumberId,
                                   Vat = tb.Vat,
                                   VatPercent = tb.VatPercent,
                                   TotalAmount = tb.TotalAmount,
                                   PaytoName = tb.PaytoName,
                                   JobName = tb.JobId == null ? tb.PurchaseRequestId : tb.JobId,
                                   IsBalance = tb.IsBalance
                               }).ToListAsync();
            return model;
        }


        public Task<List<PaymentRequest>> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public async Task<PaymentRequest> Get(string Id)
        {
            try
            {
                return await (from tb in context.PaymentRequest
                              join b in context.LegalEntity on tb.LegalEntityId equals b.Id into gb
                              from bb in gb.DefaultIfEmpty()
                              where tb.IsDeleted.Equals(false) && tb.Id == Id
                              select new PaymentRequest
                              {
                                  Id = tb.Id,
                                  InvoiceReceivableId = tb.InvoiceReceivableId,
                                  InvoiceReceivableNumber = tb.InvoiceReceivableNumber,
                                  AccountId = tb.AccountId,
                                  AdvertisingTax = tb.AdvertisingTax,
                                  AffilicationId = tb.AffilicationId,
                                  BusinessUnitId = tb.BusinessUnitId,
                                  Amount = tb.Amount,
                                  BankName = tb.BankName,
                                  CurrencyId = tb.CurrencyId,
                                  Discount = tb.Discount,
                                  DueDate = tb.DueDate,
                                  BankId = tb.BankId,
                                  MainServiceCategoryId = tb.MainServiceCategoryId,
                                  SourceDoc = tb.SourceDoc,
                                  PayToVendor = tb.PayToVendor,
                                  AccountNumber = tb.AccountNumber,
                                  ExchangeRate = tb.ExchangeRate,
                                  LegalEntityId = tb.LegalEntityId,
                                  LegalEntityName = bb.LegalEntityName,
                                  PaymentRequestDate = tb.PaymentRequestDate,
                                  PaymentRequestNumber = tb.PaymentRequestNumber,
                                  PaymentTypeId = tb.PaymentTypeId,
                                  TaxPayable = tb.TaxPayable,
                                  TaxPayablePercent = tb.TaxPayablePercent,
                                  Status = tb.Status,
                                  AmountPay = tb.AmountPay,
                                  ReferenceNumberId = tb.ReferenceNumberId,
                                  Vat = tb.Vat,
                                  VatPercent = tb.VatPercent,
                                  TotalAmount = tb.TotalAmount,
                                  PaytoName = tb.PaytoName,
                                  JobName = tb.JobId == null ? tb.PurchaseRequestId : tb.JobId,
                                  PairingGroupId = tb.PairingGroupId,
                                  Balance = tb.Balance,
                                  BankChargesCoaId = tb.BankChargesCoaId,
                                  Charges = tb.Charges,
                                  Remarks = tb.Remarks,
                                  JobId = tb.JobId,
                                  ApprovedBy = tb.ApprovedBy,
                                  ApprovedOn = tb.ApprovedOn,
                                  CreatedBy = tb.CreatedBy,
                                  CreatedOn = tb.CreatedOn,
                                  DeletedBy = tb.DeletedBy,
                                  DeletedOn = tb.DeletedOn,
                                  IsActive = tb.IsActive,
                                  IsDefault = tb.IsDefault,
                                  IsDeleted = tb.IsDeleted,
                                  IsLocked = tb.IsLocked,
                                  IsMultiple = tb.IsMultiple,
                                  ModifiedBy = tb.ModifiedBy,
                                  ModifiedOn = tb.ModifiedOn,
                                  OwnerId = tb.OwnerId,
                                  PaytoBank = tb.PaytoBank,
                                  PaytoCoaId = tb.PaytoCoaId,
                                  PurchaseRequestId = tb.PurchaseRequestId,
                                  Recon = tb.Recon,
                                  RemarkRejected = tb.RemarkRejected,
                                  TaxPayCoaId = tb.TaxPayCoaId,
                                  PayToAccountNumber = tb.PayToAccountNumber,
                                  PayToPettyCash = tb.PayToPettyCash,
                                  PayToType = tb.PayToType,
                                  TotalAmountPay = tb.TotalAmountPay,
                                  TaxPayableCoa = tb.TaxPayableCoa
                              }).FirstOrDefaultAsync();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
        }


        public async Task<List<PaymentRequest>> GetWithFilterAP(PaymentRequestFilter Filter)
        {
            var model = await (from tb in context.PaymentRequest
                               join inv in context.InvoiceReceivables on tb.InvoiceReceivableId equals inv.Id
                               join bu in context.BusinessUnits on tb.BusinessUnitId equals bu.Id
                               join le in context.LegalEntity on tb.LegalEntityId equals le.Id into leg
                               from legg in leg.DefaultIfEmpty()
                               where tb.IsDeleted.Equals(false) && !string.IsNullOrEmpty(tb.InvoiceReceivableId)
                               select new PaymentRequest
                               {
                                   Id = tb.Id,
                                   InvoiceReceivableId = tb.InvoiceReceivableId,
                                   InvoiceReceivableNumber = inv.InvoiceNumber,
                                   AccountId = tb.AccountId,
                                   AdvertisingTax = tb.AdvertisingTax,
                                   AffilicationId = tb.AffilicationId,
                                   BusinessUnitId = tb.BusinessUnitId,
                                   BusinessUnitName = bu.UnitName,
                                   Amount = tb.Amount,
                                   BankName = tb.BankName,
                                   CurrencyId = tb.CurrencyId,
                                   Discount = tb.Discount,
                                   DueDate = tb.DueDate,
                                   BankId = tb.BankId,
                                   MainServiceCategoryId = tb.MainServiceCategoryId,
                                   SourceDoc = tb.SourceDoc,
                                   PayToVendor = tb.PayToVendor,
                                   AccountNumber = tb.AccountNumber,
                                   ExchangeRate = tb.ExchangeRate,
                                   LegalEntityId = tb.LegalEntityId,
                                   LegalEntityName = legg.LegalEntityName,
                                   PaymentRequestDate = tb.PaymentRequestDate,
                                   PaymentRequestNumber = tb.PaymentRequestNumber,
                                   PaymentTypeId = tb.PaymentTypeId,
                                   TaxPayable = tb.TaxPayable,
                                   TaxPayablePercent = tb.TaxPayablePercent,
                                   Status = tb.Status,
                                   AmountPay = tb.AmountPay,
                                   ReferenceNumberId = tb.ReferenceNumberId,
                                   Vat = tb.Vat,
                                   VatPercent = tb.VatPercent,
                                   TotalAmount = tb.TotalAmount,
                                   PaytoName = tb.PaytoName,
                                   JobName = tb.JobId == null ? tb.PurchaseRequestId : tb.JobId

                               }).ToListAsync();

            List<PaymentRequest> pay = new List<PaymentRequest>();
            pay = model;

            if (Filter.isBankPayment)
            {
                pay = model.Where(x => Convert.ToInt32(x.Status) >= Convert.ToInt32(PaymentRequestStatus.Approved)).ToList();
            }
            else
            {
                pay = model.Where(x => Convert.ToInt32(x.Status) < Convert.ToInt32(PaymentRequestStatus.Approved)).ToList();
            }

            if (!string.IsNullOrEmpty(Filter.FilterStatus))
            {
                pay = model.Where(x => x.Status == Filter.FilterStatus).ToList();
            }

            if (!string.IsNullOrEmpty(Filter.FilterBusinessUnitId))
            {
                pay = model.Where(x => x.BusinessUnitId == Filter.FilterBusinessUnitId).ToList();
            }


            if (!string.IsNullOrEmpty(Filter.FilterLegalEntity))
            {
                pay = model.Where(x => x.LegalEntityId == Filter.FilterLegalEntity).ToList();
            }

            if (!string.IsNullOrEmpty(Filter.FilterReferenceNumber))
            {
                pay = model.Where(x => x.ReferenceNumberId == Filter.FilterReferenceNumber).ToList();
            }

            if (!string.IsNullOrEmpty(Filter.FilterInvoice))
            {
                var datainv = await context.InvoiceReceivables.Where(y => y.InvoiceNumber == Filter.FilterInvoice).FirstOrDefaultAsync();
                if (!string.IsNullOrEmpty(datainv.Id))
                {
                    pay = model.Where(x => x.InvoiceReceivableId == datainv.Id).ToList();
                }
            }

            if (Filter.FilterStartDate != null && Filter.FilterEndDate != null)
            {

                pay = model.Where(y => y.PaymentRequestDate >= Convert.ToDateTime(Filter.FilterStartDate) && y.PaymentRequestDate <= Convert.ToDateTime(Filter.FilterEndDate)).ToList();
            }

            return pay;
        }

        public async Task<List<PaymentRequest>> GetWithFilterCA(PaymentRequestFilter Filter)
        {
            var model = await (from tb in context.PaymentRequest
                               join bu in context.BusinessUnits on tb.BusinessUnitId equals bu.Id
                               join le in context.LegalEntity on tb.LegalEntityId equals le.Id
                               where tb.IsDeleted.Equals(false) && string.IsNullOrEmpty(tb.InvoiceReceivableId)
                               select new PaymentRequest
                               {
                                   Id = tb.Id,
                                   InvoiceReceivableId = tb.InvoiceReceivableId,
                                   InvoiceReceivableNumber = "",
                                   AccountId = tb.AccountId,
                                   AdvertisingTax = tb.AdvertisingTax,
                                   AffilicationId = tb.AffilicationId,
                                   BusinessUnitId = tb.BusinessUnitId,
                                   BusinessUnitName = bu.UnitName,
                                   Amount = tb.Amount,
                                   BankName = tb.BankName,
                                   CurrencyId = tb.CurrencyId,
                                   Discount = tb.Discount,
                                   BankId = tb.BankId,
                                   MainServiceCategoryId = tb.MainServiceCategoryId,
                                   SourceDoc = tb.SourceDoc,
                                   PayToVendor = tb.PayToVendor,
                                   AccountNumber = tb.AccountNumber,
                                   DueDate = tb.DueDate,
                                   Status = tb.Status,
                                   ExchangeRate = tb.ExchangeRate,
                                   LegalEntityId = tb.LegalEntityId,
                                   LegalEntityName = le.LegalEntityName,
                                   PaymentRequestDate = tb.PaymentRequestDate,
                                   PaymentRequestNumber = tb.PaymentRequestNumber,
                                   PaymentTypeId = tb.PaymentTypeId,
                                   TaxPayable = tb.TaxPayable,
                                   TaxPayablePercent = tb.TaxPayablePercent,
                                   ReferenceNumberId = tb.ReferenceNumberId,
                                   Vat = tb.Vat,
                                   AmountPay = tb.AmountPay,
                                   VatPercent = tb.VatPercent,
                                   TotalAmount = tb.TotalAmount,
                                   PaytoName = tb.PaytoName,
                                   JobName = tb.JobId == null ? tb.PurchaseRequestId : tb.JobId
                               }).ToListAsync();

            List<PaymentRequest> pay = new List<PaymentRequest>();
            pay = model;

            if (!string.IsNullOrEmpty(Filter.FilterStatus))
            {
                pay = model.Where(x => x.Status == Filter.FilterStatus).ToList();
            }

            if (!string.IsNullOrEmpty(Filter.FilterBusinessUnitId))
            {
                pay = model.Where(x => x.BusinessUnitId == Filter.FilterBusinessUnitId).ToList();
            }

            if (!string.IsNullOrEmpty(Filter.FilterLegalEntity))
            {
                pay = model.Where(x => x.LegalEntityId == Filter.FilterLegalEntity).ToList();
            }

            if (!string.IsNullOrEmpty(Filter.FilterReferenceNumber))
            {
                pay = model.Where(x => x.ReferenceNumberId == Filter.FilterReferenceNumber).ToList();
            }

            if (!string.IsNullOrEmpty(Filter.FilterInvoice))
            {
                var datainv = await context.InvoiceReceivables.Where(y => y.InvoiceNumber == Filter.FilterInvoice).FirstOrDefaultAsync();
                if (!string.IsNullOrEmpty(datainv.Id))
                {
                    pay = model.Where(x => x.InvoiceReceivableId == datainv.Id).ToList();
                }
            }

            if (Filter.FilterStartDate != null && Filter.FilterEndDate != null)
            {

                pay = model.Where(y => y.PaymentRequestDate >= Convert.ToDateTime(Filter.FilterStartDate) && y.PaymentRequestDate <= Convert.ToDateTime(Filter.FilterEndDate)).ToList();
            }

            return model.ToList();
        }

        public async Task<List<PaymentInformationData>> GetPaymentInformationData(string referenceId)
        {
            var model = await (from tb in context.PaymentRequest
                               join su in context.InvoiceReceivables on tb.InvoiceReceivableId equals su.Id
                               where tb.IsDeleted.Equals(false) && tb.ReferenceNumberId == referenceId && tb.Status == PaymentRequestStatus.PayBalance
                               select new PaymentInformationData
                               {
                                   Id = tb.Id,
                                   Amount = tb.TotalAmountPay,
                                   Billing = su.TotalCostVendor,
                                   Charges = tb.Charges == null ? 0 : tb.Charges,
                                   EvidenceNo = tb.ReferenceNumberId,
                                   Discount = tb.Discount == null ? 0 : tb.Discount,
                                   TaxPayable = tb.TaxPayable == null ? 0 : tb.TaxPayable,
                                   TaxPayablePercent = tb.TaxPayablePercent == null ? 0 : tb.TaxPayablePercent,
                                   Vat = tb.Vat == null ? 0 : tb.Vat,
                                   VatPercent = tb.VatPercent == null ? 0 : tb.VatPercent,
                                   Paid = su.TotalCostVendor - tb.AmountPay,
                                   Recon = tb.Recon == null ? false : tb.Recon
                               }).ToListAsync();

            if (model == null)
            {
                model = new List<PaymentInformationData>();
                var model2 = await (from tb in context.PaymentRequest
                                    join su in context.CashAdvances on tb.ReferenceNumberId equals su.Id
                                    join de in context.CashAdvanceDetails on su.Id equals de.CashAdvanceId
                                    where tb.IsDeleted.Equals(false) && tb.ReferenceNumberId == referenceId && tb.Status == PaymentRequestStatus.PayBalance
                                    select new PaymentInformationData
                                    {
                                        Id = tb.Id,
                                        Amount = tb.TotalAmountPay,
                                        Billing = de.UnitPrice * de.Qty,
                                        Charges = tb.Charges == null ? 0 : tb.Charges,
                                        EvidenceNo = tb.ReferenceNumberId,
                                        Discount = tb.Discount == null ? 0 : tb.Discount,
                                        TaxPayable = tb.TaxPayable == null ? 0 : tb.TaxPayable,
                                        TaxPayablePercent = tb.TaxPayablePercent == null ? 0 : tb.TaxPayablePercent,
                                        Vat = tb.Vat == null ? 0 : tb.Vat,
                                        VatPercent = tb.VatPercent == null ? 0 : tb.VatPercent,
                                        Paid = (de.UnitPrice * de.Qty) - tb.AmountPay,
                                        Recon = tb.Recon == null ? false : tb.Recon
                                    }).ToListAsync();
                model.AddRange(model2);
            }
            return model.OrderByDescending(x => x.CreatedOn).ToList();
        }

        public async Task<List<Journal>> GetBankPaymentJournal(string BankPaymentId)
        {
            try
            {
                var bp = await context.PaymentRequest.Where(x => x.IsDeleted.Equals(false) && x.Id == BankPaymentId).FirstOrDefaultAsync();
                var tapname = "";
                if (string.IsNullOrEmpty(bp.JobId))
                {
                    tapname = "-";
                }
                else
                {
                    tapname = "" + bp.JobId;
                }

                var data = await (from tb in context.JournalTemps
                                  join coa in context.ChartOfAccounts on tb.AccountId equals coa.Id
                                  join bu in context.BusinessUnits on tb.DivisiId equals bu.Id
                                  where tb.ReferenceId == BankPaymentId
                                  orderby tb.Credit descending
                                  orderby tb.Debit descending
                                  select new Journal
                                  {
                                      AccountId = coa.Id,
                                      AccountCode = coa.CodeRec,
                                      AccountName = coa.Name5,
                                      JobId = tb.JobId,
                                      Description = tb.Description == null ? coa.Name5 : tb.Description,
                                      JobIdName = tapname,
                                      DivisiId = tb.DivisiId,
                                      DivisiName = bu.UnitName,
                                      Credit = tb.Credit,
                                      Debit = tb.Debit,
                                      LineCode = tb.LineCode,
                                  }).ToListAsync();


                return data.OrderByDescending(x => x.CreatedOn).ToList();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public async Task<int> CreateJournalWithSP(string PaymentRequestId)
        {
            try
            {
                var dataPayment = context.PaymentRequest.Where(x => x.IsDeleted.Equals(false) && x.Id == PaymentRequestId).FirstOrDefault();
                var dataBank = context.Banks.Where(x => x.IsDeleted.Equals(false) && x.Id == dataPayment.BankId).FirstOrDefault();

                await financePeriod.IsClosePeriod(Convert.ToDateTime(dataPayment.PaymentRequestDate), dataBank.LegalEntityId, Models.Enum.FinancePeriodCode.BP);
                //var modeldel = await context.JournalTemps.Where(x => x.IsDeleted.Equals(false) && x.ReferenceId == PaymentRequestId).ToListAsync();
                //if (modeldel != null)
                //{
                //    List<JournalTemp> jnd = new List<JournalTemp>();
                //    foreach (JournalTemp data in modeldel)
                //    {
                //        jnd.Add(data);
                //    }
                //    JournalTemp[] jndt = jnd.ToArray();
                //    context.PhoenixDeleteRange(jndt);
                //}
                //context.
                var result = await context.Database.ExecuteSqlCommandAsync("EXECUTE fn.create_jurnal_payment_request @p_payment_request_id", parameters: new[] { new SqlParameter("@p_payment_request_id", PaymentRequestId) });
                //var result = await context.Database.ExecuteSqlCommandAsync("EXECUTE fn.create_jurnal_bp_v2_temp @p_bank_payment_id", parameters: new[] { new SqlParameter("@p_bank_payment_id", PaymentRequestId) });
                var save = await context.SaveChangesAsync();
                return save;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public async Task<Bank> GetAccountNumberData(string Id)
        {
            //return await context.Banks.Where(x => x.IsDeleted.Equals(false) && x.Id == Id).FirstOrDefaultAsync();
            return await (from bank in context.Banks
                          join leg in context.LegalEntity on bank.LegalEntityId equals leg.Id
                          where bank.IsDeleted.Equals(false) && bank.Id == Id
                          select new Bank
                          {
                              AccountNumber = bank.AccountNumber,
                              LegalEntityId = bank.LegalEntityId,
                              LegalEntityName = leg.LegalEntityName,
                              SwiftCode = bank.SwiftCode,
                              CoaId = bank.CoaId,
                              CurrencyId = bank.CurrencyId
                          }).FirstOrDefaultAsync();
        }

        public async Task<MasterPettyCash> GetPettycashData(string Id)
        {
            return await (from pet in context.MasterPettyCashs
                          join leg in context.LegalEntity on pet.LegalEntityId equals leg.Id
                          join bank in context.Banks on pet.BankId equals bank.Id into bg
                          from bankg in bg.DefaultIfEmpty()
                          where pet.IsDeleted.Equals(false) && pet.Id == Id
                          select new MasterPettyCash
                          {
                              BankAccountNumber = pet.BankAccountNumber,
                              LegalEntityId = pet.LegalEntityId,
                              ChartAccountId = pet.ChartAccountId,
                              BankCode = bankg.BankName
                          }).FirstOrDefaultAsync();
        }

        public async Task<Bank> GetVendorData(string Id)
        {
            return await (from pet in context.Companys
                          join leg in context.Banks on pet.BankId equals leg.Id
                          where pet.IsDeleted.Equals(false) && pet.Id == Id
                          select new Bank
                          {
                              AccountNumber = pet.AccountNumber,
                              LegalEntityId = leg.LegalEntityId,
                              CoaId = leg.CoaId,
                              BankName = leg.BankName,
                              SwiftCode = leg.SwiftCode
                          }).FirstOrDefaultAsync();
        }

        public async Task<List<PaymentRequest>> GetWithFilter(PaymentRequestFilter Filter)
        {
            if (string.IsNullOrEmpty(Filter.FilterStatus))
            {
                return await (from tb in context.PaymentRequest
                              join bu in context.BusinessUnits on tb.BusinessUnitId equals bu.Id
                              join le in context.LegalEntity on tb.LegalEntityId equals le.Id into gle
                              from leg in gle.DefaultIfEmpty()
                              where tb.IsDeleted.Equals(false) && tb.Status != PaymentRequestStatus.Complete //&& (tb.Status == "5"? tb.IsBalance.Equals(true) : tb.IsBalance.Equals(false))
                              select new PaymentRequest
                              {
                                  Id = tb.Id,
                                  InvoiceReceivableId = tb.InvoiceReceivableId,
                                  InvoiceReceivableNumber = tb.InvoiceReceivableNumber,
                                  AccountId = tb.AccountId,
                                  AdvertisingTax = tb.AdvertisingTax,
                                  AffilicationId = tb.AffilicationId,
                                  BusinessUnitId = tb.BusinessUnitId,
                                  BusinessUnitName = bu.UnitName,
                                  Amount = tb.Amount,
                                  BankName = tb.BankName,
                                  CurrencyId = tb.CurrencyId,
                                  Discount = tb.Discount,
                                  BankId = tb.BankId,
                                  MainServiceCategoryId = tb.MainServiceCategoryId,
                                  SourceDoc = tb.SourceDoc,
                                  PayToVendor = tb.PayToVendor,
                                  AccountNumber = tb.AccountNumber,
                                  DueDate = tb.DueDate,
                                  Status = tb.Status,
                                  ExchangeRate = tb.ExchangeRate,
                                  LegalEntityId = tb.LegalEntityId,
                                  LegalEntityName = leg.LegalEntityName,
                                  PaymentRequestDate = tb.PaymentRequestDate,
                                  PaymentRequestNumber = tb.PaymentRequestNumber,
                                  PaymentTypeId = tb.PaymentTypeId,
                                  TaxPayable = tb.TaxPayable,
                                  TaxPayablePercent = tb.TaxPayablePercent,
                                  ReferenceNumberId = tb.ReferenceNumberId,
                                  Vat = tb.Vat,
                                  AmountPay = tb.AmountPay,
                                  VatPercent = tb.VatPercent,
                                  TotalAmount = tb.TotalAmount,
                                  PaytoName = tb.PaytoName,
                                  JobName = tb.JobId == null ? tb.PurchaseRequestId : tb.JobId,
                                  IsBalance = tb.IsBalance,
                                  TotalAmountPay = tb.TotalAmountPay

                              }).ToListAsync();
            }
            else
            {
                return await (from tb in context.PaymentRequest
                              join bu in context.BusinessUnits on tb.BusinessUnitId equals bu.Id
                              join le in context.LegalEntity on tb.LegalEntityId equals le.Id into gle
                              from leg in gle.DefaultIfEmpty()
                              where tb.IsDeleted.Equals(false) && (tb.Status == Filter.FilterStatus)
                              select new PaymentRequest
                              {
                                  Id = tb.Id,
                                  InvoiceReceivableId = tb.InvoiceReceivableId,
                                  InvoiceReceivableNumber = tb.InvoiceReceivableNumber,
                                  AccountId = tb.AccountId,
                                  AdvertisingTax = tb.AdvertisingTax,
                                  AffilicationId = tb.AffilicationId,
                                  BusinessUnitId = tb.BusinessUnitId,
                                  BusinessUnitName = bu.UnitName,
                                  Amount = tb.Amount,
                                  BankName = tb.BankName,
                                  CurrencyId = tb.CurrencyId,
                                  Discount = tb.Discount,
                                  BankId = tb.BankId,
                                  MainServiceCategoryId = tb.MainServiceCategoryId,
                                  SourceDoc = tb.SourceDoc,
                                  PayToVendor = tb.PayToVendor,
                                  AccountNumber = tb.AccountNumber,
                                  DueDate = tb.DueDate,
                                  Status = tb.Status,
                                  ExchangeRate = tb.ExchangeRate,
                                  LegalEntityId = tb.LegalEntityId,
                                  LegalEntityName = leg.LegalEntityName,
                                  PaymentRequestDate = tb.PaymentRequestDate,
                                  PaymentRequestNumber = tb.PaymentRequestNumber,
                                  PaymentTypeId = tb.PaymentTypeId,
                                  TaxPayable = tb.TaxPayable,
                                  TaxPayablePercent = tb.TaxPayablePercent,
                                  ReferenceNumberId = tb.ReferenceNumberId,
                                  Vat = tb.Vat,
                                  AmountPay = tb.AmountPay,
                                  VatPercent = tb.VatPercent,
                                  TotalAmount = tb.TotalAmount,
                                  PaytoName = tb.PaytoName,
                                  JobName = tb.JobId == null ? tb.PurchaseRequestId : tb.JobId,
                                  IsBalance = tb.IsBalance,
                                  TotalAmountPay = tb.TotalAmountPay

                              }).ToListAsync();
            }

        }

        public async Task<List<AccountPayable>> GetListAPPosted(PaymentRequestFilter Filter)
        {
            var journals = (
                       from a in context.Journal
                       where a.Credit > 0
                       group a by a.ReferenceId into b
                       select b.First()).ToList();

            var model = await (from tb in context.AccountPayables
                               join inv in context.InvoiceReceivables on tb.InvoiceReceivableId equals inv.Id
                               join bu in context.BusinessUnits on tb.BusinessUnitId equals bu.Id
                               join le in context.LegalEntity on tb.LegalEntityId equals le.Id
                               join ve in context.Companys on tb.VendorId equals ve.Id
                               join bank in context.Banks on ve.BankId equals bank.Id
                               join job in context.JobDetail on tb.JobId equals job.Id
                               join coa5 in context.ChartOfAccounts on inv.TaxPayableCoaId equals coa5.Id into gcoa
                               from gcoa5 in gcoa.DefaultIfEmpty()
                               join jou in journals on tb.Id equals jou.ReferenceId
                               where tb.IsDeleted.Equals(false) && tb.IsPayment.Equals(false) && inv.InvoiceStatusId == InvoiceReceivedStatus.Posted
                               select new AccountPayable
                               {
                                   Id = tb.Id,
                                   AccountPayableNumber = tb.AccountPayableNumber,
                                   TransactionDate = tb.TransactionDate,
                                   BusinessUnitId = tb.BusinessUnitId,
                                   LegalEntityId = tb.LegalEntityId,
                                   VendorId = tb.VendorId,
                                   Amount = tb.Amount,
                                   CurrencyId = tb.CurrencyId,
                                   BusinessUnitName = bu.UnitName,
                                   LegalEntityName = le.LegalEntityName,
                                   VendorName = ve.CompanyName,
                                   JobId = tb.JobId,
                                   JobName = tb.JobId + "-" + job.JobName,
                                   Vat = inv.ValueAddedTax,
                                   TaxPayable = inv.TaxPayable,
                                   AccountId = jou.AccountId,
                                   ExchangeRate = tb.ExchangeRate,
                                   VendorAccountNumber = ve.AccountNumber,
                                   InvoiceReceivableId = tb.InvoiceReceivableId,
                                   BankAccountDestination = bank.BankName,
                                   SwiftCode = bank.SwiftCode,
                                   TaxPayableCoaName = gcoa5.CodeRec + " -" + gcoa5.Name5,
                                   TaxPayableCoaId = inv.TaxPayableCoaId,
                                   InvoiceReceivedDate = inv.DueDate
                               }).ToListAsync();

            if (model == null) model = new List<AccountPayable>();

            var model2 = await (from tb in context.AccountPayables
                                join inv in context.InvoiceReceivables on tb.InvoiceReceivableId equals inv.Id
                                join bu in context.MasterDivision on tb.BusinessUnitId equals bu.Id
                                join le in context.LegalEntity on tb.LegalEntityId equals le.Id
                                join ve in context.Companys on tb.VendorId equals ve.Id
                                join bank in context.Banks on ve.BankId equals bank.Id
                                join job in context.JobDetail on tb.JobId equals job.Id
                                join coa5 in context.ChartOfAccounts on inv.TaxPayableCoaId equals coa5.Id into gcoa
                                from gcoa5 in gcoa.DefaultIfEmpty()
                                where tb.IsDeleted.Equals(false) && tb.IsPayment.Equals(false) && inv.InvoiceStatusId == InvoiceReceivedStatus.Posted
                                select new AccountPayable
                                {
                                    Id = tb.Id,
                                    AccountPayableNumber = tb.AccountPayableNumber,
                                    TransactionDate = tb.TransactionDate,
                                    BusinessUnitId = tb.BusinessUnitId,
                                    LegalEntityId = tb.LegalEntityId,
                                    VendorId = tb.VendorId,
                                    Amount = tb.Amount,
                                    CurrencyId = tb.CurrencyId,
                                    BusinessUnitName = bu.Name,
                                    LegalEntityName = le.LegalEntityName,
                                    VendorName = ve.CompanyName,
                                    JobId = tb.JobId,
                                    JobName = tb.JobId + "-" + job.JobName,
                                    Vat = inv.ValueAddedTax,
                                    TaxPayable = inv.TaxPayable,
                                    VendorAccountNumber = ve.AccountNumber,
                                    BankAccountDestination = bank.BankName,
                                    SwiftCode = bank.SwiftCode,
                                    TaxPayableCoaId = gcoa5.CodeRec + " -" + gcoa5.Name5,
                                    InvoiceReceivedDate = inv.DueDate
                                }).ToListAsync();
            model.AddRange(model2);

            if (!string.IsNullOrEmpty(Filter.FilterBusinessUnitId))
            {
                model = model.Where(x => x.BusinessUnitId == Filter.FilterBusinessUnitId).ToList();
            }

            if (!string.IsNullOrEmpty(Filter.FilterLegalEntity))
            {
                model = model.Where(x => x.LegalEntityId == Filter.FilterLegalEntity).ToList();
            }

            if (!string.IsNullOrEmpty(Filter.VendorId))
            {
                model = model.Where(x => x.VendorId == Filter.VendorId).ToList();
            }

            if (!string.IsNullOrEmpty(Filter.MainServiceCategory))
            {
                model = model.Where(x => x.MainserviceCategoryId == Filter.MainServiceCategory).ToList();
            }

            if (!string.IsNullOrEmpty(Filter.CurrencyId))
            {
                model = model.Where(x => x.CurrencyId == Filter.CurrencyId).ToList();
            }

            return model.OrderBy(x => x.CreatedOn).ToList();
        }

        public async Task<int> AddWithDetail(PaymentRequestDTO entity)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var isApprove = true;

                    List<PaymentRequestDetail> ad = new List<PaymentRequestDetail>();

                    if (entity.Details.Count > 0)
                    {
                        //tambah baru
                        string refId = "";
                        string jobName = "";
                        foreach (PaymentRequestDetail da in entity.Details)
                        {
                            refId += da.APNumber + ",";
                            jobName += da.JobName + ",";
                            da.Id = Guid.NewGuid().ToString();
                            da.PaymentRequestId = entity.Header.Id;
                            da.PairingGroupId = entity.Header.PairingGroupId;

                            ad.Add(da);
                            var apdata = await context.AccountPayables.Where(x => x.Id == da.ReferenceId).FirstOrDefaultAsync();
                            if (apdata != null)
                            {
                                apdata.IsPayment = true;
                                entity.Header.LegalEntityId = apdata.LegalEntityId;
                                context.PhoenixEdit(apdata);
                            }
                        }
                        refId.Remove(refId.Length - 1);
                        jobName.Remove(jobName.Length - 1);
                        entity.Header.ReferenceNumberId = refId;
                        entity.Header.JobId = jobName;
                        if (ad.Count > 1)
                        {
                            entity.Header.IsMultiple = true;
                        }
                        else
                        {
                            entity.Header.IsMultiple = false;
                        }
                    }
                    else
                    {
                        entity.Header.ReferenceNumberId = "";
                    }

                    //await context.PhoenixAddAsync(entity.Header);
                    await globalFcApproval.UnsafeSubmitApproval<PaymentRequest>(false, entity.Header.Id, int.Parse(entity.Header.Status), $"{ApprovalLink.PaymentRequest}?Id={entity.Header.Id}&isApprove=true", idTemplate, MenuUnique.PaymentRequest, entity.Header);
                    if (entity.Details.Count > 0)
                    {
                        PaymentRequestDetail[] ada = ad.ToArray();
                        await context.PhoenixAddRangeAsync(ada);
                    }

                    ////approval process request to system
                    //var dataMenu = await menuService.GetByUniqueName(MenuUnique.PaymentRequest).ConfigureAwait(false);
                    //var vm = new Models.ViewModel.TransApprovalHrisVm()
                    //{
                    //    MenuId = dataMenu.Id,
                    //    RefId = entity.Header.Id,
                    //    DetailLink = $"{ApprovalLink.PaymentRequest}?Id={entity.Header.Id}&isApprove=true",
                    //    //Tname = "fn.PurchaseRequest",//"SP#NAMA SP NYA",
                    //    IdTemplate = idTemplate
                    //};
                    //var subGroupId = ClainJwtPrincipalHelper.GetBusinessSubGroup(context.HttpContext);
                    //var divisionId = ClainJwtPrincipalHelper.GetBusinessUnitDivisi(context.HttpContext);
                    //var employeeId = ClainJwtPrincipalHelper.GetEmployeeId(context.HttpContext);
                    //isApprove = await globalFcApproval.Save(vm, subGroupId, divisionId, employeeId);


                    //if (isApprove)
                    //{
                    await log.AddAsync(new StatusLog() { TransactionId = entity.Header.Id, Status = PaymentRequestStatus.GetStatus(entity.Header.Status), Description = entity.Header.LogDescription });
                    var save = await context.SaveChangesAsync();
                    transaction.Commit();
                    return save;
                    //}
                    //else
                    //{
                    //    transaction.Rollback();
                    //    throw new Exception("please check the approval template that will be processed");
                    //}
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message.ToString());
                }
            }
        }

        public async Task<int> EditWithDetail(PaymentRequestDTO entity)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var trapprovalmodel = await context.TrTemplateApprovals.Where(x => x.RefId == entity.Header.Id).FirstOrDefaultAsync();
                    if (trapprovalmodel == null)
                    {
                        await globalFcApproval.UnsafeSubmitApproval<PaymentRequest>(true, entity.Header.Id, int.Parse(entity.Header.Status), $"{ApprovalLink.PaymentRequest}?Id={entity.Header.Id}&isApprove=true", idTemplate, MenuUnique.PaymentRequest, entity.Header);
                    }
                    else
                    {
                        context.PhoenixEdit(entity.Header);
                    }

                    if (entity.Details.Count > 0)
                    {

                        //remove existing data
                        var delmod = await context.PaymentRequestDetail.Where(x => x.IsDeleted.Equals(false) && x.PaymentRequestId == entity.Header.Id).ToListAsync();
                        if (delmod.Count > 0)
                        {
                            context.PaymentRequestDetail.RemoveRange(delmod);
                        }

                        //validate again, if still exist additional data after removerange

                        List<PaymentRequestDetail> pd = new List<PaymentRequestDetail>();
                        foreach (PaymentRequestDetail data in entity.Details)
                        {
                            //data.Id = Guid.NewGuid().ToString();
                            data.PaymentRequestId = entity.Header.Id;
                            pd.Add(data);
                        }

                        PaymentRequestDetail[] dtg = pd.ToArray();
                        await context.PhoenixAddRangeAsync(dtg);
                    }

                    //var trapprovalmodel = await context.TrTemplateApprovals.Where(x => x.RefId == entity.Header.Id).FirstOrDefaultAsync();
                    //if (trapprovalmodel == null)
                    //{
                    //    var isApprove = true;
                    //    var dataMenu = await menuService.GetByUniqueName(MenuUnique.PaymentRequest).ConfigureAwait(false);
                    //    var vm = new Models.ViewModel.TransApprovalHrisVm()
                    //    {
                    //        MenuId = dataMenu.Id,
                    //        RefId = entity.Header.Id,
                    //        DetailLink = $"{ApprovalLink.PaymentRequest}?Id={entity.Header.Id}&isApprove=true",
                    //        //Tname = "fn.PurchaseRequest",//"SP#NAMA SP NYA",
                    //        IdTemplate = idTemplate
                    //    };
                    //    var subGroupId = ClainJwtPrincipalHelper.GetBusinessSubGroup(context.HttpContext);
                    //    var divisionId = ClainJwtPrincipalHelper.GetBusinessUnitDivisi(context.HttpContext);
                    //    var employeeId = ClainJwtPrincipalHelper.GetEmployeeId(context.HttpContext);
                    //    isApprove = await globalFcApproval.Save(vm, subGroupId, divisionId, employeeId);


                    //    if (isApprove)
                    //    {
                    //        var save = await context.SaveChangesAsync();
                    //        transaction.Commit();
                    //        return save;
                    //    }
                    //    else
                    //    {
                    //        transaction.Rollback();
                    //        throw new Exception("please check the approval template that will be processed");
                    //    }

                    //}
                    //else
                    //{
                    await log.AddAsync(new StatusLog() { TransactionId = entity.Header.Id, Status = PaymentRequestStatus.GetStatus(entity.Header.Status), Description = entity.Header.LogDescription });
                    var save = await context.SaveChangesAsync();
                    transaction.Commit();
                    return save;
                    // }

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message.ToString());
                }
            }
        }

        public async Task<List<AccountPayable>> GetListDetailPaymentRequestByPairingGroupId(string Id)
        {
            var model = await (from tb in context.PaymentRequestDetail
                               join ta in context.AccountPayables on tb.ReferenceId equals ta.Id
                               join inv in context.InvoiceReceivables on ta.InvoiceReceivableId equals inv.Id
                               join bu in context.BusinessUnits on ta.BusinessUnitId equals bu.Id
                               join le in context.LegalEntity on ta.LegalEntityId equals le.Id
                               join ve in context.Companys on ta.VendorId equals ve.Id
                               where tb.IsDeleted.Equals(false) && tb.PairingGroupId == Id
                               select new AccountPayable
                               {
                                   Id = ta.Id,
                                   AccountPayableNumber = ta.AccountPayableNumber,
                                   TransactionDate = ta.TransactionDate,
                                   BusinessUnitId = ta.BusinessUnitId,
                                   LegalEntityId = ta.LegalEntityId,
                                   VendorId = ta.VendorId,
                                   Amount = tb.PayUnitPrice,
                                   CurrencyId = ta.CurrencyId,
                                   BusinessUnitName = bu.UnitName,
                                   LegalEntityName = le.LegalEntityName,
                                   VendorName = ve.CompanyName,
                                   JobId = tb.JobId,
                                   Vat = inv.ValueAddedTax,
                                   TaxPayable = inv.TaxPayable

                               }).ToListAsync();

            if (model == null) model = new List<AccountPayable>();

            var model2 = await (from tb in context.PaymentRequestDetail
                                join ta in context.AccountPayables on tb.ReferenceId equals ta.Id
                                join inv in context.InvoiceReceivables on ta.InvoiceReceivableId equals inv.Id
                                join bu in context.MasterDivision on ta.BusinessUnitId equals bu.Id
                                join le in context.LegalEntity on ta.LegalEntityId equals le.Id
                                join ve in context.Companys on ta.VendorId equals ve.Id
                                where tb.IsDeleted.Equals(false) && ta.IsPayment.Equals(false) && inv.InvoiceStatusId == InvoiceReceivedStatus.Posted
                                select new AccountPayable
                                {
                                    Id = tb.Id,
                                    AccountPayableNumber = ta.AccountPayableNumber,
                                    TransactionDate = ta.TransactionDate,
                                    BusinessUnitId = ta.BusinessUnitId,
                                    LegalEntityId = ta.LegalEntityId,
                                    VendorId = ta.VendorId,
                                    Amount = tb.PayUnitPrice,
                                    CurrencyId = tb.CurrencyId,
                                    BusinessUnitName = bu.Name,
                                    LegalEntityName = le.LegalEntityName,
                                    VendorName = ve.CompanyName,
                                    JobId = tb.JobId,
                                    Vat = inv.ValueAddedTax,
                                    TaxPayable = inv.TaxPayable

                                }).ToListAsync();
            model.AddRange(model2);

            return model.OrderBy(x => x.CreatedOn).ToList();
        }


        public async Task<List<AccountPayable>> GetListDetailPaymentRequest(string Id)
        {
            var model = await (from tb in context.PaymentRequestDetail
                               join ta in context.AccountPayables on tb.ReferenceId equals ta.Id
                               join inv in context.InvoiceReceivables on ta.InvoiceReceivableId equals inv.Id
                               join bu in context.BusinessUnits on ta.BusinessUnitId equals bu.Id
                               join job in context.JobDetail on tb.JobId equals job.Id
                               join le in context.LegalEntity on ta.LegalEntityId equals le.Id into leg
                               from legg in leg.DefaultIfEmpty()
                               join ve in context.Companys on ta.VendorId equals ve.Id
                               join coa5 in context.ChartOfAccounts on inv.TaxPayableCoaId equals coa5.Id into gcoa
                               from gcoa5 in gcoa.DefaultIfEmpty()
                               join bank in context.Banks on ve.BankId equals bank.Id into gbank
                               from banks in gbank.DefaultIfEmpty()
                               where tb.IsDeleted.Equals(false) && tb.PaymentRequestId == Id
                               select new AccountPayable
                               {
                                   Id = ta.Id,
                                   AccountPayableNumber = ta.AccountPayableNumber,
                                   TransactionDate = ta.TransactionDate,
                                   BusinessUnitId = ta.BusinessUnitId,
                                   LegalEntityId = ta.LegalEntityId,
                                   VendorId = ta.VendorId,
                                   Amount = tb.PayUnitPrice,
                                   CurrencyId = ta.CurrencyId,
                                   BusinessUnitName = bu.UnitName,
                                   LegalEntityName = legg.LegalEntityName,
                                   VendorName = ve.CompanyName,
                                   JobId = tb.JobId,
                                   JobName = tb.JobId + "-" + job.JobName,
                                   Vat = inv.ValueAddedTax,
                                   TaxPayable = inv.TaxPayable,
                                   AccountId = tb.AccountId,
                                   ExchangeRate = ta.ExchangeRate,
                                   VendorAccountNumber = ve.AccountNumber,
                                   InvoiceReceivableId = ta.InvoiceReceivableId,
                                   BankAccountDestination = banks.BankName,
                                   SwiftCode = banks.SwiftCode,
                                   TaxPayableCoaName = gcoa5.CodeRec + " -" + gcoa5.Name5,
                                   TaxPayableCoaId = inv.TaxPayableCoaId,
                                   InvoiceReceivedDate = inv.DueDate
                               }).ToListAsync();

            if (model == null) model = new List<AccountPayable>();

            var model2 = await (from tb in context.PaymentRequestDetail
                                join ta in context.AccountPayables on tb.ReferenceId equals ta.Id
                                join inv in context.InvoiceReceivables on ta.InvoiceReceivableId equals inv.Id
                                join bu in context.MasterDivision on ta.BusinessUnitId equals bu.Id
                                join job in context.JobDetail on tb.JobId equals job.Id
                                join le in context.LegalEntity on ta.LegalEntityId equals le.Id into leg
                                from legg in leg.DefaultIfEmpty()
                                join ve in context.Companys on ta.VendorId equals ve.Id
                                join coa5 in context.ChartOfAccounts on inv.TaxPayableCoaId equals coa5.Id into gcoa
                                from gcoa5 in gcoa.DefaultIfEmpty()
                                join bank in context.Banks on ve.BankId equals bank.Id into gbank
                                from banks in gbank.DefaultIfEmpty()
                                where tb.IsDeleted.Equals(false) && ta.IsPayment.Equals(false) && inv.InvoiceStatusId == InvoiceReceivedStatus.Posted
                                select new AccountPayable
                                {
                                    Id = tb.Id,
                                    AccountPayableNumber = ta.AccountPayableNumber,
                                    TransactionDate = ta.TransactionDate,
                                    BusinessUnitId = ta.BusinessUnitId,
                                    LegalEntityId = ta.LegalEntityId,
                                    VendorId = ta.VendorId,
                                    Amount = tb.PayUnitPrice,
                                    CurrencyId = tb.CurrencyId,
                                    BusinessUnitName = bu.Name,
                                    LegalEntityName = legg.LegalEntityName,
                                    VendorName = ve.CompanyName,
                                    JobId = tb.JobId,
                                    JobName = tb.JobId + "-" + job.JobName,
                                    Vat = inv.ValueAddedTax,
                                    TaxPayable = inv.TaxPayable,
                                    AccountId = tb.AccountId,
                                    ExchangeRate = ta.ExchangeRate,
                                    VendorAccountNumber = ve.AccountNumber,
                                    InvoiceReceivableId = ta.InvoiceReceivableId,
                                    BankAccountDestination = banks.BankName,
                                    SwiftCode = banks.SwiftCode,
                                    TaxPayableCoaName = gcoa5.CodeRec + " -" + gcoa5.Name5,
                                    TaxPayableCoaId = inv.TaxPayableCoaId,
                                    InvoiceReceivedDate = inv.DueDate
                                }).ToListAsync();
            model.AddRange(model2);

            return model.OrderBy(x => x.CreatedOn).ToList();
        }

        public async Task<List<PaymentRequestDetail>> GetDetail(string Id)
        {
            return await context.PaymentRequestDetail.Where(x => x.IsDeleted.Equals(false) && x.PaymentRequestId == Id).ToListAsync();
        }

        public async Task<List<PaymentRequest>> GetHistoryPayment(string Id)
        {
            var dataHis = await (from a in context.PaymentRequest
                                 join b in (from dtl in context.PaymentRequestDetail
                                            group dtl by new { dtl.PaymentRequestId } into gdetail
                                            select new { idHeader = gdetail.FirstOrDefault().PairingGroupId, UnitPrice = gdetail.Sum(a => a.PayUnitPrice) }) on a.PairingGroupId equals b.idHeader into dtlGrouped
                                 where a.IsDeleted.Equals(false) && a.PairingGroupId == Id
                                 select new PaymentRequest
                                 {
                                     PaymentRequestDate = a.PaymentRequestDate,
                                     PaymentRequestNumber = a.PaymentRequestNumber,
                                     TotalAmount = dtlGrouped.FirstOrDefault().UnitPrice,
                                     AmountPay = a.AmountPay,
                                     TaxPayable = a.TaxPayable,
                                     Balance = a.Balance,
                                     Charges = a.Charges
                                 }).ToListAsync();
            //if (dataHis.Count() > 0)
            //    if (dataHis[0].Balance == 0)
            //        dataHis = null;

            return dataHis;
        }
    }
}
