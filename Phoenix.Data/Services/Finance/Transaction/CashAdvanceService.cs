﻿using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Attributes;
using Phoenix.Data.Models;
using Phoenix.Data.Models.Finance;
using Phoenix.Data.Models.Finance.Transaction;
using Phoenix.Data.Models.Finance.Transaction.PaymentRequest;
using Phoenix.Data.Services.Um;
using Phoenix.Shared.Core.PrincipalHelpers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.Data.Services.Finance
{
    public interface ICashAdvanceService : IDataService<CashAdvance>
    {
        Task<List<CashAdvance>> Get(Dictionary<string, string> filters);
        Task<int> ApproveAsync(CashAdvance entity);
        Task<List<CashAdvanceDetail>> GetDetails(string id);
        Task<List<CashAdvance>> GetApprovalList();
        Task<int> CheckApproval(CashAdvance entity);
        Task<CashAdvance> Approve(CashAdvance entity);
        //Task<CashAdvanceSettlement> ApproveSattlement(CashAdvanceSettlement entity);
        Task<CashAdvance> Reject(CashAdvance entity);
        //Task<int> AddSattlement(CashAdvanceSattlementDTO model);
        //Task<int> EditSattlement(CashAdvanceSattlementDTO model);
        //Task<CashAdvanceSettlement> GetSattlementById(string CaId);
        //Task<List<CashAdvanceSettlementDetail>> GetSattlementDetailById(string Id);
        //Task<List<Journal>> GetJournalDataBySattlementId(string Id);//pindah settlement
        Task<List<Journal>> GetJournalDataId(string Id);
        //Task<int> CreateAutomaticJournalFromSattlementId(string Id);
        Task<CashAdvance> GetByCaNumber(string Id);
        Task<JobDetail> GetJobDetail(string Id);
        Task<Uom> GetUom(string Id);
        Task<CashAdvance> GetCompanyInfo(string companyid);
    }

    public class CashAdvanceService : ICashAdvanceService
    {
        readonly DataContext context;
        readonly GlobalFunctionApproval globalFcApproval;
        readonly IManageMenuService menuService;
        readonly FinancePeriodService financePeriod;
        string idTemplate = "";
        readonly StatusLogService log;
        /// <summary>
        /// And endpoint to manage CashAdvance
        /// </summary>
        /// <param name="context">Database context</param>
        public CashAdvanceService(DataContext context, GlobalFunctionApproval globalFcApproval, IManageMenuService menuService, FinancePeriodService financePeriod, StatusLogService log)
        {
            this.context = context;
            this.globalFcApproval = globalFcApproval;
            this.menuService = menuService;
            this.financePeriod = financePeriod;
            this.log = log;
        }

        public async Task<int> AddAsync(CashAdvance entity)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    //await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.CaDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.CAS);
                    entity.CaNumber = await TransID.GetTransId(context, Code.CashAdvance, DateTime.Today);
                    await globalFcApproval.UnsafeSubmitApproval<CashAdvance>(false, entity.Id, int.Parse(entity.CaStatusId), $"{ApprovalLink.CashAdvance}?Id={entity.Id}&isApprove=true", idTemplate, MenuUnique.CashAdvance, entity);

                    if (entity.CashAdvanceDetails != null)
                    {
                        //remove existing data
                        var additional = await context.CashAdvanceDetails.AsNoTracking().Where(x => x.CashAdvanceId == entity.Id).ToListAsync();
                        if (additional.Count > 0)
                        {
                            context.CashAdvanceDetails.RemoveRange(additional);
                        }

                        //validate again, if still exist additional data after removerange
                        if (entity.CashAdvanceDetails != null)
                        {
                            Parallel.ForEach(entity.CashAdvanceDetails, (item) =>
                            {
                                item.Id = Guid.NewGuid().ToString();
                                item.CashAdvanceId = entity.Id;
                                context.PhoenixAddAsync(item);
                            });
                        }
                    }
                    await log.AddAsync(new StatusLog() { TransactionId = entity.Id, Status = StatusTransaction.StatusName(Convert.ToInt32(entity.CaStatusId)), Description = entity.LogDescription });
                    var save = await context.SaveChangesAsync();
                    transaction.Commit();
                    return save;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        //public async Task<int> AddSattlement(CashAdvanceSattlementDTO model)
        //{
        //    using (var transaction = await context.Database.BeginTransactionAsync())
        //    {
        //        try
        //        {
        //            var modelca = await context.CashAdvances.AsNoTracking().Where(x => x.Id == model.Header.CashAdvanceId).FirstOrDefaultAsync();
        //            //await financePeriod.IsClosePeriod(Convert.ToDateTime(modelca.CaDate), modelca.LegalEntityId, Models.Enum.FinancePeriodCode.CAS);

        //            model.Header.Id = Guid.NewGuid().ToString();
        //            model.Header.SattlementNumber = await TransID.GetTransId(context, Code.CashAdvanceSattlement, DateTime.Today);
        //            await globalFcApproval.UnsafeSubmitApproval<CashAdvanceSettlement>(false, model.Header.Id, int.Parse(model.Header.CaStatusId), $"{ApprovalLink.CashAdvance}?Id={model.Header.Id}&isApprove=true", idTemplate, MenuUnique.CashAdvance, model.Header);

        //            modelca.CaStatusId = model.Header.CaStatusId;
        //            context.PhoenixEdit(modelca);

        //            if (model.Details != null)
        //            {
        //                //remove existing data
        //                var additional = await context.CashAdvanceSettlementDetail.AsNoTracking().Where(x => x.CashAdvanceSettleId == model.Header.Id).ToListAsync();
        //                if (additional.Count > 0)
        //                {
        //                    context.CashAdvanceSettlementDetail.RemoveRange(additional);
        //                }


        //                //validate again, if still exist additional data after removerange
        //                if (model.Details != null)
        //                {
        //                    List<CashAdvanceSettlementDetail> cast = new List<CashAdvanceSettlementDetail>();
        //                    foreach (CashAdvanceSettlementDetail data in model.Details)
        //                    {

        //                        data.CashAdvanceSettleId = model.Header.Id;
        //                        data.Qty = data.ActualQty;
        //                        data.AccountId = "4415002503333080";
        //                        data.AccountCode = "2.1.01.01.01";
        //                        data.ActualAmount = data.Qty * data.ActualUnitPrice;
        //                        data.Id = Guid.NewGuid().ToString();
        //                        cast.Add(data);
        //                    }

        //                    CashAdvanceSettlementDetail[] npo = cast.ToArray();
        //                    await context.PhoenixAddRangeAsync(npo);
        //                }

        //            }

        //            var save = await context.SaveChangesAsync();
        //            transaction.Commit();
        //            return save;
        //        }
        //        catch (Exception ex)
        //        {
        //            transaction.Rollback();
        //            throw new Exception(ex.Message);
        //        }
        //    }
        //}

        public async Task<int> AddRangeAsync(params CashAdvance[] entities)
        {
            foreach (var entity in entities) await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.CaDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.CAS);
            await context.PhoenixAddRangeAsync(entities);
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteAsync(CashAdvance entity)
        {
            await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.CaDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.CAS);
            context.PhoenixDelete(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteRageAsync(params CashAdvance[] entities)
        {
            foreach (var entity in entities) await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.CaDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.CAS);
            context.PhoenixDeleteRange(entities);
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteSoftAsync(CashAdvance entity)
        {
            await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.CaDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.CAS);
            context.PhoenixDelete(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteSoftRangeAsync(params CashAdvance[] entities)
        {
            foreach (var entity in entities) await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.CaDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.CAS);
            context.PhoenixDeleteRange(entities);
            return await context.SaveChangesAsync();
        }

        public async Task<int> EditAsync(CashAdvance entity)
        {
            //await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.CaDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.CAS);
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    context.PhoenixEdit(entity);
                    if (entity.CashAdvanceDetails != null)
                    {

                        var additional = await context.CashAdvanceDetails.AsNoTracking().Where(x => x.CashAdvanceId == entity.Id).ToListAsync();
                        if (additional.Count > 0)
                        {
                            context.CashAdvanceDetails.RemoveRange(additional);
                        }

                        List<CashAdvanceDetail> dt = new List<CashAdvanceDetail>();
                        foreach (CashAdvanceDetail data in entity.CashAdvanceDetails)
                        {
                            data.Id = Guid.NewGuid().ToString();
                            data.CashAdvanceId = entity.Id;
                            dt.Add(data);
                        }

                        CashAdvanceDetail[] dtg = dt.ToArray();
                        await context.PhoenixAddRangeAsync(dtg);
                        //foreach (var item in entity.CashAdvanceDetails)
                        //{
                        //    var dtl = await context.CashAdvanceDetails.AsNoTracking().Where(a => a.Id == item.Id).FirstOrDefaultAsync();

                        //    if (dtl == null)
                        //    {
                        //        item.Id = Guid.NewGuid().ToString();
                        //        item.CashAdvanceId = entity.Id;
                        //        context.PhoenixAdd(item);
                        //    }
                        //    else
                        //    {
                        //        context.PhoenixEdit(item);
                        //    }
                        //}
                    }

                    var trapprovalmodel = await context.TrTemplateApprovals.AsNoTracking().Where(x => x.RefId == entity.Id).FirstOrDefaultAsync();
                    if (trapprovalmodel == null)
                    {
                        var isApprove = true;
                        var dataMenu = await menuService.GetByUniqueName(MenuUnique.CashAdvanceSettlement).ConfigureAwait(false);
                        var vm = new Models.ViewModel.TransApprovalHrisVm()
                        {
                            MenuId = dataMenu.Id,
                            RefId = entity.Id,
                            DetailLink = $"{ApprovalLink.CashAdvance}?Id={entity.Id}&isApprove=true",
                            //Tname = "fn.PurchaseRequest",//"SP#NAMA SP NYA",
                            IdTemplate = idTemplate
                        };
                        var subGroupId = ClainJwtPrincipalHelper.GetBusinessSubGroup(context.HttpContext);
                        var divisionId = ClainJwtPrincipalHelper.GetBusinessUnitDivisi(context.HttpContext);
                        var employeeId = ClainJwtPrincipalHelper.GetEmployeeId(context.HttpContext);
                        isApprove = await globalFcApproval.Save(vm, subGroupId, divisionId, employeeId);


                        if (isApprove)
                        {
                            await log.AddAsync(new StatusLog() { TransactionId = entity.Id, Status = StatusTransaction.StatusName(Convert.ToInt32(entity.CaStatusId)), Description = entity.LogDescription });
                            var save = await context.SaveChangesAsync();
                            transaction.Commit();
                            return save;
                        }
                        else
                        {
                            transaction.Rollback();
                            throw new Exception("please check the approval template that will be processed");
                        }

                    }
                    else
                    {
                        var save = await context.SaveChangesAsync();
                        transaction.Commit();
                        return save;
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message.ToString());
                }
            }

        }

        public async Task<int> CheckApproval(CashAdvance entity)
        {
            try
            {
                //await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.CaDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.CAS);
                var modelApproval = await context.TrTemplateApprovals.AsNoTracking().Where(x => x.RefId == entity.Id).FirstOrDefaultAsync();
                if (modelApproval != null)
                {
                    if (modelApproval.StatusApproved == Models.Um.StatusApproved.Approved)
                    {
                        PaymentRequest pay = new PaymentRequest();
                        pay.Id = Guid.NewGuid().ToString();
                        pay.PaymentRequestDate = DateTime.Now;
                        pay.PaymentRequestNumber = await TransID.GetTransId(context, Code.PaymentRequest, DateTime.Today);
                        pay.Status = PaymentRequestStatus.Open;
                        pay.LegalEntityId = entity.LegalEntityId;
                        pay.BusinessUnitId = entity.BusinessUnitId;
                        pay.PaymentTypeId = "1b5ac6a6-a871-498b-82d4-8f8c647f51c4";
                        pay.Vat = 0;
                        pay.DueDate = entity.CaPeriodTo;
                        pay.PaytoName = entity.VendorName;
                        pay.BankName = entity.PayeeBankId;
                        pay.TaxPayable = 0;
                        pay.VatPercent = 0;
                        pay.TaxPayablePercent = 0;
                        pay.ExchangeRate = 1;
                        pay.CurrencyId = "46e4d57e-5ce2-4c31-82be-f81382dc243a";
                        pay.SourceDoc = "CA";
                        var detailca = await context.CashAdvanceDetails.Where(x => x.IsDeleted.Equals(false) && x.CashAdvanceId == entity.Id).ToListAsync();
                        decimal? ttl = 0;
                        foreach (CashAdvanceDetail data in detailca)
                        {
                            ttl += data.Amount;
                        }

                        pay.TotalAmount = ttl;
                        pay.Amount = ttl;
                        pay.AdvertisingTax = 0;
                        pay.Discount = 0;
                        pay.JobId = entity.JobId;
                        pay.ReferenceNumberId = entity.CaNumber;
                        entity.CaStatusId = StatusTransaction.Approved.ToString();
                        entity.StatusDesc = StatusTransaction.StatusName(StatusTransaction.Approved);
                        await context.PhoenixAddAsync(pay);

                    }
                    else if (modelApproval.StatusApproved == Models.Um.StatusApproved.Rejected)
                    {
                        entity.CaStatusId = StatusTransaction.Rejected.ToString();
                        entity.StatusDesc = StatusTransaction.StatusName(StatusTransaction.Rejected);

                    }

                    context.PhoenixEdit(entity);

                }


                var save = await context.SaveChangesAsync();
                return save;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }

        }

        public async Task<int> EditRangeAsync(CashAdvance entity)
        {
            await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.CaDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.CAS);
            context.PhoenixEdit(entity);
            return await context.SaveChangesAsync();
        }

        //public async Task<List<CashAdvance>> Get() => await context.CashAdvances.AsNoTracking().Where(x => !(x.IsDeleted ?? false)).ToListAsync();

        public async Task<List<CashAdvance>> Get()
        {
            //yang bisa akses ke pca adalah pm team, creator dan approval
            var userId = context.GetUserId();
            var employeeId = context.GetEmployeeId();

            var team = await (from a in context.PmTeams
                              join b in context.PmTeamMembers on a.Id equals b.TeamId
                              where !(a.IsDeleted ?? false) && !(b.IsDeleted ?? false) && b.ApplicationUserId == userId
                              select new { a.JobId }).ToListAsync();

            var approval = await (from a in context.TrTemplateApprovals
                                  join b in context.TrUserApprovals on a.Id equals b.TrTempApprovalId
                                  where b.EmployeeBasicInfoId == employeeId
                                  select new { a.RefId }).ToListAsync();


            var result = await (
                from ca in context.CashAdvances
                join b in context.EmployeeBasicInfos on ca.RequestedBy equals b.Id
                join c in context.TrTemplateApprovals on ca.Id equals c.RefId
                join comp in context.Companys on ca.ClientName equals comp.Id into compTemp
                where !(ca.IsDeleted ?? false)
                from comps in compTemp.DefaultIfEmpty()
                select new CashAdvance
                {
                    Id = ca.Id,
                    CaNumber = ca.CaNumber,
                    CaDate = ca.CaDate,
                    Revision = ca.Revision,
                    RequestedBy = ca.RequestedBy,
                    RequestedOn = ca.RequestedOn,
                    BusinessUnitId = ca.BusinessUnitId,
                    LegalEntityId = ca.LegalEntityId,
                    IsProjectCost = ca.IsProjectCost,
                    PurchaseOrderId = ca.PurchaseOrderId,
                    PurchaseOrderNumber = ca.PurchaseOrderNumber,
                    JobId = ca.JobId,
                    JobNumber = ca.JobNumber,
                    ClientName = comps.CompanyName,
                    ProjectActivity = ca.ProjectActivity,
                    ProjectArea = ca.ProjectArea,
                    VendorId = ca.VendorId,
                    VendorName = ca.VendorName,
                    EmployeeId = ca.EmployeeId,
                    Notes = ca.Notes,
                    PayeeBankId = ca.PayeeBankId,
                    PayeeAccountNumber = ca.PayeeAccountNumber,
                    PayeeAccountName = ca.PayeeAccountName,
                    PayeeBankBranch = ca.PayeeBankBranch,
                    CaPeriodFrom = ca.CaPeriodFrom,
                    CaPeriodTo = ca.CaPeriodTo,
                    ApprovalId = ca.ApprovalId,
                    CaStatusId = ca.CaStatusId,
                    StatusDesc = c.StatusApprovedDescription,
                    RequestEmployee = b
                }
                                ).Where(x => (x.CreatedBy == userId || x.RequestedBy == employeeId || approval.Any(y => y.RefId == x.Id)))
                                .OrderByDescending(x => x.CreatedOn).ToListAsync();
            return result;
        }

        public async Task<List<CashAdvance>> Get(Dictionary<string, string> filters)
        {
            var userId = context.GetUserId();
            var employeeId = context.GetEmployeeId();

            var team = await (from a in context.PmTeams
                              join b in context.PmTeamMembers on a.Id equals b.TeamId
                              where !(a.IsDeleted ?? false) && !(b.IsDeleted ?? false) && b.ApplicationUserId == userId
                              select new { a.JobId }).ToListAsync();

            var approval = await (from a in context.TrTemplateApprovals
                                  join b in context.TrUserApprovals on a.Id equals b.TrTempApprovalId
                                  where b.EmployeeBasicInfoId == employeeId
                                  select new { a.RefId }).ToListAsync();


            var result = from ca in context.CashAdvances
                         join b in context.EmployeeBasicInfos on ca.RequestedBy equals b.Id
                         join c in context.TrTemplateApprovals on ca.Id equals c.RefId
                         join job in context.JobDetail on ca.JobId equals job.Id
                         join legal in context.LegalEntity on ca.LegalEntityId equals legal.Id into lca
                         from legal in lca.DefaultIfEmpty()
                         join div in context.BusinessUnits on ca.BusinessUnitId equals div.Id into dca
                         from div in dca.DefaultIfEmpty()
                         join divReq in context.BusinessUnits on ca.RequestBusinessUnitId equals divReq.Id into drca
                         from divReq in drca.DefaultIfEmpty()
                         join comp in context.Companys on ca.ClientName equals comp.Id into compTemp
                         join mservice in context.MainServiceCategorys on ca.ProjectActivity equals mservice.Id into gmservice
                         from mservice in gmservice.DefaultIfEmpty()
                         where !(ca.IsDeleted ?? false)
                         from comps in compTemp.DefaultIfEmpty()
                         select new CashAdvance
                         {
                             Id = ca.Id,
                             CaNumber = ca.CaNumber,
                             CaDate = ca.CaDate,
                             Revision = ca.Revision,
                             RequestedBy = ca.RequestedBy,
                             RequestedOn = ca.RequestedOn,
                             BusinessUnitId = ca.BusinessUnitId,
                             LegalEntityId = ca.LegalEntityId,
                             IsProjectCost = ca.IsProjectCost,
                             PurchaseOrderId = ca.PurchaseOrderId,
                             PurchaseOrderNumber = ca.PurchaseOrderNumber,
                             JobId = ca.JobId,
                             JobNumber = ca.JobNumber,
                             ClientName = comps.CompanyName,
                             ProjectActivity = ca.ProjectActivity,
                             ProjectArea = ca.ProjectArea,
                             VendorId = ca.VendorId,
                             VendorName = ca.VendorName,
                             EmployeeId = ca.EmployeeId,
                             Notes = ca.Notes,
                             PayeeBankId = ca.PayeeBankId,
                             RequestEmployeeId = ca.RequestEmployeeId,
                             RequestEmployeeName = ca.RequestEmployeeName,
                             RemarkRejected = ca.RemarkRejected,
                             RequestJobGradeId = ca.RequestJobGradeId,
                             RequestJobGradeName = ca.RequestJobGradeName,
                             RequestJobTitleId = ca.RequestJobTitleId,
                             RequestJobTitleName = ca.RequestJobTitleName,
                             PayeeAccountNumber = ca.PayeeAccountNumber,
                             PayeeAccountName = ca.PayeeAccountName,
                             PayeeBankBranch = ca.PayeeBankBranch,
                             CaPeriodFrom = ca.CaPeriodFrom,
                             CaPeriodTo = ca.CaPeriodTo,
                             ApprovalId = ca.ApprovalId,
                             CaStatusId = ca.CaStatusId,
                             StatusDesc = c.StatusApprovedDescription,
                             RequestEmployee = b,
                             BusinessUnitName = div == null ? "" : div.UnitName,
                             RequestBusinessUnitName = divReq == null ? "" : divReq.UnitName,
                             LegalEntityName = legal == null ? "" : legal.LegalEntityName,
                             MainServiceCategoryName = mservice == null? "" : mservice.Name,
                             JobName = job.Id + " - " +job.JobName

                         };
            if (filters != null && filters.Count > 0)
            {
                foreach (var filter in filters)
                {
                    switch (filter.Key)
                    {
                        case "legal":
                            result = result.Where(x => x.LegalEntityId == filter.Value);
                            break;
                        case "svc":
                            result = result.Where(x => x.ProjectActivity == filter.Value);
                            break;
                    }
                }
            }
            //return await result.Where(x => ( x.RequestEmployeeId == employeeId || approval.Any(y => y.RefId == x.Id))).OrderByDescending(x => x.CreatedOn).ToListAsync();
            return await result
                .OrderByDescending(x => x.CreatedOn).ToListAsync();
        }


        public Task<List<CashAdvance>> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public async Task<List<CashAdvanceDetail>> GetByTask(string taskId)
        {
            var result = await (from cad in context.CashAdvanceDetails
                                join c in context.TrTemplateApprovals on cad.CashAdvanceId equals c.RefId
                                where !(cad.IsDeleted ?? false)
                                select new CashAdvanceDetail
                                {
                                    Id = cad.Id,
                                    UnitPrice = cad.UnitPrice,
                                    Qty = cad.Qty,
                                    TaskId = cad.TaskId,
                                    Amount = cad.Amount,
                                }
                                ).AsNoTracking().ToListAsync();
            return result;
        }


        public async Task<CashAdvance> Get(string Id)
        {
            var result = await (from ca in context.CashAdvances
                                where !(ca.IsDeleted ?? false) && ca.Id == Id
                                join empBasic in context.EmployeeBasicInfos on ca.RequestedBy equals empBasic.Id into caTemp
                                from caNew in caTemp.DefaultIfEmpty()
                                join companyVendor in context.Companys on ca.VendorId equals companyVendor.Id into gComp
                                from companyVendor in gComp.DefaultIfEmpty()
                                select new CashAdvance
                                {
                                    Id = ca.Id,
                                    CaNumber = ca.CaNumber,
                                    CaDate = ca.CaDate,
                                    Revision = ca.Revision,
                                    RequestedBy = ca.RequestedBy,
                                    RequestedOn = ca.RequestedOn,
                                    BusinessUnitId = ca.BusinessUnitId,
                                    LegalEntityId = ca.LegalEntityId,
                                    IsProjectCost = ca.IsProjectCost,
                                    PurchaseOrderId = ca.PurchaseOrderId,
                                    PurchaseOrderNumber = ca.PurchaseOrderNumber,
                                    JobId = ca.JobId,
                                    JobNumber = ca.JobNumber,
                                    ClientName = ca.ClientName,
                                    ProjectActivity = ca.ProjectActivity,
                                    ProjectArea = ca.ProjectArea,
                                    VendorId = ca.VendorId,
                                    VendorName = companyVendor.CompanyName,
                                    EmployeeId = ca.EmployeeId,
                                    Notes = ca.Notes,
                                    PayeeBankId = companyVendor.BankName,
                                    RequestEmployeeId = ca.RequestEmployeeId,
                                    RequestEmployeeName = ca.RequestEmployeeName,
                                    RemarkRejected = ca.RemarkRejected,
                                    RequestJobGradeId = ca.RequestJobGradeId,
                                    RequestJobGradeName = ca.RequestJobGradeName,
                                    RequestJobTitleId = ca.RequestJobTitleId,
                                    RequestJobTitleName = ca.RequestJobTitleName,
                                    PayeeAccountNumber = companyVendor.AccountNumber,
                                    PayeeAccountName = companyVendor.AccountName,
                                    PayeeBankBranch = ca.PayeeBankBranch,
                                    CaPeriodFrom = ca.CaPeriodFrom,
                                    CaPeriodTo = ca.CaPeriodTo,
                                    ApprovalId = ca.ApprovalId,
                                    CaStatusId = ca.CaStatusId,
                                    RequestEmployee = caNew,
                                    Amount = ca.Amount,
                                    MainServiceCategoryId = ca.MainServiceCategoryId,
                                    ClientBrandId = ca.ClientBrandId,
                                    PCETypeId = ca.PCETypeId,
                                    RequestBusinessUnitId = ca.RequestBusinessUnitId,
                                    ShareservicesId = ca.ShareservicesId,
                                    VendorAddress = companyVendor.OfficeAddress,
                                    VendorPIC = companyVendor.ContactName,
                                    VendorTaxNumber= companyVendor.TaxRegistrationNumber
                                }
                                ).AsNoTracking().FirstOrDefaultAsync();
            return result;
        }

        public async Task<List<CashAdvanceDetail>> GetDetails(string id)
        {
            var result = await (from caD in context.CashAdvanceDetails
                                join u in context.Uom on caD.UomId equals u.Id
                                where !(caD.IsDeleted ?? false) && caD.CashAdvanceId == id

                                select new CashAdvanceDetail
                                {
                                    Id = caD.Id,
                                    CashAdvanceId = id,
                                    TaskId = caD.TaskId,
                                    TaskDetail = caD.TaskDetail,
                                    Qty = caD.Qty,
                                    UnitPrice = caD.UnitPrice,
                                    Amount = caD.Amount,
                                    UomId = caD.UomId,
                                    UomName = u.unit_symbol,
                                    TotalRequest = caD.TotalRequest
                                }
                                ).AsNoTracking().ToListAsync<CashAdvanceDetail>();
            return result;
        }

        public async Task<List<CashAdvance>> GetApprovalList()
        {
            var result = await (from a in context.CashAdvances
                                join b in context.EmployeeBasicInfos on a.RequestedBy equals b.Id
                                join c in context.TrTemplateApprovals on a.Id equals c.RefId
                                join d in context.TrUserApprovals on c.Id equals d.TrTempApprovalId
                                where !(a.IsDeleted ?? false) && d.StatusApproved == Models.Um.StatusApproved.CurrentApproval && d.EmployeeBasicInfoId == context.GetEmployeeId()
                                select new CashAdvance
                                {
                                    Id = a.Id,
                                    RequestedBy = a.RequestedBy,
                                    RequestedOn = a.RequestedOn,
                                    Notes = c.StatusApprovedDescription,
                                    CreatedOn = a.CreatedOn,
                                    RequestEmployee = b
                                }).OrderByDescending(x => x.CreatedOn).ToListAsync();
            return result;
        }

        public async Task<int> ApproveAsync(CashAdvance entity)
        {
            await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.CaDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.CAS);
            int _status = 0;
            int.TryParse(entity.CaStatusId, out _status);
            return await globalFcApproval.ProcessApproval<CashAdvance>(entity.Id, _status, entity.Notes, entity);

        }

        public async Task<CashAdvance> Approve(CashAdvance entity)
        {
            try
            {
                //await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.CaDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.CAS);
                entity.CaStatusId = StatusTransaction.Approved.ToString();
                await globalFcApproval.ProcessApproval<CashAdvance>(entity.Id, Int32.Parse(entity.CaStatusId), entity.RemarkRejected, entity);

                var modeltrapp = await context.TrTemplateApprovals.Where(x => x.RefId == entity.Id).FirstOrDefaultAsync();
                if (modeltrapp != null)
                {
                    if (modeltrapp.StatusApprovedDescription == StatusTransaction.StatusName(StatusTransaction.Approved))
                    {
                        entity.CaStatusId = CashAdvanceStatus.Approved;
                        //entity.Notes = entity.RemarkRejected;
                        
                        PaymentRequest pay = new PaymentRequest();
                        pay.Id = Guid.NewGuid().ToString();
                        pay.PaymentRequestDate = DateTime.Now;
                        pay.PaymentRequestNumber = await TransID.GetTransId(context, Code.PaymentRequest, DateTime.Today);
                        pay.Status = PaymentRequestStatus.Open;
                        pay.LegalEntityId = string.IsNullOrEmpty(entity.LegalEntityId) ? "" : entity.LegalEntityId;
                        pay.BusinessUnitId = entity.BusinessUnitId;
                        pay.PaymentTypeId = "1b5ac6a6-a871-498b-82d4-8f8c647f51c4";
                        pay.Vat = 0;
                        pay.DueDate = entity.CaPeriodTo;
                        pay.PaytoName = entity.VendorName;
                        pay.PaytoBank = entity.PayeeBankId;
                        pay.TaxPayable = 0;
                        pay.VatPercent = 0;
                        pay.TaxPayablePercent = 0;
                        pay.ExchangeRate = 1;
                        pay.IsBalance = false;
                        pay.CurrencyId = "46e4d57e-5ce2-4c31-82be-f81382dc243a";
                        pay.SourceDoc = "CA";
                        pay.Balance = 0;
                        var detailca = await context.CashAdvanceDetails.AsNoTracking().Where(x => x.IsDeleted.Equals(false) && x.CashAdvanceId == entity.Id).ToListAsync();
                        decimal? ttl = 0;
                        foreach (CashAdvanceDetail data in detailca)
                        {
                            ttl += data.TotalRequest;
                        }

                        pay.TotalAmount = ttl;
                        pay.Amount = ttl;
                        pay.TotalAmountPay = ttl;
                        pay.AmountPay = ttl;
                        pay.AdvertisingTax = 0;
                        pay.Discount = 0;
                        pay.JobId = entity.JobId;
                        pay.MainServiceCategoryId = entity.MainServiceCategoryId;
                        pay.ReferenceNumberId = entity.CaNumber;
                        entity.CaStatusId = CashAdvanceStatus.Approved.ToString();
                        entity.StatusDesc = CashAdvanceStatus.GetStatus(CashAdvanceStatus.Approved);
                        await context.PhoenixAddAsync(pay);
                    }
                    else
                    {
                        entity.CaStatusId = CashAdvanceStatus.WaitingApproval;
                    }
                }
                else
                {
                    entity.CaStatusId = CashAdvanceStatus.WaitingApproval;
                }
                await log.AddAsync(new StatusLog() { TransactionId = entity.Id, Status = StatusTransaction.StatusName(Convert.ToInt32(entity.CaStatusId)), Description = entity.LogDescription });
                context.PhoenixEdit(entity);
                var save = await context.SaveChangesAsync();
                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }

        }
        //public async Task<CashAdvanceSettlement> ApproveSattlement(CashAdvanceSettlement entity)
        //{
        //    try
        //    {
        //        var modelcab = await context.CashAdvances.AsNoTracking().Where(x => x.IsDeleted.Equals(false) && x.Id == entity.CashAdvanceId).FirstOrDefaultAsync();
        //        //await financePeriod.IsClosePeriod(Convert.ToDateTime(modelcab.CaDate), modelcab.LegalEntityId, Models.Enum.FinancePeriodCode.CAS);

        //        await globalFcApproval.ProcessApproval<CashAdvanceSettlement>(entity.Id, StatusTransaction.Approved, entity.RemarkRejected, entity);

        //        var modeltrapp = await context.TrTemplateApprovals.Where(x => x.RefId == entity.Id).FirstOrDefaultAsync();
        //        if (modeltrapp != null)
        //        {
        //            if (modeltrapp.StatusApprovedDescription == StatusTransaction.StatusName(StatusTransaction.Approved))
        //            {
        //                entity.CaStatusId = CashAdvanceStatus.Complete;
        //                entity.RemarkRejected = entity.RemarkRejected;

        //                if (modelcab != null)
        //                {
        //                    modelcab.CaStatusId = entity.CaStatusId;
        //                    modelcab.ModifiedOn = DateTime.Now;
        //                    context.PhoenixEdit(modelcab);
        //                }

        //                //if (entity.Balance > 0)
        //                //{
        //                //    var modelca = await context.CashAdvances.Where(x => x.IsDeleted.Equals(false) && x.Id == entity.CashAdvanceId).FirstOrDefaultAsync();
        //                //    BankReceived br = new BankReceived();
        //                //    br.Id = Guid.NewGuid().ToString();
        //                //    br.Remarks = entity.RemarkRejected;
        //                //    if (modelca != null)
        //                //    {
        //                //        br.LegalEntityId = modelca.LegalEntityId;
        //                //        br.BusinessUnitId = modelca.BusinessUnitId;
        //                //        br.ReferenceNumber = entity.SattlementNumber;
        //                //    }

        //                //    br.CurrencyId = "46e4d57e-5ce2-4c31-82be-f81382dc243a";
        //                //    br.ExchangeRate = 1;
        //                //    br.ReceivedAmount = entity.Balance;
        //                //    br.ReceivedFrom = "Client";
        //                //    br.ReceivedOn = DateTime.Now;
        //                //    br.BankReceivedTypeId = "EC1E4F0F-0F40-4B11-9432-F0E53FGRW2";
        //                //    br.SourceDoc = "CAS";
        //                //    br.BankReceivedStatusId = "1";

        //                //    br.MainServiceCategoryId = entity.MainServiceCategoryId;


        //                //    await context.PhoenixAddAsync(br);
        //                //}

        //            }
        //            else
        //            {
        //                entity.CaStatusId = CashAdvanceStatus.SattlementCheck;
        //            }
        //        }
        //        else
        //        {
        //            entity.CaStatusId = CashAdvanceStatus.SattlementCheck;
        //        }

        //        context.PhoenixEdit(entity);
        //        var save = await context.SaveChangesAsync();
        //        return entity;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message.ToString());
        //    }

        //}
        public async Task<CashAdvance> Reject(CashAdvance entity)
        {
            try
            {
                //await financePeriod.IsClosePeriod(Convert.ToDateTime(entity.CaDate), entity.LegalEntityId, Models.Enum.FinancePeriodCode.CAS);
                entity.CaStatusId = StatusTransaction.Rejected.ToString();
                await globalFcApproval.ProcessApproval<CashAdvance>(entity.Id, Int32.Parse(entity.CaStatusId), entity.RemarkRejected, entity);

                var modeltrapp = await context.TrTemplateApprovals.AsNoTracking().Where(x => x.RefId == entity.Id).FirstOrDefaultAsync();
                if (modeltrapp != null)
                {
                    if (modeltrapp.StatusApprovedDescription == StatusTransaction.StatusName(StatusTransaction.Rejected))
                    {
                        entity.CaStatusId = PaymentRequestStatus.Reject;
                        entity.Notes = entity.RemarkRejected;
                    }
                }
                else
                {
                    entity.CaStatusId = PaymentRequestStatus.Reject;
                }
                await log.AddAsync(new StatusLog() { TransactionId = entity.Id, Status = StatusTransaction.StatusName(Convert.ToInt32(StatusTransaction.Rejected)), Description = entity.RemarkRejected });
                context.PhoenixEdit(entity);
                var save = await context.SaveChangesAsync();
                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }

        }



        //public async Task<int> EditSattlement(CashAdvanceSattlementDTO model)
        //{
        //    using (var transaction = await context.Database.BeginTransactionAsync())
        //    {
        //        try
        //        {
        //            var modelca = await context.CashAdvances.AsNoTracking().Where(x => x.Id == model.Header.CashAdvanceId).FirstOrDefaultAsync();
        //            //await financePeriod.IsClosePeriod(Convert.ToDateTime(modelca.CaDate), modelca.LegalEntityId, Models.Enum.FinancePeriodCode.CAS);
        //            context.PhoenixEdit(model.Header);

        //            modelca.CaStatusId = model.Header.CaStatusId;
        //            context.PhoenixEdit(modelca);

        //            if (model.Details != null)
        //            {

        //                if (model.Details.Count > 0)
        //                {
        //                    var additional = await context.CashAdvanceSettlementDetail.Where(x => x.CashAdvanceSettleId == model.Header.Id).ToListAsync();
        //                    if (additional.Count > 0)
        //                    {
        //                        context.CashAdvanceSettlementDetail.RemoveRange(additional);
        //                    }

        //                    List<CashAdvanceSettlementDetail> dt = new List<CashAdvanceSettlementDetail>();
        //                    foreach (CashAdvanceSettlementDetail data in model.Details)
        //                    {
        //                        data.AccountId = "4415002503333080";
        //                        data.AccountCode = "2.1.01.01.01";
        //                        dt.Add(data);
        //                    }

        //                    CashAdvanceSettlementDetail[] dtg = dt.ToArray();
        //                    await context.PhoenixAddRangeAsync(dtg);
        //                }
        //            }

        //            var trapprovalmodel = await context.TrTemplateApprovals.AsNoTracking().Where(x => x.RefId == model.Header.Id).FirstOrDefaultAsync();
        //            if (trapprovalmodel == null)
        //            {
        //                var isApprove = true;
        //                var dataMenu = await menuService.GetByUniqueName(MenuUnique.CashAdvance).ConfigureAwait(false);
        //                var vm = new Models.ViewModel.TransApprovalHrisVm()
        //                {
        //                    MenuId = dataMenu.Id,
        //                    RefId = model.Header.Id,
        //                    DetailLink = $"{ApprovalLink.CashAdvance}?Id={model.Header.Id}&isApprove=true",
        //                    //Tname = "fn.PurchaseRequest",//"SP#NAMA SP NYA",
        //                    IdTemplate = idTemplate
        //                };
        //                var subGroupId = ClainJwtPrincipalHelper.GetBusinessSubGroup(context.HttpContext);
        //                var divisionId = ClainJwtPrincipalHelper.GetBusinessUnitDivisi(context.HttpContext);
        //                var employeeId = ClainJwtPrincipalHelper.GetEmployeeId(context.HttpContext);
        //                isApprove = await globalFcApproval.Save(vm, subGroupId, divisionId, employeeId);


        //                if (isApprove)
        //                {
        //                    var save = await context.SaveChangesAsync();
        //                    transaction.Commit();
        //                    return save;
        //                }
        //                else
        //                {
        //                    transaction.Rollback();
        //                    throw new Exception("please check the approval template that will be processed");
        //                }

        //            }
        //            else
        //            {

        //                var save = await context.SaveChangesAsync();
        //                transaction.Commit();
        //                return save;
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            transaction.Rollback();
        //            throw new Exception(ex.Message.ToString());
        //        }
        //    }
        //}

        // pindah settlement service
        //public async Task<CashAdvanceSettlement> GetSattlementById(string CaId)
        //{
        //    return await context.CashAdvanceSettlement.Where(x => x.IsDeleted.Equals(false) && x.CashAdvanceId == CaId).FirstOrDefaultAsync();
        //}

        //public async Task<List<CashAdvanceSettlementDetail>> GetSattlementDetailById(string Id)
        //{
        //    return await context.CashAdvanceSettlementDetail.Where(x => x.IsDeleted.Equals(false) && x.CashAdvanceSettleId == Id).ToListAsync();
        //}

        // pindah settlement service
        //public async Task<List<Journal>> GetJournalDataBySattlementId(string Id)
        //{
        //    var data = await (from tmpj in context.Journal
        //                      join tbu in context.BusinessUnits on tmpj.DivisiId equals tbu.Id
        //                      join st in context.CashAdvanceSettlement on tmpj.ReferenceId equals st.Id into stn
        //                      from stnj in stn.DefaultIfEmpty()
        //                      join coa in context.ChartOfAccounts on tmpj.AccountId equals coa.Id
        //                      where tmpj.ReferenceId == Id && tmpj.IsDeleted.Equals(false)
        //                      select new Journal
        //                      {
        //                          AccountId = tmpj.AccountId,
        //                          AccountCode = coa.CodeRec,
        //                          JobId = tmpj.JobId,
        //                          AccountName = coa.Name5,
        //                          Description = coa.Name5,
        //                          JobIdName = stnj.JobId == null ? "INTERNAL " : "PROJECT " + stnj.JobId,
        //                          DivisiId = tmpj.DivisiId,
        //                          DivisiName = tbu.UnitName,
        //                          Credit = tmpj.Credit,
        //                          Debit = tmpj.Debit,
        //                          LineCode = tmpj.LineCode,
        //                      }).ToListAsync();

        //    return data;
        //}

        public async Task<List<Journal>> GetJournalDataId(string Id)
        {
            var data = await (from tmpj in context.Journal
                              join tbu in context.BusinessUnits on tmpj.DivisiId equals tbu.Id
                              join st in context.CashAdvances on tmpj.ReferenceId equals st.Id into stn
                              from stnj in stn.DefaultIfEmpty()
                              join coa in context.ChartOfAccounts on tmpj.AccountId equals coa.Id
                              where tmpj.ReferenceId == Id && tmpj.IsDeleted.Equals(false)
                              select new Journal
                              {
                                  AccountId = tmpj.AccountId,
                                  AccountCode = coa.CodeRec,
                                  JobId = tmpj.JobId,
                                  AccountName = coa.Name5,
                                  Description = coa.Name5,
                                  JobIdName = stnj.JobId == null ? "INTERNAL " : "PROJECT " + stnj.JobId,
                                  DivisiId = tmpj.DivisiId,
                                  DivisiName = tbu.UnitName,
                                  Credit = tmpj.Credit,
                                  Debit = tmpj.Debit,
                                  LineCode = tmpj.LineCode,
                              }).ToListAsync();

            return data;
        }

        //public async Task<int> CreateAutomaticJournalFromSattlementId(string Id)
        //{
        //    using (var transaction = await context.Database.BeginTransactionAsync())
        //    {
        //        try
        //        {
        //            var modelca = await (from a in context.CashAdvances.AsNoTracking()
        //                                 join b in context.CashAdvanceSettlement.AsNoTracking() on a.Id equals b.CashAdvanceId
        //                                 where b.Id == Id
        //                                 select a).FirstOrDefaultAsync();
        //            await financePeriod.IsClosePeriod(Convert.ToDateTime(modelca.CaDate), modelca.LegalEntityId, Models.Enum.FinancePeriodCode.CAS);
        //            var modeljournal = await context.Journal.AsNoTracking().Where(x => x.IsDeleted.Equals(false) && x.ReferenceId == Id).ToListAsync();
        //            if (modeljournal != null)
        //            {
        //                List<Journal> jn = new List<Journal>();
        //                foreach (Journal data in modeljournal)
        //                {
        //                    jn.Add(data);
        //                }
        //                Journal[] dtn = jn.ToArray();
        //                context.PhoenixDeleteRange(dtn);
        //            }

        //            //context.
        //            await context.Database.ExecuteSqlCommandAsync("EXECUTE fn.create_jurnal_cas @p_cas_id", parameters: new[] { new SqlParameter("@p_cas_id", Id) });
        //            var save = await context.SaveChangesAsync();

        //            transaction.Commit();
        //            return save;
        //        }
        //        catch (Exception ex)
        //        {
        //            transaction.Rollback();
        //            throw new Exception(ex.Message.ToString());
        //        }
        //    }
        //}

        public async Task<CashAdvance> GetByCaNumber(string Id)
        {
            //return await context.CashAdvances.Where(x => x.IsDeleted.Equals(false) && x.CaNumber == Id).FirstOrDefaultAsync();

            var result = await (from ca in context.CashAdvances
                                where !(ca.IsDeleted ?? false) && ca.CaNumber == Id
                                join empBasic in context.EmployeeBasicInfos on ca.RequestedBy equals empBasic.Id into caTemp
                                from caNew in caTemp.DefaultIfEmpty()
                                join companyVendor in context.Companys on ca.VendorId equals companyVendor.Id into gComp
                                from companyVendor in gComp.DefaultIfEmpty()
                                select new CashAdvance
                                {
                                    Id = ca.Id,
                                    CaNumber = ca.CaNumber,
                                    CaDate = ca.CaDate,
                                    Revision = ca.Revision,
                                    RequestedBy = ca.RequestedBy,
                                    RequestedOn = ca.RequestedOn,
                                    BusinessUnitId = ca.BusinessUnitId,
                                    LegalEntityId = ca.LegalEntityId,
                                    IsProjectCost = ca.IsProjectCost,
                                    PurchaseOrderId = ca.PurchaseOrderId,
                                    PurchaseOrderNumber = ca.PurchaseOrderNumber,
                                    JobId = ca.JobId,
                                    JobNumber = ca.JobNumber,
                                    ClientName = ca.ClientName,
                                    ProjectActivity = ca.ProjectActivity,
                                    ProjectArea = ca.ProjectArea,
                                    VendorId = ca.VendorId,
                                    VendorName = companyVendor.CompanyName,
                                    EmployeeId = ca.EmployeeId,
                                    Notes = ca.Notes,
                                    PayeeBankId = companyVendor.BankName,
                                    RequestEmployeeId = ca.RequestEmployeeId,
                                    RequestEmployeeName = ca.RequestEmployeeName,
                                    RemarkRejected = ca.RemarkRejected,
                                    RequestJobGradeId = ca.RequestJobGradeId,
                                    RequestJobGradeName = ca.RequestJobGradeName,
                                    RequestJobTitleId = ca.RequestJobTitleId,
                                    RequestJobTitleName = ca.RequestJobTitleName,
                                    PayeeAccountNumber = companyVendor.AccountNumber,
                                    PayeeAccountName = companyVendor.AccountName,
                                    PayeeBankBranch = ca.PayeeBankBranch,
                                    CaPeriodFrom = ca.CaPeriodFrom,
                                    CaPeriodTo = ca.CaPeriodTo,
                                    ApprovalId = ca.ApprovalId,
                                    CaStatusId = ca.CaStatusId,
                                    RequestEmployee = caNew,
                                    Amount = ca.Amount,
                                    MainServiceCategoryId = ca.MainServiceCategoryId,
                                    ClientBrandId = ca.ClientBrandId,
                                    PCETypeId = ca.PCETypeId,
                                    RequestBusinessUnitId = ca.RequestBusinessUnitId,
                                    ShareservicesId = ca.ShareservicesId,
                                    VendorAddress = companyVendor.OfficeAddress,
                                    VendorPIC = companyVendor.ContactName,
                                    VendorTaxNumber = companyVendor.TaxRegistrationNumber
                                }
                               ).AsNoTracking().FirstOrDefaultAsync();
            return result;
        }

        public async Task<JobDetail> GetJobDetail(string Id) => await context.JobDetail.Where(x => !(x.IsDeleted ?? false) && x.Id == Id).FirstOrDefaultAsync();
        public async Task<Uom> GetUom(string Id) => await context.Uom.Where(x => !(x.IsDeleted ?? false)).FirstOrDefaultAsync();

        public async Task<CashAdvance> GetCompanyInfo(string companyid)
        {
            var company = await (
                from a in context.Companys
                where a.Id == companyid
                select new CashAdvance
                {
                    VendorPIC = a.ContactName,
                    VendorTaxNumber = a.TaxRegistrationNumber,
                    VendorAddress = a.OfficeAddress,
                    PayeeBankId = a.BankName,
                    PayeeAccountName = a.AccountName,
                    PayeeAccountNumber = a.AccountNumber
                }).AsNoTracking().FirstOrDefaultAsync();
            return company;
        }
    }
}
