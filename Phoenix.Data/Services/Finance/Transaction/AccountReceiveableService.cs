﻿using Phoenix.Data.Models.Finance.Transaction;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Data.SqlClient;
using Phoenix.Data.Models;
using Phoenix.Data.Models.Finance.Transaction.AccReceivable;

namespace Phoenix.Data.Services.Finance.Transaction
{
    public interface IAccountReceiveableService
    {
        Task<object> GetIndexList(string legalentity, string status);
        Task<AccountReceivable> GetDetailAR(string invid);
        Task<int> CreateJournalAR(InvoiceClient iv);
        Task<int> CreateJournalMultipleAR(InvoiceClient iv);
        Task<int> SubmitAR(InvoiceClient iv);
        Task<object> GerListJournal(string ARid, string multipceid);
        Task<object> GerListJournalAR(string ARid, string multipceid);
        Task<InvoiceClient> GetInvoiceInfo(string invid);
        Task<object> ReadInvDtl(string invid);
        Task<dynamic> GetChartOfAccountTradeAndTradeInter();
        Task<object> GetListStatement(string status, string legal, string cl, string ms, string sd, string ed);
        Task<object> GetListAging(string cl, string legal, string dv);
        Task<AccountReceivable> Get(string Id);
    }

    public class AccountReceiveableService : IAccountReceiveableService
    {
        readonly DataContext context;
        readonly FinancePeriodService financePeriod;
        public AccountReceiveableService(DataContext context, FinancePeriodService financePeriod)
        {
            this.context = context;
            this.financePeriod = financePeriod;
        }

        public async Task<object> GetIndexList(string legalentity, string status)
        {
            var arlist = await (
                from a in context.AccountReceivables
                join b in context.InvoiceClients on a.Id equals b.InvoiceNumber
                join c in context.Banks on b.AccountId equals c.Id
                join d in context.Pces on b.PceId equals d.Id
                join e in context.JobDetail on d.JobId equals e.Id
                join f in context.LegalEntity on e.LegalEntityId equals f.Id
                join h in context.CompanyBrands on e.BrandId equals h.Id
                join i in context.Companys on h.CompanyId equals i.Id
                join j in context.BusinessUnits on e.BusinessUnitDivisionId equals j.Id
                where a.Status == (string.IsNullOrEmpty(status) ? a.Status : status) && f.Id == (string.IsNullOrEmpty(legalentity) ? f.Id : legalentity)
                    && b.MultiPceId == null
                select new
                {
                    a.Id,
                    b.PceId,
                    b.InvoiceDate,
                    JobId = b.JobId,
                    e.JobName,
                    ClientName = i.CompanyName,
                    GrandTotal = a.Amount,
                    AccountBankName = c.BankName,
                    AccountBankNumber = c.AccountNumber,
                    LegalEntity = f.LegalEntityName,
                    Division = j.UnitName,
                    Status = a.Status,
                    MultiPceId = ""
                }).AsNoTracking().ToListAsync();

            var arlistOther = await (
                from a in context.AccountReceivables
                join b in context.InvoiceClients on a.Id equals b.InvoiceNumber
                join c in context.Banks on b.AccountId equals c.Id
                join f in context.LegalEntity on b.LegalId equals f.Id
                join h in context.CompanyBrands on b.BrandId equals h.Id
                join i in context.Companys on h.CompanyId equals i.Id
                join j in context.BusinessUnits on b.BusinessUnitId equals j.Id
                where a.Status == (string.IsNullOrEmpty(status) ? a.Status : status) && f.Id == (string.IsNullOrEmpty(legalentity) ? f.Id : legalentity)
                    && b.MultiPceId == null && b.JobId == null
                select new
                {
                    a.Id,
                    b.PceId,
                    b.InvoiceDate,
                    JobId = "",
                    JobName = "",
                    ClientName = i.CompanyName,
                    GrandTotal = a.Amount,
                    AccountBankName = c.BankName,
                    AccountBankNumber = c.AccountNumber,
                    LegalEntity = f.LegalEntityName,
                    Division = j.UnitName,
                    Status = a.Status,
                    MultiPceId = ""
                }).AsNoTracking().ToListAsync();


            var arlist2 = await (
                from a in context.AccountReceivables
                join ab in (
                    from ic in context.InvoiceClients
                    join d in context.Pces on ic.PceId equals d.Id
                    join e in context.JobDetail on d.JobId equals e.Id
                    join c in context.Banks on ic.AccountId equals c.Id
                    select new
                    {
                        ic.MultiPceId,
                        ic.PceId,
                        ic.InvoiceDate,
                        JobId = e.Id,
                        JobName = e.JobDescription,
                        e.BrandId,
                        e.LegalEntityId,
                        c.BankName,
                        c.AccountNumber,
                        e.BusinessUnitDivisionId,
                        ic.GrandTotal
                    }
                ) on a.AccountReceivableNumber equals ab.MultiPceId//new { MultiPceId = a.AccountReceivableNumber, a.JobId } equals new { ab.MultiPceId, ab.JobId }
                join f in context.LegalEntity on ab.LegalEntityId equals f.Id
                join h in context.CompanyBrands on ab.BrandId equals h.Id
                join i in context.Companys on h.CompanyId equals i.Id
                join j in context.BusinessUnits on ab.BusinessUnitDivisionId equals j.Id
                where a.Status == (string.IsNullOrEmpty(status) ? a.Status : status) && f.Id == (string.IsNullOrEmpty(legalentity) ? f.Id : legalentity)
                    && ab.MultiPceId != null
                select new
                {
                    Id = a.AccountReceivableNumber,
                    ab.PceId,
                    ab.InvoiceDate,
                    ab.JobId,
                    ab.JobName,
                    ClientName = i.CompanyName,
                    GrandTotal = ab.GrandTotal,
                    AccountBankName = ab.BankName,
                    AccountBankNumber = ab.AccountNumber,
                    LegalEntity = f.LegalEntityName,
                    Division = j.UnitName,
                    Status = a.Status,
                    MultiPceId = ab.MultiPceId
                }).AsNoTracking().ToListAsync();

            var result = (
                from rows in arlist2
                group rows by new
                {
                    rows.Id,
                    rows.InvoiceDate,
                    rows.AccountBankName,
                    rows.AccountBankNumber,
                    rows.ClientName,
                    rows.LegalEntity,
                    rows.Division,
                    rows.Status,
                    rows.MultiPceId
                } into gr
                select new
                {
                    Id = gr.Key.Id,
                    PceId = string.Join(", ", gr.Select(i => i.PceId)),
                    gr.Key.InvoiceDate,
                    JobId = "",
                    JobName = string.Join(", ", gr.Select(i => i.JobId + "-" + i.JobName)),
                    gr.Key.ClientName,
                    GrandTotal = gr.Sum(i => i.GrandTotal),
                    gr.Key.AccountBankName,
                    gr.Key.AccountBankNumber,
                    gr.Key.LegalEntity,
                    gr.Key.Division,
                    gr.Key.Status,
                    gr.Key.MultiPceId
                }).ToList();

            if (result.Count > 0)
                arlist.AddRange(result);
            if (arlistOther.Count > 0)
                arlist.AddRange(arlistOther);
            //if (arlist2.Count > 0)
            //    arlist.AddRange(arlist2);

            if (arlist != null)
                return arlist.OrderBy(x => x.InvoiceDate).ToList();
            else
                return arlist;
        }

        public async Task<AccountReceivable> GetDetailAR(string invid)
        {
            var arlist = await (from a in context.AccountReceivables
                                join b in context.InvoiceClients on a.Id equals b.InvoiceNumber
                                join c in context.Banks on b.AccountId equals c.Id
                                join d in context.Pces on b.PceId equals d.Id
                                join f in context.CompanyBrands on b.BrandId equals f.Id
                                join g in context.Companys on b.ClientId equals g.Id
                                join k in context.MainServiceCategorys on d.MainserviceCategoryId equals k.Id
                                join l in context.Currencies on d.CurrencyId equals l.Id
                                where a.Id == invid
                                //join h in context.ClientBrief on f.Id equals h.ExternalBrandId
                                //join j in context.JobDetail on h.Id equals j.ClientBriefId
                                //join e in context.LegalEntity on j.LegalEntityId equals e.Id
                                select new AccountReceivable()
                                {
                                    Id = a.Id,
                                    TransactionDate = b.InvoiceDate,
                                    //DueDate = b.DueDate,
                                    BankAccountDestinationName = c.BankName,
                                    PceCode = d.Code,
                                    Amount = a.Amount,
                                    //LegalEntity = e.LegalEntityName,
                                    ClientBrand = f.BrandName,
                                    CompanyName = g.CompanyName,
                                    //Status = a.Status,
                                    Status = "",
                                    JobType = k.Name,
                                    CurrencyCode = l.CurrencyCode,
                                    Description = a.Description,
                                    InvoiceDeliveryDate = a.InvoiceDeliveryDate
                                }).AsNoTracking().FirstOrDefaultAsync();

            return arlist;
        }
        public async Task<int> CreateJournalAR(InvoiceClient iv)
        {
            int savedc = 0;
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    //context.PhoenixEdit(iv);

                    //context.RemoveRange(iv.items_other);
                    //await context.SaveChangesAsync();
                    //await context.AddRangeAsync(iv.items_other);
                    //await context.SaveChangesAsync();

                    //savedc = await context.SaveChangesAsync();

                    AccountReceivable ardata = (from b in context.AccountReceivables.AsNoTracking() where b.Id == iv.Id select b).FirstOrDefault();
                    await financePeriod.IsClosePeriod(Convert.ToDateTime(ardata.DueDate), ardata.LegalEntityId, Models.Enum.FinancePeriodCode.AR);

                    var listJurnal = (from a in context.JournalTemps where a.ReferenceId == iv.Id select a).ToList();
                    if (listJurnal.Count > 0)
                        context.RemoveRange(listJurnal);

                    ardata.Description = iv.Remarks;
                    context.Update(ardata);
                    savedc += await context.SaveChangesAsync();

                    await context.Database.ExecuteSqlCommandAsync("EXECUTE fn.create_jurnal_ar2 @p_account_receivable_id", parameters: new[] { new SqlParameter("@p_account_receivable_id", iv.Id) });
                    savedc += await context.SaveChangesAsync();

                    transaction.Commit();
                    return savedc;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    savedc = 0;
                    throw new Exception(ex.Message.ToString());
                }
            }

        }
        public async Task<int> CreateJournalMultipleAR(InvoiceClient iv)
        {
            int savedc = 0;
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    //savedc = await context.SaveChangesAsync();
                    //var listJurnal = (from a in context.TemporaryAccountPayableJournal where a.reference_id == iv.Id select a).ToList();
                    //if (listJurnal.Count > 0)
                    //    context.RemoveRange(listJurnal);

                    //AccountReceivable ardata = (context.AccountReceivables.Where(x => x.Id == iv.MultiPceId).FirstOrDefault()) ?? new AccountReceivable();
                    AccountReceivable ardata = (context.AccountReceivables.AsNoTracking().Where(x => x.AccountReceivableNumber == iv.MultiPceId).FirstOrDefault()) ?? new AccountReceivable();
                    await financePeriod.IsClosePeriod(Convert.ToDateTime(ardata.DueDate), ardata.LegalEntityId, Models.Enum.FinancePeriodCode.AR);
                    ardata.Description = iv.Remarks;
                    context.Update(ardata);
                    savedc += await context.SaveChangesAsync();

                    await context.Database.ExecuteSqlCommandAsync("EXECUTE fn.create_jurnal_multipce_ar @p_account_receivable_id", parameters: new[] { new SqlParameter("@p_account_receivable_id", iv.MultiPceId) });
                    savedc += await context.SaveChangesAsync();

                    transaction.Commit();
                    return savedc;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    savedc = 0;
                    throw new Exception(ex.Message.ToString());
                }
            }

        }
        public async Task<int> SubmitAR(InvoiceClient iv)
        {
            int savedc = 0;
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                AccountReceivable ar = new AccountReceivable();
                List<AccountReceivable> arlist = new List<AccountReceivable>();
                List<BankReceived> listBankReceived = new List<BankReceived>();
                decimal? amountarlist = 0;
                InvoiceClient ivc = new InvoiceClient();

                if (!string.IsNullOrWhiteSpace(iv.MultiPceId))
                {
                    var arlists = await context.AccountReceivables.Where(b => b.AccountReceivableNumber == iv.MultiPceId).AsNoTracking().ToListAsync();
                    ivc = await context.InvoiceClients.Where(ab => ab.MultiPceId == iv.MultiPceId).FirstOrDefaultAsync();
                    foreach (var item in arlists)
                    {
                        await financePeriod.IsClosePeriod(Convert.ToDateTime(item.TransactionDate), item.LegalEntityId, Models.Enum.FinancePeriodCode.AR);
                        //transaksi ar
                        item.Status = "Posted";
                        amountarlist = amountarlist + item.Amount;

                        arlist.Add(item);

                        //transaksi br untuk setiap ar di multiple invc
                        //BankReceived br = new BankReceived();
                        //br.Id = Guid.NewGuid().ToString();
                        //br.BankReceivedStatusId = PaymentRequestStatus.Open;
                        //br.BankReceivedTypeId = "EC1E4F0F-0F40-4B11-9432-F0E53C12135B";
                        //br.CurrencyId = ivc.CurrencyId;
                        //br.ExchangeRate = ivc.ExchangeRate;
                        //br.AccountId = ivc.AccountId;
                        //br.SourceDoc = "AR";
                        //br.ReceivedForm = "Client";
                        //br.MainServiceCategoryId = ivc.MainServiceCategory;
                        //br.JobId = item.JobId;
                        ////br.DueDate = ivc.DueDate;
                        //br.IsDeleted = false;
                        //br.AdvertisingTax = 0;
                        //br.ReceivedDate = DateTime.Now;
                        //br.ReceivedNumber = await TransID.GetTransId(context, Code.BankReceived, DateTime.Today);
                        //br.BusinessUnitId = item.BusinessUnitId;
                        //br.LegalEntityId = item.LegalEntityId;
                        //br.ReferenceNumber = item.Id;
                        //br.ReceivedAmount = item.Amount;
                        //listBankReceived.Add(br);
                    }

                    try
                    {
                        context.PhoenixEditRange(arlist.ToArray());
                        context.PhoenixAddRange(listBankReceived.ToArray());
                        //context.SaveChanges();

                        // Journal
                        AccountReceivable ardata = (from b in context.AccountReceivables.AsNoTracking() where b.Id == iv.MultiPceId select b).FirstOrDefault();
                        await financePeriod.IsClosePeriod(Convert.ToDateTime(ardata.TransactionDate), ardata.LegalEntityId, Models.Enum.FinancePeriodCode.AR);
                        List<Journal> listJournal = new List<Journal>();
                        var listJurnalTemp = (from a in context.JournalTemps where a.ReferenceId == iv.MultiPceId select a).ToList();
                        foreach (var j in listJurnalTemp)
                        {
                            Journal jr = new Journal
                            {
                                Id = j.Id,
                                TransactionDate = j.TransactionDate,
                                ReferenceId = j.ReferenceId,
                                ReferenceNumber = j.Referenceto,
                                ReferenceDate = j.Referencedate,
                                Description = j.Description,
                                CurrencyId = j.CurrencyId,
                                Qty = j.Qty,
                                ExchangeRate = j.ExchangeRate,
                                Price = Convert.ToInt32(j.Price),
                                PceId = j.PceId,
                                JobId = j.JobId,
                                ExternalId = j.ExternalId,
                                InternalId = j.InternalId,
                                AccountId = j.AccountId,
                                InvoiceId = j.InvoiceId,
                                PoId = j.PoId,
                                Budgetdebit = j.Budgetdebit,
                                Budgetcredit = j.Budgetcredit,
                                Budgetgballance = j.Budgetgballance,
                                Beginningballance = j.Beginningballance,
                                Beginningdebit = j.Beginningdebit,
                                Beginningcredit = j.Beginningcredit,
                                Debit = j.Debit,
                                Credit = j.Credit,
                                Balance = j.Balance,
                                Endingdebet = j.Endingdebet,
                                Endingcredit = j.Endingcredit,
                                Endingbalance = j.Endingbalance,
                                Rcondition = j.Rcondition,
                                Flag = j.Flag,
                                Fdate = j.Fdate,
                                Ftime = j.Ftime,
                                MainserviceCategoryId = j.MainserviceCategoryId,
                                ShareserviceId = j.ShareserviceId,
                                DivisiId = j.DivisiId,
                                LegalId = j.LegalId,
                                AffiliateId = j.AffiliateId,
                                IsPosted = j.IsPosted,
                                AccountCode = j.AccountCode,
                                LineCode = j.LineCode,
                                Unit = j.Unit,
                                //TypeOfExpenseId = j.TypeOfExpenseId,
                                //IsJobClosed = j.IsJobClosed,
                                //PcaTaskId = j.PcaTaskId,
                                CreatedBy = j.CreatedBy,
                                CreatedOn = j.CreatedOn,
                                ModifiedBy = j.ModifiedBy,
                                ModifiedOn = j.ModifiedOn,
                                ApprovedBy = j.ApprovedBy,
                                ApprovedOn = j.ApprovedOn,
                                IsActive = j.IsActive,
                                IsLocked = j.IsLocked,
                                IsDefault = j.IsDefault,
                                IsDeleted = j.IsDeleted,
                                OwnerId = j.OwnerId,
                                DeletedBy = j.DeletedBy,
                                DeletedOn = j.DeletedOn
                            };
                            listJournal.Add(jr);
                        }

                        if (listJournal.Count > 0)
                        {
                            context.PhoenixAddRange(listJournal.ToArray());
                        }

                        savedc += await context.SaveChangesAsync();
                        transaction.Commit();
                        return savedc;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        savedc = 0;
                        throw new Exception(ex.Message.ToString());
                    }
                }
                else
                {
                    ar = (from b in context.AccountReceivables.AsNoTracking() where b.Id == iv.Id select b).FirstOrDefault();
                    await financePeriod.IsClosePeriod(Convert.ToDateTime(ar.TransactionDate), ar.LegalEntityId, Models.Enum.FinancePeriodCode.AR);
                    ivc = await context.InvoiceClients.Where(ab => ab.Id == iv.Id).FirstOrDefaultAsync();
                    ar.Status = "Posted";

                    try
                    {
                        context.PhoenixEdit(ar);
                        //BankReceived bc = new BankReceived()
                        //{
                        //    Id = Guid.NewGuid().ToString(),
                        //    BankReceivedStatusId = PaymentRequestStatus.Open,
                        //    BankReceivedTypeId = "EC1E4F0F-0F40-4B11-9432-F0E53C12135B",
                        //    CurrencyId = ivc.CurrencyId,
                        //    ExchangeRate = ivc.ExchangeRate,
                        //    AccountId = ivc.AccountId,
                        //    SourceDoc = "AR",
                        //    ReceivedForm = "Client",
                        //    MainServiceCategoryId = ivc.MainServiceCategory,
                        //    JobId = ar.JobId,
                        //    //DueDate = ivc.DueDate,
                        //    IsDeleted = false,
                        //    AdvertisingTax = 0,
                        //    ReceivedDate = DateTime.Now,
                        //    ReceivedNumber = await TransID.GetTransId(context, Code.BankReceived, DateTime.Today),
                        //    BusinessUnitId = ar.BusinessUnitId,
                        //    LegalEntityId = ar.LegalEntityId,
                        //    ReferenceNumber = ar.Id,
                        //    ReceivedAmount = ar.Amount
                        //};

                        //await context.AddAsync(bc);

                        // Journal

                        List<Journal> listJournal = new List<Journal>();
                        var listJurnalTemp = (from a in context.JournalTemps where a.ReferenceId == iv.Id select a).ToList();
                        foreach (var j in listJurnalTemp)
                        {
                            Journal jr = new Journal
                            {
                                Id = j.Id,
                                TransactionDate = j.TransactionDate,
                                ReferenceId = j.ReferenceId,
                                ReferenceNumber = j.Referenceto,
                                ReferenceDate = j.Referencedate,
                                Description = j.Description,
                                CurrencyId = j.CurrencyId,
                                Qty = j.Qty,
                                ExchangeRate = j.ExchangeRate,
                                Price = Convert.ToInt32(j.Price),
                                PceId = j.PceId,
                                JobId = j.JobId,
                                ExternalId = j.ExternalId,
                                InternalId = j.InternalId,
                                AccountId = j.AccountId,
                                InvoiceId = j.InvoiceId,
                                PoId = j.PoId,
                                Budgetdebit = j.Budgetdebit,
                                Budgetcredit = j.Budgetcredit,
                                Budgetgballance = j.Budgetgballance,
                                Beginningballance = j.Beginningballance,
                                Beginningdebit = j.Beginningdebit,
                                Beginningcredit = j.Beginningcredit,
                                Debit = j.Debit,
                                Credit = j.Credit,
                                Balance = j.Balance,
                                Endingdebet = j.Endingdebet,
                                Endingcredit = j.Endingcredit,
                                Endingbalance = j.Endingbalance,
                                Rcondition = j.Rcondition,
                                Flag = j.Flag,
                                Fdate = j.Fdate,
                                Ftime = j.Ftime,
                                MainserviceCategoryId = j.MainserviceCategoryId,
                                ShareserviceId = j.ShareserviceId,
                                DivisiId = j.DivisiId,
                                LegalId = j.LegalId,
                                AffiliateId = j.AffiliateId,
                                IsPosted = j.IsPosted,
                                AccountCode = j.AccountCode,
                                LineCode = j.LineCode,
                                Unit = j.Unit,
                                //TypeOfExpenseId = j.TypeOfExpenseId,
                                //IsJobClosed = j.IsJobClosed,
                                //PcaTaskId = j.PcaTaskId,
                                CreatedBy = j.CreatedBy,
                                CreatedOn = j.CreatedOn,
                                ModifiedBy = j.ModifiedBy,
                                ModifiedOn = j.ModifiedOn,
                                ApprovedBy = j.ApprovedBy,
                                ApprovedOn = j.ApprovedOn,
                                IsActive = j.IsActive,
                                IsLocked = j.IsLocked,
                                IsDefault = j.IsDefault,
                                IsDeleted = j.IsDeleted,
                                OwnerId = j.OwnerId,
                                DeletedBy = j.DeletedBy,
                                DeletedOn = j.DeletedOn
                            };
                            listJournal.Add(jr);
                        }

                        if (listJournal.Count > 0)
                        {
                            context.PhoenixAddRange(listJournal.ToArray());
                        }

                        savedc += await context.SaveChangesAsync();

                        transaction.Commit();
                        return savedc;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        savedc = 0;
                        throw new Exception(ex.Message.ToString());
                    }
                }
            }
        }

        public async Task<object> GerListJournal(string ARid, string multipceid)
        {
            string arid = !string.IsNullOrWhiteSpace(multipceid) ? multipceid : ARid;
            var journal = await (from tb in context.JournalTemps
                                 join coa in context.ChartOfAccounts on tb.AccountId equals coa.Id
                                 join bu in context.BusinessUnits on tb.DivisiId equals bu.Id into arc
                                 from buu in arc.DefaultIfEmpty()
                                 join job in context.JobDetail on tb.JobId equals job.Id into gjob
                                 from jobs in gjob.DefaultIfEmpty()
                                 where tb.ReferenceId == arid
                                 orderby tb.LineCode ascending
                                 select new Journal
                                 {
                                     AccountId = coa.Id,
                                     AccountCode = coa.CodeRec,
                                     AccountName = coa.Name5,
                                     JobId = tb.JobId,
                                     Description = tb.Description == null ? coa.Name5 : tb.Description,
                                     JobIdName = tb.JobId +"-"+jobs.JobName == null? "" : jobs.JobName,
                                     DivisiId = tb.DivisiId,
                                     DivisiName = buu.UnitName,
                                     Credit = tb.Credit,
                                     Debit = tb.Debit,
                                     LineCode = tb.LineCode,
                                 }).ToListAsync();



            //var journal = await (
            //    from a in context.JournalTemps
            //    join b in context.ChartOfAccounts on a.AccountId equals b.Id
            //    join c in
            //    (
            //         from bu in context.BusinessUnits
            //         join but in context.BusinessUnitTypes on bu.BusinessUnitTypeId equals but.Id
            //         where but.BusinessUnitLevel == Models.BusinessUnitLevelCode.Division
            //         select bu

            //    ) on a.DivisiId equals c.Id into empdiv
            //    from div in empdiv.DefaultIfEmpty(new Models.BusinessUnit())
            //    join e in context.LegalEntity on a.LegalId equals e.Id
            //    where a.ReferenceId == arid
            //    orderby int.Parse(a.LineCode ?? "0")
            //    select new
            //    {
            //        AccountCode = b.CodeRec,
            //        AccountName = b.Name5,
            //        AccountDescription = a.Description,
            //        DivisionName = div.UnitName,
            //        LegalName = e.LegalEntityName,
            //        Debet = a.Debit,
            //        a.Credit
            //    }).AsNoTracking().ToListAsync();
            return journal;
        }

        public async Task<object> GerListJournalAR(string ARid, string multipceid)
        {
            string arid = !string.IsNullOrWhiteSpace(multipceid) ? multipceid : ARid;
            var journal = await (from tb in context.Journal
                                 join coa in context.ChartOfAccounts on tb.AccountId equals coa.Id
                                 join bu in context.BusinessUnits on tb.DivisiId equals bu.Id into arc
                                 from buu in arc.DefaultIfEmpty()
                                 join job in context.JobDetail on tb.JobId equals job.Id into gjob
                                 from jobs in gjob.DefaultIfEmpty()
                                 where tb.ReferenceId == arid
                                 orderby tb.LineCode ascending
                                 select new Journal
                                 {
                                     AccountId = coa.Id,
                                     AccountCode = coa.CodeRec,
                                     AccountName = coa.Name5,
                                     JobId = tb.JobId,
                                     Description = tb.Description == null ? coa.Name5 : tb.Description,
                                     JobIdName = tb.JobId + "-" + jobs.JobName == null ? "" : jobs.JobName,
                                     DivisiId = tb.DivisiId,
                                     DivisiName = buu.UnitName,
                                     Credit = tb.Credit,
                                     Debit = tb.Debit,
                                     LineCode = tb.LineCode,
                                 }).ToListAsync();

            return journal;
        }
        public async Task<InvoiceClient> GetInvoiceInfo(string invid)
        {
            //var totalAmt = await (
            //    from a in context.InvoiceClients
            //    join b in context.Pces on a.PceId equals b.Id where a.Id == invid
            //    select a.TotalAmount).Sum();

            //join divisi in (
            //    from x in context.BusinessUnits
            //    join y in context.BusinessUnitTypes on x.BusinessUnitTypeId equals y.Id
            //    where y.BusinessUnitLevel == BusinessUnitLevelCode.Division
            //    select x
            //) on b.BusinessUnitDivisionId equals divisi.Id

            var totalAmt = await (from a in context.InvoiceClients where a.Id == invid select a.TotalAmount).SumAsync();

            var pce = await (from a in context.Pces
                             join b in context.JobDetail on a.JobId equals b.Id
                             join c in context.Currencies on a.CurrencyId equals c.Id
                             join d in context.MainServiceCategorys on a.MainserviceCategoryId equals d.Id
                             join e in context.LegalEntity on b.LegalEntityId equals e.Id
                             //join f in context.Organizations on e.organization_id equals f.Id
                             join h in context.CompanyBrands on b.BrandId equals h.Id
                             join i in context.Companys on h.CompanyId equals i.Id
                             join divisi in context.BusinessUnits on b.BusinessUnitDivisionId equals divisi.Id
                             join n in context.InvoiceClients on a.Id equals n.PceId
                             join o in context.Banks on n.AccountId equals o.Id
                             join p in context.AccountReceivables on n.Id equals p.Id into arc
                             from ar in arc.DefaultIfEmpty(new AccountReceivable())
                             where n.Id == invid
                             select new InvoiceClient
                             {
                                 Id = n.Id,
                                 InvoiceNumber = n.Id,
                                 AccountId = n.AccountId,
                                 AccountBankName = o.BankName,
                                 AccountBankNumber = o.AccountNumber,
                                 InvoiceDate = n.InvoiceDate,
                                 VatNo = n.VatNo,
                                 PceBalance = (n.GrandTotal >= a.Subtotal) ? 0 : (a.Subtotal - n.GrandTotal),
                                 //DueDate = n.DueDate,
                                 PceId = a.Id,
                                 CurrencyId = c.Id,
                                 CurrencyCode = c.CurrencyCode,
                                 RefNumber = n.RefNumber,
                                 YourRef = n.YourRef,
                                 ExchangeRate = n.ExchangeRate,
                                 TotalAmount = n.TotalAmount,
                                 GrandTotal = n.GrandTotal,
                                 Vat = n.Vat,
                                 Asf = n.Asf,
                                 AsfAmount = n.AsfAmount,
                                 BrandId = h.Id,
                                 ClientId = i.Id,
                                 LegalId = e.Id,
                                 BusinessUnitId = divisi.Id,
                                 MainServiceCategory = d.Id,
                                 ARStatus = ar.Status,
                                 PayDescription = ar.Description
                             }).AsNoTracking().FirstOrDefaultAsync();

            return pce;
        }
        public async Task<object> ReadInvDtl(string invid)
        {
            var totalAmt = (from a in context.InvoiceClientItemOthers
                            group a by a.TaskId into b
                            select new InvoiceClientItemOther() { TaskId = b.Key, Amount = b.Sum(c => c.Amount) }).ToList();

            var listitem = await (from a in context.InvoiceClientItemOthers
                                  join b in context.PceTasks on a.TaskId equals b.Id
                                  join c in context.InvoiceClients on a.InvoiceId equals c.Id
                                  join d in totalAmt on a.TaskId equals d.TaskId into e
                                  from f in e.DefaultIfEmpty(new InvoiceClientItemOther())
                                  where a.InvoiceId == invid
                                  select new InvoiceClientItemOther()
                                  {
                                      Id = a.Id,
                                      TaskId = b.Id,
                                      TaskName = b.TaskName,
                                      Quantity = b.Quantity,
                                      UnitPrice = b.UnitPrice,
                                      Total = b.Total,
                                      Amount = a.Amount,
                                      AmountBilled = f.Amount,
                                      Balance = (f.Amount > b.Total) ? 0 : (b.Total - f.Amount),
                                      PceID = c.PceId,
                                      AmountPercentage = a.AmountPercentage
                                  }
                          ).AsNoTracking().ToListAsync();

            //var listitem = await (from a in context.InvoiceClientItemOthers where a.InvoiceId == invid
            //                      join b in context.ChartOfAccounts on a.AccountID equals b.Id into lcoa
            //                      from coa in lcoa.DefaultIfEmpty(new Models.Finance.Master.ChartOfAccount())
            //                      select new InvoiceClientItemOther
            //                      {
            //                          AccountID = a.AccountID,
            //                          Amount = a.Amount,
            //                          Description = a.Description,
            //                          Id = a.Id,
            //                          InvoiceId = a.InvoiceId,
            //                          JobID = a.JobID,
            //                          ParentId = a.ParentId,
            //                          PceID = a.PceID,
            //                          Remark = a.Remark,
            //                          AccountName = coa.Name5
            //                      }

            //                      ).AsNoTracking().ToListAsync();
            return listitem;
        }

        public async Task<dynamic> GetChartOfAccountTradeAndTradeInter()
        {
            List<string> coaARTrade = new List<string>() { "4799430086143340", "8289918465030040" };
            var result = await context.ChartOfAccounts.Where(coa => coaARTrade.Contains(coa.Id)).OrderBy(x => x.CodeRec).Select(x => new { Text = x.Name5, Value = x.Id }).ToListAsync();
            return result;
        }

        public async Task<object> GetListStatement(string acct, string legal, string cl, string ms, string sd, string ed)
        {
            DateTime dtfrom = DateTime.ParseExact(sd, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture).Date;
            DateTime dtTo = DateTime.ParseExact(ed, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture).Date;
            List<string> coaARTrade = new List<string>() { "4799430086143340", "8289918465030040" };

            //var pcemutliplewithrowno = context.InvoiceClients.Where(x => x.MultiPceId != null).GroupBy(x => x.MultiPceId).SelectMany(g => g.Select((j, i) => new { j.MultiPceId, j.PceId, rn = i + 1 }));
            //.Union(from a in pcemutliplewithrowno where a.rn == 1 select new { Id = a.MultiPceId, PceId = a.PceId })

            var iv = await (from invoice in
                         (from a in context.InvoiceClients join b in context.Pces on a.PceId equals b.Id where a.MultiPceId != null select new { Id = a.MultiPceId, a.PceId, b.JobId })
                         .Union(from a in context.InvoiceClients join b in context.Pces on a.PceId equals b.Id where a.MultiPceId == null select new { a.Id, a.PceId, b.JobId })
                            select invoice).ToListAsync();

            var stateAR = await (
                from a in context.AccountReceivables
                join ic in iv on new { Id = a.AccountReceivableNumber, a.JobId } equals new { ic.Id, ic.JobId }
                join jb in context.JobDetail on a.JobId equals jb.Id
                join dv in context.BusinessUnits on jb.BusinessUnitDivisionId equals dv.Id
                join msc in context.MainServiceCategorys on jb.MainServiceCategory equals msc.Id
                join lgl in context.LegalEntity on jb.LegalEntityId equals lgl.Id
                join h in context.CompanyBrands on jb.BrandId equals h.Id
                join i in context.Companys on h.CompanyId equals i.Id
                join m in context.Journal on a.AccountReceivableNumber equals m.ReferenceId
                where coaARTrade.Contains(m.AccountId)
                    && m.AccountId == ((acct == "0") ? m.AccountId : acct)
                    && lgl.Id == ((legal == "0") ? lgl.Id : legal)
                    && i.Id == ((cl == "0") ? i.Id : cl)
                    && msc.Id == ((ms == "0") ? msc.Id : ms)
                    && (((DateTime)a.TransactionDate).Date >= dtfrom && ((DateTime)a.TransactionDate).Date <= dtTo)
                select new StatementAR()
                {
                    Id = a.Id,
                    InvoiceNumber = a.AccountReceivableNumber,
                    CompanyName = i.CompanyName,
                    CompanyBrand = h.BrandName,
                    TransactionDate = a.TransactionDate,
                    JobId = jb.Id,
                    JobName = jb.JobName,
                    Division = dv.UnitName,
                    Description = m.Description,
                    MainServiceCategory = msc.Name,
                    Debet = m.Debit,
                    Credit = m.Credit,
                    Balance = m.Debit,
                }).ToListAsync();

            return stateAR;
        }

        public async Task<object> GetListAging(string cl, string legal, string dv)
        {
            //DateTime dtfrom = DateTime.ParseExact(sd, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
            //DateTime dtTo = DateTime.ParseExact(ed, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);

            List<string> coaARTrade = new List<string>() { "4799430086143340", "8289918465030040" };

            var iv = await (
                from invoice in (
                    from a in context.InvoiceClients join b in context.Pces on a.PceId equals b.Id where a.MultiPceId != null select new { Id = a.MultiPceId, a.PceId, b.JobId })// , a.DueDate
                    .Union(from a in context.InvoiceClients join b in context.Pces on a.PceId equals b.Id where a.MultiPceId == null select new { a.Id, a.PceId, b.JobId })// , a.DueDate
                select invoice).ToListAsync();

            var agingAR = await (
                from a in context.AccountReceivables
                join ic in iv on new { Id = a.AccountReceivableNumber, a.JobId } equals new { ic.Id, ic.JobId }
                join jb in context.JobDetail on a.JobId equals jb.Id
                join div in context.BusinessUnits on jb.BusinessUnitDivisionId equals div.Id
                //join msc in context.MainServiceCategory on jb.MainServiceCategory equals msc.Id
                join lgl in context.LegalEntity on jb.LegalEntityId equals lgl.Id
                join h in context.CompanyBrands on jb.BrandId equals h.Id
                join i in context.Companys on h.CompanyId equals i.Id
                join m in context.Journal on a.AccountReceivableNumber equals m.ReferenceId
                join n in context.Pces on ic.PceId equals n.Id
                where lgl.Id == ((legal == "0") ? lgl.Id : legal)
                    && i.Id == ((cl == "0") ? i.Id : cl)
                    && div.Id == ((dv == "0") ? div.Id : dv)
                select new AgingAR()
                {
                    CompanyName = i.CompanyName,
                    InvoiceNumber = a.AccountReceivableNumber,
                    TransactionDate = a.TransactionDate,
                    //DueDate = ic.DueDate,
                    JobId = jb.Id,
                    JobName = jb.JobName,
                    Division = div.UnitName,
                    LegalEntity = lgl.LegalEntityName,
                    Description = m.Description,
                    Day = Convert.ToInt32((DateTime.Now.Date - Convert.ToDateTime(a.TransactionDate).Date).TotalDays),
                    Aging = Convert.ToInt32((DateTime.Now.Date - Convert.ToDateTime(a.TransactionDate).Date).TotalDays),
                    PONumber = n.PoClient
                }).ToListAsync();

            agingAR = (
                from a in agingAR
                group a by new { a.CompanyName, a.InvoiceNumber, a.TransactionDate, a.DueDate, a.Division, a.LegalEntity, a.Description } into gr
                select new AgingAR()
                {
                    CompanyName = gr.Key.CompanyName,
                    InvoiceNumber = gr.Key.InvoiceNumber,
                    TransactionDate = gr.Key.TransactionDate,
                    DueDate = gr.Key.DueDate,
                    Division = gr.Key.Division,
                    LegalEntity = gr.Key.LegalEntity,
                    Description = gr.Key.Description,
                    Day = Convert.ToInt32((DateTime.Now.Date - Convert.ToDateTime(gr.Key.TransactionDate).Date).TotalDays),
                    Aging = Convert.ToInt32((DateTime.Now.Date - Convert.ToDateTime(gr.Key.TransactionDate).Date).TotalDays),
                    JobId = string.Join(" ,", gr.Select(i => string.Format("{0}-{1}", i.JobId, i.JobName))),
                    PONumber = string.Join(" ,", gr.Where(s => !string.IsNullOrEmpty(s.PONumber)).Select(t => t.PONumber).ToList())
                }).ToList();

            return agingAR;
        }

        public async Task<AccountReceivable> Get(string Id)
        {
            return await context.AccountReceivables.Where(x => x.IsDeleted.Equals(false) && x.Id == Id).FirstOrDefaultAsync();
        }
    }

}
