﻿using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Attributes;
using Phoenix.Data.Models;
using Phoenix.Data.Models.Finance.Transaction;
using Phoenix.Data.Services.Um;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace Phoenix.Data.Services.Finance.Transaction
{
    public interface IInvoiceClientService : IDataService<InvoiceClient>
    {
        //Task<List<InvoiceClientDetail>> GetDetailList();
        Task<object> GetPceList(string status, string legal);
        Task<List<InvoiceClient>> GetPceLists(string status, string legal);
        Task<dynamic> GetPceInfo(string pceno);
        Task<InvoiceClient> GetCompanyInfo(string companyid);
        Task<InvoiceClient> GetAffiliation(string divisiid);
        Task<InvoiceClient> GetInvoiceInfo(string pceno, string invNo);
        Task<List<PceTask>> GetDetailFromPceList(string pceno);
        Task<InvoiceClient> GetInvoice(string invid);
        Task<dynamic> GetInvoiceProduction(string invoiceId, string pceid);
        Task<List<InvoiceClientItemOther>> GetInvoiceProductionDetail(string pceid, string invNo);
        Task<List<InvoiceClientItemOther>> ReadInvDtlHistory(string invoiceId, string pceid);
        Task<InvoiceClient> InsertInvoiceClient(InvoiceClient model);
        Task<InvoiceClient> EditAsyncData(InvoiceClient model);
        Task<int> InsertAndSubmitInvoiceClient(InvoiceClient model);
        Task<dynamic> GetDDLBank(string org);
        Task<InvoiceClient> Get(string Id);
        Task<object> GetPceChoiceList(string legalid, string divid, string mservice, string clientt, string currency, string pceId, string jobId);
        Task<InvoiceClient> Approve(InvoiceClient entity);
        Task<InvoiceClient> ForceClose(InvoiceClient entity);
        Task<InvoiceClient> Reject(InvoiceClient entity);
        Task<int> EditAndSubmitAsync(InvoiceClient entity);
        Task<int> CreateJournalWithSP(string Id);
        Task<List<Journal>> GetDataJournal(string Id);
    }
    public class InvoiceClientService : IInvoiceClientService
    {
        readonly DataContext context;
        readonly GlobalFunctionApproval globalFcApproval;
        readonly IManageMenuService menuService;
        readonly StatusLogService log;
        readonly FileService uploadFile;
        public InvoiceClientService(DataContext context, GlobalFunctionApproval globalFcApproval, IManageMenuService menuService, StatusLogService log, FileService uploadFile)
        {
            this.context = context;
            this.globalFcApproval = globalFcApproval;
            this.menuService = menuService;
            this.log = log;
            this.uploadFile = uploadFile;
        }

        //Read

        public async Task<List<InvoiceClient>> GetPceLists(string status, string legal)
        {
            //var invoices = from ic in context.InvoiceClients where ic.MultiPceId == null group ic by new { ic.PceId } into ics select new { ics.Key.PceId, TotalPenagihan = ics.Sum(x => x.GrandTotal) };
            //var invoicemultipce = (from ic in context.InvoiceClients where ic.MultiPceId != null group ic by new { ic.PceId } into ics select ics.Key.PceId).ToList();
            //if (invoicemultipce == null)
            //    invoicemultipce = new List<string>();

            //InvoiceNumber
            var data = await (
                from a in context.InvoiceClients
                join job in context.JobDetail on a.JobId equals job.Id into dj
                from job in dj.DefaultIfEmpty()
                    //join d in context.CompanyBrands on a.BrandId equals d.Id
                    //join e in context.Companys on d.CompanyId equals e.Id
                    //join f in context.MainServiceCategorys on job.MainServiceCategory equals f.Id
                    //join g in context.LegalEntity on job.LegalEntityId equals g.Id
                join h in context.BusinessUnits on a.BusinessUnitId equals h.Id
                join i in context.Banks on a.AccountId equals i.Id
                where a.MultiPceId == null// && a.Status != SingleInvoiceClientStatus.Closed.ToString()
                orderby a.CreatedOn descending
                select new InvoiceClient
                {
                    Id = a.Id,
                    PceId = a.PceId,
                    InvoiceDate = a.InvoiceDate,
                    JobId = a.JobId,
                    JobName = a.JobName,
                    //ClientName = e.CompanyName,
                    GrandTotal = a.GrandTotal,
                    AccountBankName = i.BankName,
                    AccountBankNumber = i.AccountNumber,
                    //LegalEntityName = g.LegalEntityName,
                    LegalId = a.LegalId,
                    DivisionName = h.UnitName,
                    BusinessUnitId = a.BusinessUnitId,
                    Status = a.Status,
                    StatusName = SingleInvoiceClientStatus.StatusName(Convert.ToInt32(a.Status)),
                    //MainServiceCategory = f.Name,
                    CreatedOn = a.CreatedOn,

                    //invcl.Id,
                    //invcl.PceId,
                    //invcl.InvoiceDate,
                    //pce.JobId,
                    //b.JobName,
                    //ClientName = g.CompanyName,
                    //invcl.GrandTotal,
                    //AccountBankName = i.BankName,
                    //AccountBankNumber = i.AccountNumber,
                    //LegalEntity = c.LegalEntityName,
                    //Division = h.UnitName,
                    //Status = invc.TotalPenagihan >= pce.GrandTotal ? "Closed" : "Balance",
                    //MainServiceCategory = d.Name,
                    //pce.CreatedOn

                }).ToListAsync();
            if (!string.IsNullOrEmpty(status))
            {
                data = data.Where(a => a.Status == status).ToList();
            }
            if (!string.IsNullOrEmpty(legal))
            {
                data = data.Where(a => a.LegalId == legal).ToList();
            }
            return data;
        }
        public async Task<object> GetPceList(string status, string legal)
        {
            //var invoices = from ic in context.InvoiceClients where ic.MultiPceId == null group ic by new { ic.PceId } into ics select new { ics.Key.PceId, TotalPenagihan = ics.Sum(x => x.GrandTotal) };
            //var invoicemultipce = (from ic in context.InvoiceClients where ic.MultiPceId != null group ic by new { ic.PceId } into ics select ics.Key.PceId).ToList();
            //if (invoicemultipce == null)
            //    invoicemultipce = new List<string>();

            //InvoiceNumber
            var datapces = (
                from pce in context.InvoiceClients
                join job in context.JobDetail on pce.JobId equals job.Id
                join d in context.CompanyBrands on pce.BrandId equals d.Id
                join e in context.Companys on d.CompanyId equals e.Id
                join f in context.MainServiceCategorys on job.MainServiceCategory equals f.Id
                join g in context.LegalEntity on job.LegalEntityId equals g.Id
                join h in context.BusinessUnits on job.BusinessUnitDivisionId equals h.Id
                join i in context.Banks on pce.AccountId equals i.Id
                //where g.Id == (string.IsNullOrEmpty(legal) ? g.Id : legal)
                //&& string.IsNullOrEmpty(pce.MultiPceId)
                orderby pce.CreatedOn descending
                select new
                {
                    Id = pce.Id,
                    PceId = pce.PceId,
                    InvoiceDate = pce.InvoiceDate,
                    pce.JobId,
                    job.JobName,
                    ClientName = e.CompanyName,
                    GrandTotal = pce.GrandTotal,
                    AccountBankName = i.BankName,
                    AccountBankNumber = i.AccountNumber,
                    LegalEntity = g.LegalEntityName,
                    Division = h.UnitName,
                    Status = pce.Status,
                    MainServiceCategory = f.Name,
                    pce.CreatedOn,

                    //invcl.Id,
                    //invcl.PceId,
                    //invcl.InvoiceDate,
                    //pce.JobId,
                    //b.JobName,
                    //ClientName = g.CompanyName,
                    //invcl.GrandTotal,
                    //AccountBankName = i.BankName,
                    //AccountBankNumber = i.AccountNumber,
                    //LegalEntity = c.LegalEntityName,
                    //Division = h.UnitName,
                    //Status = invc.TotalPenagihan >= pce.GrandTotal ? "Closed" : "Balance",
                    //MainServiceCategory = d.Name,
                    //pce.CreatedOn

                });

            //var return_data = datapces.Where(x => !exist_invoice.Any(y => y.PceId == x.PceId)).ToList();
            //return_data.AddRange(exist_invoice);
            var return_data = datapces.ToList();
            //var hasil = return_data.OrderByDescending(x => x.CreatedOn)
            //    .Where(x => x.Status == (string.IsNullOrEmpty(status) ? x.Status : status));
            return return_data;


            //var invoices = from ic in context.InvoiceClients where ic.MultiPceId == null group ic by new { ic.PceId } into ics select new { ics.Key.PceId, TotalPenagihan = ics.Sum(x => x.GrandTotal) };
            //var invoicemultipce = (from ic in context.InvoiceClients where ic.MultiPceId != null group ic by new { ic.PceId } into ics select ics.Key.PceId).ToList();
            //if (invoicemultipce == null)
            //    invoicemultipce = new List<string>();

            ////InvoiceNumber
            //var datapces = (
            //    from pce in context.Pces
            //    join job in context.JobDetail on pce.JobId equals job.Id
            //    join d in context.CompanyBrands on job.BrandId equals d.Id
            //    join e in context.Companys on d.CompanyId equals e.Id
            //    join f in context.MainServiceCategorys on pce.MainserviceCategoryId equals f.Id
            //    join g in context.LegalEntity on job.LegalEntityId equals g.Id
            //    join h in context.BusinessUnits on job.BusinessUnitDivisionId equals h.Id
            //    where (pce.Status.ToLower() == StatusTransactionName.Approved) && g.Id == (string.IsNullOrEmpty(legal) ? g.Id : legal)
            //        && !invoicemultipce.Contains(pce.Id)
            //    orderby pce.CreatedOn descending
            //    select new
            //    {
            //        Id = "",
            //        PceId = pce.Id,
            //        InvoiceDate = (DateTime?)null,
            //        pce.JobId,
            //        job.JobName,
            //        ClientName = e.CompanyName,
            //        GrandTotal = pce.Total,
            //        AccountBankName = "",
            //        AccountBankNumber = "",
            //        LegalEntity = g.LegalEntityName,
            //        Division = h.UnitName,
            //        Status = "Open",
            //        MainServiceCategory = f.Name,
            //        pce.CreatedOn
            //    });

            //var exist_invoice = (
            //    from pce in datapces
            //    join invc in invoices on pce.PceId equals invc.PceId
            //    join invcl in context.InvoiceClients on invc.PceId equals invcl.PceId
            //    join b in context.JobDetail on pce.JobId equals b.Id
            //    join c in context.LegalEntity on b.LegalEntityId equals c.Id
            //    join d in context.MainServiceCategorys on b.MainServiceCategory equals d.Id
            //    join f in context.CompanyBrands on b.BrandId equals f.Id
            //    join g in context.Companys on f.CompanyId equals g.Id
            //    join h in context.BusinessUnits on b.BusinessUnitDivisionId equals h.Id
            //    join i in context.Banks on invcl.AccountId equals i.Id
            //    select new
            //    {
            //        invcl.Id,
            //        invcl.PceId,
            //        invcl.InvoiceDate,
            //        pce.JobId,
            //        b.JobName,
            //        ClientName = g.CompanyName,
            //        invcl.GrandTotal,
            //        AccountBankName = i.BankName,
            //        AccountBankNumber = i.AccountNumber,
            //        LegalEntity = c.LegalEntityName,
            //        Division = h.UnitName,
            //        Status = invc.TotalPenagihan >= pce.GrandTotal ? "Closed" : "Balance",
            //        MainServiceCategory = d.Name,
            //        pce.CreatedOn
            //    });
            ////UpdateDate = created_pce.MaxDateByPce

            //var return_data = datapces.Where(x => !exist_invoice.Any(y => y.PceId == x.PceId)).ToList();
            //return_data.AddRange(exist_invoice);

            //var hasil = return_data.OrderByDescending(x => x.CreatedOn)
            //    .Where(x => x.Status == (string.IsNullOrEmpty(status) ? x.Status : status));
            //return hasil;
        }

        public async Task<dynamic> GetDDLBank(string org)
        {
            return await (from a in context.Banks
                          where //a.OrganizationId == org && 
                          a.IsActive == true
                          select new
                          {
                              Text = a.BankName + "-" + a.AccountNumber,
                              Value = a.Id
                          }).AsNoTracking().ToListAsync();
        }

        public async Task<InvoiceClient> GetdataVendor(string brandId)
        {
            return await (from a in context.CompanyBrands
                          join b in context.Companys on a.CompanyId equals b.Id
                          where !(a.IsDeleted ?? false)
                                && a.Id == brandId
                          select new InvoiceClient
                          {
                              ClientPIC = a.ContactName,
                              ClientTaxNumber = b.TaxRegistrationNumber,
                              ClientInvAddress = a.BrandAddress,
                              TermOfPayment = a.TermOfPayment
                          }).FirstOrDefaultAsync();
        }

        public async Task<dynamic> GetPceInfo(string pceno)
        {
            //var total_tagihan = (from ic in context.InvoiceClients where ic.PceId == pce.PceId select ic.TotalAmount).ToList().Sum();
            var totalAmtn = await (
                from a in context.InvoiceClients
                join b in context.Pces on a.PceId equals b.Id
                where b.Code == pceno && a.MultiPceId == null
                select a.GrandTotal).ToListAsync();

            var totalAmt = totalAmtn.Sum();

            var pce = await (
                from a in context.Pces
                join b in context.JobDetail on a.JobId equals b.Id
                join c in context.Currencies on a.CurrencyId equals c.Id
                join d in context.MainServiceCategorys on a.MainserviceCategoryId equals d.Id
                join e in context.LegalEntity on b.LegalEntityId equals e.Id
                join h in context.CompanyBrands on b.BrandId equals h.Id
                join i in context.Companys on h.CompanyId equals i.Id
                join divisi in (
                    from x in context.BusinessUnits
                    join y in context.BusinessUnitTypes on x.BusinessUnitTypeId equals y.Id
                    where y.BusinessUnitLevel == BusinessUnitLevelCode.Division
                    select x
                ) on b.BusinessUnitDivisionId equals divisi.Id
                join m in context.Affiliations on b.AffiliationId equals m.Id
                where a.Code == pceno
                select new
                {
                    PceId = a.Id,
                    PceCode = a.Code,
                    PceSubtotal = a.Subtotal,
                    PceOtherFee = a.OtherFee,
                    PceVat = a.Vat,
                    PceVatPercentage = Math.Ceiling((decimal)((a.Vat / a.Total) * 100)),
                    PceGrandTotal = a.Total,
                    PceBalance = (totalAmt > a.Total) ? 0 : a.Total - totalAmt,
                    InvoiceBilled = totalAmt,
                    CurrencyId = c.Id,
                    c.CurrencyCode,
                    MainServiceCategory = d.Name,
                    a.JobId,
                    b.JobName,
                    LegalId = b.LegalEntityId,
                    LegalEntity = e.LegalEntityName,
                    BrandId = h.Id,
                    TermOfPayment = h.TermOfPayment,
                    //OrganizationId = f.Id,
                    //Brand = f.OrganizationName,
                    POClientNo = a.PoClient,
                    JobStatus = b.JobStatusId,
                    Division = divisi.UnitName,
                    DivisionId = divisi.Id,
                    ClientId = i.Id,
                    ClientName = i.CompanyName,
                    ClientBrandName = h.BrandName,
                    ClientInvAddress = h.BrandAddress,
                    ClientTaxNumber = i.TaxRegistrationNumber,
                    ClientPIC = h.ContactName,
                    Asf = a.OtherFeePercentage,
                    AsfAmount = a.OtherFee,
                    AffiliationName = m.AffiliationName,
                    AffiliationId = b.AffiliationId

                }).AsNoTracking().FirstOrDefaultAsync();

            return pce;
        }

        public async Task<InvoiceClient> GetInvoiceInfo(string pceno, string invNo)
        {
            var listData = new InvoiceClient();
            if (pceno != "null")
            {
                var invoiceOther = await (from a in context.InvoiceClientItemOthers where a.InvoiceId == invNo select a.AmountBilled).ToListAsync();
                var totlBilled = invoiceOther.Sum();
                if (pceno == "fromAR")
                {
                    listData = await (from z in context.InvoiceClients
                                      join c in context.Currencies on z.CurrencyId equals c.Id
                                      join d in context.MainServiceCategorys on z.MainServiceCategory equals d.Id
                                      join e in context.LegalEntity on z.LegalId equals e.Id
                                      join h in context.CompanyBrands on z.BrandId equals h.Id
                                      join i in context.Companys on z.ClientId equals i.Id
                                      join divisi in context.BusinessUnits on z.BusinessUnitId equals divisi.Id
                                      join m in context.Affiliations on z.AffiliationId equals m.Id
                                      join bank in context.Banks on z.AccountId equals bank.Id
                                      join job in context.JobDetail on z.JobId equals job.Id
                                      where z.Id == invNo && z.MultiPceId == null
                                      select new InvoiceClient
                                      {
                                          Remarks = z.Remarks,
                                          PayDescription = z.PayDescription,
                                          InvoiceNumber = z.Id,
                                          TotalAmount = z.TotalAmount,//totlBilled,
                                          Asf = z.Asf,
                                          AsfAmount = 0,// (a.OtherFeePercentage / 100) * totlBilled,
                                          totalamtplusasf = z.TotalAmount,//((a.OtherFeePercentage / 100) * totlBilled) + totlBilled,
                                          Vat = z.Vat,
                                          GrandTotal = z.GrandTotal,//(((a.OtherFeePercentage / 100) * totlBilled) + totlBilled) + z.Vat,
                                          PceGrandTotal = z.GrandTotal,//(((a.OtherFeePercentage / 100) * totlBilled) + totlBilled) + z.Vat,
                                          PceBalance = z.TotalAmount - totlBilled,
                                          InvoiceBilled = totlBilled,
                                          AccountId = z.AccountId,
                                          AccountBankNumber = bank.AccountNumber,
                                          Id = z.Id,
                                          VatNo = z.VatNo,
                                          InvoiceDate = z.InvoiceDate,
                                          //z.DueDate,
                                          RefNumber = z.RefNumber,
                                          YourRef = z.YourRef,
                                          ExchangeRate = z.ExchangeRate,
                                          PceId = z.Id,
                                          PceCode = z.PceId,
                                          PceSubtotal = 0,//a.Subtotal,
                                          PceOtherFee = z.PceOtherFee,
                                          PceVat = z.Vat,
                                          PceVatPercentage = Math.Ceiling((decimal)((z.Vat / z.GrandTotal) * 100)),
                                          CurrencyId = c.Id,
                                          CurrencyCode = c.CurrencyCode,
                                          MainServiceCategory = z.MainServiceCategory,
                                          MainServiceCategoryName = d.Name,
                                          JobId = z.JobId,
                                          JobName = job.JobName,
                                          LegalId = z.LegalId,
                                          LegalEntityName = e.LegalEntityName,
                                          BrandId = h.Id,
                                          TermOfPayment = h.TermOfPayment,
                                          POClientNo = z.POClientNo,
                                          JobStatus = job.JobStatusId,
                                          Division = divisi.UnitName,
                                          BusinessUnitId = divisi.Id,
                                          ClientId = i.Id,
                                          ClientName = i.CompanyName,
                                          ClientBrandName = h.BrandName,
                                          ClientInvAddress = h.BrandAddress,
                                          ClientTaxNumber = i.TaxRegistrationNumber,
                                          ClientPIC = h.ContactName,
                                          AffiliationName = m.AffiliationName,
                                          AffiliationId = z.AffiliationId,
                                          Status = z.Status,
                                          ApprovedBy = z.ApprovedBy,
                                          ApprovedOn = z.ApprovedOn,
                                          CreatedBy = z.CreatedBy,
                                          CreatedOn = z.CreatedOn,
                                          DeletedBy = z.DeletedBy,
                                          DeletedOn = z.DeletedOn,
                                          IsActive = z.IsActive,
                                          IsDefault = z.IsDefault,
                                          IsDeleted = z.IsDeleted,
                                          IsLocked = z.IsLocked,
                                          ModifiedBy = z.ModifiedBy,
                                          ModifiedOn = z.ModifiedOn,
                                          OwnerId = z.OwnerId,
                                          PoDocFileId = z.PoDocFileId,
                                          VatDocFileId = z.VatDocFileId
                                      }).AsNoTracking().FirstOrDefaultAsync();
                }
                else
                {
                    listData = await (
                    from a in context.Pces
                    join z in context.InvoiceClients on a.Id equals z.PceId
                    join b in context.JobDetail on a.JobId equals b.Id
                    join c in context.Currencies on a.CurrencyId equals c.Id
                    join d in context.MainServiceCategorys on a.MainserviceCategoryId equals d.Id
                    join e in context.LegalEntity on b.LegalEntityId equals e.Id
                    //join f in context.Organizations on e.OrganizationId equals f.Id
                    join h in context.CompanyBrands on b.BrandId equals h.Id
                    join i in context.Companys on h.CompanyId equals i.Id
                    join divisi in (
                        from x in context.BusinessUnits
                        join y in context.BusinessUnitTypes on x.BusinessUnitTypeId equals y.Id
                        where y.BusinessUnitLevel == BusinessUnitLevelCode.Division
                        select x
                    ) on b.BusinessUnitDivisionId equals divisi.Id
                    join m in context.Affiliations on b.AffiliationId equals m.Id
                    join bank in context.Banks on z.AccountId equals bank.Id
                    where a.Code == pceno && z.Id == invNo && z.MultiPceId == null
                    select new InvoiceClient
                    {
                        Remarks = z.Remarks,
                        PayDescription = z.PayDescription,
                        InvoiceNumber = z.Id,
                        TotalAmount = totlBilled,//z.TotalAmount,//totlBilled,
                        Asf = a.OtherFeePercentage,
                        AsfAmount = (a.OtherFeePercentage / 100) * totlBilled,
                        totalamtplusasf = ((a.OtherFeePercentage / 100) * totlBilled) + totlBilled,
                        Vat = z.Vat,
                        GrandTotal = z.GrandTotal,//(((a.OtherFeePercentage / 100) * totlBilled) + totlBilled) + z.Vat,
                        PceGrandTotal = z.GrandTotal,//(((a.OtherFeePercentage / 100) * totlBilled) + totlBilled) + z.Vat,
                        PceBalance = a.Total - totlBilled,
                        InvoiceBilled = totlBilled,
                        AccountId = z.AccountId,
                        AccountBankNumber = bank.AccountNumber,
                        Id = z.Id,
                        VatNo = z.VatNo,
                        InvoiceDate = z.InvoiceDate,
                        //z.DueDate,
                        RefNumber = z.RefNumber,
                        YourRef = z.YourRef,
                        ExchangeRate = z.ExchangeRate,
                        PceId = a.Id,
                        PceCode = a.Code,
                        PceSubtotal = a.Subtotal,
                        PceOtherFee = a.OtherFee,
                        PceVat = a.Vat,
                        PceVatPercentage = Math.Ceiling((decimal)((a.Vat / a.Total) * 100)),

                        CurrencyId = c.Id,
                        CurrencyCode = c.CurrencyCode,
                        MainServiceCategory = z.MainServiceCategory,
                        MainServiceCategoryName = d.Name,
                        JobId = a.JobId,
                        JobName = b.JobName,
                        LegalId = b.LegalEntityId,
                        LegalEntityName = e.LegalEntityName,
                        BrandId = h.Id,
                        TermOfPayment = h.TermOfPayment,
                        //OrganizationId = f.Id,
                        //Brand = f.OrganizationName,
                        POClientNo = z.POClientNo,
                        JobStatus = b.JobStatusId,
                        Division = divisi.UnitName,
                        BusinessUnitId = divisi.Id,
                        ClientId = i.Id,
                        ClientName = i.CompanyName,
                        ClientBrandName = h.BrandName,
                        ClientInvAddress = h.BrandAddress,
                        ClientTaxNumber = i.TaxRegistrationNumber,
                        ClientPIC = h.ContactName,

                        AffiliationName = m.AffiliationName,
                        AffiliationId = b.AffiliationId,
                        Status = z.Status,

                        ApprovedBy = z.ApprovedBy,
                        ApprovedOn = z.ApprovedOn,
                        CreatedBy = z.CreatedBy,
                        CreatedOn = z.CreatedOn,
                        DeletedBy = z.DeletedBy,
                        DeletedOn = z.DeletedOn,
                        IsActive = z.IsActive,
                        IsDefault = z.IsDefault,
                        IsDeleted = z.IsDeleted,
                        IsLocked = z.IsLocked,
                        ModifiedBy = z.ModifiedBy,
                        ModifiedOn = z.ModifiedOn,
                        OwnerId = z.OwnerId,

                        PoDocFileId = z.PoDocFileId,
                        VatDocFileId = z.VatDocFileId
                    }).AsNoTracking().FirstOrDefaultAsync();
                }
            }
            else
            {
                var invoiceOther = await (from a in context.InvoiceClientItemOthers where a.InvoiceId == invNo select a.AmountBilled).ToListAsync();
                var totlBilled = invoiceOther.Sum();
                listData = await (from z in context.InvoiceClients
                                  join c in context.Currencies on z.CurrencyId equals c.Id
                                  join d in context.MainServiceCategorys on z.MainServiceCategory equals d.Id
                                  join e in context.LegalEntity on z.LegalId equals e.Id
                                  join h in context.CompanyBrands on z.BrandId equals h.Id
                                  join i in context.Companys on z.ClientId equals i.Id
                                  join divisi in context.BusinessUnits on z.BusinessUnitId equals divisi.Id
                                  join m in context.Affiliations on z.AffiliationId equals m.Id
                                  join bank in context.Banks on z.AccountId equals bank.Id
                                  where z.Id == invNo && z.MultiPceId == null
                                  select new InvoiceClient
                                  {
                                      Remarks = z.Remarks,
                                      PayDescription = z.PayDescription,
                                      InvoiceNumber = z.Id,
                                      TotalAmount = z.TotalAmount,//totlBilled,
                                      Asf = z.Asf,
                                      AsfAmount = 0,// (a.OtherFeePercentage / 100) * totlBilled,
                                      totalamtplusasf = z.TotalAmount,//((a.OtherFeePercentage / 100) * totlBilled) + totlBilled,
                                      Vat = z.Vat,
                                      GrandTotal = z.GrandTotal,//(((a.OtherFeePercentage / 100) * totlBilled) + totlBilled) + z.Vat,
                                      PceGrandTotal = z.GrandTotal,//(((a.OtherFeePercentage / 100) * totlBilled) + totlBilled) + z.Vat,
                                      PceBalance = z.TotalAmount - totlBilled,
                                      InvoiceBilled = totlBilled,
                                      AccountId = z.AccountId,
                                      AccountBankNumber = bank.AccountNumber,
                                      Id = z.Id,
                                      VatNo = z.VatNo,
                                      InvoiceDate = z.InvoiceDate,
                                      //z.DueDate,
                                      RefNumber = z.RefNumber,
                                      YourRef = z.YourRef,
                                      ExchangeRate = z.ExchangeRate,
                                      PceId = "",//a.Id,
                                      PceCode = "",//a.Code,
                                      PceSubtotal = 0,//a.Subtotal,
                                      PceOtherFee = z.PceOtherFee,
                                      PceVat = z.Vat,
                                      PceVatPercentage = Math.Ceiling((decimal)((z.Vat / z.GrandTotal) * 100)),

                                      CurrencyId = c.Id,
                                      CurrencyCode = c.CurrencyCode,
                                      MainServiceCategory = z.MainServiceCategory,
                                      MainServiceCategoryName = d.Name,
                                      JobId = "",
                                      JobName = "",
                                      LegalId = z.LegalId,
                                      LegalEntityName = e.LegalEntityName,
                                      BrandId = h.Id,
                                      TermOfPayment = h.TermOfPayment,
                                      //OrganizationId = f.Id,
                                      //Brand = f.OrganizationName,
                                      POClientNo = z.POClientNo,
                                      JobStatus = "",//b.JobStatusId,
                                      Division = divisi.UnitName,
                                      BusinessUnitId = divisi.Id,
                                      ClientId = i.Id,
                                      ClientName = i.CompanyName,
                                      ClientBrandName = h.BrandName,
                                      ClientInvAddress = h.BrandAddress,
                                      ClientTaxNumber = i.TaxRegistrationNumber,
                                      ClientPIC = h.ContactName,

                                      AffiliationName = m.AffiliationName,
                                      AffiliationId = z.AffiliationId,
                                      Status = z.Status,

                                      ApprovedBy = z.ApprovedBy,
                                      ApprovedOn = z.ApprovedOn,
                                      CreatedBy = z.CreatedBy,
                                      CreatedOn = z.CreatedOn,
                                      DeletedBy = z.DeletedBy,
                                      DeletedOn = z.DeletedOn,
                                      IsActive = z.IsActive,
                                      IsDefault = z.IsDefault,
                                      IsDeleted = z.IsDeleted,
                                      IsLocked = z.IsLocked,
                                      ModifiedBy = z.ModifiedBy,
                                      ModifiedOn = z.ModifiedOn,
                                      OwnerId = z.OwnerId,
                                      PoDocFileId = z.PoDocFileId,
                                      VatDocFileId = z.VatDocFileId
                                  }).AsNoTracking().FirstOrDefaultAsync();
            }

            return await GetFile(listData);

        }

        private async Task<InvoiceClient> GetFile(InvoiceClient data)
        {
            if (data.PoDocFileId != null)
            {
                var a = await context.Filemasters.Where(x => x.Id == data.PoDocFileId).FirstOrDefaultAsync();
                if (a != null) data.PoDocFileName = a.Name;
            }

            if (data.VatDocFileId != null)
            {
                var b = await context.Filemasters.Where(x => x.Id == data.VatDocFileId).FirstOrDefaultAsync();
                if (b != null) data.VatDocFileName = b.Name;
            }
            return data;
        }

        public async Task<object> GetPceChoiceList(string legalid, string divid, string mservice, string clientt, string currency, string pceId, string jobId)
        {
            List<string> pceid = new List<string>();
            pceid = await (from a in context.InvoiceClients where a.MultiPceId == null select a.PceId).AsNoTracking().ToListAsync();
            var pces = await (from b in context.Pces
                              join c in context.JobDetail on b.JobId equals c.Id
                              join d in context.LegalEntity on c.LegalEntityId equals d.Id
                              join e in context.MainServiceCategorys on c.MainServiceCategory equals e.Id
                              join divisi in (
                                 from x in context.BusinessUnits
                                 join y in context.BusinessUnitTypes on x.BusinessUnitTypeId equals y.Id
                                 where y.BusinessUnitLevel == BusinessUnitLevelCode.Division
                                 select x
                             ) on c.BusinessUnitDivisionId equals divisi.Id
                              join g in context.CompanyBrands on c.BrandId equals g.Id
                              join h in context.Companys on g.CompanyId equals h.Id
                              join m in context.Affiliations on c.AffiliationId equals m.Id
                              join n in context.Currencies on b.CurrencyId equals n.Id
                              where b.Status == StatusTransactionName.Approved
                                  && c.LegalEntityId == (legalid == "0" ? c.LegalEntityId : legalid)
                                  && c.BusinessUnitDivisionId == (divid == "0" ? c.BusinessUnitDivisionId : divid)
                                  && c.MainServiceCategory == (mservice == "0" ? c.MainServiceCategory : mservice)
                                  && h.Id == (clientt == "0" ? h.Id : clientt)
                                  && b.CurrencyId == (currency == "0" ? b.CurrencyId : currency)
                                  && b.Id.Contains(pceId == "-" ? "" : pceId) && c.JobName.Contains(jobId == "-" ? "" : jobId)
                                  && b.PceReference == null
                              select new
                              {
                                  PceId = b.Id,
                                  PceCode = b.Code,
                                  PceSubtotal = b.Subtotal,
                                  PceOtherFee = b.OtherFee,
                                  PceVat = b.Vat,
                                  PceGrandTotal = b.Total,
                                  TotalAmount = (b.Subtotal + b.OtherFee),
                                  GrandTotal = b.Total,
                                  ClientName = h.CompanyName,
                                  ClientBrandName = g.BrandName,
                                  Status = "Open",
                                  BillPercent = 100,
                                  InvoiceBilled = (b.Subtotal + b.OtherFee),
                                  PceBalance = (b.Subtotal + b.OtherFee),
                                  JobId = c.Id,
                                  JobName = c.JobName,
                                  JobStatus = c.JobStatusId,
                                  LegalId = d.Id,
                                  LegalEntity = d.LegalEntityName,
                                  BusinessUnitId = divisi.Id,
                                  Division = divisi.UnitName,
                                  ClientId = h.Id,
                                  BrandId = g.Id,
                                  b.CurrencyId,
                                  CurrencyCode = n.CurrencyCode,
                                  MainServiceCategory = e.Id,
                                  MainServiceCategoryName = e.Name,
                                  b.CreatedOn,
                                  ExchangeRate = b.CurrencyId == CurrencyIDR.CurrencyId ? 1 : 0,
                                  PceVatPercentage = Math.Ceiling((decimal)((b.Vat / b.Total) * 100)),
                                  TermOfPayment = g.TermOfPayment,
                                  POClientNo = b.PoClient,
                                  ClientInvAddress = g.BrandAddress,
                                  ClientTaxNumber = h.TaxRegistrationNumber,
                                  ClientPIC = g.ContactName,
                                  Asf = b.OtherFeePercentage,
                                  AsfAmount = b.OtherFee,
                                  AffiliationName = m.AffiliationName,
                                  AffiliationId = c.AffiliationId,

                              }).AsNoTracking().ToListAsync();

            var return_data = pces.Where(x => !pceid.Any(y => y == x.PceId)).ToList();
            return return_data.OrderByDescending(x => x.CreatedOn);
        }

        public async Task<InvoiceClient> GetCompanyInfo(string companyid)
        {
            var company = await (
                from a in context.Companys
                join b in context.CompanyBrands on a.Id equals b.CompanyId
                where b.Id == companyid
                select new InvoiceClient
                {
                    ClientPIC = b.ContactName,
                    ClientTaxNumber = a.TaxRegistrationNumber,
                    ClientInvAddress = b.BrandAddress,
                    TermOfPayment = b.TermOfPayment,
                    Asf = b.ASFValue == null ? 0 : b.ASFValue
                }).AsNoTracking().FirstOrDefaultAsync();
            return company;
        }

        public async Task<InvoiceClient> GetAffiliation(string divisiid)
        {
            var company = await (
                from a in context.BusinessUnits
                where a.Id == divisiid
                select new InvoiceClient
                {
                    AffiliationId = a.AffiliationId
                }).AsNoTracking().FirstOrDefaultAsync();
            return company;
        }


        public async Task<List<PceTask>> GetDetailFromPceList(string pceno)
        {
            var pceDtl = await (
                from a in context.Pces
                join b in context.PceTasks on a.Id equals b.PceId
                where a.Code == pceno
                select b).AsNoTracking().ToListAsync();

            return pceDtl;
            //return new { pce = pce, comp = company, pcedtl = pceDtl};
        }

        public async Task<InvoiceClient> GetInvoice(string invid)
        {
            var inv = await (
                from a in context.InvoiceClients
                where a.Id == invid
                select a).AsNoTracking().FirstOrDefaultAsync();
            return inv;
        }

        public async Task<dynamic> GetInvoiceProduction(string invoiceId, string pceid)
        {
            var invNo = await (from a in context.InvoiceClients where a.PceId == pceid && a.MultiPceId == null select a.Id).ToListAsync();
            //var totalbilledamount = from a in context.InvoiceClientItemOthers where invNo.Contains(a.InvoiceId) group a by new { a.TaskId } into ics select new { ics.Key.TaskId, Billed = ics.Sum(x => x.AmountBilled) };

            var totalAmtt = (
                from a in context.InvoiceClientItemOthers
                where invNo.Contains(a.InvoiceId)
                group a by a.TaskId into b
                select new InvoiceClientItemOther()
                {
                    TaskId = b.Key,
                    AmountBilled = b.Sum(d => d.AmountBilled)
                }).ToList();

            var gg = await (
                from a in context.PceTasks
                join e in totalAmtt on a.Id equals e.TaskId //into f
                //from g in f.DefaultIfEmpty(new InvoiceClientItemOther())
                where a.PceId == pceid
                select new InvoiceClientItemOther()
                {
                    Id = Guid.NewGuid().ToString(),
                    TaskId = a.Id,
                    TaskName = a.TaskName,
                    Quantity = a.Quantity,
                    UnitPrice = a.UnitPrice,
                    Total = a.Total,
                    TempBalance = a.Total - e.AmountBilled,
                    Balance = a.Total - e.AmountBilled,
                    PceID = a.PceId
                }).AsNoTracking().ToListAsync();

            return gg;
        }

        public async Task<List<InvoiceClientItemOther>> GetInvoiceProductionDetail(string pceid, string invNo)
        {
            List<InvoiceClientItemOther> ListData = new List<InvoiceClientItemOther>();

            if (pceid != "0" && invNo != "0")
            {
                var listinvNo = await (from a in context.InvoiceClients where a.PceId == pceid && a.MultiPceId == null select a.Id).ToListAsync();
                var totalAmtt = (
                        from a in context.InvoiceClientItemOthers
                        where listinvNo.Contains(a.InvoiceId)
                        group a by a.TaskId into b
                        select new InvoiceClientItemOther()
                        {
                            TaskId = b.Key,
                            AmountBilled = b.Sum(d => d.AmountBilled)
                        }).ToList();
                if (listinvNo.Count() > 1)
                {
                    var retData = await (
                         from z in context.InvoiceClientItemOthers
                         join a in context.PceTasks on z.TaskId equals a.Id
                         join e in totalAmtt on z.TaskId equals e.TaskId
                         where a.PceId == pceid && z.InvoiceId == invNo
                         select new InvoiceClientItemOther()
                         {
                             Id = z.Id,
                             TaskId = a.Id,
                             TaskName = a.TaskName,
                             Quantity = a.Quantity,
                             UnitPrice = a.UnitPrice,
                             Total = a.Total,
                             AmountPercentage = z.AmountPercentage,
                             Amount = z.Amount,
                             AmountBilled = z.AmountBilled,
                             Balance = (z.Quantity * z.UnitPrice) - e.AmountBilled,
                             TempBalance = (z.Quantity * z.UnitPrice) - e.AmountBilled,
                             PceID = a.PceId,
                             TotalBillPercent = 0
                         }).AsNoTracking().ToListAsync();
                    ListData = retData;
                }
                else
                {
                    var retData = await (
                         from z in context.InvoiceClientItemOthers
                         join a in context.PceTasks on z.TaskId equals a.Id
                         join e in totalAmtt on z.TaskId equals e.TaskId
                         where a.PceId == pceid && z.InvoiceId == invNo
                         select new InvoiceClientItemOther()
                         {
                             Id = z.Id,
                             TaskId = a.Id,
                             TaskName = a.TaskName,
                             Quantity = a.Quantity,
                             UnitPrice = a.UnitPrice,
                             Total = a.Total,
                             AmountPercentage = z.AmountPercentage,
                             Amount = z.Amount,
                             AmountBilled = z.AmountBilled,
                             Balance = (z.Quantity * z.UnitPrice) - e.AmountBilled,
                             TempBalance = 0,
                             PceID = a.PceId,
                             TotalBillPercent = 0
                         }).AsNoTracking().ToListAsync();
                    ListData = retData;
                }

            }
            else
            {
                if (invNo != "0")
                {
                    var retData = await (
                          from a in context.InvoiceClientItemOthers
                          where a.InvoiceId == invNo
                          select new InvoiceClientItemOther()
                          {
                              Id = a.Id,
                              TaskId = a.Id,
                              TaskName = a.TaskName,
                              Quantity = a.Quantity,
                              UnitPrice = a.UnitPrice,
                              Total = a.Quantity * a.UnitPrice,
                              //AmountPercentage = a.AmountPercentage,
                              Amount = a.Quantity ?? 0 * a.UnitPrice ?? 0,
                              AmountBilled = a.AmountBilled,
                              Balance = (a.Quantity * a.UnitPrice) - a.AmountBilled,
                              PceID = a.PceID,
                          }).AsNoTracking().ToListAsync();
                    ListData = retData;
                }
                else if (pceid != "0")
                {
                    var retData = await (
                         from a in context.PceTasks
                         where a.PceId == pceid
                         select new InvoiceClientItemOther()
                         {
                             Id = a.Id,
                             TaskId = a.Id,
                             TaskName = a.TaskName,
                             Quantity = a.Quantity,
                             UnitPrice = a.UnitPrice,
                             Total = a.Quantity * a.UnitPrice,
                             Amount = a.Quantity ?? 0 * a.UnitPrice ?? 0,
                             //AmountBilled = 0,
                             //Balance = 0,
                             PceID = a.PceId
                         }).AsNoTracking().ToListAsync();
                    ListData = retData;
                }
            }

            return ListData;
        }

        public async Task<List<InvoiceClientItemOther>> ReadInvDtlHistory(string invoiceId, string pceid)
        {
            List<InvoiceClientItemOther> list = new List<InvoiceClientItemOther>();
            var invNo = await (from a in context.InvoiceClients where a.PceId == pceid && a.MultiPceId == null orderby a.Id select a.Id).ToListAsync();
            if (pceid != "0")
            {
                list = await (
                    from a in context.InvoiceClientItemOthers
                    join b in context.PceTasks on a.TaskId equals b.Id
                    join d in context.Pces on a.PceID equals d.Id
                    join c in context.InvoiceClients on a.InvoiceId equals c.Id
                    where invNo.Contains(a.InvoiceId)
                    select new InvoiceClientItemOther()
                    {
                        Id = a.Id,
                        TaskId = b.Id,
                        TaskName = b.TaskName,
                        Quantity = b.Quantity,
                        UnitPrice = b.UnitPrice,
                        Total = b.Total,
                        AmountPercentage = a.AmountPercentage,
                        Amount = a.Amount,
                        AmountBilled = a.AmountBilled,
                        PceID = a.PceID,
                        InvoiceId = a.InvoiceId,
                        Asf = c.AsfAmount,
                        AsfPercent = c.Asf,
                        Vat = (d.OtherFeePercentage / 100) * (b.Total + c.AsfAmount),
                        VatPercent = d.OtherFeePercentage,
                        GrandTotal = ((d.OtherFeePercentage / 100) * (b.Total + c.AsfAmount)) + b.Total,
                        InvoiceDate = c.InvoiceDate
                    }).AsNoTracking().ToListAsync();

            }
            else
            {
                list = await (
                    from a in context.InvoiceClientItemOthers
                    join c in context.InvoiceClients on a.InvoiceId equals c.Id
                    where a.InvoiceId == invoiceId
                    select new InvoiceClientItemOther()
                    {
                        Id = a.Id,
                        TaskId = a.Id,
                        TaskName = a.TaskName,
                        Quantity = a.Quantity,
                        UnitPrice = a.UnitPrice,
                        Total = a.Quantity * a.UnitPrice,
                        //AmountPercentage = a.AmountPercentage,
                        Amount = a.Amount,
                        AmountBilled = a.AmountBilled,
                        PceID = a.PceID,
                        InvoiceId = a.InvoiceId,
                        GrandTotal = a.Total - a.Amount,
                        InvoiceDate = c.InvoiceDate
                    }).AsNoTracking().ToListAsync();
            }
            return list;//.OrderBy(x => decimal.Parse((x.InvoiceId ?? "0").Substring(4)));
        }

        public Task<List<InvoiceClient>> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public async Task<int> InsertAndSubmitInvoiceClient(InvoiceClient model)
        {
            int savedCount = 0;
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    model.Id = await TransID.GetTransId(context, Code.InvoiceClientCode, DateTime.Today);
                    model.Status = SingleInvoiceClientStatus.WaitingApproval.ToString();
                    model.InvoiceNumber = model.Id;
                    //if (model.InvoiceBilled == null || model.InvoiceBilled == 0)
                    model.InvoiceBilled = model.TotalAmount;

                    var _model = await ProcessUpload(model);
                    await globalFcApproval.UnsafeSubmitApproval<InvoiceClient>(false, model.Id, Convert.ToInt32(model.Status), $"{ApprovalLink.InvoiceClient}?Id={model.Id}&isApprove=true", "", MenuUnique.InvoiceClient, _model);

                    savedCount = await context.SaveChangesAsync();


                    if (model.items_other != null)
                    {
                        Parallel.ForEach(model.items_other.Where(x => string.IsNullOrEmpty(x.ParentId)), (item) =>
                        {
                            item.InvoiceId = model.Id;
                            item.PceID = model.PceId;
                            item.JobID = model.JobId;
                            context.AddAsync(item);
                        });

                        savedCount += await context.SaveChangesAsync();

                        Parallel.ForEach(model.items_other.Where(x => !string.IsNullOrEmpty(x.ParentId)), (item) =>
                        {
                            item.InvoiceId = model.Id;
                            item.PceID = model.PceId;
                            item.JobID = model.JobId;
                            context.AddAsync(item);
                        });
                        savedCount += await context.SaveChangesAsync();
                    }

                    await log.AddAsync(new StatusLog() { TransactionId = model.Id, Status = SingleInvoiceClientStatus.StatusName(SingleInvoiceClientStatus.WaitingApproval), Description = model.PayDescription });
                    transaction.Commit();
                    return savedCount;
                }
                catch (Exception ex)
                {
                    savedCount = 0;
                    throw new Exception(ex.Message.ToString());
                }
            }
        }

        private async Task<InvoiceClient> ProcessUpload(InvoiceClient entity)
        {
            var file1 = await uploadFile.Upload(entity.VatDocFile, null);
            if (!string.IsNullOrEmpty(file1))
            {
                entity.VatDocFileId = file1;
            }

            var file2 = await uploadFile.Upload(entity.PoDocFile, null);
            if (!string.IsNullOrEmpty(file2))
            {
                entity.PoDocFileId = file2;
            }

            return entity;
        }
        public async Task<InvoiceClient> InsertInvoiceClient(InvoiceClient model)
        {
            int savedCount = 0;
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    model.Id = await TransID.GetTransId(context, Code.InvoiceClientCode, DateTime.Today);
                    model.Status = SingleInvoiceClientStatus.Open.ToString();
                    model.InvoiceNumber = model.Id;
                    //if (model.InvoiceBilled == null || model.InvoiceBilled == 0)
                    model.InvoiceBilled = model.TotalAmount;

                    var _model = await ProcessUpload(model);
                    await context.PhoenixAddAsync(_model);
                    savedCount = await context.SaveChangesAsync();

                    if (model.items_other != null)
                    {
                        Parallel.ForEach(model.items_other.Where(x => string.IsNullOrEmpty(x.ParentId)), (item) =>
                        {
                            item.InvoiceId = model.Id;
                            item.PceID = model.PceId;
                            item.JobID = model.JobId;
                            context.AddAsync(item);
                        });

                        savedCount += await context.SaveChangesAsync();

                        Parallel.ForEach(model.items_other.Where(x => !string.IsNullOrEmpty(x.ParentId)), (item) =>
                        {
                            item.InvoiceId = model.Id;
                            item.PceID = model.PceId;
                            item.JobID = model.JobId;
                            context.AddAsync(item);
                        });
                        savedCount += await context.SaveChangesAsync();
                    }
                    //AccountReceivable accountReceivable = new AccountReceivable()
                    //{
                    //    AccountReceivableNumber = model.Id,
                    //    Id = model.Id,
                    //    Amount = model.GrandTotal,
                    //    BrandId = model.BrandId,
                    //    JobId = model.JobId,
                    //    LegalEntityId = model.LegalId,
                    //    AffilitionId = model.AffilitionId,
                    //    Status = ARStatus.Open,
                    //    BankAccountDestination = model.AccountId,
                    //    ExchangeRate = model.ExchangeRate == null ? 0 : model.ExchangeRate,
                    //    CurrencyId = model.CurrencyId,
                    //    TransactionDate = model.InvoiceDate,
                    //    DueDate = model.DueDate,
                    //    BusinessUnitId = model.DivisionId

                    //};

                    //await context.PhoenixAddAsync(accountReceivable);
                    //savedCount += await context.SaveChangesAsync();
                    await log.AddAsync(new StatusLog() { TransactionId = model.Id, Status = SingleInvoiceClientStatus.StatusName(SingleInvoiceClientStatus.Open), Description = model.PayDescription });
                    transaction.Commit();
                    return model;
                }
                catch (Exception ex)
                {
                    savedCount = 0;
                    throw new Exception(ex.Message.ToString());
                }
            }
        }

        public async Task<InvoiceClient> Get(string id) => await context.InvoiceClients.AsNoTracking().SingleOrDefaultAsync(x => !(x.IsDeleted ?? false) && x.Id == id);

        //CUD
        public async Task<int> AddAsync(InvoiceClient entity)
        {
            entity.Id = Guid.NewGuid().ToString();
            await context.PhoenixAddAsync(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteAsync(InvoiceClient entity)
        {
            context.PhoenixApprove(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteSoftAsync(InvoiceClient entity)
        {
            context.PhoenixDelete(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<InvoiceClient> EditAsyncData(InvoiceClient entity)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    int savedCountOther = 0;
                    var header = await context.InvoiceClients.AsNoTracking().Where(x => x.Id == entity.Id).FirstOrDefaultAsync();
                    var _model = await ProcessUpload(entity);
                    _model.Status = entity.Status;
                    _model.InvoiceNumber = _model.Id;
                    //if (_model.InvoiceBilled == null || _model.InvoiceBilled == 0)
                    _model.InvoiceBilled = _model.TotalAmount;

                    context.PhoenixEdit(_model);
                    if (entity.items_other != null)
                    {
                        var additional = await context.InvoiceClientItemOthers.AsNoTracking().Where(x => x.InvoiceId == entity.InvoiceNumber).ToListAsync();
                        if (additional.Count > 0)
                        {
                            context.InvoiceClientItemOthers.RemoveRange(additional);
                        }

                        Parallel.ForEach(entity.items_other.Where(x => string.IsNullOrEmpty(x.ParentId)), (item) =>
                        {
                            item.InvoiceId = entity.Id;
                            item.PceID = entity.PceId;
                            item.JobID = entity.JobId;
                            context.PhoenixAddRangeAsync(item);
                        });

                        savedCountOther += await context.SaveChangesAsync();

                        Parallel.ForEach(entity.items_other.Where(x => !string.IsNullOrEmpty(x.ParentId)), (item) =>
                        {
                            item.InvoiceId = entity.Id;
                            item.PceID = entity.PceId;
                            item.JobID = entity.JobId;
                            context.PhoenixAddRangeAsync(item);
                        });
                        savedCountOther += await context.SaveChangesAsync();
                    }
                    await log.AddAsync(new StatusLog() { TransactionId = entity.Id, Status = SingleInvoiceClientStatus.StatusName(SingleInvoiceClientStatus.Open), Description = entity.PayDescription });
                    var save = await context.SaveChangesAsync();
                    transaction.Commit();
                    return entity;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message.ToString());
                }
            }

        }

        public async Task<int> EditAndSubmitAsync(InvoiceClient entity)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    int savedCountOther = 0;
                    entity.Status = SingleInvoiceClientStatus.WaitingApproval.ToString();
                    var _model = await ProcessUpload(entity);
                    _model.Status = SingleInvoiceClientStatus.WaitingApproval.ToString();
                    _model.InvoiceNumber = _model.Id;
                    //if (_model.InvoiceBilled == null || _model.InvoiceBilled == 0)
                    _model.InvoiceBilled = _model.TotalAmount;

                    entity.Status = SingleInvoiceClientStatus.WaitingApproval.ToString();
                    await globalFcApproval.UnsafeSubmitApproval<InvoiceClient>(true, entity.Id, Convert.ToInt32(entity.Status), $"{ApprovalLink.InvoiceClient}?Id={entity.Id}&isApprove=true", "", MenuUnique.InvoiceClient, _model);

                    //context.PhoenixEdit(entity);

                    if (entity.items_other != null)
                    {
                        var additional = await context.InvoiceClientItemOthers.AsNoTracking().Where(x => x.InvoiceId == entity.InvoiceNumber).ToListAsync();
                        if (additional.Count > 0)
                        {
                            context.InvoiceClientItemOthers.RemoveRange(additional);
                        }

                        Parallel.ForEach(entity.items_other.Where(x => string.IsNullOrEmpty(x.ParentId)), (item) =>
                        {
                            item.InvoiceId = entity.Id;
                            item.PceID = entity.PceId;
                            item.JobID = entity.JobId;
                            context.PhoenixAddRangeAsync(item);
                        });

                        savedCountOther += await context.SaveChangesAsync();

                        Parallel.ForEach(entity.items_other.Where(x => !string.IsNullOrEmpty(x.ParentId)), (item) =>
                        {
                            item.InvoiceId = entity.Id;
                            item.PceID = entity.PceId;
                            item.JobID = entity.JobId;
                            context.PhoenixAddRangeAsync(item);
                        });
                        savedCountOther += await context.SaveChangesAsync();
                    }
                    await log.AddAsync(new StatusLog() { TransactionId = entity.Id, Status = SingleInvoiceClientStatus.StatusName(SingleInvoiceClientStatus.WaitingApproval), Description = entity.PayDescription });
                    var save = await context.SaveChangesAsync();
                    transaction.Commit();
                    return save;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message.ToString());
                }
            }

        }

        public Task<List<InvoiceClient>> Get()
        {
            throw new NotImplementedException();
        }

        public Task<int> AddRangeAsync(params InvoiceClient[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> EditRangeAsync(InvoiceClient entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteRageAsync(params InvoiceClient[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteSoftRangeAsync(params InvoiceClient[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<InvoiceClient> Approve(InvoiceClient entity)
        {
            try
            {
                entity.Status = StatusTransaction.Approved.ToString();
                await globalFcApproval.ProcessApproval<InvoiceClient>(entity.Id, Convert.ToInt32(entity.Status), entity.RemarkRejected, entity);

                var modeltrapp = await context.TrTemplateApprovals.Where(x => x.RefId == entity.Id).FirstOrDefaultAsync();
                if (modeltrapp != null)
                {
                    if (modeltrapp.StatusApprovedDescription == StatusTransaction.StatusName(StatusTransaction.Approved))
                    {
                        // todo final approval
                        var invoices = await (from a in context.InvoiceClients where a.MultiPceId == null && a.PceId == entity.PceId select a.GrandTotal).ToListAsync();
                        var pceTotal = await (from a in context.Pces where a.Id == entity.PceId select a.Total).ToListAsync();
                        var totalPce = pceTotal.Sum();
                        var tagihan = invoices.Sum();
                        if (!string.IsNullOrEmpty(entity.PceId))
                        {
                            if (tagihan >= totalPce)
                            { entity.Status = SingleInvoiceClientStatus.Closed.ToString(); }
                            else
                            { entity.Status = SingleInvoiceClientStatus.Balance.ToString(); }
                        }
                        else
                        {
                            entity.Status = SingleInvoiceClientStatus.Closed.ToString();
                        }
                    }
                    else
                    {
                        entity.Status = SingleInvoiceClientStatus.WaitingApproval.ToString();
                    }
                }
                else
                {
                    entity.Status = SingleInvoiceClientStatus.WaitingApproval.ToString();
                }

                if (entity.Status == SingleInvoiceClientStatus.Closed.ToString() || entity.Status == SingleInvoiceClientStatus.Balance.ToString())
                {
                    AccountReceivable accountReceivable = new AccountReceivable()
                    {
                        AccountReceivableNumber = entity.Id,
                        Id = entity.Id,
                        Amount = entity.GrandTotal,
                        BrandId = entity.BrandId,
                        JobId = entity.JobId,
                        LegalEntityId = entity.LegalId,
                        AffilitionId = entity.AffiliationId,
                        Status = ARStatus.Open,
                        BankAccountDestination = entity.AccountId,
                        ExchangeRate = entity.ExchangeRate == null ? 0 : entity.ExchangeRate,
                        CurrencyId = entity.CurrencyId,
                        TransactionDate = entity.InvoiceDate,
                        BusinessUnitId = entity.BusinessUnitId,
                        VatNo = entity.VatNo,
                        ClientId = entity.ClientId
                    };

                    await context.PhoenixAddAsync(accountReceivable);
                }
                await log.AddAsync(new StatusLog() { TransactionId = entity.Id, Status = SingleInvoiceClientStatus.StatusName(Convert.ToInt32(entity.Status)), Description = entity.PayDescription });
                context.PhoenixEdit(entity);
                var save = await context.SaveChangesAsync();
                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public async Task<InvoiceClient> ForceClose(InvoiceClient entity)
        {
            try
            {
                entity.Status = SingleInvoiceClientStatus.Closed.ToString();

                await log.AddAsync(new StatusLog() { TransactionId = entity.Id, Status = SingleInvoiceClientStatus.StatusName(Convert.ToInt32(entity.Status)), Description = entity.PayDescription });
                context.PhoenixEdit(entity);
                var save = await context.SaveChangesAsync();
                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public async Task<InvoiceClient> Reject(InvoiceClient entity)
        {
            try
            {
                entity.Status = SingleInvoiceClientStatus.Rejected.ToString();
                await globalFcApproval.ProcessApproval<InvoiceClient>(entity.Id, Convert.ToInt32(entity.Status), entity.RemarkRejected, entity);

                var modeltrapp = await context.TrTemplateApprovals.AsNoTracking().Where(x => x.RefId == entity.Id).FirstOrDefaultAsync();
                if (modeltrapp != null)
                {
                    if (modeltrapp.StatusApprovedDescription == SingleInvoiceClientStatus.StatusName(SingleInvoiceClientStatus.Rejected))
                    {
                        entity.Status = SingleInvoiceClientStatus.Rejected.ToString();
                        entity.Remarks = entity.RemarkRejected;
                    }
                }
                else
                {
                    entity.Status = SingleInvoiceClientStatus.Rejected.ToString();
                }
                await log.AddAsync(new StatusLog() { TransactionId = entity.Id, Status = SingleInvoiceClientStatus.StatusName(Convert.ToInt32(entity.Status)), Description = entity.RemarkRejected });
                context.PhoenixEdit(entity);
                var save = await context.SaveChangesAsync();
                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public async Task<List<Journal>> GetDataJournal(string Id)
        {
            try
            {
                var bp = await context.InvoiceClients.Where(x => x.IsDeleted.Equals(false) && x.Id == Id).FirstOrDefaultAsync();
                var tapname = "";
                if (string.IsNullOrEmpty(bp.JobId))
                {
                    tapname = "";
                }
                else
                {
                    tapname = "PROJECT - " + bp.JobId;
                }

                var data = await (from tb in context.JournalTemps
                                  join coa in context.ChartOfAccounts on tb.AccountId equals coa.Id
                                  join bu in context.BusinessUnits on tb.DivisiId equals bu.Id into arc
                                  from buu in arc.DefaultIfEmpty()
                                  where tb.ReferenceId == Id
                                  orderby tb.LineCode ascending
                                  select new Journal
                                  {
                                      AccountId = coa.Id,
                                      AccountCode = coa.CodeRec,
                                      AccountName = coa.Name5,
                                      JobId = tb.JobId,
                                      Description = tb.Description == null ? coa.Name5 : tb.Description,
                                      JobIdName = tapname,
                                      DivisiId = tb.DivisiId,
                                      DivisiName = buu.UnitName,
                                      Credit = tb.Credit,
                                      Debit = tb.Debit,
                                      LineCode = tb.LineCode,
                                  }).ToListAsync();


                return data.OrderByDescending(x => x.CreatedOn).ToList();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public async Task<int> CreateJournalWithSP(string Id)
        {
            try
            {
                var modeldel = await context.JournalTemps.Where(x => x.IsDeleted.Equals(false) && x.ReferenceId == Id).ToListAsync();
                if (modeldel != null)
                {
                    List<JournalTemp> jnd = new List<JournalTemp>();
                    foreach (JournalTemp data in modeldel)
                    {
                        jnd.Add(data);
                    }
                    JournalTemp[] jndt = jnd.ToArray();
                    context.PhoenixDeleteRange(jndt);
                }
                var result = await context.Database.ExecuteSqlCommandAsync("EXECUTE fn.create_jurnal_ar2_temp @p_account_receivable_id", parameters: new[] { new SqlParameter("@p_account_receivable_id", Id) });
                var save = await context.SaveChangesAsync();
                return save;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public Task<int> EditAsync(InvoiceClient entity)
        {
            throw new NotImplementedException();
        }
    }
}
