﻿using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Models;
using Phoenix.Data.Models.Finance.Master;
using Phoenix.Data.Models.Finance.Transaction;
using Phoenix.Data.Models.Finance.Transaction.InvoiceReceivable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Data.Services.Finance.Transaction
{
    public interface IInvoiceReceivedService : IDataService<InvoiceReceived>
    {
        Task<ClientData> GetClient(string clientId);
        Task<ClientData> GetVendor(string vendorId);
        Task<PoData> GetPo(string poId);
        Task<List<InvoiceReceived>> GetHistory(string poId);
        Task<List<string>> GetInvoiceVendorList(string poId);
        Task<List<InvoiceReceived>> GetByStatus(string status);
        Task<List<InvoiceReceived>> GetByFilterCustom(string status, string businessUnit, string legalEntity, string invoiceNumber);
        Task<List<InvoiceReceivedDetail>> GetDetailById(string Id);
        Task<List<InvoiceReceived>> GetVendorByStatus(string status, string vendorId);
        Task<InvoiceReceived> GetWithPoDetail(string Id);
        Task<int> AddWithDetail(InvoiceReceived header, List<InvoiceReceivedDetail> details);
        Task<int> EditWithDetail(InvoiceReceived header, List<InvoiceReceivedDetail> details);
    }

    public class InvoiceReceivedService : IInvoiceReceivedService
    {
        readonly DataContext context;

        /// <summary>
        /// And endpoint to manage Termination
        /// </summary>
        /// <param name="context">Database context</param>
        public InvoiceReceivedService(DataContext context)
        {
            this.context = context;
        }

        public async Task<int> AddAsync(InvoiceReceived entity)
        {
            try
            {
                entity.Id = Guid.NewGuid().ToString();
                entity.InvoiceNumber = await TransID.GetTransId(context, Code.InvoiceReceived, DateTime.Today);
                await context.PhoenixAddAsync(entity);
                return await context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }

        }

        public async Task<int> AddWithDetail(InvoiceReceived header, List<InvoiceReceivedDetail> details)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    List<InvoiceReceivableDetail> invdetails = new List<InvoiceReceivableDetail>();

                    header.Id = Guid.NewGuid().ToString();
                    header.InvoiceNumber = await TransID.GetTransId(context, Code.InvoiceReceived, DateTime.Today);
                    header.organization_id = "9EE4834225E334380667DDCF2F60F6DA";
                    if (header.PoCheckListData.Equals(true))
                    {
                        header.PoCheckList = true;
                    }
                    else
                    {
                        header.PoCheckList = false;
                    }
                    if (header.ContractCheckListData.Equals(true))
                    {
                        header.ContractCheckList = true;
                    }
                    else
                    {
                        header.ContractCheckList = false;
                    }
                    if (header.TaxCheckListData.Equals(true))
                    {
                        header.TaxCheckList = true;
                    }
                    else
                    {
                        header.TaxCheckList = false;
                    }
                    if (header.GrCheckListData.Equals(true))
                    {
                        header.GrCheckList = true;
                    }
                    else
                    {
                        header.GrCheckList = false;
                    }
                    if (header.DoCheckListData.Equals(true))
                    {
                        header.DoCheckList = true;
                    }
                    else
                    {
                        header.DoCheckList = false;
                    }

                    await context.PhoenixAddAsync(header);
                    int i = 1;
                    foreach (InvoiceReceivedDetail model in details)
                    {
                        var data = new InvoiceReceivableDetail();
                        data.Id = Guid.NewGuid().ToString();
                        data.InvoiceReceivableId = header.Id;
                        data.Qty = model.qty == null ? 1 : (model.qty ?? 0);
                        data.Task = model.ItemName;
                        data.ItemDescription = model.ItemName;
                        data.Discount = 0;
                        data.AccountId = model.AccountId;
                        data.Tax = 0;
                        data.RowIndex = i;
                        i++;
                        invdetails.Add(data);
                    }

                    InvoiceReceivableDetail[] invdetailsnew = invdetails.ToArray();

                    await context.PhoenixAddRangeAsync(invdetailsnew);
                    var save = await context.SaveChangesAsync();

                    transaction.Commit();
                    return save;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message.ToString());
                }
            }
        }

        public async Task<int> EditAsync(InvoiceReceived entity)
        {
            if (entity.InvoiceStatusId == InvoiceReceivedStatus.TAP)
            {
                //TemporaryAccountPayable tap = new TemporaryAccountPayable();
                //tap.Id = Guid.NewGuid().ToString();
                //tap.TAPNumber = await TransID.GetTransId(context, Code.TemporaryAccountPayable, DateTime.Today);
                //tap.InvoiceReceivedId = entity.Id;
                //tap.VendorId = entity.ClientId;
                //tap.TransactionDate = entity.InvoiceDate;
                //tap.CurrencyId = entity.CurrencyId;
                //tap.Amount = entity.Value;
                //tap.ExchangeRate = entity.ExchangeRate;

                //await context.PhoenixAddAsync(tap);
                entity.InvoiceStatusId = InvoiceReceivedStatus.TAP;
            }
            if (entity.InvoiceStatusId == InvoiceReceivedStatus.Rejected)
            {
                entity.InvoiceStatusId = InvoiceReceivedStatus.Rejected;
            }
            context.PhoenixEdit(entity);

            return await context.SaveChangesAsync();
        }

        public async Task<int> EditWithDetail(InvoiceReceived header, List<InvoiceReceivedDetail> details)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    List<InvoiceReceivableDetail> invdetails = new List<InvoiceReceivableDetail>();
                    List<InvoiceReceivableDetail> invdetailsdel = new List<InvoiceReceivableDetail>();

                    header.organization_id = "9EE4834225E334380667DDCF2F60F6DA";
                    if (header.PoCheckListData.Equals(true))
                    {
                        header.PoCheckList = true;
                    }
                    else
                    {
                        header.PoCheckList = false;
                    }
                    if (header.ContractCheckListData.Equals(true))
                    {
                        header.ContractCheckList = true;
                    }
                    else
                    {
                        header.ContractCheckList = false;
                    }
                    if (header.TaxCheckListData.Equals(true))
                    {
                        header.TaxCheckList = true;
                    }
                    else
                    {
                        header.TaxCheckList = false;
                    }
                    if (header.GrCheckListData.Equals(true))
                    {
                        header.GrCheckList = true;
                    }
                    else
                    {
                        header.GrCheckList = false;
                    }
                    if (header.DoCheckListData.Equals(true))
                    {
                        header.DoCheckList = true;
                    }
                    else
                    {
                        header.DoCheckList = false;
                    }
                    context.PhoenixEdit(header);

                    if (details != null)
                    {
                        var modelget = await context.InvoiceReceivableDetails.Where(x => x.IsDeleted.Equals(false) && x.InvoiceReceivableId.Equals(header.Id)).ToListAsync();

                        foreach (InvoiceReceivableDetail data in modelget)
                        {
                            invdetailsdel.Add(data);
                        }
                        InvoiceReceivableDetail[] invdetailsdelete = invdetailsdel.ToArray();
                        context.PhoenixDeleteRange(invdetailsdelete);
                        int i = 1;

                        foreach (InvoiceReceivedDetail model in details)
                        {
                            var data = new InvoiceReceivableDetail();
                            data.Id = Guid.NewGuid().ToString();
                            data.InvoiceReceivableId = header.Id;
                            data.Qty = model.qty == null ? 1 : (model.qty ?? 0);
                            data.Task = model.ItemName;
                            data.ItemDescription = model.ItemName;
                            data.Discount = 0;
                            data.AccountId = model.AccountId;
                            data.Tax = 0;
                            data.RowIndex = i;
                            i++;
                            invdetails.Add(data);
                        }

                        InvoiceReceivableDetail[] invdetailsnew = invdetails.ToArray();

                        await context.PhoenixAddRangeAsync(invdetailsnew);
                    }

                    var save = await context.SaveChangesAsync();

                    transaction.Commit();
                    return save;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message.ToString());
                }
            }
        }


        public async Task<int> ApproveAsync(InvoiceReceived entity)
        {
            context.PhoenixApprove(entity);
            return await context.SaveChangesAsync();
        }

        //public async Task<List<InvoiceReceived>> Get() => await context.InvoiceReceivables.AsNoTracking().Where(x => !(x.IsDeleted ?? false)).ToListAsync();
        public async Task<List<InvoiceReceived>> Get()
        {
            var data = await (from inv in context.InvoiceReceivables
                              join po in context.PurchaseOrders on inv.PoId equals po.Id into p
                              from po in p.DefaultIfEmpty()
                              join curr in context.Currencies on inv.CurrencyId equals curr.Id into c
                              from curr in c.DefaultIfEmpty()
                              join client in context.Companys on inv.ClientId equals client.Id into cl
                              from client in cl.DefaultIfEmpty()
                              where !inv.IsDeleted.GetValueOrDefault()
                              select new InvoiceReceived
                              {
                                  Id = inv.Id,
                                  InvoiceNumber = inv.InvoiceNumber,
                                  InvoiceDate = inv.InvoiceDate,
                                  PoNumber = po.PurchaseOrderNumber,
                                  purchase_request_number = po.PurchaseRequestNumber,
                                  JobId = inv.JobId,
                                  ClientId = inv.ClientId,
                                  JobName = inv.JobId == null ? "INTERNAL" + po.PurchaseRequestNumber : inv.JobId,
                                  CurrencyName = curr.CurrencyName,
                                  InvoiceVendor = inv.InvoiceVendor,
                                  InvoiceVendorDate = inv.InvoiceVendorDate,
                                  InvoiceStatusId = inv.InvoiceStatusId == null ? "1" : inv.InvoiceStatusId,
                                  PoCheckList = inv.PoCheckList,
                                  MainServiceCategoryId = inv.MainServiceCategoryId,
                                  DueDays = inv.DueDays,
                                  Periode = inv.Periode,
                                  GoodReceivedNumber = inv.GoodReceivedNumber,
                                  TaxCheckList = inv.TaxCheckList,
                                  DoCheckList = inv.DoCheckList,
                                  GrCheckList = inv.GrCheckList,
                                  ContractCheckList = inv.ContractCheckList,
                                  DueDate = inv.DueDate,
                                  Value = (inv.Value ?? 0),
                                  Amount = (inv.Amount ?? 0),
                                  ClientName = client.CompanyName,
                                  StatusName = inv.InvoiceStatusId == "1" ? "Draft" : inv.InvoiceStatusId == "2" ? "ApproveFrontDesk" :
                                               inv.InvoiceStatusId == "3" ? "ApproveAdmin" : inv.InvoiceStatusId == "4" ? "Rejected" : inv.InvoiceStatusId == "5" ? "TAP" : "Completed"
                              }).ToListAsync();
            return data;
        }

        public async Task<List<InvoiceReceived>> GetByStatus(string status)
        {
            var data = await (from inv in context.InvoiceReceivables
                              join po in context.PurchaseOrders on inv.PoId equals po.Id into p
                              from po in p.DefaultIfEmpty()
                              join curr in context.Currencies on inv.CurrencyId equals curr.Id into c
                              from curr in c.DefaultIfEmpty()
                              join client in context.Companys on inv.ClientId equals client.Id into cl
                              from client in cl.DefaultIfEmpty()
                              where !inv.IsDeleted.GetValueOrDefault() && inv.InvoiceStatusId == status
                              select new InvoiceReceived
                              {
                                  Id = inv.Id,
                                  InvoiceNumber = inv.InvoiceNumber,
                                  InvoiceDate = inv.InvoiceDate,
                                  PoNumber = po.PurchaseOrderNumber,
                                  JobId = inv.JobId,
                                  ClientId = inv.ClientId,
                                  JobName = inv.JobId == null ? "INTERNAL" + po.PurchaseRequestNumber : inv.JobId,
                                  purchase_request_number = po.PurchaseRequestNumber,
                                  CurrencyName = curr.CurrencyName,
                                  InvoiceVendor = inv.InvoiceVendor,
                                  InvoiceVendorDate = inv.InvoiceVendorDate,
                                  MainServiceCategoryId = inv.MainServiceCategoryId,
                                  InvoiceStatusId = inv.InvoiceStatusId == null ? "1" : inv.InvoiceStatusId,
                                  DueDays = inv.DueDays,
                                  Periode = inv.Periode,
                                  GoodReceivedNumber = inv.GoodReceivedNumber,
                                  PoCheckList = inv.PoCheckList,
                                  TaxCheckList = inv.TaxCheckList,
                                  DueDate = inv.DueDate,
                                  DoCheckList = inv.DoCheckList,
                                  GrCheckList = inv.GrCheckList,
                                  ContractCheckList = inv.ContractCheckList,
                                  Value = (inv.Value ?? 0),
                                  Amount = (inv.Amount ?? 0),
                                  ClientName = client.CompanyName,
                                  StatusName = inv.InvoiceStatusId == "1" ? "Draft" : inv.InvoiceStatusId == "2" ? "ApproveFrontDesk" :
                                               inv.InvoiceStatusId == "3" ? "ApproveAdmin" : inv.InvoiceStatusId == "4" ? "Rejected" : inv.InvoiceStatusId == "5" ? "TAP" : "Completed"
                              }).ToListAsync();
            return data;
        }

        public async Task<List<InvoiceReceived>> GetByFilterCustom(string status, string businessUnit, string legalEntity, string invoiceNumber)
        {
            var data = new List<InvoiceReceived>();
            data = await (from inv in context.InvoiceReceivables
                          join po in context.PurchaseOrders on inv.PoId equals po.Id
                          join curr in context.Currencies on inv.CurrencyId equals curr.Id into currn
                          from currnw in currn.DefaultIfEmpty()
                          join client in context.Companys on inv.ClientId equals client.Id
                          join bu in context.BusinessUnits on inv.DivisionId equals bu.Id
                          join lgl in context.LegalEntity on po.LegalEntityId equals lgl.Id
                          where !inv.IsDeleted.GetValueOrDefault()
                          select new InvoiceReceived
                          {
                              Id = inv.Id,
                              InvoiceNumber = inv.InvoiceNumber,
                              InvoiceDate = inv.InvoiceDate,
                              PoNumber = po.PurchaseOrderNumber,
                              JobId = inv.JobId,
                              GoodReceivedNumber = inv.GoodReceivedNumber,
                              DueDate = inv.DueDate,
                              ClientId = inv.ClientId,
                              MainServiceCategoryId = inv.MainServiceCategoryId,
                              JobName = inv.JobId == null ? "INTERNAL" + po.PurchaseRequestNumber : inv.JobId,
                              purchase_request_number = po.PurchaseRequestNumber,
                              CurrencyName = currnw.CurrencyName,
                              InvoiceVendor = inv.InvoiceVendor,
                              InvoiceVendorDate = inv.InvoiceVendorDate,
                              InvoiceStatusId = inv.InvoiceStatusId == null ? "1" : inv.InvoiceStatusId,
                              DueDays = inv.DueDays,
                              Periode = inv.Periode,
                              PoCheckList = inv.PoCheckList,
                              TaxCheckList = inv.TaxCheckList,
                              DoCheckList = inv.DoCheckList,
                              GrCheckList = inv.GrCheckList,
                              ContractCheckList = inv.ContractCheckList,
                              Value = (inv.Value ?? 0),
                              Amount = (inv.Amount ?? 0),
                              ClientName = client.CompanyName,
                              TotalCostVendor = inv.TotalCostVendor,
                              DivisionId = inv.DivisionId,
                              DivisionName = bu.UnitName,
                              LegalEntityId = po.LegalEntityId,
                              LegalEntityName = lgl.LegalEntityName
                          }).ToListAsync();
            if (status != "0")
            {

                data = data.Where(x => x.InvoiceStatusId == status).ToList();
            }
            if (businessUnit != "0")
            {

                data = data.Where(x => x.DivisionId == businessUnit).ToList();
            }
            if (legalEntity != "0")
            {

                data = data.Where(x => x.LegalId == legalEntity).ToList();
            }
            if (invoiceNumber != "0")
            {

                data = data.Where(x => x.InvoiceNumber == invoiceNumber).ToList();
            }
            return data;
        }

        public async Task<List<InvoiceReceived>> GetVendorByStatus(string status, string vendorId)
        {
            var data = await (from inv in context.InvoiceReceivables
                              join po in context.PurchaseOrders on inv.PoId equals po.Id into p
                              from po in p.DefaultIfEmpty()
                              join curr in context.Currencies on inv.CurrencyId equals curr.Id into c
                              from curr in c.DefaultIfEmpty()
                              join client in context.Companys on inv.ClientId equals client.Id into cl
                              from client in cl.DefaultIfEmpty()
                              where !inv.IsDeleted.GetValueOrDefault() && inv.InvoiceStatusId == status && inv.ClientId == vendorId
                              select new InvoiceReceived
                              {
                                  Id = inv.Id,
                                  InvoiceNumber = inv.InvoiceNumber,
                                  InvoiceDate = inv.InvoiceDate,
                                  PoNumber = po.PurchaseOrderNumber,
                                  JobId = inv.JobId,
                                  DueDays = inv.DueDays,
                                  GoodReceivedNumber = inv.GoodReceivedNumber,
                                  ClientId = inv.ClientId,
                                  Periode = inv.Periode,
                                  MainServiceCategoryId = inv.MainServiceCategoryId,
                                  DueDate = inv.DueDate,
                                  InvoiceStatusId = inv.InvoiceStatusId == null ? "1" : inv.InvoiceStatusId,
                                  JobName = inv.JobId == null ? "INTERNAL" + po.PurchaseRequestNumber : inv.JobId,
                                  purchase_request_number = po.PurchaseRequestNumber,
                                  CurrencyName = curr.CurrencyName,
                                  InvoiceVendor = inv.InvoiceVendor,
                                  InvoiceVendorDate = inv.InvoiceVendorDate,
                                  Value = (inv.Value ?? 0),
                                  Amount = (inv.Amount ?? 0),
                                  ClientName = client.CompanyName,
                                  StatusName = inv.InvoiceStatusId == "1" ? "Draft" : inv.InvoiceStatusId == "2" ? "ApproveFrontDesk" :
                                               inv.InvoiceStatusId == "3" ? "ApproveAdmin" : inv.InvoiceStatusId == "4" ? "Rejected" : inv.InvoiceStatusId == "5" ? "TAP" : "Completed"
                              }).ToListAsync();
            return data;
        }

        public async Task<List<InvoiceReceived>> GetHistory(string poId)
        {
            var data = await (from inv in context.InvoiceReceivables
                              join po in context.PurchaseOrders on inv.PoId equals po.Id into p
                              from po in p.DefaultIfEmpty()
                              join curr in context.Currencies on inv.CurrencyId equals curr.Id into c
                              from curr in c.DefaultIfEmpty()
                              join client in context.Companys on inv.ClientId equals client.Id into cl
                              from client in cl.DefaultIfEmpty()
                              where !inv.IsDeleted.GetValueOrDefault() && inv.PoId == poId
                              select new InvoiceReceived
                              {
                                  Id = inv.Id,
                                  InvoiceNumber = inv.InvoiceNumber,
                                  InvoiceDate = inv.InvoiceDate,
                                  PoNumber = po.PurchaseOrderNumber,
                                  JobId = inv.JobId,
                                  DueDays = inv.DueDays,
                                  GoodReceivedNumber = inv.GoodReceivedNumber,
                                  ClientId = inv.ClientId,
                                  MainServiceCategoryId = inv.MainServiceCategoryId,
                                  DueDate = inv.DueDate,
                                  InvoiceStatusId = inv.InvoiceStatusId == null ? "1" : inv.InvoiceStatusId,
                                  Periode = inv.Periode,
                                  JobName = inv.JobId == null ? "INTERNAL" + po.PurchaseRequestNumber : inv.JobId,
                                  purchase_request_number = po.PurchaseRequestNumber,
                                  CurrencyName = curr.CurrencyName,
                                  InvoiceVendor = inv.InvoiceVendor,
                                  InvoiceVendorDate = inv.InvoiceVendorDate,
                                  Value = (inv.Value ?? 0),
                                  Amount = (inv.Amount ?? 0),
                                  ClientName = client.CompanyName,
                                  AdminNotes = inv.AdminNotes,
                                  FrontDeskNotes = inv.FrontDeskNotes,
                                  StatusName = inv.InvoiceStatusId == "1" ? "Draft" : inv.InvoiceStatusId == "2" ? "ApproveFrontDesk" :
                                               inv.InvoiceStatusId == "3" ? "ApproveAdmin" : inv.InvoiceStatusId == "4" ? "Rejected" : inv.InvoiceStatusId == "5" ? "TAP" : "Completed"
                              }).ToListAsync();
            return data;
        }

        public async Task<List<string>> GetInvoiceVendorList(string poId) => await context.InvoiceReceivables.AsNoTracking().Where(x => !(x.IsDeleted ?? false && x.PoId == poId)).Select(m => m.InvoiceVendor).ToListAsync();

        public async Task<int> DeleteSoftAsync(InvoiceReceived entity)
        {
            context.PhoenixDelete(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<InvoiceReceived> Get(string Id)
        {

            var data = await (from inv in context.InvoiceReceivables
                              join po in context.PurchaseOrders on inv.PoId equals po.Id into p
                              from po in p.DefaultIfEmpty()
                              join curr in context.Currencies on inv.CurrencyId equals curr.Id into c
                              from curr in c.DefaultIfEmpty()
                              join client in context.Companys on inv.ClientId equals client.Id into cl
                              from client in cl.DefaultIfEmpty()
                              where inv.IsDeleted.Equals(false) && inv.Id == Id
                              select new InvoiceReceived
                              {
                                  Id = inv.Id,
                                  InvoiceNumber = inv.InvoiceNumber,
                                  InvoiceDate = inv.InvoiceDate,
                                  PoNumber = po.PurchaseOrderNumber,
                                  AdminNotes = inv.AdminNotes,
                                  AffiliationId = inv.AffiliationId,
                                  ApprovalAdminDesk = inv.ApprovalAdminDesk,
                                  ApprovalFrontDesk = inv.ApprovalFrontDesk,
                                  ClientId = inv.ClientId,
                                  MainServiceCategoryId = inv.MainServiceCategoryId,
                                  ContractCheckList = inv.ContractCheckList,
                                  ContractObject = inv.ContractObject,
                                  CurrencyId = inv.CurrencyId,
                                  DueDate = inv.DueDate,
                                  DueDays = inv.DueDays,
                                  InvoiceStatusId = inv.InvoiceStatusId == null ? "1" : inv.InvoiceStatusId,
                                  Periode = inv.Periode,
                                  Description = inv.Description,
                                  DivisionId = inv.DivisionId,
                                  DoCheckList = inv.DoCheckList,
                                  DoObject = inv.DoObject,
                                  ExchangeRate = (inv.ExchangeRate ?? 0),
                                  FrontDeskNotes = inv.FrontDeskNotes,
                                  GrCheckList = inv.GrCheckList,
                                  GrObject = inv.GrObject,
                                  LegalId = inv.LegalEntityId, 
                                  PoCheckList = inv.PoCheckList,
                                  PoObject = inv.PoObject,
                                  ServiceCenterId = inv.ServiceCenterId,
                                  PoId = inv.PoId,
                                  TaxCheckList = inv.TaxCheckList,
                                  TaxNumber = inv.TaxNumber,
                                  TaxDate = inv.TaxDate,
                                  TaxObject = inv.TaxObject,
                                  GoodReceivedNumber = inv.GoodReceivedNumber,
                                  TotalAmount = (inv.TotalAmount ?? 0),
                                  Vat = (inv.ValueAddedTax ?? 0),
                                  Value = (inv.Value ?? 0),
                                  checklistString = "",
                                  purchase_request_number = po.PurchaseRequestNumber,
                                  JobId = inv.JobId,
                                  JobName = inv.JobId == null ? "INTERNAL" + po.PurchaseRequestNumber : inv.JobId,
                                  CurrencyName = curr.CurrencyName,
                                  InvoiceVendor = inv.InvoiceVendor,
                                  InvoiceVendorDate = inv.InvoiceVendorDate,
                                  Amount = (inv.Amount ?? 0),
                                  ClientName = client.CompanyName,
                                  StatusName = inv.InvoiceStatusId == "1" ? "Draft" : inv.InvoiceStatusId == "2" ? "ApproveFrontDesk" :
                                               inv.InvoiceStatusId == "3" ? "ApproveAdmin" : inv.InvoiceStatusId == "4" ? "Rejected" : inv.InvoiceStatusId == "5" ? "TAP" : "Completed",
                                  NetAmount = inv.NetAmount,
                                  PromptPayment = inv.PromptPayment,
                                  PromptPayment2 = inv.PromptPayment2,
                                  ValueAddedTax = inv.ValueAddedTax,
                                  ValueAddedTaxPercent = inv.ValueAddedTaxPercent,
                                  AdvertisingTax = inv.AdvertisingTax,
                                  AllocationRatio = inv.AllocationRatioId,
                                  TaxPayable = inv.TaxPayable,
                                  TaxPayablePercent = inv.TaxPayablePercent,
                                  TotalCostVendor = inv.TotalCostVendor

                              }).FirstOrDefaultAsync();
            return data;

            //return await context.InvoiceReceivables.AsNoTracking().Where(x => !(x.IsDeleted ?? false) && x.Id == Id).FirstOrDefaultAsync();

        }

        public async Task<InvoiceReceived> GetWithPoDetail(string Id)
        {
            var data = await (from inv in context.InvoiceReceivables
                              join po in context.PurchaseOrders on inv.PoId equals po.Id into p
                              from po in p.DefaultIfEmpty()
                              join curr in context.Currencies on inv.CurrencyId equals curr.Id into c
                              from curr in c.DefaultIfEmpty()
                              join client in context.Companys on inv.ClientId equals client.Id into cl
                              from client in cl.DefaultIfEmpty()
                              where !inv.IsDeleted.GetValueOrDefault() && inv.Id == Id
                              select new InvoiceReceived
                              {
                                  Id = inv.Id,
                                  InvoiceNumber = inv.InvoiceNumber,
                                  InvoiceDate = inv.InvoiceDate,
                                  PoNumber = po.PurchaseOrderNumber,
                                  AdminNotes = inv.AdminNotes,
                                  AffiliationId = inv.AffiliationId,
                                  ApprovalAdminDesk = inv.ApprovalAdminDesk,
                                  ApprovalFrontDesk = inv.ApprovalFrontDesk,
                                  ClientId = inv.ClientId,
                                  DueDays = inv.DueDays,
                                  Periode = inv.Periode,
                                  ContractCheckList = inv.ContractCheckList,
                                  ContractObject = inv.ContractObject,
                                  CurrencyId = inv.CurrencyId,
                                  Description = inv.Description,
                                  DivisionId = inv.DivisionId,
                                  GoodReceivedNumber = inv.GoodReceivedNumber,
                                  DoCheckList = inv.DoCheckList,
                                  DoObject = inv.DoObject,
                                  MainServiceCategoryId = inv.MainServiceCategoryId,
                                  ExchangeRate = (inv.ExchangeRate ?? 0),
                                  FrontDeskNotes = inv.FrontDeskNotes,
                                  GrCheckList = inv.GrCheckList,
                                  GrObject = inv.GrObject,
                                  InvoiceStatusId = inv.InvoiceStatusId == null ? "1" : inv.InvoiceStatusId,
                                  LegalId = inv.LegalEntityId, 
                                  PoCheckList = inv.PoCheckList,
                                  PoObject = inv.PoObject,
                                  ServiceCenterId = inv.ServiceCenterId,
                                  PoId = inv.PoId,
                                  TaxCheckList = inv.TaxCheckList,
                                  TaxNumber = inv.TaxNumber,
                                  TaxDate = inv.TaxDate,
                                  TaxObject = inv.TaxObject,
                                  TotalAmount = (inv.TotalAmount ?? 0),
                                  Vat = (inv.ValueAddedTax ?? 0),
                                  Value = (inv.Value ?? 0),
                                  purchase_request_number = po.PurchaseRequestNumber,
                                  JobId = inv.JobId,
                                  JobName = inv.JobId == null ? "INTERNAL" + po.PurchaseRequestNumber : inv.JobId,
                                  CurrencyName = curr.CurrencyName,
                                  InvoiceVendor = inv.InvoiceVendor,
                                  InvoiceVendorDate = inv.InvoiceVendorDate,
                                  Amount = (inv.Amount ?? 0),
                                  ClientName = client.CompanyName,
                                  StatusName = inv.InvoiceStatusId == "1" ? "Draft" : inv.InvoiceStatusId == "2" ? "ApproveFrontDesk" :
                                               inv.InvoiceStatusId == "3" ? "ApproveAdmin" : inv.InvoiceStatusId == "4" ? "Rejected" : inv.InvoiceStatusId == "5" ? "TAP" : "Completed"

                              }).FirstOrDefaultAsync();

            if (data != null)
            {
                data.purchaseOrder = await context.PurchaseOrders.Where(x => x.Id == data.PoId).FirstOrDefaultAsync();
                if (data.purchaseOrder != null)
                {
                    data.purchaseOrder.PurchaseOrderDetail = await (from a in context.PurchaseOrderDetails
                                                                    join b in context.Uom on a.UomId equals b.Id
                                                                    join c in context.ItemType on a.ItemTypeId equals c.Id
                                                                    join d in context.Item on a.ItemId equals d.Id
                                                                    where a.PurchaseOrderId == data.PoId && !(a.IsDeleted ?? false)
                                                                    select new PurchaseOrderDetail
                                                                    {
                                                                        Id = a.Id,
                                                                        PurchaseOrderId = a.PurchaseOrderId,
                                                                        ItemId = a.ItemId,
                                                                        ItemName = a.ItemName,
                                                                        ItemTypeId = a.ItemTypeId,
                                                                        UomId = a.UomId,
                                                                        Qty = a.Qty,
                                                                        UnitPrice = a.UnitPrice,
                                                                        Amount = a.Amount,
                                                                        ItemTypeName = c.TypeName,
                                                                        ItemCode = d.ItemCode,
                                                                        UomName = b.unit_symbol
                                                                    }).ToListAsync();
                }
            }
            return data;
        }

        public async Task<int> EditRangeAsync(InvoiceReceived entity)
        {
            context.PhoenixEditRange(entity);
            return await context.SaveChangesAsync();
        }

        public Task<List<InvoiceReceived>> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public async Task<int> AddRangeAsync(params InvoiceReceived[] entities)
        {
            context.PhoenixAddRange(entities);
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteAsync(InvoiceReceived entity)
        {
            context.PhoenixDelete(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteRageAsync(params InvoiceReceived[] entities)
        {
            context.PhoenixDeleteRange(entities);
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteSoftRangeAsync(params InvoiceReceived[] entities)
        {
            context.PhoenixDeleteRange(entities);
            return await context.SaveChangesAsync();
        }

        public async Task<ClientData> GetClient(string clientId)
        {
            var data = await (from company in context.Companys
                              join city in context.Citys on company.OfficeCityId equals city.Id
                              where company.Id == clientId && (company.IsClient ?? false)
                              select new ClientData
                              {
                                  CompanyName = company.CompanyName,
                                  TaxRegistrationNumber = company.TaxRegistrationNumber,
                                  Address = company.OfficeAddress,
                                  City = city.CityName,
                                  ZipCode = company.OfficeZipCode,
                                  ContactName = company.ContactName,
                                  Phone = company.Phone
                              }).FirstOrDefaultAsync();

            return data;
        }

        public async Task<ClientData> GetVendor(string vendorId)
        {
            var data = await (from company in context.Companys
                              join city in context.Citys on company.OfficeCityId equals city.Id
                              where company.Id == vendorId && company.IsClient.Equals(false) && company.IsVendor.Equals(true)
                              select new ClientData
                              {
                                  CompanyName = company.CompanyName,
                                  TaxRegistrationNumber = company.TaxRegistrationNumber,
                                  Address = company.OfficeAddress,
                                  City = city.CityName,
                                  ZipCode = company.OfficeZipCode,
                                  ContactName = company.ContactName,
                                  Phone = company.Phone
                              }).FirstOrDefaultAsync();

            return data;
        }

        public async Task<PoData> GetPo(string poId)
        {
            try
            {
                var dataap = await (from ap in context.AccountPayables
                                    where ap.PurchaseOrderId == poId
                                    select new PurchaseOrder
                                    {
                                        Id = ap.Id,
                                        SubTotal = ap.Amount
                                    }).ToListAsync();

                var countap = dataap.Count();
                var totaldata = countap;

                var total_termin = "";

                var potemp = await context.PurchaseOrders.Where(x => x.IsDeleted.Equals(false) && x.Id == poId).FirstOrDefaultAsync();
                //kalo langsung lunas 100%
                if (potemp.PercentPaymentTerm3 == 0 && potemp.PercentPaymentTerm2 == 0)
                {
                    total_termin = "1 from 1 (" + potemp.PercentPaymentTerm1 + ")";
                }//kalau di bayar 2x
                else if (potemp.PercentPaymentTerm3 == 0 && potemp.PercentPaymentTerm1 != 0 && potemp.PercentPaymentTerm2 != 0)
                {
                    if (totaldata == 0)
                    {
                        total_termin = "1 from 2 (" + potemp.PercentPaymentTerm1 + ")";

                    }
                    else if (totaldata == 1)
                    {
                        total_termin = "2 from 2 (" + potemp.PercentPaymentTerm2 + ")";
                    }
                    else
                    {

                    }
                    total_termin = "More than 2";
                }
                else
                {

                    if (totaldata == 0)
                    {
                        total_termin = "1 from 3 (" + potemp.PercentPaymentTerm1 + ")";

                    }
                    else if (totaldata == 1)
                    {
                        total_termin = "2 from 3 (" + potemp.PercentPaymentTerm2 + ")";
                    }
                    else if (totaldata == 2)
                    {
                        total_termin = "3 from 3 (" + potemp.PercentPaymentTerm3 + ")";
                    }
                    else
                    {
                        total_termin = "More than 3";
                    }

                }


                var data = await (from po in context.PurchaseOrders
                                  join bu in context.BusinessUnits on po.BusinessUnitId equals bu.Id into bun
                                  from bunw in bun.DefaultIfEmpty()
                                  join pce in context.Pces on po.JobId equals pce.JobId into pcen
                                  from pcenw in pcen.DefaultIfEmpty()
                                  join pca in context.Pcas on po.JobId equals pca.JobId into pcan
                                  from pcanw in pcan.DefaultIfEmpty()
                                  where po.Id == poId
                                  select new PoData
                                  {
                                      CurrencyId = po.CurrencyId,
                                      Value = po.SubTotal,
                                      Amount = totaldata == 0 ? (po.SubTotal * (po.PercentPaymentTerm1 / 100)) : totaldata == 1 ? (po.SubTotal * (po.PercentPaymentTerm2 / 100)) : totaldata == 2 ? (po.SubTotal * (po.PercentPaymentTerm3 / 100)) : po.SubTotal,
                                      ExchangeRate = po.ExchangeRate,
                                      DivisionId = po.BusinessUnitId,
                                      DivisionName = bunw.UnitName,
                                      termin = total_termin,
                                      JobId = po.JobId,
                                      purchase_request_id = po.PurchaseRequestId,
                                      purchase_request_number = po.PurchaseRequestNumber,
                                      PaymentMethodId = po.PaymentMethodId,
                                      PoDescription = po.Description,
                                      InvoicingType = po.InvoicingType,
                                      PercentPaymentTerm1 = po.PercentPaymentTerm1,
                                      PercentPaymentTerm2 = po.PercentPaymentTerm2,
                                      PercentPaymentTerm3 = po.PercentPaymentTerm3,
                                      PaymentTerm1 = po.SubTotal * (po.PercentPaymentTerm1 / 100),
                                      PaymentTerm2 = po.SubTotal * (po.PercentPaymentTerm2 / 100),
                                      PaymentTerm3 = po.SubTotal * (po.PercentPaymentTerm3 / 100),
                                      SubTotalPo = po.SubTotal,
                                      VatPo = po.Vat,
                                      VatPercentPo = po.VatPercent,
                                      GrandTotalPo = po.GrandTotal,
                                      MainServiceCategoryId = po.MainserviceCategoryId,
                                      BusneissUnitId = po.BusinessUnitId,
                                      LegalEntityId = po.LegalEntityId,
                                      PCENumber = pcenw.Code,
                                      PCANUmber = pcanw.Code,
                                      TypeOfExpenseId = pcenw.TypeOfExpenseId
                                  }).FirstOrDefaultAsync();
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public async Task<List<InvoiceReceivedDetail>> GetDetailById(string Id)
        {
            try
            {
                var model = await (from invd in context.InvoiceReceivableDetails
                                   join coa in context.ChartOfAccounts on invd.AccountId equals coa.Id into coan
                                   from coanw in coan.DefaultIfEmpty()
                                   join uom in context.Uom on invd.UomId equals uom.Id
                                   where invd.InvoiceReceivableId == Id && invd.IsDeleted.Equals(false)
                                   select new InvoiceReceivedDetail
                                   {
                                       Id = invd.Id,
                                       account_id = invd.AccountId,
                                       actual_cost = (decimal?)invd.ActualCost,
                                       AmountPaid = invd.AmountPaid,
                                       balance = (decimal?)invd.Balance,
                                       amountTotal = (decimal?)invd.Amount,
                                       category = invd.Category,
                                       company_brand_id = invd.CompanyBrandId,
                                       Discount = invd.Discount,
                                       d_k = invd.DK,
                                       invoice_value = (decimal?)invd.InvoiceValue,
                                       InvoiceReceivableId = invd.InvoiceReceivableId,
                                       ItemDescription = invd.ItemDescription,
                                       ItemName = invd.ItemName,
                                       ItemTypeId = invd.ItemTypeId,
                                       pce_id = invd.PceId,
                                       pce_value = (decimal?)invd.PceValue,
                                       qty = invd.Qty,
                                       RowIndex = invd.RowIndex,
                                       TaskDetail = invd.Task,
                                       Tax = invd.Tax,
                                       unit_price = invd.UnitPrice,
                                       UomId = invd.UomId,
                                       uom_name = uom.unit_name,
                                       LineCode = invd.LineCode,
                                       AccountId = invd.AccountId == null ? invd.AccountId : invd.AccountId,
                                       AccountCode = coanw.CodeRec,
                                       AccountName = coanw.Name5,
                                       TaxPayableCoaId = invd.TaxPayCoaId,
                                       TaxPayablePercent = invd.TaxPayablePct,
                                       TaxPayableCoaName = context.ChartOfAccounts.AsNoTracking().Where(x => x.Id == invd.TaxPayCoaId).FirstOrDefault().Name5

                                   }).ToListAsync();
                return model;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }






        //public async Task<PoData> GetPo(string poId)
        //{

        //    var dataap = await (from ap in context.AccountPayables
        //                        where ap.POId == poId
        //                        select new PurchaseOrder
        //                        {
        //                            Id = ap.Id,
        //                            Amount = ap.Amount
        //                        }).ToListAsync();

        //    var datatap = await (from tap in context.TemporaryAccountPayables
        //                         where tap.POId == poId
        //                         select new PurchaseOrder
        //                         {
        //                             Id = tap.Id,
        //                             Amount = tap.Amount
        //                         }).ToListAsync();

        //    var countap = dataap.Count();
        //    var counttap = datatap.Count();
        //    var totaldata = countap + counttap;



        //    var data = await (from po in context.PurchaseOrders
        //                      join bu in context.BusinessUnits on po.BusinessUnitId equals bu.Id
        //                      where po.Id == poId
        //                      select new PoData
        //                      {
        //                          CurrencyId = po.CurrencyId,
        //                          Amount = po.Amount,
        //                          ExchangeRate = po.ExchangeRate,
        //                          DivisionId = po.BusinessUnitId,
        //                          DivisionName = bu.UnitName,
        //                          termin = totaldata == 0 ? po.PercentPaymentTerm1.ToString() + " 1 from 3 " : totaldata == 1 ? po.PercentPaymentTerm2.ToString() + " 1 from 3 " : totaldata == 2 ? po.PercentPaymentTerm3.ToString() + " 1 from 3 " : 0.ToString(),
        //                          JobId = po.JobId.Trim(),
        //                          purchase_request_id = po.PurchaseRequestId,
        //                          purchase_request_number = po.PurchaseRequestNumber

        //                      }).FirstOrDefaultAsync();
        //    return data;
        //}


    }
}
