﻿using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace Phoenix.Data.Services.Finance.Transaction
{
    public interface IFixedAssetMovementService : IDataService<FixedAssetMovement>
    {
        Task<List<FixedAssetMovement>> GetByFixedAsset(string faId);
    }
    public class FixedAssetMovementService : IFixedAssetMovementService
    {
        readonly DataContext context;

        public FixedAssetMovementService(DataContext context)
        {
            this.context = context;
        }

        public async Task<int> AddAsync(FixedAssetMovement entity)
        {
            entity.Id = Guid.NewGuid().ToString();
            entity.FaaNumber = await TransID.GetTransId(context, "FAA", DateTime.Today);
            await context.PhoenixAddAsync(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> AddRangeAsync(params FixedAssetMovement[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteAsync(FixedAssetMovement entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteRageAsync(params FixedAssetMovement[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteSoftAsync(FixedAssetMovement entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteSoftRangeAsync(params FixedAssetMovement[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> EditAsync(FixedAssetMovement entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> EditRangeAsync(FixedAssetMovement entity)
        {
            throw new NotImplementedException();
        }

        public async Task<List<FixedAssetMovement>> Get()
        {
            var query = await context.FixedAssetMovement.ToListAsync();
            return query;
            //var query = await (from fam in context.FixedAssetMovement
            //                   join loc in context.MasterLocationProcurement on fam.FromLocationID equals loc.Id
            //                   join con in context.MasterConditionProcurement on fam.Condition equals con.Id
            //                   join div in context.BusinessUnits on fam.BusinessUnitID equals div.Id
            //                   join le in context.LegalEntity on fam.LegalEntityID equals le.Id
            //                   join aff in context.Affiliations on fam.AffiliationID equals aff.Id
            //                   select new FixedAssetMovement
            //                   {
            //                       Id = fam.Id,
            //                       FaaDateTime = fam.FaaDateTime,
            //                       FromLocationID = loc.LocationStore,
            //                       Condition = con.ConditionName,
            //                       BusinessUnitID = div.UnitName,
            //                       LegalEntityID = le.LegalEntityName,
            //                       AffiliationID = aff.AffiliationName,
            //                       Remark = fam.Remark,
            //                   }
            //    ).ToListAsync();
            //return query;
        }

        public Task<List<FixedAssetMovement>> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public async Task<FixedAssetMovement> Get(string id)
        {
            var query = await (from fam in context.FixedAssetMovement
                               join emp in context.EmployeeBasicInfos on fam.Employee equals emp.Id
                               join loc in context.MasterLocationProcurement on fam.FromLocationID equals loc.Id
                               join con in context.MasterConditionProcurement on fam.Condition equals con.Id
                               join bu in context.BusinessUnits on fam.BusinessUnitID equals bu.Id
                               join le in context.LegalEntity on fam.LegalEntityID equals le.Id
                               join aff in context.Affiliations on fam.AffiliationID equals aff.Id
                               join status in context.FixedAssetMovementStatus on fam.FixedAssetMovementStatusID equals status.Id
                               where fam.IsDeleted.Equals(false) && fam.Id == id
                               select new FixedAssetMovement
                               {
                                   Id=fam.Id,
                                   FaaDateTime = fam.FaaDateTime,
                                   FromLocationID = loc.Id,
                                   ToLocationID = loc.Id,
                                   LocationName = loc.LocationStore,
                                   Condition = con.Id,
                                   ConditionName = con.ConditionName,
                                   Employee = emp.Id,
                                   EmployeeName = emp.NameEmployee,
                                   BusinessUnitID = bu.Id,
                                   Division = bu.UnitName,
                                   FromBusinessUnitID = bu.Id,
                                   ToBusinessUnitID = bu.Id,
                                   LegalEntityID = le.Id,
                                   LegalEntityName = le.LegalEntityName,
                                   AffiliationID = aff.Id,
                                   AffiliationName = aff.AffiliationName,
                                   Remark = fam.Remark,
                                   FixedAssetMovementStatusID=status.Id,
                                   StatusName = status.StatusName
                               }).FirstOrDefaultAsync();
            //var query = await context.FixedAssetMovement.Where(x=>x.IsDeleted.Equals(false)&&x.Id==id).FirstOrDefaultAsync();
            return query;
        }

        public async Task<List<FixedAssetMovement>> GetByFixedAsset(string faId)
        {
            var query = await (from fam in context.FixedAssetMovement
                               join emp in context.EmployeeBasicInfos on fam.Employee equals emp.Id
                               join loc in context.MasterLocationProcurement on fam.FromLocationID equals loc.Id
                               join con in context.MasterConditionProcurement on fam.Condition equals con.Id
                               join bu in context.BusinessUnits on fam.BusinessUnitID equals bu.Id
                               join le in context.LegalEntity on fam.LegalEntityID equals le.Id
                               join aff in context.Affiliations on fam.AffiliationID equals aff.Id
                               join status in context.FixedAssetMovementStatus on fam.FixedAssetMovementStatusID equals status.Id
                               where fam.IsDeleted.Equals(false) && fam.FixedAssetId == faId
                               select new FixedAssetMovement
                               {
                                   Id = fam.Id,
                                   FaaDateTime = fam.FaaDateTime,
                                   FromLocationID = loc.Id,
                                   ToLocationID = loc.Id,
                                   LocationName = loc.LocationStore,
                                   Condition = con.Id,
                                   ConditionName = con.ConditionName,
                                   Employee = emp.Id,
                                   EmployeeName = emp.NameEmployee,
                                   BusinessUnitID = bu.Id,
                                   Division = bu.UnitName,
                                   FromBusinessUnitID = bu.Id,
                                   ToBusinessUnitID = bu.Id,
                                   LegalEntityID = le.Id,
                                   LegalEntityName = le.LegalEntityName,
                                   AffiliationID = aff.Id,
                                   AffiliationName = aff.AffiliationName,
                                   Remark = fam.Remark,
                                   FixedAssetMovementStatusID = status.Id,
                                   StatusName = status.StatusName
                               }).ToListAsync();
            //var query = await context.FixedAssetMovement.Where(x => x.IsDeleted.Equals(false) && x.FixedAssetId == faId).ToListAsync();
            return query;
        }
    }
}
