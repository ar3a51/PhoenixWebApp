﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Phoenix.Data.Models.Finance.Transaction;
using Phoenix.Data.Models;
using Phoenix.Data.Attributes;

namespace Phoenix.Data.Services.Finance.Transaction
{
    public interface IPurchaseOrderService
    {
        Task<int> ApproveAsync(PurchaseOrder entity);
        Task<int> AddAsync(PurchaseOrder entity);
        Task<int> EditAsync(PurchaseOrder entity);
        Task<List<PurchaseOrder>> GetPOList(string status);
        Task<PurchaseOrder> Get(string id);
        Task<List<PurchaseOrderDetail>> GetDetailPOList(string id);
        Task<List<PurchaseOrder>> GetByVendor(string vendorId);
        Task<EmployeeBasicInfo> GetDataEmployee(string Id);
    }

    public class PurchaseOrderService : IPurchaseOrderService
    {
        readonly DataContext context;
        readonly GlobalFunctionApproval globalFcApproval;
        readonly Services.Um.IManageMenuService menuService;
        public PurchaseOrderService(DataContext context, GlobalFunctionApproval globalFcApproval, Services.Um.IManageMenuService menuService)
        {
            this.context = context;
            this.globalFcApproval = globalFcApproval;
            this.menuService = menuService;
        }

        public async Task<int> AddAsync(PurchaseOrder entity)
        {
            return await AddEdit(entity, false);
        }

        public async Task<int> EditAsync(PurchaseOrder entity)
        {
            return await AddEdit(entity, true);
        }

        private async Task<int> AddEdit(PurchaseOrder entity, bool isEdit)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    entity.Status = StatusTransaction.StatusName(entity.StatusApproval);
                    if (isEdit)
                    {
                        context.PhoenixEdit(entity);
                    }
                    else
                    {
                        entity.Id = await TransID.GetTransId(context, Code.PCA, DateTime.Today);
                        entity.PurchaseOrderNumber = entity.Id;
                        await context.PhoenixAddAsync(entity);
                        await context.SaveChangesAsync();
                    }

                    if (!string.IsNullOrEmpty(entity.Id))
                    {
                        var detail = context.PurchaseOrderDetails.AsNoTracking().Where(x => x.PurchaseOrderId == entity.Id).ToList();
                        if (detail.Count > 0)
                        {
                            context.PurchaseOrderDetails.RemoveRange(detail);
                        }
                    }

                    if (entity.PurchaseOrderDetail != null)
                    {
                        Parallel.ForEach(entity.PurchaseOrderDetail, (item) =>
                        {
                            item.Id = Guid.NewGuid().ToString();
                            item.PurchaseOrderId = entity.Id;
                            context.PhoenixAddAsync(item);
                        });
                    }

                    var isApprove = true;
                    if (entity.Status != StatusTransactionName.Draft)
                    {
                        var dataMenu = await menuService.GetByUniqueName(MenuUnique.PO).ConfigureAwait(false);
                        var vm = new Models.ViewModel.TransApprovalHrisVm()
                        {
                            MenuId = dataMenu.Id,
                            RefId = entity.Id,
                            DetailLink = $"{ApprovalLink.PO}?Id={entity.Id}"
                        };
                        var subGroupId = Phoenix.Shared.Core.PrincipalHelpers.ClainJwtPrincipalHelper.GetBusinessSubGroup(context.HttpContext);
                        var divisionId = Phoenix.Shared.Core.PrincipalHelpers.ClainJwtPrincipalHelper.GetBusinessUnitDivisi(context.HttpContext);
                        var employeeId = Phoenix.Shared.Core.PrincipalHelpers.ClainJwtPrincipalHelper.GetEmployeeId(context.HttpContext);
                        isApprove = await globalFcApproval.Save(vm, subGroupId, divisionId, employeeId);
                        if (isApprove)
                        {
                            var save = await context.SaveChangesAsync();
                            transaction.Commit();
                            return save;
                        }
                        else
                        {
                            transaction.Rollback();
                            throw new Exception("please check the approval template that will be processed");
                        }
                    }
                    else
                    {
                        entity.Status = StatusTransaction.StatusName(StatusTransaction.Draft);
                        context.PurchaseOrders.Update(entity);
                        var save = await context.SaveChangesAsync();
                        transaction.Commit();
                        return save;
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<int> ApproveAsync(PurchaseOrder entity)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var save = await globalFcApproval.UnsafeProcessApproval(entity.Id, entity.StatusApproval, entity.RemarkRejected, entity);
                    var approval = context.TrTemplateApprovals.Where(x => x.RefId == entity.Id && !(x.IsDeleted ?? false)).FirstOrDefault();
                    if (approval != null)
                    {
                        entity.Status = StatusTransaction.StatusName(approval.StatusApproved.GetHashCode());
                        if (approval.StatusApproved == Models.Um.StatusApproved.Approved)
                        {
                            var pca = await context.Pcas.AsNoTracking().Where(x => x.Id == entity.JobPaId).FirstOrDefaultAsync();
                            if (pca != null)
                            {
                                pca.PcaBalance = (pca.PcaBalance ?? 0) + (entity.SubTotal ?? 0);
                                context.Pcas.Update(pca);
                            }

                        }

                        if (approval.StatusApproved == Models.Um.StatusApproved.Rejected)
                        {
                            var rfq = await context.RequestForQuotation.AsNoTracking().Where(x => x.Id == entity.RequestForQuotationId).FirstOrDefaultAsync();
                            rfq.Status = entity.Status;
                            context.RequestForQuotation.Update(rfq);
                        }
                    }
                    else
                    {
                        entity.Status = StatusTransaction.StatusName(StatusTransaction.Draft);
                    }
                    context.PurchaseOrders.Update(entity);

                    save = await context.SaveChangesAsync();
                    transaction.Commit();
                    return save;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<List<PurchaseOrder>> GetPOList(string status)
        {
            //yang bisa akses ke pca adalah pm team, creator dan approval
            var userId = context.GetUserId();
            var employeeId = context.GetEmployeeId();

            var team = await (from a in context.PmTeams
                              join b in context.PmTeamMembers on a.Id equals b.TeamId
                              where !(a.IsDeleted ?? false) && !(b.IsDeleted ?? false) && b.ApplicationUserId == userId
                              select new { a.JobId }).ToListAsync();

            var approval = await (from a in context.TrTemplateApprovals
                                  join b in context.TrUserApprovals on a.Id equals b.TrTempApprovalId
                                  where b.EmployeeBasicInfoId == employeeId
                                  select new { a.RefId }).ToListAsync();

            var result = await (from a in context.PurchaseOrders
                                join b in context.EmployeeBasicInfos on a.CreatedBy equals b.Id
                                where !(a.IsDeleted ?? false) &&
                                (string.IsNullOrEmpty(status) ? true : (a.Status ?? StatusTransactionName.Draft) == status)
                                select new PurchaseOrder
                                {
                                    Id = a.Id,
                                    RequestForQuotationId = a.RequestForQuotationId,
                                    RequestForQuotationNumber = a.RequestForQuotationNumber,
                                    PurchaseOrderNumber = a.PurchaseOrderNumber,
                                    PurchaseRequestId = a.PurchaseRequestId,
                                    JobId = a.JobId,
                                    RequestorName = b.NameEmployee,
                                    CompanyId = a.CompanyId,
                                    CompanyName = a.CompanyName,
                                    GrandTotal = a.GrandTotal,
                                    Status = a.Status,
                                    CreatedOn = a.CreatedOn
                                }).ToListAsync();
            return result.OrderByDescending(x => x.CreatedOn).ToList();
        }


        public async Task<PurchaseOrder> Get(string id)
        {
            var result = await context.PurchaseOrders.Where(x => !(x.IsDeleted ?? false) && x.Id == id).FirstOrDefaultAsync();
            if (result != null)
            {
                var rfq = await context.RequestForQuotation.Where(x => x.Id == result.RequestForQuotationId).FirstOrDefaultAsync();
                if (rfq != null)
                {
                    result.JobPaId = rfq.JobPaId;
                    result.JobPeId = rfq.JobPeId;

                    var pca = await context.Pcas.Where(x => x.Id == rfq.JobPaId).FirstOrDefaultAsync();
                    if (pca != null) result.ShareServicesId = pca.ShareservicesId;
                }

                var job = await context.JobDetail.Where(x => x.Id == result.JobId).FirstOrDefaultAsync();
                if (job != null) result.JobName = job.JobName;

                var rfqVendor = await context.RequestForQuotationVendor.Where(x => x.RequestForQuotationId == result.RequestForQuotationId && x.VendorId == result.CompanyId).FirstOrDefaultAsync();
                if (rfqVendor != null)
                {
                    result.VatPercentRFQ = rfqVendor.VatPercent;
                    result.VatRFQ = rfqVendor.Vat;
                    result.SubTotalRFQ = rfqVendor.SubTotalVendor;
                    result.GrandTotalRFQ = rfqVendor.TotalAmountVendor;
                }
            }
            return result;
        }

        public async Task<List<PurchaseOrderDetail>> GetDetailPOList(string id)
        {
            var result = await (from a in context.PurchaseOrderDetails
                                join b in context.Uom on a.UomId equals b.Id
                                join c in context.ItemType on a.ItemTypeId equals c.Id
                                where a.PurchaseOrderId == id && !(a.IsDeleted ?? false)
                                select new PurchaseOrderDetail
                                {
                                    Id = a.Id,
                                    PurchaseOrderId = a.PurchaseOrderId,
                                    TaskId = a.TaskId,
                                    Task = a.Task,
                                    ItemTypeId = a.ItemTypeId,
                                    ItemId = a.ItemId,
                                    ItemCode = a.ItemCode,
                                    ItemName = a.ItemName,
                                    UomId = a.UomId,
                                    Qty = a.Qty,
                                    UnitPrice = a.UnitPrice,
                                    Amount = a.Amount,
                                    ItemTypeName = c.TypeName,
                                    UomName = b.unit_symbol
                                }).ToListAsync();
            return result;
        }

        public async Task<List<PurchaseOrder>> GetByVendor(string vendorId)
        {
            return await context.PurchaseOrders.AsNoTracking().Where(x => !(x.IsDeleted ?? false) && x.CompanyId == vendorId).ToListAsync();
        }

        public async Task<EmployeeBasicInfo> GetDataEmployee(string Id)
        {
            var data = await (from x in context.EmployeeBasicInfos.Where(c => c.Id == Id)
                              select new EmployeeBasicInfo
                              {
                                  Id = x.Id,
                                  NameEmployee = x.NameEmployee,
                                  Email = x.Email,
                                  Gender = x.Gender,
                                  IsDeleted = x.IsDeleted
                              }).Where(x => x.IsDeleted == false).FirstOrDefaultAsync();
            return data;
        }
    }
}
