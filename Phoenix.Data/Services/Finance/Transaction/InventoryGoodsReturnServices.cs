﻿using Phoenix.Data;
using Phoenix.Data.Models.Finance.Transaction.GoodReturn;
using Phoenix.Data.Services.Finance.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Phoenix.Data.Services.Finance.Transaction
{
    public interface IGoodsReturnServices : IDataService<InventoryGoodsReturn>
    {
        Task<int> AddWithDetailAsync(InventoryGoodsReturn Header, List<InventoryGoodsReturnDetail> Details);
        Task<int> EditWithDetailAsync(InventoryGoodsReturn Header, List<InventoryGoodsReturnDetail> Details);
        Task<InventoryGoodsReturnDetail> GetDetail(string Id);
        Task<List<InventoryGoodsReturnDetail>> GetListDetail(string headerId);
        Task<InventoryGoodsReturn> Approve(InventoryGoodsReturn model);
        Task<InventoryGoodsReturn> Complete(InventoryGoodsReturn model);
        Task<InventoryGoodsReturn> GetByNumber(string number);
        Task<InventoryGoodsReturn> GetByOpenStatus(string Id);
    }
}
public class InventoryGoodsReturnServices : IGoodsReturnServices
{
    readonly DataContext context;

    public InventoryGoodsReturnServices(DataContext context)
    {
        this.context = context;
    }
    public Task<int> AddAsync(InventoryGoodsReturn entity)
    {
        throw new NotImplementedException();
    }

    public Task<int> AddRangeAsync(params InventoryGoodsReturn[] entities)
    {
        throw new NotImplementedException();
    }

    public Task<int> AddWithDetailAsync(InventoryGoodsReturn Header, List<InventoryGoodsReturnDetail> Details)
    {
        throw new NotImplementedException();
    }

    public Task<InventoryGoodsReturn> Approve(InventoryGoodsReturn model)
    {
        throw new NotImplementedException();
    }

    public Task<InventoryGoodsReturn> Complete(InventoryGoodsReturn model)
    {
        throw new NotImplementedException();
    }

    public Task<int> DeleteAsync(InventoryGoodsReturn entity)
    {
        throw new NotImplementedException();
    }

    public Task<int> DeleteRageAsync(params InventoryGoodsReturn[] entities)
    {
        throw new NotImplementedException();
    }

    public Task<int> DeleteSoftAsync(InventoryGoodsReturn entity)
    {
        throw new NotImplementedException();
    }

    public Task<int> DeleteSoftRangeAsync(params InventoryGoodsReturn[] entities)
    {
        throw new NotImplementedException();
    }

    public Task<int> EditAsync(InventoryGoodsReturn entity)
    {
        throw new NotImplementedException();
    }

    public Task<int> EditRangeAsync(InventoryGoodsReturn entity)
    {
        throw new NotImplementedException();
    }

    public Task<int> EditWithDetailAsync(InventoryGoodsReturn Header, List<InventoryGoodsReturnDetail> Details)
    {
        throw new NotImplementedException();
    }

   
    public async Task<List<InventoryGoodsReturn>> Get()
    {
        List<InventoryGoodsReturn> List = new List<InventoryGoodsReturn>();
        
        List = await (
            from gr in context.GoodRequest.Where(x=> x.IsDeleted !=null && x.Status.Equals("5")) join
             grd in context.GoodRequestDetail.Where(x => x.IsDeleted != true) on gr.Id equals grd.GoodRequestId
            join igr in context.InventoryGoodsReturn.Where(x => x.IsDeleted != null) on grd.InvFaDetailId equals igr.InvFaId into aab
            from bnull in aab.DefaultIfEmpty()
            select new InventoryGoodsReturn()
            {
                Id = bnull.Id,
                GoodRequestNumber = context.GoodRequest.Where(x => x.Id == grd.GoodRequestId).SingleOrDefault().GoodRequestNumber,
                InvFaId = grd.InvFaDetailId,
                ReturnDate=bnull.ReturnDate,
                Type= context.GoodRequest.Where(x => x.Id == grd.GoodRequestId).SingleOrDefault().Type,
                InvFaNumber= context.GoodRequest.Where(x => x.Id == grd.GoodRequestId).SingleOrDefault().Type== "FIXED ASSET"? context.FixedAsset.Where(x=>x.Id.Equals(grd.InvFaDetailId)).SingleOrDefault().FixedAssetNumber : context.Inventory.Where(x => x.Id.Equals(grd.InvFaDetailId)).SingleOrDefault().InventoryInNumber,
                InvFaName= context.GoodRequest.Where(x => x.Id == grd.GoodRequestId).SingleOrDefault().Type == "FIXED ASSET" ? context.FixedAsset.Where(x => x.Id.Equals(grd.InvFaDetailId)).SingleOrDefault().FixedAssetName : context.Inventory.Where(x => x.Id.Equals(grd.InvFaDetailId)).SingleOrDefault().InventoryName,
                RequestedBy=context.EmployeeBasicInfos.Where(x=> x.Id.Equals(grd.CreatedBy)).SingleOrDefault().NameEmployee,
                Status=bnull.Status==null ? context.GoodRequest.Where(x => x.Id == grd.GoodRequestId).SingleOrDefault().Status: bnull.Status
            }
            ).ToListAsync();

        return List;
    }

    public Task<List<InventoryGoodsReturn>> Get(int skip, int take)
    {
        throw new NotImplementedException();
    }

    public Task<InventoryGoodsReturn> Get(string id)
    {
        throw new NotImplementedException();
    }

    public Task<InventoryGoodsReturn> GetByNumber(string number)
    {
        throw new NotImplementedException();
    }

    public async Task<InventoryGoodsReturn> GetByOpenStatus(string Id)
    {
        var query =await (
            from gr in context.GoodRequest join grd in context.GoodRequestDetail on gr.Id equals grd.GoodRequestId
            where grd.Id.Equals(Id)
            select new InventoryGoodsReturn() 
            { 
                GoodRequestNumber=gr.GoodRequestNumber,
                Type=gr.Type,
                RequestedBy=gr.RequestedBy,
                RequestedOn = gr.RequestedOn,
                InvFaName=gr.Type== "FIXED ASSET" ? context.FixedAsset.Where(x => x.Id.Equals(grd.InvFaDetailId)).SingleOrDefault().FixedAssetName : context.Inventory.Where(x => x.Id.Equals(grd.InvFaDetailId)).SingleOrDefault().InventoryName,
                Purpose=grd.Purpose,
                OnBehalfId=gr.OnBehalfId

            }
                     ).SingleOrDefaultAsync();
        return query;
    }

    public Task<InventoryGoodsReturnDetail> GetDetail(string Id)
    {
        throw new NotImplementedException();
    }

    public Task<List<InventoryGoodsReturnDetail>> GetListDetail(string headerId)
    {
        throw new NotImplementedException();
    }
}
