﻿using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Attributes;
using Phoenix.Data.Models;
using Phoenix.Data.Models.Finance.Transaction;
using Phoenix.Data.Models.ViewModel;
using Phoenix.Data.Services.Um;
using Phoenix.Shared.Core.PrincipalHelpers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.Data.Services.Finance.Transaction
{
    public interface IInvoiceDeliveryNotesService : IDataService<InvoiceDeliveryNotes>
    {
        Task<List<AccountReceivable>> GetInvoiceChoiceList(string invoiceid, string clientid, string brandid, string divisionid, string legalid);
        Task<List<AccountReceivable>> ReadDetail(string Id);
    }
    public class InvoiceDeliveryNotesService : IInvoiceDeliveryNotesService
    {
        readonly DataContext context;
        readonly StatusLogService log;
        readonly FileService uploadFile;
        public InvoiceDeliveryNotesService(DataContext context, StatusLogService log, FileService uploadFile)
        {
            this.context = context;
            this.log = log;
            this.uploadFile = uploadFile;
        }

        public async Task<int> AddAsync(InvoiceDeliveryNotes entity)
        {
            int savedCount = 0;
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    entity.Id = await TransID.GetTransId(context, Code.InvoiceDeliveryNotes, DateTime.Today);
                    entity.Status = "Open";
                    string invId = "";
                    foreach(var i in entity.listInvoice)
                    {
                        invId += i.Id + ";";
                    }
                    invId = invId.Remove(invId.Length - 1);
                    entity.InvoiceNumber = invId;
                    var _model = await ProcessUpload(entity);
                    await context.PhoenixAddAsync(_model);
                    savedCount = await context.SaveChangesAsync();
                    await log.AddAsync(new StatusLog() { TransactionId = entity.Id, Status = "Open", Description = entity.Remarks });
                    transaction.Commit();
                    return savedCount;
                }
                catch (Exception ex)
                {
                    savedCount = 0;
                    throw new Exception(ex.Message.ToString());
                }
            }
        }

        private async Task<InvoiceDeliveryNotes> ProcessUpload(InvoiceDeliveryNotes entity)
        {
            var file1 = await uploadFile.Upload(entity.DocFile, null);
            if (!string.IsNullOrEmpty(file1))
            {
                entity.FileMasterId = file1;
            }
            return entity;
        }

        public Task<int> AddRangeAsync(params InvoiceDeliveryNotes[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteAsync(InvoiceDeliveryNotes entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteRageAsync(params InvoiceDeliveryNotes[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteSoftAsync(InvoiceDeliveryNotes entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteSoftRangeAsync(params InvoiceDeliveryNotes[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> EditAsync(InvoiceDeliveryNotes entity)
        {
            int savedCount = 0;
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    string invId = "";
                    foreach (var i in entity.listInvoice)
                    {
                        invId += i.Id + ";";
                        //todo update AR
                        var model = await context.AccountReceivables.AsNoTracking().Where(x => x.Id == i.Id).FirstOrDefaultAsync();
                        model.InvoiceDeliveryDate = entity.DeliveredOn;
                        model.InvoiceDeliveryNotesId = entity.Id;
                        context.PhoenixEdit(model);
                    }
                    invId = invId.Remove(invId.Length - 1);
                    entity.InvoiceNumber = invId;
                    entity.Status = "Closed";

                    var _model = await ProcessUpload(entity);
                    context.PhoenixEdit(_model);
                    savedCount = await context.SaveChangesAsync();
                    await log.AddAsync(new StatusLog() { TransactionId = entity.Id, Status = entity.Status, Description = entity.Remarks });
                    transaction.Commit();
                    return savedCount;
                }
                catch (Exception ex)
                {
                    savedCount = 0;
                    throw new Exception(ex.Message.ToString());
                }
            }
        }

        public Task<int> EditRangeAsync(InvoiceDeliveryNotes entity)
        {
            throw new NotImplementedException();
        }

        public async Task<List<InvoiceDeliveryNotes>> Get()
        {
            return await (
                from a in context.InvoiceDeliveryNotes
                join b in context.Companys on a.ClientId equals b.Id
                select new InvoiceDeliveryNotes
                {
                    Attention = a.Attention,
                    ClientName = b.CompanyName,
                    Id = a.Id,
                    ClientId = a.ClientId,
                    DeliveredBy = a.DeliveredBy,
                    DeliveredOn = a.DeliveredOn,
                    InvoiceNumber = a.InvoiceNumber,
                    LegalEntityId = a.LegalEntityId,
                    ReceivedBy = a.ReceivedBy,
                    ReceivedOn = a.ReceivedOn,
                    Remarks = a.Remarks,
                    Status = a.Status,

                    ApprovedBy = a.ApprovedBy,
                    ApprovedOn = a.ApprovedOn,
                    CreatedBy = a.CreatedBy,
                    CreatedOn = a.CreatedOn,
                    DeletedBy = a.DeletedBy,
                    DeletedOn = a.DeletedOn,
                    IsActive = a.IsActive,
                    IsDefault = a.IsDefault,
                    IsDeleted = a.IsDeleted,
                    IsLocked = a.IsLocked,
                    ModifiedBy = a.ModifiedBy,
                    ModifiedOn = a.ModifiedOn,
                    OwnerId = a.OwnerId
                }).ToListAsync();
        }

        public Task<List<InvoiceDeliveryNotes>> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public async Task<InvoiceDeliveryNotes> Get(string id)
        {
           var data = await (
                from a in context.InvoiceDeliveryNotes
                join b in context.Companys on a.ClientId equals b.Id
                join c in context.EmployeeBasicInfos on a.CreatedBy equals c.Id
                where a.Id == id
                select new InvoiceDeliveryNotes
                {
                    Attention = a.Attention,
                    ClientName = b.CompanyName,
                    Id = a.Id,
                    ClientId = a.ClientId,
                    DeliveredBy = a.DeliveredBy,
                    DeliveredOn = a.DeliveredOn,
                    InvoiceNumber = a.InvoiceNumber,
                    LegalEntityId = a.LegalEntityId,
                    ReceivedBy = a.ReceivedBy,
                    ReceivedOn = a.ReceivedOn,
                    Remarks = a.Remarks,
                    Status = a.Status,
                    CreatedByName = c.NameEmployee,
                    FileMasterId = a.FileMasterId,

                    ApprovedBy = a.ApprovedBy,
                    ApprovedOn = a.ApprovedOn,
                    CreatedBy = a.CreatedBy,
                    CreatedOn = a.CreatedOn,
                    DeletedBy = a.DeletedBy,
                    DeletedOn = a.DeletedOn,
                    IsActive = a.IsActive,
                    IsDefault = a.IsDefault,
                    IsDeleted = a.IsDeleted,
                    IsLocked = a.IsLocked,
                    ModifiedBy = a.ModifiedBy,
                    ModifiedOn = a.ModifiedOn,
                    OwnerId = a.OwnerId
                }).FirstOrDefaultAsync();

            return await GetFile(data);
        }

        private async Task<InvoiceDeliveryNotes> GetFile(InvoiceDeliveryNotes data)
        {
            if (data.FileMasterId != null)
            {
                var a = await context.Filemasters.Where(x => x.Id == data.FileMasterId).FirstOrDefaultAsync();
                if (a != null) data.FileMasterName = a.Name;
            }
          
            return data;
        }

        public async Task<List<AccountReceivable>> GetInvoiceChoiceList(string invoiceid, string clientid, string brandid, string divisionid, string legalid)
        {
            List<string> listInvoice = new List<string>();
            var _listInvoice = await (from a in context.InvoiceDeliveryNotes where !(a.IsDeleted ?? false) select a.InvoiceNumber).AsNoTracking().ToListAsync();
            foreach(var x in _listInvoice)
            {
                string [] invSplited = x.Split(';');
                if(invSplited.Length > 1)
                {
                    for(int z = 0; z< invSplited.Length; z++)
                    {
                        listInvoice.Add(invSplited[z]);
                    }
                }
                else
                {
                    listInvoice.Add(x);
                }
            }

            var data = await (
                  from a in context.AccountReceivables
                  join b in context.Currencies on a.CurrencyId equals b.Id
                  join c in context.CompanyBrands on a.BrandId equals c.Id
                  join d in context.Companys on a.ClientId equals d.Id
                  join e in context.LegalEntity on a.LegalEntityId equals e.Id
                  join divisi in (
                                 from x in context.BusinessUnits
                                 join y in context.BusinessUnitTypes on x.BusinessUnitTypeId equals y.Id
                                 where y.BusinessUnitLevel == BusinessUnitLevelCode.Division
                                 select x
                             ) on a.BusinessUnitId equals divisi.Id
                  where a.Id.Contains((invoiceid == "0" ? a.Id : invoiceid))
                                    && a.BrandId == (brandid == "0" ? a.BrandId : brandid)
                                    && a.BusinessUnitId == (divisionid == "0" ? a.BusinessUnitId : divisionid)
                                    && a.LegalEntityId == (legalid == "0" ? a.LegalEntityId : legalid)
                  select new AccountReceivable
                  {
                      Id = a.Id,
                      TransactionDate = a.TransactionDate,
                      VatNo = a.VatNo,
                      CurrencyCode = b.CurrencyCode,
                      Amount = a.Amount,
                      ClientName = d.CompanyName,
                      ClientBrand = c.BrandName,
                      LegalEntity = e.LegalEntityName,
                      BusinessUnitName = divisi.UnitName,
                      DueDate = a.DueDate
                  }).ToListAsync();

            var return_data = data.Where(x => !listInvoice.Any(y => y == x.Id)).ToList();
            return return_data;
        }

        public async Task<List<AccountReceivable>> ReadDetail(string Id)
        {
            var model = await context.InvoiceDeliveryNotes.AsNoTracking().Where(x => x.Id == Id).FirstOrDefaultAsync();
            List<AccountReceivable> List = new List<AccountReceivable>();
            List<string> listInv = new List<string>();
            if (model != null)
            {
                var arrInv = model.InvoiceNumber.Split(';');
                for (int x = 0; x < arrInv.Length; x++)
                {
                    listInv.Add(arrInv[x]);
                }
            }

            var data = await (
                  from a in context.AccountReceivables
                  join b in context.Currencies on a.CurrencyId equals b.Id
                  join c in context.CompanyBrands on a.BrandId equals c.Id
                  join d in context.Companys on a.ClientId equals d.Id
                  join e in context.LegalEntity on a.LegalEntityId equals e.Id
                  join divisi in (
                                 from x in context.BusinessUnits
                                 join y in context.BusinessUnitTypes on x.BusinessUnitTypeId equals y.Id
                                 where y.BusinessUnitLevel == BusinessUnitLevelCode.Division
                                 select x
                             ) on a.BusinessUnitId equals divisi.Id
                  where listInv.Contains(a.Id)
                  select new AccountReceivable
                  {
                      Id = a.Id,
                      TransactionDate = a.TransactionDate,
                      VatNo = a.VatNo,
                      CurrencyCode = b.CurrencyCode,
                      Amount = a.Amount,
                      ClientName = d.CompanyName,
                      ClientBrand = c.BrandName,
                      LegalEntity = e.LegalEntityName,
                      BusinessUnitName = divisi.UnitName,
                      DueDate = a.DueDate
                  }).ToListAsync();

            return data;
        }
    }
}
