using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Models;
using Phoenix.Data.RestApi;
using Phoenix.Shared.Core.Contexts;

namespace Phoenix.Data
{
    public interface IMasterAllocationRatioService : IDataService<MasterAllocationRatio>
    {
    }

    public class MasterAllocationRatioService : IMasterAllocationRatioService
    {
        readonly DataContext context;

        /// <summary>
        /// And endpoint to manage MasterAllocationRatio
        /// </summary>
        /// <param name="context">Database context</param>
        public MasterAllocationRatioService(DataContext context)
        {
            this.context = context;
        }

        public async Task<int> AddAsync(MasterAllocationRatio entity)
        {
            entity.Id = Guid.NewGuid().ToString();
            await context.PhoenixAddAsync(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> AddRangeAsync(params MasterAllocationRatio[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteAsync(MasterAllocationRatio entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteRageAsync(params MasterAllocationRatio[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> DeleteSoftAsync(MasterAllocationRatio entity)
        {
            context.PhoenixDelete(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> DeleteSoftRangeAsync(params MasterAllocationRatio[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> EditAsync(MasterAllocationRatio entity)
        {
            context.PhoenixEdit(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> EditRangeAsync(MasterAllocationRatio entity)
        {
            throw new NotImplementedException();
        }

        public async Task<List<MasterAllocationRatio>> Get() => await context.MasterAllocationRatios.AsNoTracking().Where(x => !(x.IsDeleted ?? false)).ToListAsync();

        public Task<List<MasterAllocationRatio>> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public async Task<MasterAllocationRatio> Get(string Id) => await context.MasterAllocationRatios.AsNoTracking().Where(x => !(x.IsDeleted ?? false) && x.Id == Id).FirstOrDefaultAsync();
    }
}