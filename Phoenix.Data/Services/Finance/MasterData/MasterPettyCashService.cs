﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Models;
using Phoenix.Data.Models.Finance.MasterData;

namespace Phoenix.Data.Services.Finance.MasterData
{
    public interface IMasterPettyCashService : IDataService<MasterPettyCash>
    {
        Task<List<MasterPettyCash>> GetReffList();
        Task<MasterPettyCash> GetAffiliationFromBussinesUnit(string bussinessUnit);
        //Task<dynamic> GetCOAByNameLv5();
        Task<bool> IsExistCoaIdMasterPetty(string coaid);
    }

    public class MasterPettyCashService : IMasterPettyCashService
    {
        readonly DataContext context;

        /// <summary>
        /// And endpoint to manage MasterPettyCash
        /// </summary>
        /// <param name="context">Database context</param>
        public MasterPettyCashService(DataContext context)
        {
            this.context = context;
        }

        public async Task<bool> IsExistCoaIdMasterPetty(string coaid)
        {
            var isexist = await context.MasterPettyCashs.Where(p => !(p.IsDeleted ?? false) && p.ChartAccountId == coaid).FirstOrDefaultAsync();
            if (isexist != null)
                return true;
            else
                return false;
        }

        //public async Task<dynamic> GetCOAByNameLv5()
        //{
        //    var mspt = await context.MasterPettyCashs.Where(p => !(p.IsDeleted ?? false)).Select(x => new { x.ChartAccountId }).ToListAsync();
        //    var cha = await context.ChartOfAccounts.Where(coa => !(coa.IsDeleted ?? false) && coa.Level.Equals("5") && !mspt.Any(m => m.ChartAccountId == coa.Id)).OrderBy(x => x.CodeRec).Select(x => new { Text = x.Name5, Value = x.Id }).ToListAsync();
        //    return cha;
        //}

        public async Task<List<MasterPettyCash>> GetReffList()
        {
            var listPettyCash = await (
                from a in context.MasterPettyCashs
                //join b in context.Banks on a.BankId equals b.Id
                join c in context.Currencies on a.CurrencyId equals c.Id
                join d in context.ChartOfAccounts on new { Id = a.ChartAccountId, Level = "5" } equals new { d.Id, d.Level }
                join e in context.LegalEntity on a.LegalEntityId equals e.Id
                where !(a.IsDeleted ?? false) && !a.IsSettle
                select new MasterPettyCash
                {
                    Id = a.Id,
                    BankId = a.BankId,
                    BankAccountName = a.BankAccountName,
                    BankAccountNumber = a.BankAccountNumber,
                    CurrencyId = a.CurrencyId,
                    CurrencyCode = c.CurrencyCode,
                    ChartAccountId = a.ChartAccountId,
                    ChartAccountName = d.Name5,
                    ChartAccountNumber = d.CodeRec,
                    LegalEntityId = a.LegalEntityId,
                    LegalEntity = e.LegalEntityName,
                    CreatedOn = a.CreatedOn
                }).ToListAsync();

            //return await context.MasterPettyCashs.AsNoTracking().Where(x => !(x.IsDeleted ?? false)).ToListAsync();
            return listPettyCash;
        }

        public async Task<MasterPettyCash> GetAffiliationFromBussinesUnit(string bussUnit)
        {
            var listPettyCash = await (
                from a in context.BusinessUnits
                join b in context.Affiliations on a.AffiliationId equals b.Id
                where !(a.IsDeleted ?? false) && a.Id == bussUnit
                select new MasterPettyCash
                {
                    BussinessUnitId = a.Id,
                    BussinessUnit = a.UnitName,
                    AffiliationId = b.Id,
                    Affiliation = b.AffiliationName
                }).FirstOrDefaultAsync();

            //return await context.MasterPettyCashs.AsNoTracking().Where(x => !(x.IsDeleted ?? false)).ToListAsync();
            return listPettyCash;
        }

        public async Task<int> AddAsync(MasterPettyCash entity)
        {
            string ID = await TransID.GetTransId(context, Code.MasterPettyCash, DateTime.Today);
            //entity.Id = Guid.NewGuid().ToString();
            entity.Id = ID;
            await context.PhoenixAddAsync(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> AddRangeAsync(params MasterPettyCash[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteAsync(MasterPettyCash entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteRageAsync(params MasterPettyCash[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> DeleteSoftAsync(MasterPettyCash entity)
        {
            context.PhoenixDelete(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> DeleteSoftRangeAsync(params MasterPettyCash[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> EditAsync(MasterPettyCash entity)
        {
            context.PhoenixEdit(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> EditRangeAsync(MasterPettyCash entity)
        {
            throw new NotImplementedException();
        }

        public async Task<List<MasterPettyCash>> Get() {

            var listPettyCash = await (
                from a in context.MasterPettyCashs
                join c in context.Currencies on a.CurrencyId equals c.Id
                join d in context.ChartOfAccounts on new { Id = a.ChartAccountId, Level = "5" } equals new { d.Id, d.Level }
                join e in context.LegalEntity on a.LegalEntityId equals e.Id
                where !(a.IsDeleted ?? false)
                select new MasterPettyCash
                {
                    Id = a.Id,
                    BankAccountName = a.BankAccountName,
                    BankAccountNumber = a.BankAccountNumber,
                    BeginingBalance = a.BeginingBalance,
                    CurrencyId = a.CurrencyId,
                    CurrencyCode = c.CurrencyCode,
                    ChartAccountId = a.ChartAccountId,
                    ChartAccountName = d.Name5,
                    ChartAccountNumber = d.CodeRec,
                    LegalEntityId = a.LegalEntityId,
                    LegalEntity = e.LegalEntityName,
                    CreatedOn = a.CreatedOn
                }).ToListAsync();
            
            return listPettyCash;
        } 

        public Task<List<MasterPettyCash>> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public async Task<MasterPettyCash> Get(string Id) => await context.MasterPettyCashs.AsNoTracking().Where(x => !(x.IsDeleted ?? false) && x.Id == Id).FirstOrDefaultAsync();
    }
}