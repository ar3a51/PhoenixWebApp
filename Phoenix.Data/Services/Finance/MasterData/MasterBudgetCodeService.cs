using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Models;
using Phoenix.Data.RestApi;
using Phoenix.Shared.Core.Contexts;

namespace Phoenix.Data
{
    public interface IMasterBudgetCodeService : IDataService<MasterBudgetCode>
    {
    }

    public class MasterBudgetCodeService : IMasterBudgetCodeService
    {
        readonly DataContext context;

        /// <summary>
        /// And endpoint to manage MasterBudgetCode
        /// </summary>
        /// <param name="context">Database context</param>
        public MasterBudgetCodeService(DataContext context)
        {
            this.context = context;
        }

        public async Task<int> AddAsync(MasterBudgetCode entity)
        {
            entity.Id = Guid.NewGuid().ToString();
            await context.PhoenixAddAsync(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> AddRangeAsync(params MasterBudgetCode[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteAsync(MasterBudgetCode entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteRageAsync(params MasterBudgetCode[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> DeleteSoftAsync(MasterBudgetCode entity)
        {
            context.PhoenixDelete(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> DeleteSoftRangeAsync(params MasterBudgetCode[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> EditAsync(MasterBudgetCode entity)
        {
            context.PhoenixEdit(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> EditRangeAsync(MasterBudgetCode entity)
        {
            throw new NotImplementedException();
        }

        public async Task<List<MasterBudgetCode>> Get() => await context.MasterBudgetCodes.AsNoTracking().Where(x => !(x.IsDeleted ?? false)).ToListAsync();

        public Task<List<MasterBudgetCode>> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public async Task<MasterBudgetCode> Get(string Id)
        {
            var result = await context.MasterBudgetCodes.AsNoTracking().Where(x => !(x.IsDeleted ?? false) && x.Id == Id).FirstOrDefaultAsync();
            if (result != null)
            {
                result.CodeRec = await context.ChartOfAccounts.Where(x => x.Id == result.CoaId).Select(x => x.CodeRec).FirstOrDefaultAsync();
            }
            return result;
        }
    }
}