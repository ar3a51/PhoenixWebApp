using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Models;
using Phoenix.Data.RestApi;
using Phoenix.Shared.Core.Contexts;

namespace Phoenix.Data
{
    public interface IMapCoaTransaksiService
    {
        Task<int> AddAsync(List<MapCoaTransaksi> entity);
        Task<int> DeleteSoftAsync(string mainServiceCategoryId, string shareServiceId);
        Task<dynamic> GetMasterClosing();
        Task<List<MapCoaTransaksi>> GetMasterClosingAP(string mainServiceCategoryId, string shareServiceId);
        Task<List<MapCoaTransaksi>> GetMasterClosingAR(string mainServiceCategoryId, string shareServiceId);
        Task<dynamic> GetMasterAP();
        Task<dynamic> GetMasterAPById(string mainServiceCategoryId, string shareServiceId);
        Task<List<MapCoaTransaksi>> GetMasterDetailAP(string mainServiceCategoryId, string shareServiceId);
        Task<dynamic> GetMasterAR();
        Task<dynamic> GetMasterARById(string mainServiceCategoryId, string shareServiceId);
        Task<List<MapCoaTransaksi>> GetMasterDetailAR(string mainServiceCategoryId, string shareServiceId);
    }

    public class MapCoaTransaksiService : IMapCoaTransaksiService
    {
        readonly DataContext context;

        /// <summary>
        /// And endpoint to manage MapCoaTransaksi
        /// </summary>
        /// <param name="context">Database context</param>
        public MapCoaTransaksiService(DataContext context)
        {
            this.context = context;
        }

        public async Task<int> AddAsync(List<MapCoaTransaksi> entity)
        {
            foreach (var item in entity)
            {
                var isEdit = context.MapCoaTransaksi.AsNoTracking().Where(x => x.MainserviceCategoryId == item.MainserviceCategoryId && x.SharedserviceId == item.SharedserviceId).Count() > 0;
                if (isEdit)
                {
                    context.PhoenixEdit(item);
                }
                else
                {
                    item.Id = Guid.NewGuid().ToString();
                    await context.PhoenixAddAsync(item);
                }
            }
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteSoftAsync(string mainServiceCategoryId, string shareServiceId)
        {
            var result = await context.MapCoaTransaksi.AsNoTracking().Where(x => x.MainserviceCategoryId == mainServiceCategoryId && x.SharedserviceId == shareServiceId).ToListAsync();
            context.PhoenixDeleteRange(result.ToArray());
            return await context.SaveChangesAsync();
        }

        public async Task<dynamic> GetMasterClosing()
        {
            var result = await (from a in context.MapCoaTransaksi
                                join b in context.MainServiceCategorys on a.MainserviceCategoryId equals b.Id
                                join c in context.ShareServicess on a.SharedserviceId equals c.Id
                                where !(a.IsDeleted ?? false) && a.ModuleCode.Contains("CLOSING")
                                group a by new { a.MainserviceCategoryId, b.Name, a.SharedserviceId, c.ShareserviceName } into x
                                select new
                                {
                                    x.Key.MainserviceCategoryId,
                                    MainserviceCategoryName = x.Key.Name,
                                    x.Key.SharedserviceId,
                                    SharedserviceName = x.Key.ShareserviceName
                                }).ToListAsync();
            return result;
        }

        public async Task<List<MapCoaTransaksi>> GetMasterClosingAP(string mainServiceCategoryId, string shareServiceId)
        {
            var result = await (from a in context.MapCoaTransaksi
                                join b in context.ChartOfAccounts on a.CoaId equals b.Id
                                where !(a.IsDeleted ?? false) &&
                                         a.MainserviceCategoryId == mainServiceCategoryId &&
                                         a.SharedserviceId == shareServiceId &&
                                         a.ModuleCode.Contains("CLOSING AP") && b.Level == "5"
                                select new MapCoaTransaksi
                                {
                                    Id = a.Id,
                                    ModuleCode = a.ModuleCode,
                                    MainserviceCategoryId = a.MainserviceCategoryId,
                                    SharedserviceId = a.SharedserviceId,
                                    LineCode = a.LineCode,
                                    Description = a.Description,
                                    CoaId = a.CoaId,
                                    DK = a.DK,
                                    AccountName = b.Name5,
                                    CodeRec = b.CodeRec,
                                    CreatedBy = a.CreatedBy,
                                    CreatedOn = a.CreatedOn
                                }).ToListAsync();

            if (result.Count == 0)
            {
                result = new List<MapCoaTransaksi>()
                {
                    new MapCoaTransaksi(){Id=Guid.NewGuid().ToString(), ModuleCode= "CLOSING AP",LineCode="WIP", Description="Work in Progress" },
                    new MapCoaTransaksi(){Id=Guid.NewGuid().ToString(), ModuleCode= "CLOSING AP",LineCode="COB", Description="Cost of Billing" }
                };
            }
            return result;
        }

        public async Task<List<MapCoaTransaksi>> GetMasterClosingAR(string mainServiceCategoryId, string shareServiceId)
        {
            var result = await (from a in context.MapCoaTransaksi
                                join b in context.ChartOfAccounts on a.CoaId equals b.Id
                                where !(a.IsDeleted ?? false) &&
                                         a.MainserviceCategoryId == mainServiceCategoryId &&
                                         a.SharedserviceId == shareServiceId &&
                                         a.ModuleCode.Contains("CLOSING AR") && b.Level == "5"
                                select new MapCoaTransaksi
                                {
                                    Id = a.Id,
                                    ModuleCode = a.ModuleCode,
                                    MainserviceCategoryId = a.MainserviceCategoryId,
                                    SharedserviceId = a.SharedserviceId,
                                    LineCode = a.LineCode,
                                    Description = a.Description,
                                    CoaId = a.CoaId,
                                    DK = a.DK,
                                    AccountName = b.Name5,
                                    CodeRec = b.CodeRec,
                                    CreatedBy = a.CreatedBy,
                                    CreatedOn = a.CreatedOn
                                }).ToListAsync();

            if (result.Count == 0)
            {
                result = new List<MapCoaTransaksi>()
                {
                    new MapCoaTransaksi(){Id=Guid.NewGuid().ToString(), ModuleCode= "CLOSING AR",LineCode="PRE BILLING", Description="PRE BILLING" },
                    new MapCoaTransaksi(){Id=Guid.NewGuid().ToString(), ModuleCode= "CLOSING AR",LineCode="BILLING", Description="BILLING" }
                };
            }
            return result;
        }

        public async Task<dynamic> GetMasterAP()
        {
            var shareService = await context.MainServiceCategorys.ToListAsync();
            var result = (await (from a in context.MapCoaTransaksi
                                 join b in context.MainServiceCategorys on a.MainserviceCategoryId equals b.Id
                                 where !(a.IsDeleted ?? false) && a.ModuleCode == "AP"
                                 group a by new { a.MainserviceCategoryId, b.Name, a.SharedserviceId, a.NameSetting, a.PceTypeCrossBilling } into x
                                 select new
                                 {
                                     x.Key.MainserviceCategoryId,
                                     MainserviceCategoryName = x.Key.Name,
                                     x.Key.SharedserviceId,
                                     x.Key.NameSetting,
                                     x.Key.PceTypeCrossBilling
                                 }).ToListAsync())
                          .Select(x => new
                          {
                              x.MainserviceCategoryId,
                              x.MainserviceCategoryName,
                              x.SharedserviceId,
                              SharedserviceName = shareService.Where(y => y.Id == x.SharedserviceId).Select(y => y.Name).FirstOrDefault(),
                              x.NameSetting,
                              x.PceTypeCrossBilling,
                          }).ToList();
            return result;
        }

        public async Task<dynamic> GetMasterAPById(string mainServiceCategoryId, string shareServiceId)
        {
            if (shareServiceId == "null") shareServiceId = null;
            var result = await (from a in context.MapCoaTransaksi
                                where !(a.IsDeleted ?? false) && a.ModuleCode == "AP" &&
                                a.MainserviceCategoryId == mainServiceCategoryId && a.SharedserviceId == shareServiceId
                                group a by new { a.MainserviceCategoryId, a.SharedserviceId, a.NameSetting, a.PceTypeCrossBilling } into x
                                select new
                                {
                                    x.Key.MainserviceCategoryId,
                                    x.Key.SharedserviceId,
                                    x.Key.NameSetting,
                                    x.Key.PceTypeCrossBilling
                                }).FirstOrDefaultAsync();
            return result;
        }

        public async Task<List<MapCoaTransaksi>> GetMasterDetailAP(string mainServiceCategoryId, string shareServiceId)
        {
            var result = await (from a in context.MapCoaTransaksi
                                join b in context.ChartOfAccounts on a.CoaId equals b.Id
                                where !(a.IsDeleted ?? false) &&
                                         a.MainserviceCategoryId == mainServiceCategoryId &&
                                         a.SharedserviceId == shareServiceId &&
                                         a.ModuleCode == "AP" && b.Level == "5"
                                select new MapCoaTransaksi
                                {
                                    Id = a.Id,
                                    ModuleCode = a.ModuleCode,
                                    MainserviceCategoryId = a.MainserviceCategoryId,
                                    SharedserviceId = a.SharedserviceId,
                                    LineCode = a.LineCode,
                                    Description = a.Description,
                                    CoaId = a.CoaId,
                                    DK = a.DK,
                                    IsAsf = a.IsAsf,
                                    AccountName = b.Name5,
                                    CodeRec = b.CodeRec,
                                    CreatedBy = a.CreatedBy,
                                    CreatedOn = a.CreatedOn
                                }).ToListAsync();

            if (result.Count == 0)
            {
                if (mainServiceCategoryId == MainServiceCategoryId.Production ||
                    mainServiceCategoryId == MainServiceCategoryId.Media)
                {
                    result = new List<MapCoaTransaksi>()
                    {
                        new MapCoaTransaksi(){Id=Guid.NewGuid().ToString(), ModuleCode= "AP",LineCode="VAT", Description="Tax Payable - VAT" },
                        new MapCoaTransaksi(){Id=Guid.NewGuid().ToString(), ModuleCode= "AP",LineCode="WIP", Description= mainServiceCategoryId == MainServiceCategoryId.Production? "Work in Progress - Production" : "Work in Progress - Media" },
                        new MapCoaTransaksi(){Id=Guid.NewGuid().ToString(), ModuleCode= "AP",LineCode="AP", Description=mainServiceCategoryId == MainServiceCategoryId.Production? "Account Payable Trade" : "Account Payable - Media" }
                    };
                }
                else if (mainServiceCategoryId == MainServiceCategoryId.Others)
                {
                    result = new List<MapCoaTransaksi>()
                    {
                        new MapCoaTransaksi(){Id=Guid.NewGuid().ToString(), ModuleCode= "AP",LineCode="VAT", Description="Tax Payable - VAT" },
                        new MapCoaTransaksi(){Id=Guid.NewGuid().ToString(), ModuleCode= "AP",LineCode="AP", Description="Account Payable - Others" }
                    };
                }
            }
            return result;
        }

        public async Task<dynamic> GetMasterAR()
        {
            var shareService = await context.MainServiceCategorys.ToListAsync();
            var result = (await (from a in context.MapCoaTransaksi
                                 join b in context.MainServiceCategorys on a.MainserviceCategoryId equals b.Id
                                 where !(a.IsDeleted ?? false) && a.ModuleCode == "AR"
                                 group a by new { a.MainserviceCategoryId, b.Name, a.SharedserviceId, a.NameSetting, a.PceTypeCrossBilling, a.InvoiceType } into x
                                 select new
                                 {
                                     x.Key.MainserviceCategoryId,
                                     MainserviceCategoryName = x.Key.Name,
                                     x.Key.SharedserviceId,
                                     x.Key.NameSetting,
                                     x.Key.PceTypeCrossBilling,
                                     x.Key.InvoiceType
                                 }).ToListAsync())
                          .Select(x => new
                          {
                              x.MainserviceCategoryId,
                              x.MainserviceCategoryName,
                              x.SharedserviceId,
                              SharedserviceName = shareService.Where(y => y.Id == x.SharedserviceId).Select(y => y.Name).FirstOrDefault(),
                              x.NameSetting,
                              x.PceTypeCrossBilling,
                              x.InvoiceType
                          }).ToList(); ;

            return result;
        }

        public async Task<dynamic> GetMasterARById(string mainServiceCategoryId, string shareServiceId)
        {
            if (shareServiceId == "null") shareServiceId = null;
            var result = await (from a in context.MapCoaTransaksi
                                where !(a.IsDeleted ?? false) && a.ModuleCode == "AR" &&
                                a.MainserviceCategoryId == mainServiceCategoryId && a.SharedserviceId == shareServiceId
                                group a by new { a.MainserviceCategoryId, a.SharedserviceId, a.NameSetting, a.PceTypeCrossBilling, a.InvoiceType } into x
                                select new
                                {
                                    x.Key.MainserviceCategoryId,
                                    x.Key.SharedserviceId,
                                    x.Key.NameSetting,
                                    x.Key.PceTypeCrossBilling,
                                    x.Key.InvoiceType
                                }).FirstOrDefaultAsync();
            return result;
        }

        public async Task<List<MapCoaTransaksi>> GetMasterDetailAR(string mainServiceCategoryId, string shareServiceId)
        {
            var result = await (from a in context.MapCoaTransaksi
                                join b in context.ChartOfAccounts on a.CoaId equals b.Id
                                where !(a.IsDeleted ?? false) &&
                                         a.MainserviceCategoryId == mainServiceCategoryId &&
                                         a.SharedserviceId == shareServiceId &&
                                         a.ModuleCode == "AR" && b.Level == "5"
                                select new MapCoaTransaksi
                                {
                                    Id = a.Id,
                                    ModuleCode = a.ModuleCode,
                                    MainserviceCategoryId = a.MainserviceCategoryId,
                                    SharedserviceId = a.SharedserviceId,
                                    LineCode = a.LineCode,
                                    Description = a.Description,
                                    CoaId = a.CoaId,
                                    DK = a.DK,
                                    IsAsf = a.IsAsf,
                                    AccountName = b.Name5,
                                    CodeRec = b.CodeRec,
                                    CreatedBy = a.CreatedBy,
                                    CreatedOn = a.CreatedOn
                                }).ToListAsync();

            if (result.Count == 0)
            {
                if (mainServiceCategoryId == MainServiceCategoryId.Production ||
                    mainServiceCategoryId == MainServiceCategoryId.Others)
                {
                    result = new List<MapCoaTransaksi>()
                    {
                        new MapCoaTransaksi(){Id=Guid.NewGuid().ToString(), ModuleCode= "AR",LineCode="BILLING", Description= mainServiceCategoryId == MainServiceCategoryId.Production?"Client Prebilling Production":"Prepaid Others" },
                        new MapCoaTransaksi(){Id=Guid.NewGuid().ToString(), ModuleCode= "AR",LineCode="VAT", Description="Tax Payable VAT" },
                        new MapCoaTransaksi(){Id=Guid.NewGuid().ToString(), ModuleCode= "AR",LineCode="AR", Description="Account Receivable Trade" }
                    };
                }
                else if (mainServiceCategoryId == MainServiceCategoryId.Media)
                {
                    result = new List<MapCoaTransaksi>()
                    {
                        new MapCoaTransaksi(){Id=Guid.NewGuid().ToString(), ModuleCode= "AR",LineCode="COB", Description="COB Media TV" },
                        new MapCoaTransaksi(){Id=Guid.NewGuid().ToString(), ModuleCode= "AR",LineCode="BILLING", Description="Media Billing TV" },
                        new MapCoaTransaksi(){Id=Guid.NewGuid().ToString(), ModuleCode= "AR",LineCode="VAT", Description="Tax Payable VAT" },
                        new MapCoaTransaksi(){Id=Guid.NewGuid().ToString(), ModuleCode= "AR",LineCode="AR", Description="Account Receivable Trade" },
                        new MapCoaTransaksi(){Id=Guid.NewGuid().ToString(), ModuleCode= "AR",LineCode="WIP", Description="Work in Progress Media" }
                    };
                }
            }
            return result;
        }
    }
}