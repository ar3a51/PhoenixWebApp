﻿using Phoenix.Data.Models.Finance.Report;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Data.Services.Finance.Report
{
    public interface ITImeSheetReportServices
    {
        Task<List<TimeSheetByJob>> TimeSheetByJob(string job_numr);
        Task<List<ReportTimeSheetClient>> GetReportClient(string client_id);
    }
    public class TImeSheetReportServices : ITImeSheetReportServices
    {
        readonly DataContext context;
        public TImeSheetReportServices(DataContext context)
        {
            this.context = context;
        }

        public async Task<List<ReportTimeSheetClient>> GetReportClient(string client_id)
        {
            try
            {
                List<ReportTimeSheetClient> list = new List<ReportTimeSheetClient>();
                var param = new SqlParameter[]
                        {
                        new SqlParameter("@p_client_id",client_id),

                        };
                list = await context.ExecuteStoredProcedure<ReportTimeSheetClient>("pm.report_timesheet_client", param);
                return list;
            }
            catch (Exception e)
            {
                throw new Exception("Error !, " + e.Message);
            }
        }

        public async Task<List<TimeSheetByJob>> TimeSheetByJob(string job_numr)
        {
            try
            {
                List<TimeSheetByJob> list = new List<TimeSheetByJob>();
                var param = new SqlParameter[]
                        {
                        new SqlParameter("@p_job_id",job_numr),

                        };
                list = await context.ExecuteStoredProcedure<TimeSheetByJob>("pm.report_timesheet_job", param);
                return list;
            }
            catch (Exception e)
            {
                throw new Exception("Error !, " + e.Message);
            }
            

        }
    }
}
