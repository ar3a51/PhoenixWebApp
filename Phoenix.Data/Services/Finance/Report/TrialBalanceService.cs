﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Models;
using Phoenix.Data.RestApi;
using Phoenix.Shared.Core.Contexts;

namespace Phoenix.Data
{
    public interface ITrialBalanceService
    {
        Task<List<TrialBalance>> Get(TrialBalanceFilter Filter);
        Task<FinancialYear> GetFiscalYears(string Id);
    }
    public class TrialBalanceService : ITrialBalanceService
    {
        readonly DataContext context;
        public TrialBalanceService(DataContext context)
        {
            this.context = context;
        }
        public async Task<List<TrialBalance>> Get(TrialBalanceFilter Filter)
        {
            List<TrialBalance> list = new List<TrialBalance>();
            try
            {
                if (!string.IsNullOrEmpty(Filter.LegalEntityId))
                {
                    var legal = "";
                    DateTime startDate = new DateTime();
                    DateTime endDate = new DateTime();
                    FinancialYear finYears = new FinancialYear();
                    FinancePeriod finPeriod = new FinancePeriod();

                    if (string.IsNullOrEmpty(Filter.StartPeriodId) && string.IsNullOrEmpty(Filter.EndPeriodId))
                    {
                        finYears = await context.FinancialYears.Where(x => !(x.IsDeleted ?? false) && x.Id == Filter.FiscalYearsId).FirstOrDefaultAsync();
                        startDate = finYears.StartDate;
                        endDate = finYears.EndDate;
                        legal = finYears.LegalEntityId;
                    }

                    if (!string.IsNullOrEmpty(Filter.StartPeriodId) && !string.IsNullOrEmpty(Filter.EndPeriodId))
                    {
                        //finPeriod = await context.FinancePeriods.Where(x => !(x.IsDeleted ?? false) && x.Id == Filter.FiscalYearsId).FirstOrDefaultAsync();
                        legal = Filter.LegalEntityId;
                        startDate = Convert.ToDateTime(Filter.StartPeriodId);
                        endDate = Convert.ToDateTime(Filter.EndPeriodId);
                        if (endDate < startDate)
                            throw new Exception("End date must be greather than start date !");

                    }

                    var param = new SqlParameter[]
                    {
                        new SqlParameter("@p_legal_entity",legal),
                         new SqlParameter("@p_business_unit_id",(object)Filter.BusinessUnitId ?? DBNull.Value),
                          new SqlParameter("@p_from_date",DateTime.ParseExact(startDate.ToString("dd/MM/yyyy"), "dd/MM/yyyy", CultureInfo.InvariantCulture)),
                          new SqlParameter("@p_to_date",DateTime.ParseExact(endDate.ToString("dd/MM/yyyy"), "dd/MM/yyyy", CultureInfo.InvariantCulture))
                    };

                    if (!string.IsNullOrEmpty(legal))
                    {
                        list = await context.ExecuteStoredProcedure<TrialBalance>("fn.report_trial_balance_lv5", param);

                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error !, " + e.Message);
            }
            return list;
        }

        public async Task<FinancialYear> GetFiscalYears(string Id) => await context.FinancialYears.Where(x => !(x.IsDeleted ?? false) && x.Id == Id).FirstOrDefaultAsync();
    }
}
