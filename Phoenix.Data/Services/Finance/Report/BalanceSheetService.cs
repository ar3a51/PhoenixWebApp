﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Models;
using Phoenix.Data.RestApi;
using Phoenix.Shared.Core.Contexts;

namespace Phoenix.Data
{
    public interface IBalanceSheetService
    {
        Task<List<BalanceSheet>> Get(TrialBalanceFilter Filter);
        Task<FinancialYear> GetFiscalYears(string Id);
    }
    public class BalanceSheetService : IBalanceSheetService
    {
        readonly DataContext context;
        public BalanceSheetService(DataContext context)
        {
            this.context = context;
        }

        public async Task<List<BalanceSheet>> Get(TrialBalanceFilter Filter)
        {
            List<BalanceSheet> list = new List<BalanceSheet>();
            try
            {
                if (!string.IsNullOrEmpty(Filter.StartPeriodId) && !string.IsNullOrEmpty(Filter.EndPeriodId))
                {
                    DateTime startDate = Convert.ToDateTime(Filter.StartPeriodId);
                    DateTime endDate = Convert.ToDateTime(Filter.EndPeriodId);
                    var param = new SqlParameter[]
                    {
                        new SqlParameter("@p_legal_entity",DBNull.Value),
                         new SqlParameter("@p_business_unit_id", DBNull.Value),
                          new SqlParameter("@p_from_date",DateTime.ParseExact(startDate.ToString("dd/MM/yyyy"), "dd/MM/yyyy", CultureInfo.InvariantCulture)),
                          new SqlParameter("@p_to_date",DateTime.ParseExact(endDate.ToString("dd/MM/yyyy"), "dd/MM/yyyy", CultureInfo.InvariantCulture))
                    };
                    list = await context.ExecuteStoredProcedure<BalanceSheet>("fn.report_balance_sheet_LV5", param);
                    //if (!string.IsNullOrEmpty(legal))
                    //{
                    //    list = await context.ExecuteStoredProcedure<BalanceSheet>("fn.report_balance_sheet_LV5", param);
                    //}
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error !, " + e.Message);
            }
            return list;
        }
        public async Task<FinancialYear> GetFiscalYears(string Id) => await context.FinancialYears.Where(x => !(x.IsDeleted ?? false) && x.Id == Id).FirstOrDefaultAsync();
    }
}
