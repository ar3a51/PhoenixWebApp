﻿using Phoenix.Data.Models;
using Phoenix.Data.Models.Um;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Phoenix.Data.Services.Um
{
    public interface IManageUserAppService : IDataService<TmUserApp>
    {
        Task<BusinessUnitJobLevel> GetBusinessUnitJobLevel(string empId);
        Task<List<TmUserApp>> GetListUserByGroupId(string groupId);
        Task<TmUserApp> CekIdIsReady(string Id);
        Task<List<TmUserApp>> GetBySearch(string key);
        Task<List<EmployeeBasicInfo>> GetDataEmployee(string key);
        Task<List<TmUserApp>> GetByUserLogin();
    }
}
