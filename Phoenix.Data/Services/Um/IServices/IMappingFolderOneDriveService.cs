﻿using Phoenix.Data.Models.Um;
using Phoenix.Data.Models.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Phoenix.Data.Services.Um
{
    public interface IMappingFolderOneDriveService : IDataService<TrMappingFolderOneDrive>
    {
        Task<int> DeleteAllMappingByItemId(string itemId);
        Task<int> DeleteByItemIdAndRefId(string itemId, string refId);
        Task<List<VmMapFodAccess>> GetByItemId(string itemid);
        Task<List<VmFodUserList>> GetUserList(string itemid, string key);
        Task<List<VmFodGroupList>> GetGroupList(string itemid, string key);
    }

}
