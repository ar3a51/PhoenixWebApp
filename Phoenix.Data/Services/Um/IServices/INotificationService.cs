﻿using Phoenix.Data.Models;
using Phoenix.Data.Models.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Phoenix.Data.Services.Um
{
    public interface INotificationService : IDataService<Notification>
    {
        Task<List<NotificationVm>> GetListNotificationByUserCode(string userCode);
    }
    public interface INotificationTypeService : IDataService<NotificationType>
    { }
}
