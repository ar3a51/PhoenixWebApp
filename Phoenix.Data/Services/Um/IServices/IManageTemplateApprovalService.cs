﻿using Phoenix.Data.Models;
using Phoenix.Data.Models.Um;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Phoenix.Data.Services.Um
{
    public interface IManageTemplateApprovalService : IDataService<TmTemplateApproval>
    {
        Task<List<TmTemplateApproval>> GetBySubGroup(string groupId, string divisionId);
        Task<List<TmTemplateApproval>> GetByCategory(int typeId);
        Task<List<JobTitle>> GetJobTitlesByDivisionId(string id);
        Task<List<BusinessUnit>> GetDivision();
        Task<BusinessUnitJobLevel> GetBussinessUnitJobLevel(string jobTitleId, string divisionId);
        Task<TmTemplateApproval> GetByMenuId(string groupId, string divisionId, string menuId, string jobTitleId);
        Task<TmTemplateApproval> GetById(string Id);
        Task<TmTemplateApproval> GetByBudgeting(string groupId, string divisionId, string menuId, decimal? budget);
    }
}
