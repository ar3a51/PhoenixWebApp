﻿using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Models.Um;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.Data.Services.Um
{
    public class ManageMenuService : IManageMenuService
    {
        readonly DataContext context;

        public string UserId => context.GetUserId();

        /// <summary>
        /// And endpoint to manage SalaryComponent
        /// </summary>
        /// <param name="context">Database context</param>
        public ManageMenuService(DataContext context)
        {
            this.context = context;
        }

        public async Task<int> AddAsync(TmMenu entity)
        {
            entity.Id = Guid.NewGuid().ToString();
            await context.PhoenixAddAsync(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> AddRangeAsync(params TmMenu[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteAsync(TmMenu entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteRageAsync(params TmMenu[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> DeleteSoftAsync(TmMenu entity)
        {
            context.PhoenixDelete(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> DeleteSoftRangeAsync(params TmMenu[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> EditAsync(TmMenu entity)
        {
            context.PhoenixEdit(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> EditRangeAsync(TmMenu entity)
        {
            throw new NotImplementedException();
        }

        public async Task<List<TmMenu>> GetBySearch(string key)
        {
            var parentList = (from x in context.TmMenus select new { x.ParentId, x.MenuName, x.Id }).ToList();
            var data = await (from m in context.TmMenus
                              select new TmMenu
                              {
                                  Id = m.Id,
                                  MenuName = m.MenuName,
                                  MenuLink = m.MenuLink,
                                  MenuUnique = m.MenuUnique,
                                  ParentId = m.ParentId,
                                  IsDeleted = m.IsDeleted
                              }).Where(x => Convert.ToBoolean(x.IsDeleted) == false && x.MenuName.Contains(key)).ToListAsync();
            if (parentList.Count == 0) return data;

            foreach (var item in data.ToList())
            {
                if (item.ParentId == "0") item.ParentName = "ROOT";
                else
                {
                    if (!string.IsNullOrEmpty(item.ParentId))
                    {
                        var parentName = parentList.Where(x => x.Id.Equals(item.ParentId)).FirstOrDefault();
                        if (parentName != null)
                            item.ParentName = parentName.MenuName;
                        else item.ParentName = "NOT FOUND PARENT";
                    }
                }
            }
            return data;
        }

        public async Task<List<TmMenu>> GetRaw()
        {
            return await context.TmMenus.AsNoTracking().Where(x => !(x.IsDeleted ?? false)).ToListAsync();
        }

        public async Task<TmMenu> GetByUniqueName(string uniqueName)
        {
            return await context.TmMenus.AsNoTracking().Where(x => !(x.IsDeleted ?? false) && x.MenuUnique.Equals(uniqueName)).FirstOrDefaultAsync();
        }

        public async Task<List<TmMenu>> Get()
        {
            var list = await context.TmUserAccesses.AsNoTracking().Include(x => x.TmMenu)
                .Where(x => !(x.IsDeleted ?? false) && !(x.TmMenu.IsDeleted ?? false) && x.UserId.Equals(UserId))
                .Select(x => x.TmMenu)
                .ToListAsync();

            list.ForEach(i => i.Children = list.Where(ch => ch.ParentId == i.Id).Distinct().ToList());
            list = list.Where(x => x.ParentId == "0").Distinct().ToList();
            return list;
            //return new HashSet<TmMenu>(list);
        }

        public Task<List<TmMenu>> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public async Task<TmMenu> Get(string id)
        {
            return await context.TmMenus.AsNoTracking().Where(x => !(x.IsDeleted ?? false) && x.Id == id).FirstOrDefaultAsync();
        }
    }
}
