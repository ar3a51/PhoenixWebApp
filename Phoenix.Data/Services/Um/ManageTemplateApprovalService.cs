﻿using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Models;
using Phoenix.Data.Models.Um;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.Data.Services.Um
{
    public class ManageTemplateApprovalService : IManageTemplateApprovalService
    {
        readonly DataContext context;

        public string UserId => context.GetEmployeeId();

        /// <summary>
        /// And endpoint to manage SalaryComponent
        /// </summary>
        /// <param name="context">Database context</param>
        public ManageTemplateApprovalService(DataContext context)
        {
            this.context = context;
        }

        public async Task<int> AddAsync(TmTemplateApproval entity)
        {
            try
            {
                if (string.IsNullOrEmpty(entity.JobTitleId)) entity.JobTitleId = "0";
                await context.PhoenixAddApprovalAsync(entity);
                return await context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException.Message);
                return await Task.Run(() => 0);
            }
        }

        public Task<int> AddRangeAsync(params TmTemplateApproval[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteAsync(TmTemplateApproval entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteRageAsync(params TmTemplateApproval[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> DeleteSoftAsync(TmTemplateApproval entity)
        {
            context.PhoenixDelete(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> DeleteSoftRangeAsync(params TmTemplateApproval[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> EditAsync(TmTemplateApproval entity)
        {
            context.PhoenixEdit(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> EditRangeAsync(TmTemplateApproval entity)
        {
            throw new NotImplementedException();
        }


        public async Task<List<TmTemplateApproval>> Get()
        {
            var data = await (from t in context.TmTemplateApprovals.Where(x => x.IsDeleted == false)
                              select new TmTemplateApproval
                              {
                                  TemplateName = t.TemplateName,
                                  Id = t.Id,
                                  IsNotifyByEmail = t.IsNotifyByEmail,
                                  IsNotifyByWeb = t.IsNotifyByWeb,
                                  DueDate = t.DueDate,
                                  Reminder = t.Reminder,
                                  ApprovalCategory = t.ApprovalCategory,
                                  ApprovalType = t.ApprovalType,
                                  CreatedBy = t.CreatedBy,
                                  CreatedOn = t.CreatedOn,
                                  ListUserApprovals = null,
                                  MenuId = string.IsNullOrEmpty(t.MenuId) ? "" : t.MenuId,
                                  JobTitleId = string.IsNullOrEmpty(t.JobTitleId) ? "0" : t.JobTitleId,
                                  MinBudget = t.MinBudget,
                                  MaxBudget = t.MaxBudget
                              }).OrderByDescending(x => x.CreatedOn).ToListAsync();

            foreach (var item in data.ToList())
            {
                if (item.JobTitleId != "0") item.JobTitleName = context.JobTitles.Where(x => x.Id.Equals(item.JobTitleId)).FirstOrDefault().TitleName;
                else item.JobTitleName = "There is no Position";
                if (!string.IsNullOrEmpty(item.MenuId)) item.MenuName = context.TmMenus.Where(x => x.Id.Equals(item.MenuId)).FirstOrDefault().MenuName;
            }

            return data;
        }

        public Task<List<TmTemplateApproval>> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public async Task<TmTemplateApproval> Get(string id)
        {
            var data = await (from t in context.TmTemplateApprovals.Where(x => x.Id.Equals(id) && x.IsDeleted == false)
                              select new TmTemplateApproval
                              {
                                  TemplateName = t.TemplateName,
                                  Id = t.Id,
                                  IsNotifyByEmail = t.IsNotifyByEmail,
                                  IsNotifyByWeb = t.IsNotifyByWeb,
                                  DueDate = t.DueDate,
                                  Reminder = t.Reminder,
                                  ApprovalCategory = t.ApprovalCategory,
                                  ApprovalType = t.ApprovalType,
                                  CreatedBy = t.CreatedBy,
                                  CreatedOn = t.CreatedOn,
                                  ListUserApprovals = null,
                                  MenuId = string.IsNullOrEmpty(t.MenuId) ? "" : t.MenuId,
                                  JobTitleId = string.IsNullOrEmpty(t.JobTitleId) ? "0" : t.JobTitleId,
                                  MinBudget = t.MinBudget,
                                  MaxBudget = t.MaxBudget
                              }).OrderByDescending(x => x.CreatedOn).FirstOrDefaultAsync();
            if (data.JobTitleId != "0") data.JobTitleName = context.JobTitles.Where(x => x.Id.Equals(data.JobTitleId)).FirstOrDefault().TitleName;
            else data.JobTitleName = "There is no Position";
            if (data.MenuId != null) data.MenuName = context.TmMenus.Where(x => x.Id.Equals(data.MenuId)).FirstOrDefault().MenuName;

            if (data == null) return data;
                    data.ListUserApprovals = (from lu in context.TmTemplateUserApprovals.Where(x => x.TmTempApprovalId.Equals(data.Id) && x.IsDeleted.Equals(false))
                                              join emp in context.EmployeeBasicInfos on lu.EmployeeBasicInfoId equals emp.Id
                                              select new TmTemplateUserApproval
                                              {
                                                  Id = lu.Id,
                                                  EmployeeBasicInfoId = lu.EmployeeBasicInfoId,
                                                  EmployeeName = emp.NameEmployee,
                                                  IsCondition = lu.IsCondition,
                                                  IndexUser = lu.IndexUser,
                                                  ListConditionalApprovals = null,
                                                  IsSpecialCase = lu.IsSpecialCase,
                                                  RemarkSpecialCase = lu.RemarkSpecialCase
                                              }).OrderBy(c => c.IndexUser).ToList();

            foreach (var x in data.ListUserApprovals)
            {
                x.ListConditionalApprovals = (from ca in context.TmConditionUserApprovals.Where(c => c.TemplateUserApprovalId.Equals(x.Id) && c.IsDeleted.Equals(false))
                                              join e in context.EmployeeBasicInfos on ca.EmployeeBasicInfoId equals e.Id
                                              select new TmConditionUserApproval
                                              {
                                                  Id = ca.Id,
                                                  EmployeeBasicInfoId = ca.EmployeeBasicInfoId,
                                                  EmployeeName = e.NameEmployee,
                                                  GroupConditionIndex = ca.GroupConditionIndex,
                                                  GroupConditionApproval = ca.GroupConditionApproval,
                                                  ConditionalApproval = ca.ConditionalApproval,
                                                  TemplateUserApprovalId = ca.TemplateUserApprovalId
                                              }).OrderBy(c => c.GroupConditionIndex).ToList();
            }
            return data;
        }

        ///====Additional Function Service in Bottom line here ===///
        public async Task<List<TmTemplateApproval>> GetBySubGroup(string groupId, string divisionId)
        {
            var data = await (from t in context.TmTemplateApprovals.Where(x => x.SubGroupId.Equals(groupId) && x.DivisionId.Equals(divisionId) && x.IsDeleted == false)
                              select new TmTemplateApproval
                              {
                                  TemplateName = t.TemplateName,
                                  Id = t.Id,
                                  IsNotifyByEmail = t.IsNotifyByEmail,
                                  IsNotifyByWeb = t.IsNotifyByWeb,
                                  DueDate = t.DueDate,
                                  Reminder = t.Reminder,
                                  ApprovalCategory = t.ApprovalCategory,
                                  ApprovalType = t.ApprovalType,
                                  CreatedBy = t.CreatedBy,
                                  CreatedOn = t.CreatedOn,
                                  ListUserApprovals = null,
                                  MenuId = string.IsNullOrEmpty(t.MenuId) ? "" : t.MenuId,
                                  JobTitleId = string.IsNullOrEmpty(t.JobTitleId) ? "0" : t.JobTitleId,
                                  MinBudget = t.MinBudget,
                                  MaxBudget = t.MaxBudget
                              }).OrderByDescending(x => x.CreatedOn).ToListAsync();
            foreach (var item in data)
            {
                if (item.JobTitleId != "0") item.JobTitleName = context.JobTitles.Where(x => x.Id.Equals(item.JobTitleId)).FirstOrDefault()?.TitleName;
                else item.JobTitleName = "There is no Position";
                if (item.MenuId != null) item.MenuName = context.TmMenus.Where(x => x.Id.Equals(item.MenuId)).FirstOrDefault()?.MenuName;
            }

            if (data.Count == 0) return data;
            foreach (var item in data)
            {
                if (!string.IsNullOrEmpty(item.Id))
                    item.ListUserApprovals = (from lu in context.TmTemplateUserApprovals.Where(x => x.TmTempApprovalId.Equals(item.Id) && x.IsDeleted.Equals(false))
                                              join emp in context.EmployeeBasicInfos on lu.EmployeeBasicInfoId equals emp.Id
                                              select new TmTemplateUserApproval
                                              {
                                                  Id = lu.Id,
                                                  EmployeeBasicInfoId = lu.EmployeeBasicInfoId,
                                                  EmployeeName = emp.NameEmployee,
                                                  IsCondition = lu.IsCondition,
                                                  ListConditionalApprovals = null,
                                                  IsSpecialCase = lu.IsSpecialCase,
                                                  RemarkSpecialCase = lu.RemarkSpecialCase
                                              }).OrderBy(c => c.IndexUser).ToList();
            }

            foreach (var item in data)
            {
                if (item.ListUserApprovals.Count > 0)
                {
                    foreach (var x in item.ListUserApprovals)
                    {
                        x.ListConditionalApprovals = (from ca in context.TmConditionUserApprovals.Where(c => c.TemplateUserApprovalId.Equals(x.Id) && c.IsDeleted.Equals(false))
                                                      join e in context.EmployeeBasicInfos on ca.EmployeeBasicInfoId equals e.Id
                                                      select new TmConditionUserApproval
                                                      {
                                                          Id = ca.Id,
                                                          EmployeeBasicInfoId = ca.EmployeeBasicInfoId,
                                                          EmployeeName = e.NameEmployee,
                                                          GroupConditionIndex = ca.GroupConditionIndex,
                                                          GroupConditionApproval = ca.GroupConditionApproval,
                                                          ConditionalApproval = ca.ConditionalApproval,
                                                          TemplateUserApprovalId = ca.TemplateUserApprovalId
                                                      }).OrderBy(c => c.GroupConditionIndex).ToList();
                    }
                }
            }
            return data;
        }

        public async Task<TmTemplateApproval> GetById(string Id)
        {
            var data = await (from t in context.TmTemplateApprovals.Where(x => x.Id.Equals(Id))
                              select new TmTemplateApproval
                              {
                                  TemplateName = t.TemplateName,
                                  Id = t.Id,
                                  IsNotifyByEmail = t.IsNotifyByEmail,
                                  IsNotifyByWeb = t.IsNotifyByWeb,
                                  DueDate = t.DueDate,
                                  Reminder = t.Reminder,
                                  ApprovalCategory = t.ApprovalCategory,
                                  ApprovalType = t.ApprovalType,
                                  CreatedBy = t.CreatedBy,
                                  CreatedOn = t.CreatedOn,
                                  ListUserApprovals = null,
                                  MenuId = string.IsNullOrEmpty(t.MenuId) ? "" : t.MenuId,
                                  JobTitleId = string.IsNullOrEmpty(t.JobTitleId) ? "0" : t.JobTitleId,
                                  MinBudget = t.MinBudget,
                                  MaxBudget = t.MaxBudget
                              }).OrderByDescending(x => x.CreatedOn).FirstOrDefaultAsync();

            if (data.JobTitleId != "0") data.JobTitleName = context.JobTitles.Where(x => x.Id.Equals(data.JobTitleId)).FirstOrDefault().TitleName;
            else data.JobTitleName = "There is no Position";
            if (data.MenuId != null) data.MenuName = context.TmMenus.Where(x => x.Id.Equals(data.MenuId)).FirstOrDefault().MenuName;

            if (data == null) return data;

            if (!string.IsNullOrEmpty(data.Id))
                data.ListUserApprovals = (from lu in context.TmTemplateUserApprovals.Where(x => x.TmTempApprovalId.Equals(data.Id) && x.IsDeleted.Equals(false))
                                          join emp in context.EmployeeBasicInfos on lu.EmployeeBasicInfoId equals emp.Id
                                          select new TmTemplateUserApproval
                                          {
                                              Id = lu.Id,
                                              IndexUser = lu.IndexUser,
                                              EmployeeBasicInfoId = lu.EmployeeBasicInfoId,
                                              EmployeeName = emp.NameEmployee,
                                              IsCondition = lu.IsCondition,
                                              ListConditionalApprovals = null,
                                              IsSpecialCase = lu.IsSpecialCase,
                                              RemarkSpecialCase = lu.RemarkSpecialCase
                                          }).OrderBy(c => c.IndexUser).ToList();

            if (data.ListUserApprovals.Count > 0)
            {
                foreach (var x in data.ListUserApprovals)
                {
                    x.ListConditionalApprovals = (from ca in context.TmConditionUserApprovals.Where(c => c.TemplateUserApprovalId.Equals(x.Id) && c.IsDeleted.Equals(false))
                                                  join e in context.EmployeeBasicInfos on ca.EmployeeBasicInfoId equals e.Id
                                                  select new TmConditionUserApproval
                                                  {
                                                      Id = ca.Id,
                                                      EmployeeBasicInfoId = ca.EmployeeBasicInfoId,
                                                      EmployeeName = e.NameEmployee,
                                                      GroupConditionIndex = ca.GroupConditionIndex,
                                                      GroupConditionApproval = ca.GroupConditionApproval,
                                                      ConditionalApproval = ca.ConditionalApproval,
                                                      TemplateUserApprovalId = ca.TemplateUserApprovalId
                                                  }).OrderBy(c => c.GroupConditionIndex).ToList();
                }
            }

            return data;
        }

        public async Task<TmTemplateApproval> GetByMenuId(string groupId, string divisionId, string menuId, string jobTitleId)
        {
            var data = await (from t in context.TmTemplateApprovals.Where(x => x.SubGroupId.Equals(groupId) && x.DivisionId.Equals(divisionId) && x.IsDeleted == false
                              && x.MenuId.Equals(menuId) && x.JobTitleId.Equals(jobTitleId))
                              select new TmTemplateApproval
                              {
                                  TemplateName = t.TemplateName,
                                  Id = t.Id,
                                  IsNotifyByEmail = t.IsNotifyByEmail,
                                  IsNotifyByWeb = t.IsNotifyByWeb,
                                  DueDate = t.DueDate,
                                  Reminder = t.Reminder,
                                  ApprovalCategory = t.ApprovalCategory,
                                  ApprovalType = t.ApprovalType,
                                  CreatedBy = t.CreatedBy,
                                  CreatedOn = t.CreatedOn,
                                  ListUserApprovals = null,
                                  MenuId = string.IsNullOrEmpty(t.MenuId) ? "" : t.MenuId,
                                  JobTitleId = string.IsNullOrEmpty(t.JobTitleId) ? "0" : t.JobTitleId,
                                  MinBudget = t.MinBudget,
                                  MaxBudget = t.MaxBudget
                              }).OrderByDescending(x => x.CreatedOn).FirstOrDefaultAsync();

            if (data == null) return null;
            if (data.JobTitleId != "0") data.JobTitleName = context.JobTitles.Where(x => x.Id.Equals(data.JobTitleId)).FirstOrDefault().TitleName;
            else data.JobTitleName = "There is no Position";
            if (data.MenuId != null) data.MenuName = context.TmMenus.Where(x => x.Id.Equals(data.MenuId)).FirstOrDefault().MenuName;

            if (data == null) return data;

            if (!string.IsNullOrEmpty(data.Id))
                data.ListUserApprovals = (from lu in context.TmTemplateUserApprovals.Where(x => x.TmTempApprovalId.Equals(data.Id) && x.IsDeleted.Equals(false))
                                          join emp in context.EmployeeBasicInfos on lu.EmployeeBasicInfoId equals emp.Id
                                          select new TmTemplateUserApproval
                                          {
                                              Id = lu.Id,
                                              IndexUser = lu.IndexUser,
                                              EmployeeBasicInfoId = lu.EmployeeBasicInfoId,
                                              EmployeeName = emp.NameEmployee,
                                              IsCondition = lu.IsCondition,
                                              ListConditionalApprovals = null,
                                              IsSpecialCase = lu.IsSpecialCase,
                                              RemarkSpecialCase = lu.RemarkSpecialCase
                                          }).OrderBy(c => c.IndexUser).ToList();

            if (data.ListUserApprovals.Count > 0)
            {
                foreach (var x in data.ListUserApprovals)
                {
                    x.ListConditionalApprovals = (from ca in context.TmConditionUserApprovals.Where(c => c.TemplateUserApprovalId.Equals(x.Id) && c.IsDeleted.Equals(false))
                                                  join e in context.EmployeeBasicInfos on ca.EmployeeBasicInfoId equals e.Id
                                                  select new TmConditionUserApproval
                                                  {
                                                      Id = ca.Id,
                                                      EmployeeBasicInfoId = ca.EmployeeBasicInfoId,
                                                      EmployeeName = e.NameEmployee,
                                                      GroupConditionIndex = ca.GroupConditionIndex,
                                                      GroupConditionApproval = ca.GroupConditionApproval,
                                                      ConditionalApproval = ca.ConditionalApproval,
                                                      TemplateUserApprovalId = ca.TemplateUserApprovalId
                                                  }).OrderBy(c => c.GroupConditionIndex).ToList();
                }
            }

            return data;
        }
        public async Task<TmTemplateApproval> GetByBudgeting(string groupId, string divisionId, string menuId, decimal? budget)
        {
            var listTemp = await (from t in
                                    context.TmTemplateApprovals.Where(x => x.SubGroupId.Equals(groupId)
                                    && x.DivisionId.Equals(divisionId)
                                    && x.IsDeleted == false
                                    && x.MenuId.Equals(menuId) && x.MaxBudget > budget)
                                  select new TmTemplateApproval
                                  {
                                      TemplateName = t.TemplateName,
                                      Id = t.Id,
                                      IsNotifyByEmail = t.IsNotifyByEmail,
                                      IsNotifyByWeb = t.IsNotifyByWeb,
                                      DueDate = t.DueDate,
                                      Reminder = t.Reminder,
                                      ApprovalCategory = t.ApprovalCategory,
                                      ApprovalType = t.ApprovalType,
                                      CreatedBy = t.CreatedBy,
                                      CreatedOn = t.CreatedOn,
                                      ListUserApprovals = null,
                                      MenuId = string.IsNullOrEmpty(t.MenuId) ? "" : t.MenuId,
                                      JobTitleId = string.IsNullOrEmpty(t.JobTitleId) ? "0" : t.JobTitleId,
                                      MinBudget = t.MinBudget,
                                      MaxBudget = t.MaxBudget
                                  }).OrderBy(x => x.MinBudget).ToListAsync();

            var tempId = "";
            var data = new TmTemplateApproval();
            if (listTemp.Count > 1)
            {
                foreach (var item in listTemp)
                {
                    if (budget > item.MinBudget && budget < item.MaxBudget)
                    {
                        tempId = item.Id;
                        break;
                    }
                }
                data = listTemp.ToList().Where(x => x.Id.Equals(tempId)).FirstOrDefault();
            }
            else
            {
                data = listTemp.FirstOrDefault();
            }

            if (data.JobTitleId != "0") data.JobTitleName = context.JobTitles.Where(x => x.Id.Equals(data.JobTitleId)).FirstOrDefault().TitleName;
            else data.JobTitleName = "There is no Position";
            if (data.MenuId != null) data.MenuName = context.TmMenus.Where(x => x.Id.Equals(data.MenuId)).FirstOrDefault().MenuName;

            if (data == null) return data;

            if (!string.IsNullOrEmpty(data.Id))
                data.ListUserApprovals = (from lu in context.TmTemplateUserApprovals.Where(x => x.TmTempApprovalId.Equals(data.Id) && x.IsDeleted.Equals(false))
                                          join emp in context.EmployeeBasicInfos on lu.EmployeeBasicInfoId equals emp.Id
                                          select new TmTemplateUserApproval
                                          {
                                              Id = lu.Id,
                                              EmployeeBasicInfoId = lu.EmployeeBasicInfoId,
                                              EmployeeName = emp.NameEmployee,
                                              IsCondition = lu.IsCondition,
                                              ListConditionalApprovals = null,
                                              IsSpecialCase = lu.IsSpecialCase,
                                              RemarkSpecialCase = lu.RemarkSpecialCase
                                          }).OrderBy(c => c.IndexUser).ToList();

            if (data.ListUserApprovals.Count > 0)
            {
                foreach (var x in data.ListUserApprovals)
                {
                    x.ListConditionalApprovals = (from ca in context.TmConditionUserApprovals.Where(c => c.TemplateUserApprovalId.Equals(x.Id) && x.IsDeleted.Equals(false))
                                                  join e in context.EmployeeBasicInfos on ca.EmployeeBasicInfoId equals e.Id
                                                  select new TmConditionUserApproval
                                                  {
                                                      Id = ca.Id,
                                                      EmployeeBasicInfoId = ca.EmployeeBasicInfoId,
                                                      EmployeeName = e.NameEmployee,
                                                      GroupConditionIndex = ca.GroupConditionIndex,
                                                      GroupConditionApproval = ca.GroupConditionApproval,
                                                      ConditionalApproval = ca.ConditionalApproval,
                                                      TemplateUserApprovalId = ca.TemplateUserApprovalId
                                                  }).OrderBy(c => c.GroupConditionIndex).ToList();
                }
            }

            return data;
        }
        public async Task<List<TmTemplateApproval>> GetByCategory(int categoryId)
        {
            var data = await (from t in context.TmTemplateApprovals.Where(x => x.ApprovalCategory == categoryId)
                              select new TmTemplateApproval
                              {
                                  TemplateName = t.TemplateName,
                                  Id = t.Id,
                                  IsNotifyByEmail = t.IsNotifyByEmail,
                                  IsNotifyByWeb = t.IsNotifyByWeb,
                                  DueDate = t.DueDate,
                                  Reminder = t.Reminder,
                                  ApprovalCategory = t.ApprovalCategory,
                                  ApprovalType = t.ApprovalType,
                                  CreatedBy = t.CreatedBy,
                                  CreatedOn = t.CreatedOn,
                                  ListUserApprovals = null,
                                  MenuId = string.IsNullOrEmpty(t.MenuId) ? "" : t.MenuId,
                                  JobTitleId = string.IsNullOrEmpty(t.JobTitleId) ? "0" : t.JobTitleId,
                                  MinBudget = t.MinBudget,
                                  MaxBudget = t.MaxBudget
                              }).OrderBy(x => x.TemplateName).ToListAsync();

            foreach (var item in data)
            {
                if (item.JobTitleId != "0") item.JobTitleName = context.JobTitles.Where(x => x.Id.Equals(item.JobTitleId)).FirstOrDefault().TitleName;
                else item.JobTitleName = "There is no Position";
                if (item.MenuId != null) item.MenuName = context.TmMenus.Where(x => x.Id.Equals(item.MenuId)).FirstOrDefault().MenuName;
            }

            return data;
        }

        public async Task<List<JobTitle>> GetJobTitlesByDivisionId(string divisionId)
        {
            var sql = @"SELECT  [Id]
                          ,[title_code]
                          ,[title_name]
                          ,[created_by]
                          ,[created_on]
                          ,[modified_by]
                          ,[modified_on]
                          ,[approved_by]
                          ,[approved_on]
                          ,[is_active]
                          ,[is_locked]
                          ,[is_default]
                          ,[is_deleted]
                          ,[owner_id]
                          ,[deleted_by]
                          ,[deleted_on]
                          ,[business_unit_division_id]
                          ,[business_unit_departement_id]
                      FROM [hr].[job_title]
                      where id in (SELECT a.job_title_id FROM hr.business_unit_job_level a
                    where business_unit_division_id = '" + divisionId + "')";
            try
            {
                var listJobTitle = await context.JobTitles.FromSql(sql).ToListAsync();
                return listJobTitle;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return null;
            }
        }
        public async Task<List<BusinessUnit>> GetDivision()
        {
            return await (from x in context.BusinessUnits
                          join p in context.BusinessUnitTypes.Where(xx => xx.BusinessUnitLevel.Equals(800)).ToList()
                            on x.BusinessUnitTypeId equals p.Id
                          select new BusinessUnit
                          {
                              Id = x.Id,
                              UnitName = x.UnitName
                          }).ToListAsync();
        }
        public async Task<BusinessUnitJobLevel> GetBussinessUnitJobLevel(string jobTitleId, string divisionId)
        {
            return await context.BusinessUnitJobLevels.Where(x => x.BusinessUnitDivisionId.Equals(divisionId) && x.JobTitleId.Equals(jobTitleId)).FirstOrDefaultAsync();
        }



        public class VmJobTitle
        {
            public string JobTitleId { get; set; }
        }
    }
}
