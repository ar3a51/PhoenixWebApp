﻿using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Models;
using Phoenix.Data.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.Data.Services.Um
{
    public class NotificationService : INotificationService
    {
        readonly DataContext context;

        /// <summary>
        /// And endpoint to manage SalaryComponent
        /// </summary>
        /// <param name="context">Database context</param>
        public NotificationService(DataContext context)
        {
            this.context = context;
        }

        public async Task<int> AddAsync(Notification entity)
        {
            await context.PhoenixAddAsync(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> AddRangeAsync(params Notification[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteAsync(Notification entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteRageAsync(params Notification[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> DeleteSoftAsync(Notification entity)
        {
            context.PhoenixDelete(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> DeleteSoftRangeAsync(params Notification[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> EditAsync(Notification entity)
        {
            context.PhoenixEdit(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> EditRangeAsync(Notification entity)
        {
            throw new NotImplementedException();
        }

        public async Task<List<Notification>> Get() => await context.Notifications.AsNoTracking().ToListAsync();

        public Task<List<Notification>> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public async Task<Notification> Get(string Id) => await context.Notifications.AsNoTracking().Where(x => x.Id == Id).AsNoTracking().FirstOrDefaultAsync();

        public async Task<List<NotificationVm>> GetListNotificationByUserCode(string userCode)
        {
            var data = await(from x in  context.Notifications.Where(c=>c.IsSeen.Equals(false) && c.UserId.Equals(userCode))
                             join t in context.NotificationTypes on x.NotificationTypeId equals t.Id
                             select new NotificationVm
                             {
                                 TemplateNotify = "<a href='javascript:directToDetailNotif(\"" + x.Id + "\",\"" + t.Href + "\")' title='click here to see detail'><div><p>" + t.ShortText + "</p></div></a>",
                                 CreatedOn = x.CreatedOn,
                                 Tipe = x.NotificationTypeId.Contains("approv") ? 2 : 1
                             }).OrderByDescending(x=>x.CreatedOn).ToListAsync();
            return data;
        }
    }
}
