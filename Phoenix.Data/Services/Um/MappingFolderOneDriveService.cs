﻿using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Models.Um;
using Phoenix.Data.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.Data.Services.Um
{
    public class MappingFolderOneDriveService : IMappingFolderOneDriveService
    {
        readonly DataContext context;

        /// <summary>
        /// And endpoint to manage SalaryComponent
        /// </summary>
        /// <param name="context">Database context</param>
        public MappingFolderOneDriveService(DataContext context)
        {
            this.context = context;
        }

        public async Task<int> AddAsync(TrMappingFolderOneDrive entity)
        {
            entity.Id = Guid.NewGuid().ToString();
            await context.PhoenixAddAsync(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> AddRangeAsync(params TrMappingFolderOneDrive[] entities)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteAsync(TrMappingFolderOneDrive entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteRageAsync(params TrMappingFolderOneDrive[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> DeleteSoftAsync(TrMappingFolderOneDrive entity)
        {
            context.PhoenixDelete(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> DeleteSoftRangeAsync(params TrMappingFolderOneDrive[] entities)
        {
            throw new NotImplementedException();
        }

        public async Task<int> EditAsync(TrMappingFolderOneDrive entity)
        {
            context.PhoenixEdit(entity);
            return await context.SaveChangesAsync();
        }

        public Task<int> EditRangeAsync(TrMappingFolderOneDrive entity)
        {
            throw new NotImplementedException();
        }

        //public async Task<List<TrMappingFolderOneDrive>> GetBySearch(string key) => await context.TrMappingFolderOneDrives.AsNoTracking().Where(x => !(x.IsDeleted ?? false) && x.GroupName.Contains(key)).ToListAsync();
        public async Task<List<TrMappingFolderOneDrive>> Get() => await context.TrMappingFolderOneDrives.AsNoTracking().Where(x => !(x.IsDeleted ?? false)).ToListAsync();

        public Task<List<TrMappingFolderOneDrive>> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public async Task<TrMappingFolderOneDrive> Get(string Id) => await context.TrMappingFolderOneDrives.AsNoTracking().Where(x => !(x.IsDeleted ?? false) && x.Id == Id).AsNoTracking().FirstOrDefaultAsync();

        public Task<int> DeleteAllMappingByItemId(string itemId)
        {
            var commandText = @"UPDATE um.TrMappingFolderOneDrive SET
	                                    IsDeleted = 1,
	                                    DeletedBy = @DeletedBy,
	                                    DeletedOn = GETDATE()
                                    where ItemId = @Id";
            var deletedBy = new SqlParameter("@DeletedBy", context.GetUserId());
            var driveId = new SqlParameter("@Id", itemId);
            return context.Database.ExecuteSqlCommandAsync(commandText, deletedBy, driveId);
        }
        public Task<int> DeleteByItemIdAndRefId(string itemId, string refId)
        {
            var commandText = @"UPDATE um.TrMappingFolderOneDrive SET
	                                    IsDeleted = 1,
	                                    DeletedBy = @DeletedBy,
	                                    DeletedOn = GETDATE()
                                    where ItemId = @Id AND RefId = @RefId";
            var deletedBy = new SqlParameter("@DeletedBy", context.GetUserId());
            var driveId = new SqlParameter("@Id", itemId);
            var referenceId = new SqlParameter("@RefId", refId);
            return context.Database.ExecuteSqlCommandAsync(commandText, deletedBy, driveId, referenceId);
        }
        public async Task<List<VmMapFodAccess>> GetByItemId(string itemid)
        {
            var sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@ItemId",itemid)
            };
            return await context.ExecuteStoredProcedure<VmMapFodAccess>("dbo.uspGet_MappingOneDriveByItemId", sqlParameters);            
        }

        public async Task<List<VmFodUserList>> GetUserList(string itemid,string key)
        {
            var sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@key",key),
                new SqlParameter("@ItemId",itemid)
            };
            return await context.ExecuteStoredProcedure<VmFodUserList>("dbo.uspGet_FodUserNotInMapping", sqlParameters);
        }
        public async Task<List<VmFodGroupList>> GetGroupList(string itemid, string key)
        {
            var sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@key",key),
                new SqlParameter("@ItemId",itemid)
            };
            return await context.ExecuteStoredProcedure<VmFodGroupList>("dbo.uspGet_FodGroupNotInMapping", sqlParameters);
        }
    }
}
