﻿using Microsoft.EntityFrameworkCore;
using Phoenix.Data.Models.Um;
using Phoenix.Data.Models.ViewModel;
using Phoenix.Data.Services.Um;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.Data.Attributes
{
    public class GlobalFunctionApproval
    {
        readonly DataContext _context;
        private readonly IManageTransactionApprovalService _repo;
        private readonly IManageTemplateApprovalService _repoTemplate;
        private readonly ITransactionUserApprovalService _repoManageUserApproval;
        private readonly ITransactionConditionalApprovalService _repoManageConditionApproval;
        readonly Phoenix.Data.Services.Um.IManageMenuService _menuService;
        public GlobalFunctionApproval(DataContext context, IManageTransactionApprovalService repo, ITransactionUserApprovalService repoManageUserApproval, ITransactionConditionalApprovalService repoManageConditionApproval,
            IManageTemplateApprovalService repoTemplate, Phoenix.Data.Services.Um.IManageMenuService menuService)
        {
            _context = context;
            _repo = repo;
            _repoManageUserApproval = repoManageUserApproval;
            _repoManageConditionApproval = repoManageConditionApproval;
            _repoTemplate = repoTemplate;
            _menuService = menuService;
        }

        public async Task<int> SubmitApproval<T>(bool isEdit, string Id, int? status, string detailLink, string idTemplate, string uniqueName, T model) where T : class
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var save = await UnsafeSubmitApproval(isEdit, Id, status, detailLink, idTemplate, uniqueName, model);
                    transaction.Commit();
                    return save;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<int> UnsafeSubmitApproval<T>(bool isEdit, string Id, int? status, string detailLink, string idTemplate, string uniqueName, T model) where T : class
        {
            if (isEdit)
            {
                _context.PhoenixEdit(model);
            }
            else
            {
                await _context.PhoenixAddAsync(model);
            }
            var isApprove = true;
            if (status != 0)//0: Draft
            {

                var dataMenu = await _menuService.GetByUniqueName(uniqueName).ConfigureAwait(false);
                var vm = new Models.ViewModel.TransApprovalHrisVm()
                {
                    MenuId = dataMenu.Id,
                    RefId = Id,
                    DetailLink = detailLink,
                    //Tname = "hr.Rotation",//"SP#NAMA SP NYA",
                    IdTemplate = idTemplate
                };
                var subGroupId = Phoenix.Shared.Core.PrincipalHelpers.ClainJwtPrincipalHelper.GetBusinessSubGroup(_context.HttpContext);
                var divisionId = Phoenix.Shared.Core.PrincipalHelpers.ClainJwtPrincipalHelper.GetBusinessUnitDivisi(_context.HttpContext);
                var employeeId = Phoenix.Shared.Core.PrincipalHelpers.ClainJwtPrincipalHelper.GetEmployeeId(_context.HttpContext);
                isApprove = await Save(vm, subGroupId, divisionId, employeeId);

                if (isApprove)
                {
                    return await _context.SaveChangesAsync();
                }
                else
                {
                    throw new Exception("please check the approval template that will be processed");
                }
            }
            else
            {
                return await _context.SaveChangesAsync();
            }
        }



        public async Task<int> ProcessApproval<T>(string Id, int? status, string remarks, T model) where T : class
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var save = await UnsafeProcessApproval(Id, status, remarks, model);
                    transaction.Commit();
                    return save;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<int> UnsafeProcessApproval<T>(string Id, int? status, string remarks, T model) where T : class
        {
            _context.PhoenixApprove(model);

            var vm = new TrUpdateStatusApprovalViewModel()
            {
                RefId = Id,
                StatusApproved = status ?? 0,
                Remarks = remarks
            };

            var employeeId = Phoenix.Shared.Core.PrincipalHelpers.ClainJwtPrincipalHelper.GetEmployeeId(_context.HttpContext);
            var isApprove = await UpdateStatusApproval(vm, employeeId);

            if (isApprove)
            {
                return await _context.SaveChangesAsync();
            }
            else
            {
                throw new Exception();
            }
        }


        public async Task<bool> Save(TransApprovalHrisVm dataVm, string subGroupId, string divisionId, string employeeId)
        {
            if (!string.IsNullOrEmpty(dataVm.Tname) && !string.IsNullOrEmpty(dataVm.RefId))
            {
                if (!string.IsNullOrEmpty(dataVm.MenuId) || !string.IsNullOrEmpty(dataVm.DetailLink))
                {
                    await _repo.DeleteRefTable(dataVm.Tname, dataVm.RefId).ConfigureAwait(false);
                    return await Task.FromResult(false);
                }
            }

            if (!string.IsNullOrEmpty(dataVm.RefId))
            {
                var trApproval = _context.TrTemplateApprovals.AsNoTracking().Where(x => x.RefId == dataVm.RefId && !(x.IsDeleted ?? false) && x.StatusApproved == StatusApproved.Rejected);
                if (trApproval != null)
                {
                    foreach (var item in trApproval)
                    {
                        item.IsDeleted = true;
                        _context.Update(item);
                    }
                }
            }

            try
            {
                var jobTitleId = "0";
                var data = new TmTemplateApproval();
                if (!string.IsNullOrEmpty(dataVm.IdTemplate))
                {
                    data = await _repoTemplate.GetById(dataVm.IdTemplate).ConfigureAwait(false);
                }
                else
                {
                    if (string.IsNullOrEmpty(subGroupId) || string.IsNullOrEmpty(divisionId))
                    {
                        throw new Exception("this employee doesn't have a sub group or division, please check this employee data again.");
                    }

                    data = await _repoTemplate.GetByMenuId(subGroupId, divisionId, dataVm.MenuId, jobTitleId).ConfigureAwait(false);

                    if (data == null)
                    {
                        throw new Exception("please check the approval template that will be processed");
                    }
                }
                var model = new TrTemplateApproval();
                model.TemplateName = data.TemplateName;
                model.IsNotifyByWeb = data.IsNotifyByWeb;
                model.IsNotifyByEmail = data.IsNotifyByEmail;
                model.DueDate = data.DueDate;
                model.DetailLink = dataVm.DetailLink;
                model.StatusApproved = StatusApproved.WaitingApproval;
                model.Reminder = data.Reminder;
                model.ApprovalCategory = data.ApprovalCategory;
                model.ApprovalType = data.ApprovalType;
                model.FormReqName = data.TemplateName;
                model.CreatedOn = DateTime.Now;
                model.CreatedBy = employeeId;
                model.DivisionId = divisionId;
                model.IsDeleted = false;
                model.RefId = dataVm.RefId;
                var tempId = Guid.NewGuid().ToString();
                model.Id = tempId;

                await _context.PhoenixAddAsync(model);
                //await _repo.AddAsync(model).ConfigureAwait(false);


                var isFirst = 0;
                foreach (var item in data.ListUserApprovals)
                {
                    var vmUser = new TrUserApproval();
                    vmUser.EmployeeBasicInfoId = item.EmployeeBasicInfoId;
                    vmUser.IndexUser = item.IndexUser;
                    vmUser.IsCondition = item.IsCondition;
                    vmUser.TrTempApprovalId = tempId;
                    vmUser.Id = Guid.NewGuid().ToString();
                    if (isFirst == 0) vmUser.StatusApproved = StatusApproved.CurrentApproval;
                    else vmUser.StatusApproved = StatusApproved.WaitingApproval;
                    await _context.PhoenixAddAsync(vmUser);
                    //await _repoManageUserApproval.AddAsync(vmUser).ConfigureAwait(false);

                    //looping data conditional user approval.....
                    foreach (var con in item.ListConditionalApprovals)
                    {
                        var modelCon = new TrConditionUserApproval();
                        modelCon.Id = Guid.NewGuid().ToString();
                        modelCon.EmployeeBasicInfoId = con.EmployeeBasicInfoId;
                        modelCon.GroupConditionIndex = con.GroupConditionIndex;
                        modelCon.GroupConditionApproval = con.GroupConditionApproval;
                        modelCon.ConditionalApproval = con.ConditionalApproval;
                        if (isFirst == 0) modelCon.StatusApproved = StatusApproved.CurrentApproval;
                        else modelCon.StatusApproved = StatusApproved.WaitingApproval;
                        modelCon.TrUserApprovalId = vmUser.Id;
                        modelCon.CreatedOn = DateTime.Now;
                        modelCon.CreatedBy = employeeId;
                        await _context.PhoenixAddAsync(modelCon);
                        //await _repoManageConditionApproval.AddAsync(modelCon).ConfigureAwait(false);
                    }
                    await _context.SaveChangesAsync();
                    isFirst++;
                }
                var vm = new
                {
                    IdTemp = tempId
                };
                return await Task.FromResult(true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                if (!string.IsNullOrEmpty(dataVm.RefId) && !string.IsNullOrEmpty(dataVm.Tname))
                {
                    await _repo.DeleteRefTable(dataVm.Tname, dataVm.RefId).ConfigureAwait(false);
                }
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> UpdateStatusApproval(TrUpdateStatusApprovalViewModel data, string empId)
        {
            if (data.StatusApproved == 4 && string.IsNullOrEmpty(data.Remarks))
            {
                return await Task.FromResult(false);
            }
            //1. Cek user/employee yang approve di conditional or di concurrent....

            //--Get Template Approval nya dulu...
            var dataApproval = await _repo.GetByRefId(data.RefId).ConfigureAwait(false);

            foreach (var item in dataApproval.ListUserApprovals)
            {
                if (item.IsCondition == true && item.StatusApproved == StatusApproved.CurrentApproval)
                {
                    // -- Cek di Conditional nya...
                    foreach (var x in item.ListConditionalApprovals)
                    {
                        if (x.EmployeeBasicInfoId == empId)
                        {
                            if (x.ConditionalApproval == "OR")
                            {
                                if (x.GroupConditionApproval == "OR")
                                {
                                    CheckAllOrCondition(empId, data, x.Id, item.Id, dataApproval.Id, dataApproval.ListUserApprovals);
                                    break;
                                }
                                else // kalau kondisi nya AND..
                                {
                                    if (data.StatusApproved == 4) // jika rejected...kelar alias close..
                                    {
                                        //update Table Conditional...
                                        var dataCon = await _repoManageConditionApproval.Get(x.Id).ConfigureAwait(false);
                                        dataCon.StatusApproved = (StatusApproved)Enum.ToObject(typeof(StatusApproved), data.StatusApproved);
                                        dataCon.CreatedBy = empId;
                                        dataCon.CreatedOn = DateTime.Now;
                                        await _repoManageConditionApproval.EditAsync(dataCon);

                                        //update table User APproval...
                                        var dataUserApp = await _repoManageUserApproval.Get(x.TrUserApprovalId).ConfigureAwait(false);
                                        dataUserApp.CreatedBy = empId;
                                        dataUserApp.CreatedOn = DateTime.Now;
                                        dataUserApp.StatusApproved = (StatusApproved)Enum.ToObject(typeof(StatusApproved), data.StatusApproved);
                                        await _repoManageUserApproval.EditAsync(dataUserApp).ConfigureAwait(false);

                                        var dataApp = await _repo.Get(dataApproval.Id);
                                        dataApp.RemarkRejected = data.Remarks;
                                        dataApp.StatusApproved = StatusApproved.Rejected;
                                        dataApp.RejectedBy = empId;
                                        dataApp.RejectedOn = DateTime.Now;
                                        await _repo.EditAsync(dataApp).ConfigureAwait(false);
                                        goto FinishedProcess;
                                    }
                                    else
                                    {
                                        CheckConditionGroupForOrAnd(item.ListConditionalApprovals, empId, data, dataApproval.ListUserApprovals, dataApproval.Id);
                                        goto FinishedProcess;
                                    }
                                }
                            }
                            else // conditional = AND
                            {
                                if (x.GroupConditionApproval == "OR")
                                {
                                    CheckConditionGroupForAndPlusOr(item.ListConditionalApprovals, empId, data, dataApproval.ListUserApprovals);
                                    goto FinishedProcess;
                                }
                                else // Contional Group = AND
                                {
                                    CheckConditionGroupForAndPlusOr(item.ListConditionalApprovals, empId, data, dataApproval.ListUserApprovals);
                                    goto FinishedProcess;
                                }
                            }
                        }
                    }
                }
                else // JIka tidak ada Conditional approval... 
                {
                    if (item.StatusApproved == StatusApproved.CurrentApproval)
                    {
                        if (item.EmployeeBasicInfoId == empId)
                        {

                            //update table User APproval...
                            var dataUserApp = await _repoManageUserApproval.Get(item.Id).ConfigureAwait(false);
                            //var dataUserApp = await _repoManageUserApproval.GetByTemplateSingleId(item.TrTempApprovalId).ConfigureAwait(false);
                            dataUserApp.CreatedBy = empId;
                            dataUserApp.CreatedOn = DateTime.Now;
                            dataUserApp.StatusApproved = (StatusApproved)Enum.ToObject(typeof(StatusApproved), data.StatusApproved);
                            await _repoManageUserApproval.EditAsync(dataUserApp).ConfigureAwait(false);

                            if (data.StatusApproved == 4)
                            {
                                //update table tr approval header nya...and request form is Close...
                                // var dataApp = await _repo.Get(dataUserApp.TrTempApprovalId).ConfigureAwait(false);
                                var dataApp = await _repo.Get(dataUserApp.TrTempApprovalId).ConfigureAwait(false);
                                dataApp.RemarkRejected = data.Remarks;
                                dataApp.StatusApproved = StatusApproved.Rejected;
                                dataApp.RejectedBy = empId;
                                dataApp.RejectedOn = DateTime.Now;
                                await _repo.EditAsync(dataApp).ConfigureAwait(false);
                            }
                            else
                            {
                                //baca list user approval nya, masih ada atau nggak ada...
                                var dataUserConCurrentApproval = dataApproval.ListUserApprovals.Where(x => x.StatusApproved == StatusApproved.CurrentApproval || x.StatusApproved == StatusApproved.WaitingApproval).ToList();
                                if (dataUserConCurrentApproval.Count == 0)
                                {
                                    // var dataApp = await _repo.Get(dataUserApp.TrTempApprovalId).ConfigureAwait(false);
                                    var dataApp = await _repo.Get(dataUserApp.Id).ConfigureAwait(false);
                                    dataApp.StatusApproved = StatusApproved.Approved;
                                    dataApp.RejectedBy = empId;
                                    dataApp.RejectedOn = DateTime.Now;
                                    await _repo.EditAsync(dataApp).ConfigureAwait(false);
                                }
                                else if (dataUserConCurrentApproval.Count == 1)
                                {
                                    var nextIndex = dataUserApp.IndexUser + 1;
                                    await _repoManageUserApproval.UpdateNextApproval(dataUserApp.TrTempApprovalId, Convert.ToInt32(nextIndex)).ConfigureAwait(false);
                                    var dataConcurrent = _repoManageUserApproval.GetCurrentApproval(dataUserApp.TrTempApprovalId).Result;
                                    if (dataConcurrent != null)
                                        await _repoManageConditionApproval.UpdateNextApproval(dataConcurrent.Id).ConfigureAwait(false);

                                    // var dataApp = await _repo.Get(dataUserApp.Id).ConfigureAwait(false);
                                    var dataApp = await _repo.Get(dataUserApp.TrTempApprovalId).ConfigureAwait(false);
                                    dataApp.StatusApproved = StatusApproved.Approved;
                                    dataApp.RejectedBy = empId;
                                    dataApp.RejectedOn = DateTime.Now;
                                    await _repo.EditAsync(dataApp).ConfigureAwait(false);
                                }
                                else
                                {
                                    var nextIndex = dataUserApp.IndexUser + 1;
                                    await _repoManageUserApproval.UpdateNextApproval(dataUserApp.TrTempApprovalId, Convert.ToInt32(nextIndex)).ConfigureAwait(false);
                                    var dataConcurrent = _repoManageUserApproval.GetCurrentApproval(dataUserApp.TrTempApprovalId).Result;
                                    if (dataConcurrent != null)
                                        await _repoManageConditionApproval.UpdateNextApproval(dataConcurrent.Id).ConfigureAwait(false);
                                }
                            }
                            break;
                        }
                    }
                }
            }

        FinishedProcess:
            return await Task.FromResult(true);
        }


        public async Task clearApproval(string refId)
        {
            var template = await _context.TrTemplateApprovals.AsNoTracking().Where(x => x.RefId == refId).ToListAsync();
            var templateUser = await (from b in _context.TrUserApprovals.AsNoTracking()
                                      join c in _context.TrTemplateApprovals on b.TrTempApprovalId equals c.Id
                                      where c.RefId == refId
                                      select b).ToListAsync();

            var templateCondition = await (from a in _context.TrConditionUserApprovals.AsNoTracking()
                                           join b in _context.TrUserApprovals.AsNoTracking() on a.TrUserApprovalId equals b.Id
                                           join c in _context.TrTemplateApprovals on b.TrTempApprovalId equals c.Id
                                           where c.RefId == refId
                                           select a).ToListAsync();

            _context.TrConditionUserApprovals.RemoveRange(templateCondition.ToArray());
            _context.TrUserApprovals.RemoveRange(templateUser.ToArray());
            _context.TrTemplateApprovals.RemoveRange(template.ToArray());
        }

        #region Condition OR (or + And)
        private void CheckConditionGroupForOrAnd(List<TrConditionUserApproval> listConditional, string empId, TrUpdateStatusApprovalViewModel data, List<TrUserApproval> listConcurrentUser, string trApprovalId)
        {
            //1. Cek Self Group...
            //--Cek and Insert Group Index...Gropuing By GroupIndexApproval...
            var vmListGroup = new List<VmGrouping>();
            foreach (var item in listConditional)
            {
                var isready = false;
                foreach (var g in vmListGroup)
                {
                    if (item.GroupConditionIndex == g.GroupIndex) isready = true;
                }
                if (!isready)
                {
                    vmListGroup.Add(new VmGrouping { GroupIndex = Convert.ToInt32(item.GroupConditionIndex) });
                }
            }

            //update Table Conditional...
            var conData = listConditional.Where(c => c.EmployeeBasicInfoId == empId).FirstOrDefault();
            var dataCon = _repoManageConditionApproval.Get(conData.Id).Result;
            dataCon.StatusApproved = (StatusApproved)Enum.ToObject(typeof(StatusApproved), data.StatusApproved);
            dataCon.CreatedBy = empId;
            dataCon.CreatedOn = DateTime.Now;
            _repoManageConditionApproval.EditAsync(dataCon).ConfigureAwait(false);

            //--Add List Condition to Grouping INdex and CheckIN if user already approved all....
            foreach (var item in vmListGroup)
            {
                var dataConditionList = listConditional.Where(c => c.GroupConditionIndex == item.GroupIndex && (c.StatusApproved == StatusApproved.WaitingApproval || c.StatusApproved == StatusApproved.CurrentApproval)).ToList();
                if (dataConditionList.Count == 0)// semua dalam group sudah approved..
                {
                    var dataUserConCurrentApproval = listConcurrentUser.Where(x => x.StatusApproved == StatusApproved.WaitingApproval).ToList();
                    if (dataUserConCurrentApproval.Count == 0) // jika semua user dalam Group itu sudah approved..
                    {
                        //update table User APproval...
                        var dataUserApp = _repoManageUserApproval.Get(conData.TrUserApprovalId).Result;
                        dataUserApp.CreatedBy = empId;
                        dataUserApp.CreatedOn = DateTime.Now;
                        dataUserApp.StatusApproved = (StatusApproved)Enum.ToObject(typeof(StatusApproved), data.StatusApproved);
                        _repoManageUserApproval.EditAsync(dataUserApp).ConfigureAwait(false);

                        // Update table Header (TrTemplateApproval)....
                        var dataApp = _repo.Get(trApprovalId).Result;
                        dataApp.StatusApproved = StatusApproved.Approved;
                        dataApp.RejectedBy = empId;
                        dataApp.RejectedOn = DateTime.Now;
                        _repo.EditAsync(dataApp).ConfigureAwait(false);

                        var nextIndex = dataUserApp.IndexUser + 1;
                        //update User Approval Concurrent next Index...
                        _repoManageUserApproval.UpdateNextApproval(trApprovalId, Convert.ToInt32(nextIndex)).ConfigureAwait(false);
                        var dataConcurrent = _repoManageUserApproval.GetCurrentApproval(trApprovalId).Result;
                        if (dataConcurrent != null)
                            _repoManageConditionApproval.UpdateNextApproval(dataConcurrent.Id).ConfigureAwait(false);
                        break;
                    }
                }
            }
        }

        private void CheckAllOrCondition(string empId, TrUpdateStatusApprovalViewModel data, string idConditional, string idUserApproval, string idApproval, List<TrUserApproval> listConcurrentUser)
        {
            //update Table Conditional...
            var dataCon = _repoManageConditionApproval.Get(idConditional).Result;
            dataCon.StatusApproved = (StatusApproved)Enum.ToObject(typeof(StatusApproved), data.StatusApproved);
            dataCon.CreatedBy = empId;
            dataCon.CreatedOn = DateTime.Now;
            _repoManageConditionApproval.EditAsync(dataCon).ConfigureAwait(false);

            //update table User APproval...
            var dataUserApp = _repoManageUserApproval.Get(idUserApproval).Result;
            dataUserApp.CreatedBy = empId;
            dataUserApp.CreatedOn = DateTime.Now;
            dataUserApp.StatusApproved = (StatusApproved)Enum.ToObject(typeof(StatusApproved), data.StatusApproved);
            _repoManageUserApproval.EditAsync(dataUserApp).ConfigureAwait(false);

            if (data.StatusApproved == 4)// rejected
            {
                //update table tr approval header nya...and request form is Close...
                var dataApp = _repo.Get(idApproval).Result;
                dataApp.RemarkRejected = data.Remarks;
                dataApp.StatusApproved = StatusApproved.Rejected;
                dataApp.RejectedBy = empId;
                dataApp.RejectedOn = DateTime.Now;
                _repo.EditAsync(dataApp).ConfigureAwait(false);
            }
            else
            {
                //baca list user approval nya, masih ada atau nggak ada...
                var dataUserConCurrentApproval = listConcurrentUser.Where(x => x.StatusApproved == StatusApproved.CurrentApproval || x.StatusApproved == StatusApproved.WaitingApproval).ToList();
                if (dataUserConCurrentApproval.Count == 0)
                {
                    var dataApp = _repo.Get(idApproval).Result;
                    dataApp.StatusApproved = StatusApproved.Approved;
                    dataApp.RejectedBy = empId;
                    dataApp.RejectedOn = DateTime.Now;
                    _repo.EditAsync(dataApp).ConfigureAwait(false);
                }
                else
                {
                    var nextIndex = dataUserApp.IndexUser + 1;
                    //update User Approval Concurrent next Index...
                    _repoManageUserApproval.UpdateNextApproval(idApproval, Convert.ToInt32(nextIndex)).ConfigureAwait(false);
                    var dataConcurrent = _repoManageUserApproval.GetCurrentApproval(idApproval).Result;
                    if (dataConcurrent != null)
                        _repoManageConditionApproval.UpdateNextApproval(dataConcurrent.Id).ConfigureAwait(false);
                }
            }
        }
        #endregion

        #region Condition AND (or + And)
        private void CheckConditionGroupForAndPlusOr(List<TrConditionUserApproval> listConditional, string empId, TrUpdateStatusApprovalViewModel data, List<TrUserApproval> listConcurrentUser)
        {
            //cek status approved nya Reject or Approve...
            if (data.StatusApproved == 4)
            {
                //update Table Conditional...
                var conData = listConditional.Where(c => c.EmployeeBasicInfoId == empId).FirstOrDefault();
                var dataCon = _repoManageConditionApproval.Get(conData.Id).Result;
                dataCon.StatusApproved = (StatusApproved)Enum.ToObject(typeof(StatusApproved), data.StatusApproved);
                dataCon.CreatedBy = empId;
                dataCon.CreatedOn = DateTime.Now;
                _repoManageConditionApproval.EditAsync(dataCon).ConfigureAwait(false);
                //update table User APproval...
                var dataUserApp = _repoManageUserApproval.Get(conData.TrUserApprovalId).Result;
                dataUserApp.CreatedBy = empId;
                dataUserApp.CreatedOn = DateTime.Now;
                dataUserApp.StatusApproved = (StatusApproved)Enum.ToObject(typeof(StatusApproved), data.StatusApproved);
                _repoManageUserApproval.EditAsync(dataUserApp).ConfigureAwait(false);

                //update table tr approval header nya...and request form is Close...
                var dataApp = _repo.Get(dataUserApp.TrTempApprovalId).Result;
                dataApp.RemarkRejected = data.Remarks;
                dataApp.StatusApproved = StatusApproved.Rejected;
                dataApp.RejectedBy = empId;
                dataApp.RejectedOn = DateTime.Now;
                _repo.EditAsync(dataApp).ConfigureAwait(false);
            }
            else
            {
                //1. Cek Self Group...
                //--Cek and Insert Group Index...Gropuing By GroupIndexApproval...
                var vmListGroup = new List<VmGrouping>();
                foreach (var item in listConditional)
                {
                    var isready = false;
                    foreach (var g in vmListGroup)
                    {
                        if (item.GroupConditionIndex == g.GroupIndex) isready = true;
                    }
                    if (!isready)
                    {
                        vmListGroup.Add(new VmGrouping { GroupIndex = Convert.ToInt32(item.GroupConditionIndex) });
                    }
                }

                //update Table Conditional...
                var conData = listConditional.Where(c => c.EmployeeBasicInfoId == empId).FirstOrDefault();
                var dataCon = _repoManageConditionApproval.Get(conData.Id).Result;
                dataCon.StatusApproved = (StatusApproved)Enum.ToObject(typeof(StatusApproved), data.StatusApproved);
                dataCon.CreatedBy = empId;
                dataCon.CreatedOn = DateTime.Now;
                _repoManageConditionApproval.EditAsync(dataCon).ConfigureAwait(false);

                //...cek other Group ,satu satu apakah group conditional nya AND atau OR..
                var vmListTempGroup = new List<VmGroupingChekclist>();
                vmListTempGroup.Add(new VmGroupingChekclist { GroupIndex = Convert.ToInt32(conData.GroupConditionIndex), IsAllApproved = true });

                foreach (var item in vmListGroup)
                {
                    if (conData.GroupConditionIndex != item.GroupIndex) // yang di cek Group INdex nya yang berbeda dari yang nge-Prove..
                    {
                        var vmGI = vmListTempGroup.Where(c => c.GroupIndex == item.GroupIndex).FirstOrDefault();
                        if (vmGI == null)
                        {
                            //..cek group approval nya..
                            var dataCondisi = listConditional.Where(c => c.GroupConditionIndex == item.GroupIndex).FirstOrDefault();
                            if (dataCondisi.GroupConditionApproval == "OR")//jika Or..
                            {
                                var dt = listConditional.Where(c => c.GroupConditionIndex == item.GroupIndex && c.StatusApproved == StatusApproved.Approved).ToList();
                                if (dt.Count > 0)
                                    vmListTempGroup.Add(new VmGroupingChekclist { GroupIndex = item.GroupIndex, IsAllApproved = true });
                                else vmListTempGroup.Add(new VmGroupingChekclist { GroupIndex = item.GroupIndex, IsAllApproved = false });

                            }
                            else // JIka AND group conditional nya..
                            {
                                var dt = listConditional.Where(c => c.GroupConditionIndex == item.GroupIndex && c.StatusApproved != StatusApproved.Approved).ToList();
                                if (dt.Count > 0)
                                    vmListTempGroup.Add(new VmGroupingChekclist { GroupIndex = item.GroupIndex, IsAllApproved = false });
                                else vmListTempGroup.Add(new VmGroupingChekclist { GroupIndex = item.GroupIndex, IsAllApproved = true });

                            }
                        }
                    }
                }

                var result = vmListTempGroup.Where(c => c.IsAllApproved == false).ToList();
                if (result.Count == 0) // sudah approved semua groupnya..
                {
                    //update table User APproval...
                    var dataUserApp = _repoManageUserApproval.Get(conData.TrUserApprovalId).Result;
                    dataUserApp.CreatedBy = empId;
                    dataUserApp.CreatedOn = DateTime.Now;
                    dataUserApp.StatusApproved = StatusApproved.Approved;
                    _repoManageUserApproval.EditAsync(dataUserApp).ConfigureAwait(false);

                    //baca list user approval nya, masih ada atau nggak ada...
                    var dataUserConCurrentApproval = listConcurrentUser.Where(x => x.StatusApproved == StatusApproved.CurrentApproval || x.StatusApproved == StatusApproved.WaitingApproval).ToList();
                    if (dataUserConCurrentApproval.Count == 0)
                    {
                        var dataApp = _repo.Get(dataUserApp.TrTempApprovalId).Result;
                        dataApp.StatusApproved = StatusApproved.Approved;
                        dataApp.RejectedBy = empId;
                        dataApp.RejectedOn = DateTime.Now;
                        _repo.EditAsync(dataApp).ConfigureAwait(false);
                    }
                    else
                    {
                        var nextIndex = dataUserApp.IndexUser + 1;
                        _repoManageUserApproval.UpdateNextApproval(dataUserApp.TrTempApprovalId, Convert.ToInt32(nextIndex)).ConfigureAwait(false);
                        var dataConcurrent = _repoManageUserApproval.GetCurrentApproval(dataUserApp.TrTempApprovalId).Result;
                        if (dataConcurrent != null)
                            _repoManageConditionApproval.UpdateNextApproval(dataConcurrent.Id).ConfigureAwait(false);
                    }
                }

            }

        }

        #endregion
        public class VmListGroupCondition
        {
            public string Id { get; set; }
            public string EmpId { get; set; }
            public int IndexGroup { get; set; }
            public string GroupConditionalApproval { get; set; }
            public StatusApproved StatusApproved { get; set; }
        }

        public class VmGrouping
        {
            public int GroupIndex { get; set; }
        }
        public class VmGroupingChekclist
        {
            public int GroupIndex { get; set; }
            public bool IsAllApproved { get; set; }
        }

    }

}