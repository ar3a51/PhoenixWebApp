﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Shared.Core.Areas.ProjectManagement.Dtos
{
    public class TimeSheetDto
    {
        public string id { get; set; }
        public string job_id { get; set; }
        public string company_id { get; set; }
        public string company_name { get; set; }
        public string job_name { get; set; }
        public string non_job_name { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }
        public DateTime Sunday { get; set; }
        public DateTime Monday { get; set; }
        public DateTime Tuesday { get; set; }
        public DateTime Wednesday { get; set; }
        public DateTime Thursday { get; set; }
        public DateTime Friday { get; set; }
        public DateTime Saturday { get; set; }
    }
}
