using Phoenix.Shared.Core.Areas.ProjectManagement.Dtos;
using Phoenix.Shared.Core.Entities;
using Phoenix.Shared.Core.Parameters;
using Phoenix.Shared.Core.Repositories;
using Phoenix.Shared.Core.Services;
using Phoenix.Shared.Helpers;
using Phoenix.Shared.Responses;
using System;
using System.Linq;


namespace Phoenix.Shared.Core.Areas.ProjectManagement.Services
{
    public partial interface IDamServices : IBaseService<DamDto, Dam, string>
    {
        GeneralResponse<DamJob> GetListByJob(string job_id);
        GeneralResponseList<DamTypeStatusList> GetDamTypelist();
    }
    public partial class DamServices : BaseService<DamDto, Dam, string>, IDamServices
    {
        CommonHelper _commonHelper = new CommonHelper();
        public DamServices(IEFRepository<Dam, string> repo) : base(repo)
        {
            _repo = repo;
        }
        public GeneralResponseList<DamTypeStatusList> GetDamTypelist()
        {
            GeneralResponseList<DamTypeStatusList> resp = new GeneralResponseList<DamTypeStatusList>();
            resp.Success = false;
            try
            {
                IQueryable<DamTypeStatusList> que = from tb1 in _repo.GetContext().Set<JobStatus>()
                                                    join tb2 in _repo.GetContext().Set<DamTypeStatus>() on tb1.Id equals tb2.job_status_id
                                                    join tb3 in _repo.GetContext().Set<DamType>() on tb2.dam_type_id equals tb3.Id
                                                    select new DamTypeStatusList()
                                                    {
                                                        dam_type_id = tb2.dam_type_id,
                                                        dam_type_name = tb3.name,
                                                        job_status_id = tb1.Id,
                                                        job_status_name = tb1.name
                                                    };
                que = que.OrderBy(x => x.job_status_name).ThenBy(x => x.dam_type_name).Where(x => x.job_status_id != "closed");
                resp.Rows = que.ToList();

                resp.Success = true;


            }
            catch (Exception ex)
            {

                resp.Message = ex.Message;
            }
            return resp;
        }
        public GeneralResponse<DamJob> GetListByJob(string id)
        {
            GeneralResponse<DamJob> resp = new GeneralResponse<DamJob>();
            resp.Success = false;
            try
            {
                IQueryable<DamJob> que = from tb1 in _repo.GetContext().Set<JobDetail>()
                                         join tb2 in _repo.GetContext().Set<ClientBrief>() on tb1.client_brief_id equals tb2.Id
                                         join tb3 in _repo.GetContext().Set<CompanyBrand>() on tb1.brand_id equals tb3.Id
                                         join tb4 in _repo.GetContext().Set<Company>() on tb3.company_id equals tb4.Id
                                         join tb5 in _repo.GetContext().Set<JobStatus>() on tb1.job_status_id equals tb5.Id
                                         join tb6 in _repo.GetContext().Set<BusinessUnit>() on tb2.business_unit_id equals tb6.Id
                                         join tb7 in _repo.GetContext().Set<ApplicationUser>() on tb1.pm_id equals tb7.Id into lj1
                                         from pmuser in lj1.DefaultIfEmpty()
                                         join tb8 in _repo.GetContext().Set<vClientBriefList>() on tb2.Id equals tb8.Id
                                         where tb1.Id == id
                                         select new DamJob()
                                         {
                                             Id = tb1.Id,
                                             job_number = tb1.job_number,
                                             job_name = tb1.job_name,
                                             deadline = tb1.deadline,
                                             final_delivery = tb1.final_delivery,
                                             client_brief_id = tb1.client_brief_id,
                                             campaign_name = tb2.campaign_name,
                                             job_status_id = tb1.job_status_id,
                                             job_description = tb1.job_description,
                                             company_name = tb4.company_name,
                                             company_id = tb4.Id,
                                             brand_name = tb3.brand_name,
                                             brand_id = tb3.Id,
                                             status_name = tb5.name,
                                             business_unit_name = tb6.unit_name,
                                             pm_id = tb1.pm_id,
                                             pm = pmuser.app_fullname,
                                             DamFile = (from a in _repo.GetContext().Set<Dam>()
                                                        where a.job_detail_id == tb1.Id
                                                        select new DamNew()
                                                        {
                                                            dam_type_id = a.dam_type_id,
                                                            file_id = a.file_id,
                                                            job_detail_id = a.job_detail_id,
                                                            notes = a.notes,
                                                            sub_task_id = a.sub_task_id,
                                                            task_id = a.task_id
                                                        }).ToList(),
                                             Centralresource = (from a in _repo.GetContext().Set<CentralResource>()
                                                                join b in _repo.GetContext().Set<ClientBriefCentralResource>() on a.Id equals b.central_resource_id
                                                                where b.job_detail_id == tb1.Id
                                                                select new Centralresourcelist()
                                                                {
                                                                    Id = b.central_resource_id,
                                                                    name = a.name
                                                                }).ToList(),

                                         };
                resp.Data = que.FirstOrDefault();
                if (resp.Data != null)
                {
                    resp.Success = true;
                }

            }
            catch (Exception ex)
            {

                resp.Message = ex.Message;
            }
            return resp;

        }
    }
}

