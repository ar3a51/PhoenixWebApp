using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Phoenix.Shared.Core.Entities
{
    [Table("pe_file",Schema ="pm")]
    public partial class PeFile : GenericEntity<string>
    {
        public string file_name { get; set; }
        public string file_path { get; set; }
        public int? file_size { get; set; }
        public string file_type { get; set; }
        public int? flag { get; set; }
        public DateTime? upload_date { get; set; }
    }
}