using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Phoenix.Shared.Core.Entities
{
    [Table("education_level",Schema ="hr")]
    public partial class EducationLevel : GenericEntity<string>
    {
        public string education_name { get; set; }
    }
}