using Phoenix.Shared.Core.Areas.Hris.Dtos;
using Phoenix.Shared.Core.Entities;
using Phoenix.Shared.Core.Entities.Hris;
using Phoenix.Shared.Core.Parameters;
using Phoenix.Shared.Core.Repositories;
using Phoenix.Shared.Core.Services;
using Phoenix.Shared.Helpers;
using Phoenix.Shared.Responses;
using System;
using System.Linq;


namespace Phoenix.Shared.Core.Areas.Hris.Services
{
    public partial interface ICandidateRecruitmentServices : IBaseService<CandidateRecruitmentDto, CandidateRecruitment, string>
    {
        GeneralResponseList<CandidateRecruitmentDto> ListAll(CandidateRecruitmentSearch filter);
        GeneralResponse<CandidateRecruitment> CreateDetil(CandidateRecruitmentNew dto);
        GeneralResponse<CandidateRecruitment> DeleteDetil(CandidateRecruitmentDto dto);
        GeneralResponse<CandidateRecruitment> UpdateCustom(CandidateRecruitmentDto dto);
        GeneralResponse<CandidateRecruitment> CheckApprovalCustom(CandidateRecruitmentDto dto);
        GeneralResponse<CandidateRecruitment> UpdateCustomWithContract(CandidateRecruitmentDto dto);
    }
    public partial class CandidateRecruitmentServices : BaseService<CandidateRecruitmentDto, CandidateRecruitment, string>, ICandidateRecruitmentServices
    {
        IEFRepository<HiringApplicant, string> _repoHiringApplicant;
        IEFRepository<CandidateRecruitmentSignContract, string> _repoCandidateRecruitmentSignContract;
        IEFRepository<JobVacancy, string> _repoJobVacancy;
        public CandidateRecruitmentServices(IEFRepository<CandidateRecruitment, string> repo, IEFRepository<HiringApplicant, string> repoHiringApplicant, IEFRepository<CandidateRecruitmentSignContract, string> repoCandidateRecruitmentSignContract, IEFRepository<JobVacancy, string> repoJobVacancy) : base(repo)
        {
            _repo = repo;
            _repoHiringApplicant = repoHiringApplicant;
            _repoCandidateRecruitmentSignContract = repoCandidateRecruitmentSignContract;
            _repoJobVacancy = repoJobVacancy;
        }

        public GeneralResponse<CandidateRecruitment> CreateDetil(CandidateRecruitmentNew dto)
        {
            GeneralResponse<CandidateRecruitment> resp = new GeneralResponse<CandidateRecruitment>();
            try
            {
                Random rn = new Random();
                CandidateRecruitment candidate = new CandidateRecruitment();
                PropertyMapper.All(dto, candidate);
                
                candidate.last_education = "S1";
                
                candidate.applicant_progress_id= "0";
                candidate.applicant_move_stage_id = "1";
                candidate.code = "APL" + rn.Next(1, 99999);
                candidate.job_vacancy_id = dto.job_vacancy_id;


                _repo.Create(false, candidate);

                HiringApplicant newHiringAplicant = new HiringApplicant();
                newHiringAplicant.job_vacancy_id = dto.job_vacancy_id;
                newHiringAplicant.candidate_recruitment_id = candidate.Id;

                _repoHiringApplicant.Create(false, newHiringAplicant);
                


                _repo.SaveChanges();
                _repoHiringApplicant.SaveChanges();
                _repo.Dispose();
                _repoHiringApplicant.Dispose();
                resp.Success = true;
                resp.Message = "Sucess";
                resp.Code = "00";
                resp.Data = candidate;

            }
            catch (Exception x)
            {
                resp.Success = false;
                resp.Code = "500";
                resp.Message = x.Message.ToString();
            }

            return resp;
        }

        public GeneralResponse<CandidateRecruitment> DeleteDetil(CandidateRecruitmentDto dto)
        {
            GeneralResponse<CandidateRecruitment> resp = new GeneralResponse<CandidateRecruitment>();
            try
            {
              

                HiringApplicantDto ha = new HiringApplicantDto();
                PagingHelper q = new PagingHelper();
                IQueryable<HiringApplicantDto> que = from tb1 in _repo.GetContext().Set<HiringApplicant>()
                                                     where tb1.job_vacancy_id == dto.job_vacancy_id && tb1.candidate_recruitment_id == dto.Id
                                                          select new HiringApplicantDto()
                                                          {
                                                              Id = tb1.Id,
                                                              candidate_recruitment_id = tb1.candidate_recruitment_id,
                                                              hiring_request_id = tb1.candidate_recruitment_id
                                                          };
                ha = que.FirstOrDefault();
                HiringApplicant hanew = new HiringApplicant();
                hanew = _repoHiringApplicant.FindByID(ha.Id);

                _repo.RemoveByID(false, dto.Id);
                _repoHiringApplicant.RemoveByID(false,hanew.Id);


                _repo.SaveChanges();
                _repoHiringApplicant.SaveChanges();
                _repo.Dispose();
                _repoHiringApplicant.Dispose();
                resp.Success = true;
                resp.Message = "Sucess";
                resp.Code = "00";

            }
            catch (Exception x)
            {
                resp.Success = false;
                resp.Code = "500";
                resp.Message = x.Message.ToString();
            }

            return resp;
        }

        public GeneralResponseList<CandidateRecruitmentDto> ListAll(CandidateRecruitmentSearch filter)
        {

            GeneralResponseList<CandidateRecruitmentDto> resp = new GeneralResponseList<CandidateRecruitmentDto>();
            try
            {
                PagingHelper q = new PagingHelper();
                IQueryable<CandidateRecruitmentDto> que =   from thp in _repo.GetContext().Set<HiringApplicant>() join
                                                            tb1 in _repo.GetContext().Set<CandidateRecruitment>() on thp.candidate_recruitment_id equals tb1.Id
                          select new CandidateRecruitmentDto()
                          {
                              Id = tb1.Id,
                              code = tb1.code,
                              achievement = tb1.achievement,
                              address = tb1.address,
                              alcohol_willing = tb1.alcohol_willing,
                              applicant_progress_id = tb1.applicant_progress_id,
                              approver_id = tb1.approver_id,
                              birth_place = tb1.birth_place,
                              certificate = tb1.certificate,
                              cigaret_willing = tb1.cigaret_willing,
                              city_id = tb1.city_id,
                              company_last_address = tb1.company_last_address,
                              company_last_from = tb1.company_last_from,
                              company_last_name = tb1.company_last_name,
                              company_last_phone = tb1.company_last_phone,
                              company_last_until = tb1.company_last_until,
                              country = tb1.country,
                              course_institution = tb1.course_institution,
                              course_tittle = tb1.course_tittle,
                              course_years = tb1.course_years,
                              date_birth = tb1.date_birth,
                              education_level_id = tb1.education_level_id,
                              education_year_from = tb1.education_year_from,
                              education_year_to = tb1.education_year_to,
                              email = tb1.email,
                              file_id = tb1.file_id,
                              first_name = tb1.first_name,
                              gender = tb1.gender,
                              identity_card = tb1.identity_card,
                              institusion = tb1.institusion,
                              interest = tb1.interest,
                              job_repository_id = tb1.job_repository_id,
                              last_education = tb1.last_education,
                              last_name = tb1.last_name,
                              last_position = tb1.last_position,
                              looking_job = tb1.looking_job,
                              major = tb1.major,
                              married_status_id = tb1.married_status_id,
                              mobile_number = tb1.mobile_number,
                              nationality_id = tb1.nationality_id,
                              nick_name = tb1.nick_name,
                              pay_slip = tb1.pay_slip,
                              phone_number = tb1.phone_number,
                              portofolio = tb1.portofolio,
                              profile_file = tb1.profile_file,
                              province = tb1.province,
                              relation_people = tb1.relation_people,
                              religion_id = tb1.religion_id,
                              resume_screening = tb1.resume_screening,
                              source_recruitment = tb1.source_recruitment,
                              work_number = tb1.work_number,
                              zip_code = tb1.zip_code,
                              job_vacancy_id = thp.job_vacancy_id,
                              source = tb1.source,
                              year_experience = tb1.year_experience,
                              applicant_move_stage_id = tb1.applicant_move_stage_id
                          };
                
                resp.RecordsTotal = que.Count();

             
                que = q.filterEquals(que, "applicant_progress_id", filter.Search_applicant_progress_id);
                que = q.filterEquals(que, "job_vacancy_id", filter.Search_job_vacancy_id);
                que = q.filterEquals(que, "applicant_move_stage_id", filter.Search_applicant_move_stage_id); 




                resp.RecordsFiltered = que.Count();
                resp.Total = resp.RecordsTotal;

                que = q.sortir(que, "Id", filter.sortBy, filter.sortDir);
                que = q.limit(que, filter.GetLimit(), filter.GetOffset());
 
                resp.Rows = que.ToList();
                resp.Success = true;

            }
            catch (Exception ex)
            {
                resp.Success = false;
                resp.Message = ex.Message;
            }
            return resp;

        }

        public GeneralResponse<CandidateRecruitment> UpdateCustom(CandidateRecruitmentDto dto)
        {
            GeneralResponse<CandidateRecruitment> resp = new GeneralResponse<CandidateRecruitment>();
            try
            {

                CandidateRecruitment candidate = _repo.FindByID(dto.Id); 
                candidate.applicant_progress_id = dto.applicant_progress_id;
                candidate.applicant_move_stage_id = dto.applicant_move_stage_id;

                _repo.Update(candidate);

               
                resp.Success = true;
                resp.Message = "Sucess";
                resp.Code = "00";

            }
            catch (Exception x)
            {
                resp.Success = false;
                resp.Code = "500";
                resp.Message = x.Message.ToString();
            }

            return resp;
        }

        public GeneralResponse<CandidateRecruitment> CheckApprovalCustom(CandidateRecruitmentDto dto)
        {
            GeneralResponse<CandidateRecruitment> resp = new GeneralResponse<CandidateRecruitment>();
            try
            {
                IQueryable<TrTemplateApproval> que = from tb1 in _repo.GetContext().Set<TrTemplateApproval>()
                                                          where tb1.RefId == dto.Id
                                                          select new TrTemplateApproval
                                                          {
                                                              Id = tb1.Id,
                                                              RefId = tb1.RefId,
                                                              StatusApproved = tb1.StatusApproved
                                                          };

                TrTemplateApproval Header = que.FirstOrDefault();

                if (Header.StatusApproved.Equals("3")|| Header.StatusApproved ==3) {
                    CandidateRecruitment candidate = _repo.FindByID(dto.Id);
                    candidate.applicant_progress_id = "4";
                    candidate.applicant_move_stage_id = "6";

                    _repo.Update(candidate);
                }


                resp.Success = true;
                resp.Message = "Sucess";
                resp.Code = "00";

            }
            catch (Exception x)
            {
                resp.Success = false;
                resp.Code = "500";
                resp.Message = x.Message.ToString();
            }

            return resp;
        }

        public GeneralResponse<CandidateRecruitment> UpdateCustomWithContract(CandidateRecruitmentDto dto)
        {
            GeneralResponse<CandidateRecruitment> resp = new GeneralResponse<CandidateRecruitment>();
            try
            {

                CandidateRecruitment candidate = _repo.FindByID(dto.Id);
                candidate.applicant_progress_id = dto.applicant_progress_id;
                candidate.applicant_move_stage_id = dto.applicant_move_stage_id;
                

                _repo.Update(false,candidate);

                CandidateRecruitmentSignContract candidatesigncontract = new CandidateRecruitmentSignContract();
                candidatesigncontract.candidate_recruitment_id = candidate.Id;
                candidatesigncontract.filemaster_id = dto.filemaster_id;
                candidatesigncontract.remarks = dto.remarks;
                candidatesigncontract.start_date = dto.start_date;
                candidatesigncontract.end_date = dto.end_date;

                _repoCandidateRecruitmentSignContract.Create(false, candidatesigncontract);

                JobVacancy vacancy = _repoJobVacancy.FindByID(candidate.job_vacancy_id);
                vacancy.posting_status = "5";
                _repoJobVacancy.Update(false, vacancy);


                _repo.SaveChanges();
                _repoCandidateRecruitmentSignContract.SaveChanges();
                _repoJobVacancy.SaveChanges();
                _repo.Dispose();
                _repoCandidateRecruitmentSignContract.Dispose();
                _repoJobVacancy.Dispose();
                
                resp.Success = true;
                resp.Message = "Sucess";
                resp.Code = "00";

            }
            catch (Exception x)
            {
                resp.Success = false;
                resp.Code = "500";
                resp.Message = x.Message.ToString();
            }

            return resp;
        }
    }
}

