using Microsoft.AspNetCore.Mvc;
using Phoenix.Shared.Core.Areas.Hris.Services;
using Phoenix.Shared.Constants;
using Phoenix.Shared.Core.Filters;
using Phoenix.Shared.Responses;
using Phoenix.Shared.Core.Entities;
using Phoenix.Shared.Core.Areas.Hris.Dtos;
using Phoenix.Shared.Helpers;

namespace Phoenix.Api.Areas.Hris.Controllers
{
    [Area(PhoenixModule.HRIS)]
    [Route(PhoenixModule.HRIS_ROUTE_API)]
    [JwtAuthentication]
    [ApiExplorerSettings(GroupName = PhoenixModule.HRIS)]
    public class CandidateRecruitmentController : Controller
    {
        private ICandidateRecruitmentServices _service;

        public CandidateRecruitmentController(ICandidateRecruitmentServices service)
        {
            _service = service;
        }
        [HttpGet]
        public GeneralResponseList<CandidateRecruitmentDto> List(CandidateRecruitmentSearch parameter)
        {
            return _service.ListAll(parameter);
        }
        [HttpGet("{id}")]
        public GeneralResponse<CandidateRecruitmentDto> Get(string id)
        {
            return _service.GetByIDResponseDto(id);
        }
        [HttpPost]
        [ValidateModel]
        public GeneralResponse<CandidateRecruitment> Create(CandidateRecruitmentNew dto)
        {
            CandidateRecruitmentDto data = new CandidateRecruitmentDto();
            PropertyMapper.All(dto, data);        
            return _service.CreateResponse(data);
        }
        [HttpPost]
        [ValidateModel]
        public GeneralResponse<CandidateRecruitment> CreateDetil(CandidateRecruitmentNew dto)
        {      
            return _service.CreateDetil(dto);
        }

        [HttpPost, ValidateModel]
        public GeneralResponse<CandidateRecruitment> Update(CandidateRecruitmentDto dto)
        {
            return _service.UpdateResponse(dto.Id, dto);
        }
        [HttpPost, ValidateModel]
        public GeneralResponse<CandidateRecruitment> UpdateCustom([FromBody]CandidateRecruitmentDto dto)
        {
            return _service.UpdateCustom(dto);
        }
        [HttpPost, ValidateModel]
        public GeneralResponse<CandidateRecruitment> CheckApprovalCustom([FromBody]CandidateRecruitmentDto dto)
        {
            return _service.CheckApprovalCustom(dto);
        }
        [HttpPost, ValidateModel]
        public GeneralResponse<CandidateRecruitment> UpdateCustomWithContract([FromBody]CandidateRecruitmentDto dto)
        {
            return _service.UpdateCustomWithContract(dto);
        }
        [HttpDelete("{id}")]
        public GeneralResponse<CandidateRecruitment> Delete(string id)
        {
            return _service.DeleteByIDResponse(id);
        }
        [HttpPost]
        public GeneralResponse<CandidateRecruitment> DeleteDetil([FromBody]CandidateRecruitmentDto dto)
        {
            return _service.DeleteDetil(dto);
        }

    }
}