﻿using Microsoft.AspNetCore.Http;


namespace Phoenix.Shared.Core.Areas.General.Dtos
{
    public class DamfileDto
    {

    }
    public class DamUpload
    {
        public string name { get; set; }
        public string tags { get; set; }
        public IFormFile file { get; set; }
    }
    public class FileMasterDto
    {
        public string name { get; set; }
        public string tags { get; set; }
        public string id { get; set; }
    }
}
