﻿using Microsoft.AspNetCore.Mvc;
using Phoenix.Shared.Core.Areas.General.Services;
using Phoenix.Shared.Constants;
using Phoenix.Shared.Core.Filters;
using Phoenix.Shared.Responses;
using System.Threading.Tasks;
using Phoenix.Shared.Core.Areas.General.Dtos;
using System.Net.Http;
using System.Net;
using System.IO;
using System;
using Phoenix.Shared.Core.Entities;

namespace Phoenix.Api.Areas.General.Controllers
{
    [Area(PhoenixModule.GENERAL)]
    [Route(PhoenixModule.GENERAL_ROUTE_API)]
    [JwtAuthentication]
    [ApiExplorerSettings(GroupName = PhoenixModule.GENERAL)]
    public class FileController : Controller
    {

        private IFileServices _service;
        private readonly Phoenix.Data.Services.Um.IManageAzurePortalSetupService _repoAzure;
        public FileController(IFileServices service, Phoenix.Data.Services.Um.IManageAzurePortalSetupService repoAzure)
        {
            _service = service;
            _repoAzure = repoAzure;
        }
        [HttpPost]
        public async Task<GeneralResponse<FileMasterDto>>  Upload(DamUpload files)
        {
             return await _service.Upload(files);
        }

        [HttpPost]
        public async Task<GeneralResponse<FileMasterDto>> UploadOneDrive(DamUpload files)
        {
            var model = _repoAzure.GetOnlyOne().Result;
            return await _service.UploadOneDrive(files,model);
        }

        [HttpGet]
        public async Task<IActionResult> getfile(Guid id)
        {
            Filemaster filemaster = _service.getfile(id);
            if (filemaster == null)
                return Content("filename not present");

            var path = filemaster.name;

            var memory = new MemoryStream();
            using (var stream = new FileStream(path, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, filemaster.mimetype, Path.GetFileName(path));
        }
    }
}