﻿using Microsoft.AspNetCore.SignalR;
using Phoenix.Shared.Core.Areas.General.Dtos;
using Phoenix.Shared.Core.Areas.General.Services;
using Phoenix.Shared.Core.Entities;
using Phoenix.Shared.Core.Repositories;
using server.Hubs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Shared.Core.Areas.General.Helpers
{
    public class Notify
    {
        protected IHubContext<NotificationHub> _hubContext;
        IEFRepository<Notification, string> _repo;
        IEFRepository<NotificationType, string> _repoType;
        protected NotificationHub _notif = new NotificationHub();

        private string noticode = "ntfks_";

        public Notify(IHubContext<NotificationHub> hubContext,
            IEFRepository<Notification, string> repo, IEFRepository<NotificationType, string> repoType)
        {
            _hubContext = hubContext;
            _repo = repo;
            _repoType = repoType;

        }
        public void user(string user_id, string item_id, string msg)
        {
            Notification data = new Notification();
            data.user_id = user_id;
            data.notification_type_id = msg;
            data.item_id = item_id;
            _repo.Create(data);
            _hubContext.Clients.Group(noticode + user_id).SendAsync("ReceiveMessage", msg);
        }


        public void createNotification(string notifUser, string notifItemId, string notifModule, string notifTypeModule, string notifItemName)
        {
            Notification _notification = new Notification();
            _notification.Id = Guid.NewGuid().ToString().ToUpper();
            _notification.notification_type_id = Guid.NewGuid().ToString().ToUpper();
            _notification.user_id = notifUser;
            _notification.item_id = notifItemId;
            _notification.module = notifModule;
            _notification.is_active = true;
            _repo.Create(_notification);

            //var _notifType = _repoType.FindByID(_notification.notification_type_id);
            //if (_notifType == null) {
            NotificationType _notifType = new NotificationType();
            _notifType.Id = _notification.notification_type_id;
            _notifType.is_active = true;
            _notifType.shorttext = shortextProjectManagementNotification(notifTypeModule, notifItemName);
            _notifType.longtext = longtextProjectManagementNotification(notifTypeModule);
            _notifType.href = urlProjectManagementNotification(notifTypeModule, notifItemId);
            _repoType.Create(_notifType);

            _hubContext.Clients.Group(noticode + notifUser).SendAsync("ReceiveMessage", _notifType.href);
        }

        public string urlProjectManagementNotification(string type, string id)
        {
            string ret = "";
            switch (type)
            {
                case "jobdetail":
                    ret = String.Format("/projectmanagement/job/detail/{0}", id);
                    break;
                case "jobassignmember":
                    ret = String.Format("/projectmanagement/job/detail/{0}", id);
                    break;
                case "kanban":
                    ret = String.Format("/projectmanagement/job/kanban/{0}", id);
                    break;
                default: break;

            }
            return ret;
        }

        public string shortextProjectManagementNotification(string type, string ItemName)
        {
            string ret = "";
            switch (type)
            {
                case "jobdetail":
                    ret = "Assign Job - " + ItemName;
                    break;
                case "jobassignmember":
                    ret = "Assign PM & Member - " + ItemName;
                    break;
                case "kanban":
                    ret = "Approved All Task - " + ItemName;
                    break;
                default: break;

            }
            return ret;
        }

        public string longtextProjectManagementNotification(string type)
        {
            string ret = "";
            switch (type)
            {
                case "jobdetail":
                    ret = "New Job has been assign to you";
                    break;
                case "jobassignmember":
                    ret = "Job has been assign to PM";
                    break;
                case "kanban":
                    ret = "Task already approved all";
                    break;
                default: break;

            }
            return ret;
        }

    }
}
