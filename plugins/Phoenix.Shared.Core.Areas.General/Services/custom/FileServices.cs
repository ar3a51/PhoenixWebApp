﻿using Phoenix.Shared.Core.Areas.General.Dtos;
using System;
using System.Linq;
using Microsoft.Extensions.Options;
using Phoenix.Shared.Helpers;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using System.IO;
using Phoenix.Shared.Responses;
using Phoenix.Shared.Core.Repositories;
using Phoenix.Shared.Core.Entities;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Phoenix.Data.MsGraphExtention;

namespace Phoenix.Shared.Core.Areas.General.Services
{
    public partial interface IFileServices
    {
        Task<GeneralResponse<FileMasterDto>> Upload(DamUpload files);
        Task<GeneralResponse<FileMasterDto>> UploadOneDrive(DamUpload files, Phoenix.Data.Models.Um.AzurePortalSetup modelAzure);
        Filemaster getfile(Guid id);
    }
    public partial class FileServices : IFileServices
    {

        private readonly FileConfig _fileconfig;
        private IEFRepository<Filemaster, Guid> _repo;
        const int twidth = 150;
        const int theight = 150;
        public FileServices(IOptions<FileConfig> settings,IEFRepository<Filemaster, Guid> repo) 
        {
            _fileconfig = settings.Value;
            _repo = repo;
        }
        private bool ThumbnailCallback()
        {
            return false;
        }
        //private void createthumbnail(string inputPath)
        //{
        //    using (var ResourceImage = new Bitmap(System.Drawing.Image.FromFile(inputPath)))
        //    {
        //        Image ReducedImage;

        //        Image.GetThumbnailImageAbort callb = new Image.GetThumbnailImageAbort(ThumbnailCallback);

        //        ReducedImage = ResourceImage.GetThumbnailImage(twidth, theight, callb, IntPtr.Zero);
        //        ReducedImage.Save("E:\\temp\\filephoenix\\thumb.jpg");
        //    }

        //}
        public Filemaster getfile(Guid id)
        {
            return _repo.FindByID(id);
        }
        public async Task<GeneralResponse<FileMasterDto>> Upload(DamUpload files)
        {
            GeneralResponse<FileMasterDto> response = new GeneralResponse<FileMasterDto>();
            long size = files.file.Length;

            // full path to file in temp location
            var filePath = _fileconfig.Path;
            try
            {


            
            string fullPath = filePath + files.file.FileName;
            string fileNameOnly = Path.GetFileNameWithoutExtension(fullPath);
            string extension = Path.GetExtension(fullPath);
            string newFullPath = fullPath;
            string newfilename= files.file.FileName;
            string mime = MimeTypes.GetMimeType(newfilename);
                int count = 0;
            while (File.Exists(newFullPath))
            {
                string tempFileName = string.Format("{0}({1})", fileNameOnly, count++);
                newfilename = tempFileName + extension;
                newFullPath = Path.Combine(filePath, tempFileName + extension);
            }

            Filemaster filemaster = new Filemaster();
            filemaster.filepath = filePath;
                filemaster.mimetype = mime;
            filemaster.size = size;
            filemaster.tags = files.tags;
            filemaster.name = newfilename;
            _repo.Create(false, filemaster);


                if (size  > 0)
                {
                    using (var stream = new FileStream(newFullPath, FileMode.Create))
                    {
                         await files.file.CopyToAsync(stream);
                    }
            }
                //createthumbnail(newFullPath);

                _repo.SaveChanges();
                _repo.Dispose();

                response.Data = new FileMasterDto();
                response.Data.id = filemaster.Id.ToString();
                response.Data.name = filemaster.name;
                response.Data.tags = filemaster.tags;
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Success = false;
            }

            ;
            return response;
        }

        public async Task<GeneralResponse<FileMasterDto>> UploadOneDrive(DamUpload files, Phoenix.Data.Models.Um.AzurePortalSetup modelAzure)
        {
            GeneralResponse<FileMasterDto> response = new GeneralResponse<FileMasterDto>();
            long size = files.file.Length;

            // full path to file in temp location
            var filePath = _fileconfig.Path;
            try
            {
                string fileNameOnly = Path.GetFileNameWithoutExtension(files.file.FileName);
                string extension = Path.GetExtension(files.file.FileName);
                string pathFile = "/PROJECTMANAGEMENT/" + fileNameOnly + extension;

                Filemaster filemaster = new Filemaster();
                filemaster.tags = files.tags;
                filemaster.name = files.file.FileName;
                filemaster.size = files.file.Length;
                filemaster.filepath = filePath;
                filemaster.mimetype = GetMimeTypeByWindowsRegistry(filemaster.name);
                var streamFile = files.file.OpenReadStream();
                var graph = new MsGraph(modelAzure.ClientId,modelAzure.TenantId);
                var authResult = await graph.GetAuthentication(modelAzure.EmailAccount, modelAzure.SecurityPword);
                var graphClient = graph.CreateGraphServiceClient(authResult.AccessToken);
                
                var sizes = (streamFile.Length / 1024) / 1024;
                if (sizes > 3)
                {
                    var item = await graphClient.UploadLargeFile(streamFile, pathFile, false);
                }
                else
                {
                    var item = await graphClient.UploadSmallFile(streamFile, pathFile , false);
                }

                _repo.Create(false, filemaster);
                _repo.SaveChanges();
                _repo.Dispose();


                response.Data = new FileMasterDto();
                response.Data.id = filemaster.Id.ToString();
                response.Data.name = filemaster.name;
                response.Data.tags = filemaster.tags;
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Success = false;
            }
            return response;
        }

        private string GetMimeTypeByWindowsRegistry(string fileNameOrExtension)
        {
            string mimeType = "application/unknown";
            string ext = (fileNameOrExtension.Contains(".")) ? Path.GetExtension(fileNameOrExtension).ToLower() : "." + fileNameOrExtension;
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (regKey != null && regKey.GetValue("Content Type") != null) mimeType = regKey.GetValue("Content Type").ToString();
            return mimeType;
        }
    }
}
