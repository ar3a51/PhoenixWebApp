﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Phoenix.Data.Attributes;
using Phoenix.Data.Models.Finance.Transaction.InvoiceReceivable;
using Phoenix.Data.RestApi;
using Phoenix.Data.Services.Finance.Transaction;

namespace PhoenixWebApi.Controllers.Finance
{
    [Produces("application/json")]
    [ApiExplorerSettings(IgnoreApi = false, GroupName = @"Finance")]
    [Route("api/[controller]")]
    [ApiController]
    public class InvoiceReceivedController : ControllerBase
    {
        readonly IInvoiceReceivedService service;
        readonly ITemporaryAccountPayableService serviceTAP;
        public InvoiceReceivedController(IInvoiceReceivedService service, ITemporaryAccountPayableService serviceTAP)
        {
            this.service = service;
            this.serviceTAP = serviceTAP;
        }
        [HttpGet]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> Get()
        {
            var result = await service.Get();
            return Ok(new ApiResponse(result));
        }
        [HttpGet("GetByStatus/{status}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetByStatus(string status)
        {
            var result = await service.GetByStatus(status);
            return Ok(new ApiResponse(result));
        }
        
        [HttpGet("GetVendorByStatus/{status}/{vendorId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetVendorByStatus(string status,string vendorId)
        {
          
            var result = await service.GetVendorByStatus(status,vendorId);
            return Ok(new ApiResponse(result));
        }
        [HttpGet("GetWithPoDetail/{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetWithPoDetail(string Id)
        {
            var result = await service.GetWithPoDetail(Id);
            return Ok(new ApiResponse(result));
        }
        [HttpGet("{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> Get(string Id)
        {
            var result = await service.Get(Id);
            return Ok(new ApiResponse(result));
        }
        [HttpGet("getClient/{clientId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetClient(string clientId)
        {
            var result = await service.GetClient(clientId);
            return Ok(new ApiResponse(result));
        }
        [HttpGet("getVendor/{vendorId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetVendor(string vendorId)
        {
            var result = await service.GetVendor(vendorId);
            return Ok(new ApiResponse(result));
        }
        [HttpGet("getPo/{poId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetPo(string poId)
        {
            var result = await service.GetPo(poId);
            return Ok(new ApiResponse(result));
        }
        [HttpGet("getHistory/{poId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetHistory(string poId)
        {
            var result = await service.GetHistory(poId);
            return Ok(new ApiResponse(result));
        }
        [HttpGet("getInvoiceVendor/{poId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetInvoiceVendor(string poId)
        {
            var result = await service.GetInvoiceVendorList(poId);
            return Ok(new ApiResponse(result));
        }
        [HttpGet("GetDetailById/{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetDetailById(string Id)
        {
            var result = await service.GetDetailById(Id);
            return Ok(new ApiResponse(result));
        }
        [HttpPost]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> Post([FromBody]InvoiceReceiveableDTO model)
        {
            var Header = new InvoiceReceived();
            Header = model.Header;
            var Details = new List<InvoiceReceivedDetail>();
            Details = model.Details;
            await service.AddWithDetail(Header, Details);
            return Ok(new ApiResponse(true, Messages.SuccessAdd));
        }

        [HttpPut]
        [ActionRole(ActionMethod.Edit)]
        public async Task<IActionResult> Put([FromBody]InvoiceReceiveableDTO model)
        {
            var Header = new InvoiceReceived();
            Header = model.Header;
            var Details = new List<InvoiceReceivedDetail>();
            Details = model.Details;
            await service.EditWithDetail(Header,Details);
            //if (model.Header.InvoiceStatusId == Phoenix.Data.Models.InvoiceReceivedStatus.TAXTAP)
            //{
            //    await serviceTAP.AddWithDetailSP(model.Header.Id);
            //}
            return Ok(new ApiResponse(true, Messages.SuccessEdit));
        }
        [HttpDelete("{id}")]
        [ActionRole(ActionMethod.Delete)]
        public async Task<IActionResult> Delete(string id)
        {
            var model = await service.Get(id);
            await service.DeleteSoftAsync(model);
            return Ok(new ApiResponse(true, Messages.SuccessDelete));
        }

        [HttpGet("GetByFilterCustom/{status}/{businessUnit}/{legalEntity}/{invoiceNumber}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetByFilterCustom(string status, string businessUnit, string legalEntity,string invoiceNumber)
        {
            var result = await service.GetByFilterCustom(status, businessUnit, legalEntity, invoiceNumber);
            return Ok(new ApiResponse(result));
        }
    }
}