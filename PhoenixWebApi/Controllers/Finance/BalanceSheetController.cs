﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Phoenix.Data;
using Phoenix.Data.Attributes;
using Phoenix.Data.Models;
using Phoenix.Data.RestApi;

namespace PhoenixWebApi.Controllers.Finance
{
    [Produces("application/json")]
    [Route("api/balancesheet")]
    public class BalanceSheetController : ControllerBase
    {
        readonly IBalanceSheetService service;
        public BalanceSheetController(IBalanceSheetService service)
        {
            this.service = service;
        }

        [HttpPost("GetData")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetData([FromBody] TrialBalanceFilter Filter)
        {
            var result = await service.Get(Filter);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("GetFiscalYearsById/{Id}")]
        public async Task<IActionResult> GetTransactionID(string Id)
        {
            var result = await service.GetFiscalYears(Id);
            return Ok(new ApiResponse(result));
        }
    }
}