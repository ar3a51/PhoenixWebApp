﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.Data.Attributes;
using Phoenix.Data.Models;
using Phoenix.Data.RestApi;
using Phoenix.Data.Services.Finance.Transaction;

namespace PhoenixWebApi.Controllers.Finance
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class AccountPayableController : ControllerBase
    {
        readonly IAccountPayableService service;
        public AccountPayableController(IAccountPayableService service)
        {
            this.service = service;
        }
        [HttpGet]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> Get()
        {
            var result = await service.Get();
            return Ok(new ApiResponse(result));
        }
        [HttpGet("{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> Get(string Id)
        {
            var result = await service.Get(Id);
            return Ok(new ApiResponse(result));
        }
        [HttpGet("getJoin")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetJoin()
        {
            var result = await service.GetJoin();
            return Ok(new ApiResponse(result));
        }
        [HttpGet("getAccountPayable/{referenceId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetAccountPayableData(string referenceId)
        {
            var result = await service.GetAccountPayableData(referenceId);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("GetAccountPayableByAccountNumber/{AccountNumber}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetAccountPayableByAccountNumber(string AccountNumber)
        {
            var result = await service.GetAccountPayableByAccountNumber(AccountNumber);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("GetJoinCustom/{status}/{businessUnit}/{legalEntity}/{ApNumber}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetJoinCustom(string status, string businessUnit, string legalEntity, string ApNumber)
        {
            var result = await service.GetJoinCustom(status, businessUnit, legalEntity, ApNumber);
            return Ok(new ApiResponse(result));
        }

        [HttpPost]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> Post(AccountPayable model)
        {

            try
            {
                await service.InsertAccountPaybleDTO(model);
                return Ok(new ApiResponse(model, true, Messages.SuccessAdd));
            }
            catch (Exception ex) {
                return Ok(new ApiResponse(false, ex.Message.ToString()));
            }
            
        }
    }
}