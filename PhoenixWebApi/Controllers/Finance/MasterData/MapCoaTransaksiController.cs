using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Phoenix.Data;
using Phoenix.Data.Attributes;
using Phoenix.Data.Models;
using Phoenix.Data.RestApi;

namespace PhoenixWebApi.Controllers.Finance
{
    /// <summary>
    /// MapCoaTransaksi API Endpoint.
    /// </summary>
    [Produces("application/json")]
    [Route("api/mapcoatransaksi")]
    public class MapCoaTransaksiController : Controller
    {
        readonly IMapCoaTransaksiService service;

        /// <summary>
        /// And endpoint to manage MapCoaTransaksi
        /// </summary>
        /// <param name="context">Database context</param>
        public MapCoaTransaksiController(IMapCoaTransaksiService service)
        {
            this.service = service;
        }

        /// <summary>
        /// Creates new record.
        /// </summary>
        /// <param name="model">Object parameter to post in json format.</param>
        /// <returns>Json message if the process is successfully done or failed.</returns>
        [HttpPost]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> Post([FromBody]List<MapCoaTransaksi> model)
        {
            await service.AddAsync(model);
            return Ok(new ApiResponse(true, Messages.SuccessAdd));
        }

        /// <summary>
        /// Deletes specific record based on given id parameter.
        /// </summary>
        /// <param name="id">Key id parameter.</param>
        /// <returns>Json message if the process is successfully done or failed.</returns>
        [HttpDelete("{mainServiceCategoryId}/{shareServiceId}")]
        [ActionRole(ActionMethod.Delete)]
        public async Task<IActionResult> Delete(string mainServiceCategoryId, string shareServiceId)
        {
            await service.DeleteSoftAsync(mainServiceCategoryId, shareServiceId);
            return Ok(new ApiResponse(true, Messages.SuccessDelete));
        }

        /// <summary>
        /// Gets all records without parameter as criteria.
        /// </summary>
        /// <returns>List of json object.</returns>
        [HttpGet("GetMasterClosing")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetMasterClosing()
        {
            var result = await service.GetMasterClosing();
            return Ok(new ApiResponse(result));
        }

        /// <summary>
        /// Gets all records without parameter as criteria.
        /// </summary>
        /// <returns>List of json object.</returns>
        [HttpGet("GetMasterClosingAP/{mainServiceCategoryId}/{shareServiceId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetMasterClosingAP(string mainServiceCategoryId, string shareServiceId)
        {
            var result = await service.GetMasterClosingAP(mainServiceCategoryId, shareServiceId);
            return Ok(new ApiResponse(result));
        }

        /// <summary>
        /// Gets all records without parameter as criteria.
        /// </summary>
        /// <returns>List of json object.</returns>
        [HttpGet("GetMasterClosingAR/{mainServiceCategoryId}/{shareServiceId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetMasterClosingAR(string mainServiceCategoryId, string shareServiceId)
        {
            var result = await service.GetMasterClosingAR(mainServiceCategoryId, shareServiceId);
            return Ok(new ApiResponse(result));
        }

        /// <summary>
        /// Gets all records without parameter as criteria.
        /// </summary>
        /// <returns>List of json object.</returns>
        [HttpGet("GetMasterAP")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetMasterAP()
        {
            var result = await service.GetMasterAP();
            return Ok(new ApiResponse(result));
        }

        /// <summary>
        /// Gets all records without parameter as criteria.
        /// </summary>
        /// <returns>List of json object.</returns>
        [HttpGet("GetMasterAPById/{mainServiceCategoryId}/{shareServiceId?}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetMasterAPById(string mainServiceCategoryId, string shareServiceId)
        {
            var result = await service.GetMasterAPById(mainServiceCategoryId, shareServiceId);
            return Ok(new ApiResponse(result));
        }

        /// <summary>
        /// Gets all records without parameter as criteria.
        /// </summary>
        /// <returns>List of json object.</returns>
        [HttpGet("GetMasterDetailAP/{mainServiceCategoryId}/{shareServiceId?}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetMasterDetailAP(string mainServiceCategoryId, string shareServiceId)
        {
            var result = await service.GetMasterDetailAP(mainServiceCategoryId, shareServiceId);
            return Ok(new ApiResponse(result));
        }

        /// <summary>
        /// Gets all records without parameter as criteria.
        /// </summary>
        /// <returns>List of json object.</returns>
        [HttpGet("GetMasterAR")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetMasterAR()
        {
            var result = await service.GetMasterAR();
            return Ok(new ApiResponse(result));
        }


        /// <summary>
        /// Gets all records without parameter as criteria.
        /// </summary>
        /// <returns>List of json object.</returns>
        [HttpGet("GetMasterARById/{mainServiceCategoryId}/{shareServiceId?}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetMasterAR(string mainServiceCategoryId, string shareServiceId)
        {
            var result = await service.GetMasterARById(mainServiceCategoryId, shareServiceId);
            return Ok(new ApiResponse(result));
        }

        /// <summary>
        /// Gets all records without parameter as criteria.
        /// </summary>
        /// <returns>List of json object.</returns>
        [HttpGet("GetMasterDetailAR/{mainServiceCategoryId}/{shareServiceId?}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetMasterDetailAR(string mainServiceCategoryId, string shareServiceId)
        {
            var result = await service.GetMasterDetailAR(mainServiceCategoryId, shareServiceId);
            return Ok(new ApiResponse(result));
        }
    }
}