﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Phoenix.Data;
using Phoenix.Data.Attributes;
using Phoenix.Data.Models;
using Phoenix.Data.Models.Finance.MasterData;
using Phoenix.Data.RestApi;
using Phoenix.Data.Services.Finance.MasterData;

namespace PhoenixWebApi.Controllers.Finance.MasterData
{
    /// <summary>
    /// MasterPettyCash API Endpoint.
    /// </summary>
    [Produces("application/json")]
    [Route("api/masterpettycash")]
    public class MasterPettyCashController : Controller
    {
        readonly IMasterPettyCashService service;

        [HttpGet("IsExistCoaIdMasterPetty/{coaid}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> IsExistCoaIdMasterPetty(string coaid)
        {
            var data = await service.IsExistCoaIdMasterPetty(coaid);
            return Ok(new ApiResponse(data, true));
        }

        [HttpGet("GetReffList")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetReffList()
        {
            var data = await service.GetReffList();
            return Ok(new ApiResponse(data, true));
        }

        [HttpGet("GetAffiliationFromBussinesUnit/{businessUnit}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetAffiliationFromBussinesUnit(string businessUnit)
        {
            var data = await service.GetAffiliationFromBussinesUnit(businessUnit);
            return Ok(new ApiResponse(data, true));
        }
        
        /// <summary>
        /// And endpoint to manage MasterPettyCash
        /// </summary>
        /// <param name="context">Database context</param>
        public MasterPettyCashController(IMasterPettyCashService service)
        {
            this.service = service;
        }

        /// <summary>
        /// Creates new record.
        /// </summary>
        /// <param name="model">Object parameter to post in json format.</param>
        /// <returns>Json message if the process is successfully done or failed.</returns>
        [HttpPost]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> Post([FromBody]MasterPettyCash model)
        {
            await service.AddAsync(model);
            return Ok(new ApiResponse(true, Messages.SuccessAdd));
        }

        /// <summary>
        /// Edits existing record based on primary key id.
        /// </summary>
        /// <param name="model">Object parameter to post in json format.</param>
        /// <returns>Json message if the process is successfully done or failed.</returns>
        [HttpPut]
        [ActionRole(ActionMethod.Edit)]
        public async Task<IActionResult> Put([FromBody]MasterPettyCash model)
        {
            await service.EditAsync(model);
            return Ok(new ApiResponse(true, Messages.SuccessEdit));
        }

        /// <summary>
        /// Deletes specific record based on given id parameter.
        /// </summary>
        /// <param name="id">Key id parameter.</param>
        /// <returns>Json message if the process is successfully done or failed.</returns>
        [HttpDelete("{Id}")]
        [ActionRole(ActionMethod.Delete)]
        public async Task<IActionResult> Delete(string Id)
        {
            var model = await service.Get(Id);
            await service.DeleteSoftAsync(model);
            return Ok(new ApiResponse(true, Messages.SuccessDelete));
        }

        /// <summary>
        /// Gets all records without parameter as criteria.
        /// </summary>
        /// <returns>List of json object.</returns>
        [HttpGet]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> Get()
        {
            var result = await service.Get();
            return Ok(new ApiResponse(result));
        }

        /// <summary>
        /// Gets single record based on specific primary key id.
        /// </summary>
        /// <param name="id">Key id parameter.</param>
        /// <returns>A json object with given id.</returns>
        [HttpGet("{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> Get(string Id)
        {
            var result = await service.Get(Id);
            return Ok(new ApiResponse(result));
        }
    }
}