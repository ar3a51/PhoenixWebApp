﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.Data;
using Phoenix.Data.Attributes;
using Phoenix.Data.Models;
using Phoenix.Data.Models.Finance;
using Phoenix.Data.Models.Finance.Transaction;
using Phoenix.Data.RestApi;
using Phoenix.Data.Services.Finance;
using Phoenix.Data.Services.Um;
using PhoenixWebApi.Services;

namespace PhoenixWebApi.Controllers.Finance
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class CashAdvanceController : ControllerBase
    {
        readonly ICashAdvanceService service;
        readonly INotificationCenter notify;
        readonly ITransactionUserApprovalService trUserApproval;
        /// <summary>
        /// And endpoint to manage CashAdvance
        /// </summary>
        /// <param name="context">Database context</param>
        public CashAdvanceController(ICashAdvanceService service, INotificationCenter notify, ITransactionUserApprovalService trUserApproval)
        {
            this.service = service;
            this.notify = notify;
            this.trUserApproval = trUserApproval;
        }

        /// <summary>
        /// Creates new record.
        /// </summary>
        /// <param name="model">Object parameter to post in json format.</param>
        /// <returns>Json message if the process is successfully done or failed.</returns>
        [HttpPost]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> Post([FromBody]CashAdvance model)
        {
            try
            {
                model.Id = Guid.NewGuid().ToString();
                await service.AddAsync(model);
                if (model.CaStatusId == StatusTransaction.WaitingApproval.ToString())
                {
                    var listUserApproval = await trUserApproval.GetAppIdFromReferenceId(model.Id);
                    foreach (var data in listUserApproval)
                    {
                        await Task.Run(() => notify.AddNotification(NotificationTypeCode.CashAdvanceApprovalNotification, data.AppIdUser, NotificationModuleType.ModuleFinance));
                    }
                }
          
                return Ok(new ApiResponse(true, Messages.SuccessAdd));
            }
            catch (Exception ex) {
                return Ok(new ApiResponse(false, ex.Message.ToString()));
            }
           
        }

        /// <summary>
        /// Creates new record.
        /// </summary>
        /// <param name="model">Object parameter to post in json format.</param>
        /// <returns>Json message if the process is successfully done or failed.</returns>
        //[HttpPost("AddSattlement")]
        //[ActionRole(ActionMethod.Add)]
        //public async Task<IActionResult> AddSattlement(CashAdvanceSattlementDTO model)
        //{
        //    try
        //    {
        //        await service.AddSattlement(model);              
        //        return Ok(new ApiResponse(model, true, Messages.SuccessAdd));
        //    }
        //    catch (Exception ex) {
        //        return Ok(new ApiResponse(false, ex.Message.ToString()));
        //    }
        //}

        /// <summary>
        /// Edits existing record based on primary key id.
        /// </summary>
        /// <param name="model">Object parameter to post in json format.</param>
        /// <returns>Json message if the process is successfully done or failed.</returns>
        [HttpPut]
        [ActionRole(ActionMethod.Edit)]
        public async Task<IActionResult> Put([FromBody]CashAdvance model)
        {
            await service.EditAsync(model);
            if (model.CaStatusId == StatusTransaction.WaitingApproval.ToString())
            {
                var listUserApproval = await trUserApproval.GetAppIdFromReferenceId(model.Id);
                foreach (var data in listUserApproval)
                {
                    await Task.Run(() => notify.AddNotification(NotificationTypeCode.CashAdvanceApprovalNotification, data.AppIdUser, NotificationModuleType.ModuleFinance));
                }
            }
            return Ok(new ApiResponse(true, Messages.SuccessEdit));
        }

        /// <summary>
        /// Edits existing record based on primary key id.
        /// </summary>
        /// <param name="model">Object parameter to post in json format.</param>
        /// <returns>Json message if the process is successfully done or failed.</returns>
        //[HttpPut("EditSattlement")]
        //[ActionRole(ActionMethod.Edit)]
        //public async Task<IActionResult> EditSattlement([FromBody]CashAdvanceSattlementDTO model)
        //{
        //    await service.EditSattlement(model);
        //    return Ok(new ApiResponse(model,true, Messages.SuccessEdit));
        //}

        [HttpPut("Approve")]
		[ActionRole(ActionMethod.Edit)]
		public async Task<IActionResult> Approve([FromBody]CashAdvance model)
		{
            try
            {
                await service.Approve(model);
                return Ok(new ApiResponse(model, true, Messages.SuccessApprove));
            }
            catch (Exception ex) {
                return Ok(new ApiResponse(false, ex.Message.ToString()));
            }
            
		}

        //[HttpPost("PostDataSattlement")]
        //[ActionRole(ActionMethod.Add)]
        //public async Task<IActionResult> PostDataSattlement(CashAdvanceSettlement model)
        //{
        //    try
        //    {
        //        await service.CreateAutomaticJournalFromSattlementId(model.Id);
        //        return Ok(new ApiResponse(model, true, Messages.SuccessAdd));
        //    }
        //    catch (Exception ex)
        //    {
        //        return Ok(new ApiResponse(false, ex.Message.ToString()));

        //    }
        //}

        //[HttpPut("ApproveSattlement")]
        //[ActionRole(ActionMethod.Edit)]
        //public async Task<IActionResult> ApproveSattlement([FromBody]CashAdvanceSettlement model)
        //{
        //    try
        //    {
        //        await service.ApproveSattlement(model);
        //        return Ok(new ApiResponse(model, true, Messages.SuccessApprove));
        //    }
        //    catch (Exception ex)
        //    {
        //        return Ok(new ApiResponse(false, ex.Message.ToString()));
        //    }

        //}


        /// <summary>
        /// Creates new record.
        /// </summary>
        /// <param name="model">Object parameter to post in json format.</param>
        /// <returns>Json message if the process is successfully done or failed.</returns>
        [HttpPut("Reject")]
		[ActionRole(ActionMethod.Edit)]
		public async Task<IActionResult> Reject([FromBody]CashAdvance model)
		{
            try
            {
                await service.Reject(model);
                return Ok(new ApiResponse(model, true, Messages.SuccessReject));
            }
            catch (Exception ex) {
                return Ok(new ApiResponse(false, ex.Message.ToString()));
            }
			
		}

		/// <summary>
		/// Deletes specific record based on given id parameter.
		/// </summary>
		/// <param name="id">Key id parameter.</param>
		/// <returns>Json message if the process is successfully done or failed.</returns>
		[HttpDelete("{Id}")]
        [ActionRole(ActionMethod.Delete)]
        public async Task<IActionResult> Delete(string Id)
        {
            var model = await service.Get(Id);
            await service.DeleteSoftAsync(model);

			return Ok(new ApiResponse(true, Messages.SuccessDelete));
        }

        /// <summary>
        /// Gets all records without parameter as criteria.
        /// </summary>
        /// <returns>List of json object.</returns>
        [HttpGet]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> Get([FromQuery]string legal, [FromQuery]string svc)
        {
			Dictionary<string, string> filters = new Dictionary<string, string>();
			if (!string.IsNullOrEmpty(legal)){
				filters.Add("legal", legal);
			}
			if (!string.IsNullOrEmpty(svc))
			{
				filters.Add("svc", svc);
			}

			
            var result = await service.Get(filters);
			return Ok(new ApiResponse(result));
        }

		[HttpGet("GetDetails/{caId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetDetails(string caId)
		{
			var result = await service.GetDetails(caId);
			return Ok(new ApiResponse(result));
		}

        [HttpGet("GetByCaNumber/{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetByCaNumber(string Id)
        {
            var result = await service.GetByCaNumber(Id);
            return Ok(new ApiResponse(result));
        }
        
        //pindah ke settlement controller
        //[HttpGet("GetSattlementById/{CaId}")]
        //[ActionRole(ActionMethod.Read)]
        //public async Task<IActionResult> GetSattlementById(string CaId)
        //{
        //    var result = await service.GetSattlementById(CaId);
        //    return Ok(new ApiResponse(result));
        //}

        //pindah ke settlement controller
        //[HttpGet("GetJournalDataBySattlementId/{Id}")]
        //[ActionRole(ActionMethod.Read)]
        //public async Task<IActionResult> GetJournalDataBySattlementId(string Id)
        //{
        //    var result = await service.GetJournalDataBySattlementId(Id);
        //    return Ok(new ApiResponse(result));
        //}
        [HttpGet("GetJournalDataId/{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetJournalDataId(string Id)
        {
            var result = await service.GetJournalDataId(Id);
            return Ok(new ApiResponse(result));
        }

        //pindah ke settlement controller
        //[HttpGet("GetSattlementDetailById/{Id}")]
        //[ActionRole(ActionMethod.Read)]
        //public async Task<IActionResult> GetSattlementDetailById(string Id)
        //{
        //    var result = await service.GetSattlementDetailById(Id);
        //    return Ok(new ApiResponse(result));
        //}

        /// <summary>
        /// Gets single record based on specific primary key id.
        /// </summary>
        /// <param name="id">Key id parameter.</param>
        /// <returns>A json object with given id.</returns>
        [HttpGet("{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> Get(string Id)
        {
            var result = await service.Get(Id);
			return Ok(new ApiResponse(result));
        }

		[HttpGet("approvalList")]
		[ActionRole(ActionMethod.Read)]
		public async Task<IActionResult> GetApprovalList()
		{
			var result = await service.GetApprovalList();
			return Ok(new ApiResponse(result));
		}

        [HttpGet("GetJobDetail/{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetJobDetail(string Id)
        {
            var result = await service.GetJobDetail(Id);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("GetUom/{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetUom(string Id)
        {
            var result = await service.GetUom(Id);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("GetCompanyInfo/{companyid}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetCompanyInfo(string companyid)
        {
            var result = await service.GetCompanyInfo(companyid);
            return Ok(new ApiResponse(result, true));
        }
    }
}