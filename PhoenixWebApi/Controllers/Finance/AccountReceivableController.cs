﻿
using Phoenix.Data.RestApi;
using Microsoft.AspNetCore.Mvc;
using Phoenix.Data.Services.Finance.Transaction;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Phoenix.Data.Models.Finance.Transaction;
using Phoenix.Data.Attributes;

namespace PhoenixWebApi.Controllers.Finance
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class AccountReceivableController : ControllerBase
    {
        readonly IAccountReceiveableService service;
        public AccountReceivableController(IAccountReceiveableService service)
        {
            this.service = service;
        }

        [HttpGet("GetIndexList/{legalentity?}/{status?}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetIndexList(string legalentity, string status)
        {
            var result = await service.GetIndexList(legalentity, status);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("GetDetailAR/{invid}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetDetailAR(string invid)
        {
            var result = await service.GetDetailAR(invid);
            return Ok(new ApiResponse(result, true));
        }

        [HttpPost("CreateJournalAR")]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> CreateJournalAR([FromBody]InvoiceClient iv)
        {
            var result = await service.CreateJournalAR(iv);
            return Ok(new ApiResponse(result, true));
        }


        [HttpGet("{Id?}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> Get(string Id)
        {
            var result = await service.Get(Id);
            return Ok(new ApiResponse(result));
        }
        
        [HttpPost("CreateJournalMultipleAR")]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> CreateJournalMultipleAR([FromBody]InvoiceClient iv)
        {
            var result = await service.CreateJournalMultipleAR(iv);
            return Ok(new ApiResponse(result, true));
        }

        [HttpPost("SubmitAR")]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> SubmitAR([FromBody]InvoiceClient iv)
        {
            var result = await service.SubmitAR(iv);
            return Ok(new ApiResponse(result, true));
        }
        

        [HttpGet("GerListJournal/{arid?}/{multipceid?}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GerListJournal(string arid, string multipceid)
        {
            var result = await service.GerListJournal(arid, multipceid);
            return Ok(new ApiResponse(result, true));
        }

        [HttpGet("GerListJournalAR/{arid?}/{multipceid?}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GerListJournalAR(string arid, string multipceid)
        {
            var result = await service.GerListJournalAR(arid, multipceid);
            return Ok(new ApiResponse(result, true));
        }

        [HttpGet("GetInvoiceInfo/{invid}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetInvoiceInfo(string invid)
        {
            var result = await service.GetInvoiceInfo(invid);
            return Ok(new ApiResponse(result, true));
        }

        [HttpGet("ReadInvDtl/{invno?}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> ReadInvDtl(string invno)
        {
            var result = await service.ReadInvDtl(invno);
            return Ok(new ApiResponse(result, true));
        }

        [HttpGet("GetListStatement/{acct}/{legal}/{cl}/{ms}/{sd}/{ed}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetListStatement(string acct, string legal, string cl, string ms, string sd, string ed)
        {   
            var result = await service.GetListStatement(acct, legal, cl, ms, sd, ed);
            return Ok(new ApiResponse(result, true));
        }

        [HttpGet("GetListAging/{cl}/{legal}/{dv}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetListAging(string cl, string legal,string dv)
        {
            var result = await service.GetListAging(cl, legal, dv);
            return Ok(new ApiResponse(result, true));
        }

        [HttpGet("GetChartOfAccountTradeAndTradeInter")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetChartOfAccountTradeAndTradeInter()
        {   
            var result = await service.GetChartOfAccountTradeAndTradeInter();
            return Ok(new ApiResponse(result, true));
        }
    }
}
