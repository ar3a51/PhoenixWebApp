﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodeMarvel.Infrastructure.ModelShared;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.Data.Attributes;
using Phoenix.Data.Services.Finance.Report;
using Phoenix.Data.RestApi;

namespace PhoenixWebApi.Controllers.Finance
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class TimeSheetGridReportController : ControllerBase
    {
        private readonly ITImeSheetReportServices _iTImeSheetReportServices;
        public TimeSheetGridReportController(ITImeSheetReportServices tImeSheetReportServices)
        {
            this._iTImeSheetReportServices = tImeSheetReportServices;
        }

        [HttpGet("get_by_job")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> Get(string job_id)
        {
            var result = await _iTImeSheetReportServices.TimeSheetByJob(job_id);
            return Ok(new Phoenix.Data.RestApi.ApiResponse(result));
        }

        [HttpGet("get_by_client")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetByClient(string client_id)
        {
            var result = await _iTImeSheetReportServices.GetReportClient(client_id);
            return Ok(new Phoenix.Data.RestApi.ApiResponse(result));
        }
    }
}