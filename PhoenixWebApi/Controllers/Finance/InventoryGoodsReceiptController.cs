﻿using DataAccess.Dto.Custom.Finance;
using Microsoft.AspNetCore.Mvc;
using Phoenix.Data.Attributes;
using Phoenix.Data.Models.Finance.Transaction.InventoryGoodsReceipt;
using Phoenix.Data.RestApi;
using Phoenix.Data.Services.Finance.Transaction;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhoenixWebApi.Controllers.Finance
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class InventoryGoodsReceiptController: ControllerBase
    {
        readonly IInventoryGoodsReceiptService service;
       
        public InventoryGoodsReceiptController(IInventoryGoodsReceiptService service)
        {
            this.service = service;
        }

        [HttpGet]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> Get()
        {
            var result = await service.Get();
            return Ok(new ApiResponse(result));
        }

        [HttpGet("{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> Get(string Id)
        {
            var result = await service.Get(Id);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("getDetail/{grId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetDetail(string grId)
        {
            var result = await service.GetDetail(grId);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("getDetailHistory/{poId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetDetailHistory(string poId)
        {
            var result = await service.GetDetailHistory(poId);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("getPO/{poId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetPO(string poId)
        {
            var result = await service.GetPODTOById(poId);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("getPODetail/{poId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetPODetail(string poId)
        {
            var result = await service.GetPODetail(poId);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("GetPODData/{podId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetPODData(string podId)
        {
            var result = await service.GetPODData(podId);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("getDDLItemName/{poId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetDDLPODetail(string poId)
        {
            var result = await service.GetDDLPODetail(poId);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("cekDetail/{poId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> CekDetail(string poId)
        {
            var result = await service.CekDetail(poId);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("getInventoryDetail/{grId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetInventoryDetail(string grId)
        {
            var result = await service.GetInventoryDetail(grId);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("getFixedAsset/{grId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetFixedAsset(string grId)
        {
            var result = await service.GetFixedAsset(grId);
            return Ok(new ApiResponse(result));
        }

        [HttpPost]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> Post([FromBody]InventoryGoodsReceiptDTO model)
        {
            var Header = new InventoryGoodsReceipt();
            Header = model.Header;
            var Details = new List<InventoryGoodsReceiptDetail>();
            Details = model.Details;

            var result = await service.AddWithDetailAsync(Header,Details);

            return Ok(new ApiResponse(true, Messages.SuccessAdd));
        }

        [HttpPut]
        [ActionRole(ActionMethod.Edit)]
        public async Task<IActionResult> Put([FromBody]InventoryGoodsReceiptDTO model)
        {
            var Header = new InventoryGoodsReceipt();
            Header = model.Header;
            var Details = new List<InventoryGoodsReceiptDetail>();
            Details = model.Details;

            await service.EditWithDetailAsync(Header, Details);

            return Ok(new ApiResponse(true, Messages.SuccessEdit));
        }

        [HttpPut("submit_good_receipts")]
        [ActionRole(ActionMethod.Edit)]
        public async Task<IActionResult> SubmitGoodReceipts(string InventoryGoodReceiptId)
        {


           var result= await service.SubmitGoodReceipt(InventoryGoodReceiptId);

            return Ok(new ApiResponse(result,true));
        }

        [HttpGet("generate/{grId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> Generate(string grId)
        {
            var result = await service.Generate(grId);
            return Ok(new ApiResponse(result));
        }
    }
}
