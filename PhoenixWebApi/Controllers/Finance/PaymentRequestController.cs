﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.Data;
using Phoenix.Data.Attributes;
using Phoenix.Data.Models;
using Phoenix.Data.Models.Finance;
using Phoenix.Data.Models.Finance.Transaction.PaymentRequest;
using Phoenix.Data.RestApi;
using Phoenix.Data.Services.Finance;
using Phoenix.Data.Services.Um;
using PhoenixWebApi.Services;

namespace PhoenixWebApi.Controllers.Finance
{
    [Produces("application/json")]
    [ApiExplorerSettings(IgnoreApi = false, GroupName = @"Finance")]
    [Route("api/[controller]")]
    public class PaymentRequestController : ControllerBase
    {
        readonly IPaymentRequestService service;
        readonly INotificationCenter notify;
        readonly ITransactionUserApprovalService trUserApproval;
        readonly DataContext context;
        public PaymentRequestController(IPaymentRequestService service, INotificationCenter notify, ITransactionUserApprovalService trUserApproval, DataContext context)
        {
            this.service = service;
            this.notify = notify;
            this.trUserApproval = trUserApproval;
            this.context = context;
        }

        /// <summary>
        /// Creates new record.
        /// </summary>
        /// <param name="model">Object parameter to post in json format.</param>
        /// <returns>Json message if the process is successfully done or failed.</returns>
        [HttpPost]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> Post([FromBody]PaymentRequest model)
        {
            await service.AddAsync(model);
            return Ok(new ApiResponse(true, Messages.SuccessAdd));
        }

        /// <summary>
        /// Creates new record.
        /// </summary>
        /// <param name="model">Object parameter to post in json format.</param>
        /// <returns>Json message if the process is successfully done or failed.</returns>
        [HttpPost("AddWithDetail")]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> AddWithDetail([FromBody]PaymentRequestDTO model)
        {
            try
            {
                model.Header.TempHeaderId = model.Header.Id;
                model.Header.Id = Guid.NewGuid().ToString();
                model.Header.PaymentRequestNumber = await TransID.GetTransId(context, Code.PaymentRequest, DateTime.Today);
                await service.AddWithDetail(model);
                var listUserApproval = await trUserApproval.GetAppIdFromReferenceId(model.Header.Id);
                foreach (var data in listUserApproval)
                {
                    await Task.Run(() => notify.AddNotification(NotificationTypeCode.BankPaymentApprovalNotification, data.AppIdUser, "Finance"));
                }
                return Ok(new ApiResponse(model, true, Messages.SuccessAdd));
            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));
            }
        }

        /// <summary>
        /// Edits existing record based on primary key id.
        /// </summary>
        /// <param name="model">Object parameter to post in json format.</param>
        /// <returns>Json message if the process is successfully done or failed.</returns>
        [HttpPut]
        [ActionRole(ActionMethod.Edit)]
        public async Task<IActionResult> Put([FromBody]PaymentRequest model)
        {
            await service.EditAsync(model);
            return Ok(new ApiResponse(true, Messages.SuccessEdit));
        }

        /// <summary>
        /// Edits existing record based on primary key id.
        /// </summary>
        /// <param name="model">Object parameter to post in json format.</param>
        /// <returns>Json message if the process is successfully done or failed.</returns>
        [HttpPut("EditWithDetail")]
        [ActionRole(ActionMethod.Edit)]
        public async Task<IActionResult> EditWithDetail([FromBody]PaymentRequestDTO model)
        {
            await service.EditWithDetail(model);
            if (model.Header.Status == StatusTransaction.WaitingApproval.ToString())
            {
                var listUserApproval = await trUserApproval.GetAppIdFromReferenceId(model.Header.Id);
                foreach (var data in listUserApproval)
                {
                    await Task.Run(() => notify.AddNotification(NotificationTypeCode.CashAdvanceApprovalNotification, data.AppIdUser, NotificationModuleType.ModuleFinance));
                }
            }
            return Ok(new ApiResponse(model, true, Messages.SuccessEdit));
        }

        /// <summary>
        /// Edits existing record based on primary key id.
        /// </summary>
        /// <param name="model">Object parameter to post in json format.</param>
        /// <returns>Json message if the process is successfully done or failed.</returns>
        [HttpPut("ApproveData")]
        [ActionRole(ActionMethod.Edit)]
        public async Task<IActionResult> ApproveData([FromBody]PaymentRequest model)
        {
            try
            {
                await service.ApproveData(model);
                return Ok(new ApiResponse(model, true, Messages.SuccessEdit));
            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));
            }
        }


        [HttpPut("Reconsiliation")]
        [ActionRole(ActionMethod.Edit)]
        public async Task<IActionResult> Reconsiliation([FromBody]PaymentRequest model)
        {
            try
            {
                await service.Reconsiliation(model);
                return Ok(new ApiResponse(model, true, Messages.SuccessEdit));
            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));
            }
        }



        /// <summary>
        /// Edits existing record based on primary key id.
        /// </summary>
        /// <param name="model">Object parameter to post in json format.</param>
        /// <returns>Json message if the process is successfully done or failed.</returns>
        [HttpPut("Reject")]
        [ActionRole(ActionMethod.Edit)]
        public async Task<IActionResult> Reject([FromBody]PaymentRequest model)
        {
            try
            {
                await service.Reject(model);
                return Ok(new ApiResponse(model, true, Messages.SuccessEdit));
            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));
            }
        }


        /// <summary>
        /// Deletes specific record based on given id parameter.
        /// </summary>
        /// <param name="id">Key id parameter.</param>
        /// <returns>Json message if the process is successfully done or failed.</returns>
        [HttpDelete("{Id}")]
        [ActionRole(ActionMethod.Delete)]
        public async Task<IActionResult> Delete(string Id)
        {
            var model = await service.Get(Id);
            await service.DeleteSoftAsync(model);
            return Ok(new ApiResponse(true, Messages.SuccessDelete));
        }

        /// <summary>
        /// Gets all records without parameter as criteria.
        /// </summary>
        /// <returns>List of json object.</returns>
        [HttpGet]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> Get()
        {
            var result = await service.Get();
            return Ok(new ApiResponse(result));
        }

        [HttpGet("GetAccountNumberData/{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetAccountNumberData(string Id)
        {
            var result = await service.GetAccountNumberData(Id);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("GetVendorData/{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetVendorData(string Id)
        {
            var result = await service.GetVendorData(Id);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("GetPettycashData/{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetPettycashData(string Id)
        {
            var result = await service.GetPettycashData(Id);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("GetDetail/{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetDetail(string Id)
        {
            var result = await service.GetDetail(Id);
            return Ok(new ApiResponse(result));
        }

        /// <summary>
        /// Gets all records without parameter as criteria.
        /// </summary>
        /// <returns>List of json object.</returns>
        [HttpGet("GetPaymentInformationData/{referenceId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetPaymentInformationData(string referenceId)
        {
            var result = await service.GetPaymentInformationData(referenceId);
            return Ok(new ApiResponse(result));
        }

        /// <summary>
        /// Gets all records without parameter as criteria.
        /// </summary>
        /// <returns>List of json object.</returns>
        [HttpGet("GetListDetailPaymentRequest/{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetListDetailPaymentRequest(string Id)
        {
            var result = await service.GetListDetailPaymentRequest(Id);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("GetListDetailPaymentRequestByPairingGroup/{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetListDetailPaymentRequestByPairingGroup(string Id)
        {
            var result = await service.GetListDetailPaymentRequestByPairingGroupId(Id);
            return Ok(new ApiResponse(result));
        }


        /// <summary>
        /// Gets all records without parameter as criteria.
        /// </summary>
        /// <returns>List of json object.</returns>
        [HttpGet("GetBankPaymentJournal/{BankPaymentId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetBankPaymentJournal(string BankPaymentId)
        {
            var result = await service.GetBankPaymentJournal(BankPaymentId);
            return Ok(new ApiResponse(result));
        }

        /// <summary>
        /// Gets all records without parameter as criteria.
        /// </summary>
        /// <returns>List of json object.</returns>
        /// 
        [HttpGet("GetWithFilterAP/{Filter}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetWithFilterAP(PaymentRequestFilter Filter)
        {

            var result = await service.GetWithFilterAP(Filter);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("GetHistoryPayment/{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetHistoryPayment(string Id)
        {

            var result = await service.GetHistoryPayment(Id);
            return Ok(new ApiResponse(result));
        }

        [HttpPost("GetWithFilterAPPost")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetWithFilterAPPost([FromBody]PaymentRequestFilter Filter)
        {

            var result = await service.GetWithFilterAP(Filter);
            return Ok(new ApiResponse(result, true, Messages.SuccessSubmit));
        }

        /// <summary>
        /// Gets all records without parameter as criteria.
        /// </summary>
        /// <returns>List of json object.</returns>
        [HttpGet("GetWithFilterCA/{Filter}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetWithFilterCA(PaymentRequestFilter Filter)
        {
            var result = await service.GetWithFilterCA(Filter);
            return Ok(new ApiResponse(result));
        }

        /// <summary>
        /// Gets all records list ap with posted satus and filter parameter.
        /// </summary>
        /// <returns>List of json object.</returns>
        [HttpPost("GetListAPPosted")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetListAPPosted([FromBody] PaymentRequestFilter Filter)
        {
            var result = await service.GetListAPPosted(Filter);
            return Ok(new ApiResponse(result));
        }

        [HttpPost("GetWithFilterCAPost")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetWithFilterCAPost([FromBody] PaymentRequestFilter Filter)
        {

            var result = await service.GetWithFilterCA(Filter);
            return Ok(new ApiResponse(result, true, Messages.SuccessSubmit));
        }

        [HttpPost("GetWithFilter")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetWithFilter([FromBody] PaymentRequestFilter Filter)
        {

            var result = await service.GetWithFilter(Filter);
            return Ok(new ApiResponse(result, true, Messages.SuccessSubmit));
        }

        /// <summary>
        /// Gets all records without parameter as criteria.
        /// </summary>
        /// <returns>List of json object.</returns>
        [HttpPost("PostDatatoSp")]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> PostDatatoSp([FromBody]PaymentRequest model)
        {
            try
            {
                await service.CreateJournalWithSP(model.Id);
                return Ok(new ApiResponse(model, true, Messages.SuccessAdd));
            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));

            }
        }

        /// <summary>
        /// Gets single record based on specific primary key id.
        /// </summary>
        /// <param name="id">Key id parameter.</param>
        /// <returns>A json object with given id.</returns>
        [HttpGet("{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> Get(string Id)
        {
            var result = await service.Get(Id);
            return Ok(new ApiResponse(result));
        }
    }
}