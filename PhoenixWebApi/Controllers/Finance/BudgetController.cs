﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Phoenix.Data;
using Phoenix.Data.Attributes;
using Phoenix.Data.Models.Finance;
using Phoenix.Data.Models.Finance.Transaction;
using Phoenix.Data.RestApi;
using Phoenix.Data.Services.Finance;
using Phoenix.Data.Services.Finance.Transaction;
using PhoenixWebApi.Base;

namespace PhoenixWebApi.Controllers.Finance
{
    [Produces("application/json")]
    [ApiExplorerSettings(IgnoreApi = false, GroupName = @"Finance")]
    [Route("api/finance/[controller]")]
    public class BudgetController : ApiBaseController
    {
        readonly IBudgetService _service;
        public BudgetController(IBudgetService service)
        {
            this._service = service;
        }

        [HttpGet("GetMasterFinancialYear")]
        [ActionRole(ActionMethod.Allowed)]
        public async Task<IActionResult> GetMasterFinancialyear()
        {
            var result = await _service.GetMasterFinancialyear();
            return Ok(new ApiResponse(result));
        }

        [HttpGet("GetWithName")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetWithName()
        {
            var result = await _service.GetWithName();
            return Ok(new ApiResponse(result));
        }
        
        [HttpGet("GetDetailTypeBudget/{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetTypeBugetWithHeaderId(string Id)
        {
            var result = await _service.GetTypeBugetWithHeaderId(Id);
            return Ok(new ApiResponse(result));
        }
        [HttpGet("GetIdWithName/{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetIdWithName(string Id)
        {
            var result = await _service.GetIdWithName(Id);
            return Ok(new ApiResponse(result));
        }

        [HttpPost]
        [Route("uploaddata")]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> PostUploadData([FromForm]IFormCollection collection)
        {
            if (collection.Count == 0)
            {
                return Ok(new ApiResponse(null, false, "Bad Request"));
            }

            try
            {
                var model = JsonConvert.DeserializeObject<TypeBudgetHdr>(collection["TypeBudgetHdr"].ToString());
                var file = collection.Files.GetFile("files");
                var result = await _service.ReadExcel(file);

                Budget budget = new Budget();
                budget.FinancialMonth = model.bulan;
                budget.employee_basic_info_id = model.employee_basic_info_id;
                budget.FinancialYearId = model.financial_year_id;
                budget.revision = model.revision;
                budget.FinancialYearName = model.financialyear;
                string headerid = Guid.NewGuid().ToString();

                await _service.InsertBudget(budget, result, headerid);

                await _service.InsertTmpTabletoBudgetSp(model.financial_year_id, model.bulan, model.revision, headerid);


                return Ok(new ApiResponse(true, Messages.SuccessAdd));
            }
            catch (Exception ex) {

                return Ok(new ApiResponse(false, ex.Message.ToString()));

            }
        
        }

        [HttpDelete("{Id}")]
        [ActionRole(ActionMethod.Delete)]
        public async Task<IActionResult> Delete(string Id)
        {
            var model = await _service.GetIdWithName(Id);
            await _service.DeleteSoftBudgetHdrAsync(model);
            return Ok(new ApiResponse(true, Messages.SuccessDelete));
        }
    }
}