﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Phoenix.Data;
using Phoenix.Data.Attributes;
using Phoenix.Data.Models;
using Phoenix.Data.RestApi;

namespace PhoenixWebApi.Controllers.Finance
{
    [Produces("application/json")]
    [Route("api/trialbalance")]
    public class TrialBalanceController : ControllerBase
    {
        readonly ITrialBalanceService service;
        public TrialBalanceController(ITrialBalanceService service)
        {
            this.service = service;
        }

        //[HttpGet("{month}/{year}")]
        //[HttpGet]
        //[ActionRole(ActionMethod.Read)]
        //public async Task<IActionResult> Get()
        //{
        //    var result = await service.Get();
        //    return Ok(new ApiResponse(result));
        //}

        [HttpPost("GetData")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetData([FromBody] TrialBalanceFilter Filter)
        {
            var result = await service.Get(Filter);
            return Ok(new ApiResponse(result));
        }


        [HttpGet("GetFiscalYearsById/{Id}")]
        public async Task<IActionResult> GetTransactionID(string Id)
        {
            var result = await service.GetFiscalYears(Id);
            return Ok(new ApiResponse(result));
        }
        
    }
}