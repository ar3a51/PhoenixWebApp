﻿
using Phoenix.Data.RestApi;
using Microsoft.AspNetCore.Mvc;
using Phoenix.Data.Services.Finance.Transaction;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Phoenix.Data.Models.Finance.Transaction;
using System.Linq;
using Phoenix.Data.Attributes;
using PhoenixWebApi.Services;
using Phoenix.Data.Services.Um;
using Phoenix.Data.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace PhoenixWebApi.Controllers.Finance
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class InvoiceClientMultiPceController : ControllerBase
    {
        readonly IInvoiceClientMultiPceService service;
        readonly INotificationCenter notify;
        readonly ITransactionUserApprovalService trUserApproval;
        public InvoiceClientMultiPceController(IInvoiceClientMultiPceService service, INotificationCenter notify, ITransactionUserApprovalService trUserApproval)
        {
            this.service = service;
            this.notify = notify;
            this.trUserApproval = trUserApproval;
        }

        [HttpGet("GetListIndex/{status?}/{legal?}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetPceList(string status, string legal)
        {
            var result = await service.GetListIndex(status, legal);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("GetPceInfo/{multipceid}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetPceInfo(string multipceid)
        {
            var result = await service.GetPceInfo(multipceid);
            return Ok(new ApiResponse(result, true));
        }

        [HttpGet("GetInfoBank/{id?}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetInfoBank(string id)
        {
            var result = await service.GetInfoBank(id);
            return Ok(new ApiResponse(result, true));
        }
        

        [HttpGet("ReadMultiPceList/{multipceid?}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> ReadMultiPceList(string multipceid)
        {
            var result = await service.ReadMultiPceList(multipceid);
            return Ok(new ApiResponse(result, true));
        }
        
        [HttpGet("GetPceChoiceList/{multipceid?}/{legalid?}/{divid?}/{mservice?}/{clientt?}/{currency?}/{brandid?}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetPceChoiceList(string multipceid, string legalid, string divid, string mservice, string clientt, string currency, string brandid)
        {
            var result = await service.GetPceChoiceList(multipceid, legalid, divid, mservice, clientt, currency, brandid);
            return Ok(new ApiResponse(result, true));
        }

        [HttpGet("ReadInvHistory/{multipceid}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> ReadInvHistory(string multipceid)
        {
            var result = await service.ReadInvHistory(multipceid);
            return Ok(new ApiResponse(result));
        }


        [HttpPost("addandsubmit")]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> PostSubmit([FromForm]IFormCollection collection)// submit add
        {
            try
            {
                if (collection.Count == 0 || !collection.Keys.Contains(nameof(InvoiceClient)))
                {
                    return Ok(new ApiResponse(null, false, "bad request"));
                }

                var model = JsonConvert.DeserializeObject<InvoiceClient>(collection[nameof(InvoiceClient)].ToString());
                if (collection.Files.Count() == 2)
                {
                    model.VatDocFile = collection.Files[0];
                    model.PoDocFile = collection.Files[1];
                }

                await service.InsertAndSubmitInvoiceClient(model);

                var listUserApproval = await trUserApproval.GetAppIdFromReferenceId(model.Id);
                foreach (var data in listUserApproval)
                {
                    await Task.Run(() => notify.AddNotification(NotificationTypeCode.InvoiceClientMultiple, data.AppIdUser, NotificationModuleType.ModuleFinance));
                }
                return Ok(new ApiResponse(true, Messages.SuccessAdd));
            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));
            }
        }

        [HttpPost("adddata")]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> PostAdd([FromForm]IFormCollection collection)// add
        {
            try
            {
                var model = JsonConvert.DeserializeObject<InvoiceClient>(collection[nameof(InvoiceClient)].ToString());
               
                if (collection.Files.Count() == 2)
                {
                    model.VatDocFile = collection.Files[0];
                    model.PoDocFile = collection.Files[1];
                }

               var retData =  await service.InsertInvoiceClient(model);
                return Ok(new ApiResponse(retData, true, Messages.SuccessAdd));
            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));
            }
        }

        [HttpPut("updateandsubmit")]
        [ActionRole(ActionMethod.Edit)]
        public async Task<IActionResult> Put([FromForm]IFormCollection collection)
        {
            var model = JsonConvert.DeserializeObject<InvoiceClient>(collection[nameof(InvoiceClient)].ToString());
           
            if (collection.Files.Count() == 2)
            {
                model.VatDocFile = collection.Files[0];
                model.PoDocFile = collection.Files[1];
            }

            await service.EditAndSubmitAsync(model);
            var listUserApproval = await trUserApproval.GetAppIdFromReferenceId(model.Id);
            foreach (var data in listUserApproval)
            {
                await Task.Run(() => notify.AddNotification(NotificationTypeCode.InvoiceClientMultiple, data.AppIdUser, NotificationModuleType.ModuleFinance));
            }
            return Ok(new ApiResponse(true, Messages.SuccessEdit));
        }

        [HttpPut("updatedata")]
        [ActionRole(ActionMethod.Edit)]
        public async Task<IActionResult> PutData([FromForm]IFormCollection collection)
        {
            var model = JsonConvert.DeserializeObject<InvoiceClient>(collection[nameof(InvoiceClient)].ToString());
            
            if (collection.Files.Count() == 2)
            {
                model.VatDocFile = collection.Files[0];
                model.PoDocFile = collection.Files[1];
            }

            var dataReturn =  await service.EditAsyncData(model);
            return Ok(new ApiResponse(dataReturn, true, Messages.SuccessEdit));
        }

        
        [HttpPost]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> Post([FromBody]InvoiceClient model)
        {
            //var other_model = (from a in model.items_other where a.IsSelected == true select a).ToList();
            //model.items_other = other_model;
            try
            {
                await service.InsertInvoiceClient(model);
                return Ok(new ApiResponse(true, Messages.SuccessAdd));
            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));
            }

        }

        [HttpGet("GetDDLBank/{orgid?}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetDDLBank(string orgid)
        {
            var result = await service.GetDDLBank(orgid);
            return Ok(new ApiResponse(result));
        }

        [HttpPut("Approve")]
        [ActionRole(ActionMethod.Edit)]
        public async Task<IActionResult> Approve([FromBody]InvoiceClient model)
        {
            try
            {
                await service.Approve(model);
                return Ok(new ApiResponse(model, true, Messages.SuccessApprove));
            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));
            }

        }

        [HttpPut("Reject")]
        [ActionRole(ActionMethod.Edit)]
        public async Task<IActionResult> Reject([FromBody]InvoiceClient model)
        {
            try
            {
                await service.Reject(model);
                return Ok(new ApiResponse(model, true, Messages.SuccessReject));
            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));
            }

        }

        [HttpPost("PostDatatoSp")]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> PostDatatoSp([FromBody]InvoiceClient model)
        {
            try
            {
                string invNo = "";
                if (string.IsNullOrEmpty(model.MultiPceId))
                    invNo = model.InvoiceNumber;
                else
                    invNo = model.MultiPceId;

                await service.CreateJournalWithSP(invNo);
                return Ok(new ApiResponse(model, true, Messages.SuccessAdd));
            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));

            }
        }

        [HttpGet("GetDataJournal/{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetDataJournal(string Id)
        {
            var result = await service.GetDataJournal(Id);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("GetInvoiceProductionDetail/{pceid}/{invNo}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetInvoiceProductionDetail(string pceid, string invNo)
        {
            var result = await service.GetInvoiceProductionDetail(pceid, invNo);
            return Ok(new ApiResponse(result));
        }

    }
}
