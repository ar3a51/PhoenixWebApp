﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.Data.Attributes;
using Phoenix.Data.Models.Finance;
using Phoenix.Data.Models.Finance.Transaction;
using Phoenix.Data.RestApi;
using Phoenix.Data.Services.Finance;

namespace PhoenixWebApi.Controllers.Finance
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class PettyCashController : ControllerBase
    {
        readonly IPettyCashService service;

        /// <summary>
        /// And endpoint to manage NewPettyCash
        /// </summary>
        /// <param name="context">Database context</param>
        public PettyCashController(IPettyCashService service)
        {
            this.service = service;
        }

        /// <summary>
        /// Gets all records without parameter as criteria.
        /// </summary>
        /// <returns>List of json object.</returns>
        [HttpPost("ReadIndex")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> ReadIndex([FromBody]NewPettyCash model)
        {
            var result = await service.ReadIndex(model);
            return Ok(new ApiResponse(result));
        }

        /// <summary>
        /// Gets all Detail records without parameter as criteria.
        /// </summary>
        /// <returns>List of json object.</returns>
        [HttpPost("GetDetails/{Id}/{ReffNum}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetDetails(string Id, string ReffNum)
        {
            var result = await service.GetDetails(Id, ReffNum);
            return Ok(new ApiResponse(result));
        }

        /// <summary>
        /// Gets all Detail records without parameter as criteria.
        /// </summary>
        /// <returns>List of json object.</returns>
        [HttpGet("GetInfoPceByJobId/{jobid}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetInfoPceByJobId(string jobid)
        {
            var result = await service.GetInfoPceByJobId(jobid);
            return Ok(new ApiResponse(result));
        }
		
		/// <summary>
		/// Gets group name.
		/// </summary>
		/// <returns>List of json object.</returns>
		[HttpGet("GetGroupName")]
		[ActionRole(ActionMethod.Read)]
		public async Task<IActionResult> GetGroupName()
		{
			var result = await service.GetGroupName();
			return Ok(new ApiResponse(result, true));
		}

		/// <summary>
		/// Gets all Detail records without parameter as criteria.
		/// </summary>
		/// <returns>List of json object.</returns>
		[HttpGet("GetPcaTasks/{pcaId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetPcaTasks(string pcaId)
        {
            var result = await service.GetPcaTasks(pcaId);
            return Ok(new ApiResponse(result));
        }        

        /// <summary>
        /// Gets single record based on specific primary key id.
        /// </summary>
        /// <param name="id">Key id parameter.</param>
        /// <returns>A json object with given id.</returns>
        [HttpGet("{Id}/{ReffNum}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> Get(string Id, string ReffNum)
        {
            var result = await service.Get(Id, ReffNum);
            return Ok(new ApiResponse(result, true));
        }

        /// <summary>
        /// Creates new record.
        /// </summary>
        /// <param name="model">Object parameter to post in json format.</param>
        /// <returns>Json message if the process is successfully done or failed.</returns>
        [HttpPost]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> Post([FromBody]NewPettyCash model)
        {
            await service.AddAsync(model);
            return Ok(new ApiResponse(model, true, Messages.SuccessAdd));
        }

        /// <summary>
        /// Creates new record.
        /// </summary>
        /// <param name="model">Object parameter to post in json format.</param>
        /// <returns>Json message if the process is successfully done or failed.</returns>
        [HttpPost("CreateJournal")]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> CreateJournal([FromBody]NewPettyCash model)
        {
            await service.CreateJournal(model);
            return Ok(new ApiResponse(model, true, Messages.SuccessAdd));
        }

        /// <summary>
        /// Creates new record.
        /// </summary>
        /// <param name="model">Object parameter to post in json format.</param>
        /// <returns>Json message if the process is successfully done or failed.</returns>
        [HttpPut("EditJournal")]
        [ActionRole(ActionMethod.Edit)]
        public async Task<IActionResult> EditJournal([FromBody]NewPettyCash model)
        {
            await service.EditJournal(model);
            return Ok(new ApiResponse(model, true, Messages.SuccessEdit));
        }

        /// <summary>
        /// Gets single record based on specific primary key id.
        /// </summary>
        /// <param name="id">Key id parameter.</param>
        /// <returns>A json object with given id.</returns>
        [HttpGet("JournalList/{id}/{rfn}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetJournal(string Id, string rfn)
        {
            var result = await service.GetJournal(Id, rfn);
            return Ok(new ApiResponse(result));
        }

        /// <summary>
        /// Edits existing record based on primary key id.
        /// </summary>
        /// <param name="model">Object parameter to post in json format.</param>
        /// <returns>Json message if the process is successfully done or failed.</returns>
        [HttpPut]
        [ActionRole(ActionMethod.Edit)]
        public async Task<IActionResult> Put([FromBody]NewPettyCash model)
        {
            await service.EditAsync(model);
            return Ok(new ApiResponse(true, Messages.SuccessEdit));
        }

        /// <summary>
        /// Deletes specific record based on given id parameter.
        /// </summary>
        /// <param name="id">Key id parameter.</param>
        /// <returns>Json message if the process is successfully done or failed.</returns>
        [HttpDelete("{Id}")]
        [ActionRole(ActionMethod.Delete)]
        public async Task<IActionResult> Delete(string Id)
        {
            var model = await service.Get(Id, "");
            //await service.DeleteSoftAsync(model);
            return Ok(new ApiResponse(true, Messages.SuccessDelete));
        }
    }
}