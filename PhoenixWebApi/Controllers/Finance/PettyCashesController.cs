﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Phoenix.Data;
using Phoenix.Data.Models.Finance;
using Phoenix.Data.Models.Finance.Transaction;

namespace PhoenixWebApi.Controllers.Finance
{
    [Route("api/[controller]")]
    [ApiController]
    public class PettyCashesController : ControllerBase
    {
        private readonly DataContext _context;

        public PettyCashesController(DataContext context)
        {
            _context = context;
        }

        // GET: api/PettyCashes
        [HttpGet]
        public IEnumerable<NewPettyCash> GetPettyCashs()
        {
            return _context.PettyCashs;
        }

        // GET: api/PettyCashes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPettyCash([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pettyCash = await _context.PettyCashs.FindAsync(id);

            if (pettyCash == null)
            {
                return NotFound();
            }

            return Ok(pettyCash);
        }

        // PUT: api/PettyCashes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPettyCash([FromRoute] string id, [FromBody] PettyCash pettyCash)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pettyCash.Id)
            {
                return BadRequest();
            }

            _context.Entry(pettyCash).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PettyCashExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PettyCashes
        [HttpPost]
        public async Task<IActionResult> PostPettyCash([FromBody] NewPettyCash pettyCash)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.PettyCashs.Add(pettyCash);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPettyCash", new { id = pettyCash.Id }, pettyCash);
        }

        // DELETE: api/PettyCashes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePettyCash([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pettyCash = await _context.PettyCashs.FindAsync(id);
            if (pettyCash == null)
            {
                return NotFound();
            }

            _context.PettyCashs.Remove(pettyCash);
            await _context.SaveChangesAsync();

            return Ok(pettyCash);
        }

        private bool PettyCashExists(string id)
        {
            return _context.PettyCashs.Any(e => e.Id == id);
        }
    }
}