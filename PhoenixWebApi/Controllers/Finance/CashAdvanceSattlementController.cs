﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Phoenix.Data;
using Phoenix.Data.Attributes;
using Phoenix.Data.Models;
using Phoenix.Data.Models.Finance;
using Phoenix.Data.Models.Finance.Transaction;
using Phoenix.Data.RestApi;
using Phoenix.Data.Services.Finance;
using Phoenix.Data.Services.Um;
using PhoenixWebApi.Services;

namespace PhoenixWebApi.Controllers.Finance
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class CashAdvanceSattlementController : ControllerBase
    {
        readonly ICashAdvanceSattlementService service;
        readonly INotificationCenter notify;
        readonly ITransactionUserApprovalService trUserApproval;
        public CashAdvanceSattlementController(ICashAdvanceSattlementService service, INotificationCenter notify, ITransactionUserApprovalService trUserApproval)
        {
            this.service = service;
            this.notify = notify;
            this.trUserApproval = trUserApproval;
        }

        [HttpGet]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> Get([FromQuery]string legal, [FromQuery]string svc)
        {
            Dictionary<string, string> filters = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(legal))
            {
                filters.Add("legal", legal);
            }
            if (!string.IsNullOrEmpty(svc))
            {
                filters.Add("svc", svc);
            }
            var result = await service.Get(filters);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("GetSattlementById/{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetSattlementById(string Id)
        {
            var result = await service.GetSattlementById(Id);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("GetSattlementDetailById/{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetSattlementDetailById(string Id)
        {
            var result = await service.GetSattlementDetailById(Id);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("GetJournalDataBySattlementId/{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetJournalDataBySattlementId(string Id)
        {
            var result = await service.GetJournalDataBySattlementId(Id);
            return Ok(new ApiResponse(result));
        }

        [HttpPut("EditSattlement")]
        [ActionRole(ActionMethod.Edit)]
        public async Task<IActionResult> EditSattlement([FromForm]IFormCollection collection)/*([FromBody]CashAdvanceSattlementDTO model, [FromForm]IFormCollection collection)*/
        {
            if (collection.Count == 0 || !collection.Keys.Contains(nameof(CashAdvanceSattlementDTO)))
            {
                return Ok(new ApiResponse(null, false, "bad request"));
            }

            var model = JsonConvert.DeserializeObject<CashAdvanceSattlementDTO>(collection[nameof(CashAdvanceSattlementDTO)].ToString());

            if (collection.Files.Count() > 0)
            {
                model.fileUpload = collection.Files[0];
            }
            
            await service.EditSattlement(model);
            //if (model.Header.CaStatusId == StatusTransaction.WaitingApproval.ToString())
            //{
            //    var listUserApproval = await trUserApproval.GetAppIdFromReferenceId(model.Header.Id);
            //    foreach (var data in listUserApproval)
            //    {
            //        await Task.Run(() => notify.AddNotification(NotificationTypeCode.CashAdvanceApprovalNotification, data.AppIdUser, NotificationModuleType.ModuleFinance));
            //    }
            //}
            return Ok(new ApiResponse(model.Header, true, Messages.SuccessEdit));
        }

        [HttpPost("AddSattlement")]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> AddSattlement(CashAdvanceSattlementDTO model)
        {
            try
            {
                await service.AddSattlement(model);
                return Ok(new ApiResponse(model, true, Messages.SuccessAdd));
            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));
            }
        }

        [HttpPut("ApproveSattlement")]
        [ActionRole(ActionMethod.Edit)]
        public async Task<IActionResult> ApproveSattlement([FromBody]CashAdvanceSettlement model)
        {
            try
            {
                await service.ApproveSattlement(model);
                return Ok(new ApiResponse(model, true, Messages.SuccessApprove));
            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));
            }

        }

        [HttpPut("Reject")]
        [ActionRole(ActionMethod.Edit)]
        public async Task<IActionResult> Reject([FromBody]CashAdvanceSettlement model)
        {
            try
            {
                await service.Reject(model);
                return Ok(new ApiResponse(model, true, Messages.SuccessReject));
            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));
            }

        }

        [HttpPost("PostDataSattlement")]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> PostDataSattlement(CashAdvanceSettlement model)
        {
            try
            {
                await service.CreateAutomaticJournalFromSattlementId(model.Id);
                return Ok(new ApiResponse(model, true, Messages.SuccessAdd));
            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));

            }
        }

        [HttpPost("uploadfile")]
        public async Task<IActionResult> UploadFile([FromForm]IFormCollection collection)
        {
            try
            {
                var files = collection.Files[0];
                var result = await service.UploadFile(files);
                return Ok(new ApiResponse(result));

            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));
            }

        }


    }
}