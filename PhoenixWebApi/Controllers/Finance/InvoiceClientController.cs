﻿
using Phoenix.Data.RestApi;
using Microsoft.AspNetCore.Mvc;
using Phoenix.Data.Services.Finance.Transaction;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Phoenix.Data.Models.Finance.Transaction;
using System.Linq;
using Phoenix.Data.Attributes;
using PhoenixWebApi.Services;
using Phoenix.Data.Services.Um;
using Phoenix.Data.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace PhoenixWebApi.Controllers.Finance
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class InvoiceClientController : ControllerBase
    {
        readonly IInvoiceClientService service;
        readonly INotificationCenter notify;
        readonly ITransactionUserApprovalService trUserApproval;
        public InvoiceClientController(IInvoiceClientService service, INotificationCenter notify, ITransactionUserApprovalService trUserApproval)
        {
            this.service = service;
            this.notify = notify;
            this.trUserApproval = trUserApproval;
        }

        [HttpGet("GetPceList/{status?}/{legal?}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetPceList(string status, string legal)
        {
            var result = await service.GetPceLists(status, legal);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("GetPceInfo/{pceno}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetPceInfo(string pceno)
        {
            var result = await service.GetPceInfo(pceno);
            return Ok(new ApiResponse(result, true));
        }

        [HttpGet("GetInvoiceInfo/{pceno}/{invNo}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetInvoiceInfo(string pceno, string invNo)
        {
            var result = await service.GetInvoiceInfo(pceno, invNo);
            return Ok(new ApiResponse(result, true));
        }

        [HttpGet("GetCompanyInfo/{companyid}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetCompanyInfo(string companyid)
        {
            var result = await service.GetCompanyInfo(companyid);
            return Ok(new ApiResponse(result, true));
        }

        [HttpGet("GetAffiliation/{divisiid}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetAffiliation(string divisiid)
        {
            var result = await service.GetAffiliation(divisiid);
            return Ok(new ApiResponse(result, true));
        }
        
        
        [HttpGet("GetDetailFromPceList/{pceno}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetDetailFromPceList(string pceno)
        {
            var result = await service.GetDetailFromPceList(pceno);
            return Ok(new ApiResponse(result, true));
        }

        [HttpGet("GetInvoice/{invid}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetInvoice(string invid)
        {
            var result = await service.GetInvoice(invid);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("GetInvoiceProduction/{invid}/{pceid}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetInvoiceProduction(string invid, string pceid)
        {
            var result = await service.GetInvoiceProduction(invid, pceid);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("GetInvoiceProductionDetail/{pceid}/{invNo}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetInvoiceProductionDetail(string pceid, string invNo)
        {
            var result = await service.GetInvoiceProductionDetail(pceid, invNo);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("ReadInvDtlHistory/{invid}/{pceid}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> ReadInvDtlHistory(string invid, string pceid)
        {
            var result = await service.ReadInvDtlHistory(invid, pceid);
            return Ok(new ApiResponse(result));
        }

        [HttpPost("addandsubmit")]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> Post([FromForm]IFormCollection collection)// submit
        {
            if (collection.Count == 0 || !collection.Keys.Contains(nameof(InvoiceClient)))
            {
                return Ok(new ApiResponse(null, false, "bad request"));
            }

            var model = JsonConvert.DeserializeObject<InvoiceClient>(collection[nameof(InvoiceClient)].ToString());
            if (collection.Files.Count() == 2)
            {
                model.VatDocFile = collection.Files[0];
                model.PoDocFile = collection.Files[1];
            }
            //if (!string.IsNullOrEmpty(model.PceCode))
            //{
            //    var other_model = (from a in model.items_other where a.IsSelected == true select a).ToList();
            //    model.items_other = other_model;
            //}
            try
            {
                await service.InsertAndSubmitInvoiceClient(model);

                var listUserApproval = await trUserApproval.GetAppIdFromReferenceId(model.Id);
                foreach (var data in listUserApproval)
                {
                    await Task.Run(() => notify.AddNotification(NotificationTypeCode.InvoiceClient, data.AppIdUser, NotificationModuleType.ModuleFinance));
                }
                return Ok(new ApiResponse(true, Messages.SuccessAdd));
            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));
            }
        }

        [HttpPost("adddata")]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> PostAdd([FromForm]IFormCollection collection)// submit
        {
            if (collection.Count == 0 || !collection.Keys.Contains(nameof(InvoiceClient)))
            {
                return Ok(new ApiResponse(null, false, "bad request"));
            }

            var model = JsonConvert.DeserializeObject<InvoiceClient>(collection[nameof(InvoiceClient)].ToString());
            if (collection.Files.Count() == 2)
            {
                model.VatDocFile = collection.Files[0];
                model.PoDocFile = collection.Files[1];
            }

            //if (!string.IsNullOrEmpty(model.PceCode))
            //{
            //    var other_model = (from a in model.items_other where a.IsSelected == true select a).ToList();
            //    model.items_other = other_model;
            //}
            try
            {
                //await service.InsertInvoiceClient(model);
                var dataReturn = await service.InsertInvoiceClient(model);
                return Ok(new ApiResponse(dataReturn, true, Messages.SuccessEdit));
            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));
            }
        }



        [HttpPut("updateandsubmit")]
        [ActionRole(ActionMethod.Edit)]
        public async Task<IActionResult> Put([FromForm]IFormCollection collection)
        {
            if (collection.Count == 0 || !collection.Keys.Contains(nameof(InvoiceClient)))
            {
                return Ok(new ApiResponse(null, false, "bad request"));
            }

            var model = JsonConvert.DeserializeObject<InvoiceClient>(collection[nameof(InvoiceClient)].ToString());
            if (collection.Files.Count() == 2)
            {
                model.VatDocFile = collection.Files[0];
                model.PoDocFile = collection.Files[1];
            }

            await service.EditAndSubmitAsync(model);
            var listUserApproval = await trUserApproval.GetAppIdFromReferenceId(model.Id);
            foreach (var data in listUserApproval)
            {
                await Task.Run(() => notify.AddNotification(NotificationTypeCode.InvoiceClient, data.AppIdUser, NotificationModuleType.ModuleFinance));
            }
            return Ok(new ApiResponse(true, Messages.SuccessEdit));
        }

        [HttpPut("updatedata")]
        [ActionRole(ActionMethod.Edit)]
        public async Task<IActionResult> PutData([FromForm]IFormCollection collection)
        {
            if (collection.Count == 0 || !collection.Keys.Contains(nameof(InvoiceClient)))
            {
                return Ok(new ApiResponse(null, false, "bad request"));
            }

            var model = JsonConvert.DeserializeObject<InvoiceClient>(collection[nameof(InvoiceClient)].ToString());
            if (collection.Files.Count() == 2)
            {
                model.VatDocFile = collection.Files[0];
                model.PoDocFile = collection.Files[1];
            }

            //await service.EditAsync(model);
            var dataReturn = await service.EditAsyncData(model);
            return Ok(new ApiResponse(dataReturn, true, Messages.SuccessEdit));
            
        }

        [HttpGet("GetDDLBank/{orgid}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetDDLBank(string orgid)
        {
            var result = await service.GetDDLBank(orgid);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("{Id?}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> Get(string Id)
        {
            var result = await service.Get(Id);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("GetPceChoiceList/{legalid?}/{divid?}/{mservice?}/{clientt?}/{currency?}/{pceId?}/{jobId?}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetPceChoiceList(string multipceid, string legalid, string divid, string mservice, string clientt, string currency, string pceId, string jobId)
        {
            var result = await service.GetPceChoiceList(legalid, divid, mservice, clientt, currency, pceId, jobId);
            return Ok(new ApiResponse(result, true));
        }

        [HttpPut("Approve")]
        [ActionRole(ActionMethod.Edit)]
        public async Task<IActionResult> Approve([FromBody]InvoiceClient model)
        {
            try
            {
                await service.Approve(model);
                return Ok(new ApiResponse(model, true, Messages.SuccessApprove));
            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));
            }

        }

        [HttpPut("ForceClose")]
        [ActionRole(ActionMethod.Edit)]
        public async Task<IActionResult> ForceClose([FromBody]InvoiceClient model)
        {
            try
            {
                await service.ForceClose(model);
                return Ok(new ApiResponse(model, true, Messages.SuccessApprove));
            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));
            }

        }

        [HttpPut("Reject")]
        [ActionRole(ActionMethod.Edit)]
        public async Task<IActionResult> Reject([FromBody]InvoiceClient model)
        {
            try
            {
                await service.Reject(model);
                return Ok(new ApiResponse(model, true, Messages.SuccessReject));
            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));
            }

        }

        [HttpPost("PostDatatoSp")]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> PostDatatoSp([FromBody]InvoiceClient model)
        {
            try
            {
                await service.CreateJournalWithSP(model.Id);
                return Ok(new ApiResponse(model, true, Messages.SuccessAdd));
            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));

            }
        }

        [HttpGet("GetDataJournal/{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetDataJournal(string Id)
        {
            var result = await service.GetDataJournal(Id);
            return Ok(new ApiResponse(result));
        }

    }
}
