﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.Data.Attributes;
using Phoenix.Data.Services.Finance.Transaction;
using Phoenix.Data.RestApi;

namespace PhoenixWebApi.Controllers.Finance
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class GoodsReturnController : ControllerBase
    {
        private readonly IGoodsReturnServices services;
        public GoodsReturnController(IGoodsReturnServices goodsReturnServices)
        {
            this.services = goodsReturnServices;
        }

        [HttpGet]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> Get()
        {
            var result = await services.Get();
            return Ok(new ApiResponse(result));
        }
    }
}