﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.Data.Attributes;
using Phoenix.Data.Models;
using Phoenix.Data.Models.Finance.Transaction;
using Phoenix.Data.RestApi;
using Phoenix.Data.Services.Finance.Transaction;

namespace PhoenixWebApi.Controllers.Finance
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class FixedAssetController : ControllerBase
    {
        readonly IFixedAssetService service;

        public FixedAssetController(IFixedAssetService service)
        {
            this.service = service;
        }

        [HttpGet]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> Get()
        {
            var result = await service.Get();
            return Ok(new ApiResponse(result));
        }

        [HttpGet("{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> Get(string Id)
        {
            var result = await service.Get(Id);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("GetAffiliation/{buId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetAffiliation(string buId)
        {
            var result = await service.GetAffiliation(buId);
            return Ok(new ApiResponse(result));
        }

        [HttpPost]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> Post([FromBody]FixedAssetDTO model)
        {
            var Header = new FixedAsset();
            Header = model.Header;
            var Details = new List<FixedAssetMovement>();
            Details = model.Details;
            await service.AddWithDetailAsync(Header,Details);
            return Ok(new ApiResponse(true, Messages.SuccessAdd));
        }

        [HttpPut]
        [ActionRole(ActionMethod.Edit)]
        public async Task<IActionResult> Put([FromBody]FixedAssetDTO model)
        {
            var Header = new FixedAsset();
            Header = model.Header;
            var Details = new List<FixedAssetMovement>();
            Details = model.Details;
            await service.EditWithDetailAsync(Header,Details);
            return Ok(new ApiResponse(true, Messages.SuccessEdit));
        }

        [HttpPut("editlist")]
        [ActionRole(ActionMethod.Edit)]
        public async Task<IActionResult> PutList([FromBody]List<FixedAsset> model)
        {  
            await service.EditListHeaderAsync(model);
            return Ok(new ApiResponse(true, Messages.SuccessEdit));
        }

        [HttpPut("editcost")]
        [ActionRole(ActionMethod.Edit)]
        public async Task<IActionResult> PutCost([FromBody]FixedAsset model)
        {
            await service.EditCostAsync(model);
            return Ok(new ApiResponse(true, Messages.SuccessEdit));
        }

        [HttpDelete("{Id}")]
        [ActionRole(ActionMethod.Delete)]
        public async Task<IActionResult> Delete(string Id)
        {
            var model = await service.Get(Id);
            await service.DeleteSoftAsync(model);
            return Ok(new ApiResponse(true, Messages.SuccessDelete));
        }
    }
}