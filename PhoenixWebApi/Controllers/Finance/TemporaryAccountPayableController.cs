﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.Data.Attributes;
using Phoenix.Data.Models;
using Phoenix.Data.Models.Finance.Transaction;
using Phoenix.Data.Models.Finance.Transaction.InvoiceReceivable;
using Phoenix.Data.RestApi;
using Phoenix.Data.Services.Finance.Transaction;


namespace PhoenixWebApi.Controllers.Finance
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class TemporaryAccountPayableController : ControllerBase
    {
        readonly ITemporaryAccountPayableService service;
        public TemporaryAccountPayableController(ITemporaryAccountPayableService service)
        {
            this.service = service;
        }
        [HttpGet]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> Get()
        {
            var result = await service.Get();
            return Ok(new ApiResponse(result));
        }
        [HttpGet("{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> Get(string Id)
        {
            var result = await service.Get(Id);
            return Ok(new ApiResponse(result));
        }
        [HttpGet("getjoinlist")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetJoinList()
        {
            var result = await service.GetJoinList();
            return Ok(new ApiResponse(result));
        }
        [HttpGet("getjoin/{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetJoin(string Id)
        {
            var result = await service.GetJoin(Id);
            return Ok(new ApiResponse(result));
        }
        [HttpGet("getAccountData/{Id}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetAccountData(string Id)
        {
            var result = await service.GetAccountData(Id);
            return Ok(new ApiResponse(result));
        }
        [HttpGet("GetJournalTmp/{referenceId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetJournalTmp(string referenceId)
        {
            var result = await service.GetJournalTmp(referenceId);
            return Ok(new ApiResponse(result));
        }
        [HttpGet("GetJournalTmpTax/{referenceId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetJournalTmpTaxOnly(string referenceId)
        {
            var result = await service.GetJournalTmpTaxOnly(referenceId);
            return Ok(new ApiResponse(result));
        }
        [HttpGet("GetJournalTmpAll/{referenceId}/{invoiceReceivedId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetJournalTmpAll(string referenceId,string invoiceReceivedId)
        {
            var result = await service.GetJournalTmpAll(referenceId, invoiceReceivedId);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("GetTAPFromINV/{invoiceReceivedId}/{referenceId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetTAPFromINV(string invoiceReceivedId, string referenceId)
        {
            var result = await service.GetTAPFromINV(invoiceReceivedId, referenceId);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("GetTapJournalWithTAPId/{TAPId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetTapJournalWithTAPId(string TAPId)
        {
            var result = await service.GetTapJournalWithTAPId(TAPId);
            return Ok(new ApiResponse(result));
        }
        

        [HttpGet("GetJournalTmpAllFromTAP/{invoiceReceivedId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetJournalTmpAllFromTAP( string invoiceReceivedId)
        {
            var result = await service.GetJournalTmpAllFromTAP(invoiceReceivedId);
            return Ok(new ApiResponse(result));
        }
        [HttpGet("GetJournalTmpAllFromTAPHeader/{invoiceReceivedId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetJournalTmpAllFromTAPHeader(string invoiceReceivedId)
        {
            var result = await service.GetJournalTmpAllFromTAPHeader(invoiceReceivedId);
            return Ok(new ApiResponse(result));
        }

        [HttpGet("GetTapFromINV/{invoiceReceivedId}")]
        [ActionRole(ActionMethod.Read)]
        public async Task<IActionResult> GetTapFromINV(string invoiceReceivedId)
        {
           
            try
            {
                var result = await service.GetTapFromINV(invoiceReceivedId);
                return Ok(new ApiResponse(result));
            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));

            }
        }
        [HttpPost("Post")]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> Post(TemporaryAccountPayableDTO dto)
        {
            try
            {
                //manual
                // await service.AddWithDetail(dto);
                //withsp
                await service.AddWithDetailSP(dto.Header.InvoiceReceivableId);
                return Ok(new ApiResponse(dto, true, Messages.SuccessAdd));
            }
            catch (Exception ex) {
                return Ok(new ApiResponse(false, ex.Message.ToString()));
                
            }  
        }

        [HttpPost("PostData")]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> Post(TemporaryAccountPayable tap)
        {
            try
            {
                var TapNumber = await service.AddWithDetailSP(tap.InvoiceReceivableId);
                await service.CreateAutomaticJournalFromTAPNumber(TapNumber);
                return Ok(new ApiResponse(tap, true, Messages.SuccessAdd));
            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));

            }
        }

        [HttpPost("PostDataINV")]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> PostDataINV(InvoiceReceived InvoiceReceivable)
        {
            try
            {
                var TapNumber = await service.AddWithDetailSPPO(InvoiceReceivable.Id, InvoiceReceivable.PoId);
                await service.CreateAutomaticJournalFromTAPNumber(TapNumber);
                return Ok(new ApiResponse(InvoiceReceivable, true, Messages.SuccessAdd));
            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));

            }
        }

        [HttpPost("PostEdit")]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> PostEdit(TemporaryAccountPayableDTO dto)
        {
            try
            {
                //manual
                await service.AddWithDetail(dto);
                //withsp
                //await service.AddWithDetailSP(dto.Header.InvoiceReceivableId);
                return Ok(new ApiResponse(dto, true, Messages.SuccessAdd));
            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));

            }
        }
        [HttpPost("CreateAutomaticJournal")]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> CreateAutomaticJournal(TemporaryAccountPayable TapId)
        {
            try
            {
                await service.CreateAutomaticJournal(TapId.Id);
                return Ok(new ApiResponse(true, Messages.SuccessAdd));
            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));

            }
        }
        [HttpPost("CreateAPData")]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> CreateAPData(TemporaryAccountPayable TapId)
        {

            try
            {
                var newId = Guid.NewGuid().ToString();
                await service.CreateAPData(TapId.Id, newId);
                await service.CreateAPJournal(newId);

                return Ok(new ApiResponse(true, Messages.SuccessAdd));
            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));

            }

        }
        [HttpPost("ConfirmJournal")]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> ConfirmJournal(TemporaryAccountPayable TapId)
        {
            
                try
                {
                    await service.CreateAutomaticJournal(TapId.Id);
                    
                    return Ok(new ApiResponse(true, Messages.SuccessAdd));
                }
                catch (Exception ex)
                {
                    return Ok(new ApiResponse(false, ex.Message.ToString()));

                }
        }

        [HttpPost("ConfirmEditJournal")]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> ConfirmEditJournal(TemporaryAccountPayableDTO TapDto)
        {

            try
            {
                await service.EditAutomaticJournal(TapDto);

                return Ok(new ApiResponse(true, Messages.SuccessAdd));
            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));

            }

        }

        [HttpPost("TaxVerification")]
        [ActionRole(ActionMethod.Add)]
        public async Task<IActionResult> TaxVerification(TemporaryAccountPayable TapId)
        {
            try
            {
                TapId.Status = InvoiceReceivedStatus.TAXTAP;
                await service.ChangeStatusInvoiceReceived(TapId.Id, TapId.Status);
                return Ok(new ApiResponse(true, Messages.SuccessAdd));
            }
            catch (Exception ex)
            {
                return Ok(new ApiResponse(false, ex.Message.ToString()));

            }
        }
    }
}