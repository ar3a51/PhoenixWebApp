﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phoenix.Data.Models.Um;
using Phoenix.Data.Models.ViewModel;
using Phoenix.Data.Services.Um;
using Phoenix.Data.RestApi;
using Phoenix.ApiExtension.Helpers;
using System.Linq;
using System.Collections.Generic;
using Phoenix.Data.Attributes;
using Phoenix.Data.Models;

namespace PhoenixWebApi.Controllers.Core
{
    [Route("api/ManageUser")]
    [ApiController]
    [ApiExplorerSettings(IgnoreApi = false, GroupName = @"Um")]
    public class ManageUserController : Controller
    {
        private readonly IManageUserAppService _repo;
        private readonly IManageGroupAccessService _repoGa;
        private readonly IManageUserAccessService _repoUa;
        public ManageUserController(IManageUserAppService repo, IManageGroupAccessService repoGa, IManageUserAccessService repoUa)
        {
            _repo = repo;
            _repoGa = repoGa;
            _repoUa = repoUa;
        }

        [AllowAnonymousRole]
        [HttpGet]
        [Route("GetEmployee")]
        [Route("GetEmployee/{key?}")]
        public async Task<IActionResult> GetEmployee(string key = "")
        {
            return Ok(await _repo.GetDataEmployee(key).ConfigureAwait(false));
        }

        [AllowAnonymousRole]
        [HttpGet]
        [Route("GetListUserByGroupId/{groupId?}")]
        public async Task<IActionResult> GetListUserByGroupId(string groupId)
        {
            return Ok(await _repo.GetListUserByGroupId(groupId).ConfigureAwait(false));
        }

        [AllowAnonymousRole]
        [HttpGet]
        [Route("Get")]
        [Route("Get/{key?}")]
        public async Task<IActionResult> Get(string key = "")
        {
            return Ok(await _repo.GetBySearch(key).ConfigureAwait(false));
        }

        [AllowAnonymousRole]
        [HttpPost]
        [Route("Save")]
        public async Task<IActionResult> Save([FromBody]UserAppViewModel data)
        {
            if (!ModelState.IsValid)
            {
                return Ok(new ApiResponse(false, "data not valid"));
            }
            var model = await _repo.CekIdIsReady(data.Id);

            if (model == null)
            {
                model = new TmUserApp();
                model.Id = data.Id;
                model.AliasName = data.AliasName;
                model.Email = data.Email;
                model.UserPwd = SecurityHelper.EncryptString("Nava12345");
                model.EmployeeBasicInfoId = data.EmployeeBasicInfoId;
                model.GroupId = data.GroupId;
                model.IsMsUser = data.IsMsUser;
                model.CreatedOn = DateTime.Now;
                await _repo.AddAsync(model).ConfigureAwait(false);

                //additional New...atas permintaan Argantha, default user by Group access di user access nya...
                await _repoUa.DeleteByUserIdAsync(data.Id);
                var listGa = await _repoGa.GetById(data.GroupId);
                foreach (var item in listGa.ToList())
                {
                    var modelUA = new TmUserAccess();
                    modelUA.Id = Guid.NewGuid().ToString();
                    modelUA.MenuId = item.MenuId;
                    modelUA.GroupId = item.GroupId;
                    modelUA.UserId = data.Id;
                    modelUA.IsRead = item.IsRead;
                    modelUA.IsAdd = item.IsAdd;
                    modelUA.IsEdit = item.IsEdit;
                    modelUA.IsDelete = item.IsDelete;
                    await _repoUa.AddAsync(modelUA).ConfigureAwait(false);
                }

                //SendEmail.SendMail(data.Id, "Nava12345");
                return Ok(new ApiResponse(true, Messages.SuccessAdd));
            }
            else
            {
                model.AliasName = data.AliasName;
                model.Email = data.Email;
                model.EmployeeBasicInfoId = data.EmployeeBasicInfoId;
                model.GroupId = data.GroupId;
                model.IsMsUser = data.IsMsUser;
                model.ModifiedOn = DateTime.Now;
                model.IsDeleted = false;
                //model.UserPwd = SecurityHelper.EncryptString("Nava12345");
                await _repo.EditAsync(model).ConfigureAwait(false);
                //additional New...atas permintaan Argantha, default user by Group access di user access nya...
                await _repoUa.DeleteByUserIdAsync(data.Id);// delete UserAccess By UserId , di delete user access di group sebelum nya...
                var listGa = await _repoGa.GetById(data.GroupId);
                foreach (var item in listGa.ToList())
                {
                    var modelUA = new TmUserAccess();
                    modelUA.Id = Guid.NewGuid().ToString();
                    modelUA.MenuId = item.MenuId;
                    modelUA.GroupId = item.GroupId;
                    modelUA.UserId = data.Id;
                    modelUA.IsRead = item.IsRead;
                    modelUA.IsAdd = item.IsAdd;
                    modelUA.IsEdit = item.IsEdit;
                    modelUA.IsDelete = item.IsDelete;
                    await _repoUa.AddAsync(modelUA).ConfigureAwait(false);
                }


                //SendEmail.SendMail(data.Id, "Nava12345");
                return Ok(new ApiResponse(true, Messages.SuccessEdit));
            }
        }



        [AllowAnonymousRole]
        [HttpPost]
        [Route("ChangePword")]
        public async Task<IActionResult> ChangePword([FromBody]ChangePwordVm data)
        {
            if (!ModelState.IsValid)
            {
                return Ok(new ApiResponse(false, "data not valid"));
            }
            var userId = Phoenix.Shared.Core.PrincipalHelpers.ClainJwtPrincipalHelper.GetUserId(HttpContext);
            var model = await _repo.Get(userId);
            model.UserPwd = SecurityHelper.EncryptString(data.newpwd);
            model.ModifiedOn = DateTime.Now;
            await _repo.EditAsync(model);

            return Ok(new ApiResponse(true, Messages.SuccessEdit));
        }

        [AllowAnonymousRole]
        [HttpPost]
        [Route("BulkSave")]
        public async Task<IActionResult> BulkSave([FromBody]List<UserAppViewModel> listData)
        {
            if (!ModelState.IsValid)
            {
                return Ok(new ApiResponse(false, "data not valid"));
            }
            foreach (var data in listData)
            {
                var model = await _repo.CekIdIsReady(data.Id);

                if (model == null)
                {
                    model = new TmUserApp();
                    model.Id = data.Id;
                    model.AliasName = data.AliasName;
                    model.Email = data.Email;
                    model.UserPwd = SecurityHelper.EncryptString("Nava12345");
                    model.EmployeeBasicInfoId = data.EmployeeBasicInfoId;
                    model.GroupId = data.GroupId;
                    model.IsMsUser = data.IsMsUser;
                    model.CreatedOn = DateTime.Now;
                    await _repo.AddAsync(model).ConfigureAwait(false);

                    //additional New...atas permintaan Argantha, default user by Group access di user access nya...
                    await _repoUa.DeleteByUserIdAsync(data.Id);
                    var listGa = await _repoGa.GetById(data.GroupId);
                    foreach (var item in listGa.ToList())
                    {
                        var modelUA = new TmUserAccess();
                        modelUA.Id = Guid.NewGuid().ToString();
                        modelUA.MenuId = item.MenuId;
                        modelUA.GroupId = item.GroupId;
                        modelUA.UserId = data.Id;
                        modelUA.IsRead = item.IsRead;
                        modelUA.IsAdd = item.IsAdd;
                        modelUA.IsEdit = item.IsEdit;
                        modelUA.IsDelete = item.IsDelete;
                        await _repoUa.AddAsync(modelUA).ConfigureAwait(false);
                    }

                }
                else
                {
                    model.AliasName = data.AliasName;
                    model.Email = data.Email;
                    model.EmployeeBasicInfoId = data.EmployeeBasicInfoId;
                    model.GroupId = data.GroupId;
                    model.IsMsUser = data.IsMsUser;
                    model.ModifiedOn = DateTime.Now;
                    model.IsDeleted = false;
                    //model.UserPwd = SecurityHelper.EncryptString("Nava12345");
                    await _repo.EditAsync(model).ConfigureAwait(false);
                    //additional New...atas permintaan Argantha, default user by Group access di user access nya...
                    await _repoUa.DeleteByUserIdAsync(data.Id);// delete UserAccess By UserId , di delete user access di group sebelum nya...
                    var listGa = await _repoGa.GetById(data.GroupId);
                    foreach (var item in listGa.ToList())
                    {
                        var modelUA = new TmUserAccess();
                        modelUA.Id = Guid.NewGuid().ToString();
                        modelUA.MenuId = item.MenuId;
                        modelUA.GroupId = item.GroupId;
                        modelUA.UserId = data.Id;
                        modelUA.IsRead = item.IsRead;
                        modelUA.IsAdd = item.IsAdd;
                        modelUA.IsEdit = item.IsEdit;
                        modelUA.IsDelete = item.IsDelete;
                        await _repoUa.AddAsync(modelUA).ConfigureAwait(false);
                    }
                }
            }
            return Ok(new ApiResponse(true, Messages.SuccessAdd));

        }


        [AllowAnonymousRole]
        [HttpGet]
        [Route("Delete/{Id?}")]
        public async Task<IActionResult> Delete(string Id)
        {
            var model = await _repo.Get(Id).ConfigureAwait(false);
            await _repo.DeleteSoftAsync(model);
            return Ok(new ApiResponse(true, Messages.SuccessDelete));
        }

        [AllowAnonymousRole]
        [HttpGet]
        [Route("DeleteFromGroup/{groupId?}/{userId?}")]
        public async Task<IActionResult> DeleteFromGroup(string groupId, string userId)
        {
            await _repoUa.DeleteByUserIdAsync(userId).ConfigureAwait(false);
            var model = await _repo.Get(userId);
            model.GroupId = string.Empty;
            await _repo.EditAsync(model).ConfigureAwait(false);

            return Ok(new ApiResponse(true, Messages.SuccessDelete));
        }

        /// <summary>
        /// Kebutuhan untuk detail form request....
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        [AllowAnonymousRole]
        [HttpGet]
        [Route("IsHCAdmin")]
        public async Task<IActionResult> IsHCAdmin()
        {
            var group = await _repo.GetByUserLogin();
            var HCAdmin = group.Where(x => x.GroupId == GroupAcess.HCAdmin).Count() > 0;
            return Ok(HCAdmin);
        }

        /// <summary>
        /// Kebutuhan untuk detail form request....
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        [AllowAnonymousRole]
        [HttpGet]
        [Route("GetByUserLogin")]
        public async Task<IActionResult> GetByUserLogin()
        {
            var group = await _repo.GetByUserLogin();
            return Ok(new ApiResponse(group));
        }
    }
}