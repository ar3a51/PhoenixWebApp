﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phoenix.ApiExtension;
using Phoenix.Data.Attributes;
using Phoenix.Data.MsGraphExtention;

namespace PhoenixWebApi.Controllers.Core
{
    [Route("api/odrive")]
    [ApiController]
    [ApiExplorerSettings(IgnoreApi = false, GroupName = @"Um")]
    public class OneDriveOfficeController : Controller
    {
        private IHostingEnvironment environment;
        //private string office365Username = DataConfiguration.Configuration.MsGraphSettings.office365Username;
        //private string office365Password = DataConfiguration.Configuration.MsGraphSettings.office365Password;
        private readonly Phoenix.Data.Services.Um.IManageAzurePortalSetupService _repoAzure;
        public OneDriveOfficeController(IHostingEnvironment environment, Phoenix.Data.Services.Um.IManageAzurePortalSetupService repoAzure)
        {
            this.environment = environment;
            this._repoAzure = repoAzure;
        }

        [AllowAnonymousRole]
        [HttpGet]
        [Route("getroot")]
        [Route("getroot/{folder?}")]
        public async Task<IActionResult> GetRootFile(string folder = "")
        {
            if (!string.IsNullOrEmpty(folder))
                folder = folder.Replace('*', '/');

            var model = _repoAzure.GetOnlyOne().Result;
            var graph = new MsGraph(model.ClientId,model.TenantId);
            var authResult = await graph.GetAuthentication(model.EmailAccount, model.SecurityPword);
            var graphClient = graph.CreateGraphServiceClient(authResult.AccessToken);
            //AMBIL SEMUA FILE DAN FOLDER => DI ROOT USER YANG LOGIN
            if (string.IsNullOrEmpty(folder))
                return Ok(await graphClient.Me.Drive.Root.Children.Request().GetAsync());
            else
                return Ok(await graphClient.Me.Drive.Root.ItemWithPath("/" + folder).Children.Request().GetAsync());
        }

        [AllowAnonymousRole]
        [HttpGet]
        [Route("download/{path?}")]
        [EnableCors("AllowAll")]
        public async Task<IActionResult> Download(string path = "")
        {
            if (!string.IsNullOrEmpty(path))
                path = path.Replace('*', '/');

            var model = _repoAzure.GetOnlyOne().Result;
            var graph = new MsGraph(model.ClientId, model.TenantId);
            var authResult = await graph.GetAuthentication(model.EmailAccount, model.SecurityPword);
            var graphClient = graph.CreateGraphServiceClient(authResult.AccessToken);
            var stream = await graphClient.DownloadFile("/" + path, false);
            var arr = path.Split('/');
            var filename = arr[arr.Length - 1];
            var pathRelative = HttpContext.Request.Scheme + "://" + HttpContext.Request.Host;

            var pathDownload = Path.Combine(environment.WebRootPath, "fileOneDrive");
            using (var ms = new MemoryStream())
            {
                stream.CopyTo(ms);
                FileStream file = new FileStream(pathDownload + "\\" + filename, FileMode.Create, FileAccess.Write);
                ms.WriteTo(file);
                file.Close();
                ms.Close();
            }
            var fullpath = pathRelative + "\\fileOneDrive\\" + filename;
            return Ok(new { path = fullpath });
        }

        [AllowAnonymousRole]
        [HttpPost]
        [Route("uploadtodrive")]
        public async Task<IActionResult> UploadToDrive([FromForm]VmUploadDrive data)
        {
            if (!string.IsNullOrEmpty(data.Path))
                data.Path = data.Path.Replace('*', '/');

            if (!string.IsNullOrEmpty(data.PathFullWithFileName))
                data.PathFullWithFileName = data.PathFullWithFileName.Replace('*', '/');

            var model = _repoAzure.GetOnlyOne().Result;
            var graph = new MsGraph(model.ClientId, model.TenantId);
            var authResult = await graph.GetAuthentication(model.EmailAccount, model.SecurityPword);

            var graphClient = graph.CreateGraphServiceClient(authResult.AccessToken);
            var file = data.File.OpenReadStream();
            var upload = await graphClient.UploadLargeFile(file, data.PathFullWithFileName, false);

            if (string.IsNullOrEmpty(data.Path))
                return Ok(await graphClient.Me.Drive.Root.Children.Request().GetAsync());
            else
                return Ok(await graphClient.Me.Drive.Root.ItemWithPath("/" + data.Path).Children.Request().GetAsync());

            //return Ok("data success");
        }
    }
    public class VmUploadDrive
    {
        public string Path { get; set; }
        public string PathFullWithFileName { get; set; }
        public IFormFile File { get; set; }
        public bool IsBig { get; set; }
    }
}