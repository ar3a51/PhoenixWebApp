﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phoenix.Data.Models.Um;
using Phoenix.Data.Models.ViewModel;
using Phoenix.Data.Services.Um;
using Phoenix.Data.RestApi;
using Phoenix.Data.Attributes;

namespace PhoenixWebApi.Controllers.Core
{
    [Route("api/ManageMenu")]
    [ApiController]
    [ApiExplorerSettings(IgnoreApi = false, GroupName = @"Um")]
    public class ManageMenuController : Controller
    {
        private readonly IManageMenuService _repo;
        public ManageMenuController(IManageMenuService repo)
        {
            _repo = repo;
        }

        [AllowAnonymousRole]
        [HttpGet]
        [Route("Get")]
        [Route("Get/{key}")]
        public async Task<IActionResult> Get(string key = "")
        {
            //var data =  Phoenix.Shared.Core.PrincipalHelpers.ClainJwtPrincipalHelper.GetUserId(this.HttpContext);
            return Ok(await _repo.GetBySearch(key).ConfigureAwait(false));
        }

        [AllowAnonymousRole]
        [HttpPost]
        [Route("Save")]
        public async Task<IActionResult> Save([FromBody]MenuViewModel data)
        {
            if (!ModelState.IsValid)
            {
                return Ok(new ApiResponse(false, "data not valid"));
            }
            var model = new TmMenu();
            model.MenuName = data.MenuName.Replace(System.Environment.NewLine, data.MenuName.TrimStart().TrimEnd());
            model.MenuLink = data.MenuLink;
            model.MenuUnique = data.MenuUnique;
            model.ParentId = data.ParentId.Replace(System.Environment.NewLine, data.ParentId.TrimStart().TrimEnd());
            if (data.Id == "0")
            {
                model.CreatedOn = DateTime.Now;
                var id = Guid.NewGuid().ToString();
                model.Id = id.Replace(System.Environment.NewLine, id.TrimStart().TrimEnd());
                await _repo.AddAsync(model).ConfigureAwait(false);
                return Ok(new ApiResponse(true, Messages.SuccessAdd));
            }
            else
            {
                data.Id = data.Id.Replace(System.Environment.NewLine, data.Id.TrimStart().TrimEnd());
                var dataMenu = await _repo.Get(data.Id).ConfigureAwait(false);
                dataMenu.MenuName = data.MenuName;
                dataMenu.MenuLink = data.MenuLink;
                dataMenu.MenuUnique = data.MenuUnique;
                dataMenu.ParentId = data.ParentId;
                dataMenu.ModifiedOn = DateTime.Now;
                await _repo.EditAsync(dataMenu).ConfigureAwait(false);
                return Ok(new ApiResponse(true, Messages.SuccessEdit));
            }
        }

        [AllowAnonymousRole]
        [HttpGet]
        [Route("Delete/{Id}")]
        public async Task<IActionResult> Delete(string Id)
        {
            var model = await _repo.Get(Id).ConfigureAwait(false);
            await _repo.DeleteSoftAsync(model);
            return Ok(new ApiResponse(true, Messages.SuccessDelete));
        }
    }

}