﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phoenix.Data.Services.Um;
using Phoenix.Data.Attributes;
using Phoenix.Data.Models.ViewModel;
using PhoenixWebApi.Services;
using Phoenix.Data.Models;

namespace PhoenixWebApi.Controllers.Core
{
    [Route("api/notifycenter")]
    [ApiController]
    [ApiExplorerSettings(IgnoreApi = false, GroupName = @"Um")]
    public class NotifyCenterController : Controller
    {
        private readonly INotificationService _repo;
        private readonly INotificationCenter _notify;
        public NotifyCenterController(INotificationService repo, INotificationCenter notify)
        {
            _repo = repo;
            _notify = notify;
        }

        [AllowAnonymousRole]
        [HttpGet]
        [Route("Get")]
        public async Task<IActionResult> Get()
        {
            var userId = Phoenix.Shared.Core.PrincipalHelpers.ClainJwtPrincipalHelper.GetUserId(HttpContext);
            return Ok(await _repo.GetListNotificationByUserCode(userId));
        }

        [AllowAnonymousRole]
        [HttpPost]
        [Route("UpdateIsSeen")]
        public async Task<IActionResult> UpdateIsSeen([FromBody]CodeNotifyVm data)
        {
            var model = await _repo.Get(data.Codex);
            model.IsSeen = true;
            await _repo.EditAsync(model);
            return Ok(new { msg ="success update seen"});
        }

        private void CaraMemakaiFunctionNotification()
        {
            //instan Interface INotificationCenter in Controller API or Class..like in this controller...

            //call function with overload entity class parameter...
            var model = new Notification();
            //just this propertie, dont need else..
            model.NotificationTypeId = "pm.approval-team";//this is just sample..
            model.UserId = "delegeate to who member";// ....value must be UserAppId
            model.ItemId = "I dont know what this mean";
            model.Module = "MOdule name like HRIS | PM | FINANCE | OTHERS";
            ///Juist that...
            Task.Run(()=> _notify.AddNotification(model));

            //Overload MEthod value parameter...
            Task.Run(() => _notify.AddNotification("Notification Type","UserAppId","ITem Id","Module Name"));
            //Or just this...
            Task.Run(() => _notify.AddNotification("Notification Type", "UserAppId", "Module Name"));


        }
    }
}