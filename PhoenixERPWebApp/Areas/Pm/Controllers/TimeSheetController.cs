﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Phoenix.WebExtension.RestApi;
using PhoenixERPWebApp.Models;
using PhoenixERPWebApp.Models.ProjectManagement;

namespace PhoenixERPWebApp.Areas.Pm.Controllers
{
    [Area(nameof(Pm))]
    public class TimeSheetController : Controller
    {
        string url = ApiUrl.TimeSheetUrl;
        private readonly ApiClientFactory client;

        public TimeSheetController(ApiClientFactory _client)
        {
            this.client = _client;
        }
        public async Task<IActionResult> Index()
        {
            var requestor = await client.Get<InfoEmployee>($"{ApiUrl.EmployeeInfoUrl}/getByUserLogin");
            ViewBag.Name = requestor.EmployeeName;
            ViewBag.BusineesUnit = requestor.BusinessUnitId;
            ViewBag.Division = requestor.Division;
            Timesheet data = new Timesheet();
            return View(data);
        }

        public async Task<DataTable> GetDataTable(string date)
        {
            var data = await client.Get($"{url}/ListTimeSheet/{date}");
            DataTable result = (DataTable)JsonConvert.DeserializeObject(data, (typeof(DataTable)));
            return result;
        }

        public async Task<IActionResult> Read([DataSourceRequest] DataSourceRequest request, string date)
        {
            DataTable data = await GetDataTable(date);
            string json = JsonConvert.SerializeObject(data);
            List<Timesheet> convertData = JsonConvert.DeserializeObject<List<Timesheet>>(json);
            var result = convertData.ToDataSourceResult(request);
            return Json(result, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> Update([DataSourceRequest] DataSourceRequest request, Timesheet data, string start_date, string end_date,
            string Sunday, string Monday, string Tuesday, string Wednesday, string Thursday, string Friday, string Saturday)
        {
            data.start_date = DateTime.ParseExact(start_date, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture).Date;
            data.end_date = DateTime.ParseExact(end_date, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture).Date;
            data.Sunday = DateTime.ParseExact(Sunday, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
            data.Monday = DateTime.ParseExact(Monday, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
            data.Tuesday = DateTime.ParseExact(Tuesday, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
            data.Wednesday = DateTime.ParseExact(Wednesday, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
            data.Thursday = DateTime.ParseExact(Thursday, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
            data.Friday = DateTime.ParseExact(Friday, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
            data .Saturday = DateTime.ParseExact(Saturday, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
            await client.Post($"{url}/UpdateTimeSheet",data);
            return Json(data);
        }
        
        public async Task<IActionResult> Submit(string id)
        {
            await client.Post($"{url}/SubmitTimeSheet/", id);
            return View("Index");
        }
    }
}