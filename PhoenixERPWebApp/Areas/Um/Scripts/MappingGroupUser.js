﻿let loadData = function () {
    let dataSearch = $('.searchbox').val();
    let urlApi = $.helper.resolveApi("~ManageGroupAccess/GetListUserHavingGroup");

    mApp.blockPage({
        overlayColor: "#000000",
        type: "loader",
        state: "primary",
        message: "Processing..."
    }),
        $.ajax({
        type: "GET",
        dataType: 'json',
        url: urlApi,
        success: function (data) {
            sessionStorage.setItem('dataGrouping', JSON.stringify(data));
            bindDropdownGroup(data);
            bindListGrouping(data);
            },
        error: function (e, t, s) {
                swal(
                    'Information',
                    'Ooops, something went wrong !',
                    'info'
                );
            }
        }).then(setTimeout(function () {
            mApp.unblockPage();
        }, 2e3));
};

let bindDropdownGroup = function (data) {
    var ddl = $('#ddlGroup');
    ddl.empty();
    $(ddl).append($('<option>', {
        value: "0",
        text: "ALL"
    }));

    $.each(data, function (i, item) {
        $(ddl).append($('<option>', {
            value: item.id,
            text: item.groupName
        }));
    });
};


let bindListGrouping = function (data) {
    let divTemp = $('#dvLgm');
    divTemp.empty();
    $.each(data, function (x, r) {
        let result = templateTable(r);
        divTemp.append(result);
    });
};

let templateTable = function (data) {

    let tbl = '<table class="table table-bordered">' +
        '<thead>' +
        '        <tr>' +
        '            <th>' +
        '                <button class="m-btn btn btn-primary btn-sm m-btn--sm" onclick="addUserToGroup(\'' + data.id + '\',\'' + data.groupName + '\')" title="add new user to this group ' + data.groupName + '">' +
        '                    <i class="la la-plus"></i>' +
        '                </button>&nbsp;&nbsp;' +
        '                        <i class="fa fa-users" style="color: #47b7bc"></i>&nbsp;&nbsp;<b class="text-uppercase">' + data.groupName + '</b>' +
        '            <span class="pull-right">Total : ' + data.listUsers.length + '</span>' +
        '        </th>' +
        '    </tr>' +
        '                </thead>' +
        '                    <tbody> <tr><td><div style="max-height:200px;overflow:auto">';
    if (data.listUsers.length === 0) {
        tbl = tbl + '<span class="col text-center">there is no user in this group<span>';
    }
    else {
        tbl = tbl + '    <table class="table table-striped table-hover">' +
        '        <thead>' +
        '            <tr>' +
        '                <th>#NO</th>' +
        '                <th>#EMPLOYEE NAME</th>' +
        '                <th>#NICK NAME</th>' +
        '                <th>#EMAIL</th>' +
        '                <th>#JOB TITLE</th>' +
        '                <th>#DIVISION</th>' +
        '                <th>#<i class="fa fa-cogs"></i></th>' +
        '            </tr>' +
        '        </thead>' +
        '        <tbody>';
    let no = 1;
    $.each(data.listUsers, function (x, r) {
        tbl = tbl + '<tr>' +
            '                <td>'+no+'</td>' +
            '                <td>' + r.employeeName+'</td>' +
            '                <td>' + r.nickName +'</td>' +
            '                <td>' + r.email +'</td>' +
            '                <td>' + r.jobTitleName +'</td>' +
            '                <td>' + r.divisiName +'</td>' +
            '                <td>' +
            '                    <button class="btn btn-sm m-btn--sm md-btn-icon-default btn-default" onclick="editUserAccess(\'' + r.groupId + '\',\'' + r.userId + '\',\'' + r.employeeName + '\',\'' +data.groupName + '\')"><i class="fa fa-edit"></i></button>' +
            '                    <button class="btn btn-sm m-btn--sm md-btn-danger btn-danger"  onclick="deleteUserFromGroup(\'' + r.groupId + '\',\'' + r.userId + '\')"><i class="fa fa-times"></i></button>' +
            '                </td>' +
            '            </tr>';
        no++;
    });

        tbl = tbl + '</tbody></table> ';
    }
           '</div></td></tr>'+
           '</tbody> '+
        '</table>';

    return tbl;
};

let addUserToGroup = function (groupId,groupName) {
    sessionStorage.setItem('setToGroup', groupId);
    sessionStorage.setItem('setToGroupName', groupName);
    sessionStorage.removeItem('setUserToGroup');
    sessionStorage.removeItem('dataGrouping');
    
    window.location.href = '/um/mappinggroupuser/adduser';
};
let editUserAccess = function (groupId,userId,empName,groupName) {
    sessionStorage.setItem('setToGroup', groupId);
    sessionStorage.setItem('setUserToGroup', userId);
    sessionStorage.setItem('setToGroupName', groupName);
    sessionStorage.setItem('setToEmpName', empName);
    sessionStorage.removeItem('dataGrouping');
    window.location.href = '/um//manageuseraccess/edituseraccess';
};

let deleteUserFromGroup = function (groupid,userid) {
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't delete this user from group!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
         $.ajax({
             type: "GET",
             dataType: 'json',
             url: $.helper.resolveApi("~ManageUser/DeleteFromGroup/" + groupid + "/" + userid),
             success: function (data) {
                 toastr.success('Data has been success remove', "Information");          
                 loadData();
             },
             error: function (e) {
                 console.log(e);
                 swal(
                     'Information',
                     'Ooops, something went wrong !',
                     'info'
                 );
             }
         });
        }
    });
    
};

let selectedChangeGroup = function (groupId) {
    let ds = sessionStorage.getItem('dataGrouping');
    ds = JSON.parse(ds);
    if (ds === null || ds === undefined) ds = [];

    if (groupId === "0") {
        bindListGrouping(ds);
    }
    else {
        let data = ds.find(x => x.id === groupId);
        let newds = [];
        newds.push(data);
        bindListGrouping(newds);
    }
};
$(document).ready(function () {
    sessionStorage.removeItem('dataGrouping');
    loadData();
    $('#ddlGroup').select2({
        allowClear: true,
        placeholder :'ALL'
    });
    $('#ddlGroup').on('select2:select', function (e) {
        if (e.params.data.id === undefined)
            selectedChangeGroup("0");
        else
            selectedChangeGroup(e.params.data.id);
    });
});
