﻿let loadData = function () {
    let dataSearch = $('.searchbox').val();
    let urlApi = $.helper.resolveApi("~ManageGroup/Get/" + dataSearch);
    if (dataSearch.length === 0) urlApi = $.helper.resolveApi("~ManageGroup/Get");

    mApp.blockPage({
        overlayColor: "#000000",
        type: "loader",
        state: "primary",
        message: "Processing..."
    }),
        $.ajax({
        type: "GET",
        dataType: 'json',
        url: urlApi,
        success: function (data) {
            if (data.length === 0) {
                bindEmptyTable();
            }
            else {
                bindTable(data);
                if ($.fn.dataTable.isDataTable('#dt_basic') === false) {
                    $('#dt_basic').dataTable({ pageLength: 10 });
                }
            }
            },
            error: function (e, t, s) {
                swal(
                    'Information',
                    'Ooops, something went wrong !',
                    'info'
                );
            }
        }).then(setTimeout(function () {
            mApp.unblockPage();
        }, 2e3));
};

let bindEmptyTable = function () {
    $('#dt_basic> tbody').empty();
    let table = $('#dt_basic > tbody:last-child');
    table.append('<tr><td colspan="2" class="text-center">there is no data</td></tr>');
};

let bindTable = function (data) {
    $('#dt_basic> tbody').empty();
    let table = $('#dt_basic > tbody:last-child');
    $.each(data, function (r, val) {
        var row = "<tr>";
        //row = row + "<td><label class='m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--brand lb-item-" + val.id +"'>";
        //row = row + "<input type='checkbox' value='' class='m-group-checkable cb-item' id='cbItem-"+val.id+"'>";
        //row = row + "<span></span>";
        //row = row + "</label>";
        //row = row + "</td>";
        row = row + "<td>" + val.groupName + "</td>";
        row = row + "<td>";
        row = row + "<button class='m-btn btn btn-danger btn-sm m-btn--sm pull-right' title='delete' type='button' onclick='removeData(\"" + val.id +"\")'><i class='fa fa-trash-alt'></i></button>";
        row = row + "<button class='m-btn btn btn-default btn-sm m-btn--sm pull-right' title='edit' type='button' onclick='editedData(\"" + val.id + "\",\"" + val.groupName +"\")'><i class='fa fa-edit'></i></button>";
        row = row + "</td>";
        row = row + "</tr>";
        table.append(row);
    });
};

let selectedAll = function () {
    let cbhead = $('#cbHead').prop('checked');
    let $table = $('#dt_basic');
    var $tdCheckbox = $table.find('tbody input:checkbox'); // checboxes inside table body   
    $.each($tdCheckbox, function (i, d) {
        $(this).trigger("click");
    });
    $tdCheckbox.prop('checked', cbhead);
};

let saveData = function () {
    if ($('#tbGroupName').val() === '') {
        swal(
            'Information',
            'Group Name cannot empty!',
            'error'
        );
        return false;
    }

    let model = { Id: $('#hidIdGroup').val(), GroupName: $('#tbGroupName').val() };

    $.ajax({
        type: "POST",
        dataType: 'json',
        contentType: 'application/json',
        url: $.helper.resolveApi("~/ManageGroup/Save"),
        data: JSON.stringify(model),
        success: function (r) {
            $('#hidIdGroup').val('0');
            $('#tbGroupName').val('');
            $('#myModal').modal('hide');
            toastr.success('Data has been success save', "Information");
            window.location.href = "/um/managegroup";
        },
        error: function (r) {
            console.log('masuk error');
            console.log(r);
            swal(
                '' + r.status,
                r.statusText,
                'error'
            );
        }
    });

};


let editedData = function (id, groupName) {
    $('#hidIdGroup').val(id);
    $('#tbGroupName').val(groupName);    
    $('#myModal').modal('show');
};

let removeData = function (id) {
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
         $.ajax({
             type: "GET",
             dataType: 'json',
             url: $.helper.resolveApi("~ManageGroup/Delete/" + id),
             success: function (data) {
                 window.location.href = "/um/managegroup";
             },
             error: function (e, t, s) {
                 swal(
                     'Information',
                     'Ooops, something went wrong !',
                     'info'
                 );
             }
         });
        }
    });

    //return swal({
    //    title: 'Information',
    //    text: 'are you sure want delete this record?',
    //    type: 'warning',
    //    showCancelButton: true,
    //    confirmButtonColor: '#5cb85c',
    //    confirmButtonText: 'OK',
    //    cancelButtonText: 'No',
    //    closeOnConfirm: false,
    //    closeOnCancel: false
    //},
    //    function (isConfirm) {
    //        console.log('isConfirm :' + isConfirm);
    //        if (isConfirm) {
    //            $.ajax({
    //                type: "GET",
    //                dataType: 'json',
    //                url: $.helper.resolveApi("~ManageGroup/Delete/" + id),
    //                success: function (data) {
    //                    loadData();
    //                },
    //                error: function (e, t, s) {
    //                    swal(
    //                        'Information',
    //                        'Ooops, something went wrong !',
    //                        'info'
    //                    );
    //                }
    //            });
    //        }
    //    });
};

let openForm = function () {
    $('#myModal').modal('show');
    $('#hidIdGroup').val('0');
    $('#tbGroupName').val('');    
};

$(document).ready(function () {
    loadData();
    $('.cb-head').on('click', function () { selectedAll(); });
    $('.btn-save').on('click', function () { saveData(); });
    
});
