﻿using Microsoft.AspNetCore.Mvc;

namespace PhoenixERPWebApp.Areas.Um.Controllers
{
    [Area(nameof(Um))]
    public class GroupChattingController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [Route("Um/GroupChatting/Add")]
        [Route("Um/GroupChatting/Add/{id}")]
        public IActionResult Add(string id = "0")
        {
            ViewBag.Codex = id;
            return View();
        }
    }
}