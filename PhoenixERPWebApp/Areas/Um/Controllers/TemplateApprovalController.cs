﻿using Microsoft.AspNetCore.Mvc;
using Phoenix.WebExtension.Attributes;

namespace PhoenixERPWebApp.Areas.Um.Controllers
{
    [Area(nameof(Um))]
    public class TemplateApprovalController : Controller
    {
        public IActionResult Index()
        {

            return View();
        }
        public IActionResult AdditionalTemplate()
        {
            return View();
        }
        
        public IActionResult AddConditional()
        {
            return View();
        }
        public IActionResult DetailConditional()
        {
            return View();
        }
    }
}