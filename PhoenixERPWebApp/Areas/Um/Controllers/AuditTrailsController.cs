﻿using Microsoft.AspNetCore.Mvc;

namespace PhoenixERPWebApp.Areas.Um.Controllers
{
    [Area(nameof(Um))]
    public class AuditTrailsController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}