﻿using Microsoft.AspNetCore.Mvc;

namespace PhoenixERPWebApp.Areas.Um.Controllers
{
    [Area(nameof(Um))]
    public class OneDriveOfficeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult ListEmail()
        {
            return View();
        }
        public IActionResult ComposeEmail()
        {
            return View();
        }
    }
}