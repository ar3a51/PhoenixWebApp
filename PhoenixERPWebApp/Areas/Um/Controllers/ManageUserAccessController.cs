﻿using Microsoft.AspNetCore.Mvc;
using Phoenix.WebExtension.Attributes;

namespace PhoenixERPWebApp.Areas.Um.Controllers
{
    [Area(nameof(Um))]
    public class ManageUserAccessController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult EditUserAccess()
        {
            return View();
        }

    }

}