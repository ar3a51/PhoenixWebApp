﻿using Microsoft.AspNetCore.Mvc;

namespace PhoenixERPWebApp.Areas.Um.Controllers
{
    [Area(nameof(Um))]
    public class ManageAzureAccountController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Add(string id = "")
        {
            if (string.IsNullOrEmpty(id)) id = "0";
            ViewBag.Codex = id;
            return View();
        }
    }
}