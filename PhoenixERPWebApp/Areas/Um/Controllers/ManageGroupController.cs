﻿using Microsoft.AspNetCore.Mvc;
using Phoenix.WebExtension.Attributes;

namespace PhoenixERPWebApp.Areas.Um.Controllers
{
    [Area(nameof(Um))]
    public class ManageGroupController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}