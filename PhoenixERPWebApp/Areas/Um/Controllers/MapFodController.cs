﻿using Microsoft.AspNetCore.Mvc;

namespace PhoenixERPWebApp.Areas.Um.Controllers
{
    [Area(nameof(Um))]
    public class MapFodController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult MappingDrive()
        {
            return View();
        }
    }
}