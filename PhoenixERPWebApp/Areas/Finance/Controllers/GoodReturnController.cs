﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Phoenix.WebExtension.RestApi;
using PhoenixERPWebApp.Models;
using PhoenixERPWebApp.Models.Finance.Transaction.GoodReturn;

namespace PhoenixERPWebApp.Areas.Finance.Controllers
{
    [Area("Finance")]
    public class GoodReturnController : Controller
    {
        string url = ApiUrl.GoodReturn;
        //string urlInvt = ApiUrl.Inventory;
        //string urlFA = ApiUrl.FixedAssetUrl;
        private readonly ApiClientFactory client;

        public GoodReturnController(ApiClientFactory client)
        {
            this.client = client;
        }
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Read([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                //List<InventoryGoodsReceipt> model = new List<InventoryGoodsReceipt>();

                var model = await client.Get<List<InventoryGoodsReturn>>($"{url}") ?? new List<InventoryGoodsReturn>();
                //if (response != null)
                //{
                //    model = CastResponseData<List<InventoryGoodsReceipt>>(response.Data);
                //}

                var data = model.ToDataSourceResult(request);
                return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }

        }

        public async Task<IActionResult> Detail([FromQuery(Name = "id")] string Id, bool isApprove)
        {
            //ClearCacheDetailInv();
            DateTime _now = DateTime.Now;
            if (isApprove.Equals(null))
            {
                isApprove = false;
            }
            var model = new InventoryGoodsReturn();

            var isCurrentApproval = false;

            ViewBag.Header = "Goods Request";

            if (!string.IsNullOrEmpty(Id))
            {
                if (isApprove == true)
                    ViewBag.Header = "Approve Good Request";
                else
                    ViewBag.Header = "Update Good Request";
                //ViewBag.RecordID = Id;
                model = await client.Get<InventoryGoodsReturn>($"{url}/{Id}");
                //isCurrentApproval = await client.Get<bool>($"{ApiUrl.ManageTransApproval}/IsCurrentApproval/{Id}");
                //ViewBag.isRequestor = isApprove == false && (model.Status == StatusTransaction.Draft);
                var requestor = await client.Get<InfoEmployee>($"{ApiUrl.EmployeeInfoUrl}/getByUserLogin");
            }
            else
            {
                ViewBag.Header = "Create Good Request";
                var requestor = await client.Get<InfoEmployee>($"{ApiUrl.EmployeeInfoUrl}/getByUserLogin");
            }

            ViewBag.processApprove = isApprove && isCurrentApproval;

            ViewBag.isApprove = isApprove;

            return View(model);
        }
    }
}