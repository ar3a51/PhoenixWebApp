﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CodeMarvel.Infrastructure.WebNetCore;
using Kendo.Mvc.UI;
using Phoenix.WebExtension.RestApi;
using PhoenixERPWebApp.Models.Finance.Transaction;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace PhoenixERPWebApp.Areas.Finance.Controllers
{
    [Area("Finance")]
    public class AccountReceivableAgingController : BaseController
    {
        private readonly ApiClientFactory client;
        readonly string ar_url = ApiUrl.ARClientUrl;

        public AccountReceivableAgingController(ApiClientFactory client)
        {
            this.client = client;
        }

        public IActionResult Index()
        {
            return View();
        }        
        
        public async Task<IActionResult> GetListAging([DataSourceRequest] DataSourceRequest request, string cl, string legal, string dv)
        {
            cl = String.IsNullOrWhiteSpace(cl) ? "0" : cl;
            legal = String.IsNullOrWhiteSpace(legal) ? "0" : legal;
            dv = String.IsNullOrWhiteSpace(dv) ? "0" : dv;

            string url = $"{ar_url}/GetListAging/{cl}/{legal}/{dv}";//{ms}/{sd.ToString("yyyyMMdd")}/{ed.ToString("yyyyMMdd")
            var data = await client.Get<List<AgingAR>>(url) ?? new List<AgingAR>();
            var model = data.ToDataSourceResult(request);
            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }        
    }
}