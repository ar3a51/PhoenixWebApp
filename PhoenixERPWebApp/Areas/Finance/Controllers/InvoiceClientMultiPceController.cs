﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CodeMarvel.Infrastructure;
using CodeMarvel.Infrastructure.Sessions;
using CodeMarvel.Infrastructure.WebNetCore;
using Microsoft.Extensions.Configuration;
using CodeMarvel.Infrastructure.Options;
using Microsoft.Extensions.Options;
using Kendo.Mvc.UI;
using PhoenixERPWebApp.Models.Finance.Transaction;
using PhoenixERPWebApp.Models.Enum;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PhoenixERPWebApp.Models.Finance;
using Phoenix.WebExtension.RestApi;
using PhoenixERPWebApp.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Http;

namespace PhoenixERPWebApp.Areas.Finance.Controllers
{
    [Area("Finance")]
    public class InvoiceClientMultiPceController : BaseController
    {
        private readonly ApiClientFactory client;
        string url_invoice = ApiUrl.InvoiceClientMultipleUrl;
        string url_dropdown = ApiUrl.financedropdownUrl;
        string url_pce = ApiUrl.PceUrl;

        public InvoiceClientMultiPceController(ApiClientFactory client)
        {
            this.client = client;
        }
        public IActionResult Index()
        {
            return View();
        }        
        
        public async Task<IActionResult> ReadIndex([DataSourceRequest] DataSourceRequest request, string status, string legal, string mainservice, string division)
        {
            var dataPC = await client.Get<List<InvoiceClient>>($"{url_invoice}/GetListIndex/{status ?? "0"}/{legal ?? "0"}") ?? new List<InvoiceClient>();
            var data = dataPC.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }
        
        public async Task<IActionResult> Detail(string multipceid, bool isApprove)
        {
            string status = "";
            if (isApprove.Equals(null))
            {
                isApprove = false;
            }
            var isCurrentApproval = false;

            if (!String.IsNullOrEmpty(HttpContext.Request.Query["sts"]))
                status = HttpContext.Request.Query["sts"];

            InvoiceClient model = new InvoiceClient();
            if (!string.IsNullOrEmpty(multipceid))
            {
                isCurrentApproval = await client.Get<bool>($"{ApiUrl.ManageTransApproval}/IsCurrentApproval/{multipceid}");
                string url = $"{url_invoice}/GetPceInfo/{multipceid}";
                model = await client.Get<InvoiceClient>(url);
                if(model ==null)
                    model = new InvoiceClient();
                model.Status = status;
            }
            else
            {
                model.Status = "";
            }

            ViewBag.processApprove = isApprove && isCurrentApproval;
            model.isApprove = isApprove;
            return View(model);
        }
        
        public async Task<IActionResult> ReadMultiPceList([DataSourceRequest] DataSourceRequest request, string multipceid)
        {
            string invidstr = (string.IsNullOrEmpty(multipceid) ? "0" : multipceid);
            if (string.IsNullOrEmpty(multipceid))
                multipceid = "0";

            string url = $"{url_invoice}/ReadMultiPceList/{multipceid}";
            var data = await client.Get<List<InvoiceClient>>(url) ?? new List<InvoiceClient>();
            var model = data.ToDataSourceResult(request);
            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        // multipceid: mpceid, legalid: legal, divid: divisionid, mservice: mainservice, client: clientid, currency: currencyid
        public async Task<IActionResult> GetPceChoiceList([DataSourceRequest] DataSourceRequest request, string multipceid, string legalid, string divid, string mservice, string clientt, string currency, string brandid)
        {
            string url = $"{url_invoice}/GetPceChoiceList/{multipceid ?? "0"}/{legalid ?? "0"}/{divid ?? "0"}/{mservice ?? "0"}/{clientt ?? "0"}/{currency ?? "0"}/{brandid ?? "0"}";
            var data = await client.Get<List<InvoiceClient>>(url) ?? new List<InvoiceClient>();
            var model = data.ToDataSourceResult(request);
            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> ReadInvHistory([DataSourceRequest] DataSourceRequest request, string multipceid)
        {
            string multipceidstr = (string.IsNullOrEmpty(multipceid) ? "0" : multipceid);

            string url = $"{url_invoice}/ReadInvHistory/{multipceidstr}";
            var data = await client.Get<List<InvoiceClient>>(url) ?? new List<InvoiceClient>();
            var model = data.ToDataSourceResult(request);
            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<JsonResult> GetInfoBank([DataSourceRequest] DataSourceRequest request, string id)
        {
            string invidstr = (string.IsNullOrEmpty(id) ? "0" : id);

            string url = $"{url_invoice}/GetInfoBank/{id}";
            var data = await client.Get<string>(url);
            return Json(data);
        }

        [HttpPost]
        public async Task<IActionResult> Post(InvoiceClient invoice, List<InvoiceClient> PceListMultiple, List<InvoiceClientItemOther> InvoiceMultipleListOther, IFormFile fileVatDoc, IFormFile filePoDoc)
        {
            var response = new ApiResponse();
            //Parallel.ForEach(invoice.PceListMultiple, (item) =>
            //{
            //    item.Id = Guid.NewGuid().ToString();
            //    item.TotalAmount = invoice.TotalAmount;
            //    item.GrandTotal = invoice.GrandTotal;
            //    item.VatNo = invoice.VatNo;
            //    item.AccountId = invoice.AccountId;
            //    item.InvoiceDate = invoice.InvoiceDate;
            //    //item.DueDate = invoice.DueDate;
            //    item.Vat = invoice.Vat;
            //    item.ExchangeRate = invoice.ExchangeRate;
            //    item.RefNumber = invoice.RefNumber;
            //    item.YourRef = invoice.YourRef;
            //    item.Asf = invoice.Asf;
            //    item.AsfAmount = invoice.AsfAmount;
            //});

            invoice.PceListMultiple = PceListMultiple;
            invoice.InvoiceMultipleListOther = InvoiceMultipleListOther;

            if (!string.IsNullOrEmpty(invoice.Id))
            {
                response = await client.PutApiResponse<InvoiceClient>($"{url_invoice}/updateandsubmit", invoice, fileVatDoc, filePoDoc);
            }
            else
            {
                response = await client.PostApiResponse<InvoiceClient>($"{url_invoice}/addandsubmit", invoice, fileVatDoc, filePoDoc);
            }

            return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });

            //var model = await client.PostApiResponse<InvoiceClient>($"{url_invoice}", invoice);
            //return Json(new { success = model.Success, message = model.Message, url = Url.Action(nameof(Index)) });
            //return Json(model, url = Url.Action(nameof(Index)), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<ActionResult> GetDDLBank(string orgid)
        {
            var getUrl = $"{url_invoice}/GetDDLBank/{orgid}";
            var model = await client.Get<List<SelectListItem>>(getUrl) ?? new List<SelectListItem>();
            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }
        

        public async Task<ActionResult> ReadPceTaskDetail([DataSourceRequest] DataSourceRequest request, string pceId, string invNo)
        {
            pceId = string.IsNullOrEmpty(pceId) ? "0" : pceId;
            invNo = string.IsNullOrEmpty(invNo) ? "0" : invNo;
            var dataTask = await client.Get<List<InvoiceClientItemOther>>($"{url_invoice}/GetInvoiceProductionDetail/{pceId}/{invNo}") ?? new List<InvoiceClientItemOther>();
            var data = dataTask.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        [HttpPost]
        public async Task<IActionResult> Approve(InvoiceClient model, List<InvoiceClient> PceListMultiple, List<InvoiceClientItemOther> InvoiceMultipleListOther)
        {
            model.PceListMultiple = PceListMultiple;
            model.InvoiceMultipleListOther = InvoiceMultipleListOther;
            var response = await client.PutApiResponse<InvoiceClient>($"{url_invoice}/Approve", model);
            return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });

        }

        [HttpPost]
        public async Task<IActionResult> Reject(InvoiceClient model, List<InvoiceClient> PceListMultiple, List<InvoiceClientItemOther> InvoiceMultipleListOther)
        {
            //model.Status = StatusTransaction.Rejected;
            model.PceListMultiple = PceListMultiple;
            model.InvoiceMultipleListOther = InvoiceMultipleListOther;
            var response = await client.PutApiResponse<InvoiceClient>($"{url_invoice}/Reject", model);
            return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
        }

        [HttpPost]
        public async Task<IActionResult> CreateAndReadAutomaticJournal(InvoiceClient model, List<InvoiceClient> PceListMultiple, List<InvoiceClientItemOther> InvoiceMultipleListOther, IFormFile fileVatDoc, IFormFile filePoDoc)
        {
            var response = new ApiResponse();
            model.PceListMultiple = PceListMultiple;
            model.InvoiceMultipleListOther = InvoiceMultipleListOther;
            try
            {
                if (string.IsNullOrEmpty(model.Status))
                    model.Status = SingleInvoiceClientStatus.Open.ToString();

                if (!string.IsNullOrEmpty(model.Id))
                {
                    response = await client.PutApiResponse<InvoiceClient>($"{url_invoice}/updatedata", model, fileVatDoc, filePoDoc);
                }
                else
                {
                    response = await client.PostApiResponse<InvoiceClient>($"{url_invoice}/adddata", model, fileVatDoc, filePoDoc);
                }

                if (response.Success)
                {
                    //var Id = model.Id;
                    //var PR = await client.Get<InvoiceClient>($"{url_invoice}/{Id}") ?? new InvoiceClient();
                    await client.PostApiResponse<InvoiceClient>($"{url_invoice}/PostDatatoSp", response.Data);
                    return Json(new { success = response.Success, message = response.Message, refreshGrid = "gridjournal", data = response.Data as InvoiceClient });
                    //if (response.Success)
                    //return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
                    //return Json(new { success = response.Success, message = response.Message });
                }
                else
                {
                    return Json(new { success = false, message = response.Message });
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message.ToString() });
            }
        }

        public async Task<IActionResult> ReadJournalInvoiceClient([DataSourceRequest] DataSourceRequest request, string Id, string invoiceId)
        {
            if (string.IsNullOrEmpty(Id))
            {
                if (string.IsNullOrEmpty(invoiceId))
                    Id = "0";
                else
                    Id = invoiceId;
            }
            var model = await client.Get<List<Journal>>($"{url_invoice}/GetDataJournal/{Id}") ?? new List<Journal>();
            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        [AcceptVerbs("Post")]
        public async Task<IActionResult> Update_Child_Grid([DataSourceRequest] DataSourceRequest request, InvoiceClientItemOther model)
        {
            return Json(new[] { model }.ToTreeDataSourceResult(request, ModelState), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

    }
}