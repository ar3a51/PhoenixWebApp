﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CodeMarvel.Infrastructure;
using CodeMarvel.Infrastructure.Sessions;
using CodeMarvel.Infrastructure.WebNetCore;
using Microsoft.Extensions.Configuration;
using CodeMarvel.Infrastructure.Options;
using Microsoft.Extensions.Options;
using Phoenix.WebExtension.RestApi;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using PhoenixERPWebApp.Models.Finance;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PhoenixERPWebApp.Models;
using System.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.Net.Http.Headers;
using System.IO;
using Microsoft.AspNetCore.Hosting.Internal;
using PhoenixERPWebApp.Models.Enum;
using PhoenixERPWebApp.Models.Finance.Transaction;
using PhoenixERPWebApp.Models;

namespace PhoenixERPWebApp.Areas.Finance.Controllers
{
    [Area("Finance")]
    public class CashAdvanceSattlementController : BaseController
    {
        string url = ApiUrl.CashAdvanceSattleUrl;
        string urlca = ApiUrl.CashAdvanceUrl;
        private readonly ApiClientFactory client;
        public IHostingEnvironment HostingEnvironment { get; set; }

        public CashAdvanceSattlementController(ApiClientFactory client, IHostingEnvironment hostingEnvironment)
        {
            this.client = client;
            HostingEnvironment = hostingEnvironment;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult IndexApproval()
        {
            return View();
        }
        
        public async Task<IActionResult> Settle(string Id, bool isApprove)
        {
            if (isApprove.Equals(null))
            {
                isApprove = false;
            }
            string CaId = "";
            var model = new CashAdvanceSattlement();
            if (string.IsNullOrEmpty(Id))
            {
                return Json(new { success = true, url = Url.Action("Index", "CashAdvance") });
            }
            else
            {
                var modelcas = await client.Get<CashAdvanceSattlement>($"{url}/GetSattlementById/{Id}");
                CaId = modelcas.CashAdvanceId;
                var isCurrentApproval = false;
                if (modelcas == null)
                {
                    var modelca = await client.Get<CashAdvance>($"{url}/{CaId}");

                    model.BusinessUnitId = modelca.BusinessUnitId;
                    model.AccountNumber = modelca.PayeeAccountNumber;
                    model.Amount = modelca.Amount;
                    model.RequestedOn = modelca.RequestedOn;
                    model.BankId = modelca.PayeeBankId;
                    model.CashAdvanceId = CaId;
                    model.CashAdvanceSettleDate = DateTime.Now;
                    model.CaDate = modelca.CaDate;
                    model.CaNumber = modelca.CaNumber;
                    model.JobId = modelca.JobId;
                    model.JobNumber = modelca.JobNumber;
                    model.RequestEmployeeId = modelca.RequestEmployeeId;
                    model.RequestEmployeeName = modelca.RequestEmployeeName;
                    model.RequestJobGradeId = modelca.RequestJobGradeId;
                    model.RequestJobGradeName = modelca.RequestJobGradeName;
                    model.RequestJobTitleId = modelca.RequestJobTitleId;
                    model.RequestJobTitleName = modelca.RequestJobTitleName;
                    model.ClientName = modelca.ClientName;
                    model.ProjectActivity = modelca.ProjectActivity;
                    model.LegalEntityId = modelca.LegalEntityId;
                    model.VendorName = modelca.VendorName;
                    model.PayeeAccountName = modelca.PayeeAccountName;
                    model.PayeeAccountNumber = modelca.PayeeAccountNumber;
                    model.PayeeBankBranch = modelca.PayeeBankBranch;
                    model.PayeeBankId = modelca.PayeeBankId;
                    model.CaPeriodFrom = modelca.CaPeriodFrom;
                    model.CaPeriodTo = modelca.CaPeriodTo;
                    model.MainServiceCategoryId = modelca.ProjectActivity;
                    model.Id = null;
                    model.Status = int.Parse(modelca.CaStatusId);
                    model.CashAdvanceCa = modelca.Amount;
                    model.VendorId = modelca.VendorId;
                    model.VendorAddress = modelca.VendorAddress;
                    model.VendorPIC = modelca.VendorPIC;
                    model.Ca_pceId = modelca.PurchaseOrderNumber;
                    model.Ca_PCETypeId = modelca.PCETypeId;
                    model.Ca_BrandId = modelca.ClientBrandId;
                    model.Ca_ShareservicesId = modelca.ShareservicesId;
                    model.VendorTaxNumber = modelca.VendorTaxNumber;
                }
                else
                {
                    var modelca = await client.Get<CashAdvance>($"{urlca}/{CaId}");
                    var modelcad = await client.Get<List<CashAdvanceDetail>>($"{urlca}/GetDetails/{CaId}") ?? new List<CashAdvanceDetail>();
                    var modelcasdetail = await client.Get<List<CashAdvanceSettlementDetail>>($"{url}/GetSattlementDetailById/{modelcas.CashAdvanceId}");

                    isCurrentApproval = await client.Get<bool>($"{ApiUrl.ManageTransApproval}/IsCurrentApproval/{modelcas.Id}");
                    ViewBag.isRequestor = isApprove == false; //|| model.Status == StatusTransaction.Rejected);

                    model.BusinessUnitId = modelca.BusinessUnitId;
                    model.AccountNumber = modelca.PayeeAccountNumber;
                    if (modelcasdetail != null)
                    {
                        decimal? ttl = 0;
                        foreach (CashAdvanceSettlementDetail data in modelcasdetail)
                        {
                            ttl += data.ActualAmount;
                        }
                        model.Amount = ttl;
                    }
                    model.RequestBusinessUnitId = modelca.RequestBusinessUnitId;
                    model.RequestedOn = modelca.RequestedOn;
                    model.BankId = modelca.PayeeBankId;
                    model.CashAdvanceId = CaId;
                    model.CashAdvanceSettleDate = DateTime.Now;
                    model.CaDate = modelca.CaDate;
                    model.CaNumber = modelca.CaNumber;
                    model.JobId = modelca.JobId;
                    model.JobNumber = modelca.JobNumber;
                    model.RequestEmployeeId = modelca.RequestEmployeeId;
                    model.RequestEmployeeName = modelca.RequestEmployeeName;
                    model.RequestJobGradeId = modelca.RequestJobGradeId;
                    model.RequestJobGradeName = modelca.RequestJobGradeName;
                    model.RequestJobTitleId = modelca.RequestJobTitleId;
                    model.RequestJobTitleName = modelca.RequestJobTitleName;
                    model.ClientName = modelca.ClientName;
                    model.ProjectActivity = modelca.ProjectActivity;
                    model.LegalEntityId = modelca.LegalEntityId;
                    model.VendorName = modelca.VendorName;
                    model.PayeeAccountName = modelca.PayeeAccountName;
                    model.PayeeAccountNumber = modelca.PayeeAccountNumber;
                    model.PayeeBankBranch = modelca.PayeeBankBranch;
                    model.PayeeBankId = modelca.PayeeBankId;
                    model.CaPeriodFrom = modelca.CaPeriodFrom;
                    model.CaPeriodTo = modelca.CaPeriodTo;
                    model.MainServiceCategoryId = modelca.ProjectActivity;
                    model.Vat = modelcas.Vat;
                    model.Id = modelcas.Id;
                    ViewBag.RecordId = modelcas.Id;
                    model.VatPercent = modelcas.VatPercent;
                    model.VatOn = modelcas.VatOn;
                    model.TaxPayablePercent = modelcas.TaxPayablePercent;
                    model.TaxPayable = modelcas.TaxPayable;
                    model.TaxPayableOn = modelcas.TaxPayableOn;
                    model.TotalAmount = modelcas.TotalAmount;
                    model.CashAdvanceCa = modelcad.Sum(a => a.TotalRequest);//modelca.Amount;
                    model.Balance = modelcas.Balance;
                    model.CaStatusId = modelcas.CaStatusId;
                    model.Status = int.Parse(modelcas.CaStatusId);
                    model.FileReceipt = modelcas.FileReceipt;
                    model.FileReceiptName = modelcas.FileReceiptName;
                    model.VendorId = modelca.VendorId;
                    model.VendorAddress = modelca.VendorAddress;
                    model.VendorPIC = modelca.VendorPIC;
                    model.TaxPayCoaId = modelcas.TaxPayCoaId;
                    model.Ca_pceId = modelca.PurchaseOrderNumber;
                    model.Ca_PCETypeId = modelca.PCETypeId;
                    model.Ca_BrandId = modelca.ClientBrandId;
                    model.Ca_ShareservicesId = modelca.ShareservicesId;
                    model.VendorTaxNumber = modelca.VendorTaxNumber;
                }

                ViewBag.processApprove = isApprove && isCurrentApproval;//(model.Status == StatusTransaction.WaitingApproval || model.Status == StatusTransaction.CurrentApproval);
                model.IsApprove = isApprove;
                if (isApprove)
                {
                    ViewBag.isHcAdmin = true;//belum di set
                }
                ViewBag.isApprove = isApprove;
                //var requestor = await client.Get<InfoEmployee>($"{ApiUrl.EmployeeInfoUrl}/getByUserLogin");
                //ViewBag.isRequestor = 
            }
            
            return View(model);
        }

        public async Task<IActionResult> Read([DataSourceRequest] DataSourceRequest request, string legalEntity, string mainServiceCategory)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings { ContractResolver = new CustomContractResolver() };
            List<string> query = new List<string>();
            if (!string.IsNullOrEmpty(legalEntity))
            {
                query.Add($"legal={legalEntity}");
            }
            if (!string.IsNullOrEmpty(mainServiceCategory))
            {
                query.Add($"svc={mainServiceCategory}");
            }
            string queryParam = query.Count > 0 ? "?" + string.Join("&", query) : string.Empty;

            var model = await client.Get<List<CashAdvanceSattlement>>($"{url}{queryParam}", settings) ?? new List<CashAdvanceSattlement>();
            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> ReadDetailSattelement([DataSourceRequest] DataSourceRequest request, string Id)
        {
            var returnVal = new List<CashAdvanceSettlementDetail>();
            returnVal = await client.Get<List<CashAdvanceSettlementDetail>>($"{url}/GetSattlementDetailById/{Id}") ?? new List<CashAdvanceSettlementDetail>();
            return Json(returnVal.ToDataSourceResult(request), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> ReadJournal([DataSourceRequest] DataSourceRequest request, string CaId)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings { ContractResolver = new CustomContractResolver() };
            var model = await client.Get<List<Journal>>($"{url}/GetJournalDataBySattlementId/{CaId}", settings) ?? new List<Journal>();
            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }
    
        [AcceptVerbs("Post")]
        public ActionResult Events_Save([DataSourceRequest] DataSourceRequest request, CashAdvanceSettlementDetail cad)
        {
            if (cad != null && ModelState.IsValid)
            {
                //productService.Update(cad);
            }

            return Json(new[] { cad }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs("Post")]
        public ActionResult Events_Remove([DataSourceRequest] DataSourceRequest request, CashAdvanceSettlementDetail cad)
        {
            if (cad != null && ModelState.IsValid)
            {
                //productService.Update(cad);
            }

            return Json(new[] { cad }.ToDataSourceResult(request, ModelState));
        }


        [AcceptVerbs("Post")]
        public ActionResult EditingSettlement_Create([DataSourceRequest] DataSourceRequest request, CashAdvanceSettlementDetail cad, IFormFile settlementFiles)
        {
            if (cad != null && ModelState.IsValid)
            {
                //productService.Create(cad);
            }

            return Json(new[] { cad }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs("Post")]
        public ActionResult EditingSettlement_Update([DataSourceRequest] DataSourceRequest request, CashAdvanceSettlementDetail cad, [FromForm(Name = "Receipt_Receipt")]IFormFile fileuploadheader)
        {
            if (cad != null && ModelState.IsValid)
            {
                //productService.Update(cad);
            }

            return Json(new[] { cad }.ToDataSourceResult(request, ModelState));
        }

        public async Task<IActionResult> UploadFile([FromForm(Name = "Receipt.Receipt")]IFormFile formId)
        {
            var response = new ApiResponse();
            if (formId != null)
                response = await client.PostApiResponse<CashAdvanceSettlementDetail>($"{url}/uploadfile", null, formId);
            return Json(new { success = response.Success, message = response.Message, data = response.Data });

        }

        [AcceptVerbs("Post")]
        public ActionResult EditingSettlement_Destroy([DataSourceRequest] DataSourceRequest request, CashAdvanceSettlementDetail cad, IFormFile settlementFiles)
        {
            if (cad != null && ModelState.IsValid)
            {
                //productService.Update(cad);
            }

            return Json(new[] { cad }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public async Task<IActionResult> ApproveSattlement(CashAdvanceSattlement model)
        {
            model.CaStatusId = StatusTransaction.Approved.ToString();
            var response = await client.PutApiResponse<CashAdvanceSattlement>($"{url}/ApproveSattlement", model);
            return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
        }

        [HttpPost]
        public async Task<IActionResult> RejectSattlement(CashAdvanceSattlement model)
        {
            model.CaStatusId = StatusTransaction.Approved.ToString();
            var response = await client.PutApiResponse<CashAdvanceSattlement>($"{url}/Reject", model);
            return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
        }
        
        [HttpPost]
        public async Task<IActionResult> CreateAndReadAutomaticJournal(CashAdvanceSattlement header, List<CashAdvanceSettlementDetail> details, IFormFile fileuploadheader)
        {
            try
            {
                var response = new ApiResponse();
                var model = new CashAdvanceSattlementDTO();
                model.Header = header;
                model.Details = details;

                if (!string.IsNullOrEmpty(header.Id))
                {
                    response = await client.PutApiResponse<CashAdvanceSattlementDTO>($"{url}/EditSattlement", model, fileuploadheader);
                }
                else
                {
                    response = await client.PostApiResponse<CashAdvanceSattlementDTO>($"{url}/AddSattlement", model, fileuploadheader);
                }

                if (response.Success)
                {

                    response = await client.PostApiResponse<CashAdvanceSattlement>($"{url}/PostDataSattlement", model.Header);

                    return Json(new { success = response.Success, message = response.Message });
                }
                else
                {
                    return Json(new { success = false, message = response.Message });
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message.ToString() });
            }
        }

        [HttpPost]
        public async Task<IActionResult> SaveSettlement(CashAdvanceSattlement header, List<CashAdvanceSettlementDetail> details, [FromForm(Name = "fileuploadheader")]IFormFile fileuploadheader)//
        {
            //model.Status = StatusTransaction.;
            if (string.IsNullOrEmpty(header.CaStatusId))
            {
                header.CaStatusId = CashAdvanceStatus.Approved;
            }
            if (string.IsNullOrEmpty(header.Id))
            {
                header.CaStatusId = CashAdvanceStatus.Approved;
            }
            List<CashAdvanceSettlementDetail> _details = new List<CashAdvanceSettlementDetail>();
            foreach (var i in details)
            {
                if (!string.IsNullOrEmpty(i.TempFileIdDetail))
                {
                    i.Receipt = i.TempFileIdDetail;
                }
                _details.Add(i);
            }
            return await ProcessSattlement(header, _details, fileuploadheader);// , details
        }


        [HttpPost]
        public async Task<IActionResult> SubmitSettlement(CashAdvanceSattlement header, List<CashAdvanceSettlementDetail> details, [FromForm(Name = "fileuploadheader")]IFormFile fileuploadheader)
        {
            header.CaStatusId = StatusTransaction.WaitingApproval.ToString();
            List<CashAdvanceSettlementDetail> _details = new List<CashAdvanceSettlementDetail>();
            foreach (var i in details)
            {
                if (!string.IsNullOrEmpty(i.TempFileIdDetail))
                {
                    i.Receipt = i.TempFileIdDetail;
                }
                _details.Add(i);
            }

            return await ProcessSattlement(header, _details, fileuploadheader);
        }

        private async Task<IActionResult> ProcessSattlement(CashAdvanceSattlement header, List<CashAdvanceSettlementDetail> details, IFormFile fileuploadheader)
        {
            try
            {
                var response = new ApiResponse();
                CashAdvanceSattlementDTO model = new CashAdvanceSattlementDTO();
                model.Header = header;
                List<CashAdvanceSettlementDetail> dtl = new List<CashAdvanceSettlementDetail>();
                dtl = details;
                model.Details = details;
                if (!string.IsNullOrEmpty(model.Header.Id))
                {
                    response = await client.PutApiResponse<CashAdvanceSattlementDTO>($"{url}/EditSattlement", model, fileuploadheader);
                }
                else
                {
                    response = await client.PostApiResponse<CashAdvanceSattlementDTO>($"{url}/AddSattlement", model, fileuploadheader);
                }

                return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message.ToString() });
            }
        }
        
        public async Task<ActionResult> SaveFileAsync(IEnumerable<IFormFile> settlementFiles)
        {
            // The Name of the Upload component is "files"
            if (settlementFiles != null)
            {
                foreach (var file in settlementFiles)
                {
                    var fileContent = ContentDispositionHeaderValue.Parse(file.ContentDisposition);

                    // Some browsers send file names with full path.
                    // We are only interested in the file name.
                    var fileName = Path.GetFileName(fileContent.FileName.ToString().Trim('"'));
                    var physicalPath = Path.Combine(HostingEnvironment.WebRootPath, "App_Data", fileName);

                    // The files are not actually saved in this demo
                    //using (var fileStream = new FileStream(physicalPath, FileMode.Create))
                    //{
                    //    await file.CopyToAsync(fileStream);
                    //}
                }
            }

            // Return an empty string to signify success
            return Content("");
        }

        public ActionResult RemoveFile(string[] fileNames)
        {
            // The parameter of the Remove action must be called "fileNames"

            if (fileNames != null)
            {
                foreach (var fullName in fileNames)
                {
                    var fileName = Path.GetFileName(fullName);
                    var physicalPath = Path.Combine(HostingEnvironment.WebRootPath, "App_Data", fileName);

                    // TODO: Verify user permissions

                    if (System.IO.File.Exists(physicalPath))
                    {
                        // The files are not actually removed in this demo
                        // System.IO.File.Delete(physicalPath);
                    }
                }
            }

            // Return an empty string to signify success
            return Content("");
        }
    }
}