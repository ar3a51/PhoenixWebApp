﻿using CodeMarvel.Infrastructure.WebNetCore;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Phoenix.WebExtension.RestApi;
using PhoenixERPWebApp.Models;
using PhoenixERPWebApp.Models.Finance;
using PhoenixERPWebApp.Models.Finance.MasterData;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Areas.Finance.Controllers
{
	[Area("Finance")]
	public class PettyCashController : BaseController
	{
		private readonly ApiClientFactory client;
		string url = ApiUrl.PettyCashUrl;
		string url_dropdown = ApiUrl.financedropdownUrl;

		public PettyCashController(ApiClientFactory client)
		{
			this.client = client;
		}

		public async Task<IActionResult> Index()
		{
			var groupName = await client.Get<string>($"{url}/GetGroupName");
			ViewBag.GroupName = groupName;

			return View();
		}

		[AcceptVerbs("Post")]
		public async Task<IActionResult> ReadIndex([DataSourceRequest] DataSourceRequest request, PettyCash petty)
		{
			var model = await client.PostApiResponse<List<PettyCash>>($"{url}/ReadIndex", petty);
			var List = new List<PettyCash>();
			if (model.Data != null) List = model.Data as List<PettyCash>;
			var data = List.ToDataSourceResult(request);
			return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
		}

		public async Task<IActionResult> Detail([FromQuery(Name = "Id")] string Id, [FromQuery(Name = "rn")] string reffNum)
		{
			string id = string.IsNullOrWhiteSpace(Id) ? "0" : Id;
			string rn = string.IsNullOrWhiteSpace(reffNum) ? "0" : reffNum;

			PettyCash model = await client.Get<PettyCash>($"{url}/{id}/{rn}");

			ViewBag.processApprove = await client.Get<bool>($"{ApiUrl.ManageTransApproval}/IsCurrentApproval/{Id}");
			ViewBag.isRequestor = ViewBag.processApprove == true && (model.TrnStatus == StatusTransaction.Draft || model.TrnStatus == StatusTransaction.Rejected
				|| model.TrnStatus == StatusTransaction.CurrentApproval);

			if (id == "0")
			{
				ViewBag.Header = "Create";
				ViewBag.isRequestor = ViewBag.processApprove == false;
				model.Status = StatusTransaction.Draft;
			}
			else
			{
				if (ViewBag.processApprove == true)
					ViewBag.Header = "Approval";
				else
					ViewBag.Header = "Update";
			}

			if (model.GroupName.Equals(PettyGroupName.GroupSettle))
				return View("DetailUser", model);
			else
				return View(model);
		}

		public async Task<IActionResult> GetPettyCashDetail([DataSourceRequest] DataSourceRequest request, string Id, string rn)
		{
			string id = string.IsNullOrWhiteSpace(Id) ? "0" : Id;
			string rfn = string.IsNullOrWhiteSpace(rn) ? "0" : rn;
			var model = await client.PostApiResponse<List<PettyCashDetail>>($"{url}/GetDetails/{id}/{rfn}");
			var List = new List<PettyCashDetail>();
			if (model.Data != null) List = model.Data as List<PettyCashDetail>;
			var data = List.ToDataSourceResult(request);
			return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
		}

		public async Task<ActionResult> GetPcaTasks(string pcaId)
		{
			string pcaid = string.IsNullOrEmpty(pcaId) ? "0" : pcaId;
			var getUrl = $"{url}/GetPcaTasks/{pcaid}";
			var model = await client.Get<List<SelectListItem>>(getUrl) ?? new List<SelectListItem>();
			return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
		}

		public async Task<IActionResult> GetInfoPceByJobId([DataSourceRequest] DataSourceRequest request, string jobId)
		{
			string jb = string.IsNullOrWhiteSpace(jobId) ? "0" : jobId;
			string urljbdtl = $"{url}/GetInfoPceByJobId/{jb}";
			var model = await client.Get<JobDetail>(urljbdtl);
			if (model == null)
				model = new JobDetail();
			return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
		}

		[AcceptVerbs("Post")]
		public ActionResult CreateDetail([DataSourceRequest] DataSourceRequest request, PettyCashDetail model)
		{
			if (model != null && ModelState.IsValid)
			{
				model.Id = Guid.NewGuid().ToString();
			}
			return Json(new[] { model }.ToDataSourceResult(request, ModelState), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
		}

		[AcceptVerbs("Post")]
		public ActionResult UpdateDetail([DataSourceRequest] DataSourceRequest request, PettyCashDetail model)
		{
			if (model != null && ModelState.IsValid)
			{
			}

			return Json(new[] { model }.ToDataSourceResult(request, ModelState), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
		}

		[AcceptVerbs("Post")]
		public ActionResult DestroyDetail([DataSourceRequest] DataSourceRequest request, PettyCashDetail model)
		{
			if (model != null)
			{
			}
			return Json(new[] { model }.ToDataSourceResult(request, ModelState), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
		}

		public async Task<IActionResult> ReadJournal([DataSourceRequest] DataSourceRequest request, string Id, string rn)
		{
			string id = string.IsNullOrWhiteSpace(Id) ? "0" : Id;
			string rfn = string.IsNullOrWhiteSpace(rn) ? "0" : rn;
			var model = await client.Get<List<Journal>>($"{url}/JournalList/{id}/{rfn}") ?? new List<Journal>();
			var data = model.ToDataSourceResult(request);
			return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
		}

		[HttpPost]
		public async Task<IActionResult> Submit(PettyCash model)
		{
			if (model.GroupName.Equals(PettyGroupName.GroupSettle))
			{
				if (model.Status == PettyCashStatus.Draft || model.Status == PettyCashStatus.Open)
				{
					model.TrnStatus = StatusTransaction.CurrentApproval;
					model.Status = PettyCashStatus.Open;
				}
			}
			else
			{
				if (model.Status == PettyCashStatus.Open)
				{
					model.TrnStatus = StatusTransaction.WaitingApproval;
					model.Status = PettyCashStatus.WAITING_APPROVAL;
				}
			}

			return await Process(model, false, false, null);
		}

		[HttpPost]
		public async Task<IActionResult> Save(PettyCash model)
		{
			model.TrnStatus = StatusTransaction.CurrentApproval;

			if (model.GroupName.Equals(PettyGroupName.GroupSettle))
			{
				model.Status = PettyCashStatus.Draft;
				return await Process(model, true, false, model.Id);
			}
			else
			{
				//else if(model.GroupName.Equals(PettyGroupName.GroupJournal))
				model.Status = PettyCashStatus.Open;
				return await Process(model, true, false, model.Id);
			}

		}

		[HttpPost]
		public async Task<IActionResult> CreateJournal(PettyCash model)
		{
			model.IsDraft = true;
			return await Process(model, true, true, null);
		}

		private async Task<IActionResult> Process(PettyCash model, bool isDraft, bool isJournal, string id)
		{
			var response = new Phoenix.WebExtension.RestApi.ApiResponse();
			if (model.Status != PettyCashStatus.Draft)
			{
				if (isJournal)
				{
					response = await client.PutApiResponse<PettyCash>($"{url}/EditJournal", model);
				}
				else
				{
					response = await client.PutApiResponse<PettyCash>(url, model);
				}
			}
			else
			{
				if (isJournal)
				{
					response = await client.PostApiResponse<PettyCash>($"{url}/CreateJournal", model);
				}
				else
				{
					if (string.IsNullOrWhiteSpace(model.Id))
					{
						response = await client.PostApiResponse<PettyCash>(url, model);
					}
					else
					{
						response = await client.PutApiResponse<PettyCash>(url, model);
					}
				}
			}

			if (isJournal)
			{
				return Json(new { success = response.Success, message = response.Message, refreshGrid = "gridJournal", data = response.Data as PettyCash });
			}
			else if (isDraft)
			{
				return Json(new { success = response.Success, message = response.Message, data = response.Data as PettyCash, btnTriggerId = "btnTriggerReloadState" });
			}
			else
			{
				return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
			}
		}

		public async Task<ActionResult> GetReffPettyCash()
		{
			var getUrl = $"{ApiUrl.MasterPettyCashUrl}/GetReffList";
			var model = await client.Get<List<MasterPettyCash>>(getUrl) ?? new List<MasterPettyCash>();
			return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
		}

		[HttpGet]
		public async Task<JsonResult> GetInfoMasterPetty(string id)
		{
			var getUrlbyId = $"{ApiUrl.MasterPettyCashUrl}/{id}";
			var model = await client.Get<MasterPettyCash>(getUrlbyId) ?? new MasterPettyCash();
			return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
		}

		[HttpGet]
		public async Task<JsonResult> GetAffiliationFromBussinesUnit(string id)
		{
			var getUrlbyId = $"{ApiUrl.MasterPettyCashUrl}/GetAffiliationFromBussinesUnit/{id}";
			var model = await client.Get<MasterPettyCash>(getUrlbyId) ?? new MasterPettyCash();
			return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
		}
	}
}