﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using CodeMarvel.Infrastructure.WebNetCore;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Phoenix.WebExtension.RestApi;
using PhoenixERPWebApp.Models;
using PhoenixERPWebApp.Models.Enum;

namespace PhoenixERPWebApp.Areas.Finance.Controllers
{
    [Area("Finance")]
    public class TemporaryAccountPayableController : BaseController
    {
        string url = ApiUrl.InvoiceReceivable;
        private readonly ApiClientFactory client;

        public TemporaryAccountPayableController(ApiClientFactory client)
        {
            this.client = client;
        }

        public async Task<IActionResult> Index()
        {
            var group = await client.Get<List<TmUserApp>>($"{ApiUrl.ManageUserUrl}/GetByUserLogin");
            var accountingSupervisor = group.Where(x => x.GroupId == GroupAcess.AccountingSupervisor).Count() > 0;
            var taxSupervisor = group.Where(x => x.GroupId == GroupAcess.TaxSupervisor).Count() > 0;
            var financeManager = group.Where(x => x.GroupId == GroupAcess.FinanceManager).Count() > 0;
            var model = new InvoiceReceivable() { InvoiceStatusId = accountingSupervisor ? InvoiceReceivedStatus.TAP : taxSupervisor ? InvoiceReceivedStatus.TAXTAP : financeManager ? InvoiceReceivedStatus.OpenAP : InvoiceReceivedStatus.TAP };
            return View(model);
        }

        public async Task<IActionResult> Read([DataSourceRequest] DataSourceRequest request, string status, string businessUnit, string legalEntity, string invoiceNumber)
        {
            var model = await client.Get<List<InvoiceReceivable>>($"{url}/GetList/{(status ?? "0")}/{(businessUnit ?? "0")}/{(legalEntity ?? "0")}/{(invoiceNumber ?? "0")}") ?? new List<InvoiceReceivable>();
            return Json(model.ToDataSourceResult(request), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }


        //public async Task<IActionResult> Read([DataSourceRequest] DataSourceRequest request)
        //{
        //    var model = await client.Get<List<TemporaryAccountPayableList>>($"{url}/getjoinlist") ?? new List<TemporaryAccountPayableList>();
        //    var data = model.ToDataSourceResult(request);
        //    return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //}

        //public async Task<IActionResult> Detail(string Id, string VendorId, string InvoiceReceivableId, string OurRef)
        //{
        //    var model = new TemporaryAccountPayable();
        //    ViewBag.Header = "Account Payable";

        //    if (!string.IsNullOrEmpty(Id))
        //    {
        //        ViewBag.Header = "Update";
        //        model = await client.Get<TemporaryAccountPayable>($"{url}/{Id}");
        //    }
        //    else
        //    {
        //        if (!string.IsNullOrEmpty(VendorId) && !string.IsNullOrEmpty(InvoiceReceivableId) && !string.IsNullOrEmpty(OurRef))
        //        {
        //            model.VendorId = VendorId;
        //            model.InvoiceReceivableId = InvoiceReceivableId;
        //            model.OurRef = OurRef;
        //            ViewBag.Header = "Create";
        //        }
        //        else
        //        {

        //            ViewBag.Header = "Create";
        //        }
        //    }

        //    return View(model);
        //}

        //public async Task<IActionResult> ReadDetail([DataSourceRequest] DataSourceRequest request)
        //{
        //    var model = new List<TemporaryAccountPayableDetailList>();

        //    var data = model.ToDataSourceResult(request);
        //    return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //}

        //[HttpGet]
        //public async Task<IActionResult> GetAccountData(string id)
        //{

        //    var model = await client.Get<ChartofAccount>($"{ApiUrl.TemporaryAccountPayable}/getAccountData/{id}");
        //    return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //}

        //public async Task<IActionResult> ReadJournalEmpty([DataSourceRequest] DataSourceRequest request, string referenceId)
        //{
        //    var model = new List<ExecJournalAPView>();

        //    var data = model.ToDataSourceResult(request);
        //    return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //}

        //public async Task<IActionResult> ReadJournalFromTAP([DataSourceRequest] DataSourceRequest request, string invoiceReceivedId)
        //{
        //    var modelDetail = await client.Get<List<ExecJournalAPView>>($"{url}/GetJournalTmpAllFromTAP/{invoiceReceivedId}") ?? new List<ExecJournalAPView>();
        //    var modelHeader = await client.Get<List<ExecJournalAPView>>($"{url}/GetJournalTmpAllFromTAPHeader/{invoiceReceivedId}") ?? new List<ExecJournalAPView>();
        //    var newmodel = new List<ExecJournalAPView>();
        //    foreach (ExecJournalAPView newdatadetail in modelDetail)
        //    {
        //        newdatadetail.Id = Guid.NewGuid().ToString();
        //        newmodel.Add(newdatadetail);
        //    }
        //    foreach (ExecJournalAPView newdataheader in modelHeader)
        //    {
        //        newdataheader.Id = Guid.NewGuid().ToString();
        //        newmodel.Add(newdataheader);
        //    }
        //    var data = newmodel.ToDataSourceResult(request);
        //    return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //}

        //public async Task<IActionResult> ReadJournalFromPaymentRequest([DataSourceRequest] DataSourceRequest request, string invoiceReceivedId,string ApNumber)
        //{
        //    var modelAP = await client.Get<AccountPayable>($"{ApiUrl.AccountPayableUrl}/GetAccountPayableByAccountNumber/{ApNumber}") ?? new AccountPayable();
        //    var modelTap = new TemporaryAccountPayable();
        //    if (modelAP != null) {
        //        modelTap = await client.Get<TemporaryAccountPayable>($"{ApiUrl.TemporaryAccountPayable}/{modelAP.TemporaryAccountPayableId}") ?? new TemporaryAccountPayable();
        //    }

        //    var modelJournal = new List<TemporayAccountPayableJournal>();
        //    if (!string.IsNullOrEmpty(modelTap.Id))
        //    {
        //        modelJournal = await client.Get<List<TemporayAccountPayableJournal>>($"{ApiUrl.TemporaryAccountPayable}/GetTapJournalWithTAPId/{modelTap.Id}") ?? new List<TemporayAccountPayableJournal>();

        //    }

        //    var data = modelJournal.ToDataSourceResult(request);
        //    return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //}

        //public async Task<IActionResult> ReadJournalFromPaymentRequestCa([DataSourceRequest] DataSourceRequest request, string CaId)
        //{
        //    var modelca = await client.Get<CashAdvance>($"{ApiUrl.CashAdvanceUrl}/GetAccountPayableByAccountNumber/{CaId}") ?? new CashAdvance();

        //    var modelJournal = new List<TemporayAccountPayableJournal>();
        //    if (!string.IsNullOrEmpty(modelca.Id))
        //    {
        //        modelJournal = await client.Get<List<TemporayAccountPayableJournal>>($"{ApiUrl.CashAdvanceUrl}/GetJournalDataId/{modelca.Id}") ?? new List<TemporayAccountPayableJournal>();

        //    }

        //    var data = modelJournal.ToDataSourceResult(request);
        //    return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //}

        //public async Task<IActionResult> ReadTmpJournal([DataSourceRequest] DataSourceRequest request, string invoiceReceivedId,string referenceId)
        //{
        //    var model = await client.Get<TemporaryAccountPayableList>($"{url}/GetTAPFromINV/{invoiceReceivedId}/{referenceId}") ?? new TemporaryAccountPayableList();
        //    var tap = new List<TemporayAccountPayableJournal>();
        //    if (!string.IsNullOrEmpty(model.Id))
        //    {
        //        tap = await client.Get<List<TemporayAccountPayableJournal>>($"{url}/GetTapJournalWithTAPId/{model.Id}") ?? new List<TemporayAccountPayableJournal>();
        //    }

        //    var data = tap.ToDataSourceResult(request);
        //    return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //}

        //public async Task<IActionResult> ReadJournal([DataSourceRequest] DataSourceRequest request, string referenceId)
        //{
        //    var model = await client.Get<List<ExecJournalAPView>>($"{url}/GetJournalTmp/{referenceId}") ?? new List<ExecJournalAPView>();
        //    var data = model.ToDataSourceResult(request);
        //    return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //}

        //public async Task<IActionResult> ReadJournalTax([DataSourceRequest] DataSourceRequest request, string referenceId)
        //{
        //    var model = await client.Get<List<ExecJournalAPView>>($"{url}/GetJournalTmpTax/{referenceId}") ?? new List<ExecJournalAPView>();
        //    var data = model.ToDataSourceResult(request);
        //    return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //}

        //public async Task<IActionResult> ReadJournalAll([DataSourceRequest] DataSourceRequest request, string referenceId, string invoiceReceivedId)
        //{
        //    var model = await client.Get<List<ExecJournalAPView>>($"{url}/GetJournalTmpAll/{referenceId}/{invoiceReceivedId}") ?? new List<ExecJournalAPView>();
        //    var modeltmp = new List<ExecJournalAPView>();
        //    foreach (ExecJournalAPView datatmp in model)
        //    {
        //        datatmp.Id = Guid.NewGuid().ToString();
        //        modeltmp.Add(datatmp);
        //    }
        //    var data = modeltmp.ToDataSourceResult(request);
        //    return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //}

        //[HttpPost]
        //public async Task<IActionResult> CreateAndReadAutomaticJournal(InvoiceReceivedList header, List<InvoiceReceivedDetail> details)
        //{

        //    try
        //    {
        //        var response = new ApiResponse();
        //        header.InvoiceVendor = header.InvoiceVendorText != null ? header.InvoiceVendorText : header.InvoiceVendor;
        //        var newModel = new InvoiceReceivedDTO();
        //        newModel.Header = header;
        //        newModel.Details = details;

        //        if (!string.IsNullOrEmpty(header.Id))
        //        {
        //            response = await client.PutApiResponse<InvoiceReceivedList>(urlInv, newModel);
        //        }
        //        else
        //        {
        //            response = await client.PostApiResponse<InvoiceReceivedList>(urlInv, newModel);
        //        }

        //        if (response.Success)
        //        {
        //            string referenceId = header.PoId;
        //            string invoiceReceivedId = header.Id;

        //            var Id = invoiceReceivedId;
        //            var inv = await client.Get<InvoiceReceivable>($"{ApiUrl.InvoiceReceived}/{Id}") ?? new InvoiceReceivable();


        //            response = await client.PostApiResponse<TemporaryAccountPayable>($"{ApiUrl.TemporaryAccountPayable}/PostDataINV", inv);

        //            return Json(new { success = response.Success, message = response.Message });
        //        }
        //        else {
        //            return Json(new { success = false, message = response.Message });
        //        }
        //    }
        //    catch (Exception ex) {
        //        return Json(new { success = false, message = ex.Message.ToString()});
        //    }
        //}

        //public async Task<IActionResult> ReadDetailFromPOId(string poId, string invoiceId, decimal AmountPaid)
        //{
        //    try
        //    {
        //        var poDetails = new List<PurchaseOrderDetail>();
        //        var invDetails = new List<InvoiceReceivedDetail>();
        //        if (!String.IsNullOrEmpty(invoiceId))
        //        {

        //            //poDetails = await client.Get<List<PurchaseOrderDetailList>>($"{ApiUrl.PurchaseOrderUrl}/getDetailByPOId/{poId}");
        //            //poDetails = await client.Get<List<PurchaseOrderDetailList>>($"{ApiUrl.PurchaseOrderUrl}/GetDetailByPOIdVIew/{poId}");
        //            invDetails = await client.Get<List<InvoiceReceivedDetail>>($"{ApiUrl.InvoiceReceived}/GetDetailById/{invoiceId}");
        //        }
        //        var model = new List<TemporaryAccountPayableDetailList>();
        //        if (poDetails.Count > 0)
        //        {
        //            int row = 0;
        //            foreach (var item in poDetails)
        //            {
        //                model.Add(new TemporaryAccountPayableDetailList()
        //                {
        //                    Id = Guid.NewGuid().ToString(),
        //                    EvidentNumber = item.PurchaseOrderId,
        //                    PayableBalance = null,
        //                    AmountPaid = AmountPaid == 0 ? item.Amount : AmountPaid,
        //                    //Discount = item.DiscountAmount == null ? 0 : item.DiscountAmount,
        //                    //Tax = item.TaxAmount == null ? 0 : item.TaxAmount,
        //                    EndingBalance = null,
        //                    RowIndex = ++row,
        //                    Ite = item.ItemTypeName,
        //                    TaskDetail = item.ItemName,
        //                    qty = item.Qty == null ? 0 : item.Qty,
        //                    //amountTotal = item.AmountNet == null ? 0 : item.AmountNet,
        //                    ItemName = item.ItemName,
        //                    //ItemDescription = item.ItemDescription,
        //                    Item_type_id = item.ItemTypeId,
        //                    ItemTypeName = item.ItemTypeName,
        //                    uom_id = item.UomId,
        //                    uom_name = item.UomName,
        //                    unit_price = item.UnitPrice == null ? 0 : item.UnitPrice,
        //                });
        //            }
        //        }
        //        if (invDetails.Count > 0)
        //        {
        //            int row = 0;
        //            foreach (var item in invDetails)
        //            {
        //                model.Add(new TemporaryAccountPayableDetailList()
        //                {
        //                    Id = Guid.NewGuid().ToString(),
        //                    EvidentNumber = item.InvoiceReceivedNo,
        //                    PayableBalance = null,
        //                    AmountPaid = item.AmountPaid == 0 ? item.Amount : item.AmountPaid,
        //                    Discount = item.Discount == null ? 0 : item.Discount,
        //                    Tax = item.Tax == null ? 0 : item.Tax,
        //                    EndingBalance = null,
        //                    RowIndex = ++row,
        //                    Ite = item.ItemTypeName,
        //                    TaskDetail = item.ItemName,
        //                    qty = item.qty == null ? 0 : item.qty,
        //                    amountTotal = item.amountTotal == null ? 0 : item.amountTotal,
        //                    ItemName = item.ItemName,
        //                    ItemDescription = item.ItemDescription,
        //                    Item_type_id = item.Item_type_id,
        //                    ItemTypeName = item.ItemTypeName,
        //                    uom_id = item.UomId,
        //                    uom_name = item.uom_name,
        //                    unit_price = item.unit_price == null ? 0 : item.unit_price,
        //                });
        //            }
        //        }

        //        return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message.ToString());
        //    }
        //}

        //public async Task<IActionResult> IndexRef(int aptype, string VendorId)
        //{
        //    ViewBag.ApType = aptype;
        //    ViewBag.VendorId = VendorId;

        //    if (aptype == AccountPayableTypeConst.Normal)
        //    {
        //        ViewBag.ApTypeName = "Temporary Account Payable Ref";
        //    }
        //    else if (aptype == AccountPayableTypeConst.OtherForm)
        //    {
        //        ViewBag.ApTypeName = "Purchase Order Ref";
        //    }
        //    else if (aptype == AccountPayableTypeConst.CashAdvance)
        //    {
        //        ViewBag.ApTypeName = "Cash Advance Ref";
        //    }
        //    else if (aptype == AccountPayableTypeConst.PettyCash)
        //    {
        //        ViewBag.ApTypeName = "Petty Cash Ref";
        //    }
        //    return PartialView("_IndexRef");
        //}

        //public async Task<IActionResult> ReadRef([DataSourceRequest] DataSourceRequest request, int aptype, string vendorId)
        //{
        //    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
        //    var model = new List<OurReferenceList>();
        //    if (aptype == AccountPayableTypeConst.PettyCash)
        //    {
        //        var dataPC = await client.Get<List<PettyCash>>(ApiUrl.PettyCashUrl) ?? new List<PettyCash>();
        //        model = (from pc in dataPC
        //                 select new OurReferenceList
        //                 {
        //                     Id = pc.Id,
        //                     MCSNumber = pc.PettyCashNumber,
        //                     InvoiceNumber = pc.Id,
        //                     Date = pc.SettlementDate
        //                 }).ToList();
        //    }
        //    else if (aptype == AccountPayableTypeConst.CashAdvance)
        //    {
        //        var dataCA = await client.Get<List<CashAdvance>>(ApiUrl.CashAdvanceUrl) ?? new List<CashAdvance>();
        //        model = (from ca in dataCA
        //                 select new OurReferenceList
        //                 {
        //                     Id = ca.Id,
        //                     MCSNumber = ca.CaNumber,
        //                     InvoiceNumber = ca.Id,
        //                     Date = ca.CaDate
        //                 }).ToList();
        //    }
        //    else if (aptype == AccountPayableTypeConst.Normal)
        //    {
        //        try
        //        {

        //            var dataTAP = await client.Get<List<InvoiceReceivedList>>($"{ApiUrl.InvoiceReceived}/GetVendorByStatus/3/{vendorId}") ?? new List<InvoiceReceivedList>();
        //            //var dataTAP = await client.Get<List<TemporaryAccountPayableList>>($"{url}/getjoinlist") ?? new List<TemporaryAccountPayableList>();
        //            model = (from inv in dataTAP
        //                     select new OurReferenceList
        //                     {
        //                         Id = inv.Id,
        //                         MCSNumber = inv.InvoiceNumber,
        //                         InvoiceNumber = inv.InvoiceVendor,
        //                         Date = inv.InvoiceDate
        //                     }).ToList();
        //        }
        //        catch (Exception ex)
        //        {
        //            throw new Exception(ex.Message.ToString());

        //        }
        //    }
        //    else if (aptype == AccountPayableTypeConst.OtherForm)
        //    {
        //        var dataPO = await client.Get<List<PurchaseOrder>>($"{ApiUrl.PurchaseOrderUrl}/GetByVendor/{vendorId}") ?? new List<PurchaseOrder>();
        //        model = (from po in dataPO
        //                 select new OurReferenceList
        //                 {
        //                     Id = po.Id,
        //                     MCSNumber = po.PurchaseOrderNumber,
        //                     InvoiceNumber = po.JobId == null ? "INTERNAL " + po.PurchaseOrderNumber : po.JobId,
        //                     Date = po.PurchaseOrderDate
        //                 }).ToList();
        //    }

        //    var data = model.ToDataSourceResult(request);
        //    return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //}

        //public async Task<IActionResult> ChangeData(string id, int aptype)
        //{
        //    if (aptype == AccountPayableTypeConst.PettyCash)
        //    {
        //        var data = await client.Get<PettyCash>($"{ApiUrl.PettyCashUrl}/{id}") ?? new PettyCash();
        //        return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //    }
        //    else if (aptype == AccountPayableTypeConst.CashAdvance)
        //    {
        //        var data = await client.Get<CashAdvance>($"{ApiUrl.CashAdvanceUrl}/{id}") ?? new CashAdvance();
        //        return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //    }
        //    else if (aptype == AccountPayableTypeConst.Normal)
        //    {
        //        var data = await client.Get<InvoiceReceivedList>($"{ApiUrl.InvoiceReceived}/GetWithPoDetail/{id}") ?? new InvoiceReceivedList();
        //        return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //    }
        //    else
        //    {
        //        var data = await client.Get<PurchaseOrder>($"{ApiUrl.PurchaseOrderUrl}/{id}") ?? new PurchaseOrder();
        //        return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //    }

        //}

        //public async Task<IActionResult> Attachment()
        //{
        //    return PartialView("_IndexAttachment");
        //}

        //[HttpPost]
        //public async Task<IActionResult> Save(TemporaryAccountPayable model, List<TemporaryAccountPayableDetailList> details)
        //{
        //    try
        //    {
        //        var response = new Phoenix.WebExtension.RestApi.ApiResponse();
        //        TemporaryAccountPayableDTO dto = new TemporaryAccountPayableDTO();
        //        dto.Header = model;
        //        dto.Details = details;
        //        if (!string.IsNullOrEmpty(model.Id))
        //        {
        //            response = await client.PostApiResponse<TemporaryAccountPayableDTO>($"{ApiUrl.TemporaryAccountPayable}/PostEdit", dto);
        //        }
        //        else
        //        {
        //            response = await client.PostApiResponse<TemporaryAccountPayableDTO>($"{ApiUrl.TemporaryAccountPayable}/Post", dto);
        //        }
        //        return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message.ToString());
        //    }



        //}

        //[HttpPost]
        //public async Task<IActionResult> CreateAutomaticJournal(string TapId)
        //{
        //    var response = new Phoenix.WebExtension.RestApi.ApiResponse();
        //    var model = await client.Get<TemporaryAccountPayableList>($"{ApiUrl.TemporaryAccountPayable}/getjoin/{TapId}") ?? new TemporaryAccountPayableList();
        //    response = await client.PostApiResponse<TemporaryAccountPayableList>($"{ApiUrl.TemporaryAccountPayable}/CreateAutomaticJournal", model);
        //    return Json(new { success = response.Success, message = response.Message });
        //}

        //[HttpPost]
        //public async Task<IActionResult> ConfirmJournal(string TapId)
        //{
        //    var response = new Phoenix.WebExtension.RestApi.ApiResponse();
        //    var model = await client.Get<TemporaryAccountPayableList>($"{ApiUrl.TemporaryAccountPayable}/getjoin/{TapId}") ?? new TemporaryAccountPayableList();
        //    response = await client.PostApiResponse<TemporaryAccountPayableList>($"{ApiUrl.TemporaryAccountPayable}/ConfirmJournal", model);
        //    return Json(new { success = response.Success, message = response.Message });
        //}

        //[HttpPost]
        //public async Task<IActionResult> ConfirmJournalFromInv(InvoiceReceivedList header, List<InvoiceReceivedDetail> details)
        //{
        //    try
        //    {
        //        var response = new ApiResponse();

        //        header.InvoiceVendor = header.InvoiceVendorText != null ? header.InvoiceVendorText : header.InvoiceVendor;
        //        var newModel = new InvoiceReceivedDTO();
        //        newModel.Header = header;
        //        newModel.Details = details;
        //        if (newModel.Header.InvoiceStatusId == Models.Enum.InvoiceReceivedStatus.TAXTAP)
        //        {
        //            newModel.Header.InvoiceStatusId = Models.Enum.InvoiceReceivedStatus.OpenAP;
        //        }

        //        if (!string.IsNullOrEmpty(header.Id))
        //        {
        //            response = await client.PutApiResponse<InvoiceReceivedList>(urlInv, newModel);
        //        }
        //        else
        //        {
        //            response = await client.PostApiResponse<InvoiceReceivedList>(urlInv, newModel);
        //        }
        //        if (response.Success)
        //        {
        //            //TemporaryAccountPayableDTO tapdto = new TemporaryAccountPayableDTO();
        //            //List<TemporaryAccountPayableDetailList> newdt = new List<TemporaryAccountPayableDetailList>();
        //            //var model = new TemporaryAccountPayable();
        //            //model = await client.Get<TemporaryAccountPayable>($"{ApiUrl.TemporaryAccountPayable}/GetTapFromINV/{header.Id}") ?? new TemporaryAccountPayable();
        //            //if (!string.IsNullOrEmpty(model.Id))
        //            //{

        //            //    tapdto.Header = model;

        //            //    foreach (ExecJournalAPView data in details)
        //            //    {
        //            //        TemporaryAccountPayableDetailList dt = new TemporaryAccountPayableDetailList();
        //            //        dt.Id = Guid.NewGuid().ToString();
        //            //        dt.AccountPayableTemporaryId = tapdto.Header.Id;
        //            //        dt.AccountId = data.AccountId == null ? data.coa_id : data.AccountId;
        //            //        dt.AccountName = data.AccountName;
        //            //        if (data.Debit == 0)
        //            //        {
        //            //            dt.AmountPaid = data.Credit;
        //            //        }
        //            //        else
        //            //        {
        //            //            dt.AmountPaid = data.Debit;
        //            //        }
        //            //        dt.unit_price = dt.AmountPaid;
        //            //        dt.uom_id = "EB0F4540-E7E0-4170-A97C-2F21D5E62E7E";
        //            //        dt.qty = 1;
        //            //        dt.ItemDescription = data.DescriptionJournal;
        //            //        dt.amountTotal = dt.AmountPaid * dt.unit_price;
        //            //        newdt.Add(dt);
        //            //    }
        //            //    tapdto.Details = newdt;

        //            //    response = await client.PostApiResponse<TemporaryAccountPayableDTO>($"{ApiUrl.TemporaryAccountPayable}/ConfirmEditJournal", tapdto);
        //            //}
        //            //else
        //            //{
        //            //    TemporaryAccountPayable tap = new TemporaryAccountPayable();
        //            //    tap.InvoiceReceivableId = header.Id;
        //            //    tap.Amount = header.Amount;
        //            //    tap.ExchangeRate = 1;
        //            //    response = await client.PostApiResponse<TemporaryAccountPayable>($"{ApiUrl.TemporaryAccountPayable}/PostData", tap);
        //            //}
        //            return Json(new { success = response.Success, message = response.Message });
        //        }
        //        else {
        //            return Json(new { success = false, message = response.Message });
        //        }

        //    }
        //    catch (Exception ex)
        //    {

        //        return Json(new { success = false, message = ex.Message.ToString() });
        //    }

        //}

        //[HttpPost]
        //public async Task<IActionResult> TaxVerification(string TapId)
        //{
        //    var response = new Phoenix.WebExtension.RestApi.ApiResponse();
        //    var model = await client.Get<TemporaryAccountPayableList>($"{ApiUrl.TemporaryAccountPayable}/getjoin/{TapId}") ?? new TemporaryAccountPayableList();
        //    response = await client.PostApiResponse<TemporaryAccountPayableList>($"{ApiUrl.TemporaryAccountPayable}/TaxVerification", model);
        //    return Json(new { success = response.Success, message = response.Message });
        //}

        //[HttpPost]
        //public async Task<IActionResult> CreateAPData(string TapId)
        //{
        //    var response = new Phoenix.WebExtension.RestApi.ApiResponse();
        //    var model = await client.Get<TemporaryAccountPayableList>($"{ApiUrl.TemporaryAccountPayable}/getjoin/{TapId}") ?? new TemporaryAccountPayableList();
        //    response = await client.PostApiResponse<TemporaryAccountPayableList>($"{ApiUrl.TemporaryAccountPayable}/CreateAPData", model);
        //    return Json(new { success = response.Success, message = response.Message });
        //}

        //[HttpPost]
        //public async Task<IActionResult> CreateAPDataFromInv(string InvoiceReceiveableId)
        //{
        //    var response = new Phoenix.WebExtension.RestApi.ApiResponse();

        //    var model = await client.Get<TemporaryAccountPayableList>($"{ApiUrl.TemporaryAccountPayable}/GetTapFromINV/{InvoiceReceiveableId}") ?? new TemporaryAccountPayableList();
        //    response = await client.PostApiResponse<TemporaryAccountPayableList>($"{ApiUrl.TemporaryAccountPayable}/CreateAPData", model);
        //    return Json(new { success = response.Success, message = response.Message });
        //}

        ////detail list
        //[AcceptVerbs("Post")]
        //public ActionResult CreateDetailList([DataSourceRequest] DataSourceRequest request, TemporaryAccountPayableDetailList model)
        //{
        //    if (model != null && ModelState.IsValid)
        //    {
        //        model.Id = Guid.NewGuid().ToString();
        //    }

        //    return Json(new[] { model }.ToDataSourceResult(request, ModelState), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //}

        //[AcceptVerbs("Post")]
        //public ActionResult UpdateDetailList([DataSourceRequest] DataSourceRequest request, TemporaryAccountPayableDetailList model)
        //{
        //    if (model != null && ModelState.IsValid)
        //    {
        //    }

        //    return Json(new[] { model }.ToDataSourceResult(request, ModelState), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //}

        //[AcceptVerbs("Post")]
        //public ActionResult DestroyDetailList([DataSourceRequest] DataSourceRequest request, TemporaryAccountPayableDetailList model)
        //{
        //    if (model != null)
        //    {
        //    }

        //    return Json(new[] { model }.ToDataSourceResult(request, ModelState), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //}

        //[AcceptVerbs("Post")]
        //public ActionResult CreateJournalList([DataSourceRequest] DataSourceRequest request, ExecJournalAPView model)
        //{
        //    if (model != null && ModelState.IsValid)
        //    {
        //        model.Id = Guid.NewGuid().ToString();
        //    }

        //    return Json(new[] { model }.ToDataSourceResult(request, ModelState), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //}

        //[AcceptVerbs("Post")]
        //public ActionResult UpdateJournalList([DataSourceRequest] DataSourceRequest request, ExecJournalAPView model)
        //{
        //    if (model != null && ModelState.IsValid)
        //    {
        //    }

        //    return Json(new[] { model }.ToDataSourceResult(request, ModelState), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //}

        //[AcceptVerbs("Post")]
        //public ActionResult DestroyJournalList([DataSourceRequest] DataSourceRequest request, ExecJournalAPView model)
        //{
        //    if (model != null)
        //    {
        //    }

        //    return Json(new[] { model }.ToDataSourceResult(request, ModelState), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //}
    }
}