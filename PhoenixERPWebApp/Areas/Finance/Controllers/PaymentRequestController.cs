﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Phoenix.WebExtension.Attributes;
using Phoenix.WebExtension.RestApi;
using PhoenixERPWebApp.Models.Finance.Transaction.PaymentRequest;
using System.Xml;
using PhoenixERPWebApp.Models;
using PhoenixERPWebApp.Models.Enum;
using PhoenixERPWebApp.Models.Finance;
using PhoenixERPWebApp.Models.Finance.Transaction;
using PhoenixERPWebApp.Models.Finance.Transaction.BankPayment;
using Microsoft.AspNetCore.Mvc.Rendering;
using PhoenixERPWebApp.Models.Finance.MasterData;

namespace PhoenixERPWebApp.Areas.Finance.Controllers
{
    [Area("Finance")]
    public class PaymentRequestController : Controller
    {
        string url = ApiUrl.PaymentRequestUrl;
        private readonly ApiClientFactory client;

        public PaymentRequestController(ApiClientFactory client)
        {
            this.client = client;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Detail(string Id, bool isApprove, bool isAdvancePayment)
        {
            var model = new PaymentRequestList();
            if (isApprove.Equals(null))
            {
                isApprove = false;
            }
            ViewBag.RecordID = Id;
            var isCurrentApproval = false;
            if (!string.IsNullOrEmpty(Id))
            {
                if (isApprove == true)
                    ViewBag.Header = "Approve Payment Request";
                else
                    ViewBag.Header = "Update Payment Request";


                model = await client.Get<PaymentRequestList>($"{url}/{Id}");
                if (isAdvancePayment == true)
                {
                    ViewBag.Header = "Update Payment Request";
                    isApprove = false;
                    model.isAdvancePayment = true;
                    //model.Status = "1";
                }

                //jika sumber AP
                if (model.SourceDoc == "AP" || model.SourceDoc == "GL")
                {
                    if (model.PayToType == "1")//petty
                    {

                    }
                    if (model.PayToType == "2")//bank
                    {

                    }
                    if (model.PayToType == "3")//vendor
                    {
                        var vendorModel = await client.Get<Banks>($"{ApiUrl.PaymentRequestUrl}/GetVendorData/{model.PayToVendor}");
                        model.PaytoBankName = vendorModel.BankName;
                        model.SwiftCode = vendorModel.SwiftCode;
                    }
                    if (model.PayToType == "4")//other
                    {

                    }
                    
                    //multiple ap
                    if (!string.IsNullOrEmpty(model.Id))
                    {
                        var detail = await client.Get<List<PaymentRequestDetail>>($"{ApiUrl.PaymentRequestUrl}/GetDetail/{model.Id}");
                        if (detail != null)
                        {
                            decimal? subtotal = 0;
                            foreach (PaymentRequestDetail data in detail)
                            {
                                subtotal += data.PayUnitPrice;
                            }
                            model.pd_NetAmount = subtotal;
                            model.pd_VatPercent = 10;
                            model.pd_Vat = model.pd_NetAmount * (model.pd_VatPercent / 100);
                            model.pd_GrandTotal = model.pd_NetAmount + model.pd_Vat;
                        }
                    }

                }//sumber CA
                else
                {
                    ViewBag.Header = "Request Cash Advance Detail";
                    model.SourceDoc = "CA";
                    var modelca = await client.Get<CashAdvance>($"{ApiUrl.CashAdvanceUrl}/GetByCaNumber/{model.ReferenceNumberId}");
                    //var modelca = await client.Get<CashAdvance>($"{ApiUrl.CashAdvanceUrl}/{model.ReferenceNumberId}");
                    if (modelca != null)
                    {
                        model.cashAdvanceList = modelca;
                        model.Ca_JobId = modelca.JobId;
                        model.Ca_CaPeriodFrom = modelca.CaPeriodFrom;
                        model.Ca_CaPeriodTo = modelca.CaPeriodTo;
                        model.Ca_CashAdvanceId = modelca.Id;
                        model.Ca_ClientName = modelca.ClientName;
                        model.Ca_JobNumber = modelca.JobNumber;
                        model.Ca_LegalEntityId = modelca.LegalEntityId;
                        model.Ca_PayeeAccountName = modelca.PayeeAccountName;
                        model.Ca_PayeeAccountNumber = modelca.PayeeAccountNumber;
                        model.Ca_PayeeBankBranch = modelca.PayeeBankBranch;
                        model.Ca_PayeeBankId = modelca.PayeeBankId;
                        model.Ca_ProjectActivity = modelca.ProjectActivity;
                        model.Ca_VendorName = modelca.VendorName;
                        model.MainServiceCategoryId = modelca.ProjectActivity;
                        model.Ca_pceId = modelca.PurchaseOrderNumber;
                        model.Ca_vendorId = modelca.VendorId;
                        model.Ca_VendorPIC = modelca.VendorPIC;
                        model.Ca_VendorTaxNumber = modelca.VendorTaxNumber;
                        model.Ca_VendorAddress = modelca.VendorAddress;
                        model.CaTotalAmount = modelca.Amount;
                        model.Ca_BusinessUnit = modelca.BusinessUnitId;
                        model.Ca_BrandId = modelca.ClientBrandId;
                        model.Ca_PCETypeId = modelca.PCETypeId;
                        model.Ca_ShareservicesId = modelca.ShareservicesId;
                        model.PayToType = "3";//vendor
                        model.PayToVendor = modelca.VendorId;
                        model.PaytoBankName = modelca.PayeeAccountName;
                        model.PayToAccountNumber = modelca.PayeeAccountNumber;
                        //if (!string.IsNullOrEmpty(model.MainServiceCategoryId))
                        //{
                        //    if (model.MainServiceCategoryId == "1105194186501656222" || model.MainServiceCategoryId == "2225194186501656576")
                        //    {
                        //        model.PayToVendor = "Vendor";
                        //        model.PaytoName = modelca.ClientName;
                        //    }
                        //    else
                        //    {
                        //        model.PayToVendor = "GL";
                        //        model.PaytoName = "Internal";
                        //    }
                        //}

                    }
                }
                isCurrentApproval = await client.Get<bool>($"{ApiUrl.ManageTransApproval}/IsCurrentApproval/{Id}");
                ViewBag.isRequestor = isApprove == false && (model.Status == PaymentRequestStatus.Open);
            }
            else
            {
                var requestor = await client.Get<InfoEmployee>($"{ApiUrl.EmployeeInfoUrl}/getByUserLogin");
                model.SourceDoc = "AP";
                ViewBag.Header = "Create Payment Request";
                model.PaymentRequestDate = DateTime.Now;
                if (!string.IsNullOrEmpty(requestor.BusinessUnitId))
                {
                    model.BusinessUnitId = requestor.BusinessUnitId;
                    ViewBag.isRequestor = isApprove == false;
                }

            }
            ViewBag.processApprove = isApprove && isCurrentApproval;//(model.Status == StatusTransaction.WaitingApproval || model.Status == StatusTransaction.CurrentApproval);
            model.isApprove = isApprove;
            if (isApprove)
            {
                ViewBag.isHcAdmin = true;//belum di set
            }
            return View(model);
        }

        static T CastResponseData<T>(object entity) where T : class
        {
            return entity as T;
        }

        public async Task<IActionResult> Read([DataSourceRequest] DataSourceRequest request, PaymentRequestFilter Filter)
        {
            try
            {
                var response = new ApiResponse();
                List<PaymentRequestList> model = new List<PaymentRequestList>();
                Filter.isBankPayment = false;
                response = await client.PostApiResponse<List<PaymentRequestList>>($"{url}/GetWithFilter/", Filter);

                if (response.Data != null)
                {
                    model = CastResponseData<List<PaymentRequestList>>(response.Data);
                }

                var data = model.ToDataSourceResult(request);
                return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public async Task<IActionResult> GetHistoryPayment([DataSourceRequest] DataSourceRequest request, string Id)
        {
            try
            {

                var model = await client.Get<List<PaymentRequestList>>($"{ApiUrl.PaymentRequestUrl}/GetHistoryPayment/{Id}") ?? new List<PaymentRequestList>();

                var data = model.ToDataSourceResult(request);
                return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }



        public async Task<IActionResult> ReadAPPosted([DataSourceRequest] DataSourceRequest request, PaymentRequestFilter Filter)
        {
            try
            {

                var response = new ApiResponse();
                List<AccountPayable> model = new List<AccountPayable>();

                response = await client.PostApiResponse<List<AccountPayable>>($"{url}/GetListAPPosted/", Filter);

                if (response.Data != null)
                {
                    model = CastResponseData<List<AccountPayable>>(response.Data);
                }

                var data = model.ToDataSourceResult(request);
                return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }


        }

        public async Task<IActionResult> ReadDetailPaymentRequest([DataSourceRequest] DataSourceRequest request, string Id)
        {
            try
            {

                var response = new ApiResponse();
                var model = await client.Get<List<AccountPayable>>($"{ApiUrl.PaymentRequestUrl}/GetListDetailPaymentRequest/{Id}") ?? new List<AccountPayable>();

                var data = model.ToDataSourceResult(request);
                return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }


        }

        public async Task<IActionResult> ReadDetailPaymentRequestByPairingGroup([DataSourceRequest] DataSourceRequest request, string Id)
        {
            try
            {
                var response = new ApiResponse();
                var model = await client.Get<List<AccountPayable>>($"{ApiUrl.PaymentRequestUrl}/GetListDetailPaymentRequest/{Id}") ?? new List<AccountPayable>();

                var data = model.ToDataSourceResult(request);
                return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAccountNumberData(string Id)
        {
            var model = await client.Get<Banks>($"{ApiUrl.PaymentRequestUrl}/GetAccountNumberData/{Id}");
            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        [HttpGet]
        public async Task<IActionResult> GetPettycashData(string Id)
        {
            var model = await client.Get<MasterPettyCash>($"{ApiUrl.PaymentRequestUrl}/GetPettycashData/{Id}");
            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        [HttpGet]
        public async Task<IActionResult> GetVendorData(string Id)
        {
            var model = await client.Get<Banks>($"{ApiUrl.PaymentRequestUrl}/GetVendorData/{Id}");
            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> ReadDetail([DataSourceRequest] DataSourceRequest request)
        {
            //var model = await client.Get<List<AccountPayable>>(url) ?? new List<AccountPayable>();
            var model = new List<PaymentRequestDetailList>();
            model.Add(new PaymentRequestDetailList()
            {
                Id = "001",
                EVDNo = 1080000,
                BeginningBalance = 1729591,
                Applied = 1729591,
                Discount = 0,
                Taxation = 0,
                EndingBalance = 0
            });
            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> ReadPaymentData([DataSourceRequest] DataSourceRequest request, string referenceId)
        {
            var model = await client.Get<List<PaymentInformationData>>($"{ApiUrl.PaymentRequestUrl}/GetPaymentInformationData/{referenceId}") ?? new List<PaymentInformationData>();
            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> ReadJournalBankPayment([DataSourceRequest] DataSourceRequest request, string BankPaymentId)
        {
            var model = await client.Get<List<Journal>>($"{ApiUrl.PaymentRequestUrl}/GetBankPaymentJournal/{BankPaymentId}") ?? new List<Journal>();
            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        [HttpPost]
        public async Task<IActionResult> Save(PaymentRequestList model, List<AccountPayable> Details)
        {
            var response = new ApiResponse();
            List<PaymentRequestDetail> modeldetail = new List<PaymentRequestDetail>();

            if (Details.Count > 0)
            {
                foreach (AccountPayable data in Details)
                {
                    PaymentRequestDetail pd = new PaymentRequestDetail();
                    pd.ReferenceId = data.Id;
                    pd.APNumber = data.AccountPayableNumber;
                    pd.Qty = 1;
                    pd.PayUnitPrice = data.Amount;
                    pd.CurrencyId = model.CurrencyId;
                    pd.Exchange = model.ExchangeRate;
                    pd.SouceDoc = "AP";
                    pd.JobId = data.JobId;
                    pd.Vat = data.Vat;
                    pd.TaxPayable = data.TaxPayable;
                    pd.JobName = data.JobName;
                    pd.TaxPayableCoaId = data.TaxPayableCoaId;
                    modeldetail.Add(pd);
                }
            }


            if (!string.IsNullOrEmpty(model.Id))
            {
                if (string.IsNullOrEmpty(model.Id))
                {
                    model.Status = PaymentRequestStatus.Open;
                }
                if (string.IsNullOrEmpty(model.Status))
                {
                    model.Status = PaymentRequestStatus.Open;
                }

                if(model.PayToType == "1" || model.PayToType == "2" || model.PayToType == "4" )
                { model.SourceDoc = "GL"; }
                    return await Process(model, modeldetail);

            }
            else
            {

                if (string.IsNullOrEmpty(model.Id))
                {
                    model.Status = PaymentRequestStatus.Open;
                }
                if (string.IsNullOrEmpty(model.Status))
                {
                    model.Status = PaymentRequestStatus.Open;
                }
                return await Process(model, modeldetail);

            }
        }

        [HttpPut("Approve")]
        public async Task<IActionResult> Approve(PaymentRequestList model)
        {
            var response = await client.PutApiResponse<PaymentRequestList>($"{url}/ApproveData", model);
            return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
        }

        [HttpPut("Reconsiliations")]
        public async Task<IActionResult> Reconsiliations(PaymentRequestList model)
        {
            var response = await client.PutApiResponse<PaymentRequestList>($"{url}/Reconsiliation", model);
            return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
        }


        [HttpPut("Reject")]
        public async Task<IActionResult> Reject(PaymentRequestList model)
        {

            var response = await client.PutApiResponse<PaymentRequestList>($"{url}/Reject", model);
            return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
        }

        [HttpPost]
        public async Task<IActionResult> CreateAndReadAutomaticJournal(PaymentRequestList model, List<AccountPayable> Details)
        {

            try
            {
                var response = new ApiResponse();
                List<PaymentRequestDetail> modeldetail = new List<PaymentRequestDetail>();
                if (model.PayToType == "1" || model.PayToType == "2" || model.PayToType == "4")
                { model.SourceDoc = "GL"; }
                if (Details.Count > 0)
                {
                    foreach (AccountPayable data in Details)
                    {
                        PaymentRequestDetail pd = new PaymentRequestDetail();
                        pd.ReferenceId = data.Id;
                        pd.APNumber = data.AccountPayableNumber;
                        pd.Qty = 1;
                        pd.PayUnitPrice = data.Amount;
                        pd.CurrencyId = model.CurrencyId;
                        pd.Exchange = model.ExchangeRate;
                        pd.SouceDoc = "AP";
                        pd.JobId = data.JobId;
                        pd.Vat = data.Vat;
                        pd.TaxPayable = data.TaxPayable;
                        pd.JobName = data.JobName;
                        pd.TaxPayableCoaId = data.TaxPayableCoaId;
                        modeldetail.Add(pd);
                    }
                }


                PaymentRequestDTO pay = new PaymentRequestDTO();
                pay.Header = model;
                pay.Details = modeldetail;

                if (!string.IsNullOrEmpty(model.Id))
                {
                    if (model.isAdvancePayment == true)
                    {
                        model.Status = PaymentRequestStatus.Open;
                        response = await client.PostApiResponse<PaymentRequestDTO>($"{url}/AddWithDetail", pay);
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(model.Status))
                        {
                            pay.Header.Status = PaymentRequestStatus.Open;
                        }
                        response = await client.PutApiResponse<PaymentRequestDTO>($"{url}/EditWithDetail", pay);
                    }
                }
                else
                {
                    pay.Header.Status = PaymentRequestStatus.Open;
                    response = await client.PostApiResponse<PaymentRequestDTO>($"{url}/AddWithDetail", pay);
                }
                
                if (response.Success)
                {
                    var responseJournal = new ApiResponse();
                    responseJournal = await client.PostApiResponse<PaymentRequestList>($"{ApiUrl.PaymentRequestUrl}/PostDatatoSp", ((PaymentRequestDTO)response.Data).Header);
                    if (responseJournal.Success)
                    {
                        return Json(new { success = response.Success, message = response.Message, refreshGrid = "gridjournal", data = ((PaymentRequestDTO)response.Data).Header as PaymentRequestList });
                    }
                    else
                    {
                        return Json(new { success = responseJournal.Success, message = responseJournal.Message });
                    }
                }
                else
                {
                    return Json(new { success = false, message = response.Message });
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message.ToString() });
            }
        }

        [HttpPost]
        public async Task<IActionResult> Submit(PaymentRequestList model, List<AccountPayable> Details)
        {
            var response = new ApiResponse();

            List<PaymentRequestDetail> modeldetail = new List<PaymentRequestDetail>();
            if (model.PayToType == "1" || model.PayToType == "2" || model.PayToType == "4")
            { model.SourceDoc = "GL"; }
            if (Details.Count > 0)
            {

                foreach (AccountPayable data in Details)
                {
                    PaymentRequestDetail pd = new PaymentRequestDetail();
                    pd.ReferenceId = data.Id;
                    pd.APNumber = data.AccountPayableNumber;
                    pd.Qty = 1;
                    pd.PayUnitPrice = data.Amount;
                    pd.CurrencyId = model.CurrencyId;
                    pd.Exchange = model.ExchangeRate;
                    pd.SouceDoc = "AP";
                    pd.JobId = data.JobId;
                    pd.Vat = data.Vat;
                    pd.TaxPayable = data.TaxPayable;
                    pd.JobName = data.JobName;
                    pd.TaxPayableCoaId = data.TaxPayableCoaId;
                    modeldetail.Add(pd);
                }

            }

            if (!string.IsNullOrEmpty(model.Id))
            {
                if (string.IsNullOrEmpty(model.Status))
                {
                    model.Status = PaymentRequestStatus.Open;
                }
                else if (model.Status == PaymentRequestStatus.Open)
                {
                    model.Status = PaymentRequestStatus.WaitingApproval;
                }

                return await Process(model, modeldetail);

            }
            else
            {

                if (string.IsNullOrEmpty(model.Status))
                {
                    model.Status = PaymentRequestStatus.Open;
                }
                else if (model.Status == PaymentRequestStatus.Open)
                {
                    model.Status = PaymentRequestStatus.WaitingApproval;
                }

                return await Process(model, modeldetail);

            }
        }

        public async Task<IActionResult> Process(PaymentRequestList model, List<PaymentRequestDetail> Details)
        {
            try
            {
                var response = new ApiResponse();
                PaymentRequestDTO pay = new PaymentRequestDTO();
                pay.Header = model;
                pay.Details = Details;

                if (!string.IsNullOrEmpty(model.Id))
                {
                    if (model.isAdvancePayment == true)
                    {
                        model.Status = PaymentRequestStatus.Open;
                        response = await client.PostApiResponse<PaymentRequestDTO>($"{url}/AddWithDetail", pay);
                    }
                    else
                        response = await client.PutApiResponse<PaymentRequestDTO>($"{url}/EditWithDetail", pay);
                }
                else
                {
                    response = await client.PostApiResponse<PaymentRequestDTO>($"{url}/AddWithDetail", pay);
                }
                return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message.ToString(), url = Url.Action(nameof(Index)) });
            }
        }

        public IActionResult GetPayToType()
        {
            var result = new List<SelectListItem>()
            {
                new SelectListItem() { Text = "Petty Cash", Value = "1" },
                new SelectListItem() { Text = "Bank", Value = "2" },
                new SelectListItem() { Text = "Vendor", Value = "3", Disabled= true},
                new SelectListItem() { Text = "Other", Value = "4"}
            };
            return Json(result, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(PaymentRequestList model) => Json(await client.DeleteApiResponse<PaymentRequestList>($"{url}/{model.Id}"));
    }


}