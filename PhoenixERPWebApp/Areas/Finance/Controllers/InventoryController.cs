﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodeMarvel.Infrastructure.WebNetCore;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Phoenix.Data.Models.Finance.Transaction;
using Phoenix.WebExtension.Extentions;
using Phoenix.WebExtension.RestApi;
using PhoenixERPWebApp.Models;
using PhoenixERPWebApp.Models.Finance.Transaction.Inventory;

namespace PhoenixERPWebApp.Areas.Finance.Controllers
{
    [Area("Finance")]
    public class InventoryController : BaseController
    {
        string uniqueDetailInv = "DETAILINV";
        string url = ApiUrl.Inventory;
        private readonly ApiClientFactory client;

        public InventoryController(ApiClientFactory client)
        {
            this.client = client;
        }

        public IActionResult Index()
        {
            ClearCacheDetailInv();
            return View();
        }

        public async Task<IActionResult> Detail([FromQuery(Name = "id")] string Id)
        {
            ClearCacheDetailInv();
            var model = new Inventory();
            ViewBag.Header = "Inventory Form";
            if (!string.IsNullOrEmpty(Id))
            {
                ViewBag.RecordID = Id;
                model = await client.Get<Inventory>($"{url}/{Id}");
            }
            model.InventoryDetail = new InventoryDetail();
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Save(Inventory Header, List<InventoryDetail> Details)
        {
            try
            {
                var requestor = await client.Get<InfoEmployee>($"{ApiUrl.EmployeeInfoUrl}/getByUserLogin");
                var response = new ApiResponse();
                var model = new InventoryDTO();
                model.Header = Header;
                model.Details = new List<InventoryDetail>();

                if (!string.IsNullOrEmpty(Header.Id))
                {
                    response = await client.PutApiResponse<InventoryDTO>(url, model);
                    //foreach (InventoryDetail data in GetCacheDetailInv())
                    //{
                    //    response = await client.PutApiResponse<InventoryDetail>($"{url}/detail", data, data.FileUpload);
                    //}
                }
                else
                {
                    response = await client.PostApiResponse<InventoryDTO>(url, model);
                }

                return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message.ToString() });
            }

        }

        [HttpPost]
        public async Task<IActionResult> SaveDetailPersist(Inventory model, IFormFile FileAttachmentUpload)
        {
            try
            {
                var response = new ApiResponse();
                var data = model.InventoryDetail;
                response = await client.PutApiResponse<InventoryDetail>($"{url}/detail", data, FileAttachmentUpload);
                return Json(new { success = response.Success, message = response.Message, refreshGrid = "grid", hideModalPopUp = "modalDetailInv" });
            }
            catch(Exception ex)
            {
                return Json(new { success = false, message = ex.Message.ToString() });
            }
        }

        private void ClearCacheDetailInv()
        {
            CacheStore.Remove<List<InventoryDetail>>(uniqueDetailInv);
        }

        public void SetCacheDetailInv(List<InventoryDetail> models)
        {
            ClearCacheDetailInv();
            CacheStore.Add<List<InventoryDetail>>(uniqueDetailInv, models);
        }

        public List<InventoryDetail> GetCacheDetailInv()
        {
            var models = CacheStore.Get<List<InventoryDetail>>(uniqueDetailInv);
            if (models == null)
                models = new List<InventoryDetail>();
            return models;
        }

        [HttpPost]
        public IActionResult SaveDetail(Inventory model, IFormFile FileAttachmentUpload)
        {
            var data = model.InventoryDetail;
            var DetailInvList = GetCacheDetailInv().Where(x => x.Id != data.Id).ToList();
            var detail = new InventoryDetail();
            detail.Id = data.Id;
            detail.DateReceipt = data.DateReceipt;
            detail.Manufacturer = data.Manufacturer;
            detail.Brand = data.Brand;
            detail.Model = data.Model;
            detail.ProductNumber = data.ProductNumber;
            detail.Color = data.Color;
            detail.Condition = data.Condition;
            detail.Qty = data.Qty;
            detail.Location = data.Location;
            detail.UomId = data.UomId;
            detail.UnitName = data.UnitName;
            detail.Location = data.Location;
            detail.ConditionName = data.ConditionName;
            detail.LocationName = data.LocationName;
            detail.Remark = data.Remark;
            if (FileAttachmentUpload != null)
            {
                detail.FileUpload = FileAttachmentUpload;
                detail.FileAttachmentName = FileAttachmentUpload.FileName;
            }
            DetailInvList.Add(detail);
            SetCacheDetailInv(DetailInvList);

            return Json(new { success = true, message = "success", refreshGrid = "grid", hideModalPopUp = "modalDetailInv" });
        }

        public IActionResult ReadDetailList([DataSourceRequest] DataSourceRequest request)
        {
            var model = GetCacheDetailInv();
            //if (!isRequestor)
            //    model = model.Where(x => x.IsRequestPurchaseOrder == true).ToList();
            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> Read([DataSourceRequest] DataSourceRequest request)
        {
            var model = await client.Get<List<Inventory>>($"{url}") ?? new List<Inventory>();
            
            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> ReadDetail([DataSourceRequest] DataSourceRequest request, string invId)
        {
            var model = new List<InventoryDetail>();
            //if (GetCacheDetailInv().Count>0)
            //{   
            //    model = GetCacheDetailInv();
            //}
            //else
            //{
            //    model = await client.Get<List<InventoryDetail>>($"{url}/getDetail/{invId}") ?? new List<InventoryDetail>();
            //    SetCacheDetailInv(model);
            //}
            model = await client.Get<List<InventoryDetail>>($"{url}/getDetail/{invId}") ?? new List<InventoryDetail>();
            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> ReadDetailByGoodReceipt([DataSourceRequest] DataSourceRequest request, string grId)
        {
            var model = await client.Get<List<InventoryDetail>>($"{url}/getDetailByGoodReceipt/{grId}") ?? new List<InventoryDetail>();
            
            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }
    }
}