﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodeMarvel.Infrastructure.WebNetCore;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Phoenix.WebExtension.RestApi;
using PhoenixERPWebApp.Models;
using PhoenixERPWebApp.Models.Finance.Transaction.FixedAsset;

namespace PhoenixERPWebApp.Areas.Finance.Controllers
{
    [Area("Finance")]
    public class FixedAssetDepreciationController : BaseController
    {
        string url = ApiUrl.FixedAssetDepreciationtUrl;
        string urlFA = ApiUrl.FixedAssetUrl;
        private readonly ApiClientFactory client;

        public FixedAssetDepreciationController(ApiClientFactory client)
        {
            this.client = client;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Detail([FromQuery(Name = "id")] string Id)
        {
            var model = new FixedAsset();
            ViewBag.processApprove = false;
            if (!string.IsNullOrEmpty(Id))
            {
                if (ViewBag.processApprove == true)
                    ViewBag.Header = "Approve Fixed Asset Depreciation";
                else
                    ViewBag.Header = "Update Fixed Asset Depreciation";
                model = await client.Get<FixedAsset>($"{urlFA}/{Id}");
                ViewBag.processApprove = await client.Get<bool>($"{ApiUrl.ManageTransApproval}/IsCurrentApproval/{Id}");
                ViewBag.isRequestor = ViewBag.processApprove == false && (model.StatusDepreciation == StatusTransaction.Draft.ToString() || model.StatusDepreciation == StatusTransaction.Rejected.ToString() || model.StatusDepreciation == null);
            }
            else
            {
                ViewBag.Header = "Create Fixed Asset Depreciation";
                ViewBag.isRequestor = ViewBag.processApprove == false;
                model.StatusDepreciation = StatusTransaction.Draft.ToString();
            }
            return View(model);
        }

        public async Task<IActionResult> Read([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                var model = await client.Get<List<FixedAsset>>($"{url}/fixedasset/") ?? new List<FixedAsset>();
                var data = model.ToDataSourceResult(request);
                return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public async Task<IActionResult> ReadDetail([DataSourceRequest] DataSourceRequest request,string faId)
        {
            try
            {
                var model = await client.Get<List<FixedAssetDepreciation>>($"{url}/getByFA/{faId}") ?? new List<FixedAssetDepreciation>();
                var data = model.ToDataSourceResult(request);
                return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public async Task<IActionResult> GetDetail([DataSourceRequest] DataSourceRequest request,string faId)
        {
            try
            {
                var newListFA = new List<FixedAsset>();
                var model = await client.Get<FixedAsset>($"{urlFA}/{faId}") ?? new FixedAsset();
                newListFA.Add(model);
                var data = newListFA.ToDataSourceResult(request);
                return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        [AcceptVerbs("Post")]
        public ActionResult FA_Update([DataSourceRequest] DataSourceRequest request, FixedAsset fa)
        {
            if (fa != null && ModelState.IsValid)
            {
                //productService.Update(cad);
            }
            return Json(new[] { fa }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public async Task<IActionResult> Generate(string faId)
        {
            try
            {   
                var response = new ApiResponse();

                response = await client.GetApiResponse<int>($"{url}/generate/{faId}");

                return Json(new { success = true, message = response.Message });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message.ToString() });
            }

        }

        [HttpPut]
        public async Task<IActionResult> Approve(FixedAsset Header, List<FixedAsset> Details)
        {
            Header.StatusDepreciation = StatusTransaction.Approved.ToString();
            Header.GrossValue = Details[0].GrossValue;
            Header.Residue = Details[0].Residue;
            Header.Lifetime = Details[0].Lifetime;
            Header.AcquisitionCost = Details[0].AcquisitionCost;
            Header.Rounding = Details[0].Rounding;

            var response = await client.PutApiResponse<FixedAsset>($"{url}/Approve", Header);
            return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
        }

        [HttpPut]
        public async Task<IActionResult> Reject(FixedAsset Header, List<FixedAsset> Details)
        {
            Header.StatusDepreciation = StatusTransaction.Rejected.ToString();
            Header.GrossValue = Details[0].GrossValue;
            Header.Residue = Details[0].Residue;
            Header.Lifetime = Details[0].Lifetime;
            Header.AcquisitionCost = Details[0].AcquisitionCost;
            Header.Rounding = Details[0].Rounding;

            var response = await client.PutApiResponse<FixedAsset>($"{url}/Reject", Header);
            return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
        }

        [HttpPost]
        public async Task<IActionResult> Submit(FixedAsset Header, List<FixedAsset> Details)
        {
            Header.StatusDepreciation = StatusTransaction.WaitingApproval.ToString();
            return await Process(Header, Details);
        }

        [HttpPost]
        public async Task<IActionResult> Save(FixedAsset Header, List<FixedAsset> Details)
        {
            Header.StatusDepreciation = StatusTransaction.Draft.ToString();
            return await Process(Header, Details);
        }

        private async Task<IActionResult> Process(FixedAsset Header, List<FixedAsset> Details)
        {
            var response = new ApiResponse();
            Header.GrossValue = Details[0].GrossValue;
            Header.Residue = Details[0].Residue;
            Header.Lifetime = Details[0].Lifetime;
            Header.AcquisitionCost = Details[0].AcquisitionCost;
            Header.Rounding = Details[0].Rounding;
            
            response = await client.PutApiResponse<FixedAsset>($"{url}/approval", Header);
            return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
        }
    }
}