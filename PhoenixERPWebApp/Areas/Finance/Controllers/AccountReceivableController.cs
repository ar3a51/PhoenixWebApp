﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CodeMarvel.Infrastructure.WebNetCore;
using Kendo.Mvc.UI;
using Phoenix.WebExtension.RestApi;
using PhoenixERPWebApp.Models.Finance.Transaction;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Microsoft.AspNetCore.Mvc.Rendering;
using PhoenixERPWebApp.Models;

namespace PhoenixERPWebApp.Areas.Finance.Controllers
{
    [Area("Finance")]
    public class AccountReceivableController : BaseController
    {
        private readonly ApiClientFactory client;
        string ar_url = ApiUrl.ARClientUrl;
        string url_invoiceMultiple = ApiUrl.InvoiceClientMultipleUrl;
        string url_invoiceSingle = ApiUrl.InvoiceClientUrl;

        public AccountReceivableController(ApiClientFactory client)
        {
            this.client = client;
        }

        public IActionResult Index()
        {
            return View();
        }
        
        public async Task<IActionResult> ReadIndex([DataSourceRequest] DataSourceRequest request, string legalentity, string status)
        {
            string url = $"{ar_url}/GetIndexList/{legalentity}/{status}";
            var dataPC = await client.Get<List<InvoiceClient>>($"{ar_url}/GetIndexList") ?? new List<InvoiceClient>();
            var data = dataPC.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> Detail(string invno, string multid, string status)
        {
            if (status == "ACCRUED")
                status = "Posted";
            if (!string.IsNullOrWhiteSpace(multid))
            {
                string urlAr = $"{ar_url}/{multid}";
                var modelAr = await client.Get<AccountReceivable>(urlAr);

                InvoiceClient model = null;
                string url = $"{url_invoiceMultiple}/GetPceInfo/{multid}";
                model = await client.Get<InvoiceClient>(url);
                if (model == null)
                    model = new InvoiceClient();

                model.InvoiceDeliveryDate = modelAr.InvoiceDeliveryDate;
                model.InvoiceDeliveryNo = modelAr.InvoiceDeliveryNotesId;
                model.MultiPceId = multid;
                model.Status = status;
                return View(model);
            }
            else
            {
                string urlAr = $"{ar_url}/{invno}";
                var modelAr = await client.Get<AccountReceivable>(urlAr);

                var pceno = "fromAR";
                string url = $"{url_invoiceSingle}/GetInvoiceInfo/{pceno}/{invno}";
                var model = await client.Get<InvoiceClient>(url);
                model.InvoiceDeliveryDate = modelAr.InvoiceDeliveryDate;
                model.InvoiceDeliveryNo = modelAr.InvoiceDeliveryNotesId;
                model.DivisionName = model.Division;
                model.MultiPceId = "";
                model.Status = status;
                return View(model);
            }

        }

        [HttpPost]
        public async Task<IActionResult> CreateJournalAR(InvoiceClient iv)
        {
            var entity = await client.Get<AccountReceivable>($"{ar_url}/GetDetailAR/{iv.Id}");
            if (entity.InvoiceDeliveryDate != null)
            {
                var model = await client.PostApiResponse<int>($"{ar_url}/CreateJournalAR", iv);
                return Json(new { success = model.Success, message = model.Message, refreshGrid = "grid" });
            }
            else
            {
                return Json(new { success = false, message = " Please complete all form" });
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateJournalMultipleAR(InvoiceClient iv)
        {
            var model = await client.PostApiResponse<int>($"{ar_url}/CreateJournalMultipleAR", iv);
            return Json(new { success = model.Success, message = model.Message, refreshGrid = "grid" });
        }        

        [HttpPost]
        public async Task<IActionResult> SubmitAR(InvoiceClient iv)
        {
            var entity = await client.Get<AccountReceivable>($"{ar_url}/GetDetailAR/{iv.InvoiceNumber}");
            if (entity.InvoiceDeliveryDate != null)
            {
                var model = await client.PostApiResponse<int>($"{ar_url}/SubmitAR", iv);
                return Json(new { success = model.Success, message = model.Message, url = Url.Action(nameof(Index)) });
            }
            else
            {
                return Json(new { success = false, message = " Please complete all form" });
            }
            
        }

        public async Task<IActionResult> GerListJournalAR([DataSourceRequest] DataSourceRequest request, string invno, string multipceid)
        {
            string url = $"{ar_url}/GerListJournalAR/{invno}/{multipceid}";
            var model = await client.Get<List<Journal>>(url);
            if (model == null)
                model = new List<Journal>();
            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }
        

        public async Task<IActionResult> GerListJournal([DataSourceRequest] DataSourceRequest request, string invno, string multipceid)
        {
            string url = $"{ar_url}/GerListJournal/{invno}/{multipceid}";
            var model = await client.Get<List<Journal>>(url);
            if (model == null)
                model = new List<Journal>();
            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> ReadInvDtl([DataSourceRequest] DataSourceRequest request, string invno)
        {
            string url = $"{ar_url}/ReadInvDtl/{invno}";
            var model = await client.Get<List<InvoiceClientItemOther>>(url);
            if (model == null)
                model = new List<InvoiceClientItemOther>();
            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> ReadMultiPceList([DataSourceRequest] DataSourceRequest request, string multipceid)
        {
            string invidstr = (string.IsNullOrEmpty(multipceid) ? "0" : multipceid);

            string url = $"{url_invoiceMultiple}/ReadMultiPceList/{multipceid}";
            var data = await client.Get<List<InvoiceClient>>(url) ?? new List<InvoiceClient>();
            var model = data.ToDataSourceResult(request);
            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public IActionResult Settlement()
        {
            return View("StatementAR");
        }

        public async Task<IActionResult> GetListStatement([DataSourceRequest] DataSourceRequest request, string arid)
        {
            string url = $"{ar_url}/GetListStatement/{arid}";
            var data = await client.Get<List<InvoiceClient>>(url) ?? new List<InvoiceClient>();
            var model = data.ToDataSourceResult(request);
            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<ActionResult> GetChartOfAccountTradeAndTradeInter()
        {
            var getUrl = $"{ar_url}/GetChartOfAccountTradeAndTradeInter";
            var model = await client.Get<List<SelectListItem>>(getUrl) ?? new List<SelectListItem>();
            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        #region untukgrid
        [AcceptVerbs("Post")]
        public async Task<IActionResult> Create_([DataSourceRequest] DataSourceRequest request, InvoiceClientItemOther model)
        {
            if (ModelState.IsValid)
            {
                model.Id = Guid.NewGuid().ToString();
            }
            return Json(new[] { model }.ToTreeDataSourceResult(request, ModelState), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        [AcceptVerbs("Post")]
        public async Task<IActionResult> Update_([DataSourceRequest] DataSourceRequest request, InvoiceClientItemOther model)
        {
            //if (model != null && ModelState.IsValid)
            //{
            //}
            return Json(new[] { model }.ToTreeDataSourceResult(request, ModelState), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        [AcceptVerbs("Post")]
        public async Task<IActionResult> Delete_([DataSourceRequest] DataSourceRequest request, InvoiceClientItemOther model)
        {
            //if (model != null)
            //{
            //}
            return Json(new[] { model }.ToTreeDataSourceResult(request, ModelState), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }
        #endregion
    }
}