﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodeMarvel.Infrastructure.WebNetCore;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Phoenix.WebExtension.RestApi;
using PhoenixERPWebApp.Models;
using PhoenixERPWebApp.Models.Finance.Transaction.FixedAsset;
using PhoenixERPWebApp.Models.Finance.Transaction.InventoryGoodsReceipt;

namespace PhoenixERPWebApp.Areas.Finance.Controllers
{
    [Area("Finance")]
    public class FixedAssetController : BaseController
    {
        string url = ApiUrl.FixedAssetUrl;
        string urlGR = ApiUrl.InventoryGoodsReceipt;
        string urlFaMovement = ApiUrl.FixedAssetMovementUrl;
        
        private readonly ApiClientFactory client;

        public FixedAssetController(ApiClientFactory client)
        {
            this.client = client;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Detail([FromQuery(Name = "id")] string Id)
        {   
            var model = new FixedAsset();
            ViewBag.Header = "Fixed Asset";
            if (!string.IsNullOrEmpty(Id))
            {
                ViewBag.RecordID = Id;
                model = await client.Get<FixedAsset>($"{url}/{Id}");
            }
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Save(FixedAsset Header, List<FixedAssetMovement> Details)
        {
            try
            {
                var requestor = await client.Get<InfoEmployee>($"{ApiUrl.EmployeeInfoUrl}/getByUserLogin");
                var response = new ApiResponse();
                var model = new FixedAssetDTO();
                model.Header = Header;
                model.Details = Details;

                if (!string.IsNullOrEmpty(Header.Id))
                {
                    response = await client.PutApiResponse<FixedAsset>(url, model);
                }
                else
                {
                    response = await client.PostApiResponse<FixedAsset>(url, model);
                }

                return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message.ToString() });
            }

        }

        public async Task<IActionResult> Read([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                //List<InventoryGoodsReceipt> model = new List<InventoryGoodsReceipt>();

                var model = await client.Get<List<FixedAsset>>($"{url}") ?? new List<FixedAsset>();
                //if (response != null)
                //{
                //    model = CastResponseData<List<InventoryGoodsReceipt>>(response.Data);
                //}

                var data = model.ToDataSourceResult(request);
                return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }

        }

        public async Task<IActionResult> GetDataGR(string grId)
        {
            var model = await client.Get<InventoryGoodsReceipt>($"{urlGR}/{grId}");
            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        [AcceptVerbs("Post")]
        public ActionResult FAStatus_Create([DataSourceRequest] DataSourceRequest request, FixedAssetMovement fams)
        {
            if (fams != null && ModelState.IsValid)
            {
                fams.Id = Guid.NewGuid().ToString();
            }
            //return Json(new[] { fams }.ToDataSourceResult(request, ModelState));
            return Json(new[] { fams }.ToDataSourceResult(request, ModelState), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        [AcceptVerbs("Post")]
        public ActionResult FAStatus_Update([DataSourceRequest] DataSourceRequest request, FixedAssetMovement fams)
        {
            if (fams != null && ModelState.IsValid)
            {
                //productService.Update(cad);
            }
            return Json(new[] { fams }.ToDataSourceResult(request, ModelState));
            //return Json(new[] { fams }.ToDataSourceResult(request, ModelState), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        [AcceptVerbs("Post")]
        public ActionResult FAStatus_Destroy([DataSourceRequest] DataSourceRequest request, FixedAssetMovementStatus fams)
        {
            if (fams != null && ModelState.IsValid)
            {
                //productService.Update(cad);
            }
            return Json(new[] { fams }.ToDataSourceResult(request, ModelState));
            //return Json(new[] { fams }.ToDataSourceResult(request, ModelState), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> ReadFAStatus([DataSourceRequest] DataSourceRequest request, string faId)
        {
            try
            {
                var model = await client.Get<List<FixedAssetMovement>>($"{urlFaMovement}/getByFixedAsset/{faId}") ?? new List<FixedAssetMovement>();
                var data = model.ToDataSourceResult(request);
                return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }

        }

        public async Task<IActionResult> GetAffiliation(string buId)
        {
            var data = await client.Get<Affiliation>($"{url}/GetAffiliation/{buId}") ?? new Affiliation();
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }
    }
}