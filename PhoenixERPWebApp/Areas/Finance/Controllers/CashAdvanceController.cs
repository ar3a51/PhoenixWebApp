﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CodeMarvel.Infrastructure;
using CodeMarvel.Infrastructure.Sessions;
using CodeMarvel.Infrastructure.WebNetCore;
using Microsoft.Extensions.Configuration;
using CodeMarvel.Infrastructure.Options;
using Microsoft.Extensions.Options;
using Phoenix.WebExtension.RestApi;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using PhoenixERPWebApp.Models.Finance;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PhoenixERPWebApp.Models;
using System.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.Net.Http.Headers;
using System.IO;
using Microsoft.AspNetCore.Hosting.Internal;
using PhoenixERPWebApp.Models.Enum;
using PhoenixERPWebApp.Models.Finance.Transaction;

namespace PhoenixERPWebApp.Areas.Finance.Controllers
{
    [Area("Finance")]
    public class CashAdvanceController : BaseController
    {
        string url = ApiUrl.CashAdvanceUrl;
        private readonly ApiClientFactory client;
        public IHostingEnvironment HostingEnvironment { get; set; }

        public CashAdvanceController(ApiClientFactory client, IHostingEnvironment hostingEnvironment)
        {
            this.client = client;
            HostingEnvironment = hostingEnvironment;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult IndexApproval()
        {
            return View();
        }

        public async Task<IActionResult> Detail(string Id, bool isApprove)
        {
            DateTime _now = DateTime.Now;
            if (isApprove.Equals(null))
            {
                isApprove = false;
            }
            var model = new CashAdvance()
            {
                CreatedOn = _now,
                RequestedOn = _now,
                CaDate = _now,
                CaNumber = $"CA{_now.ToString("yyyyMMdd")}00000",
            };
            var isCurrentApproval = false;
            if (!string.IsNullOrEmpty(Id))
            {
                if (isApprove == true)
                    ViewBag.Header = "Approve Cash Advance";
                else
                    ViewBag.Header = "Update Cash Advance";
                JsonSerializerSettings settings = new JsonSerializerSettings { ContractResolver = new CustomContractResolver() };
                model = await client.Get<CashAdvance>($"{url}/{Id}", settings);
                isCurrentApproval = await client.Get<bool>($"{ApiUrl.ManageTransApproval}/IsCurrentApproval/{Id}");
                ViewBag.isRequestor = isApprove == false && (model.Status == StatusTransaction.Draft); //|| model.Status == StatusTransaction.Rejected);

                var requestor = await client.Get<InfoEmployee>($"{ApiUrl.EmployeeInfoUrl}/getByUserLogin");
                model.RequestEmployee = requestor;
                model.RequestedBy = requestor.EmployeeId;
                model.CreatedBy = requestor.EmployeeId;
                model.EmployeeId = requestor.EmployeeId;
                ViewBag.EmployeeIdx = requestor.EmployeeId;
                ViewBag.BU = requestor.BusinessUnitId;
            }
            else
            {
                ViewBag.Header = "Create Cash Advance";
                var requestor = await client.Get<InfoEmployee>($"{ApiUrl.EmployeeInfoUrl}/getByUserLogin");
                model.RequestEmployee = requestor;
                model.RequestedBy = requestor.EmployeeId;
                model.CreatedBy = requestor.EmployeeId;
                model.EmployeeId = requestor.EmployeeId;
                model.RequestEmployeeName = requestor.EmployeeName;
                model.RequestJobGradeName = requestor.Grade;
                model.RequestJobTitleName = requestor.JobTitle;
                model.RequestJobGradeId = requestor.GradeId;
                model.RequestJobTitleId = requestor.JobTitleId;
                model.RequestBusinessUnitId = requestor.BusinessUnitId;
                model.ProjectActivity = MainServiceCategoryOther.MainServiceCategoryId;
                model.BusinessUnitId = requestor.BusinessUnitId;
                ViewBag.EmployeeIdx = requestor.EmployeeId;
                //model.infoEmployee = new InfoEmployee();
                ViewBag.isRequestor = isApprove == false;
                ViewBag.BU = requestor.BusinessUnitId;

                //PCA --Yanto
                var get = GlobalCache.GetPCA();
                if (get != null)
                {
                    JobDetail job = await client.Get<JobDetail>($"{url}/GetJobDetail/{get.JobId}");
                    model.JobId = get.JobId;
                    model.ProjectActivity = get.MainserviceCategoryId;
                    model.JobNumber = get.Id;
                    model.ProjectActivity = get.MainserviceCategoryId;
                    model.IsProjectCost = true;
                    model.MainserviceCategoryId = get.MainserviceCategoryId;
                    model.ClientName = job.CompanyId;
                    model.ShareservicesId = get.ShareservicesId;
                    model.PCETypeId = get.TypeOfExpenseId;
                    model.ClientBrandId = job.BrandId;
                    model.BusinessUnitId = job.BusinessUnitDivisionId;
                    model.LegalEntityId = job.LegalEntityId;
                    model.PurchaseOrderNumber = get.PceId;
                    GlobalCache.ClearPCA();
                }else
                { model.IsProjectCost = false; }
            }

            ViewBag.processApprove = isApprove && isCurrentApproval;//(model.Status == StatusTransaction.WaitingApproval || model.Status == StatusTransaction.CurrentApproval);
            model.Notes = HttpUtility.HtmlDecode(model.Notes);
            model.IsApprove = isApprove;
            if (isApprove)
            {
                ViewBag.isHcAdmin = true;//belum di set
            }
            ViewBag.isApprove = isApprove;

            return View(model);
        }

        public async Task<IActionResult> Settle(string CaId, bool isApprove)
        {
            if (isApprove.Equals(null))
            {
                isApprove = false;
            }

            var model = new CashAdvanceSattlement();
            if (string.IsNullOrEmpty(CaId))
            {
                return Json(new { success = true, url = Url.Action("Index", "CashAdvance") });
            }
            else
            {
                var modelcas = await client.Get<CashAdvanceSattlement>($"{url}/GetSattlementById/{CaId}");

                var isCurrentApproval = false;
                if (modelcas == null)
                {
                    var modelca = await client.Get<CashAdvance>($"{url}/{CaId}");

                    model.BusinessUnitId = modelca.BusinessUnitId;
                    model.AccountNumber = modelca.PayeeAccountNumber;
                    model.Amount = modelca.Amount;
                    model.RequestedOn = modelca.RequestedOn;
                    model.BankId = modelca.PayeeBankId;
                    model.CashAdvanceId = CaId;
                    model.CashAdvanceSettleDate = DateTime.Now;
                    model.CaDate = modelca.CaDate;
                    model.CaNumber = modelca.CaNumber;
                    model.JobId = modelca.JobId;
                    model.JobNumber = modelca.JobNumber;
                    model.RequestEmployeeId = modelca.RequestEmployeeId;
                    model.RequestEmployeeName = modelca.RequestEmployeeName;
                    model.RequestJobGradeId = modelca.RequestJobGradeId;
                    model.RequestJobGradeName = modelca.RequestJobGradeName;
                    model.RequestJobTitleId = modelca.RequestJobTitleId;
                    model.RequestJobTitleName = modelca.RequestJobTitleName;
                    model.ClientName = modelca.ClientName;
                    model.ProjectActivity = modelca.ProjectActivity;
                    model.LegalEntityId = modelca.LegalEntityId;
                    model.VendorName = modelca.VendorName;
                    model.PayeeAccountName = modelca.PayeeAccountName;
                    model.PayeeAccountNumber = modelca.PayeeAccountNumber;
                    model.PayeeBankBranch = modelca.PayeeBankBranch;
                    model.PayeeBankId = modelca.PayeeBankId;
                    model.CaPeriodFrom = modelca.CaPeriodFrom;
                    model.CaPeriodTo = modelca.CaPeriodTo;
                    model.MainServiceCategoryId = modelca.ProjectActivity;
                    model.Id = null;
                    model.Status = int.Parse(modelca.CaStatusId);
                    model.CashAdvanceCa = modelca.Amount;
                }
                else
                {
                    var modelca = await client.Get<CashAdvance>($"{url}/{CaId}");
                    var modelcasdetail = await client.Get<List<CashAdvanceSettlementDetail>>($"{url}/GetSattlementDetailById/{modelcas.Id}");

                    isCurrentApproval = await client.Get<bool>($"{ApiUrl.ManageTransApproval}/IsCurrentApproval/{modelcas.Id}");
                    ViewBag.isRequestor = isApprove == false; //|| model.Status == StatusTransaction.Rejected);

                    model.BusinessUnitId = modelca.BusinessUnitId;
                    model.AccountNumber = modelca.PayeeAccountNumber;
                    if (modelcasdetail != null)
                    {
                        decimal? ttl = 0;
                        foreach (CashAdvanceSettlementDetail data in modelcasdetail)
                        {
                            ttl += data.ActualAmount;
                        }
                        model.Amount = ttl;
                    }
                    else
                    {
                        model.Amount = 0;
                    }
                    model.RequestedOn = modelca.RequestedOn;
                    model.BankId = modelca.PayeeBankId;
                    model.CashAdvanceId = CaId;
                    model.CashAdvanceSettleDate = DateTime.Now;
                    model.CaDate = modelca.CaDate;
                    model.CaNumber = modelca.CaNumber;
                    model.JobId = modelca.JobId;
                    model.JobNumber = modelca.JobNumber;
                    model.RequestEmployeeId = modelca.RequestEmployeeId;
                    model.RequestEmployeeName = modelca.RequestEmployeeName;
                    model.RequestJobGradeId = modelca.RequestJobGradeId;
                    model.RequestJobGradeName = modelca.RequestJobGradeName;
                    model.RequestJobTitleId = modelca.RequestJobTitleId;
                    model.RequestJobTitleName = modelca.RequestJobTitleName;
                    model.ClientName = modelca.ClientName;
                    model.ProjectActivity = modelca.ProjectActivity;
                    model.LegalEntityId = modelca.LegalEntityId;
                    model.VendorName = modelca.VendorName;
                    model.PayeeAccountName = modelca.PayeeAccountName;
                    model.PayeeAccountNumber = modelca.PayeeAccountNumber;
                    model.PayeeBankBranch = modelca.PayeeBankBranch;
                    model.PayeeBankId = modelca.PayeeBankId;
                    model.CaPeriodFrom = modelca.CaPeriodFrom;
                    model.CaPeriodTo = modelca.CaPeriodTo;
                    model.MainServiceCategoryId = modelca.ProjectActivity;
                    model.Vat = modelcas.Vat;
                    model.Id = modelcas.Id;
                    ViewBag.RecordId = modelcas.Id;
                    model.VatPercent = modelcas.VatPercent;
                    model.VatOn = modelcas.VatOn;
                    model.TaxPayablePercent = modelcas.TaxPayablePercent;
                    model.TaxPayable = modelcas.TaxPayable;
                    model.TaxPayableOn = modelcas.TaxPayableOn;
                    model.TotalAmount = modelcas.TotalAmount;
                    model.CashAdvanceCa = modelca.Amount;
                    model.Balance = modelcas.Balance;
                    model.CaStatusId = modelcas.CaStatusId;
                    model.Status = int.Parse(modelcas.CaStatusId);
                }

                ViewBag.processApprove = isApprove && isCurrentApproval;//(model.Status == StatusTransaction.WaitingApproval || model.Status == StatusTransaction.CurrentApproval);
                model.IsApprove = isApprove;
                if (isApprove)
                {
                    ViewBag.isHcAdmin = true;//belum di set
                }
                ViewBag.isApprove = isApprove;

            }

            return View(model);
        }

        public async Task<IActionResult> Read([DataSourceRequest] DataSourceRequest request, string legalEntity, string mainServiceCategory)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings { ContractResolver = new CustomContractResolver() };
            List<string> query = new List<string>();
            if (!string.IsNullOrEmpty(legalEntity))
            {
                query.Add($"legal={legalEntity}");
            }
            if (!string.IsNullOrEmpty(mainServiceCategory))
            {
                query.Add($"svc={mainServiceCategory}");
            }
            string queryParam = query.Count > 0 ? "?" + string.Join("&", query) : string.Empty;

            var model = await client.Get<List<CashAdvance>>($"{url}{queryParam}", settings) ?? new List<CashAdvance>();
            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> ReadJournal([DataSourceRequest] DataSourceRequest request, string CaId)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings { ContractResolver = new CustomContractResolver() };

            //var cas = await client.Get<CashAdvanceSattlement>($"{url}/GetSattlementById/{CaId}", settings) ?? new CashAdvanceSattlement();

            var model = await client.Get<List<Journal>>($"{url}/GetJournalDataBySattlementId/{CaId}", settings) ?? new List<Journal>();
            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> ReadApproval([DataSourceRequest] DataSourceRequest request)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() };
            var model = await client.Get<List<CashAdvance>>($"{url}/approvalList", settings) ?? new List<CashAdvance>();
            var data = model.ToDataSourceResult(request);
            return Json(data);
        }

        public async Task<IActionResult> ReadDetail([DataSourceRequest] DataSourceRequest request, string caId)
        {
            var returnVal = new List<CashAdvanceDetail>();
            var pcaTaskList = GlobalCache.GetCashAdvanceList();

            if (pcaTaskList != null && pcaTaskList.Count > 0)
            {
                foreach (CashAdvanceDetail data in pcaTaskList)
                {
                    //Uom uomdata =  await client.Get<Uom>($"{url}/GetUom/{data.UomId}");
                    data.Id = Guid.NewGuid().ToString();
                    //data.Amount = data.UnitPrice * data.Qty;
                    data.CashAdvanceId = Guid.NewGuid().ToString();
                    //data.UomName = uomdata.unit_name;
                    returnVal.Add(data);
                }
                GlobalCache.ClearCashAdvanceList();
            }
            else
            {
                returnVal = await client.Get<List<CashAdvanceDetail>>($"{url}/GetDetails/{caId}") ?? new List<CashAdvanceDetail>();
            }
            return Json(returnVal.ToDataSourceResult(request), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        //public async Task<IActionResult> ReadDetailSattelement([DataSourceRequest] DataSourceRequest request, string Id)
        //{
        //    var returnVal = new List<CashAdvanceSettlementDetail>();

        //    returnVal = await client.Get<List<CashAdvanceSettlementDetail>>($"{url}/GetSattlementDetailById/{Id}") ?? new List<CashAdvanceSettlementDetail>();

        //    return Json(returnVal.ToDataSourceResult(request), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //}

        public async Task<IActionResult> ReadDetail2([DataSourceRequest] DataSourceRequest request, string caId)
        {
            var returnVal = new List<CashAdvanceDetail>();
            var pcaTaskList = GlobalCache.GetCashAdvanceList();

            if (pcaTaskList != null && pcaTaskList.Count > 0)
            {
                foreach (CashAdvanceDetail data in pcaTaskList)
                {
                    data.Id = Guid.NewGuid().ToString();
                    data.Amount = data.UnitPrice * data.Qty;
                    data.CashAdvanceId = Guid.NewGuid().ToString();
                    returnVal.Add(data);
                }
                GlobalCache.ClearCashAdvanceList();
            }
            else
            {
                List<CashAdvanceDetail> newListCad = new List<CashAdvanceDetail>();
                returnVal = await client.Get<List<CashAdvanceDetail>>($"{url}/GetDetails/{caId}") ?? new List<CashAdvanceDetail>();
                foreach(var i in returnVal)
                {
                    if (i.Amount == null)
                        i.Amount = i.TotalRequest;

                    newListCad.Add(i);
                }
                returnVal = newListCad;
            }
            return Json(returnVal.ToDataSourceResult(request), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        [AcceptVerbs("Post")]
        public ActionResult EditingInline_Create([DataSourceRequest] DataSourceRequest request, CashAdvanceDetail cad)
        {
            if (cad != null && ModelState.IsValid)
            {
                //productService.Create(cad);
            }

            return Json(new[] { cad }.ToDataSourceResult(request, ModelState));
        }

        public ActionResult EditingInline_Create2([DataSourceRequest] DataSourceRequest request, CashAdvanceDetail cad)
        {
            if (cad != null && ModelState.IsValid)
            {
                //productService.Create(cad);
            }

            return Json(new[] { cad }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs("Post")]
        public ActionResult EditingInline_Update([DataSourceRequest] DataSourceRequest request, CashAdvanceDetail cad)
        {
            if (cad != null && ModelState.IsValid)
            {
                //productService.Update(cad);
            }

            return Json(new[] { cad }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs("Post")]
        public ActionResult EditingInline_Update2([DataSourceRequest] DataSourceRequest request, CashAdvanceDetail cad)
        {
            if (cad != null && ModelState.IsValid)
            {
                //productService.Update(cad);
            }

            return Json(new[] { cad }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs("Post")]
        public ActionResult EditingInline_Destroy([DataSourceRequest] DataSourceRequest request, CashAdvanceDetail cad)
        {
            if (cad != null)
            {
                //productService.Destroy(cad);
            }

            return Json(new[] { cad }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs("Post")]
        public ActionResult EditingInline_Destroy2([DataSourceRequest] DataSourceRequest request, CashAdvanceDetail cad)
        {
            if (cad != null)
            {
                //productService.Destroy(cad);
            }

            return Json(new[] { cad }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs("Post")]
        public ActionResult EditingSettlement_Create([DataSourceRequest] DataSourceRequest request, CashAdvanceSettlementDetail cad, IFormFile settlementFiles)
        {
            if (cad != null && ModelState.IsValid)
            {
                //productService.Create(cad);
            }

            return Json(new[] { cad }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs("Post")]
        public ActionResult EditingSettlement_Update([DataSourceRequest] DataSourceRequest request, CashAdvanceSettlementDetail cad, IFormFile settlementFiles)
        {
            if (cad != null && ModelState.IsValid)
            {
                //productService.Update(cad);
            }

            return Json(new[] { cad }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public async Task<IActionResult> Approve(CashAdvance model)
        {
            //model.Status = StatusTransaction.Process;
            //model.Status = StatusTransaction.Approved;
            var response = await client.PutApiResponse<CashAdvance>($"{url}/Approve", model);
            return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
        }

        //[HttpPost]
        //public async Task<IActionResult> ApproveSattlement(CashAdvanceSattlement model)
        //{
        //    //model.Status = StatusTransaction.Process;
        //    //model.Status = StatusTransaction.Approved;
        //    var response = await client.PutApiResponse<CashAdvanceSattlement>($"{url}/ApproveSattlement", model);
        //    return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
        //}

        //[HttpPost]
        //public async Task<IActionResult> RejectSattlement(CashAdvanceSattlement model)
        //{
        //    CashAdvance ca = new CashAdvance();
        //    var modelca = await client.Get<CashAdvance>($"{url}/{model.CashAdvanceId}");
        //    if (modelca != null)
        //    {
        //        ca = modelca;
        //    }
        //    var response = await client.PutApiResponse<CashAdvance>($"{url}/Reject", ca);
        //    return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
        //}

        [HttpPost]
        public async Task<IActionResult> Reject(CashAdvance model)
        {
            //model.Status = StatusTransaction.Rejected;
            var response = await client.PutApiResponse<CashAdvance>($"{url}/Reject", model);
            return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
        }

        [HttpPost]
        public async Task<IActionResult> Submit(CashAdvance model)
        {
            if (string.IsNullOrEmpty(model.Id))
            {
                model.CaStatusId = CashAdvanceStatus.WaitingApproval;
            }
            else
            {

                if (string.IsNullOrEmpty(model.CaStatusId))
                {
                    model.CaStatusId = CashAdvanceStatus.WaitingApproval;
                }
                else
                {
                    if (model.CaStatusId == CashAdvanceStatus.Open)
                    {
                        model.CaStatusId = CashAdvanceStatus.WaitingApproval;
                    }
                }
            }

            return await Process(model, false);
        }

        [HttpPost]
        public async Task<IActionResult> Save(CashAdvance model)
        {

            if (string.IsNullOrEmpty(model.Id))
            {
                model.CaStatusId = CashAdvanceStatus.Open;
            }
            if (string.IsNullOrEmpty(model.CaStatusId))
            {
                model.CaStatusId = CashAdvanceStatus.Open;
            }
            return await Process(model, true);
        }

        [HttpPost]
        public async Task<IActionResult> CreateAndReadAutomaticJournal(CashAdvanceSattlement header, List<CashAdvanceSettlementDetail> details)
        {

            try
            {
                var response = new ApiResponse();
                var model = new CashAdvanceSattlementDTO();
                model.Header = header;
                model.Details = details;

                if (!string.IsNullOrEmpty(header.Id))
                {
                    response = await client.PutApiResponse<CashAdvanceSattlementDTO>($"{url}/EditSattlement", model);
                }
                else
                {
                    response = await client.PostApiResponse<CashAdvanceSattlementDTO>($"{url}/AddSattlement", model);
                }

                if (response.Success)
                {

                    response = await client.PostApiResponse<CashAdvanceSattlement>($"{url}/PostDataSattlement", model.Header);

                    return Json(new { success = response.Success, message = response.Message });
                }
                else
                {
                    return Json(new { success = false, message = response.Message });
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message.ToString() });
            }
        }

        //[HttpPost]
        //public async Task<IActionResult> SaveSettlement(CashAdvanceSattlement header, List<CashAdvanceSettlementDetail> details)
        //{
        //    //model.Status = StatusTransaction.;
        //    if (string.IsNullOrEmpty(header.CaStatusId))
        //    {
        //        header.CaStatusId = CashAdvanceStatus.Approved;
        //    }
        //    if (string.IsNullOrEmpty(header.Id))
        //    {
        //        header.CaStatusId = CashAdvanceStatus.Approved;
        //    }
        //    return await ProcessSattlement(header, details);
        //}

        [HttpPost]
        public async Task<IActionResult> SubmitSettlement(CashAdvanceSattlement header, List<CashAdvanceSettlementDetail> details)
        {
            //model.Status = StatusTransaction.;
            if (string.IsNullOrEmpty(header.CaStatusId))
            {
                header.CaStatusId = CashAdvanceStatus.SattlementCheck;
            }
            else if (header.CaStatusId == CashAdvanceStatus.Approved)
            {
                header.CaStatusId = CashAdvanceStatus.SattlementCheck;
            }



            return await ProcessSattlement(header, details);
        }

        private async Task<IActionResult> ProcessSattlement(CashAdvanceSattlement header, List<CashAdvanceSettlementDetail> details)
        {
            try
            {
                var response = new ApiResponse();
                CashAdvanceSattlementDTO model = new CashAdvanceSattlementDTO();
                model.Header = header;
                List<CashAdvanceSettlementDetail> dtl = new List<CashAdvanceSettlementDetail>();
                dtl = details;
                model.Details = details;
                if (!string.IsNullOrEmpty(model.Header.Id))
                {
                    response = await client.PutApiResponse<CashAdvanceSattlementDTO>($"{url}/EditSattlement", model);
                }
                else
                {
                    response = await client.PostApiResponse<CashAdvanceSattlementDTO>($"{url}/AddSattlement", model);
                }

                return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });

            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message.ToString() });
            }

        }


        //public async Task<ActionResult> SaveFileAsync(IEnumerable<IFormFile> settlementFiles)
        //{
        //    // The Name of the Upload component is "files"
        //    if (settlementFiles != null)
        //    {
        //        foreach (var file in settlementFiles)
        //        {
        //            var fileContent = ContentDispositionHeaderValue.Parse(file.ContentDisposition);

        //            // Some browsers send file names with full path.
        //            // We are only interested in the file name.
        //            var fileName = Path.GetFileName(fileContent.FileName.ToString().Trim('"'));
        //            var physicalPath = Path.Combine(HostingEnvironment.WebRootPath, "App_Data", fileName);

        //            // The files are not actually saved in this demo
        //            //using (var fileStream = new FileStream(physicalPath, FileMode.Create))
        //            //{
        //            //    await file.CopyToAsync(fileStream);
        //            //}
        //        }
        //    }

        //    // Return an empty string to signify success
        //    return Content("");
        //}

        ////private string ProcessUpload(IFormFile settlementFile)
        ////{
        ////	string warningLetter = "";
        ////	FileService UploadFile = new FileService(context);
        ////	var file = UploadFile.Upload(fileWarningLetter, null);
        ////	if (!string.IsNullOrEmpty(file))
        ////	{
        ////		warningLetter = file;
        ////	}

        ////	return warningLetter;
        ////}

        //public ActionResult RemoveFile(string[] fileNames)
        //{
        //    // The parameter of the Remove action must be called "fileNames"

        //    if (fileNames != null)
        //    {
        //        foreach (var fullName in fileNames)
        //        {
        //            var fileName = Path.GetFileName(fullName);
        //            var physicalPath = Path.Combine(HostingEnvironment.WebRootPath, "App_Data", fileName);

        //            // TODO: Verify user permissions

        //            if (System.IO.File.Exists(physicalPath))
        //            {
        //                // The files are not actually removed in this demo
        //                // System.IO.File.Delete(physicalPath);
        //            }
        //        }
        //    }

        //    // Return an empty string to signify success
        //    return Content("");
        //}


        private async Task<IActionResult> Process(CashAdvance model, bool isDraft)
        {
            try
            {
                var response = new ApiResponse();
                if (!string.IsNullOrEmpty(model.Id))
                {
                    response = await client.PutApiResponse<CashAdvance>(url, model);
                }
                else
                {
                    response = await client.PostApiResponse<CashAdvance>(url, model);
                }
                return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
                //if (isDraft)
                //{
                //    return Json(new { success = response.Success, message = response.Message, data = response.Data as CashAdvance });
                //}
                //else
                //{
                //    return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
                //}
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message.ToString(), url = Url.Action(nameof(Index)) });
            }

        }

        public async Task<IActionResult> GetVendorData(string vendorid)
        {
            var model = new CashAdvance();
            if (!string.IsNullOrEmpty(vendorid))
                model = await client.Get<CashAdvance>($"{url}/{"GetCompanyInfo"}/{vendorid}") ?? new CashAdvance();

            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });

        }
    }
}