﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodeMarvel.Infrastructure.ModelShared;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Phoenix.WebExtension.RestApi;
using PhoenixERPWebApp.Models;
using PhoenixERPWebApp.Models.Finance.MasterData;

namespace PhoenixERPWebApp.Areas.Finance.Controllers
{
    [Area(nameof(Finance))]
    public class MasterPettyCashController : Controller
    {
        string url = ApiUrl.MasterPettyCashUrl;
        private readonly ApiClientFactory client;

        public MasterPettyCashController(ApiClientFactory client)
        {
            this.client = client;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> AddEdit(string Id)
        {
            var model = new MasterPettyCash();
            ViewBag.Header = "Create";

            if (!string.IsNullOrEmpty(Id))
            {
                ViewBag.Header = "Update";
                model = await client.Get<MasterPettyCash>($"{url}/{Id}");
            }

            return View(model);
        }

        public async Task<IActionResult> Read([DataSourceRequest] DataSourceRequest request)
        {
            var model = await client.Get<List<MasterPettyCash>>(url) ?? new List<MasterPettyCash>();
            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> IsExistCoaIdMasterPetty(string coaid)
        {
            string urlc = $"{url}/IsExistCoaIdMasterPetty/{coaid}";
            var model = await client.Get<bool>(urlc);
            return Json(model);
        }

        [HttpPost]
        public async Task<IActionResult> Submit(MasterPettyCash model)
        {
            var response = new Phoenix.WebExtension.RestApi.ApiResponse();
            if (!string.IsNullOrEmpty(model.Id))
            {
                response = await client.PutApiResponse<MasterPettyCash>(url, model);
            }
            else
            {
                response = await client.PostApiResponse<MasterPettyCash>(url, model);
            }
            return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(MasterPettyCash model) => Json(await client.DeleteApiResponse<MasterPettyCash>($"{url}/{model.Id}"));
    }
}