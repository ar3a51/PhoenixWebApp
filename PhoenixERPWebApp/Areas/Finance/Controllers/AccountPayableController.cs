﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CodeMarvel.Infrastructure;
using CodeMarvel.Infrastructure.Sessions;
using CodeMarvel.Infrastructure.WebNetCore;
using Microsoft.Extensions.Configuration;
using CodeMarvel.Infrastructure.Options;
using Microsoft.Extensions.Options;
using Phoenix.WebExtension.RestApi;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Phoenix.WebExtension.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using PhoenixERPWebApp.Models;
using PhoenixERPWebApp.Models.Enum;

namespace PhoenixERPWebApp.Areas.Finance.Controllers
{
    [Area("Finance")]
    public class AccountPayableController : BaseController
    {
        string url = ApiUrl.InvoiceReceivable;
        private readonly ApiClientFactory client;

        public AccountPayableController(ApiClientFactory client)
        {
            this.client = client;
        }

        public ActionResult Index()
        {
            var model = new AccountPayable() { Status = InvoiceReceivedStatus.Posted };
            return View(model);
        }

        public async Task<IActionResult> Read([DataSourceRequest] DataSourceRequest request, string status, string businessUnit, string legalEntity, string accountPayableNumber)
        {
            var model = await client.Get<List<AccountPayable>>($"{url}/GetAPList/{(status ?? "0")}/{(businessUnit ?? "0")}/{(legalEntity ?? "0")}/{(accountPayableNumber ?? "0")}") ?? new List<AccountPayable>();
            return Json(model.ToDataSourceResult(request), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        //// GET: AccountPayable/Details/5
        ////public ActionResult Detail(int id)
        ////{
        ////    return View();
        ////}

        //public async Task<IActionResult> Detail(string Id)
        //{
        //    var model = new AccountPayable();
        //    ViewBag.Header = "Create";

        //    if (!string.IsNullOrEmpty(Id))
        //    {
        //        ViewBag.Header = "Update";
        //        model = await client.Get<AccountPayable>($"{url}/{Id}");
        //    }

        //    return View(model);
        //}
        //public async Task<IActionResult> Read([DataSourceRequest] DataSourceRequest request)
        //{
        //    var model = await client.Get<List<AccountPayable>>($"{url}/getJoin") ?? new List<AccountPayable>();
        //    //var model = new List<AccountPayable>();
        //    var data = model.ToDataSourceResult(request);
        //    return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //}

        //public async Task<IActionResult> ReadCustom([DataSourceRequest] DataSourceRequest request, string status, string businessUnit, string legalEntity, string ApNumber)
        //{
        //    if (string.IsNullOrEmpty(status))
        //    {
        //        status = "0";
        //    }
        //    if (string.IsNullOrEmpty(businessUnit))
        //    {
        //        businessUnit = "0";
        //    }
        //    if (string.IsNullOrEmpty(legalEntity))
        //    {
        //        legalEntity = "0";
        //    }
        //    if (string.IsNullOrEmpty(ApNumber))
        //    {
        //        ApNumber = "0";
        //    }
        //    var model = await client.Get<List<AccountPayable>>($"{url}/GetJoinCustom/{status}/{businessUnit}/{legalEntity}/{ApNumber}") ?? new List<AccountPayable>();
        //    //var model = new List<AccountPayable>();
        //    var data = model.ToDataSourceResult(request);
        //    return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //}

        //public async Task<IActionResult> ReadDetail([DataSourceRequest] DataSourceRequest request,string referenceId=null)
        //{
        //    //var model = await client.Get<List<AccountPayable>>(url) ?? new List<AccountPayable>();
        //    var model = new List<AccountPayableDetailList>();
        //    if (string.IsNullOrEmpty(referenceId)){
        //        model.Add(new AccountPayableDetailList()
        //        {
        //            Id = "001",
        //            Lte = "Others",
        //            TaskDetail = "Free Project Office Mei - June",
        //            AmountPaid = 6185567.00m
        //        });
        //    }
        //    else {

        //        model.AddRange( new List<AccountPayableDetailList>() {
        //        new AccountPayableDetailList()
        //        {
        //            Id = "001",
        //            Lte = "Others",
        //            TaskDetail = "Free Project Office Mei - June",
        //            AmountPaid = 6185567.00m
        //        },
        //        new AccountPayableDetailList()
        //        {
        //            Id = "002",
        //            Lte = "Item",
        //            TaskDetail = "LCD Screen",
        //            AmountPaid = 20000000
        //        }});


        //    }

        //    var data = model.ToDataSourceResult(request);
        //    return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //}
        //public async Task<IActionResult> ReadJournal([DataSourceRequest] DataSourceRequest request)
        //{
        //    //var model = await client.Get<List<AccountPayable>>(url) ?? new List<AccountPayable>();
        //    var model = new List<AccountPayableJournalList>();
        //    model.Add(new AccountPayableJournalList()
        //    {
        //        Id = "001",
        //        AccountCode = "10420",
        //        AccountName = "Work In Progress - Production",
        //        Description = "Free Project Officer Mei - June",
        //        JobId = "J02018010101001",
        //        Department = "IRIS A",
        //        Debit = 6000000m
        //    });
        //    var data = model.ToDataSourceResult(request);
        //    return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //}
        //public async Task<JsonResult> DepartmentLookUp([DataSourceRequest] DataSourceRequest request)
        //{
        //    //var model = await client.Get<List<AccountPayable>>(url) ?? new List<AccountPayable>();
        //    var model = new List<object> {
        //        new {
        //            Id = 1,
        //            UnitName = "IRIS A"
        //        },
        //        new
        //        {
        //            Id = 1,
        //            UnitName = "IRIS B"
        //        },
        //        new
        //        {
        //            Id = 1,
        //            UnitName = "OPTIMA MEDIA"
        //        },
        //        new
        //        {
        //            Id = 1,
        //            UnitName = "CREATIVE INDOSAT"
        //        }
        //    };
        //    var data = model.ToDataSourceResult(request);
        //    return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //}

        //public async Task<ActionResult> GetRefNumber()
        //{
        //    var getUrl = $"{ApiUrl.dropdownUrl}/TAPDDL";
        //    var model = await client.Get<List<SelectListItem>>(getUrl) ?? new List<SelectListItem>();
        //    return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //}

        //public async Task<IActionResult> GetAPData(string referenceId)
        //{
        //    var model = await client.Get<TemporaryAccountPayable>($"{url}/getAccountPayable/{referenceId}");
        //    return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //}

        //[HttpPost]
        //public async Task<IActionResult> Post(AccountPayable data)
        //{
        //    var model = await client.PostApiResponse<AccountPayable>($"{url}", data);
        //    return Json(new { success = model.Success, message = model.Message, url = Url.Action(nameof(Index)) });
        //    //return Json(model, url = Url.Action(nameof(Index)), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //}
    }
}