﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodeMarvel.Infrastructure.WebNetCore;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Phoenix.WebExtension.RestApi;
using PhoenixERPWebApp.Models;
using PhoenixERPWebApp.Models.Finance.FinanceReport;

namespace PhoenixERPWebApp.Areas.Finance.Controllers
{
    [Area("Finance")]
    public class TimeSheetGridReportController : BaseController
    {
        string url = ApiUrl.TimeSheetByJobUrl;
        private readonly ApiClientFactory client;

        public TimeSheetGridReportController(ApiClientFactory client)
        {
            this.client = client;
        }

        public async Task<IActionResult> Index()
        {
            return View();
        }

        public async Task<IActionResult> ReportByClient()
        {
            return View();
        }
        public async Task<IActionResult> ReadTimeSheetByJob([DataSourceRequest] DataSourceRequest request, string jobId)
        {
            //string urls = url + "/get_by_job?jobId="+jobId;
            var model = await client.Get<List<TimeSheetByJob>>($"{url}/get_by_job?job_id={(jobId ?? "0")}");
            if (model == null)
            {
                model = new List<TimeSheetByJob>();
            }
            return Json(model.ToDataSourceResult(request), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> ReadTimeSheetByClientId([DataSourceRequest] DataSourceRequest request, string ClientId)
        {
            //string urls = url + "/get_by_job?jobId=" + jobId;
            var model = await client.Get<List<ReportTimeSheetClient>>($"{url}/get_by_client?client_id={(ClientId ?? "0")}");
            if (model == null)
            {
                model = new List<ReportTimeSheetClient>();
            }
            return Json(model.ToDataSourceResult(request), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        [HttpPost]
        public ActionResult Excel_Export_Save(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents, contentType, fileName);
        }
    }
}