﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodeMarvel.Infrastructure.ModelShared;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Phoenix.WebExtension.RestApi;
using PhoenixERPWebApp.Models;
using System.IO;
using Microsoft.AspNetCore.Http;
using System.Data.OleDb;
using System.Data;
//using ExcelDataReader = Excel;
using Stimulsoft.System.Data;

namespace PhoenixERPWebApp.Areas.Finance.Controllers
{
    [Area(nameof(Finance))]
    public class MasterBudgetOpexController : Controller
    {
        string url = ApiUrl.MasterBudgetOpexUrl;
        private readonly ApiClientFactory client;

        public MasterBudgetOpexController(ApiClientFactory client)
        {
            this.client = client;
        }
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> AddEdit(string Id)
        {
            var model = new BudgetMaster();
            ViewBag.Header = "Create";

            if (!string.IsNullOrEmpty(Id))
            {
                ViewBag.Header = "Update";
                model = await client.Get<BudgetMaster>($"{url}/{Id}");
            }
            else
            {
                model.CreatedOn = DateTime.Now;
            }

            return View(model);
        }

        public IActionResult GetBudgetYear()
        {
            var dateNow = DateTime.Now;
            int year = dateNow.Year;
            var result = new List<SelectListItem>()
            {
                new SelectListItem() { Text = year.ToString(), Value = year.ToString() },
                new SelectListItem() { Text = (year+1).ToString(), Value = (year+1).ToString() },
                new SelectListItem() { Text = (year+2).ToString(), Value = (year+2).ToString()}
            };
            return Json(result, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        //public  IActionResult UploadFile(IFormFile fileupload)
        //{
        //    DataSet ds = new DataSet();
        //    var extension = Path.GetExtension(fileupload.FileName);

        //    if (fileupload.Length > 0)
        //    {
        //        using (var ms = new MemoryStream())
        //        {
        //            fileupload.CopyToAsync(ms);
        //            ds = ConvertFileToDataTable(ms, extension);
        //        }
        //    }
        //    var model3 = (from a in ds.Tables[0].AsEnumerable() select a).ToList();

        //    return Json(new { success = true, message = "Succes Import excel data", refreshGrid = "gridjournal", data = new BudgetMaster() as BudgetMaster });
        //}

        //public DataSet ConvertFileToDataTable(MemoryStream stream, string ext)
        //{
        //    ExcelDataReader.IExcelDataReader excelReader2007 = null;
        //    excelReader2007 = ext.EndsWith(".xls")
        //        ? ExcelDataReader.ExcelReaderFactory.CreateBinaryReader(stream)
        //        : ExcelDataReader.ExcelReaderFactory.CreateOpenXmlReader(stream);
        //    excelReader2007.IsFirstRowAsColumnNames = true;
        //    DataSet result = excelReader2007.AsDataSet();
        //    excelReader2007.Close();
        //    stream.Close();
        //    stream.Dispose();
        //    return result;
        //}


        //[ActionName("Importexcel")]
        //[HttpPost]
        //public ActionResult Importexcel1(IFormFile filePoDoc)
        //{
        //    if (filePoDoc.Length > 0)
        //    {
        //        string extension = System.IO.Path.GetExtension(filePoDoc.FileName).ToLower();
        //        string connString = "";

        //        string[] validFileTypes = { ".xls", ".xlsx", ".csv" };

        //        string path1 = string.Format("{0}/{1}", Server.MapPath("~/Content/Uploads"), filePoDoc.FileName);
        //        if (!Directory.Exists(path1))
        //        {
        //            Directory.CreateDirectory(Server.MapPath("~/Content/Uploads"));
        //        }
        //        if (validFileTypes.Contains(extension))
        //        {
        //            if (System.IO.File.Exists(path1))
        //            { System.IO.File.Delete(path1); }
        //            Request.Files["FileUpload1"].SaveAs(path1);
        //            if (extension == ".csv")
        //            {
        //                DataTable dt = Utility.ConvertCSVtoDataTable(path1);
        //                ViewBag.Data = dt;
        //            }
        //            //Connection String to Excel Workbook  
        //            else if (extension.Trim() == ".xls")
        //            {
        //                connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path1 + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
        //                DataTable dt = Utility.ConvertXSLXtoDataTable(path1, connString);
        //                ViewBag.Data = dt;
        //            }
        //            else if (extension.Trim() == ".xlsx")
        //            {
        //                connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path1 + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
        //                DataTable dt = Utility.ConvertXSLXtoDataTable(path1, connString);
        //                ViewBag.Data = dt;
        //            }

        //        }
        //        else
        //        {
        //            ViewBag.Error = "Please Upload Files in .xls, .xlsx or .csv format";

        //        }

        //    }

        //    return View();
        //}


    }
}