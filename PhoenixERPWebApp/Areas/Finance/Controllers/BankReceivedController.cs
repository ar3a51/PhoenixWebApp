﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Phoenix.WebExtension.Attributes;
using Phoenix.WebExtension.RestApi;
using PhoenixERPWebApp.Models.Finance.Transaction.PaymentRequest;
using System.Xml;
using PhoenixERPWebApp.Models;
using PhoenixERPWebApp.Models.Enum;
using CodeMarvel.Infrastructure.WebNetCore;
using PhoenixERPWebApp.Models.Finance.Transaction.BankReceived;
using PhoenixERPWebApp.Models.Finance;
using PhoenixERPWebApp.Models.Finance.Transaction.BankPayment;
using PhoenixERPWebApp.Models.Finance.Transaction;
using PhoenixERPWebApp.Models.Finance.MasterData;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace PhoenixERPWebApp.Areas.Finance.Controllers
{
    [Area("Finance")]
    public class BankReceivedController : Controller
    {
        string url = ApiUrl.BankReceivedUrl;
        private readonly ApiClientFactory client;

        public BankReceivedController(ApiClientFactory client)
        {
            this.client = client;
        }
        public IActionResult Index()
        {
            return View();
        }

        //public async Task<IActionResult> Detail(string Id, bool isApprove)
        //{
        //    var model = new BankReceivedList();
        //    if (isApprove.Equals(null))
        //    {
        //        isApprove = false;
        //    }
        //    ViewBag.RecordID = Id;
        //    var isCurrentApproval = false;
        //    if (!string.IsNullOrEmpty(Id))
        //    {
        //        if (isApprove == true)
        //            ViewBag.Header = "Update Bank Received";
        //        else
        //            ViewBag.Header = "Update Bank Received";
        //        model = await client.Get<BankReceivedList>($"{url}/{Id}");
        //        //jika sumber AP
        //        if (!string.IsNullOrEmpty(model.SourceDoc))
        //        {
        //            var Armodel = await client.Get<AccountReceivable>($"{ApiUrl.ARClientUrl}/{model.ReferenceNumberId}");
        //            var invclient = await client.Get<InvoiceClient>($"{ApiUrl.InvoiceClientUrl}/{Armodel.Id}");
        //            if (Armodel != null && invclient != null)
        //            {
        //                model.Inv_AccountReceivableNumber = Armodel.AccountReceivableNumber;
        //                model.Inv_AdvertisingTax = 0;
        //                model.Inv_AllocationRatio = 0;
        //                model.Inv_BusinessUnitId = Armodel.BusinessUnitId;
        //                model.Inv_CurrencyId = Armodel.CurrencyId;
        //                model.Inv_DueDate = Armodel.DueDate;
        //                model.Inv_ExhangeRate = Armodel.ExchangeRate;
        //                model.Inv_NetAmount = Armodel.Amount;
        //                model.Inv_OtherDiscount = 0;
        //                model.Inv_PromptPayment = 0;
        //                model.Inv_PromptPayment2 = 0;
        //                model.Inv_TotalCostVendor = Armodel.Amount + invclient.Vat;
        //                model.Inv_ValueAddedTax = invclient.Vat;
        //                model.Inv_ValueAddedTaxOn = 0;
        //                model.Inv_ValueAddedTaxPercent = 0;
        //                model.Inv_VendorId = invclient.ClientId;
        //                model.Inv_VendorName = invclient.ClientName;
        //                model.Inv_VendorNameSpan = invclient.ClientName;
        //                model.Inv_ExhangeRate = invclient.ExchangeRate;
        //                model.Inv_InvoiceReceivableDate = invclient.InvoiceDate;
        //                model.Inv_InvoiceReceivableNumber = invclient.Id;
        //                model.Inv_LegalEntityId = Armodel.LegalEntityId;
        //                model.Inv_ValueAddedTaxPercent = 10;


        //                model.Amount = model.Inv_TotalCostVendor;
        //                model.TotalAmount = Armodel.Amount + invclient.Vat;
        //                model.BusinessUnitId = Armodel.BusinessUnitId;
        //                model.SourceDoc = "AR";
        //                model.LegalEntityId = Armodel.LegalEntityId;
        //                model.VatPercent = 10;
        //                model.Vat = model.Inv_ValueAddedTax;

        //                var modeljob = await client.Get<JobDetail>($"{ApiUrl.JobDetail}/{Armodel.JobId}");
        //                if (modeljob != null)
        //                {
        //                    model.Inv_MainServiceCategory = modeljob.MainServiceCategory;
        //                    model.MainServiceCategoryId = modeljob.MainServiceCategory;
        //                }

        //                if (!string.IsNullOrEmpty(model.Inv_MainServiceCategory))
        //                {
        //                    if (model.Inv_MainServiceCategory == "1105194186501656222" || model.Inv_MainServiceCategory == "2225194186501656576")
        //                    {
        //                        //model.ReceivedForm = "Client";
        //                        model.PaytoName = invclient.ClientId;
        //                    }
        //                    else
        //                    {
        //                        //model.ReceivedForm = "GL";
        //                        model.PaytoName = "Internal";
        //                    }
        //                }
        //            }
        //        }//sumber CA
        //        else
        //        {
        //            model.SourceDoc = "CAS";
        //            var modelca = await client.Get<CashAdvance>($"{ApiUrl.CashAdvanceUrl}/GetByCaNumber/{model.ReferenceNumberId}");

        //            if (modelca != null)
        //            {
        //                model.cashAdvanceList = modelca;
        //                model.Ca_JobId = modelca.JobId;
        //                model.Ca_CaPeriodFrom = modelca.CaPeriodFrom;
        //                model.Ca_CaPeriodTo = modelca.CaPeriodTo;
        //                model.Ca_CashAdvanceId = modelca.Id;
        //                model.Ca_ClientName = modelca.ClientName;
        //                model.Ca_JobNumber = modelca.JobNumber;
        //                model.Ca_LegalEntityId = modelca.LegalEntityId;
        //                model.Ca_PayeeAccountName = modelca.PayeeAccountName;
        //                model.Ca_PayeeAccountNumber = modelca.PayeeAccountNumber;
        //                model.Ca_PayeeBankBranch = modelca.PayeeBankBranch;
        //                model.Ca_PayeeBankId = modelca.PayeeBankId;
        //                model.Ca_ProjectActivity = modelca.ProjectActivity;
        //                model.Ca_VendorName = modelca.VendorName;
        //                model.MainServiceCategoryId = modelca.ProjectActivity;

        //                if (!string.IsNullOrEmpty(model.MainServiceCategoryId))
        //                {
        //                    if (model.MainServiceCategoryId == "1105194186501656222" || model.MainServiceCategoryId == "2225194186501656576")
        //                    {
        //                        //model.ReceivedForm = "Client";
        //                        model.PaytoName = modelca.ClientName;
        //                    }
        //                    else
        //                    {
        //                        //model.ReceivedForm = "GL";
        //                        model.PaytoName = "Internal";
        //                    }
        //                }

        //            }
        //        }
        //        isCurrentApproval = await client.Get<bool>($"{ApiUrl.ManageTransApproval}/IsCurrentApproval/{Id}");
        //        ViewBag.isRequestor = isApprove == false && (model.Status == PaymentRequestStatus.Open);
        //    }
        //    else
        //    {
        //        var requestor = await client.Get<InfoEmployee>($"{ApiUrl.EmployeeInfoUrl}/getByUserLogin");

        //        ViewBag.Header = "Create Bank Received";
        //        //model.ReceivedDate = DateTime.Now;
        //        if (!string.IsNullOrEmpty(requestor.BusinessUnitId))
        //        {
        //            model.BusinessUnitId = requestor.BusinessUnitId;
        //            ViewBag.isRequestor = isApprove == false;
        //        }
        //        model.Vat = 0;
        //        model.TaxPayable = 0;
        //        model.ExchangeRate = 1;

        //    }
        //    ViewBag.processApprove = isApprove && isCurrentApproval;//(model.Status == StatusTransaction.WaitingApproval || model.Status == StatusTransaction.CurrentApproval);
        //    model.isApprove = isApprove;
        //    if (isApprove)
        //    {
        //        ViewBag.isHcAdmin = true;//belum di set
        //    }
        //    return View(model);
        //}

        public async Task<IActionResult> Detail(string Id, bool isApprove)
        {

            var model = new BankReceivedList();
            if (isApprove.Equals(null))
            {
                isApprove = false;
            }
            ViewBag.RecordID = Id;
            var isCurrentApproval = false;
            if (!string.IsNullOrEmpty(Id))
            {
                if (isApprove == true)
                    ViewBag.Header = "Approve Bank Received";
                else
                    ViewBag.Header = "Update Bank Received";


                model = await client.Get<BankReceivedList>($"{url}/{Id}");
                //jika sumber AP
                if (model.SourceDoc == "AR")
                {
                    //multiple ap
                    if (!string.IsNullOrEmpty(model.Id))
                    {
                        var detail = await client.Get<List<BankReceivedDetail>>($"{url}/GetDetail/{model.Id}");
                        if (detail != null)
                        {
                            decimal? subtotal = 0;
                            foreach (BankReceivedDetail data in detail)
                            {
                                subtotal += data.Amount;
                            }
                            model.pd_NetAmount = subtotal;
                            model.pd_VatPercent = 10;
                            model.pd_Vat = model.pd_NetAmount * (model.pd_VatPercent / 100);
                            model.pd_GrandTotal = model.pd_NetAmount + model.pd_Vat;
                        }
                    }

                }
                isCurrentApproval = await client.Get<bool>($"{ApiUrl.ManageTransApproval}/IsCurrentApproval/{Id}");
                ViewBag.isRequestor = isApprove == false && (model.Status == PaymentRequestStatus.Open);
            }
            else
            {
                var requestor = await client.Get<InfoEmployee>($"{ApiUrl.EmployeeInfoUrl}/getByUserLogin");
                model.SourceDoc = "AR";
                ViewBag.Header = "Create Bank Received";
                model.PaymentRequestDate = DateTime.Now;
                if (!string.IsNullOrEmpty(requestor.BusinessUnitId))
                {
                    model.BusinessUnitId = requestor.BusinessUnitId;
                    ViewBag.isRequestor = isApprove == false;
                }

            }
            ViewBag.processApprove = isApprove && isCurrentApproval;//(model.Status == StatusTransaction.WaitingApproval || model.Status == StatusTransaction.CurrentApproval);
            model.isApprove = isApprove;
            if (isApprove)
            {
                ViewBag.isHcAdmin = true;//belum di set
            }
            return View(model);
        }

        static T CastResponseData<T>(object entity) where T : class
        {
            return entity as T;
        }

        public async Task<IActionResult> Read([DataSourceRequest] DataSourceRequest request, PaymentRequestFilter Filter)
        {
            try
            {
                var response = new ApiResponse();
                List<BankReceivedList> model = new List<BankReceivedList>();
                Filter.isBankPayment = false;
                response = await client.PostApiResponse<List<BankReceivedList>>($"{url}/GetWithFilter/", Filter);

                if (response.Data != null)
                {
                    model = CastResponseData<List<BankReceivedList>>(response.Data);
                }

                var data = model.ToDataSourceResult(request);
                return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        
        public async Task<IActionResult> ReadARPosted([DataSourceRequest] DataSourceRequest request, PaymentRequestFilter Filter)
        {
            try
            {
                var response = new ApiResponse();
                List<AccountReceivable> model = new List<AccountReceivable>();
                response = await client.PostApiResponse<List<AccountReceivable>>($"{url}/GetListARPosted/", Filter);

                if (response.Data != null)
                {
                    model = CastResponseData<List<AccountReceivable>>(response.Data);
                }

                var data = model.ToDataSourceResult(request);
                return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
        
        public async Task<IActionResult> ReadDetailPaymentRequest([DataSourceRequest] DataSourceRequest request, string Id)
        {
            try
            {

                var response = new ApiResponse();
                var model = await client.Get<List<AccountReceivable>>($"{url}/GetListDetailPaymentRequest/{Id}") ?? new List<AccountReceivable>();

                var data = model.ToDataSourceResult(request);
                return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }


        }

        [HttpGet]
        public async Task<IActionResult> GetAccountNumberData(string Id)
        {
            var model = await client.Get<Banks>($"{url}/GetAccountNumberData/{Id}");
            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        [HttpGet]
        public async Task<IActionResult> GetPettycashData(string Id)
        {
            var model = await client.Get<MasterPettyCash>($"{url}/GetPettycashData/{Id}");
            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        [HttpGet]
        public async Task<IActionResult> GetVendorData(string Id)
        {
            var model = await client.Get<Banks>($"{url}/GetVendorData/{Id}");
            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }
        
        public async Task<IActionResult> ReadPaymentData([DataSourceRequest] DataSourceRequest request, string referenceId)
        {
            var model = await client.Get<List<PaymentInformationData>>($"{url}/GetPaymentInformationData/{referenceId}") ?? new List<PaymentInformationData>();
            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> ReadJournalBankPayment([DataSourceRequest] DataSourceRequest request, string BankPaymentId)
        {
            var model = await client.Get<List<Journal>>($"{url}/GetBankPaymentJournal/{BankPaymentId}") ?? new List<Journal>();
            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        [HttpPost]
        public async Task<IActionResult> Save(BankReceivedList model, List<AccountReceivable> Details)
        {
            var response = new ApiResponse();
            List<BankReceivedDetail> modeldetail = new List<BankReceivedDetail>();

            if (Details.Count > 0)
            {
                model.JobId = Details[0].JobId;
                model.ReferenceNumberId = Details[0].AccountReceivableNumber;
                foreach (AccountReceivable data in Details)
                {
                    BankReceivedDetail pd = new BankReceivedDetail();
                    pd.AccountReceivebleNumber = data.AccountReceivableNumber;
                    pd.LegalEntityId = data.LegalEntityId;
                    pd.BusinessUnitId = data.BusinessUnitId;
                    pd.AffiliationId = data.AffilitionId;
                    pd.AccountId = data.AccountId;
                    pd.BrandId = data.BrandId;
                    pd.JobId = data.JobId;
                    pd.TransactionDate = data.TransactionDate;
                    pd.DueDate = data.DueDate;
                    pd.CurrencyId = data.CurrencyId;
                    pd.Amount = data.Amount;
                    pd.ExchangeRate = data.ExchangeRate;
                    pd.BankAccountDestination = data.BankAccountDestination;
                    pd.Charges = data.Charges;
                    pd.ClientId = data.ClientId;
                    pd.SourceDoc = "AR";
                    modeldetail.Add(pd);
                }
            }
            else
            {
                model.ReferenceNumberId = "";
                model.JobId = "";
                model.AffilitionId = "";
            }


            if (!string.IsNullOrEmpty(model.Id))
            {
                if (string.IsNullOrEmpty(model.Id))
                {
                    model.Status = PaymentRequestStatus.Open;
                }
                if (string.IsNullOrEmpty(model.Status))
                {
                    model.Status = PaymentRequestStatus.Open;
                }

                if (model.PayToType == "3")
                { model.SourceDoc = "AR"; }
                else { model.SourceDoc = "GL"; }
                return await Process(model, modeldetail);

            }
            else
            {

                if (string.IsNullOrEmpty(model.Id))
                {
                    model.Status = PaymentRequestStatus.Open;
                }
                if (string.IsNullOrEmpty(model.Status))
                {
                    model.Status = PaymentRequestStatus.Open;
                }
                return await Process(model, modeldetail);

            }
        }

        [HttpPut("ApproveBR")]
        public async Task<IActionResult> ApproveBR(BankReceivedList model)
        {
            var response = await client.PutApiResponse<BankReceivedList>($"{url}/ApproveData", model);
            return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
        }

        [HttpPut("Reconsiliation")]
        public async Task<IActionResult> Reconsiliation(BankReceivedList model)
        {
            var response = await client.PutApiResponse<BankReceivedList>($"{url}/Reconsiliation", model);
            return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
        }


        [HttpPut("RejectBR")]
        public async Task<IActionResult> RejectBR(BankReceivedList model)
        {

            var response = await client.PutApiResponse<BankReceivedList>($"{url}/Reject", model);
            return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
        }

        [HttpPost]
        public async Task<IActionResult> CreateAndReadAutomaticJournal(BankReceivedList model, List<AccountReceivable> Details)
        {

            try
            {
                var response = new ApiResponse();
                List<BankReceivedDetail> modeldetail = new List<BankReceivedDetail>();
                if (model.PayToType == "3")
                { model.SourceDoc = "AR"; }
                else { model.SourceDoc = "GL"; }
                if (Details.Count > 0)
                {
                    model.JobId = Details[0].JobId;
                    model.ReferenceNumberId = Details[0].AccountReceivableNumber;
                    foreach (AccountReceivable data in Details)
                    {
                        BankReceivedDetail pd = new BankReceivedDetail();
                        pd.AccountReceivebleNumber = data.AccountReceivableNumber;
                        pd.LegalEntityId = data.LegalEntityId;
                        pd.BusinessUnitId = data.BusinessUnitId;
                        pd.AffiliationId = data.AffilitionId;
                        pd.AccountId = data.AccountId;
                        pd.BrandId = data.BrandId;
                        pd.JobId = data.JobId;
                        pd.TransactionDate = data.TransactionDate;
                        pd.DueDate = data.DueDate;
                        pd.CurrencyId = data.CurrencyId;
                        pd.Amount = data.Amount;
                        pd.ExchangeRate = data.ExchangeRate;
                        pd.BankAccountDestination = data.BankAccountDestination;
                        pd.Charges = data.Charges;
                        pd.ClientId = data.ClientId;
                        pd.SourceDoc = "AR";
                        modeldetail.Add(pd);
                    }
                }
                else
                {
                    model.ReferenceNumberId = "";
                    model.JobId = "";
                    model.AffilitionId = "";
                }


                BankReceivedDTO pay = new BankReceivedDTO();
                pay.Header = model;
                pay.Details = modeldetail;

                if (!string.IsNullOrEmpty(model.Id))
                {
                    if (model.isAdvancePayment == true)
                    {
                        model.Status = PaymentRequestStatus.Open;
                        response = await client.PostApiResponse<BankReceivedDTO>($"{url}/AddWithDetail", pay);
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(model.Status))
                        {
                            pay.Header.Status = PaymentRequestStatus.Open;
                        }
                        response = await client.PutApiResponse<BankReceivedDTO>($"{url}/EditWithDetail", pay);
                    }
                }
                else
                {
                    pay.Header.Status = PaymentRequestStatus.Open;
                    response = await client.PostApiResponse<BankReceivedDTO>($"{url}/AddWithDetail", pay);
                }
                

                if (response.Success)
                {
                    var responseJournal = new ApiResponse();
                    responseJournal = await client.PostApiResponse<BankReceivedList>($"{url}/PostDatatoSp", ((BankReceivedDTO)response.Data).Header);
                    if (responseJournal.Success)
                    {
                        return Json(new { success = response.Success, message = response.Message, refreshGrid = "gridjournal", data = ((BankReceivedDTO)response.Data).Header as BankReceivedList });
                    }
                    else
                    {
                        return Json(new { success = responseJournal.Success, message = responseJournal.Message });
                    }
                }
                else
                {
                    return Json(new { success = false, message = response.Message });
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message.ToString() });
            }
        }

        [HttpPost]
        public async Task<IActionResult> Submit(BankReceivedList model, List<AccountReceivable> Details)
        {
            var response = new ApiResponse();

            List<BankReceivedDetail> modeldetail = new List<BankReceivedDetail>();
            if (model.PayToType == "3")
            { model.SourceDoc = "AR"; }
            else { model.SourceDoc = "GL"; }
            if (Details.Count > 0)
            {
                model.JobId = Details[0].JobId;
                model.ReferenceNumberId = Details[0].AccountReceivableNumber;
                foreach (AccountReceivable data in Details)
                {
                    BankReceivedDetail pd = new BankReceivedDetail();
                    pd.AccountReceivebleNumber = data.AccountReceivableNumber;
                    pd.LegalEntityId = data.LegalEntityId;
                    pd.BusinessUnitId = data.BusinessUnitId;
                    pd.AffiliationId = data.AffilitionId;
                    pd.AccountId = data.AccountId;
                    pd.BrandId = data.BrandId;
                    pd.JobId = data.JobId;
                    pd.TransactionDate = data.TransactionDate;
                    pd.DueDate = data.DueDate;
                    pd.CurrencyId = data.CurrencyId;
                    pd.Amount = data.Amount;
                    pd.ExchangeRate = data.ExchangeRate;
                    pd.BankAccountDestination = data.BankAccountDestination;
                    pd.Charges = data.Charges;
                    pd.ClientId = data.ClientId;
                    pd.SourceDoc = "AR";
                    modeldetail.Add(pd);
                }

            }
            else
            {
                model.ReferenceNumberId = "";
                model.JobId = "";
                model.AffilitionId = "";
            }
            model.Status = PaymentRequestStatus.WaitingApproval;
            return await Process(model, modeldetail);
        }

        public async Task<IActionResult> Process(BankReceivedList model, List<BankReceivedDetail> Details)
        {
            try
            {
                var response = new ApiResponse();
                BankReceivedDTO pay = new BankReceivedDTO();
                pay.Header = model;
                pay.Details = Details;

                if (!string.IsNullOrEmpty(model.Id))
                {
                    if (model.isAdvancePayment == true)
                    {
                        model.Status = PaymentRequestStatus.Open;
                        response = await client.PostApiResponse<BankReceivedDTO>($"{url}/AddWithDetail", pay);
                    }
                    else
                        response = await client.PutApiResponse<BankReceivedDTO>($"{url}/EditWithDetail", pay);
                }
                else
                {
                    response = await client.PostApiResponse<BankReceivedDTO>($"{url}/AddWithDetail", pay);
                }
                return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message.ToString(), url = Url.Action(nameof(Index)) });
            }
        }

        public IActionResult GetPayToType()
        {
            var result = new List<SelectListItem>()
            {
                new SelectListItem() { Text = "Petty Cash", Value = "1" },
                new SelectListItem() { Text = "Bank", Value = "2" },
                new SelectListItem() { Text = "Client", Value = "3"},
                new SelectListItem() { Text = "Other", Value = "4"}
            };
            return Json(result, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(BankReceivedList model) => Json(await client.DeleteApiResponse<BankReceivedList>($"{url}/{model.Id}"));
    }


}
