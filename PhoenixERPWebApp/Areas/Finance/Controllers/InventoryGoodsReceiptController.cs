﻿using CodeMarvel.Infrastructure.WebNetCore;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Phoenix.WebExtension.RestApi;
using PhoenixERPWebApp.Models;
using PhoenixERPWebApp.Models.Finance.Transaction.FixedAsset;
using PhoenixERPWebApp.Models.Finance.Transaction.Inventory;
using PhoenixERPWebApp.Models.Finance.Transaction.InventoryGoodsReceipt;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Areas.Finance.Controllers
{
    [Area("Finance")]
    public class InventoryGoodsReceiptController : BaseController
    {
        string url = ApiUrl.InventoryGoodsReceipt;
        string urlInvt = ApiUrl.Inventory;
        string urlFA = ApiUrl.FixedAssetUrl;
        private readonly ApiClientFactory client;

        public InventoryGoodsReceiptController(ApiClientFactory client)
        {
            this.client = client;
        }

        // GET: GoodReceipt
        public IActionResult Index()
        {
            return View();
        }

        // GET: GoodReceipt/Details/5
        public async Task<IActionResult> Detail([FromQuery(Name = "id")] string Id)
        {
            var model = new InventoryGoodsReceipt();
            ViewBag.Header = "Goods Receipt";

            if (!string.IsNullOrEmpty(Id))
            {
                ViewBag.RecordID = Id;
                model = await client.Get<InventoryGoodsReceipt>($"{url}/{Id}");
            }

            return View(model);
        }

        static T CastResponseData<T>(object entity) where T : class
        {
            return entity as T;
        }

        public async Task<IActionResult> Read([DataSourceRequest] DataSourceRequest request)
        {
            try
            {               
                //List<InventoryGoodsReceipt> model = new List<InventoryGoodsReceipt>();
               
                var model = await client.Get<List<InventoryGoodsReceipt>>($"{url}") ?? new List<InventoryGoodsReceipt>();
                //if (response != null)
                //{
                //    model = CastResponseData<List<InventoryGoodsReceipt>>(response.Data);
                //}
                                
                var data = model.ToDataSourceResult(request);
                return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
            
        }

        public async Task<IActionResult> ReadDetail([DataSourceRequest] DataSourceRequest request, string grId)
        {
            try
            {   
                var model = await client.Get<List<InventoryGoodsReceiptDetail>>($"{url}/getDetail/{grId}") ?? new List<InventoryGoodsReceiptDetail>();
                var data = model.ToDataSourceResult(request);
                return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }

        }

        public async Task<IActionResult> ReadHistory([DataSourceRequest] DataSourceRequest request, string poId)
        {
            try
            {
                var model = await client.Get<List<InventoryGoodsReceiptDetail>>($"{url}/getDetailHistory/{poId}") ?? new List<InventoryGoodsReceiptDetail>();
                var data = model.ToDataSourceResult(request);
                return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }

        }

        public async Task<IActionResult> ReadInventoryDetail([DataSourceRequest] DataSourceRequest request, string grId)
        {
            try
            {
                var model = await client.Get<List<InventoryDetail>>($"{url}/getInventoryDetail/{grId}") ?? new List<InventoryDetail>();
                var data = model.ToDataSourceResult(request);
                return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }

        }

        public async Task<IActionResult> ReadFixedAsset([DataSourceRequest] DataSourceRequest request, string grId)
        {
            try
            {
                var model = await client.Get<List<FixedAsset>>($"{url}/getFixedAsset/{grId}") ?? new List<FixedAsset>();
                var data = model.ToDataSourceResult(request);
                return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }

        }

        public async Task<IActionResult> GetDataPO(string poId)
        {
            var model = await client.Get<InventoryGoodsReceipt>($"{url}/getPO/{poId}");
            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> GetDataPODetail([DataSourceRequest] DataSourceRequest request, string poId)
        {
            var model = await client.Get<List<InventoryGoodsReceiptDetail>>($"{url}/cekDetail/{poId}") ?? new List<InventoryGoodsReceiptDetail>();
            //var model = new List<InventoryGoodsReceiptDetail>();
            //if (cek > 0)
            //{
            //    model = await client.Get<List<InventoryGoodsReceiptDetail>>($"{url}/getDetail/{grId}") ?? new List<InventoryGoodsReceiptDetail>();
            //}
            //else
            //{
            //    model = await client.Get<List<InventoryGoodsReceiptDetail>>($"{url}/getPODetail/{poId}") ?? new List<InventoryGoodsReceiptDetail>();
            //}
            
            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        [HttpPost]
        public async Task<IActionResult> Save(InventoryGoodsReceipt Header, List<InventoryGoodsReceiptDetail> Details)
        {
            try
            {
                var response = new ApiResponse();
                response.Success = true;

                var requestor = await client.Get<InfoEmployee>($"{ApiUrl.EmployeeInfoUrl}/getByUserLogin");
                if (requestor.EmployeeId != Header.PicId) {
                    throw new Exception("That allow save is "+Header.PicName +" Thanks !");
                }
                Header.ReceivedBy = requestor.EmployeeId;
                Header.ReceivedByName = requestor.EmployeeName;
                
                var model = new InventoryGoodsReceiptDTO();
                model.Header = Header;
                model.Details = Details;

                if (!string.IsNullOrEmpty(Header.Id))
                {   
                    response = await client.PutApiResponse<InventoryGoodsReceipt>(url, model);
                }
                else
                {
                    Header.Id = Guid.NewGuid().ToString();
                    response = await client.PostApiResponse<InventoryGoodsReceipt>(url, model);
                }
                
                return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Detail)) + "?Id="+Header.Id });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message.ToString() });
            }

        }

        [HttpPost]
        public async Task<IActionResult> Submit(string InventoryGoodReceiptId)
        {
            try
            {
                var response = await client.PutApiResponse<object>($"{url}/submit_good_receipts?InventoryGoodReceiptId=" + InventoryGoodReceiptId);
                //    new
                //{
                //    success = true
                //    ,
                //    data = await client.PutApiResponse<object>($"{url}/submit_good_receipts?PoId=" + PurchaseOrderId),
                //    url = Url.Action(nameof(Index))
                //};
                return Json(response);
            }
            catch (Exception ex)
            {
                var response = new { success = false,data=new { }, message = ex.Message.ToString() };
                return Json(response);
            }

        }

        [HttpPost]
        public async Task<IActionResult> Generate(string grId)
        {
            try
            {   
                //var requestor = await client.Get<InfoEmployee>($"{ApiUrl.EmployeeInfoUrl}/getByUserLogin");
                var response = new ApiResponse();
                //var model = new InventoryGoodsReceipt();
                
                var result = await client.Get<int>($"{url}/generate/{grId}");
                
                return Json(new { success = true, message = response.Message, url = Url.Action(nameof(Index)) });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message.ToString() });
            }

        }

        //[AcceptVerbs("Post")]
        //public ActionResult EditingPOD_Create([DataSourceRequest] DataSourceRequest request, InventoryGoodsReceiptDetail igrd)
        //{
        //    if (igrd != null && ModelState.IsValid)
        //    {
        //        igrd.Id = Guid.NewGuid().ToString();
        //    }

        //    return Json(new[] { igrd }.ToDataSourceResult(request, ModelState), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //}

        [AcceptVerbs("Post")]
        public ActionResult EditingPOD_Update([DataSourceRequest] DataSourceRequest request, InventoryGoodsReceiptDetail igrd)
        {
            if (igrd != null && ModelState.IsValid)
            {
                
            }

            return Json(new[] { igrd }.ToDataSourceResult(request, ModelState), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        [AcceptVerbs("Post")]
        public ActionResult EditingInv_Update([DataSourceRequest] DataSourceRequest request, InventoryDetail invd)
        {
            if (invd != null && ModelState.IsValid)
            {
                
            }

            return Json(new[] { invd }.ToDataSourceResult(request, ModelState), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        [AcceptVerbs("Post")]
        public ActionResult EditingFA_Update([DataSourceRequest] DataSourceRequest request, FixedAsset fa)
        {
            if (fa != null && ModelState.IsValid)
            {

            }

            return Json(new[] { fa }.ToDataSourceResult(request, ModelState), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<ActionResult> GetDDLItemName(string poId)
        {
            //var getUrl = $"{ApiUrl.dropdownUrl}/GetItemName/{poId}";
            var model = await client.Get<List<SelectListItem>>($"{url}/getDDLItemName/{poId}") ?? new List<SelectListItem>();
            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> GetItemPODetail(string podId)
        {
            var data = await client.Get<PurchaseOrderDetail>($"{url}/GetPODData/{podId}") ?? new PurchaseOrderDetail();
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        //[AcceptVerbs("Post")]
        //public ActionResult UpdateDetailList([DataSourceRequest] DataSourceRequest request, PurchaseRequestDetailList model)
        //{
        //    if (model != null && ModelState.IsValid)
        //    {
        //    }

        //    return Json(new[] { model }.ToDataSourceResult(request, ModelState), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        //}
    }
}