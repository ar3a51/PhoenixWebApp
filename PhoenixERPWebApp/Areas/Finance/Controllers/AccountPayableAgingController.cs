﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Phoenix.WebExtension.Attributes;
using Phoenix.WebExtension.RestApi;
using PhoenixERPWebApp.Models.Finance.Transaction.AccountPayableAging;

namespace PhoenixERPWebApp.Areas.Finance.Controllers
{
    [Area("Finance")]
    //[MenuArea("590541e3-a932-4d29-b972-ddccb8efc1d7")]
    public class AccountPayableAgingController : Controller
    {
        string url = ApiUrl.PettyCashSettlement;
        private readonly ApiClientFactory client;

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Read([DataSourceRequest] DataSourceRequest request)
        {
            //var model = await client.Get<List<AccountPayable>>(url) ?? new List<AccountPayable>();
            var model = new List<AccountPayableAgingList>();
            model.Add(new AccountPayableAgingList()
            {
                Id = "001",
                ClientName = "Aliati",
                Invoice = "INV001",
                Date = DateTime.Today,
                APNumber = "AP001",
                APDate = DateTime.Today,
                Received = DateTime.Today,
                DueDate = DateTime.Today,
                Day = 77,
                YourRef = "REF001",
                Remarks = "Lorem Isum",
                JobId = "JOB001",
                Divisi = "IT",
                Legal = "Advis",
                P0130 = 0,
                P3160 = 0,
                P6190 = 91201231,
                P90 = 0,
                NotDue = 0,
                OverDue = 84912311,
                May0107 = 0,
                May0814 = 0,
                May1522 = 0,
                May2330 = 63128121
            });
            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }
    }
}