﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Phoenix.WebExtension.Attributes;
using Phoenix.WebExtension.RestApi;
using PhoenixERPWebApp.Models.Finance.Transaction.PaymentRequest;

namespace PhoenixERPWebApp.Areas.Finance.Controllers
{
    [Area("Finance")]
    public class BankPaymentController : Controller
    {
        string url = ApiUrl.PaymentRequestUrl;
        private readonly ApiClientFactory client;

        public BankPaymentController(ApiClientFactory client)
        {
            this.client = client;
        }

        public IActionResult Index()
        {
            return View();
        }
        static T CastResponseData<T>(object entity) where T : class
        {
            return entity as T;
        }

        public async Task<IActionResult> Read([DataSourceRequest] DataSourceRequest request, PaymentRequestFilter Filter)
        {
            try
            {
                var response = new ApiResponse();
                List<PaymentRequestList> model = new List<PaymentRequestList>();
                Filter.isBankPayment = true;
                response = await client.PostApiResponse<List<PaymentRequestList>>($"{url}/GetWithFilter/", Filter);

                if (response.Data != null)
                {
                    model = CastResponseData<List<PaymentRequestList>>(response.Data);
                }

                var data = model.ToDataSourceResult(request);
                return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
    }
}