﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CodeMarvel.Infrastructure.WebNetCore;
using Kendo.Mvc.UI;
using Phoenix.WebExtension.RestApi;
using PhoenixERPWebApp.Models.Finance.Transaction;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace PhoenixERPWebApp.Areas.Finance.Controllers
{
    [Area("Finance")]
    public class AccountReceivableStatementController : BaseController
    {
        private readonly ApiClientFactory client;
        string ar_url = ApiUrl.ARClientUrl;
        string url_invoice = ApiUrl.InvoiceClientMultipleUrl;

        public AccountReceivableStatementController(ApiClientFactory client)
        {
            this.client = client;
        }

        public IActionResult Index()
        {
            return View();
        }        
        
        public async Task<IActionResult> GetListStatement([DataSourceRequest] DataSourceRequest request, string acct, string legal, string cl, string ms, DateTime sd, DateTime ed)
        {
            acct = String.IsNullOrWhiteSpace(acct) ? "0" : acct;
            legal = String.IsNullOrWhiteSpace(legal) ? "0" : legal;
            cl = String.IsNullOrWhiteSpace(cl) ? "0" : cl;
            ms = String.IsNullOrWhiteSpace(ms) ? "0" : ms;
            string url = $"{ar_url}/GetListStatement/{acct}/{legal}/{cl}/{ms}/{sd.ToString("yyyyMMdd")}/{ed.ToString("yyyyMMdd")}";
            var data = await client.Get<List<StatementAR>>(url) ?? new List<StatementAR>();
            var model = data.ToDataSourceResult(request);
            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }        
    }
}