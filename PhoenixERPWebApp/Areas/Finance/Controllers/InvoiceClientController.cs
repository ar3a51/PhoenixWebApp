﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CodeMarvel.Infrastructure;
using CodeMarvel.Infrastructure.Sessions;
using CodeMarvel.Infrastructure.WebNetCore;
using Microsoft.Extensions.Configuration;
using CodeMarvel.Infrastructure.Options;
using Microsoft.Extensions.Options;
using Kendo.Mvc.UI;
using PhoenixERPWebApp.Models.Finance.Transaction;
using PhoenixERPWebApp.Models.Enum;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PhoenixERPWebApp.Models.Finance;
using Phoenix.WebExtension.RestApi;
using PhoenixERPWebApp.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using PhoenixERPWebApp.Models.Media.MediaOrder;
using Microsoft.AspNetCore.Http;

namespace PhoenixERPWebApp.Areas.Finance.Controllers
{
    [Area("Finance")]
    public class InvoiceClientController : BaseController
    {
        private readonly ApiClientFactory client;
        string url_invoice = ApiUrl.InvoiceClientUrl;
        string url_mediaorder = ApiUrl.MediaOrderURL;
        string url_dropdown = ApiUrl.financedropdownUrl;
        string url_pce = ApiUrl.PceUrl;

        public InvoiceClientController(ApiClientFactory client)
        {
            this.client = client;
        }
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> ReadIndex([DataSourceRequest] DataSourceRequest request, string status, string legal)
        {
            var dataPC = await client.Get<List<InvoiceClient>>($"{url_invoice}/GetPceList/{status}/{legal}") ?? new List<InvoiceClient>();
            var data = dataPC.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> Detail(string Id, string pceno, string invNo, string statusDesc, string Source, bool isApprove)
        {
            InvoiceClient model = new InvoiceClient();
            if (isApprove.Equals(null))
            {
                isApprove = false;
            }
            var isCurrentApproval = false;

            if (!string.IsNullOrEmpty(invNo))
            {
                isCurrentApproval = await client.Get<bool>($"{ApiUrl.ManageTransApproval}/IsCurrentApproval/{invNo}");

                //else
                //{
                string url = $"{url_invoice}/GetInvoiceInfo/{pceno}/{invNo}";
                model = await client.Get<InvoiceClient>(url);
                if (model.Status != "1")
                    model.PayDescription = "";
                //}
                if (statusDesc == "Balance")
                {
                    model.InvoiceNumber = "";
                    model.Id = "";
                    model.TotalAmount = 0;
                    model.AsfAmount = 0;
                    model.totalamtplusasf = 0;
                    model.Vat = 0;
                    model.GrandTotal = 0;
                }
                model.StatusName = statusDesc;
            }
            else
            {
                model.SourceOption = Source;
                model.StatusName = "New";
                if (Source == "Other")
                {
                    model.MainServiceCategory = MainServiceCategoryOther.MainServiceCategoryId;
                    model.MainServiceCategoryName = MainServiceCategoryOther.MainServiceCategoryName;
                }
            }
            ViewBag.processApprove = isApprove && isCurrentApproval;
            model.isApprove = isApprove;
            return View("DetailProdMedia", model);
        }


        public async Task<IActionResult> ReadInvDtl([DataSourceRequest] DataSourceRequest request, string invid, string pceid, string viewData, string invId)
        {
            string invidstr = (string.IsNullOrEmpty(invid) ? "0" : invid);
            string pceidstr = (string.IsNullOrEmpty(pceid) ? "0" : pceid);

            string url = "";
            if (viewData == "Detail")
                url = $"{url_invoice}/GetInvoiceProductionDetail/{pceidstr}/{invId}";
            else
                url = $"{url_invoice}/GetInvoiceProduction/{invidstr}/{pceidstr}";

            var data = await client.Get<List<InvoiceClientItemOther>>(url) ?? new List<InvoiceClientItemOther>();
            var model = data.ToDataSourceResult(request);
            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> ReadInvDtlHistory([DataSourceRequest] DataSourceRequest request, string invid, string pceid)
        {
            string invidstr = (string.IsNullOrEmpty(invid) ? "0" : invid);
            string pceidstr = (string.IsNullOrEmpty(pceid) ? "0" : pceid);

            string url = $"{url_invoice}/ReadInvDtlHistory/{invidstr}/{pceidstr}";
            var data = await client.Get<List<InvoiceClientItemOther>>(url) ?? new List<InvoiceClientItemOther>();
            var model = data.ToDataSourceResult(request);
            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }


        [AcceptVerbs("Post")]
        public async Task<IActionResult> Create_([DataSourceRequest] DataSourceRequest request, InvoiceClientItemOther model)
        {
            if (ModelState.IsValid)
            {
                model.Id = Guid.NewGuid().ToString();
            }
            return Json(new[] { model }.ToTreeDataSourceResult(request, ModelState), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        [AcceptVerbs("Post")]
        public async Task<IActionResult> Update_([DataSourceRequest] DataSourceRequest request, InvoiceClientItemOther model)
        {
            //if (model != null && ModelState.IsValid)
            //{
            //}
            return Json(new[] { model }.ToTreeDataSourceResult(request, ModelState), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        [AcceptVerbs("Post")]
        public async Task<IActionResult> Delete_([DataSourceRequest] DataSourceRequest request, InvoiceClientItemOther model)
        {
            return Json(new[] { model }.ToTreeDataSourceResult(request, ModelState), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        [HttpPost]
        public async Task<IActionResult> Post(InvoiceClient invoice, List<InvoiceClientItemOther> items_other, IFormFile fileVatDoc, IFormFile filePoDoc)
        {
            var response = new ApiResponse();
            invoice.items_other = items_other;
            if (string.IsNullOrEmpty(invoice.PceId))
                invoice.PceId = invoice.PceCode;

            invoice.Status = SingleInvoiceClientStatus.WaitingApproval.ToString();

            if (!string.IsNullOrEmpty(invoice.Id))
            {
                response = await client.PutApiResponse<InvoiceClient>($"{url_invoice}/updateandsubmit", invoice, fileVatDoc, filePoDoc);
            }
            else
            {
                response = await client.PostApiResponse<InvoiceClient>($"{url_invoice}/addandsubmit", invoice, fileVatDoc, filePoDoc);
                // response = await client.PostApiResponse<InvoiceClient>($"{url_invoice}", invoice);
            }

            return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
            //return Json(new { success = model.Success, message = model.Message, url = Url.Action(nameof(Index)) });
            //return Json("", new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<ActionResult> GetDDLBank(string orgid)
        {
            var getUrl = $"{url_invoice}/GetDDLBank/{orgid}";
            var model = await client.Get<List<SelectListItem>>(getUrl) ?? new List<SelectListItem>();
            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<ActionResult> GetDetailFromPceList(string pceId)
        {
            var getUrl = $"{url_invoice}/GetDetailFromPceList/{pceId}";
            var model = await client.Get<List<PceTask>>(getUrl) ?? new List<PceTask>();
            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }


        //GetDetailFromPceList/{pceno}/{companyid}

        public async Task<IActionResult> GetPceChoiceList([DataSourceRequest] DataSourceRequest request, string multipceid, string legalid, string divid, string mservice, string clientt, string currency, string pceId, string jobId)
        {
            string url = $"{url_invoice}/GetPceChoiceList/{legalid ?? "0"}/{divid ?? "0"}/{mservice ?? "0"}/{clientt ?? "0"}/{currency ?? "0"}/{pceId ?? "-"}/{jobId ?? "-"}";
            var data = await client.Get<List<InvoiceClient>>(url) ?? new List<InvoiceClient>();
            var model = data.ToDataSourceResult(request);
            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> GetVendorData(string vendorid)
        {
            var model = new InvoiceClient();
            if (!string.IsNullOrEmpty(vendorid))
                model = await client.Get<InvoiceClient>($"{url_invoice}/{"GetCompanyInfo"}/{vendorid}") ?? new InvoiceClient();

            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });

        }

        public async Task<IActionResult> GetAffiliation(string divisiid)
        {
            var model = new InvoiceClient();
            if (!string.IsNullOrEmpty(divisiid))
                model = await client.Get<InvoiceClient>($"{url_invoice}/{"GetAffiliation"}/{divisiid}") ?? new InvoiceClient();

            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });

        }
        

        [HttpPost]
        public async Task<IActionResult> Approve(InvoiceClient model)
        {
            var response = await client.PutApiResponse<InvoiceClient>($"{url_invoice}/Approve", model);
            return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
        }

        [HttpPost]
        public async Task<IActionResult> ApprForceCloseove(InvoiceClient model)
        {
            var response = await client.PutApiResponse<InvoiceClient>($"{url_invoice}/ForceClose", model);
            return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
        }

        [HttpPost]
        public async Task<IActionResult> Reject(InvoiceClient model)
        {
            //model.Status = StatusTransaction.Rejected;
            var response = await client.PutApiResponse<InvoiceClient>($"{url_invoice}/Reject", model);
            return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
        }

        [HttpPost]
        public async Task<IActionResult> CreateAndReadAutomaticJournal(InvoiceClient model, IFormFile fileVatDoc, IFormFile filePoDoc)
        {
            var response = new ApiResponse();
            try
            {
                if (string.IsNullOrEmpty(model.Status))
                    model.Status = SingleInvoiceClientStatus.Open.ToString();

                if (string.IsNullOrEmpty(model.PceId))
                    model.PceId = model.PceCode;

                if (!string.IsNullOrEmpty(model.InvoiceNumber))
                {
                    response = await client.PutApiResponse<InvoiceClient>($"{url_invoice}/updatedata", model, fileVatDoc, filePoDoc);
                }
                else
                {
                    response = await client.PostApiResponse<InvoiceClient>($"{url_invoice}/adddata", model, fileVatDoc, filePoDoc);
                }

                if (response.Success)
                {
                    await client.PostApiResponse<InvoiceClient>($"{url_invoice}/PostDatatoSp", response.Data);
                    return Json(new { success = response.Success, message = response.Message, refreshGrid = "gridjournal", data = response.Data as InvoiceClient });
                }
                else
                {
                    return Json(new { success = false, message = response.Message });
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message.ToString() });
            }
        }


        public async Task<IActionResult> ReadJournalInvoiceClient([DataSourceRequest] DataSourceRequest request, string Id)
        {
            var model = await client.Get<List<Journal>>($"{url_invoice}/GetDataJournal/{Id}") ?? new List<Journal>();
            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }
    }
}