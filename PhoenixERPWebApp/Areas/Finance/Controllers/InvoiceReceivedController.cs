﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CodeMarvel.Infrastructure;
using CodeMarvel.Infrastructure.Sessions;
using CodeMarvel.Infrastructure.WebNetCore;
using Microsoft.Extensions.Configuration;
using CodeMarvel.Infrastructure.Options;
using Microsoft.Extensions.Options;
using Phoenix.WebExtension.Attributes;
using Kendo.Mvc.UI;
using PhoenixERPWebApp.Models.Finance.Transaction.InvoiceReceived;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using Kendo.Mvc.Extensions;
using Phoenix.WebExtension.RestApi;
using PhoenixERPWebApp.Models.Enum;
using PhoenixERPWebApp.Models;

namespace PhoenixERPWebApp.Areas.Finance.Controllers
{
    [Area("Finance")]
    public class InvoiceReceivedController : BaseController
    {
        string url = "";// ApiUrl.InvoiceReceived;
        private readonly ApiClientFactory client;

        public InvoiceReceivedController(ApiClientFactory client)
        {
            this.client = client;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Detail(string Id)
        {
            var model = new InvoiceReceivedList();
            

            if (!string.IsNullOrEmpty(Id))
            {
                ViewBag.Header = "Update";
                model = await client.Get<InvoiceReceivedList>($"{url}/{Id}");
                var modelpo = new PurchaseOrder();
                if (model != null) {
                    modelpo = await client.Get<PurchaseOrder>($"{ApiUrl.PurchaseOrderUrl}/{model.PoId}") ?? new PurchaseOrder();
                }
                if (!string.IsNullOrEmpty(modelpo.Id)) {
                    model.MainServiceCategoryId = modelpo.MainserviceCategoryId;
                }
            }
            else
            {      
                ViewBag.Header = "Create";
                model.InvoiceDate = DateTime.Today;
                model.ApprovalFrontDesk = true;
                
            }

            return View(model);
        }

        public async Task<IActionResult> Read([DataSourceRequest] DataSourceRequest request, string status, string businessUnit, string legalEntity, string invoiceNumber)
        {
            try
            {
                var model = new List<InvoiceReceivedList>();
                if (string.IsNullOrEmpty(status))
                {
                    status = "0";
                }
                if (string.IsNullOrEmpty(businessUnit))
                {
                    businessUnit = "0";
                }
                if (string.IsNullOrEmpty(legalEntity))
                {
                    legalEntity = "0";
                }
                if (string.IsNullOrEmpty(invoiceNumber))
                {
                    invoiceNumber = "0";
                }
                if (!string.IsNullOrEmpty(status))
                {
                    model = await client.Get<List<InvoiceReceivedList>>($"{url}/GetByFilterCustom/{status}/{businessUnit}/{legalEntity}/{invoiceNumber}") ?? new List<InvoiceReceivedList>();
                }
                else
                {
                    model = await client.Get<List<InvoiceReceivedList>>(url) ?? new List<InvoiceReceivedList>();
                }

                var data = model.ToDataSourceResult(request);
                return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public async Task<IActionResult> ReadCustom([DataSourceRequest] DataSourceRequest request, string status, string businessUnit, string legalEntity, string invoiceNumber)
        {
            try
            {
                if (string.IsNullOrEmpty(status)) {
                    status = "0";
                }
                if (string.IsNullOrEmpty(businessUnit))
                {
                    businessUnit = "0";
                }
                if (string.IsNullOrEmpty(legalEntity))
                {
                    legalEntity = "0";
                }
                if (string.IsNullOrEmpty(invoiceNumber))
                {
                    invoiceNumber = "0";
                }
                var model = new List<InvoiceReceivedList>();
            
                model = await client.Get<List<InvoiceReceivedList>>($"{url}/GetByFilterCustom/{status}/{businessUnit}/{legalEntity}/{invoiceNumber}") ?? new List<InvoiceReceivedList>();
           

                var data = model.ToDataSourceResult(request);
                return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public async Task<IActionResult> GetClientData(string clientId)
        {
            var model = await client.Get<ClientData>($"{url}/getClient/{clientId}");
            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> GetVendorData(string vendorId)
        {
            var model = await client.Get<ClientData>($"{url}/getVendor/{vendorId}");
            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> GetDataPO(string poId)
        {
            var model = await client.Get<PoData>($"{url}/getPo/{poId}");
            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> GetInvoiceVendorList(string poId)
        {
            var model = await client.Get<List<string>>($"{url}/getInvoiceVendor/{poId}");
            return Json(model, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> GetHistory([DataSourceRequest] DataSourceRequest request, string poId)
        {
            var model = await client.Get<List<InvoiceReceivedList>>($"{url}/getHistory/{poId}") ?? new List<InvoiceReceivedList>();
            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> ReadDetail([DataSourceRequest] DataSourceRequest request,string Id)
        {
            var model = await client.Get<List<InvoiceReceivedDetail>>($"{url}/GetDetailById/{Id}") ?? new List<InvoiceReceivedDetail>();
            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }
        //detail list
        [AcceptVerbs("Post")]
        public ActionResult CreateDetailList([DataSourceRequest] DataSourceRequest request, InvoiceReceivedDetailList model)
        {
            if (model != null && ModelState.IsValid)
            {
                model.Id = Guid.NewGuid().ToString();
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        [AcceptVerbs("Post")]
        public ActionResult UpdateDetailList([DataSourceRequest] DataSourceRequest request, InvoiceReceivedDetailList model)
        {
            if (model != null && ModelState.IsValid)
            {
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        [AcceptVerbs("Post")]
        public ActionResult DestroyDetailList([DataSourceRequest] DataSourceRequest request, InvoiceReceivedDetailList model)
        {
            if (model != null)
            {
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState), new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        [HttpPost]
        public async Task<IActionResult> Save(InvoiceReceivedList model, List<InvoiceReceivedDetail> details)
        {
            var response = new ApiResponse();
            if (!string.IsNullOrEmpty(model.Id))
            {
                if (details.Count <= 0)
                {
                    return Json(new { success = false, message = "Please Enter Invoice Detail Data in Invoice Detail Section" });
                }
                else
                {
                    if (string.IsNullOrEmpty(model.Id))
                    {
                        model.InvoiceStatusId = InvoiceReceivedStatus.Open;
                    }
                    if (string.IsNullOrEmpty(model.InvoiceStatusId))
                    {
                        model.InvoiceStatusId = InvoiceReceivedStatus.Open;
                    }
                    return await Process(model, details);
                }
            }
            else {

                if (string.IsNullOrEmpty(model.Id))
                {
                    model.InvoiceStatusId = InvoiceReceivedStatus.Open;
                }
                if (string.IsNullOrEmpty(model.InvoiceStatusId))
                {
                    model.InvoiceStatusId = InvoiceReceivedStatus.Open;
                }
                return await Process(model, details);

            }
        }
        [HttpPost]
        public async Task<IActionResult> RevisedtoAdmin(InvoiceReceivedList model, List<InvoiceReceivedDetail> details)
        {
            var response = new ApiResponse();
        
            if (details.Count <= 0)
            {
                return Json(new { success = false, message = "Please Enter Invoice Detail Data in Invoice Detail Section" });
            }
            else
            {
                if (string.IsNullOrEmpty(model.InvoiceStatusId))
                {
                    model.InvoiceStatusId = InvoiceReceivedStatus.Open;
                }
                else if (model.InvoiceStatusId == InvoiceReceivedStatus.TAP)
                {
                    model.InvoiceStatusId = InvoiceReceivedStatus.Open;
                }
                else {
                    model.InvoiceStatusId = InvoiceReceivedStatus.Open;
                }
                
                return await Process(model, details);
            } 
        }
        [HttpPost]
        public async Task<IActionResult> RevisedtoTaxAP(InvoiceReceivedList model, List<InvoiceReceivedDetail> details)
        {
            var response = new ApiResponse();

            if (details.Count <= 0)
            {
                return Json(new { success = false, message = "Please Enter Invoice Detail Data in Invoice Detail Section" });
            }
            else
            {
                if (string.IsNullOrEmpty(model.InvoiceStatusId))
                {
                    model.InvoiceStatusId = InvoiceReceivedStatus.Open;
                }
                else if (model.InvoiceStatusId == InvoiceReceivedStatus.TAP)
                {
                    model.InvoiceStatusId = InvoiceReceivedStatus.Open;
                }
                else if (model.InvoiceStatusId == InvoiceReceivedStatus.OpenAP)
                {
                    model.InvoiceStatusId = InvoiceReceivedStatus.TAXTAP;
                }
                else
                {
                    model.InvoiceStatusId = InvoiceReceivedStatus.Open;
                }

                return await Process(model, details);
            }
        }
        
        [HttpPost]
        public async Task<IActionResult> Submit(InvoiceReceivedList model,List<InvoiceReceivedDetail> details)
        {
            var response = new ApiResponse();
            if (!string.IsNullOrEmpty(model.Id))
            {
                if (details.Count <= 0)
                {
                    return Json(new { success = false, message = "Please Enter Invoice Detail Data in Invoice Detail Section" });
                }
                else
                {
                    if (string.IsNullOrEmpty(model.InvoiceStatusId))
                    {
                        model.InvoiceStatusId = InvoiceReceivedStatus.Open;
                    }
                    else if (model.InvoiceStatusId == InvoiceReceivedStatus.Open)
                    {
                        model.InvoiceStatusId = InvoiceReceivedStatus.TAP;
                    }
                    else if (model.InvoiceStatusId == InvoiceReceivedStatus.TAP)
                    {
                        model.InvoiceStatusId = InvoiceReceivedStatus.TAXTAP;
                    }
                    else if (model.InvoiceStatusId == InvoiceReceivedStatus.TAXTAP)
                    {
                        model.InvoiceStatusId = InvoiceReceivedStatus.OpenAP;
                    }

                    return await Process(model, details);
                }
            }
            else {
                if (string.IsNullOrEmpty(model.InvoiceStatusId))
                {
                    model.InvoiceStatusId = InvoiceReceivedStatus.Open;
                }
                else if (model.InvoiceStatusId == InvoiceReceivedStatus.Open)
                {
                    model.InvoiceStatusId = InvoiceReceivedStatus.TAP;
                }
                else if (model.InvoiceStatusId == InvoiceReceivedStatus.TAP)
                {
                    model.InvoiceStatusId = InvoiceReceivedStatus.TAXTAP;
                }
                else if (model.InvoiceStatusId == InvoiceReceivedStatus.TAXTAP)
                {
                    model.InvoiceStatusId = InvoiceReceivedStatus.OpenAP;
                }


                return await Process(model, details);

            }
        }

        public async Task<IActionResult> Process(InvoiceReceivedList model, List<InvoiceReceivedDetail> details)
        {
            var response = new ApiResponse();         
            model.InvoiceVendor = model.InvoiceVendorText != null ? model.InvoiceVendorText : model.InvoiceVendor;
            var newModel = new InvoiceReceivedDTO();
            newModel.Header = model;
            newModel.Details = details;
            if (!string.IsNullOrEmpty(model.Id))
            {
                response = await client.PutApiResponse<InvoiceReceivedList>(url, newModel);
            }
            else
            {
                response = await client.PostApiResponse<InvoiceReceivedList>(url, newModel);
            }
            return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(InvoiceReceivedList model) => Json(await client.DeleteApiResponse<InvoiceReceivedList>($"{url}/{model.Id}"));

       
    }
}