﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodeMarvel.Infrastructure.WebNetCore;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Microsoft.AspNetCore.Mvc;
using PhoenixERPWebApp.Models.Finance.Transaction.FnCashAdvance;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Phoenix.WebExtension.RestApi;
using Phoenix.WebExtension.Attributes;

namespace PhoenixERPWebApp.Areas.Finance.Controllers
{
    [Area("Finance")]
    public class FnCashAdvanceController : BaseController
    {
        string url = ApiUrl.CashAdvanceUrl;
        private readonly ApiClientFactory client;

        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> Detail(string Id)
        {
            var model = new FnCashAdvanceDetail();
            ViewBag.Header = "Create";

            if (!string.IsNullOrEmpty(Id))
            {
                ViewBag.Header = "Update";
                model = await client.Get<FnCashAdvanceDetail>($"{url}/{Id}");
            }

            return View(model);
        }
        public async Task<IActionResult> Read([DataSourceRequest] DataSourceRequest request)
        {
            //var model = await client.Get<List<AccountPayable>>(url) ?? new List<AccountPayable>();
            var model = new List<FnCashAdvanceList>();
            model.Add(new FnCashAdvanceList() {
                Id = "001",
                CANumber = "CA001",
                JobNumber = "NES120014",
                TaskDetail = "FA Development",
                Quantity = 1,
                UnitPrice = 114445,
                Amount = 114445,
                Date = DateTime.Now,
                DueDate = DateTime.Now.AddDays(5),
                Status = "Purposed"
            });
            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }
        public async Task<IActionResult> ReadDetail([DataSourceRequest] DataSourceRequest request)
        {
            //var model = await client.Get<List<AccountPayable>>(url) ?? new List<AccountPayable>();
            var model = new List<FnCashAdvanceDetailList>();
            model.Add(new FnCashAdvanceDetailList()
            {
                Id = "001",
                TaskDetail = "CA001",
                Quantity = 1,
                UnitPrice = 10000000,
                Amount = 10000000
            });
            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }
    }
}