﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Phoenix.WebExtension.Attributes;
using Phoenix.WebExtension.RestApi;
using PhoenixERPWebApp.Models.Finance.Transaction.PettyCashSettlement;
using CodeMarvel.Infrastructure.WebNetCore;

namespace PhoenixERPWebApp.Areas.Finance.Controllers
{
    [Area("Finance")]
    public class PettyCashSettlementController : BaseController
    {
        string url = ApiUrl.PettyCashSettlement;
        private readonly ApiClientFactory client;

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Detail(string Id)
        {
            var model = new PettyCashSettlementDetail();
            ViewBag.Header = "Create";

            if (!string.IsNullOrEmpty(Id))
            {
                ViewBag.Header = "Update";
                model = await client.Get<PettyCashSettlementDetail>($"{url}/{Id}");
            }

            return View(model);
        }

        public async Task<IActionResult> Read([DataSourceRequest] DataSourceRequest request)
        {
            //var model = await client.Get<List<AccountPayable>>(url) ?? new List<AccountPayable>();
            var model = new List<PettyCashSettlementList>();
            model.Add(new PettyCashSettlementList()
            {
                Id = "001",
                PettyCashSettlementNo = "PCS001",
                SettlementDate = DateTime.Now,
                CashHolder = "Secretary",
                Division = "IRIS A",
                BeginningDate = DateTime.Now,
                EndingDate = DateTime.Now.AddDays(5),
                AccountPayableNo = "AP001",
                Currency = "Rp",
                Amount = 100000000,
                Remarks = "Lorem ipsum",
                Status = "Complete"
            });
            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> ReadDetail([DataSourceRequest] DataSourceRequest request)
        {
            //var model = await client.Get<List<AccountPayable>>(url) ?? new List<AccountPayable>();
            var model = new List<PettyCashSettlementDetailList>();
            model.Add(new PettyCashSettlementDetailList()
            {
                Id = "001",
                Date = DateTime.Now,
                Nota = "Upload",
                Reference = "",
                Transaction = "Saldo Awal",
                Debit = 5000000,
                Credit = 0
            });
            model.Add(new PettyCashSettlementDetailList()
            {
                Id = "002",
                Date = DateTime.Now.AddDays(1),
                Nota = "Upload",
                Reference = "1",
                Transaction = "BBM + Parkir",
                Debit = 0,
                Credit = 500000
            });
            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }
    }
}