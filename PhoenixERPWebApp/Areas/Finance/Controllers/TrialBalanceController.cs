﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Phoenix.WebExtension.Attributes;
using Phoenix.WebExtension.RestApi;
using System.Xml;
using PhoenixERPWebApp.Models;
using PhoenixERPWebApp.Models.Enum;
using PhoenixERPWebApp.Models.Finance;

namespace PhoenixERPWebApp.Areas.Finance.Controllers
{
    [Area("Finance")]
    public class TrialBalanceController : Controller
    {
        string url = ApiUrl.TrialBalanceUrl;
        private readonly ApiClientFactory client;
        public TrialBalanceController(ApiClientFactory client)
        {
            this.client = client;
        }
        public IActionResult Index()
        {
            return View();
        }

        static T CastResponseData<T>(object entity) where T : class
        {
            return entity as T;
        }

        public async Task<IActionResult> Read([DataSourceRequest] DataSourceRequest request, TrialBalanceFilter Filter)
        {
            List<TrialBalance> model = new List<TrialBalance>();
            var response = new ApiResponse();
            response = await client.PostApiResponse<List<TrialBalance>>($"{url}/GetData/", Filter);
            if (response.Data != null)
            {
                model = CastResponseData<List<TrialBalance>>(response.Data);
            }

            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        
        public async Task<IActionResult> GetFiscalYearsById(string Id)
        {
            var data = await client.Get<FinancialYear>($"{url}/{"GetFiscalYearsById"}/{Id}");
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }


    }
}