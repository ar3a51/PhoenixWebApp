﻿
(function ($) {
    'use strict';
    var ListChecked = [];
    var url = '';
    var pageFunction = function () {
        var $rfqType = $('#rfqType');
        $('#m_blockui_list').show();
        $('#m_blockui_list2').hide();

        var gridUI = $('#grid');
        var gridUIDialog = $('#dialog_grid');
        gridUIDialog.kendoWindow({
            width: "600px",
            visible: false,
            modal: true,
            actions: [
                "Minimize",
                "Maximize",
                "Close"
            ]
        });

        var gridProjectUI = $('#grid-project');
        // Begin : Request For Quotation Type

        $rfqType.change(function (e) {
            var filters = [];
            if ($(this).val() === "project") {
                $('#m_blockui_list').hide();
                $('#m_blockui_list2').show();
                filters.push({ field: "job_id", operator: "isnotnull" });
                projectGrid(filters);
            } else {
                $('#m_blockui_list').show();
                $('#m_blockui_list2').hide();
                filters.push({ field: "job_id", operator: "isnull" });
                internalGrid(filters);
            }
        });

        // End : Request For Quotation Type
        // Begin : Load Datatable
        function internalGrid(filters = []) {
            $("#grid").empty();
            $.helper.kendoUI.grid($("#grid"), {
                options: {
                    url: $.helper.resolveApi('~/finance/RequestForQuotation/KendoGrid?isinternal=true')
                },
                dataSource: {
                    pageSize: 10,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true,
                    filter: filters
                },
                schema: {
                    model: {
                        id: "id",
                        fields: {
                            id: { editable: true, nullable: true }
                        }
                    },
                    data: "data",
                    total: "recordsTotal"
                },
                resizable: true,
                marvelCheckboxSelectable: {
                    enable: true,
                    ListCheckedTemp: ListChecked
                },
                dataBound: function (e) {

                    var data = e.sender.dataSource.data().toJSON();
                    buildMenu(true);

                    //$('.k-grid-content-locked').css({
                    //    overflow: "visible"
                    //});

                    //$(".row-vendor").click(function () {
                    //    alert('test');
                    //});
                },
                pageable: {
                    refresh: true,
                    pageSizes: false,
                    input: true
                },
                columns: [
                    {
                        title: "Actions", width: "230px",
                        attributes: {
                            style: "text-align:center;overflow: visible;"
                        },
                        template: "<ul class='internal-action-menu' data-uid='#= uid #' data-id='#= data.id #'></ul>"
                    },
                    {
                        field: "request_for_quotation_number",
                        title: "CODE",
                        width: "200px",
                        template: kendo.template($("#row_code_template").html())
                    },
                    {
                        field: "request_for_quotation_date",
                        title: "RFQ Date",
                        width: "100px",
                        template: "#= kendo.toString(kendo.parseDate(request_for_quotation_date, 'yyyy-MM-dd'), 'MMM-dd-yyyy') #"
                    },
                    {
                        field: "business_unit_name",
                        title: "Dept",
                        width: "120px"

                    },
                    {
                        field: "legal_entity_name",
                        title: "Legal",
                        width: "120px"

                    },
                    {
                        field: "record_created_by",
                        title: "Creator",
                        width: "120px"
                    },
                    {
                        field: "approval_status",
                        title: "Status",
                        width: "300px",
                        template: "<span class='approval' data-id='#= data.id #' ></span>"
                    },
                    //{
                    //    field: "vendor",
                    //    title: "Vendor",
                    //    width: "300px",
                    //    template: "<button type='button' class='btn btn-info btn-sm row-vendor'>View</button>"
                    //}

                ]
            });
            url = $.helper.resolveApi("~/Finance/RequestForQuotation/delete")
        }

        function projectGrid(filters = []) {
            $("#grid-project").html('');
            $.helper.kendoUI.grid($("#grid-project"), {
                options: {
                    url: $.helper.resolveApi('~/finance/RequestForQuotation/KendoGrid')
                },
                //height: 550,
                //filterable: true,
                dataSource: {
                    pageSize: 10,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true,
                    filter: filters

                },
                schema: {
                    model: {
                        id: "id",
                        fields: {
                            id: { editable: true, nullable: true }
                        }
                    },
                    data: "data",
                    total: "recordsTotal"
                },
                marvelCheckboxSelectable: {
                    enable: true,
                    ListCheckedTemp: ListChecked
                },
                //detailTemplate: kendo.template($("#project_template").html()),
                //detailInit: detailInit,
                dataBound: function (e) {
                    this.expandRow(this.tbody.find("tr.k-master-row").first());
                    buildMenu(false);
                    $.helper.kendoUI.resizeGrid();
                    $('.k-grid-content-locked').css({
                        overflow: "visible"
                    });
                },
                pageable: {
                    refresh: true,
                    pageSizes: false,
                    input: true
                },
                columns: [
                    //{

                    //    command: [
                    //        {
                    //            name: "Edit",
                    //            click: function (e) {
                    //                console.log(e);
                    //                e.preventDefault();
                    //                var row = e.target.closest('tr');
                    //                var uid = $(row).data(uid);
                    //                var dataSource = gridProjectUI.data("kendoGrid").dataSource;
                    //                var item = dataSource.getByUid(uid.uid);
                    //                window.location.href = $.helper.resolve("~/Finance/RequestForQuotationProject/Detail/?id=" + item.id);
                    //            }
                    //        },
                    //        {
                    //            name: "rowDelete", text: "Delete", click: function (e) {
                    //                e.preventDefault();
                    //                var row = e.target.closest('tr');
                    //                var uid = $(row).data(uid);
                    //                var dataSource = gridProjectUI.data("kendoGrid").dataSource;
                    //                var item = dataSource.getByUid(uid.uid);
                    //                if (item.id) DeleteData([item.id]);
                    //                else {
                    //                    dataSource.cancelChanges(item);
                    //                }
                    //            }
                    //        }
                    //    ], title: "Actions", width: "160px",
                    //},
                    {
                        title: "Actions", width: "230px",
                        attributes: {
                            style: "text-align:center;overflow: visible;"
                        },
                        template: "<ul class='external-action-menu' data-uid='#= uid #' data-id='#= data.id #'></ul>"
                    },
                    {
                        field: "request_for_quotation_number",
                        title: "RFQ Number",
                        width: "100px"

                    },
                    {
                        field: "request_for_quotation_date",
                        title: "RFQ Date",
                        width: "100px",
                        template: "#= kendo.toString(kendo.parseDate(request_for_quotation_date, 'yyyy-MM-dd'), 'MMM-dd-yyyy') #"
                    },
                    {
                        field: "job_id",
                        title: "Job ID",
                        width: "100px"
                    },
                    {
                        field: "pa_number",
                        title: "PCA No.",
                        width: "100px"
                    },
                    {
                        field: "pe_number",
                        title: "PCE No.",
                        width: "100px"
                    },
                    {
                        field: "prefered_vendor_name",
                        title: "Prefered Vendor",
                        width: "100px",
                        template: function (dataObj) {
                            return (dataObj === 1) ? "Yes" : "No";
                        }
                    },
                    {
                        field: "brand_name",
                        title: "Brand",
                        width: "100px"
                    },
                    {
                        field: "business_unit_name",
                        title: "Dept",
                        width: "120px"

                    },
                    {
                        field: "legal_entity_name",
                        title: "Legal",
                        width: "120px"

                    },
                    {
                        field: "record_created_by",
                        title: "Creator",
                        width: "120px"
                    }
                ]
            });

            url = $.helper.resolveApi("~/Finance/RequestForQuotation/delete");
        }

        gridUI.on("click", ".row-vendor", function (e) {
            e.preventDefault();
            
            var enableCreateAllQuotation = false;
            var item = gridUI.data("kendoGrid").dataItem($(this).closest("tr")).toJSON();
            console.log(item);
            if (item.vendor.length === 0) {
                alert('Noting vendor registered');
            }
            enableCreateAllQuotation = (item.vendor.filter(o => o.quotation_id === null).length === item.vendor.length);
            item["enableCreateAll"] = enableCreateAllQuotation;
            var dialog = gridUIDialog.data("kendoWindow");
            dialog.content("");
            kendo.ui.progress(gridUI, true);
            var temp = kendo.template($("#row_vendor_template").html());

            dialog.title("Vendor List Quotation");
            dialog.content(temp(item));

            if (!enableCreateAllQuotation) {
                $('.row-create-bidding').show();
            } else {
                $('.row-create-bidding').hide();
            }

            $('.row-create-bidding').on('click', function () {
                var vendorQuotationIds = [];
                jQuery("input[name='vendor_id']:checked").each(function () {
                    vendorQuotationIds.push(this.getAttribute('data-quotation-id'));
                });
                if (vendorQuotationIds.length > 0) {
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        contentType: 'application/json',
                        url: $.helper.resolveApi("~/finance/PurchaseBidding/createbiddingfromrfq?rfqid=" + item.id),
                        data: JSON.stringify(vendorQuotationIds),
                        success: function (r) {
                            if (r && r.status.success) {
                                toastr.success(r.status.message, "Information");
                                btn.replaceWith("<a href='" + $.helper.resolve(" ~/Finance/VendorQuotation/Detail/?id=" + r.data[0].quotation_id) + "' class='btn btn-sm btn-success'>View</a>")
                                reloadGrid();

                                //create checkbox
                                var chktemplate = `<label class="m-checkbox m-checkbox--solid m-checkbox--single m-checkbox--brand">
                                                    <input type="checkbox" name="vendor_id" value="`+ r.data[0].id + `">
                                                    <span></span>
                                                </label>`;
                                jQuery('.m-widget2__item[data-vendor=' + r.data[0].id + '] > .m-widget2__checkbox').html(chktemplate);
                                $('.row-create-bidding').show();
                            }
                            else {
                                toastr.error(r.status.message, "Information");
                                btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                            }
                        },
                        error: function (r) {
                            toastr.error(r.statusText, r.status);
                            btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                        }
                    });
                } else {
                    toastr.warning('Please select at least one vendor to continue the process.');
                }
                return;
            });

            $('.row-create-quotation').on('click', function () {
                var btn = $(this);
                var VendorIds = btn.data('vendor').split(',');
                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: 'application/json',
                    url: $.helper.resolveApi("~/finance/RequestForQuotation/" + item.id + "/vendorQuotation/create"),
                    data: JSON.stringify(VendorIds),
                    success: function (r) {
                        if (r && r.status.success) {
                            toastr.success(r.status.message, "Information");
                            btn.replaceWith("<a href='" + $.helper.resolve(" ~/Finance/VendorQuotation/Detail/?id=" + r.data[0].quotation_id) + "' class='btn btn-sm btn-success'>View</a>")
                            reloadGrid();

                            //create checkbox
                            var chktemplate = `<label class="m-checkbox m-checkbox--solid m-checkbox--single m-checkbox--brand">
                                                    <input type="checkbox" name="vendor_id" value="`+ r.data[0].id + `">
                                                    <span></span>
                                                </label>`;
                            jQuery('.m-widget2__item[data-vendor=' + r.data[0].id + '] > .m-widget2__checkbox').html(chktemplate);
                            $('.row-create-bidding').show();
                        }
                        else {
                            toastr.error(r.status.message, "Information");
                            btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                        }
                    },
                    error: function (r) {
                        toastr.error(r.statusText, r.status);
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    }
                });


            });
            dialog.center().open();
            kendo.ui.progress(gridUI, false);
        })

        var getMenuItem = function (target, el) {
            var itemIndexes = target.split(/[.,]/),
                item = el;

            if (itemIndexes[0] !== "") {
                for (var i = 0, len = itemIndexes.length; i < len; i++) {
                    item = item.children("li").eq(itemIndexes[i]);
                    if (i < len - 1) {
                        item = item.find("ul:first");
                    }
                }
            }

            return item;
        };

        function buildMenu(isInternal) {

            if (isInternal) {
                $(".internal-action-menu").kendoMenu({
                    dataSource: [
                        {
                            text: "Edit"
                        },
                        {
                            text: "Delete"
                        }
                        , {
                            text: "Other", // TODO how to get the rows first column value here?
                            items: [
                                { text: "Create Vendor Quotation" },
                                { text: "Create Email" }
                            ]
                        }
                    ],
                    open: function (e) {
                        var menu = e.sender;
                        var operation = $(e.item).text();
                        // TODO how to get the current row data here?
                        var Uid = e.sender.element.attr('data-uid');
                        var dataSource = gridUI.data("kendoGrid").dataSource;
                        var item = dataSource.getByUid(Uid);
                        
                        if (!item.is_prefered_vendor) {
                            var createBidding = {
                                text: "Create Bidding",
                                cssClass: "k-menu-create-bidding"
                            };

                            menu.remove(".k-menu-create-bidding");
                            menu.insertBefore(createBidding, getMenuItem('2.1', menu.element));
                        }
                    },
                    select: function (e) {
                        var operation = $(e.item).text();
                        // TODO how to get the current row data here?
                        var Uid = e.sender.element.attr('data-uid');
                        var dataSource = gridUI.data("kendoGrid").dataSource;
                        var item = dataSource.getByUid(Uid);
                        switch (operation) {
                            case "Edit":
                                window.location.href = $.helper.resolve("~/Finance/RequestForQuotation/Detail/?id=" + item.id);
                                break;
                            case "Delete":
                                if (item.id) DeleteData([item.id]);

                                break;
                            case "Create Vendor Quotation":
                                var template = kendo.template($("#create_vendor_quotation_internal_template").html());
                                gridUIDialog.kendoDialog({
                                    width: "400px",
                                    title: "Create Vendor Quotation",
                                    closable: false,
                                    modal: false,
                                    content: template(item),
                                    actions: [
                                        { text: 'Close' },
                                        {
                                            text: 'Create', primary: true,
                                            action: function (e) {
                                                kendo.ui.progress(gridUIDialog, true);

                                                $.ajax({
                                                    type: "POST",
                                                    contentType: "application/json; charset=utf-8",
                                                    url: $.helper.resolveApi("~/finance/RequestForQuotation/createvendorquotation/?rfqid=" + item.id),
                                                    success: function (result) {
                                                        if (result.status.success)
                                                            setTimeout(function () {
                                                                gridUIDialog.data("kendoDialog").close();
                                                                toastr.success(result.status.message, "Information");
                                                                if (item.is_prefered_vendor) {
                                                                    window.location.href = $.helper.resolve("~/Finance/VendorQuotation/Detail/?id=" + result.data.recordID);
                                                                }
                                                            }, 3000);
                                                        else {
                                                            gridUIDialog.data("kendoDialog").close();
                                                            toastr.info(result.status.message, "information");
                                                        }
                                                    },
                                                    error: function () {
                                                        alert('err');
                                                    }
                                                });

                                                return false;
                                            }
                                        }
                                    ],
                                    initOpen: initOpenCreateVendorQuotation,
                                    //open: onOpen,
                                    //close: onClose,
                                    //show: onShow,
                                    //hide: onHide
                                });
                                gridUIDialog.data("kendoDialog").open();
                                break;
                            case "Create Bidding":
                                template = kendo.template($("#create_vendor_quotation_internal_template").html());
                                gridUIDialog.kendoDialog({
                                    width: "400px",
                                    title: "Create Bidding",
                                    closable: false,
                                    modal: false,
                                    content: template(item),
                                    actions: [
                                        { text: 'Close' },
                                        {
                                            text: 'Create', primary: true,
                                            action: function (e) {
                                                kendo.ui.progress(gridUIDialog, true);

                                                $.ajax({
                                                    type: "POST",
                                                    contentType: "application/json; charset=utf-8",
                                                    url: $.helper.resolveApi("~/finance/PurchaseBidding/createbidding?rfqid=" + item.id),
                                                    success: function (result) {
                                                        if (result.status.success)
                                                            setTimeout(function () {
                                                                gridUIDialog.data("kendoDialog").close();
                                                                toastr.success(result.status.message, "Information");
                                                                if (item.is_prefered_vendor) {
                                                                    window.location.href = $.helper.resolve("~/Finance/bidding/VendorSelection?id=" + result.data.recordID);
                                                                }
                                                            }, 3000);
                                                        else {
                                                            gridUIDialog.data("kendoDialog").close();
                                                            toastr.info(result.status.message, "information");
                                                        }
                                                    },
                                                    error: function () {
                                                        alert('err');
                                                    }
                                                });

                                                return false;
                                            }
                                        }
                                    ]
                                });
                                gridUIDialog.data("kendoDialog").open();
                                break;
                            case "Create Email":
                                window.location.href = $.helper.resolve("~/Finance/RequestForQuotation/CreateEmail/?id=" + item.id);
                                break;
                        }
                    }
                });
            }
            else {
                $(".external-action-menu").kendoMenu({
                    dataSource: [
                        {
                            text: "Edit"
                        },
                        {
                            text: "Delete"
                        },
                        {
                            text: "Other", // TODO how to get the rows first column value here?
                            items: [
                                { text: "Create Vendor Quotation" },
                                //{ text: "Create PO" }
                            ]
                        }],

                    select: function (e) {
                        var operation = $(e.item).text();
                        // TODO how to get the current row data here?
                        var Uid = e.sender.element.attr('data-uid');
                        var dataSource = gridProjectUI.data("kendoGrid").dataSource;
                        var item = dataSource.getByUid(Uid);
                        switch (operation) {
                            case "Edit":
                                window.location.href = $.helper.resolve("~/Finance/RequestForQuotationProject/Detail/?id=" + item.id);
                                break;
                            case "Delete":
                                if (item.id) DeleteData([item.id]);

                                break;
                            case "Create Vendor Quotation":
                                var template = kendo.template($("#create_vendor_quotation_external_template").html());

                                gridUIDialog.kendoDialog({
                                    width: "400px",
                                    title: "Create Vendor Quotation",
                                    closable: false,
                                    modal: false,
                                    content: template(item),
                                    actions: [
                                        { text: 'Close' },
                                        {
                                            text: 'Create', primary: true,
                                            action: function (e) {
                                                kendo.ui.progress(gridUIDialog, true);
                                                var vendorSelected = gridUIDialog.find('#company_id').data('kendoComboBox');
                                                $.ajax({
                                                    type: "POST",
                                                    contentType: "application/json; charset=utf-8",
                                                    url: $.helper.resolveApi("~/finance/RequestForQuotation/createvendorquotation/?rfqid=" + item.id + "&vendorid=" + vendorSelected.value()),
                                                    success: function (result) {

                                                        if (result.status.success)
                                                            setTimeout(function () {
                                                                gridUIDialog.data("kendoDialog").close();
                                                                toastr.success(result.status.message, "Information");
                                                                window.location.href = $.helper.resolve("~/Finance/VendorQuotation/Detail/?id=" + result.data.recordID);
                                                            }, 3000);
                                                        else {
                                                            gridUIDialog.data("kendoDialog").close();
                                                            toastr.success(result.status.message, "Error");
                                                        }
                                                    },
                                                    error: function () {
                                                        alert('err');
                                                    }
                                                });
                                                return false;
                                            }
                                        }
                                    ],
                                    initOpen: initOpenCreateVendorQuotation
                                });
                                gridUIDialog.data("kendoDialog").open();
                                break;
                        }
                    }
                });
            }
        }

        var vendorDataSource = new kendo.data.DataSource({
            type: "json",
            pageSize: 100,
            serverPaging: true,
            serverFiltering: true,
            filter: { field: "is_vendor", operator: "eq", value: true },
            transport: {
                read: {
                    url: $.helper.resolveApi("~/finance/CompanyProfile/KendoLookup"),
                    contentType: 'application/json',
                    dataType: 'json',
                    type: 'POST',
                    cache: true
                },
                parameterMap: function (data, operation) {
                    var mapRequest = data;
                    return JSON.stringify(mapRequest);
                }
            },
            schema: {
                model: {
                    id: "id"
                },
                data: "items"
            }
        });
        vendorDataSource.read();
        function initOpenCreateVendorQuotation(e) {
            gridUIDialog.find('#company_id')
                .kendoComboBox({
                    placeholder: "Choose ..",
                    dataTextField: "company_name",
                    dataValueField: "id",
                    filter: "contains",
                    autoBind: false,
                    delay: 1000,
                    dataSource: vendorDataSource
                });
        }

        // End : Load Datatable
        function reloadProjectGrid() {
            gridProjectUI.data("kendoGrid").dataSource.read();
            gridProjectUI.data('kendoGrid').refresh();
        }

        gridProjectUI.find(".k-grid-toolbar").on("click", ".k-grid-Reload", function (e) {
            e.preventDefault();
            reloadProjectGrid();
        });


        function reloadGrid() {
            gridUI.data("kendoGrid").dataSource.read();
            gridUI.data('kendoGrid').refresh();
        }

        gridUI.find(".k-grid-toolbar").on("click", ".k-grid-Reload", function (e) {
            e.preventDefault();
            reloadGrid();
        });

        function detailInit(e) {
            var detailRow = e.detailRow;
            detailRow.find(".tabstrip").kendoTabStrip({
                animation: {
                    open: { effects: "fadeIn" }
                }
            });
            if ((e.data.is_locked && e.data.is_active) || e.data.approval_id !== null) {
                var mApproval = new buildViewer(detailRow.find(".status-approval"), {
                    approvalId: e.data.approval_id,
                    onApprove: function (approvalId, e) {

                        $.ajax({
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: $.helper.resolveApi("~/core/Approval/SetApprovalStatus/" + approvalId + "?ApprovalStatus=APPROVED"),
                            success: function (result) {
                                e.refresh();
                            },
                            error: function () {
                                alert('err');
                            }
                        });
                    },
                    onReject: function (approvalId, e) {
                        $.ajax({
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: $.helper.resolveApi("~/core/Approval/SetApprovalStatus/" + approvalId + "?ApprovalStatus=REJECTED"),
                            success: function (result) {
                                e.refresh();
                            },
                            error: function () {
                                alert('err');
                            }
                        });
                    }
                });
            } else {
                detailRow.find(".status-approval").html('<span class="m-badge m-badge--warning m-badge--wide">Draft</span>');
            }
        }

        var DeleteData = function (ArrID) {
            swal({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                reverseButtons: !0
            }).then(function (e) {
                if (e.value) {
                    mApp.block("#m_blockui_list", {
                        overlayColor: "#000000",
                        type: "loader",
                        state: "primary",
                        message: "Processing..."
                    });

                    $.ajax({
                        type: "DELETE",
                        dataType: 'json',
                        contentType: 'application/json',

                        url: url,
                        data: JSON.stringify(ArrID),
                        success: function (r) {
                            if (r.status.success) {
                                swal({
                                    title: 'Deleted!',
                                    text: r.status.message,
                                    type: "success"
                                }).then(function () {
                                    ListChecked = [];
                                    if ($rfqType.val() === "project") {
                                        reloadProjectGrid();
                                    } else {
                                        reloadGrid();
                                    }
                                    location.reload();
                                });
                            } else {
                                swal(
                                    'Information',
                                    r.status.message,
                                    'info'
                                );
                            }

                        },
                        error: function (e, t, s) {
                            swal(
                                'Information',
                                'Ooops, something went wrong !',
                                'info'
                            );
                        }
                    }).then(setTimeout(function () {
                        mApp.unblock("#m_blockui_list");
                    }, 2e3));
                }


            });
        };
        //BEGIN::EVENT BUTTON
        $('#btn-delete').click(function () {
            if (ListChecked.length > 0)
                DeleteData(ListChecked);

        });
        //END::EVENT BUTTON
        return {
            init: function () {
                //initialize
                $rfqType.trigger('change');
            }
        };
    }();



    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery));


