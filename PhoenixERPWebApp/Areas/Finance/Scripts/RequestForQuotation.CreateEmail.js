﻿function calculateAmount(dataItem) {
    dataItem.amount = dataItem.qty * dataItem.unit_price;
    return dataItem.amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") || 0;
}

function itemCodeEditable(dataItem) {
    return dataItem.type_name == "Inventory" ? true : false;
}

function itemUomEditable(dataItem) {
    return dataItem.type_name == "Inventory" ? false : true;
}

(function ($) {
    'use strict';

    var _detailID = $('#detail-id');
    var gridUI = $('#grid');

    var pageFunction = function () {
        var _formRequestForQuotation = $('#form-RequestForQuotation');
        var $companyLookup = $("#company_id");
        var $emailLookup = $("#email_cc");
        var currentId = 1;

        $("#email_attach").kendoUpload();

        //BEGIN:: Kendo MultiTag
        //function onDataBound(e) {
        //    $('.k-multiselect .k-input').unbind('keyup');
        //    $('.k-multiselect .k-input').on('keyup', onClickEnter);
        //}

        //function onClickEnter(e) {
        //    if (e.keyCode === 13) {
        //        var widget = $('#email_cc').getKendoMultiSelect();
        //        var dataSource = widget.dataSource;
        //        var input = $('.k-multiselect .k-input');
        //        var value = input.val().trim();
        //        if (!value || value.length === 0) {
        //            return;
        //        }
        //        var newItem = {
        //            emailCcId: kendo.guid(),
        //            emailCc: value
        //        };

        //        dataSource.add(newItem);
        //        var newValue = newItem.emailCcId;
        //        widget.value(widget.value().concat([newValue]));
        //    }
        //}

        $emailLookup.kendoMultiSelect({
            //dataTextField: "emailCc",
            //dataValueField: "emailCcId",
            //dataSource: {
            //    data: []
            //},
            //dataBound: onDataBound

            placeholder: "Choose ..",
            dataTextField: "email",
            dataValueField: "id",
            filter: "contains",
            delay: 1000,
            autoBind: false,
            dataSource: {
                type: "json",
                pageSize: 100,
                serverPaging: true,
                serverFiltering: true,
                //filter: { field: "is_vendor", operator: "eq", value: true },
                transport: {
                    read: {
                        url: $.helper.resolveApi("~/finance/CompanyProfile/KendoLookup"),
                        contentType: 'application/json',
                        dataType: 'json',
                        type: 'POST',
                        cache: true
                    },
                    parameterMap: function (data, operation) {
                        var mapRequest = data;
                        return JSON.stringify(mapRequest);
                    }
                },
                schema: {
                    data: "items"
                }
            }
        });

        $companyLookup.kendoMultiSelect({
            placeholder: "Choose ..",
            dataTextField: "company_name",
            dataValueField: "id",
            filter: "contains",
            delay: 1000,
            autoBind: false,
            maxSelectedItems: 1,
            dataSource: {
                type: "json",
                pageSize: 100,
                serverPaging: true,
                serverFiltering: true,
                //filter: { field: "is_vendor", operator: "eq", value: true },
                transport: {
                    read: {
                        url: $.helper.resolveApi("~/finance/CompanyProfile/KendoLookup"),
                        contentType: 'application/json',
                        dataType: 'json',
                        type: 'POST',
                        cache: true
                    },
                    parameterMap: function (data, operation) {
                        var mapRequest = data;
                        return JSON.stringify(mapRequest);
                    }
                },
                schema: {
                    data: "items"
                }
            }
        });
        //END:: Kendo MultiTag

        //BEGIN::Form Validation
        _formRequestForQuotation.validate({
            rules: {
                purchase_order_date: {
                    required: true
                },
                delivery_date: {
                    required: true
                },
                company_id: {
                    required: true
                },
                business_unit_id: {
                    required: true
                },
                legal_entity_id: {
                    required: true
                },
                affiliation_id: {
                    required: true
                }
            },
            invalidHandler: function (e, r) {
                var errors = r.numberOfInvalids();
                if (errors) {
                    var message = errors === 1
                        ? 'Please correct the following error:\n'
                        : 'Please correct the following ' + errors + ' errors.\n';

                    if (r.errorList.length > 0) {
                        for (var x = 0; x < r.errorList.length; x++) {
                            console.warn(r.errorList[x]);
                            errors += "\n\u25CF " + r.errorList[x].message;
                        }
                    }
                    alert(message + errors);
                }
                mUtil.scrollTo(_formRequestForQuotation, -200);
            },
            submitHandler: function (e) { }
        });
        ////END::Form Validation



        $('input[type=radio][name=is_prefered_vendor]').on('change', function (e) {
            if ($(this).val() == "true") {
                companyMultiSelect.setOptions({
                    maxSelectedItems: 1
                });
            } else {
                companyMultiSelect.setOptions({
                    maxSelectedItems: 3
                });
            }
        });

        //BEGIN::Load Datatable
        var loadDetailData = function () {
            if (_detailID.val() !== '') {
                mApp.blockPage({
                    overlayColor: "#000000",
                    type: "loader",
                    state: "primary",
                    message: "Processing..."
                }),
                    $.ajax({
                        type: "GET",
                        dataType: 'json',
                        url: $.helper.resolveApi("~/finance/RequestForQuotation/" + _detailID.val() + "/details"),
                        success: function (r) {
                            if (r.status.success && r.data) {
                                var record = r.data;
                                var vendor = r.data.vendor;
                                $('input[name="request_for_quotation_number"]').val(record.request_for_quotation_number);
                                if (record.request_for_quotation_date != null) $('#request_for_quotation_date').datepicker("setDate", moment(record.request_for_quotation_date).format("MM/DD/YYYY"));
                                
                                $.helper.kendoUI.multiSelect.setValue($companyLookup, vendor);
                                $.helper.kendoUI.multiSelect.setValue($emailLookup, vendor);
                                $('input[name="delivery_place"]').val(record.delivery_place);
                                if (record.delivery_date != null) $('#delivery_date').datepicker("setDate", moment(record.delivery_date).format("MM/DD/YYYY"));
                                if (record.closing_date != null) $('#closing_date').datepicker("setDate", moment(record.closing_date).format("MM/DD/YYYY"));
                                $('input[name="brand"]').val(record.brand_name);
                                $('input[name="job_number"]').val(record.job_id);
                                $('input[name="pca_number"]').val(record.pa_number);
                                $('input[id="business_unit_id"]').val(record.business_unit_name);
                                formControll();
                            } else {
                                swal(
                                    'Information',
                                    r.status.message,
                                    'info'
                                );
                            }
                        },
                        error: function (e, t, s) {
                            swal(
                                'Information',
                                'Ooops, something went wrong !',
                                'info'
                            );
                        }
                    }).then(setTimeout(function () {
                        mApp.unblockPage();
                    }, 2e3));
            } else {
                formControll();
                setStatus('new');
            }

        };
        //END::Load Datatable

        //BEGIN::DataSource
        var uomDataSource = new kendo.data.DataSource({
            type: "json",
            pageSize: 100,
            serverPaging: true,
            serverFiltering: true,
            transport: {
                read: {
                    url: $.helper.resolveApi("~/core/Uom/KendoLookup"),
                    contentType: 'application/json',
                    dataType: 'json',
                    type: 'POST'
                },
                parameterMap: function (data, operation) {
                    var mapRequest = data;

                    return JSON.stringify(mapRequest);
                }
            },
            schema: {
                model: {
                    id: "id"
                },
                data: "items"
            }
        });

        uomDataSource.read();

        var itemDataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: {
                    url: $.helper.resolveApi("~/core/Item/lookup"),
                    contentType: 'application/json',
                    dataType: 'json',
                    type: 'GET',
                    cache: true
                },
                parameterMap: function (data) {
                    var mapRequest = data;
                    var extractOption = {
                        filters: " ( r.item_code like @0 )",
                        parameters: '%%'
                    };
                    mapRequest = $.extend(true, mapRequest, extractOption);
                    return mapRequest;

                }
            },
            schema: {
                model: {
                    id: "id"
                },
                data: "items",
                total: "totalItems"
            },
        });

        itemDataSource.read();

        var itemTypeDataSource = new kendo.data.DataSource({
            type: "json",
            pageSize: 100,
            serverPaging: true,
            serverFiltering: true,
            transport: {
                read: {
                    url: $.helper.resolveApi("~/Core/ItemType/KendoLookup"),
                    contentType: 'application/json',
                    dataType: 'json',
                    type: 'POST',
                    cache: true
                },
                parameterMap: function (data) {
                    var mapRequest = data;
                    return JSON.stringify(mapRequest);
                }
            },
            schema: {
                model: {
                    id: "id"
                },
                data: "items"
            },
        });
        itemTypeDataSource.read();
        //END::Datasource

        //BEGIN::Editor
        function itemEditor(container, options) {
            $('<input required name="' + options.field + '"/>')
                .appendTo(container)
                .kendoMultiColumnComboBox({
                    autoBind: false,
                    dataTextField: "item_code",
                    dataValueField: "id",
                    value: options.model.item_id,
                    text: options.model.item_code,
                    height: 550,
                    columns: [
                        { field: "item_code", title: "Item Code", width: 200 },
                        { field: "item_name", title: "Item Name", width: 200 }
                    ],
                    dataSource: itemDataSource,
                    change: function (e) {

                        var equipmentModel = this.dataItem(e.item);
                        console.log(equipmentModel);
                        options.model.item_id = equipmentModel.id;
                        options.model.uom_id = equipmentModel.uom_id;
                        options.model.set("item_code", equipmentModel.item_code);
                        options.model.set("item_name", equipmentModel.item_name);
                        options.model.set("uom_name", equipmentModel.uom_name);
                    }
                });
            var tooltipElement = $('<span class="k-invalid-msg" data-for="' + options.field + '"></span>');
            tooltipElement.appendTo(container);
        }

        function itemTypeEditor(container, options) {

            $('<input required name="' + options.field + '" width="250px"/>')
                .appendTo(container)
                .kendoComboBox({
                    placeholder: "Choose ..",
                    filter: "contains",
                    autoBind: false,
                    dataTextField: "type_name",
                    dataValueField: "type_name",
                    value: options.model.item_type_id,
                    text: options.model.type_name,
                    dataSource: itemTypeDataSource,
                    change: function (e) {
                        var equipmentModel = this.dataItem(e.item);
                        options.model.item_type = equipmentModel.id;
                        options.model.set("type_name", equipmentModel.type_name);
                        if (equipmentModel.type_name != "Inventory") {
                            options.model.item_code = null;
                            options.model.item_name = null;
                            options.model.uom_name = null;
                        }
                    }
                });
            var tooltipElement = $('<span class="k-invalid-msg" data-for="' + options.field + '" ></span>');
            tooltipElement.appendTo(container);
        }

        function uomEditor(container, options) {
            $('<input required name="' + options.field + '"/>')
                .appendTo(container)
                .kendoComboBox({
                    autoBind: false,
                    filter: "contains",
                    dataTextField: "uom_name",
                    dataValueField: "uom_name",
                    value: options.model.uom_id,
                    text: options.model.uom_name,
                    dataSource: uomDataSource,
                    change: function (e) {
                        var equipmentModel = this.dataItem(e.item);
                        options.model.uom_id = equipmentModel.id;
                        options.model.set("uom_name", equipmentModel.uom_name);
                    }
                });
            var tooltipElement = $('<span class="k-invalid-msg" data-for="' + options.field + '" ></span>');
            tooltipElement.appendTo(container);
        }
        //END::Editor

        //BEGIN::Request For Quotation Detail
        $.helper.kendoUI.grid($("#grid"), {
            options: {
                url: $.helper.resolveApi('~/finance/RequestForQuotationDetail/KendoGrid?requestforquotationid=' + _detailID.val())
            },
            navigatable: true,
            pageable: false,
            //height: 300,
            resizable: true,
            save: function () {
                var grid = this;
                setTimeout(function () {
                    grid.refresh();
                })
            },
            dataSource: {
                pageSize: 1000,
                serverPaging: true,
                serverSorting: false,
                batch: true,
                serverOperation: false,
                aggregate: [
                    { field: "amount", aggregate: "sum" },
                ]
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        id: { editable: true, nullable: true },
                        uom_name: { editable: true },
                        item_code: { editable: true }
                    }
                },
                data: "data",
                total: "recordsTotal"
            },

            aggregate: [
                { field: "amount", aggregate: "sum" },
            ],

            marvelCheckboxSelectable: {
                enable: false
            },

            dataBound: function (e) {
                var data = e.sender.dataSource.view().toJSON();
                var TotalAmount = 0;
                $.each(data, function (i, v) {
                    TotalAmount += v.amount;
                });

                var footerAmount = $('span.amount');

                $(footerAmount).html(TotalAmount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                $.helper.kendoUI.resizeGrid();
            },

            columns: [

                {
                    template: "<b>#= data.type_name ? type_name : ''#</b>",
                    field: "type_name",
                    title: "Type",
                    width: "200px",
                    editor: itemTypeEditor
                },
                {
                    field: "item_code",
                    title: "Item Code",
                    width: "150px",
                    editable: itemCodeEditable,
                    editor: itemEditor
                },
                {
                    field: "item_name",
                    title: "Item Name",
                    width: "150px"
                },
                {
                    field: "description",
                    title: "Remarks",
                    attributes: {
                        style: "text-align:left;"
                    },
                    width: "250px"
                },
                {
                    field: "qty",
                    title: "Qty",
                    type: "Number",
                    attributes: {
                        style: "text-align:center;"
                    },
                    width: "75px"
                },
                {
                    field: "uom_name",
                    title: "Unit",
                    attributes: {
                        style: "text-align:center;"
                    },
                    width: "100px",
                    editor: uomEditor,
                    editable: itemUomEditable
                },
                {
                    field: "unit_price",
                    title: "Estimated Price Per Quantity",
                    type: "Number",
                    attributes: {
                        style: "text-align:right;"
                    },
                    template: "#= kendo.toString(data.unit_price, 'n') #",
                    width: "150px"
                },
                {
                    field: "amount",
                    title: "Total Price",
                    type: "Number",
                    editable: function () { return false },
                    attributes: {
                        style: "text-align:right;"
                    },
                    template: "<b>#= calculateAmount(data) #</b>",
                    width: "150px",
                    footerTemplate: "<span align='right' class='amount'></span>"
                }
            ],
            editable: false
        });

        function reloadGrid() {
            gridUI.data("kendoGrid").dataSource.read();
            gridUI.data('kendoGrid').refresh();
        }

        gridUI.find(".k-grid-toolbar").on("click", ".k-grid-Reload", function (e) {
            e.preventDefault();
            reloadGrid();
        });


        //END::Load Request For Quotation Detail

        var formControll = function () {
            $('#btn-save').removeAttr('disabled').show();

            if (_detailID.val() !== '') {
                $('#btn-approval').removeAttr('disabled', '').show();
                $('#btn-remove_approval').attr('disabled', 'disabled').hide();
                $('#btn-submit').attr('disabled', 'disabled').hide();

                if ($('input[name="approval_id"]').val() !== '') {
                    $('#btn-approval').attr('disabled', 'disabled').hide();
                    $('#btn-remove_approval').removeAttr('disabled', '').show();

                    $('#btn-submit').removeAttr('disabled', '').show();
                }

            } else {
                $('#btn-approval').attr('disabled', 'disabled').hide();
                $('#btn-remove_approval').attr('disabled', 'disabled').hide();
                $('#btn-submit').attr('disabled', 'disabled').hide();
            }
            $('#tb').show();
        };

        var formControllSubmited = function (submited) {
            if (!submited) {
                $('#tb').show();
            }
            else {
                $('#tb').hide();
                $('#btn-save').attr('disabled', 'disabled').hide();
                $('#btn-approval').attr('disabled', 'disabled').hide();
                $('#btn-remove_approval').attr('disabled', 'disabled').hide();
                $('#btn-submit').attr('disabled', 'disabled').hide();
            }
        };

        var setStatus = function (state) {
            var output = '';
            switch (state) {
                case 'new':
                    output = `<span class="m-badge m-badge--info m-badge--wide">New</span>`;
                    break;
                case 'draft':
                    output = `<span class="m-badge m-badge--warning m-badge--wide">Draft</span>`;
                    break;
                case 'submited':
                    output = `<span class="m-badge m-badge--danger m-badge--wide">Submited</span>`;
                    break;
                case 'approved':
                    output = `<span class="m-badge m-badge--success m-badge--wide">Approved</span>`;
                    break;
            }
            $('#status').html(output);
        };

        $('#btn-send').click(function () {
            console.log($("#email_cc").getKendoMultiSelect().dataSource.data().toJSON());
        });

        $('#btn-save').click(function () {
            var btn = $(this);
            if (_formRequestForQuotation.valid()) {
                var requestForQuotation = _formRequestForQuotation.serializeToJSON();
                var detailTransactions = $("#grid").data("kendoGrid");
                var approverId = $('#approverId').val();
                var data =
                {
                    requestForQuotation: requestForQuotation,
                    vendorList: companyMultiSelect.value(),
                    details: detailTransactions.dataSource.view().toJSON()
                };
                console.log(data);

                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: 'application/json',
                    url: $.helper.resolveApi("~/finance/RequestForQuotation/saveasdraft"),
                    data: JSON.stringify(data),
                    success: function (r) {
                        if (r && r.status.success) {
                            console.log(r);
                            history.pushState('', 'ID', location.hash.split('?')[0] + '?id=' + r.data.recordID);
                            _detailID.val(r.data.recordID);
                            reloadGrid();
                            $('input[name="id"]').val(r.data.recordID);
                            toastr.success(r.status.message, "Information");
                            setStatus('draft');
                        }
                        else {
                            swal(
                                'Saving',
                                r.status.message,
                                'info'
                            );
                        }
                    },
                    error: function (r) {
                        swal(
                            '' + r.status,
                            r.statusText,
                            'error'
                        );
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    }
                }).then(function () { btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false); });


            }

        });

        $('#btn-submit').click(function () {
            var btn = $(this);
            if (_formRequestForQuotation.valid()) {

                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: 'application/json',
                    url: $.helper.resolveApi("~/finance/RequestForQuotation/submited?RecordId=" + _detailID.val()),
                    //data: JSON.stringify(data),
                    success: function (r) {
                        if (r && r.status.success) {
                            //history.pushState('', 'ID', location.hash.split('?')[0] + '?id=' + r.data.recordID);
                            //_detailID.val(r.data.recordID);
                            //$('input[name="id"]').val(r.data.recordID);
                            btn.removeClass('m-loader m-loader--right m-loader--light');
                            formControllSubmited(true);
                            formControll();
                            reloadGrid();
                            setStatus('submited');
                            toastr.success(r.status.message, "Information");

                        }
                        else {
                            swal(
                                'Saving',
                                r.status.message,
                                'info'
                            );
                            btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                        }


                    },
                    error: function (r) {
                        swal(
                            '' + r.status,
                            r.statusText,
                            'error'
                        );
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    }
                });


            }

        });

        //BEGIN::EVENT BUTTON
        $('#btn-choose_business_unit').click(function () {
            var btn = $(this);
            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
            $('.modal-container').load($.helper.resolve("~/Core/BusinessUnit/Lookup/"), function (result) {
                var $modal = $('#m_modal_business_unit');

                $modal.modal({ show: true });
                btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                var $tree = $('#tree_businessunit');
                $modal.find('#btn-addselected').click(function () {
                    var selected = $tree.jstree("get_selected", true)[0];
                    $('input[name="business_unit_id"]').val(selected.id);
                    $('input[id="business_unit_id"]').val(selected.text);
                    $modal.modal('toggle');
                });
            });
        });
        //END::EVENT BUTTON

        return {
            init: function () {
                //initialize
                loadDetailData()
            }
        };
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery));