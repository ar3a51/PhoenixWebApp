﻿function calculateAmount(dataItem) {
    dataItem.amount = dataItem.qty * dataItem.unit_price;
    return dataItem.amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") || 0;
}

function itemCodeEditable(dataItem) {
    return dataItem.type_name == "Inventory" ? true : false;
}

function itemUomEditable(dataItem) {
    return dataItem.type_name == "Inventory" ? false : true;
}

(function ($) {
    'use strict';

    var _detailID = $('#detail-id');
    var dgdetail = $('#dgdetail');
    var gridUI = $('#grid');
    var item_type_id = '';

    var pageFunction = function () {
        var _formRequestForQuotation = $('#form-RequestForQuotation');
        var $legalEntityLookup = $("#legal_entity_id");
        var $affiliationLookup = $("#affiliation_id");
        var $companyLookup = $("#company_id");
        var $currencyDropdown = $("#currency_id");
        var $paymentTypeLookup = $("#payment_type_id");
        var $invoicingTypeLookup = $("#invoicing_type_id");
        var $purchaseOrderStatusLookup = $("#purchase_order_status_id");
        var $jobIdLookup = $("#job_id");


        //BEGIN::Form Validation
        _formRequestForQuotation.validate({
            rules: {
                purchase_order_date: {
                    required: true
                },
                delivery_date: {
                    required: true
                },
                company_id: {
                    required: true
                },
                business_unit_id: {
                    required: true
                },
                legal_entity_id: {
                    required: true
                },
                affiliation_id: {
                    required: true
                }
            },
            invalidHandler: function (e, r) {
                var errors = r.numberOfInvalids();
                if (errors) {
                    var message = errors === 1
                        ? 'Please correct the following error:\n'
                        : 'Please correct the following ' + errors + ' errors.\n';

                    if (r.errorList.length > 0) {
                        for (var x = 0; x < r.errorList.length; x++) {
                            console.warn(r.errorList[x]);
                            errors += "\n\u25CF " + r.errorList[x].message;
                        }
                    }
                    alert(message + errors);
                }
                mUtil.scrollTo(_formRequestForQuotation, -200);
            },
            submitHandler: function (e) { }
        });
        ////END::Form Validation

        //BEGIN::Dropdown
        var dgOptions = function () {
            var currencyDropdown = undefined;
            $.ajax({
                url: $.helper.resolveApi("~/core/Currency/lookup?length=300"),
                type: "GET",
                success: function (response) {
                    currencyDropdown = response.items;
                    $.each(currencyDropdown, function (key, data) {
                        $currencyDropdown.append($('<option></option>').attr('value', data.id).text(data.currency_code));
                    });
                },
                async: false
            });

            return currencyDropdown;
        };

        //END:: Dropdown

        //BEGIN::Lookup
        $companyLookup.cmSelect2({
            url: $.helper.resolveApi("~/finance/CompanyProfile/lookup"),
            filters: ' r.company_name LIKE @0 and r.is_vendor = 1',
            //paramaters: [],
            result: {
                id: 'id',
                text: 'company_name',
                tax_registration_number: 'tax_registration_number'
            }
        }).on('select2:select', function (e) {
            var data = e.params.data;
            $('input[name="company_name"]').val(data.text);
            $('input[name="tax_registration_number"]').val(data.tax_registration_number);
        });

        $purchaseOrderStatusLookup.cmSelect2({
            url: $.helper.resolveApi("~/finance/PurchaseOrderStatus/lookup"),
            filters: ' r.status_name LIKE @0',
            //paramaters: [],
            result: {
                id: 'id',
                text: 'status_name'
            }
        });

        $jobIdLookup.cmSelect2({
            url: $.helper.resolveApi("~/ProjectManagement/Job/List"),
            filters: ' r.job_number LIKE @0',
            result: {
                id: 'id',
                text: 'job_number'
            },
            options: {
                ajax: {
                    data: function (params) {
                        return {
                            Search_job_number: params.term
                        }
                    },
                    processResults: function (data, page) {
                        var result = {
                            results: [],
                            more: false
                        };
                        if (data && data.rows) {
                            $.each(data.rows, function () {
                                var i = this;
                                var obj = { id: i.id, text: i.job_number, job_name: i.job_name, brand_name: i.brand_name };

                                result.results.push(obj);
                            });
                        }
                        return result;
                    }
                },
                templateResult: formatJobLookup
            }


        }).on('select2:select', function (e) {
            var data = e.params.data;
            $('input[name="brand_name"]').val(data.brand_name);
        });

        function formatJobLookup(opt) {
            if (!opt.id) {
                return opt.text;
            }

            var template = "<span><b>" + opt.text + "</b></span><br/>" + opt.job_name;
            return template;
        }

        $paymentTypeLookup.cmSelect2({
            url: $.helper.resolveApi("~/finance/PaymentType/lookup"),
            filters: ' r.type_name LIKE @0',
            //paramaters: [],
            result: {
                id: 'id',
                text: 'type_name'
            }
        });

        $invoicingTypeLookup.cmSelect2({
            url: $.helper.resolveApi("~/finance/InvoicingType/lookup"),
            filters: ' r.type_name LIKE @0',
            //paramaters: [],
            result: {
                id: 'id',
                text: 'type_name'
            }
        });

        $affiliationLookup.cmSelect2({
            url: $.helper.resolveApi("~/core/Affiliation/lookup"),
            filters: ' r.affiliation_name LIKE @0 ',
            //paramaters: [],
            result: {
                id: 'id',
                text: 'affiliation_name'
            }
        });

        $legalEntityLookup.cmSelect2({
            url: $.helper.resolveApi("~/core/LegalEntity/lookup"),
            filters: ' r.legal_entity_name LIKE @0 ',
            //paramaters: [],
            result: {
                id: 'id',
                text: 'legal_entity_name'
            }
        });
        //END: Lookup

        //BEGIN::Load Datatable
        var loadDetailData = function () {
            if (_detailID.val() !== '') {
                mApp.blockPage({
                    overlayColor: "#000000",
                    type: "loader",
                    state: "primary",
                    message: "Processing..."
                }),
                    $.ajax({
                        type: "GET",
                        dataType: 'json',
                        url: $.helper.resolveApi("~/finance/RequestForQuotation/" + _detailID.val() + "/details"),
                        success: function (r) {
                            if (r.status.success && r.data) {
                                var record = r.data;
                                $('input[name="approval_id"]').val(record.approval_id);
                                $('input[name="request_for_quotation_number"]').val(record.request_for_quotation_number);
                                $('input[name="request_for_quotation_date"]').val(moment(record.request_for_quotation_date).format('MM/DD/YYYY'));
                                $('input[name="job_id"]').val(record.job_id);
                                $companyLookup.cmSetLookup(record.prefered_vendor_id, record.prefered_vendor_name);
                                $('input[name="brand_name"]').val(record.brand_name);
                                $('input[name="delivery_place"]').val(record.delivery_place);
                                $('input[name="delivery_date"]').val(moment(record.delivery_date).format('MM/DD/YYYY'));
                                $('input[name="closing_date"]').val(moment(record.closing_date).format('MM/DD/YYYY'));
                                $jobIdLookup.cmSetLookup(record.job_id, record.job_id);
                                $('input[name="business_unit_id"]').val(record.business_unit_id);
                                $('input[id="business_unit_id"]').val(record.business_unit_name);
                                $legalEntityLookup.cmSetLookup(record.legal_entity_id, record.legal_entity_name);
                                $affiliationLookup.cmSetLookup(record.affiliation_id, record.affiliation_name);
                                $('input[name="exchange_rate"]').val(record.exchange_rate);
                                $paymentTypeLookup.cmSetLookup(record.payment_type_id, record.payment_type_name);

                                $('input[id="pca_number"]').val(record.job_pa_id);
                                $('input[id="pce_number"]').val(record.job_pe_id);
                                $('input[id="term_of_payment"]').val(record.term_of_payment);
                                formControll();
                                if (record.is_locked) {
                                    formControllSubmited(true);
                                    setStatus('submited');
                                } else {
                                    setStatus('draft');
                                }

                            } else {
                                swal(
                                    'Information',
                                    r.status.message,
                                    'info'
                                );
                            }
                        },
                        error: function (e, t, s) {
                            swal(
                                'Information',
                                'Ooops, something went wrong !',
                                'info'
                            );
                        }
                    }).then(setTimeout(function () {
                        mApp.unblockPage();
                    }, 2e3));
            } else {
                formControll();
                setStatus('new');
            }

        };
        //END::Load Datatable

        //BEGIN::DataSource
        var categoryDataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: {
                    url: $.helper.resolveApi("~/core/ItemCategory/lookup"),
                    contentType: 'application/json',
                    dataType: 'json',
                    type: 'GET',
                    cache: true
                },
                parameterMap: function (data) {
                    var mapRequest = data;
                    var extractOption = {
                        filters: " ( r.category_name like @0 )",
                        parameters: '%%'
                    };
                    mapRequest = $.extend(true, mapRequest, extractOption);
                    return mapRequest;

                }
            },
            schema: {
                model: {
                    id: "id"
                },
                data: "items",
                total: "totalItems"
            },
        });

        categoryDataSource.read();

        //BEGIN::DataSource
        var uomDataSource = new kendo.data.DataSource({
            type: "json",
            pageSize: 100,
            serverPaging: true,
            serverFiltering: true,
            transport: {
                read: {
                    url: $.helper.resolveApi("~/core/Uom/KendoLookup"),
                    contentType: 'application/json',
                    dataType: 'json',
                    type: 'POST'
                },
                parameterMap: function (data, operation) {
                    var mapRequest = data;

                    return JSON.stringify(mapRequest);
                }
            },
            schema: {
                model: {
                    id: "id"
                },
                data: "items"
            }
        });

        uomDataSource.read();

        var itemDataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: {
                    url: $.helper.resolveApi("~/core/Item/lookup"),
                    contentType: 'application/json',
                    dataType: 'json',
                    type: 'GET',
                    cache: true
                },
                parameterMap: function (data) {
                    var mapRequest = data;
                    var extractOption = {
                        filters: " ( r.item_type_id = '" + item_type_id + "' )",
                        parameters: ""
                    };
                    mapRequest = $.extend(true, mapRequest, extractOption);
                    return mapRequest;

                }
            },
            schema: {
                model: {
                    id: "id"
                },
                data: "items",
                total: "totalItems"
            },
        });

        itemDataSource.read();

        var itemTypeDataSource = new kendo.data.DataSource({
            type: "json",
            pageSize: 100,
            serverPaging: true,
            serverFiltering: true,
            transport: {
                read: {
                    url: $.helper.resolveApi("~/Core/ItemType/KendoLookup"),
                    contentType: 'application/json',
                    dataType: 'json',
                    type: 'POST',
                    cache: true
                },
                parameterMap: function (data) {
                    var mapRequest = data;
                    return JSON.stringify(mapRequest);
                },
                requestEnd: function (e) {

                }
            },
            schema: {
                model: {
                    id: "id"
                },
                data: "items"
            },
        });
        itemTypeDataSource.read();
        //END::Datasource

        //BEGIN::Editor
        function itemEditor(container, options) {
            $('<input required name="' + options.field + '"/>')
                .appendTo(container)
                .kendoMultiColumnComboBox({
                    autoBind: false,
                    dataTextField: "item_code",
                    dataValueField: "id",
                    value: options.model.item_id,
                    text: options.model.item_code,
                    height: 550,
                    columns: [
                        { field: "item_code", title: "Item Code", width: 200 },
                        { field: "item_name", title: "Item Name", width: 200 }
                    ],
                    dataSource: itemDataSource,
                    change: function (e) {

                        var equipmentModel = this.dataItem(e.item);
                        console.log(equipmentModel);
                        options.model.item_id = equipmentModel.id;
                        options.model.uom_id = equipmentModel.uom_id;
                        options.model.set("item_code", equipmentModel.item_code);
                        options.model.set("item_name", equipmentModel.item_name);
                        options.model.set("uom_name", equipmentModel.uom_name);
                    }
                });
            var tooltipElement = $('<span class="k-invalid-msg" data-for="' + options.field + '"></span>');
            tooltipElement.appendTo(container);
        }

        function itemTypeEditor(container, options) {

            var newDataSource = itemTypeDataSource._data.filter(x => x.type_name == "Inventory" || x.type_name == "Non Inventory");
            $('<input required name="' + options.field + '" width="250px"/>')
                .appendTo(container)
                .kendoComboBox({
                    placeholder: "Choose ..",
                    filter: "contains",
                    autoBind: false,
                    dataTextField: "type_name",
                    dataValueField: "type_name",
                    value: options.model.item_type_id,
                    text: options.model.type_name,
                    dataSource: newDataSource,
                    change: function (e) {
                        debugger;
                        var equipmentModel = this.dataItem(e.item);
                        options.model.item_type = equipmentModel.id;
                        options.model.set("type_name", equipmentModel.type_name);
                        if (equipmentModel.type_name != "Inventory") {
                            options.model.item_code = null;
                            options.model.item_name = null;
                            options.model.uom_name = null;
                        }
                        item_type_id = equipmentModel.id;
                    }
                });
            var tooltipElement = $('<span class="k-invalid-msg" data-for="' + options.field + '" ></span>');
            tooltipElement.appendTo(container);
        }

        function uomEditor(container, options) {
            $('<input required name="' + options.field + '"/>')
                .appendTo(container)
                .kendoComboBox({
                    autoBind: false,
                    filter: "contains",
                    dataTextField: "uom_name",
                    dataValueField: "uom_name",
                    value: options.model.uom_id,
                    text: options.model.uom_name,
                    dataSource: uomDataSource,
                    change: function (e) {
                        var equipmentModel = this.dataItem(e.item);
                        options.model.uom_id = equipmentModel.id;
                        options.model.set("uom_name", equipmentModel.uom_name);
                    }
                });
            var tooltipElement = $('<span class="k-invalid-msg" data-for="' + options.field + '" ></span>');
            tooltipElement.appendTo(container);
        }

        function categoryEditor(container, options) {
            $('<input required name="' + options.field + '"/>')
                .appendTo(container)
                .kendoMultiColumnComboBox({
                    autoBind: false,
                    dataTextField: "category_name",
                    dataValueField: "id",
                    value: options.model.category_id,
                    text: options.model.category_name,
                    height: 550,
                    columns: [
                        { field: "category_name", title: "Unit Name", width: 200 }
                    ],
                    dataSource: categoryDataSource,
                    change: function (e) {
                        console.log(e);
                        var equipmentModel = this.dataItem(e.item);
                        options.model.category_id = equipmentModel.id;
                        options.model.set("category_name", equipmentModel.category_name);
                        //options.model.Unit = equipmentModel.DefaultUnit;
                        //options.model.Price = equipmentModel.DefaultPrice;
                        //alert(equipmentModel.DefaultUnit);
                    }
                });
            var tooltipElement = $('<span class="k-invalid-msg" data-for="' + options.field + '"></span>');
            tooltipElement.appendTo(container);
        }

        //END::Editor
        $.helper.kendoUI.grid($("#grid"), {
            options: {
                url: $.helper.resolveApi('~/finance/RequestForQuotationDetail/KendoGrid?requestforquotationid=' + _detailID.val())
            },
            navigatable: true,
            pageable: false,
            //height: 300,
            resizable: true,
            save: function () {
                var grid = this;
                setTimeout(function () {
                    grid.refresh();
                })
            },
            dataSource: {
                pageSize: 1000,
                serverPaging: true,
                serverSorting: false,
                batch: true,
                serverOperation: false,
                aggregate: [
                    { field: "amount", aggregate: "sum" },
                ]
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        id: { editable: true, nullable: true }
                    }
                },
                data: "data",
                total: "recordsTotal"
            },

            aggregate: [
                { field: "amount", aggregate: "sum" },
            ],

            marvelCheckboxSelectable: {
                enable: false
            },

            dataBound: function (e) {
                var data = e.sender.dataSource.view().toJSON();
                var TotalAmount = 0;
                $.each(data, function (i, v) {
                    TotalAmount += v.amount;
                });

                var footerAmount = $('span.amount');

                $(footerAmount).html(TotalAmount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                $.helper.kendoUI.resizeGrid();
            },
            toolbar: ["create", "cancel", "Reload"],
            columns: [
                {
                    template: "<b>#= data.type_name ? type_name : ''#</b>",
                    field: "type_name",
                    title: "Type",
                    width: "200px",
                    locked: true,
                    lockable: false,
                    groupable: true,
                    sortable: false,
                    editor: itemTypeEditor
                },
                {
                    field: "item_code",
                    title: "Item Code",
                    width: "150px",
                    editable: itemCodeEditable,
                    editor: itemEditor
                },
                {
                    field: "item_name",
                    title: "Item Name",
                    width: "250px"
                },
                {
                    field: "category_name",
                    title: "Category",
                    width: "250px",
                    //locked: true,
                    //lockable: false,
                    editor: categoryEditor
                },
                {
                    field: "subbrand_name",
                    title: "Sub Brand",
                    attributes: {
                        style: "text-align:right;"
                    },
                    footerTemplate: "Total",
                    width: "250px",
                },
                {
                    field: "description",
                    title: "Remarks",
                    attributes: {
                        style: "text-align:right;"
                    },
                    width: "100px"
                },
                {
                    field: "uom_name",
                    title: "Unit",
                    attributes: {
                        style: "text-align:center;"
                    },
                    width: "100px",
                    editor: uomEditor,
                    editable: itemUomEditable
                },
                {
                    field: "qty",
                    title: "Qty",
                    type: "Number",
                    attributes: {
                        style: "text-align:right;"
                    },
                    width: "75px"
                },
                {
                    field: "qty_shift",
                    title: "Shift",
                    type: "Number",
                    attributes: {
                        style: "text-align:right;"
                    },
                    width: "75px"
                },
                {
                    field: "qty_day",
                    title: "Days",
                    type: "Number",
                    attributes: {
                        style: "text-align:right;"
                    },
                    width: "75px"
                },
                {
                    field: "qty_week",
                    title: "Week",
                    type: "Number",
                    attributes: {
                        style: "text-align:right;"
                    },
                    width: "75px"
                },
                {
                    field: "qty_city",
                    title: "City",
                    type: "Number",
                    attributes: {
                        style: "text-align:right;"
                    },
                    width: "75px"
                },
                {
                    field: "unit_price",
                    title: "Estimated Price Per Quantity",
                    type: "Number",
                    attributes: {
                        style: "text-align:right;"
                    },
                    width: "150px"
                },
                {
                    field: "amount",
                    title: "Total Price",
                    type: "Number",
                    editable: function () { return false },
                    attributes: {
                        style: "text-align:right;"
                    },
                    template: "<b>#= calculateAmount(data) #</b>",
                    width: "150px",
                    footerTemplate: "<span class='amount'></span>",
                },
                {
                    command: [
                        {
                            name: "Undo", click: function (e) {
                                e.preventDefault();
                                var row = e.target.closest('tr');
                                var uid = $(row).data(uid);
                                var dataSource = gridUI.data("kendoGrid").dataSource;
                                var item = dataSource.getByUid(uid.uid);
                                dataSource.cancelChanges(item);
                                this.refresh()
                            }
                        },
                        {
                            name: "rowDelete", text: "Delete", click: function (e) {
                                e.preventDefault();
                                var row = e.target.closest('tr');
                                var uid = $(row).data(uid);
                                var dataSource = gridUI.data("kendoGrid").dataSource;
                                var item = dataSource.getByUid(uid.uid);
                                if (item.id) DeleteData([item.id]);
                                else {
                                    dataSource.cancelChanges(item);
                                }
                            }
                        }
                    ], title: "Actions", width: "200px",
                }
            ],
            editable: true
        });

        function reloadGrid() {
            gridUI.data("kendoGrid").dataSource.read();
            gridUI.data('kendoGrid').refresh();
        }

        gridUI.find(".k-grid-toolbar").on("click", ".k-grid-Reload", function (e) {
            e.preventDefault();
            reloadGrid();
        });
        //END::Load Request For Quotation Detail

        var formControll = function () {
            $('#btn-save').removeAttr('disabled').show();

            if (_detailID.val() !== '') {
                $('#btn-approval').removeAttr('disabled', '').show();
                $('#btn-remove_approval').attr('disabled', 'disabled').hide();
                $('#btn-submit').attr('disabled', 'disabled').hide();

                if ($('input[name="approval_id"]').val() !== '') {
                    $('#btn-approval').attr('disabled', 'disabled').hide();
                    $('#btn-remove_approval').removeAttr('disabled', '').show();

                    $('#btn-submit').removeAttr('disabled', '').show();
                }

            } else {
                $('#btn-approval').attr('disabled', 'disabled').hide();
                $('#btn-remove_approval').attr('disabled', 'disabled').hide();
                $('#btn-submit').attr('disabled', 'disabled').hide();
            }
            $('#tb').show();
        };

        var formControllSubmited = function (submited) {
            if (!submited) {
                $('#tb').show();
            }
            else {
                $('#tb').hide();
                $('#btn-save').attr('disabled', 'disabled').hide();
                $('#btn-approval').attr('disabled', 'disabled').hide();
                $('#btn-remove_approval').attr('disabled', 'disabled').hide();
                $('#btn-submit').attr('disabled', 'disabled').hide();
            }
        };

        var setStatus = function (state) {
            var output = '';
            switch (state) {
                case 'new':
                    output = `<span class="m-badge m-badge--info m-badge--wide">New</span>`;
                    break;
                case 'draft':
                    output = `<span class="m-badge m-badge--warning m-badge--wide">Draft</span>`;
                    break;
                case 'submited':
                    output = `<span class="m-badge m-badge--danger m-badge--wide">Submited</span>`;
                    break;
                case 'approved':
                    output = `<span class="m-badge m-badge--success m-badge--wide">Approved</span>`;
                    break;
            }
            $('#status').html(output);
        };

        $('#btn-save').click(function () {
            var btn = $(this);
            if (_formRequestForQuotation.valid()) {
                var requestForQuotation = _formRequestForQuotation.serializeToJSON();
                var detailTransactions = $("#grid").data("kendoGrid");
                var approverId = $('#approverId').val();
                var data =
                {
                    requestForQuotation: requestForQuotation,
                    details: detailTransactions.dataSource.view().toJSON()
                };

                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
                //console.log(data);
                //return;
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: 'application/json',
                    url: $.helper.resolveApi("~/finance/RequestForQuotation/saveasdraft"),
                    data: JSON.stringify(data),
                    success: function (r) {
                        if (r && r.status.success) {
                            console.log(r);
                            history.pushState('', 'ID', location.hash.split('?')[0] + '?id=' + r.data.recordID);
                            _detailID.val(r.data.recordID);
                            $('input[name="id"]').val(r.data.recordID);
                            $('input[name="request_for_quotation_number"]').val(r.data.request_for_quotation_number);
                            reloadGrid();
                            toastr.success(r.status.message, "Information");
                            setStatus('draft');
                        }
                        else {
                            swal(
                                'Saving',
                                r.status.message,
                                'info'
                            );
                        }
                    },
                    error: function (r) {
                        swal(
                            '' + r.status,
                            r.statusText,
                            'error'
                        );
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    }
                }).then(function () { btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false); });


            }

        });

        $('#btn-submit').click(function () {
            var btn = $(this);
            if (_formRequestForQuotation.valid()) {

                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: 'application/json',
                    url: $.helper.resolveApi("~/finance/RequestForQuotation/submited?RecordId=" + _detailID.val()),
                    //data: JSON.stringify(data),
                    success: function (r) {
                        if (r && r.status.success) {
                            //history.pushState('', 'ID', location.hash.split('?')[0] + '?id=' + r.data.recordID);
                            //_detailID.val(r.data.recordID);
                            //$('input[name="id"]').val(r.data.recordID);
                            btn.removeClass('m-loader m-loader--right m-loader--light');
                            formControllSubmited(true);
                            formControll();
                            reloadGrid();
                            setStatus('submited');
                            toastr.success(r.status.message, "Information");

                        }
                        else {
                            swal(
                                'Saving',
                                r.status.message,
                                'info'
                            );
                            btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                        }


                    },
                    error: function (r) {
                        swal(
                            '' + r.status,
                            r.statusText,
                            'error'
                        );
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    }
                });
            }

        });

        $('#btn-remove_approval').click(function () {
            var btn = $(this);
            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
            swal({
                title: "Are you sure reset approval template ?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                reverseButtons: !0
            }).then(function (e) {
                if (e.value) {
                    $.ajax({
                        type: "DELETE",
                        dataType: 'json',
                        url: $.helper.resolveApi("~/core/Approval/removeApprovalEntity/" + $('#entity_id').val() + "/?RecordID=" + _detailID.val() + "&ApprovalID=" + $('input[name="approval_id"]').val()),
                        success: function (r) {
                            if (r.status.success) {

                                toastr.success(r.status.message, "Information");
                                $('input[name="approval_id"]').val('');
                                formControll();

                            } else {
                                swal(
                                    'Information',
                                    r.status.message,
                                    'info'
                                );

                            }
                            btn.removeClass('m-loader m-loader--right m-loader--light');
                        },
                        error: function (e, t, s) {
                            swal(
                                'Information',
                                'Ooops, something went wrong !',
                                'info'
                            );
                            btn.removeClass('m-loader m-loader--right m-loader--light');
                        }
                    });
                }
            });

        });

        $('#btn-approval').click(function () {
            var url = $.helper.resolve("~/Core/ApprovalTemplate/SetupApproval/?EntityId=" + $('#entity_id').val() + "&RecordID=" + _detailID.val()
                + "&Title=Request For Quotation&Ref=" + $('input[name="request_for_quotation_number"]').val() + "&url=~/Finance/RequestForQuotationProject/Detail/?id=" + _detailID.val());
            window.location.href = url;
        });

        //BEGIN::EVENT BUTTON
        $('#btn-choose_business_unit').click(function () {
            var btn = $(this);
            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
            $('.modal-container').load($.helper.resolve("~/Core/BusinessUnit/Lookup/"), function (result) {
                var $modal = $('#m_modal_business_unit');

                $modal.modal({ show: true });
                btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                var $tree = $('#tree_businessunit');
                $modal.find('#btn-addselected').click(function () {
                    var selected = $tree.jstree("get_selected", true)[0];
                    $('input[name="business_unit_id"]').val(selected.id);
                    $('input[id="business_unit_id"]').val(selected.text);
                    $modal.modal('toggle');
                });
            });
        });
        //END::EVENT BUTTON

        return {
            init: function () {
                //initialize
                loadDetailData(),
                    dgOptions()
            }
        };
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery));