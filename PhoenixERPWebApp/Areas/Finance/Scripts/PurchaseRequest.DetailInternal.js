﻿function estimatePerPrice(dataItem) {

    return (dataItem.unit_price || 0).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") || 0;
}

function calculateAmount(dataItem) {
    dataItem.amount = (dataItem.unit_price * dataItem.exchange_rate) * dataItem.quantity;
    return (dataItem.amount || 0).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") || 0;
}

function itemCodeEditable(dataItem) {
    return dataItem.type_name == "Inventory" || dataItem.type_name == "Fixed Asset" ? true : false;
}
function itemUomEditable(dataItem) {
    return dataItem.type_name == "Inventory" ? true : false;
}
var webUrl = $.helper.resolve("~/Finance/PurchaseRequest");
var ajaxUrlGetTempateApprovalLink = $.helper.resolveApi("~/General/TmTemplateApproval/List");

(function ($) {
    'use strict';

    var asideLeftToggle = new mToggle('m_aside_left_minimize_toggle', {
        target: 'body',
        targetState: 'm-brand--minimize m-aside-left--minimize',
        togglerState: 'm-brand__toggler--active'
    });
    asideLeftToggle.toggleOn();
    var ListChecked = [];
    var gridUI = $('#grid');
    var pageFunction = function () {
        var _detailID = $('#detail-id');
        var $frmPurchaseRequest = $('#form-purchase_request');
        var $legal_entityLookup = $("#legal_entity_id");
        var $businessUnitLookup = $('#business_unit_id');

        var $rfq_date = $('#internal_rfq_date');
        var $delivery_date = $('#delivery_date');
        var $vendor_id = $('#vendor_id');
        var $currencyLookup = $("#currency_id");
        var $paymentTypeLookup = $("#payment_type_id");

        var $accountLookup = $("#account_id");

        $rfq_date.kendoDatePicker();
        $delivery_date.kendoDatePicker();

        //BEGIN::LOOKUP

        $paymentTypeLookup.kendoComboBox({
            placeholder: "Choose ..",
            dataTextField: "type_name",
            dataValueField: "id",
            filter: "contains",
            autoBind: false,
            delay: 1000,
            dataSource: {
                type: "json",
                pageSize: 100,
                serverPaging: true,
                serverFiltering: true,
                transport: {
                    read: {
                        url: $.helper.resolveApi("~/Finance/PaymentType/KendoLookup"),
                        contentType: 'application/json',
                        dataType: 'json',
                        type: 'POST',
                        cache: true
                    },
                    parameterMap: function (data, operation) {
                        var mapRequest = data;
                        return JSON.stringify(mapRequest);
                    }
                },
                schema: {
                    model: {
                        id: "id"
                    },
                    data: "items"
                }

            }
        });

        $currencyLookup.kendoComboBox({
            placeholder: "Choose ..",
            dataTextField: "currency_code",
            dataValueField: "id",
            dataSource: {
                type: "json",
                transport: {
                    read: {
                        url: $.helper.resolveApi("~/Core/Currency/lookup"),
                        contentType: 'application/json',
                        dataType: 'json',
                        type: 'GET',
                        cache: true
                    }
                },
                schema: {
                    model: {
                        id: "id"
                    },
                    data: "items",
                    total: "totalItems"
                },
            }
        });

        $vendor_id.kendoComboBox({
            placeholder: "Choose ..",
            dataTextField: "company_name",
            dataValueField: "id",
            filter: "contains",
            autoBind: false,
            delay: 1000,
            dataSource: {
                type: "json",
                pageSize: 100,
                serverPaging: true,
                serverFiltering: true,
                transport: {
                    read: {
                        url: $.helper.resolveApi("~/Finance/CompanyProfile/KendoLookup"),
                        contentType: 'application/json',
                        dataType: 'json',
                        type: 'POST',
                        cache: true
                    },
                    parameterMap: function (data, operation) {
                        var mapRequest = data;
                        return JSON.stringify(mapRequest);
                    }
                },
                schema: {
                    model: {
                        id: "id"
                    },
                    data: "items"
                }

            }
        });

        $legal_entityLookup.kendoComboBox({
            placeholder: "Choose ..",
            dataTextField: "legal_entity_name",
            dataValueField: "id",
            filter: "contains",
            autoBind: false,
            delay: 1000,
            dataSource: {
                type: "json",
                pageSize: 100,
                serverPaging: true,
                serverFiltering: true,
                transport: {
                    read: {
                        url: $.helper.resolveApi("~/core/LegalEntity/KendoLookup"),
                        contentType: 'application/json',
                        dataType: 'json',
                        type: 'POST',
                        cache: true
                    },
                    parameterMap: function (data, operation) {
                        var mapRequest = data;
                        console.log(data);
                        return JSON.stringify(mapRequest);
                    }
                },
                schema: {
                    model: {
                        id: "id"
                    },
                    data: "items"
                }

            }
        });

        $businessUnitLookup.kendoDropDownTree({
            autoBind: true,
            placeholder: "Choose ..",
            dataTextField: "text",
            dataValueField: "id",
            dataSource: {
                type: "json",
                transport: {
                    read: {
                        url: $.helper.resolveApi("~/core/BusinessUnit/getBusinessUnitTree"),
                        contentType: 'application/json',
                        dataType: 'json',
                        type: 'GET',
                        cache: true
                    },
                    parameterMap: function (data) {
                        return data;
                    }
                },
                schema: {
                    model: {
                        id: "id",
                        children: "items"
                    },
                    data: "data"
                },
            },
            width: "300px",
            select: function (e) {
                try {
                    var data = $businessUnitLookup.data('kendoDropDownTree').dataItem(e.node)._childrenOptions.data;
                    console.log(data);
                    $('input[name=affiliation_id]').val(data.affiliation_id || null);
                    $('.affiliation-name').text(data.affiliation_name || 'N/A');
                } catch (e) {
                    console.log(e);
                }
            }
        });

        $accountLookup.cmSelect2({
            url: $.helper.resolveApi("~/Finance/Account/lookup"),
            filters: ' r.account_name LIKE @0 OR r.account_number LIKE @0 ',
            //paramaters: [],
            result: {
                id: 'id',
                text: 'account_name'
            }
        });

        //END::LOOKUP
        //BEGIN::Form Validation
        $frmPurchaseRequest.validate({
            rules: {
                legal_entity_id: {
                    required: true
                }
            },
            invalidHandler: function (e, r) {
                var errors = r.numberOfInvalids();
                if (errors) {
                    var message = errors === 1
                        ? 'Please correct the following error:\n'
                        : 'Please correct the following ' + errors + ' errors.\n';

                    if (r.errorList.length > 0) {
                        for (var x = 0; x < r.errorList.length; x++) {
                            console.warn(r.errorList[x]);
                            errors += "\n\u25CF " + r.errorList[x].message;
                        }
                    }
                }
                mUtil.scrollTo($frmPurchaseRequest, -200);
            },
            submitHandler: function (e) { }
        });
        ////END::Form Validation

        var loadDetailTransaction = function () {
            $("#grid").html('');
            $.helper.kendoUI.grid($("#grid"), {
                options: {
                    url: $.helper.resolveApi('~/finance/PurchaseRequestDetail/KendoGrid?purchaseRequestId=' + _detailID.val())
                },
                navigatable: true,
                pageable: false,
                resizable: true,
                groupable: false,
                toolbar: ["create"],
                save: function (e) {
                    var grid = this;
                    setTimeout(function () {
                        grid.refresh();
                    })
                },
                serverOperation: false,
                dataSource: {
                    type: "json",
                    transport: {
                        //update: false,
                        update: function (options) {
                            options.success();
                        }
                    },
                    pageSize: 1000,
                    serverPaging: true,
                    serverSorting: false,
                    batch: true,
                    serverOperation: false,
                    aggregate: [
                        { field: "amount", aggregate: "sum" }
                    ],
                    schema: {
                        model: {
                            id: "id",
                            fields: {
                                id: { editable: true, nullable: true },
                                item_name: { editable: true, nullable: true },
                                type_name: { editable: true, validation: { required: true } },
                                item_code: { editable: true },
                                item_type_id: { editable: true, validation: { required: true } },
                                exchange_rate: { type: "number", defaultValue: 1, validation: { required: true, min: 1 } },
                                currency_code: { editable: true, validation: { required: true }, defaultValue: "IDR" },
                                currency_id: { editable: true, validation: { required: true }, defaultValue: "46e4d57e-5ce2-4c31-82be-f81382dc243a" },
                                quantity: { type: "number", defaultValue: 1, validation: { required: true, min: 1 } },
                                amount: { type: "number", defaultValue: 0 },
                                uom_id: { editable: true, validation: { required: true }, defaultValue: "62919a04-5f2e-44d4-89a6-c831dd3c32e4" },
                                uom_name: { editable: true, validation: { required: true }, defaultValue: "unit" }
                            }
                        },
                        data: "data",
                        total: "recordsTotal"
                    },
                },
                schema: {
                    model: {
                        id: "id"
                    },
                    data: "data",
                    total: "recordsTotal"
                },
                aggregate: [
                    { field: "amount", aggregate: "sum" }
                ],
                marvelCheckboxSelectable: {
                    enable: true,
                    listCheckedTemp: ListChecked
                },
                dataBound: function (e) {
                    var data = e.sender.dataSource.data().toJSON();
                    var TotalAmount = 0;
                    $.each(data, function (i, v) {
                        TotalAmount += v.amount || 0;
                    });
                    var footerAmount = $('span.totalAmount');
                    $(footerAmount).html("Rp. " + TotalAmount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                },
                columns: [
                    {
                        command: [
                            {
                                name: "rowDelete", text: "<i class='fa 	fa-trash'></i>", click: function (e) {
                                    e.preventDefault();
                                    var row = e.target.closest('tr')
                                    var uid = $(row).data(uid);
                                    var dataSource = gridUI.data("kendoGrid").dataSource;
                                    var item = dataSource.getByUid(uid.uid);
                                    if (item.id) DeleteData([item.id]);
                                    else {
                                        dataSource.cancelChanges(item);
                                    }
                                }
                            }
                        ], title: "&nbsp;", width: "100px",
                        locked: true,
                        lockable: false,
                        groupable: true,
                        sortable: false
                    },
                    {
                        field: "type_name",
                        title: "Budget Code",
                        width: "200px",
                        editor: budgetCodeEditor
                    },
                    {
                        template: "<b>#= data.type_name ? type_name : ''#</b>",
                        field: "type_name",
                        title: "Type",
                        width: "200px",
                        editor: itemTypeEditor
                    },
                    {
                        field: "item_code",
                        title: "Item Code",
                        width: "150px",
                        editable: itemCodeEditable,
                        editor: itemEditor
                    },
                    {
                        field: "item_name",
                        title: "Item Name",
                        width: "150px"
                    },
                    {
                        field: "description",
                        title: "Description",
                        width: "300px",
                        attributes: {
                            style: "white-space: pre-line;"
                        },
                        editor: function (container, options) {
                            $('<textarea name="' + options.field + '" style="width: ' + container.width() + 'px;height:' + container.height() + 'px" />')
                                .appendTo(container);
                        }
                    },
                    {
                        field: "uom_name",
                        title: "UOM",
                        width: "100px",
                        editable: itemUomEditable,
                        editor: uomEditor
                    },
                    {
                        field: "currency_id",
                        title: "Currency",
                        attributes: {
                            style: "color:red;"
                        },
                        template: "#= currency_code #",
                        width: "100px",
                        editor: currencyEditor
                    },
                    {
                        field: "exchange_rate",
                        title: "Exchange Rate",
                        attributes: {
                            style: "text-align:right;"
                        },
                        template: "#= kendo.toString(data.exchange_rate, 'n') #",
                        width: "120px"
                    },
                    {
                        field: "unit_price",
                        title: "Estimated Price Per Quantity",
                        attributes: {
                            style: "text-align:right;"
                        },
                        template: "<b>#= estimatePerPrice(data) #</b>",
                        width: "150px"
                    },
                    {
                        field: "quantity",
                        title: "Qty",
                        width: "80px"
                    },
                    {
                        field: "amount",
                        title: "Amount",
                        editable: function () { return false },
                        attributes: {
                            style: "text-align:right;"
                        },
                        footerAttributes: {
                            style: "text-align: right"
                        },
                        template: "<b>#= calculateAmount(data) #</b>",
                        footerTemplate: "<span style='text-align:right;' class='totalAmount'></span>",
                        width: "150px"
                    }
                ],
                editable: {
                    mode: "incell",
                    createAt: "bottom"
                }
            });
        }

        var itemTypeDataSource = new kendo.data.DataSource({
            type: "json",
            pageSize: 100,
            serverPaging: true,
            serverFiltering: true,
            transport: {
                read: {
                    url: $.helper.resolveApi("~/Core/ItemType/KendoLookup"),
                    contentType: 'application/json',
                    dataType: 'json',
                    type: 'POST',
                    cache: true
                },
                parameterMap: function (data) {
                    var mapRequest = data;
                    return JSON.stringify(mapRequest);
                }
            },
            schema: {
                model: {
                    id: "id"
                },
                data: "items"
            },
        });

        itemTypeDataSource.read();

        var itemDataSource = new kendo.data.DataSource({
            type: "json",
            pageSize: 100,
            serverPaging: true,
            serverFiltering: true,
            transport: {
                read: {
                    url: $.helper.resolveApi("~/core/Item/kendoLookup"),
                    contentType: 'application/json',
                    dataType: 'json',
                    type: 'POST',
                    cache: true
                },
                parameterMap: function (data) {
                    var mapRequest = data;
                    return JSON.stringify(mapRequest);
                }
            },
            schema: {
                model: {
                    id: "id"
                },
                data: "items",
                total: "totalItems"
            },
        });

        function itemEditor(container, options) {
            if (options.model.type_name.toUpperCase() == "INVENTORY") {
                itemDataSource.filter(
                    {
                        field: "type_name",
                        operator: "eq",
                        value: "INVENTORY"
                    }
                );
            } else if (options.model.type_name.toUpperCase() == "FIXED ASSET") {
                itemDataSource.filter(
                    {
                        field: "type_name",
                        operator: "eq",
                        value: "FIXED ASSET"
                    }
                );
            }

            $('<input required name="' + options.field + '"/>')
                .appendTo(container)
                .kendoMultiColumnComboBox({
                    autoBind: false,
                    filter: "contains",
                    delay: 500,
                    dataTextField: "item_code",
                    dataValueField: "id",
                    value: options.model.item_id,
                    text: options.model.item_code,
                    height: 550,
                    columns: [
                        { field: "item_code", title: "Item Code", width: 200 },
                        { field: "item_name", title: "Item Name", width: 200 }
                    ],
                    dataSource: itemDataSource,
                    change: function (e) {
                        var equipmentModel = this.dataItem(e.item);
                        if (equipmentModel) {
                            options.model.item_id = equipmentModel.id;
                            options.model.uom_id = equipmentModel.uom_id;
                            options.model.uom_name = equipmentModel.uom_name;
                            options.model.set("item_code", equipmentModel.item_code);
                            options.model.set("item_name", equipmentModel.item_name);
                        }
                    }
                });
            var tooltipElement = $('<span class="k-invalid-msg" data-for="' + options.field + '"></span>');
            tooltipElement.appendTo(container);
        }

        function budgetCodeEditor(container, options) {
            $('<input required name="' + options.field + '" width="250px"/>')
                .appendTo(container)
                .kendoComboBox({
                    placeholder: "Choose ..",
                    filter: "contains",
                    autoBind: false,
                    dataTextField: "Text",
                    dataValueField: "ID",
                    dataSource: [
                        { "ID": 1, "Text": '52115A' },
                        { "ID": 2, "Text": '52115B' },
                        { "ID": 3, "Text": '52115C' },
                        { "ID": 4, "Text": '52115D' },
                        { "ID": 5, "Text": '52115E' },
                    ]
                });
            var tooltipElement = $('<span class="k-invalid-msg" data-for="' + options.field + '" ></span>');
            tooltipElement.appendTo(container);
        }

        function itemTypeEditor(container, options) {
            $('<input required name="' + options.field + '" width="250px"/>')
                .appendTo(container)
                .kendoComboBox({
                    placeholder: "Choose ..",
                    filter: "contains",
                    autoBind: false,
                    dataTextField: "type_name",
                    dataValueField: "type_name",
                    value: options.model.item_type_id,
                    text: options.model.type_name,
                    dataSource: itemTypeDataSource,
                    change: function (e) {

                        var equipmentModel = this.dataItem(e.item);
                        options.model.item_id = null;
                        options.model.item_code = null;
                        options.model.item_name = null;

                        if (equipmentModel) {
                            //reset all row related
                            options.model.item_type_id = equipmentModel.id;
                            options.model.type_name = equipmentModel.type_name;
                            //set unit nya belum
                        }
                    }
                });
            var tooltipElement = $('<span class="k-invalid-msg" data-for="' + options.field + '" ></span>');
            tooltipElement.appendTo(container);
        }

        var currencyDataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: {
                    url: $.helper.resolveApi("~/Core/Currency/lookup"),
                    contentType: 'application/json',
                    dataType: 'json',
                    type: 'GET',
                    cache: true
                },
                parameterMap: function (data) {
                    var mapRequest = data;
                    var extractOption = {
                        filters: " (r.currency_name like @0 or r.currency_code like @0 )",
                        parameters: '%%'
                    };
                    mapRequest = $.extend(true, mapRequest, extractOption);
                    return mapRequest;

                }
            },
            schema: {
                model: {
                    id: "id"
                },
                data: "items",
                total: "totalItems"
            },
        });
        currencyDataSource.read();
        function currencyEditor(container, options) {
            $('<input required name="' + options.field + '"/>')
                .appendTo(container)
                .kendoComboBox({
                    autoBind: false,
                    dataTextField: "currency_code",
                    dataValueField: "currency_id",
                    value: options.model.currency_id,
                    text: options.model.currency_code,
                    dataSource: currencyDataSource,
                    change: function (e) {
                        var equipmentModel = this.dataItem(e.item);
                        try {
                            options.model.currency_id = equipmentModel.id;
                            options.model.set("currency_code", equipmentModel.currency_code);
                        } catch (e) {

                        }

                    }
                });

            console.log(options.model);
            var tooltipElement = $('<span class="k-invalid-msg" data-for="' + options.field + '" ></span>');
            tooltipElement.appendTo(container);
        }
        var uomDataSource = new kendo.data.DataSource({
            type: "json",
            pageSize: 100,
            serverPaging: true,
            serverFiltering: true,
            transport: {
                read: {
                    url: $.helper.resolveApi("~/core/Uom/KendoLookup"),
                    contentType: 'application/json',
                    dataType: 'json',
                    type: 'POST'
                },
                parameterMap: function (data, operation) {
                    var mapRequest = data;
                    return JSON.stringify(mapRequest);
                }
            },
            schema: {
                model: {
                    id: "id"
                },
                data: "items"
            }
        });
        //uomDataSource.read();
        function uomEditor(container, options) {
            $('<input name="' + options.field + '"/>')
                .appendTo(container)
                .kendoComboBox({
                    autoBind: false,
                    filter: "contains",
                    dataTextField: "uom_name",
                    dataValueField: "uom_name",
                    value: options.model.uom_id,
                    text: options.model.uom_name,
                    dataSource: uomDataSource,
                    change: function (e) {
                        var equipmentModel = this.dataItem(e.item);
                        try {
                            options.model.uom_id = equipmentModel.id;
                            options.model.set("uom_name", equipmentModel.uom_name);
                        } catch (e) {

                        }

                    }
                });
        }

        function reloadGrid() {
            gridUI.data("kendoGrid").dataSource.read();
            gridUI.data('kendoGrid').refresh();
        }

        gridUI.find(".k-grid-toolbar").on("click", ".k-grid-Reload", function (e) {
            e.preventDefault();
            reloadGrid();
        });

        function AddRowTable(no, title, nama) {
            var acak = randomIntFromInterval(1, 999);
            var markup = "<tr id=" + acak + "><td>" + no + "</td><td>" + title + "</td><td>" + nama + "</td></tr>";
            $("#PurchaseRequestListApprover tbody").append(markup);
        }

        function Deleteaja(id) {
            $('table#PurchaseRequestListApprover tr#' + id + '').remove();
        }

        function randomIntFromInterval(min, max) // min and max included
        {
            return Math.floor(Math.random() * (max - min + 1) + min);
        }

        function getApproval() {
            var data = { Search_MenuName: "Purchase Request", Search_TemplateName: "Purchase Request Budgeted" };
            $.ajax({
                type: "GET",
                dataType: 'json',
                url: ajaxUrlGetTempateApprovalLink,
                data: data,
                success: function (response) {


                    Deleteaja('no_data');
                    var count = 1;
                    $("#PurchaseRequestListApprover > tbody").html("");
                    $.each(response.rows[0].listTmUserApproval, function (index, value) {
                        AddRowTable(count, value.job_title_name, value.employee_full_name);
                        count = count + 1;
                    });
                    $("#template_approval_id").val(response.rows[0].id);
                },
                error: function (e, t, s) {

                    swal(
                        'Information',
                        'Ooops, something went wrong !',
                        'info'
                    );
                }
            });

            //ajaxGet(ajaxUrlGetTempateApprovalLink, { Search_MenuName: "Purchase Request", Search_TemplateName: "Purchase Request Budgeted" }, function (response) {

            //    response = parseJson(response);
            //    Deleteaja('no_data');
            //    var count = 1;
            //    $("#PurchaseRequestListApprover > tbody").html("");
            //    $.each(response.rows[0].listTmUserApproval, function (index, value) {
            //        AddRowTable(count, value.job_title_name, value.employee_full_name);
            //        count = count + 1;
            //    });
            //    $("#template_approval_id").val(response.rows[0].id);

            //});
        }

        //BEGIN::Load Detail
        var loadDetailData = function () {

            if (_detailID.val() !== '') {
                mApp.blockPage({
                    overlayColor: "#000000",
                    type: "loader",
                    state: "primary",
                    message: "Processing..."
                }),
                    $.ajax({
                        type: "GET",
                        dataType: 'json',
                        url: $.helper.resolveApi("~/Finance/PurchaseRequest/" + _detailID.val() + "/details"),
                        success: function (r) {
                            if (r.status.success && r.data) {
                                var record = r.data;
                                _detailID.val(record.id);
                                $('input[name=id]').val(record.id);
                                $('input[name="request_datetime"]').data("kendoDatePicker").value(moment(record.request_datetime).format('MM/DD/YYYY'));
                                $('#remarks').val(record.remarks);
                                $('#request_number').text(record.request_number);
                                $('input[name="request_number"]').val(record.request_number);
                                $.helper.kendoUI.combobox.setValue($legal_entityLookup, { id: record.legal_entity_id, legal_entity_name: record.legal_entity_name });
                                $.helper.kendoUI.dropDownTree.setValue($businessUnitLookup, { id: record.business_unit_id, text: record.unit_name });

                                if (record.affiliation_id != null) {
                                    $('input[name=affiliation_id]').val(record.affiliation_id || '');
                                }
                                $('.affiliation-name').text(record.affiliation_name || 'N/A');


                            } else {
                                swal(
                                    'Information',
                                    r.status.message,
                                    'info'
                                );
                            }
                        },
                        error: function (e, t, s) {
                            swal(
                                'Information',
                                'Ooops, something went wrong !',
                                'info'
                            );
                        }
                    }).then(setTimeout(function () {
                        mApp.unblockPage();
                    }, 2e3));

            }

        };
        //END::Load Detail

        //BEGIN::EVENT BUTTON
        $('#btn-save').click(function (e) {
            var btn = $(this);
            debugger;
            if ($frmPurchaseRequest.valid()) {
                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
                var header = $frmPurchaseRequest.serializeToJSON();
                var transactionDetailKendo = $("#grid").data("kendoGrid");
                var data = {
                    purchase_request: header,
                    purchase_request_details: transactionDetailKendo.dataSource.data().toJSON()
                };
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: 'application/json',
                    url: $.helper.resolveApi("~/Finance/PurchaseRequest/saveasdraft"),
                    data: JSON.stringify(data),
                    success: function (r) {
                        if (r && r.status.success) {
                            history.pushState('', 'ID', location.hash.split('?')[0] + '?id=' + r.data.recordID);
                            _detailID.val(r.data.recordID);
                            $('input[name=id]').val(r.data.recordID);

                            //loadDetailData(),
                            //loadDetailTransaction(),
                            //gridUI.data('kendoGrid').dataSource.read(),
                            toastr.success(r.status.message, "Information"),
                                btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                            //window.location.href = webUrl;
                            // redirecttolink(webUrl);

                            var data = {
                                refId: response.data.id,
                                detailLink: "/Finance/PurchaseRequest/Approval/" + r.data.recordID,
                                tName: "fn.purchase_request",
                                menuId: "vo13ff0d-6d9c-461e-9b21-ee42f2543212",
                                IdTemplate: $("#template_approval_id").val()
                            };
                            saveTemplateApprovalHris(data, webUrl);
                        }
                        else {
                            swal(
                                'Saving',
                                r.status.message,
                                'info'
                            );
                            btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                        }

                    },
                    error: function (r) {
                        swal(
                            '' + r.status,
                            r.statusText,
                            'error'
                        );
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    }
                });

            }
        });
        //END::EVENT BUTTON

        var DeleteData = function (ArrID) {
            swal({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                reverseButtons: !0
            }).then(function (e) {
                if (e.value) {
                    mApp.block("#m_blockui_list", {
                        overlayColor: "#000000",
                        type: "loader",
                        state: "primary",
                        message: "Processing..."
                    });

                    $.ajax({
                        type: "DELETE",
                        dataType: 'json',
                        contentType: 'application/json',
                        url: $.helper.resolveApi("~/Finance/PurchaseRequestDetail/delete"),
                        data: JSON.stringify(ArrID),
                        success: function (r) {
                            if (r.status.success) {
                                swal({
                                    title: 'Deleted!',
                                    text: r.status.message,
                                    type: "success"
                                });

                            } else {
                                swal(
                                    'Information',
                                    r.status.message,
                                    'info'
                                );
                            }
                            reloadGrid();
                        },
                        error: function (e, t, s) {
                            swal(
                                'Information',
                                'Ooops, something went wrong !',
                                'info'
                            );
                        }
                    }).then(setTimeout(function () {
                        mApp.unblock("#m_blockui_list");
                    }, 2e3));
                }


            });
        };

        return {
            init: function () {
                //initialize
                loadDetailData();
                loadDetailTransaction();
                getApproval();
            }
        };
    }();

    $(document).ready(function () {


        pageFunction.init();

        //console.log(pageFunction.init());
    });
}(jQuery));
