﻿(function ($) {
	'use strict';
	var pageFunction = (function (pageFunction) {
		pageFunction.init = function () {
			//initialize
        };
        pageFunction.save = function () {
            callActionFile({ formId: "form", title: "save", type: "POST", url: param.saveUrl });
        };
        pageFunction.submit = function () {
            callActionFile({ formId: "form", title: "submit", type: "POST", url: param.submitUrl });
        };
		return pageFunction;
	})(pageFunction || {});

	$(document).ready(function () {
        pageFunction.init();
        $('#btn-save').click(pageFunction.save);
        $('#btn-submit').click(pageFunction.submit);

        if ($("#ClientId").val() !== "") {
            $.ajax({
                type: "POST",
                url: param.getClientData,
                data: { clientId: $("#ClientId").val() },
                success: function (data) {
                    $("#VendorName").val(data.CompanyName);
                    $("#VendorNPWP").val(data.TaxRegistrationNumber);
                    $("#VendorAddress").val(data.Address);
                    $("#VendorCity").val(data.City);
                    $("#VendorZipCode").val(data.ZipCode);
                    $("#VendorContactPerson").val(data.ContactName);
                    $("#VendorContactNumber").val(data.Phone);
                }
            });
        }
    });
}(jQuery));