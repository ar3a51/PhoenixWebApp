﻿function getPrice(dataItem) {
    return (dataItem.unit_price * dataItem.exchange_rate);
}

function calculateAmount(dataItem) {
    var price = getPrice(dataItem);
    dataItem.amount = price * dataItem.qty;
    return dataItem.amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") || 0;
}

function itemCodeEditable(dataItem) {
    return dataItem.type_name == "Inventory" ? true : false;
}

function itemUomEditable(dataItem) {
    return dataItem.type_name == "Inventory" ? false : true;
}

(function ($) {
    'use strict';
    //console.log($.helper.getTokenData());
    var _detailID = $('#detail-id');
    var gridUI = $('#grid');

    var pageFunction = function () {
        
        var _formRequestForQuotation = $('#form-RequestForQuotation');
       
        var $businessUnitLabel = $("#business_unit_id");
        var $legalEntityLabel = $("#legal_entity_id");
        var $affiliationLabel = $("#affiliation_id");
        var $companyLookup = $("#company_id");
        var $currencyLookup = $("#currency_id");
        var $paymentTypeLookup = $("#payment_type_id");


        //BEGIN::Form Validation
        _formRequestForQuotation.validate({
            rules: {
                purchase_order_date: {
                    required: true
                },
                delivery_date: {
                    required: true
                },
                company_id: {
                    required: true
                },
                business_unit_id: {
                    required: true
                },
                legal_entity_id: {
                    required: true
                },
                affiliation_id: {
                    required: true
                },
                exchange_rate: {
                    required: true,
                    min: 1
                },
                payment_type_id: {
                    required: true
                },
                term_of_payment: {
                    required: true,
                    min: 1
                }
            },
            invalidHandler: function (e, r) {
                mUtil.scrollTo(_formRequestForQuotation, -200);
            },
            submitHandler: function (e) { }
        });
        ////END::Form Validation



        //BEGIN::Lookup
        $companyLookup.kendoMultiSelect({
            placeholder: "Choose ..",
            dataTextField: "company_name",
            dataValueField: "id",
            filter: "contains",
            delay: 1000,
            autoBind: false,
            maxSelectedItems: 1,
            dataSource: {
                type: "json",
                pageSize: 100,
                serverPaging: true,
                serverFiltering: true,
                //filter: { field: "is_vendor", operator: "eq", value: true },
                transport: {
                    read: {
                        url: $.helper.resolveApi("~/finance/CompanyProfile/KendoLookup"),
                        contentType: 'application/json',
                        dataType: 'json',
                        type: 'POST',
                        cache: true
                    },
                    parameterMap: function (data, operation) {
                        var mapRequest = data;
                        return JSON.stringify(mapRequest);
                    }
                },
                schema: {
                    data: "items"
                }
            }
        });

        var companyMultiSelect = $companyLookup.data("kendoMultiSelect"),
            setValuecompanyMultiSelect = function (val) {
                if (val == null) return;
                companyMultiSelect.dataSource.filter({}); //clear applied filter before setting value
                companyMultiSelect.value(val);
            };


        $paymentTypeLookup.kendoComboBox({
            placeholder: "Choose ..",
            dataTextField: "type_name",
            dataValueField: "id",
            filter: "contains",
            autoBind: false,
            delay: 1000,
            dataSource: {
                type: "json",
                pageSize: 100,
                serverPaging: true,
                serverFiltering: true,
                transport: {
                    read: {
                        url: $.helper.resolveApi("~/Finance/PaymentType/KendoLookup"),
                        contentType: 'application/json',
                        dataType: 'json',
                        type: 'POST',
                        cache: true
                    },
                    parameterMap: function (data, operation) {
                        var mapRequest = data;
                        return JSON.stringify(mapRequest);
                    }
                },
                schema: {
                    model: {
                        id: "id"
                    },
                    data: "items"
                }

            }
        });

        $currencyLookup.kendoDropDownList({
            dataTextField: "currency_code",
            dataValueField: "id",
            dataSource: {
                type: "json",
                transport: {
                    read: {
                        url: $.helper.resolveApi("~/Core/Currency/lookup"),
                        contentType: 'application/json',
                        dataType: 'json',
                        type: 'GET',
                        cache: true
                    }
                },
                schema: {
                    model: {
                        id: "id"
                    },
                    data: "items",
                    total: "totalItems"
                },
            },
            change: function (e) {
                var equipmentModel = this.dataItem(e.item);
                try {
                    options.model.currency_id = equipmentModel.id;
                    options.model.set("currency_code", equipmentModel.currency_code);
                } catch (e) {

                }

            }
        });

        //END: Lookup


        $('input[type=radio][name=is_prefered_vendor]').on('change', function (e) {

            if ($(this).val() == "true") {
                companyMultiSelect.setOptions({
                    maxSelectedItems: 1
                });
            } else {
                companyMultiSelect.setOptions({
                    maxSelectedItems: 3
                });
            }
        });

        //BEGIN::Load Datatable
        var loadDetailData = function () {
            if (_detailID.val() !== '') {
                mApp.blockPage({
                    overlayColor: "#000000",
                    type: "loader",
                    state: "primary",
                    message: "Processing..."
                }),
                    $.ajax({
                        type: "GET",
                        dataType: 'json',
                        url: $.helper.resolveApi("~/finance/RequestForQuotation/" + _detailID.val() + "/details"),
                        success: function (r) {
                            if (r.status.success && r.data) {
                                var record = r.data;
                                var vendor = r.data.vendor;
                                $('input[name="approval_id"]').val(record.approval_id);
                                $('input[name="request_for_quotation_number"]').val(record.request_for_quotation_number);
                                $('input[name="exchange_rate"]').val(record.exchange_rate);
                                $('input[id="term_of_payment"]').val(record.term_of_payment);
                                $('input[name="delivery_place"]').val(record.delivery_place);
                                if (record.delivery_date != null) $('#delivery_date').datepicker("setDate", moment(record.delivery_date).format("MM/DD/YYYY"));
                                if (record.request_for_quotation_date != null) $('#request_for_quotation_date').datepicker("setDate", moment(record.request_for_quotation_date).format("MM/DD/YYYY"));

                                if (record.is_prefered_vendor == null || record.is_prefered_vendor == false) {
                                    companyMultiSelect.setOptions({
                                        maxSelectedItems: 3
                                    });
                                    $("input[name=is_prefered_vendor][value='false']").prop("checked", true);
                                } else {
                                    companyMultiSelect.setOptions({
                                        maxSelectedItems: 1
                                    });
                                    $("input[name=is_prefered_vendor][value='true']").prop("checked", true);
                                }
                                $currencyLookup.data("kendoDropDownList").value(record.currency_id);

                                $.helper.kendoUI.multiSelect.setValue($companyLookup, vendor);
                                $businessUnitLabel.text(record.business_unit_name || "N/A");
                                $legalEntityLabel.text(record.legal_entity_name || "N/A");
                                $affiliationLabel.text(record.affiliation_name || "N/A");

                                $.helper.kendoUI.combobox.setValue($paymentTypeLookup, { id: record.payment_type_id, type_name: record.payment_type_name });

                                formControll();
                                if (record.is_locked) {
                                    formControllSubmited(true);
                                    setStatus('submited');
                                } else {
                                    setStatus('draft');
                                }

                            } else {
                                swal(
                                    'Information',
                                    r.status.message,
                                    'info'
                                );
                            }
                        },
                        error: function (e, t, s) {
                            swal(
                                'Information',
                                'Ooops, something went wrong !',
                                'info'
                            );
                        }
                    }).then(setTimeout(function () {
                        mApp.unblockPage();
                    }, 2e3));
            } else {
                formControll();
                setStatus('new');
            }

        };
        //END::Load Datatable

        //BEGIN::DataSource
        var uomDataSource = new kendo.data.DataSource({
            type: "json",
            pageSize: 100,
            serverPaging: true,
            serverFiltering: true,
            transport: {
                read: {
                    url: $.helper.resolveApi("~/core/Uom/KendoLookup"),
                    contentType: 'application/json',
                    dataType: 'json',
                    type: 'POST'
                },
                parameterMap: function (data, operation) {
                    var mapRequest = data;

                    return JSON.stringify(mapRequest);
                }
            },
            schema: {
                model: {
                    id: "id"
                },
                data: "items"
            }
        });

        uomDataSource.read();

        var itemDataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: {
                    url: $.helper.resolveApi("~/core/Item/lookup"),
                    contentType: 'application/json',
                    dataType: 'json',
                    type: 'GET',
                    cache: true
                },
                parameterMap: function (data) {
                    var mapRequest = data;
                    var extractOption = {
                        filters: " ( r.item_code like @0 )",
                        parameters: '%%'
                    };
                    mapRequest = $.extend(true, mapRequest, extractOption);
                    return mapRequest;

                }
            },
            schema: {
                model: {
                    id: "id"
                },
                data: "items",
                total: "totalItems"
            },
        });

        itemDataSource.read();

        var itemTypeDataSource = new kendo.data.DataSource({
            type: "json",
            pageSize: 100,
            serverPaging: true,
            serverFiltering: true,
            transport: {
                read: {
                    url: $.helper.resolveApi("~/Core/ItemType/KendoLookup"),
                    contentType: 'application/json',
                    dataType: 'json',
                    type: 'POST',
                    cache: true
                },
                parameterMap: function (data) {
                    var mapRequest = data;
                    return JSON.stringify(mapRequest);
                }
            },
            schema: {
                model: {
                    id: "id"
                },
                data: "items"
            },
        });
        itemTypeDataSource.read();
        //END::Datasource

        //BEGIN::Editor
        function itemEditor(container, options) {
            $('<input required name="' + options.field + '"/>')
                .appendTo(container)
                .kendoMultiColumnComboBox({
                    autoBind: false,
                    dataTextField: "item_code",
                    dataValueField: "id",
                    value: options.model.item_id,
                    text: options.model.item_code,
                    height: 550,
                    columns: [
                        { field: "item_code", title: "Item Code", width: 200 },
                        { field: "item_name", title: "Item Name", width: 200 }
                    ],
                    dataSource: itemDataSource,
                    change: function (e) {

                        var equipmentModel = this.dataItem(e.item);
                        options.model.item_id = equipmentModel.id;
                        options.model.uom_id = equipmentModel.uom_id;
                        options.model.set("item_code", equipmentModel.item_code);
                        options.model.set("item_name", equipmentModel.item_name);
                        options.model.set("uom_name", equipmentModel.uom_name);
                    }
                });
            var tooltipElement = $('<span class="k-invalid-msg" data-for="' + options.field + '"></span>');
            tooltipElement.appendTo(container);
        }

        function itemTypeEditor(container, options) {
            
            $('<input required name="' + options.field + '" width="250px"/>')
                .appendTo(container)
                .kendoComboBox({
                    placeholder: "Choose ..",
                    filter: "contains",
                    autoBind: false,
                    dataTextField: "type_name",
                    dataValueField: "type_name",
                    value: options.model.item_type_id,
                    text: options.model.type_name,
                    dataSource: itemTypeDataSource,
                    change: function (e) {
                        var equipmentModel = this.dataItem(e.item);
                        options.model.item_type = equipmentModel.id;
                        options.model.set("type_name", equipmentModel.type_name);
                        if (equipmentModel.type_name != "Inventory") {
                            options.model.item_code = null;
                            options.model.item_name = null;
                            options.model.uom_name = null;
                        }
                    }
                });
            var tooltipElement = $('<span class="k-invalid-msg" data-for="' + options.field + '" ></span>');
            tooltipElement.appendTo(container);
        }

        function uomEditor(container, options) {
            $('<input required name="' + options.field + '"/>')
                .appendTo(container)
                .kendoComboBox({
                    autoBind: false,
                    filter: "contains",
                    dataTextField: "uom_name",
                    dataValueField: "uom_name",
                    value: options.model.uom_id,
                    text: options.model.uom_name,
                    dataSource: uomDataSource,
                    change: function (e) {
                        var equipmentModel = this.dataItem(e.item);
                        options.model.uom_id = equipmentModel.id;
                        options.model.set("uom_name", equipmentModel.uom_name);
                    }
                });
            var tooltipElement = $('<span class="k-invalid-msg" data-for="' + options.field + '" ></span>');
            tooltipElement.appendTo(container);
        }
        //END::Editor

        //BEGIN::Request For Quotation Detail

        $.helper.kendoUI.grid($("#grid"), {
            options: {
                url: $.helper.resolveApi('~/finance/RequestForQuotationDetail/KendoGrid?requestforquotationid=' + _detailID.val())
            },
            navigatable: true,
            pageable: false,
            //detailTemplate: kendo.template($("#template").html()),
            //detailInit: detailInit,
            resizable: true,
            save: function () {
                var grid = this;
                setTimeout(function () {
                    grid.refresh();
                })
            },
            dataSource: {
                group: { field: "type_name", aggregates: [{ field: "type_name", aggregate: "count" }] },
                pageSize: 1000,
                serverPaging: true,
                serverSorting: false,
                batch: true,
                serverOperation: false,
                aggregate: [
                    { field: "amount", aggregate: "sum" },
                ]
            },
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        id: { editable: true, nullable: true },
                        uom_name: { editable: true },
                        item_code: { editable: true }
                    }
                },
                data: "data",
                total: "recordsTotal"
            },

            aggregate: [
                { field: "amount", aggregate: "sum" },
            ],

            marvelCheckboxSelectable: {
                enable: false
            },

            dataBound: function (e) {
                this.expandRow(this.tbody.find("tr.k-master-row").first());

                var data = e.sender.dataSource.data().toJSON();
                var TotalAmount = 0;
                $.each(data, function (i, v) {
                    TotalAmount += v.amount;
                });
                var footerAmount = $('span.amount');
                $(footerAmount).html(TotalAmount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
            },
            columns: [
                {
                    template: "<b>#= data.type_name ? type_name : ''#</b>",
                    field: "type_name",
                    title: "Type",
                    width: "200px",
                    editor: itemTypeEditor
                },
                {
                    field: "item_code",
                    title: "Item Code",
                    width: "150px",
                    editable: itemCodeEditable,
                    editor: itemEditor
                },
                {
                    field: "item_name",
                    title: "Item Name",
                    width: "150px"
                },
                {
                    field: "description",
                    title: "Remarks",
                    attributes: {
                        style: "text-align:left;"
                    },
                    width: "250px"
                },
                {
                    field: "qty",
                    title: "Qty",
                    type: "Number",
                    attributes: {
                        style: "text-align:center;"
                    },
                    width: "75px"
                },
                {
                    field: "uom_name",
                    title: "Unit",
                    attributes: {
                        style: "text-align:center;"
                    },
                    width: "100px",
                    editor: uomEditor,
                    editable: itemUomEditable
                },
                {
                    field: "unit_price",
                    title: "Estimated Price Per Quantity",
                    type: "Number",
                    attributes: {
                        style: "text-align:right;"
                    },
                    template: function (data) { return "<b>Rp. " + (getPrice(data).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") || 0) + "</b>"; },
                    width: "150px"
                },
                {
                    field: "amount",
                    title: "Total Price",
                    type: "Number",
                    editable: function () { return false },
                    attributes: {
                        style: "text-align:right;"
                    },
                    footerAttributes: {
                        style: "text-align:right;"
                    },
                    template: "<b>Rp. #= calculateAmount(data) #</b>",
                    width: "150px",
                    footerTemplate: "<span align='right' class='amount'></span>"
                }
            ],
            editable: false
        });

        function reloadGrid() {
            gridUI.data("kendoGrid").dataSource.read();
            gridUI.data('kendoGrid').refresh();
        }


        function getFileInfo(e) {
            return $.map(e.files, function (file) {
                return file;
            });
        }

        function detailInit(e) {
            var rowData = e.data;
            var detailRow = e.detailRow;
            kendo.ui.progress(detailRow.find(".k-upload-init-progress"), true);

            $.ajax({
                type: "GET",
                dataType: 'json',
                contentType: 'application/json',
                url: $.helper.resolveApi("~/core/FileUpload/entity/record?entityId=51a46d55-6d81-2077-681d-1e1ac7b687de&recordId=" + rowData.id),
                success: function (r) {
                    var data = (r.status.success ? r.data : []);
                    detailRow.find(".file-upload").kendoUpload({
                        template: $("#fileTemplate").html(),
                        async: {
                            saveUrl: $.helper.resolveApi("~/core/FileUpload/entity?entityId=51a46d55-6d81-2077-681d-1e1ac7b687de&recordId=" + rowData.id + "&tags="),
                            removeUrl: $.helper.resolveApi("~/core/FileUpload/delete"),
                            autoUpload: true,
                        },
                        multiple: true,
                        files: (r.status.success ? r.data : []),
                        //select: function onSelect(e) {
                        //    //if (e.files.length > 1) {
                        //    //    alert("Please select max 1 files.");
                        //    //    e.preventDefault();
                        //    //}
                        //},
                        complete: function (e) {

                        },
                        success: function (e) {
                            if (e.operation != "upload") return;

                            if (e.response.status.success) {
                                e.files[0].id = e.response.data.id;
                                detailRow.find(".link_" + e.files[0].uid).attr('href', 'api/core/FileUpload?id=' + e.response.data.id);
                                var urlDownload = "";
                                urlDownload = $.helper.resolveApi("~/core/FileUpload?id=" + e.response.data.id + "&x-token=" + $.helper.getTokenData().Token);
                                detailRow.find(".link_" + e.files[0].uid + ":first").attr('href', urlDownload);
                            }
                        },
                        upload: function (e) {
                            var xhr = e.XMLHttpRequest;
                            //xhr.withCredentials = true;
                            if (xhr) {
                                xhr.addEventListener("readystatechange", function onReady(e) {
                                    if (xhr.readyState == 1 /* OPENED */) {
                                        xhr.setRequestHeader("Authorization", "Bearer " + $.helper.getTokenData().Token);
                                        xhr.removeEventListener("readystatechange", onReady);
                                    }
                                });
                            }
                        },
                        remove: function (e) {
                            //send formdata
                            e.data = {
                                id: e.files[0].id
                            };
                        }
                    });


                    kendo.ui.progress(detailRow.find(".k-upload-init-progress"), false);
                    detailRow.find(".k-upload-init-progress").fadeOut();
                    detailRow.find(".file-upload").fadeIn();

                    if (data.length > 0) {
                        detailRow.find('.link-download').each(function () {
                            var id = $(this).data('id');
                            var urlDownload = "";
                            urlDownload = $.helper.resolveApi("~/core/FileUpload?id=" + id + "&x-token=" + $.helper.getTokenData().Token);
                            $(this).attr('href', urlDownload);
                        })
                    }
                }
            });


        }



        //END::Load Request For Quotation Detail

        var formControll = function () {
            $('#btn-save').removeAttr('disabled').show();

            if (_detailID.val() !== '') {
                $('#btn-approval').removeAttr('disabled', '').show();
                $('#btn-remove_approval').attr('disabled', 'disabled').hide();
                $('#btn-submit').attr('disabled', 'disabled').hide();

                if ($('input[name="approval_id"]').val() !== '') {
                    $('#btn-approval').attr('disabled', 'disabled').hide();
                    $('#btn-remove_approval').removeAttr('disabled', '').show();

                    $('#btn-submit').removeAttr('disabled', '').show();
                }

            } else {
                $('#btn-approval').attr('disabled', 'disabled').hide();
                $('#btn-remove_approval').attr('disabled', 'disabled').hide();
                $('#btn-submit').attr('disabled', 'disabled').hide();
            }
            $('#tb').show();
        };

        var formControllSubmited = function (submited) {
            if (!submited) {
                $('#btn-save').attr('disabled', 'disabled').show();
                $('#btn-approval').attr('disabled', 'disabled').hide();
                $('#btn-remove_approval').attr('disabled', 'disabled').hide();
                $('#btn-submit').attr('disabled', 'disabled').hide();
            }
            else {
                $('#btn-save').attr('disabled', 'disabled').hide();
                $('#btn-approval').attr('disabled', 'disabled').hide();
                $('#btn-remove_approval').attr('disabled', 'disabled').hide();
                $('#btn-submit').attr('disabled', 'disabled').hide();
            }
        };

        var setStatus = function (state) {
            var output = '';
            switch (state) {
                case 'new':
                    output = `<span class="m-badge m-badge--info m-badge--wide">New</span>`;
                    break;
                case 'draft':
                    output = `<span class="m-badge m-badge--warning m-badge--wide">Draft</span>`;
                    break;
                case 'submited':
                    output = `<span class="m-badge m-badge--danger m-badge--wide">Submited</span>`;
                    break;
                case 'approved':
                    output = `<span class="m-badge m-badge--success m-badge--wide">Approved</span>`;
                    break;
            }
            $('#status').html(output);
        };

        $('#btn-save').click(function () {
            var btn = $(this);
            if (_formRequestForQuotation.valid()) {
                var requestForQuotation = _formRequestForQuotation.serializeToJSON();
                var data =
                {
                    requestForQuotation: requestForQuotation,
                    vendorList: companyMultiSelect.value()
                };
                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: 'application/json',
                    url: $.helper.resolveApi("~/finance/RequestForQuotation/saveasdraft"),
                    data: JSON.stringify(data),
                    success: function (r) {
                        if (r && r.status.success) {
                            history.pushState('', 'ID', location.hash.split('?')[0] + '?id=' + r.data.recordID);
                            _detailID.val(r.data.recordID);
                            $('input[name="id"]').val(r.data.recordID);
                            toastr.success(r.status.message, "Information");
                            setStatus('draft');
                            window.location.href = $.helper.resolve("~/Finance/RequestForQuotation");
                        }
                        else {
                            swal(
                                'Saving',
                                r.status.message,
                                'info'
                            );
                        }
                    },
                    error: function (r) {
                        swal(
                            '' + r.status,
                            r.statusText,
                            'error'
                        );
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    }
                }).then(function () { btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false); });


            }

        });

        $('#btn-submit').click(function () {
            var btn = $(this);
            if (_formRequestForQuotation.valid()) {

                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: 'application/json',
                    url: $.helper.resolveApi("~/finance/RequestForQuotation/submited?RecordId=" + _detailID.val()),
                    //data: JSON.stringify(data),
                    success: function (r) {
                        if (r && r.status.success) {
                            //history.pushState('', 'ID', location.hash.split('?')[0] + '?id=' + r.data.recordID);
                            //_detailID.val(r.data.recordID);
                            //$('input[name="id"]').val(r.data.recordID);
                            btn.removeClass('m-loader m-loader--right m-loader--light');
                            formControllSubmited(true);
                            formControll();
                            reloadGrid();
                            setStatus('submited');
                            toastr.success(r.status.message, "Information");

                        }
                        else {
                            swal(
                                'Saving',
                                r.status.message,
                                'info'
                            );
                            btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                        }


                    },
                    error: function (r) {
                        swal(
                            '' + r.status,
                            r.statusText,
                            'error'
                        );
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    }
                });


            }

        });

        $('#btn-remove_approval').click(function () {
            var btn = $(this);
            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
            swal({
                title: "Are you sure reset approval template ?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                reverseButtons: !0
            }).then(function (e) {
                if (e.value) {
                    $.ajax({
                        type: "DELETE",
                        dataType: 'json',
                        url: $.helper.resolveApi("~/core/Approval/removeApprovalEntity/" + $('#entity_id').val() + "/?RecordID=" + _detailID.val() + "&ApprovalID=" + $('input[name="approval_id"]').val()),
                        success: function (r) {
                            if (r.status.success) {

                                toastr.success(r.status.message, "Information");
                                $('input[name="approval_id"]').val('');
                                formControll();

                            } else {
                                swal(
                                    'Information',
                                    r.status.message,
                                    'info'
                                );

                            }
                            btn.removeClass('m-loader m-loader--right m-loader--light');
                        },
                        error: function (e, t, s) {
                            swal(
                                'Information',
                                'Ooops, something went wrong !',
                                'info'
                            );
                            btn.removeClass('m-loader m-loader--right m-loader--light');
                        }
                    });
                }
            });

        });

        $('#btn-approval').click(function () {
            var url = $.helper.resolve("~/Core/ApprovalTemplate/SetupApproval/?EntityId=" + $('#entity_id').val() + "&RecordID=" + _detailID.val()
                + "&Title=Internal Request For Quotation&Ref=" + $('input[name="internal_rfq_number"]').val() + "&url=~/Finance/RequestForQuotation/Detail/?id=" + _detailID.val());
            window.location.href = url;
        });

        var DeleteData = function (ArrID) {
            swal({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                reverseButtons: !0
            }).then(function (e) {
                if (e.value) {
                    mApp.block("#m_blockui_list", {
                        overlayColor: "#000000",
                        type: "loader",
                        state: "primary",
                        message: "Processing..."
                    });

                    $.ajax({
                        type: "DELETE",
                        dataType: 'json',
                        contentType: 'application/json',
                        url: $.helper.resolveApi("~/Finance/RequestForQuotationDetail/delete"),
                        data: JSON.stringify(ArrID),
                        success: function (r) {
                            if (r.status.success) {
                                swal({
                                    title: 'Deleted!',
                                    text: r.status.message,
                                    type: "success"
                                }).then(function () {
                                    ListChecked = [];
                                    reloadGrid();
                                });
                            } else {
                                swal(
                                    'Information',
                                    r.status.message,
                                    'info'
                                );
                            }

                        },
                        error: function (e, t, s) {
                            swal(
                                'Information',
                                'Ooops, something went wrong !',
                                'info'
                            );
                        }
                    }).then(setTimeout(function () {
                        mApp.unblock("#m_blockui_list");
                    }, 2e3));
                }


            });
        };

        return {
            init: function () {
                //initialize
                loadDetailData()
            }
        };
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery));