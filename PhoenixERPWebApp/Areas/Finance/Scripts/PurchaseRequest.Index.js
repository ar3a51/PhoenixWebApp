﻿(function ($) {
    'use strict';
    var gridUI = $('#grid');
    var gridUIDialog = $('#dialog_grid');
    var ListChecked = [];
    var ListCheckedGrid = [];
    var kendoUIWindow = $("#window");

    var pageFunction = function () {

        function onOpen(item) {
            kendoUIWindow.find('#grid-detail').empty();
            $.helper.kendoUI.grid(kendoUIWindow.find('#grid-detail'), {
                options: {
                    url: $.helper.resolveApi('~/finance/PurchaseRequestDetail/KendoGrid?purchaseRequestId=' + item.id + '&iswindow=true')
                },
                navigatable: true,
                pageable: false,
                resizable: true,
                dataSource: {
                    pageSize: 1000,
                    serverPaging: true,
                    serverSorting: false,
                    batch: true,
                    serverOperation: false,
                    schema: {
                        model: {
                            id: "id",
                            fields: {
                                id: { editable: true, nullable: true },
                                type_name: { editable: true, validation: { required: true } },
                                item_code: { editable: true },
                                item_type_id: { editable: true, validation: { required: true } }
                            }
                        },
                        data: "data",
                        total: "recordsTotal"
                    },
                },
                marvelCheckboxSelectable: {
                    enable: true,
                    listCheckedTemp: ListCheckedGrid
                },
                schema: {
                    model: {
                        id: "id"
                    }
                },
                change: function (e) {
                    //var rows = e.sender.select();
                    //rows.each(function (e) {
                    //    var grid = $("#grid-detail").data("KendoGrid");
                    //    var dataItem = grid.dataItem(this);

                    //    console.log(dataItem);
                    //});
                },
                dataBound: function (e) {

                },
                columns: [
                    {
                        template: "<b>#= data.type_name ? type_name : ''#</b>",
                        field: "type_name",
                        title: "Type",
                        width: "100px"
                    },
                    {
                        field: "item_code",
                        title: "Item Code",
                        width: "100px"
                    },
                    {
                        field: "item_name",
                        title: "Item Name",
                        width: "100px"
                    },
                    {
                        field: "description",
                        title: "Description",
                        width: "150px",
                        attributes: {
                            style: "white-space: pre-line;"
                        },
                        editor: function (container, options) {
                            $('<textarea name="' + options.field + '" style="width: ' + container.width() + 'px;height:' + container.height() + 'px" />')
                                .appendTo(container);
                        }
                    },
                    {
                        field: "quantity",
                        title: "Qty",
                        width: "80px"
                    }
                ],
                editable: false
            });

            kendoUIWindow.find('#btn-create').click(function () {
                if (ListCheckedGrid.length > 0) {
                    kendo.ui.progress(kendoUIWindow, true);
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: $.helper.resolveApi("~/finance/PurchaseRequest/createrfq/?purchaserequestid=" + item.id),
                        data: JSON.stringify(ListCheckedGrid),
                        success: function (result) {
                            if (result.status.success) {
                                toastr.success(result.status.message, "Information");
                                setTimeout(function () {
                                    window.location.href = $.helper.resolve("~/Finance/RequestForQuotation/Detail/?id=" + result.data.recordID);
                                }, 3000);
                            }
                            else {
                                toastr.error(result.status.message, "Error");
                                kendo.ui.progress(kendoUIWindow, false);
                            }
                        },
                        error: function () {
                            alert('err');
                            kendo.ui.progress(kendoUIWindow, false);
                        }
                    });

                } else {
                    toastr.error("Please Choose detail item at least one", "Error");
                }
            });
        }

        $.helper.kendoUI.grid($("#grid"), {
            options: {
                url: $.helper.resolveApi('~/finance/PurchaseRequest/KendoGrid')
            },
            navigatable: true,
            //height: 550,
            pageable: {
                refresh: true,
                pageSizes: [5, 10, 20, 100]
            },
            resizable: true,
            dataSource: {
                pageSize: 10,
                serverPaging: true,
                serverSorting: true,
                schema: {
                    parse: function (response) {
                        for (var i = 0; i < response.data.length; i++) {
                            var dateNoTime = new Date(response.data[i].request_datetime);
                            response.data[i].request_datetime = new Date(
                                dateNoTime.getFullYear(),
                                dateNoTime.getMonth(),
                                dateNoTime.getDate());
                        }
                        return response;
                    },
                    model: {
                        id: "id"
                    }
                }
            },
            schema: {
                model: {
                    id: "id"
                }
            },
            marvelCheckboxSelectable: {
                enable: true,
                listCheckedTemp: ListChecked
            },
            dataBound: function (e) {
                buildMenu();
                $.helper.kendoUI.resizeGrid();
            },
            columns: [
                {
                    title: "Actions", width: "230px",
                    attributes: {
                        style: "text-align:center;overflow: visible;"
                    },
                    template: "<ul class='action-menu' data-uid='#= uid #' data-id='#= data.id #'></ul>"
                },
                {
                    field: "request_number",
                    title: "Request Number",
                    width: "250px",
                    template: function (dataItem) {
                        var output = '';

                        if (dataItem.requestForQuotation.toJSON().length > 0) {
                            output = "<b>" + dataItem.request_number + "</b></br>"
                            $.each(dataItem.requestForQuotation.toJSON(), function (i, v) {
                                output += "<a href='" + $.helper.resolve("~/Finance/RequestForQuotation/Detail/?id=" + v.id) + "'>Go to " + v.rfq_number + "</a><br/>";
                            });
                        }
                        else {
                            output = "<b>" + dataItem.request_number + "</b>";
                        }
                        return output;

                    }
                },
                {
                    field: "request_datetime",
                    title: "Date",
                    width: "250px",
                    template: "#= kendo.toString(request_datetime, 'MMM-dd-yyyy') #",
                    groupHeaderTemplate: "#= kendo.toString(value, 'MMMM, dd-yyyy') #"
                },
                {
                    field: "legal_entity_name",
                    title: "Legal Entity",
                    width: "150px"
                },
                {
                    field: "unit_name",
                    title: "Business Unit",
                    width: "150px"
                },
                {
                    field: "affiliation_name",
                    title: "Affiliation",
                    width: "150px"
                },
                {
                    field: "total_amount",
                    title: "Amount",
                    width: "150px",
                    headerAttributes: {
                        style: "text-align:right;"
                    },
                    attributes: {
                        style: "text-align:right;"
                    },
                    template: "<b>Rp. </b>#= kendo.toString(data.total_amount, 'n') #",
                }
            ]
        });

        function buildMenu() {

            $(".action-menu").kendoMenu({
                dataSource: [
                    {
                        text: "Edit"
                    },
                    {
                        text: "Delete"
                    },
                    {
                        text: "Other", // TODO how to get the rows first column value here?
                        items: [
                            { text: "Create RFQ" }
                        ],
                        menu: false
                    }],

                select: function (e) {
                    var operation = $(e.item).text();
                    // TODO how to get the current row data here?
                    var Uid = e.sender.element.attr('data-uid');
                    var dataSource = gridUI.data("kendoGrid").dataSource;
                    var item = dataSource.getByUid(Uid);

                    switch (operation) {
                        case "Edit":

                            window.location.href = $.helper.resolve("~/Finance/PurchaseRequest/DetailInternal/?id=" + item.id);
                            break;
                        case "Delete":
                            if (item.id) DeleteData([item.id]);

                            break;
                        case "Create RFQ":
                            var template = kendo.template($("#create_rfq_template").html());
                            ListCheckedGrid = [];
                            kendoUIWindow.empty();
                            var kw = kendoUIWindow.kendoWindow({
                                width: "615px",
                                title: "Create Request For Quotation",
                                visible: false,
                                actions: [
                                    "Maximize",
                                    "Close"
                                ],
                                open: function () { onOpen(item) },
                                close: onClose
                            }).data("kendoWindow").content(template(item)).center().open();

                            function onClose() {
                                kw.destroy();
                            }
                            break;
                    }
                }
            });
        }

        function reloadGrid() {
            gridUI.data("KendoGrid").dataSource.read();
            gridUI.data('KendoGrid').refresh();
        }

        gridUI.find(".k-grid-toolbar").on("click", ".k-grid-Reload", function (e) {
            e.preventDefault();
            reloadGrid();
        });

        var DeleteData = function (ArrID) {
            swal({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                reverseButtons: !0
            }).then(function (e) {
                if (e.value) {
                    mApp.block("#m_blockui_list", {
                        overlayColor: "#000000",
                        type: "loader",
                        state: "primary",
                        message: "Processing..."
                    });

                    $.ajax({
                        type: "DELETE",
                        dataType: 'json',
                        contentType: 'application/json',
                        url: $.helper.resolveApi("~/Finance/PurchaseRequest/delete"),
                        data: JSON.stringify(ArrID),
                        success: function (r) {
                            if (r.status.success) {
                                swal({
                                    title: 'Deleted!',
                                    text: r.status.message,
                                    type: "success"
                                }).then(function () {
                                    ListChecked = [];
                                    //reloadGrid();
                                    location.reload();
                                });
                            } else {
                                swal(
                                    'Information',
                                    r.status.message,
                                    'info'
                                );
                            }

                        },
                        error: function (e, t, s) {
                            swal(
                                'Information',
                                'Ooops, something went wrong !',
                                'info'
                            );
                        }
                    }).then(setTimeout(function () {
                        mApp.unblock("#m_blockui_list");
                    }, 2e3));
                }


            });
        };
        //BEGIN::EVENT BUTTON
        $('#btn-delete').click(function () {
            console.log(ListChecked);
            return;
            //if (ListChecked.length > 0)
            //    DeleteData(ListChecked);
        });



        //END::EVENT BUTTON


        //BEGIN::EVENT BUTTON

        //END::EVENT BUTTON
        return {
            init: function () {
                //initialize
            }
        };
    }();

    $(document).ready(function () {
        pageFunction.init();
    });

}(jQuery));