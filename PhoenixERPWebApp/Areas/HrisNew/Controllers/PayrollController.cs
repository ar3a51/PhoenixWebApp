using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CodeMarvel.Infrastructure.ModelShared;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Phoenix.WebExtension.RestApi;
using PhoenixERPWebApp.Models;

namespace PhoenixERPWebApp.Areas.HrisNew.Controllers
{
    [Area(nameof(HrisNew))]
    public class PayrollController : Controller
    {
        string url = ApiUrl.PayrollUrl;
        private readonly ApiClientFactory client;

        public PayrollController(ApiClientFactory client)
        {
            this.client = client;
        }

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult IndexApproval()
        {
            return View();
        }

        public async Task<IActionResult> AddEdit(string Id, bool isApprove)
        {
            var model = new SalaryProcessHeader();
            var isCurrentApproval = false;
            if (!string.IsNullOrEmpty(Id))
            {
                if (isApprove == true)
                    ViewBag.Header = "Approve";
                else
                    ViewBag.Header = "Update";
                model = await client.Get<SalaryProcessHeader>($"{url}/{Id}");
                isCurrentApproval = await client.Get<bool>($"{ApiUrl.ManageTransApproval}/IsCurrentApproval/{Id}");
                ViewBag.isRequestor = isApprove == false && (model.Status == StatusTransaction.Draft || model.Status == StatusTransaction.Rejected);
            }
            else
            {
                ViewBag.Header = "Create";
                ViewBag.isRequestor = isApprove == false;
                model.Status = StatusTransaction.Draft;
            }

            ViewBag.processApprove = isApprove && isCurrentApproval;
            model.IsApprove = isApprove;
            //ViewBag.isHcAdmin = await client.Get<bool>($"{ApiUrl.ManageUserUrl}/IsHCAdmin");
            return View(model);
        }

        public async Task<IActionResult> Read([DataSourceRequest] DataSourceRequest request)
        {
            var model = await client.Get<List<SalaryProcessHeader>>(url) ?? new List<SalaryProcessHeader>();
            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> ReadApproval([DataSourceRequest] DataSourceRequest request)
        {
            var model = await client.Get<List<SalaryProcessHeader>>($"{url}/approvalList") ?? new List<SalaryProcessHeader>();
            var data = model.ToDataSourceResult(request);
            return Json(data, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        [HttpPost]
        public async Task<IActionResult> Approve(SalaryProcessHeader model)
        {
            model.Status = StatusTransaction.Approved;
            model.IsTmp = false;
            var response = await client.PutApiResponse<SalaryProcessHeader>($"{url}/Approve", model);
            return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(IndexApproval)) });
        }

        [HttpPost]
        public async Task<IActionResult> Reject(SalaryProcessHeader model)
        {
            model.Status = StatusTransaction.Rejected;
            model.IsTmp = false;
            var response = await client.PutApiResponse<SalaryProcessHeader>($"{url}/Reject", model);
            return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(IndexApproval)) });
        }

        [HttpPost]
        public async Task<IActionResult> Submit(SalaryProcessHeader model)
        {
            model.Status = StatusTransaction.WaitingApproval;
            model.IsTmp = false;
            return await Process(model, false);
        }

        [HttpPost]
        public async Task<IActionResult> Save(SalaryProcessHeader model)
        {
            model.Status = StatusTransaction.Draft;
            model.IsTmp = false;
            return await Process(model, true);
        }

        private async Task<IActionResult> Process(SalaryProcessHeader model, bool isDraft)
        {
            var response = new Phoenix.WebExtension.RestApi.ApiResponse();
            if (!string.IsNullOrEmpty(model.Id))
            {
                response = await client.PutApiResponse<SalaryProcessHeader>(url, model);
            }
            else
            {
                response = await client.PostApiResponse<SalaryProcessHeader>(url, model);
            }

            if (isDraft)
            {
                return Json(new { success = response.Success, message = response.Message, data = response.Data as SalaryProcessHeader });
            }
            else
            {
                return Json(new { success = response.Success, message = response.Message, url = Url.Action(nameof(Index)) });
            }
        }

        [HttpPost]
        public async Task<IActionResult> GenerateProcess(int month, int year)
        {
            var response = await client.GetApiResponse<SalaryProcessHeader>($"{url}/Generate/{month}/{year}");
            return Json(response, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }

        public async Task<IActionResult> GetGenerateList(int month, int year)
        {
            var result = await client.GetApiResponse<DataTable>($"{url}/{month}/{year}");
            return Json(result, new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() });
        }
    }
}