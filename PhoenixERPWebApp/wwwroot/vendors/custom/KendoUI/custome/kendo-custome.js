function errorHandler(e) {
    if (e.status === 'customerror') {
        if (e.errors) {
            var message = '';
            $.each(e.errors, function (key, value) {
                title = key;
                if ('errors' in value) {
                    $.each(value.errors, function () {
                        message += this + "\n";
                    });
                }
            });

            $('.k-grid').each(function (item) {
                var grid = $(this).data("kendoGrid");
                if (e.sender === grid.dataSource) {
                    grid.cancelChanges();
                }
            });

            swal('Error', message, 'error');
        }
    } else if (e.status === "error") {
        if (e.xhr.responseJSON !== undefined) {
            var errorMessage = e.xhr.responseJSON.errorMessage;
            if (errorMessage === "" || errorMessage === undefined) {
                errorMessage = "Ooops, something went wrong !";
            }
            swal('Error', errorMessage, 'error');
        }
    }
}

function dataItem(e) {
    var $elem = $(e);
    var grid = $elem.closest("div[data-role='grid']").data("kendoGrid");
    var item = grid.dataItem($elem.closest("tr"));
    return item;
}

function refreshGrid(e) {
    var $elem = $(e);
    var grid = $elem.closest("div[data-role='grid']").data("kendoGrid");
    grid.dataSource.read();
}

function deleteRow(e, url) {
    var item = dataItem(e);
    swal({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        reverseButtons: !0
    }).then(function (ok) {
        if (ok.value) {
            mApp.block("#m_blockui_list", {
                overlayColor: "#000000",
                type: "loader",
                state: "primary",
                message: "Processing..."
            });
            $.ajax({
                type: "DELETE",
                url: url,
                data: JSON.parse(JSON.stringify(item)),
                success: function (result) {
                    if (result.success) {
                        swal('Information', result.message, 'success')
                            .then(function (ok) {
                                if (ok.value) {
                                    if (result.url !== undefined) { window.location = result.url; } else { refreshGrid(e); }
                                }
                            });
                    } else {
                        swal('Warning', result.message, 'warning');
                    }
                    mApp.unblock("#m_blockui_list");
                },
                error: function (e, t, s) {
                    var errorMessage = e.responseJSON.errorMessage;
                    if (errorMessage === "" || errorMessage === undefined) {
                        errorMessage = "Ooops, something went wrong !";
                    }
                    swal('Error', errorMessage, 'error');
                    mApp.unblock("#m_blockui_list");
                }
            }).then(setTimeout(function () {
                mApp.unblock("#m_blockui_list");
            }, 2e3));
        }
    }).catch(swal.noop);
}

function deleteGridRowDataSource(e) {
    swal({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        reverseButtons: !0
    }).then(function (ok) {
        if (ok.value) {
            mApp.block("#m_blockui_list", {
                overlayColor: "#000000",
                type: "loader",
                state: "primary",
                message: "Processing..."
            });

            var item = dataItem(e);
            $("#grid").data("kendoGrid").dataSource.remove(item);
        }
    }).catch(swal.noop);
}

function callAction(parameter) {
    if (parameter.formId !== undefined) {
        if (parameter.formId.substring(0, 1) !== "#") parameter.formId = "#" + parameter.formId;
        var valid = $(parameter.formId).validate().form();
        //console.log(parameter.formId);
        //console.log(valid);
        if (valid) {
            swal({
                title: "Confirmation",
                text: 'Are you sure want to ' + parameter.title + ' this data?',
                type: "question",
                showCancelButton: !0,
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                reverseButtons: !0
            }).then(function (ok) {
                if (ok.value) {
                    mApp.block("#m_blockui_list", {
                        overlayColor: "#000000",
                        type: "loader",
                        state: "primary",
                        message: "Processing..."
                    });

                    //var data = $(parameter.formId).serialize();

                    var object = {};
                    var params = $(parameter.formId).serializeArray();
                    $.each(params, function (i, val) {
                        object[val.name] = val.value;
                    });

                    if (parameter.objects !== undefined) {
                        $.each(parameter.objects, function (i, val) {
                            var arry = [];
                            for (var z = 0; z < val.length; z++) {
                                var obj = JSON.parse(JSON.stringify(val[z]));
                                arry.push(obj);
                            }
                            object[i] = arry;
                        });
                    }

                    var url = parameter.url;
                    if (url === undefined) url = $(parameter.formId).attr('action');
                    console.log(url);
                    $.ajax({
                        type: parameter.type,
                        url: url,
                        data: object,
                        dataType: 'json',
                        success: function (result) {
                            if (result.success) {
                                if (result.message === undefined || result.message === null) {
                                    if (result.url !== undefined) window.location = result.url;
                                } else {
                                    swal('Information', result.message, 'success')
                                        .then(function (ok) {
                                            if (ok.value) {
                                                if (result.data !== undefined) {
                                                    $("#Id").val(result.data.id);
                                                    $("#InvoiceNumber").val(result.data.invoiceNumber);
                                                    $("#RequestCode").val(result.data.requestCode);
                                                    $("#RequestNumber").val(result.data.requestNumber);
                                                    $("#Code").val(result.data.code);
                                                    $("#CreatedBy").val(result.data.createdBy);
                                                    $("#CreatedOn").val(result.data.createdOn);
                                                    var status = "Draft";
                                                    if (result.data.Status !== undefined) status = result.data.Status; $("#Status").val(status);
                                                    if (result.data.StatusDesc !== undefined) status = result.data.StatusDesc; $("#StatusDesc").val(status);

                                                }

                                                if (result.url !== undefined) window.location = result.url;
                                                if (result.refreshGrid !== undefined) {
                                                    var gridArray = result.refreshGrid.split(";");
                                                    $.each(gridArray, function (index, value) {
                                                        $('#' + value).data('kendoGrid').dataSource.read();
                                                        $('#' + value).data('kendoGrid').refresh();
                                                    });
                                                }
                                                if (result.refreshTreeList !== undefined) {
                                                    var gridTreeListArray = result.refreshGrid.split(";");
                                                    $.each(gridTreeListArray, function (index, value) {
                                                        $('#' + value).data('kendoTreeList').dataSource.read();
                                                        $('#' + value).data('kendoTreeList').refresh();
                                                    });
                                                }
                                                if (result.hideModalPopUp !== undefined)
                                                    $('#' + result.hideModalPopUp).modal('hide');
                                                if (result.btnTriggerId !== undefined) {
                                                    debugger;
                                                    $('#' + result.btnTriggerId).click();
                                                }


                                            }
                                        });
                                }
                            } else {
                                swal('Warning', result.message, 'warning');
                            }
                            mApp.unblock("#m_blockui_list");
                        },
                        error: function (e, t, s) {
                            var errorMessage = e.responseJSON.errorMessage;
                            if (errorMessage === "" || errorMessage === undefined) {
                                errorMessage = "Ooops, something went wrong !";
                            }
                            swal('Error', errorMessage, 'error');
                            mApp.unblock("#m_blockui_list");
                        }
                    }).then(setTimeout(function () {
                        mApp.unblock("#m_blockui_list");
                    }, 2e3));
                }
            }).catch(swal.noop);
        }
    }
}

function callActionFile(parameter) {
    if (parameter.formId !== undefined) {
        if (parameter.formId.substring(0, 1) !== "#") parameter.formId = "#" + parameter.formId;
        var valid = $(parameter.formId).validate().form();
        if (valid) {
            swal({
                title: "Confirmation",
                text: 'Are you sure want to ' + parameter.title + ' this data?',
                type: "question",
                showCancelButton: !0,
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                reverseButtons: !0
            }).then(function (ok) {
                if (ok.value) {
                    mApp.block("#m_blockui_list", {
                        overlayColor: "#000000",
                        type: "loader",
                        state: "primary",
                        message: "Processing..."
                    });

                    var formData = new FormData();
                    $.each($(parameter.formId).find("input[type='file']"), function (i, tag) {
                        $.each($(tag)[0].files, function (i, file) {
                            formData.append(tag.name, file);
                        });
                    });

                    var params = $(parameter.formId).serializeArray();
                    $.each(params, function (i, val) {
                        formData.append(val.name, val.value);
                    });

                    var object = {};
                    if (parameter.objects !== undefined) {
                        $.each(parameter.objects, function (i, val) {
                            var arry = [];
                            for (var z = 0; z < val.length; z++) {
                                var obj = JSON.parse(JSON.stringify(val[z]));
                                arry.push(obj);
                            }
                            object[i] = arry;
                        });
                    }
                    formData.appendRecursive(object, "");

                    var url = parameter.url;
                    if (url === undefined) url = $(parameter.formId).attr('action');

                    $.ajax({
                        type: parameter.type,
                        url: url,
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: formData,
                        success: function (result) {
                            if (result.success) {
                                swal('Information', result.message, 'success')
                                    .then(function (ok) {
                                        if (ok.value) {
                                            if (result.data !== undefined) {
                                                $("#Id").val(result.data.id);
                                                $("#InvoiceNumber").val(result.data.invoiceNumber);
                                                $("#RequestCode").val(result.data.requestCode);
                                                $("#RequestNumber").val(result.data.requestNumber);
                                                $("#Code").val(result.data.code);
                                                $("#CreatedBy").val(result.data.createdBy);
                                                $("#CreatedOn").val(result.data.createdOn);
                                            }

                                            if (result.url !== undefined) window.location = result.url;
                                            if (result.refreshGrid !== undefined) {
                                                var gridArray = result.refreshGrid.split(";");
                                                $.each(gridArray, function (index, value) {
                                                    $('#' + value).data('kendoGrid').dataSource.read();
                                                    $('#' + value).data('kendoGrid').refresh();
                                                });
                                            }
                                            if (result.refreshTreeList !== undefined) {
                                                var gridTreeListArray = result.refreshGrid.split(";");
                                                $.each(gridTreeListArray, function (index, value) {
                                                    $('#' + value).data('kendoTreeList').dataSource.read();
                                                    $('#' + value).data('kendoTreeList').refresh();
                                                });
                                            }
                                            if (result.hideModalPopUp !== undefined)
                                                $('#' + result.hideModalPopUp).modal('hide');
                                            if (result.btnTriggerId !== undefined) {
                                                $('#' + result.btnTriggerId).click();
                                            }

                                        }
                                    });
                            } else {
                                swal('Warning', result.message, 'warning');
                            }
                            mApp.unblock("#m_blockui_list");
                        },
                        error: function (e, t, s) {
                            var errorMessage = e.responseJSON.errorMessage;
                            if (errorMessage === "" || errorMessage === undefined) {
                                errorMessage = "Ooops, something went wrong !";
                            }
                            swal('Error', errorMessage, 'error');
                            mApp.unblock("#m_blockui_list");
                        }
                    }).then(setTimeout(function () {
                        mApp.unblock("#m_blockui_list");
                    }, 2e3));
                }
            }).catch(swal.noop);
        }
    }
}

function callActionWithDetails(parameter) {
    if (parameter.formId !== undefined) {
        if (parameter.formId.substring(0, 1) !== "#") parameter.formId = "#" + parameter.formId;
        var valid = $(parameter.formId).validate().form();
        if (valid) {
            swal({
                title: "Confirmation",
                text: 'Are you sure want to ' + parameter.title + ' this data?',
                type: "question",
                showCancelButton: !0,
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                reverseButtons: !0
            }).then(function (ok) {
                if (ok.value) {
                    mApp.block("#m_blockui_list", {
                        overlayColor: "#000000",
                        type: "loader",
                        state: "primary",
                        message: "Processing..."
                    });
                    var formData = new FormData();
                    var params = $(parameter.formId).serializeArray();

                    var data = {};
                    $(params).each(function (index, obj) {
                        data[obj.name] = obj.value;
                    });
                    data.details = parameter.objects;
                    //var a = {
                    //    TrMediaOrderTVDetails: parameter.objects
                    //};
                    //$.each(params, function (i, val) {
                    //    formData.append(val.name, val.value);
                    //});
                    //if (parameter.objects !== undefined) {
                    //    formData.append(parameter.objectName, parameter.objects);
                    //}
                    var url = parameter.url;
                    if (url === undefined) url = $(parameter.formId).attr('action');

                    $.ajax({
                        type: parameter.type,
                        url: url,
                        data: data,
                        success: function (result) {
                            if (result.success) {
                                swal('Information', result.message, 'success')
                                    .then(function (ok) {
                                        if (ok.value) {
                                            if (result.url !== undefined) window.location = result.url;
                                        }
                                    });
                            } else {
                                swal('Warning', result.message, 'warning');
                            }
                            mApp.unblock("#m_blockui_list");
                        },
                        error: function (e, t, s) {
                            var errorMessage = e.responseJSON.errorMessage;
                            if (errorMessage === "" || errorMessage === undefined) {
                                errorMessage = "Ooops, something went wrong !";
                            }
                            swal('Error', errorMessage, 'error');
                            mApp.unblock("#m_blockui_list");
                        }
                    }).then(setTimeout(function () {
                        mApp.unblock("#m_blockui_list");
                    }, 2e3));
                }
            }).catch(swal.noop);
        }
    }
}

function comboboxSelectEvent(e) {
    var dataItem = e.dataItem;
    if (dataItem === null && dataItem === undefined)
        this.value("");
}

function downloadAction(parameter) {
    $.ajax({
        type: "GET",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        async: false,
        data: parameter.data,
        url: parameter.url,
        success: function (result) {
            if (result.File !== undefined) {
                var extensions = result.FileName.split('.').pop();
                if (extensions.toLowerCase() == "pdf") {
                    let pdfWindow = window.open("");
                    pdfWindow.document.write("<iframe type='application/pdf' width='100%' height='100%' src='data:application/pdf;base64," + result.File + "' download='" + result.FileName + "'></iframe>");
                }
                else {
                    kendo.saveAs({
                        dataURI: "data:text/plain;base64," + result.File,
                        fileName: result.FileName
                    });
                }
            }
        }
    });
}

function dataBoundEvent(e) {
    var rows = e.sender.tbody.children();
    $(rows).each(function () {
        var rowLabel = $(this).find(".row-number");
        var pageSize = (e.sender.dataSource.pageSize() * (e.sender.dataSource.page() - 1));
        if (isNaN(pageSize)) { pageSize = 0; }
        var index = $(this).index() + 1 + pageSize;
        $(rowLabel).html(index);
    });
}

function dataBoundBgColorEvent(e, params) {
    // iterate the table rows and apply custom row and cell styling 
    var rows = e.sender.tbody.children();
    for (var j = 0; j < rows.length; j++) {
        var row = $(rows[j]);
        var rowLabel = $(rows[j]).find(".row-number");
        var pageSize = (e.sender.dataSource.pageSize() * (e.sender.dataSource.page() - 1));
        if (isNaN(pageSize)) { pageSize = 0; }
        var index = $(rows[j]).index() + 1 + pageSize;
        $(rowLabel).html(index);

        var dataItem = e.sender.dataItem(row);
        var status = null;

        $.each(params.split('.'), function (i, val) {
            if (status === null)
                status = dataItem.get(val);
            else
                status = status.get(val);
        });

        //Claer Background color grid row
        row.removeClass("currentApprove");

        if (status !== null) {
            if (status.toString().toLowerCase() === "1") {
                row.addClass("currentApprove");
            }
        }
    }
}

function yesNo(element) {
    element.kendoDropDownList({
        dataSource: [{ Text: "Yes", Value: "Yes" }, { Text: "No", Value: "No" }],
        dataTextField: "Text",
        dataValueField: "Value",
        optionLabel: "--Select Value--"
    });
}

function addTreeListRow(elem) {
    try {
        var treelist = $(elem).closest("div[data-role='treelist']").data("kendoTreeList");
        var item = treelist.dataItem($(elem).closest("tr"));
        treelist.addRow(item);
    }
    catch (err) {
        swal('Error', err.message, 'error');
    }
}

function editTreeListRow(elem) {
    try {
        var treelist = $(elem).closest("div[data-role='treelist']").data("kendoTreeList");
        var item = treelist.dataItem($(elem).closest("tr"));
        treelist.editRow(item);
    }
    catch (err) {
        swal('Error', err.message, 'error');
    }
}

function deleteTreeListRow(elem) {
    var treelist = $(elem).closest("div[data-role='treelist']").data("kendoTreeList");
    var item = treelist.dataItem($(elem).closest("tr"));
    treelist.removeRow($(elem).closest("tr"));
    //bootbox.confirm({
    //    title: 'Deletion Confirmation',
    //    message: 'Are you sure want to delete this data?',
    //    size: 'small',
    //    buttons: {
    //        cancel: {
    //            label: '<i class="fa fa-times"></i> Cancel'
    //        },
    //        confirm: {
    //            label: '<i class="fa fa-check"></i> Confirm'
    //        }
    //    },
    //    callback: function (result) {
    //        if (result) {
    //            treelist.removeRow($(elem).closest("tr"));
    //        }
    //    }
    //});
}


FormData.prototype.appendRecursive = function (data, wrapper) {
    for (var x in data) {
        if (typeof data[x] == 'object' || data[x].constructor === Array) {
            this.appendRecursive(data[x], wrapper + '[' + x + ']');
        } else {
            this.append(wrapper + '[' + x + ']', data[x]);
        }
    }
}