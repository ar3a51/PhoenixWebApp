//** Manage Connection Nava-Hub SignalR....created by M.Salih Rismadi (SoftFocusCore Indonesia)
//** Aug 29 2019...

const connection = new signalR.HubConnectionBuilder()
    .withUrl($.helper.resolveApi('~Hubs/Notification'))
    .build();

// tell the client, if we are ready connected to hub.....
//connection.start().then(function () {
//    console.log("nava-hub connected..");
//});

// tell to client, if hub is getting error....
connection.start().catch(err => console.error('Error Nava-Hub : ' + err.toString()));



connection.on("ReceiveMsgTeam", (teamId, toMember, fromMember, memberName, message,time) => {
    var data = sessionStorage.getItem('data');
    data = JSON.parse(data);
    $('#TbTextChatMsg').val('');
    if (data.AppUserId === toMember) {
        writeMsgChat(teamId, fromMember, memberName, message, time);
    }
});

connection.on("ReceiveNTFCenter", (toMember, templateNotif, tipe) => {
    
    var data = sessionStorage.getItem('data');
    data = JSON.parse(data);
    if (data.AppUserId === toMember) {
        let cn = $('#sp-bgnotif-count')[0].innerText;
        if (cn === '' || cn === null || cn === undefined) cn = 0;
        cn = parseInt(cn) + 1;
        $('#sp-bgnotif-count')[0].innerText = cn;
        $('#countnotification')[0].innerText = cn;

        if (tipe === 1) {
            $('#notification_list').append(templateNotif);
            // let counting deep just data notif
            let cnx = $('#sp-cbgn-count')[0].innerText;
            if (cnx === '' || cnx === null || cnx === undefined) cnx = 0;
            cnx = parseInt(cnx) + 1;
            $('#sp-cbgn-count')[0].innerText = cnx;
        }
        else {
            $('#approval_list').append(templateNotif);
            // let counting deep data approval
            let cax = $('#sp-cbga-count')[0].innerText;
            if (cax === '' || cax === null || cax === undefined) cax = 0;
            cax = parseInt(cax) + 1;
            $('#sp-cbga-count')[0].innerText = cax;
        }
    }
});

//** Reconnect clients
async function start() {
    try {
        await connection.start();
        console.log("connected");
    } catch (err) {
        console.log(err);
        setTimeout(() => start(), 5000);
    }
};

// client wanna close and prepare to connected again....
connection.onclose(async () => {
    await start();
});

let directToDetailNotif = function (codex,directLink) {
    let urlApi = $.helper.resolveApi("~notifycenter/UpdateIsSeen");
    var model = { Codex: codex };
    $.ajax({
        type: "POST",
        dataType: 'json',
        contentType: 'application/json',
        url: urlApi,
        data: JSON.stringify(model),
        success: function () {
        },
        error: function (e, t, s) {
            console.log('update seend notification getting error');
            console.log(e);
            console.log(t);
            console.log(s);
            console.log('======= END OF update seend notification getting error =========');
        }
    });

    window.location.href = directLink;
};

let bindNotifyContent = function () {
    let urlApi = $.helper.resolveApi("~notifycenter/Get");
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: urlApi,
        success: function (data) {
            let cax = 0;
            let cnx = 0;

            $.each(data, function (x, r) {
                if (r.tipe === 1) {
                    $('#notification_list').append(r.templateNotify);
                    cnx++;
                }
                else {
                    $('#approval_list').append(r.templateNotify);
                    cax++;
                }
            });
            $('#sp-bgnotif-count')[0].innerText = data.length;
            $('#countnotification')[0].innerText = data.length;

            $('#sp-cbga-count')[0].innerText = cax;
            $('#sp-cbgn-count')[0].innerText = cnx;

        },
        error: function (e, t, s) {
            console.log('build notification getting error');
            console.log(e);
            console.log(t);
            console.log(s);
            console.log('======= END OF build notification getting error =========');
        }
    });
};
$(function () {
    bindNotifyContent();
});
//<a href="#" title="click here to see detail"><div> <p>Assign as PM</p></div></a>