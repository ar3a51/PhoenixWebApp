var ajaxUrl = API + "/Hris/Mpp";
var ajaxUrlList = ajaxUrl + "/List";
var ajaxUrlUpdate = ajaxUrl + "/Update";
var ajaxUrlCreate = ajaxUrl + "/Create";
var ajaxUrlDelete = ajaxUrl + "/DeleteWithDetail";

var tableList = "MppList";
var formSearch = "MppFormSearch";
var modalSearch = "MppModalSearch";
var pageheader = "";
var paramsSearchIndex = function () {
    var searchdata = $("#" + formSearch).serializeArray();
    var data = {};
    $(searchdata).each(function (index, obj) {
        if (obj.name !== "__RequestVerificationToken") {
            data[obj.name] = obj.value;
        }
    });

    return data;
};
var columnsIndex = [
    {
        "data": null,
        "title": "#",
        "sClass": "actioncol",
        orderable: false,
        "render": function (data, type, row, meta) {
            return meta.row + meta.settings._iDisplayStart + 1;
        }
    },
    {
        "data": "code",
        "title": "<span data-args='code'>Code</span>",
        "sClass": "actioncol",
        orderable: false,
        "render": function (data, type, row) {
            var datastring = '';
            if (row.status === "1" || row.status === "2") {
                datastring = '<a class="fakelink" href="/Hris/MppApprovedStatus/Index/' + row.id + '">' + data + '</a>';
            } else {
                datastring = data;
            }
          
            return datastring;
        }
    }  ,{
        "data": "fiscal_year",
        "title": "<span data-args='fiscal_year'>Fiscal Year</span>",
        "sClass": "",
        orderable: true
    },
    {
        "data": null,
        "title": "<span data-args='business_unit_id'>Division</span>",
        "sClass": "actioncol",
        orderable: false,
        "render": function (data, type, row) {
            var datastring = '';
            datastring = row.businessUnit.unit_name;

            return datastring;
        }
    },
    {
        "data": null,
        "title": "<span data-args='job_title'>Job Title</span>",
        "sClass": "actioncol",
        orderable: false,
        "render": function (data, type, row) {
            var datastring = '';
            $.each(row.jobTitle, function (key, value) {
                datastring = datastring + value.job_title_name + '<br>';
            });

            return datastring;
        }
    }, {
        "data": "total_budget",
        "title": "<span data-args='total_budget'>Total Budget</span>",
        "sClass": "",
        orderable: true
    },
    {
        "data": "status",
        "title": "<span data-args='job_title'>Status</span>",
        "sClass": "actioncol",
        orderable: false,
        "render": function (data, type, row) {
            var datastring = '';

            if (data === "1") {
                datastring = "<span class='bluetext'>WAITING FOR APPROVALL</span>";
            } else if (data === "2") {
                datastring = "<span class='bluetext'>WAITING FOR NEXT APPROVAL</span>";
            } else if (data === "3") {
                datastring = "<span class='greentext'>APPROVED</span>";
            } else if (data === "4") {
                datastring = "<span class='redtext'>REJECT</span>";
            }else {
                datastring = "<span class='bluetext'>WAITING FOR APPROVALL</span>";
            }

            return datastring;
        }
    },
    {//class='translate' 
        "data": null,
        "title": "<span data-args='approved_by'>Approved By</span>",
        "sClass": "actioncol",
        orderable: false,
        "render": function (data, type, row) {

            var datastring = '';
            $.each(row.approvalReal, function (key, value) {
                if (value.statusApproved == 3 || value.statusApproved == "3") {
                    datastring = datastring + value.approved_by + '<br>';
                }
            });

            return datastring;
        }
    },
    {
        "data": null,
        "title": "<span data-args='approved_date'>Approved Date</span>",
        "sClass": "actioncol",
        orderable: false,
        "render": function (data, type, row) {

            var datastring = '';
            $.each(row.approvalReal, function (key, value) {
                if (value.statusApproved == 3 || value.statusApproved == "3") {
                    datastring = datastring + value.created_on + '<br>';
                }
            });

            return datastring;
        }
    },
    {
        "data": null,
        "title": "<span class='translate'  data-args='app-action'>Actions</span>",
        "sClass": "actioncol",
        orderable: false,
        "render": function (data, type, row) {
            var retval = '';
            if (row.status == "2" || row.status == 2) {
                retval = '';
                retval += '<span class="dropdown">';
                retval += '<a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">';
                retval += '<i class="la la-ellipsis-h"></i>';
                retval += '</a>';
                retval += '<div class="dropdown-menu dropdown-menu-right">';
                retval += '<a class="dropdown-item fakelink" href="/Hris/Mpp/update/' + row.id + '"><i class="la la-edit"></i> Details</a>';
                retval += '<a class="dropdown-item" onClick="fnMpp.delete(\'' + row.id + '\')" href="javascript:void(0)"><i class="la la-trash"></i> Delete</a>';
                retval += '</div>';
                retval += '</span>';
            } else {
                retval = '';
            }
            
            return retval;

        }
    }   
];

fnMpp = {
    generatelist: function () {
        InitTable(tableList, ajaxUrlList, paramsSearchIndex, columnsIndex, 'id', 'asc');
        setcolvis(tableList, columnsIndex);
    },
    reloadlist: function () {
        TABLES[tableList].ajax.reload(null, false);
    },
    search: function () {
        $("#" + modalSearch).show();
    },
    initsearch: function (x) {
        if (x === "reset") {
            $("#" + formSearch)[0].reset();
        }
        TABLES[tableList].ajax.reload();
        $("#" + modalSearch).modal('hide');
    },
    delete: function (id) {
        var text = t_delete;
        swal({
            title: "Confirmation",
            text: t_delete,
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            reverseButtons: !0
        }).then(function (e) {
            if (e.value) {
                ajaxGet(ajaxUrlDelete + "/" + id, {}, function (response) {
                    response = parseJson(response);
                    if (response.success) {
                        
                        SuccessNotif(t_deletesuccess);
                        fnMpp.reloadlist();
                        hideAllModal();
                    } else {
                        DangerNotif(response.Message);
                    }
                });
            }


        });

    }
};

$(function () {
    fnMpp.generatelist();
    initlanguage(lang_hr);
});
 

