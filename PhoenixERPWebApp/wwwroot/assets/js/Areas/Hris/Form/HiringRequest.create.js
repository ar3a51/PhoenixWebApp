var webUrl = WEB + "/Hris/HiringRequest";
var ajaxUrl = API + "Hris/HiringRequest";
var ajaxUrlCreate = ajaxUrl + "/Create";
var xform = "form-vendor";
var UrlGetRequestData = API + "Hris/HiringRequest/GetDataRequester";
var ajaxUrlListJobTitle = API + "Hris/JobTitle/List/";
var ajaxUrlListHiringCheck = API + "Hris/HiringRequest/GetListHiringMppDetail";
var ajaxUrlMppDetailGet = API + "Hris/Mppdetail/Get";
var ajaxUrlHiringRequestDetailGet = API + "Hris/EmployeeBasicInfo/GetWithDetail";
var ajaxUrlGetGradeData = API + "Hris/HiringRequest/GetGradeDataInBusinessUnit";
var VAR_BUGETED = "Budgeted";
var VAR_UNBUGEDTED = "Unbudgeted";
var VAR_REPLACEMENT = "Replacement";
var ajaxUrlGetTempateApprovalLink = API + "General/TmTemplateApproval/List"; 
var tableList = "HiringRequestListApprover";
var UrlSessionData = API + "employeeInfo/getByUserLogin";
var ajaxUrlGetEmployeBasicWithDevId = API + "Hris/EmployeeBasicInfo/GetWithDivisionId";

$(function () {
   
    initlanguage(lang_hr);
    $('.dtpicker').datetimepicker();

    ajaxGet(UrlSessionData, {}, function (responseSession) {
        responseSession = parseJson(responseSession);
        //console.log(responseSession);
        $("#BusinessUnitId").val(responseSession.data.businessUnitId);
        $("#EmployeeId").val(responseSession.data.employeeId);
        $("#requester_employee_basic_info_id").val(responseSession.data.employeeId);
        if (responseSession.data != null) {
            ajaxGet(UrlGetRequestData + "/" + responseSession.data.employeeId, {}, function (response) {
                if (response.data === null) {
                    DangerNotif("No Data Avaliable");
                } else {
                    parseJson(response);

                    $('#code').val("REQ" + getDateNow() + randomIntFromInterval(1, 9999999));
                    $('#requester').val(response.data.requester_name);
                    $('#job_title').val(response.data.job_title_name);
                    $('#basic_info_grade_tittle').val(response.data.job_title_name);
                    $('#division').val(response.data.business_unit_name);
                    $('#select_job_title').setcombobox({
                        data: { 'rows': 100 },
                        url: ajaxUrlListJobTitle,
                        searchparam: 'Search_title_name',
                        labelField: 'title_name',
                        valueField: 'id',
                        searchField: 'title_name'
                    });
                }
            });
        } else {
            swal('Information', 'Login User Data Not Valid!', 'info');
            setTimeout(
                function () {
                    //do something special
                    window.location.href = WEB + "/Core/Authentication?status=Logout";
                }, 2000);
        }
        
    });

    var optionsForm = GetOptionsForm(function () {
        var xret = $("#" + xform).valid();
        if (xret) startprocess();
        return xret;
    }, function (response, statusText, xhr, $form) {
        endprocess();
        response = parseJson(response);
        if (response.success) {
            SuccessNotif(t_createsuccess);
            var data = {
                refId: response.data.id,
                detailLink: "/Hris/HiringRequest/Approval/" + response.data.id,
                tName: "hr.HiringRequest",
                menuId: "4ed5d07d-ff65-415b-873e-429b4eadfa3a",
                IdTemplate: $("#template_approval_id").val()
            };
            saveTemplateApprovalHris(data, webUrl);
           
            //window.location.href = WEB + "/hris/HiringRequest";
        } else {
            DangerNotif(response.Message);
        }
    });
    InitForm(xform, optionsForm);
    setFormAction(xform, ajaxUrlCreate);

    
   

    
    var rep_table = "";
    $('#checkbox_replacement').click(function () {
        if ($(this).is(':checked')) {
            $("#label_replacement").css({ display: "block" });
            $("#budgeted_div").css({ display: "none" });
            $("#label_budgeted").css({ display: "block" });
            $('#inner_label_budgeted').html('<strong>Replacement</strong>');  
            $('#this_title_is').html('&nbsp');  
            $("#replacement_div").css({ display: "block" });
            $('#hiring_request_type_id').val(VAR_REPLACEMENT);
            var emps = "";
            ajaxGet(ajaxUrlGetEmployeBasicWithDevId + "/" + $("#BusinessUnitId").val(), {}, function (response) {
                response = parseJson(response);
                console.log(response);
                $("#table_replacement > tbody").html("");
                rep_table = "";
                $.each(response.rows, function (index, value) {
                    // console.log(value);
                    if (value.employee_status === null || value.employee_status === "") {
                        emps = "Unset Status Employee";
                    } else {
                        emps = value.employee_status;
                    }
                    rep_table = rep_table + "<tr id=" + value.id + "><td>" + value.name_employee + "</td><td>" + value.job_title + "</td><td>" + value.grade + "</td><td>" + emps + "</td><td style=\"text-align:center;vertical-align:middle\"><label class=\"m-radio m-radio--state-success\"><input  type=\"radio\" data-toggle=\"modal\" data-target=\"#HiringRequestModalDetailReplacement\" onclick=\"clearModalDetialReplacement('" + value.id + "')\"><span></span></label></td></tr>";
                    //console.log(budgted_table);
                });
                $("#table_replacement tbody").append(rep_table);
                ajaxGet(ajaxUrlGetTempateApprovalLink, { Search_MenuName: "Hiring Request", Search_BusinessUnitId: $("#BusinessUnitId").val(), Search_TemplateName: "Hiring Request Unbudgeted IRIS A / BRANDCOM" }, function (response) {
                    response = parseJson(response);
                    console.log(response);
                    if (response.rows.length > 0) {
                        Deleteaja('no_data');
                        var count = 1;
                        $("#HiringRequestListApprover > tbody").html("");
                        $.each(response.rows[0].listTmUserApproval, function (index, value) {
                            AddRowTable(count, value.job_title_name, value.employee_full_name);
                            count = count + 1;
                        });
                        $("#template_approval_id").val(response.rows[0].id);
                    } else {
                        swal('Information', 'Please Create Template Approval for this Division First !', 'info');
                        setTimeout(
                            function () {
                                //do something special
                                window.location.href = WEB + "/hris/Mpp";
                            }, 2000);
                    }

                });
            });
        } else {
            $("#budgeted_div").css({ display: "block" });
            $("#label_replacement").css({ display: "block" });
            $("#label_budgeted").css({ display: "block" });
            $('#inner_label_budgeted').html('<strong>Budgeted</strong>');
            $('#this_title_is').html('This Title is '); 
            $("#replacement_div").css({ display: "none" });
            $('#hiring_request_type_id').val(VAR_BUGETED);
        }
    });
  
});

function process() {
    var budgted_table = "";
    $("#budgeted_div").css({ display: "none" });
    $("#unbudgeted_div").css({ display: "none" });
    $("#label_budgeted").css({ display: "none" });
    $("#table_budeted tbody").empty(); 
    ajaxPost(ajaxUrlListHiringCheck, { business_unit_id: $('#BusinessUnitId').val(), job_tittle_id: $('#select_job_title').val() }, function (response) {
        response = parseJson(response);
        //console.log(response);
        if (response.success) {
            budgted_table = '';
            if (response.recordsTotal > 0) {
                $("#budgeted_div").css({ display: "block" });
                $("#label_replacement").css({ display: "block" });
                $("#label_budgeted").css({ display: "block" });
                $('#inner_label_budgeted').html('<strong>Budgeted</strong>');    
                $.each(response.rows, function (index, value) {
                   // console.log(value);
                    budgted_table = budgted_table + "<tr id=" + value.id + "><td>" + value.job_title_name + "</td><td>" + value.job_grade_name + "</td><td>" + value.ttf + "</td><td>" + value.employment_status + "</td><td>" + value.budget_souce + "</td><td> Rp. " + value.mpp_budged + "</td><td style=\"text-align:center;vertical-align:middle\"><label class=\"m-radio m-radio--state-success\"><input  type=\"radio\" data-toggle=\"modal\" data-target=\"#HiringRequestModalDetail\" onclick=\"clearModalDetialBudgeted('" + value.id + "')\"><span></span></label></td></tr>";
                    //console.log(budgted_table);
                });
                $("#table_budeted tbody").append(budgted_table);
                $('#hiring_request_type_id').val(VAR_BUGETED);
                ajaxGet(ajaxUrlGetTempateApprovalLink, { Search_MenuName: "Hiring Request", Search_BusinessUnitId: $("#BusinessUnitId").val(), Search_Budgeted : VAR_BUGETED }, function (response) {                
                    response = parseJson(response);
                    console.log(response);
                    if (response.rows.length > 0) {
                        Deleteaja('no_data');
                        var count = 1;
                        $("#HiringRequestListApprover > tbody").html("");
                        $.each(response.rows[0].listTmUserApproval, function (index, value) {
                            AddRowTable(count, value.job_title_name, value.employee_full_name);
                            count = count + 1;
                        });
                        $("#template_approval_id").val(response.rows[0].id);
                    } else {
                        swal('Information', 'Please Create Template Approval for this Division First !', 'info');
                        setTimeout(
                            function () {
                                //do something special
                                window.location.href = WEB + "/hris/Mpp";
                            }, 2000);
                    }

                });
               // HrHiringRequestCreate.generatelist();
            } else {
                $("#label_replacement").css({ display: "none" });
                $("#unbudgeted_div").css({ display: "block" });
                $("#label_budgeted").css({ display: "block" });
                $('#inner_label_budgeted').html('<strong>Unbudgeted</strong>');
                $('#hiring_request_type_id').val(VAR_UNBUGEDTED);

                if ($("#BusinessUnitId").val() == "3062e5d2-96d5-4344-a5c9-9b6d2eca24bd") {
                    ajaxGet(ajaxUrlGetTempateApprovalLink, { Search_MenuName: "Hiring Request", Search_BusinessUnitId: $("#BusinessUnitId").val(), Search_TemplateName: "Hiring Request Unbudgeted IRIS A / BRANDCOM" }, function (response) {
                        response = parseJson(response);
                        console.log(response);
                        if (response.rows.length > 0) {
                            Deleteaja('no_data');
                            var count = 1;
                            $("#HiringRequestListApprover > tbody").html("");
                            $.each(response.rows[0].listTmUserApproval, function (index, value) {
                                AddRowTable(count, value.job_title_name, value.employee_full_name);
                                count = count + 1;
                            });
                            $("#template_approval_id").val(response.rows[0].id);
                        } else {
                            swal('Information', 'Please Create Template Approval for this Division First !', 'info');
                            setTimeout(
                                function () {
                                    //do something special
                                    window.location.href = WEB + "/hris/Mpp";
                                }, 2000);
                        }

                    });
                } else {

                    ajaxGet(ajaxUrlGetTempateApprovalLink, { Search_MenuName: "Hiring Request", Search_BusinessUnitId: $("#BusinessUnitId").val(), Search_TemplateName: "Hiring Request Unbudgeted Nava" }, function (response) {
                        response = parseJson(response);
                        console.log(response);
                        if (response.rows.length > 0) {
                            Deleteaja('no_data');
                            var count = 1;
                            $("#HiringRequestListApprover > tbody").html("");
                            $.each(response.rows[0].listTmUserApproval, function (index, value) {
                                AddRowTable(count, value.job_title_name, value.employee_full_name);
                                count = count + 1;
                            });
                            $("#template_approval_id").val(response.rows[0].id);
                        } else {
                            swal('Information', 'Please Create Template Approval for this Division First !', 'info');
                            setTimeout(
                                function () {
                                    //do something special
                                    window.location.href = WEB + "/hris/Mpp";
                                }, 2000);
                        }

                    });

                }

                $('#job_title_unbudgeted').append("<option value=" + $('#select_job_title').val() + ">" + $("#select_job_title option:selected").text()+"</option>");
                $('#job_title_unbudgeted').setcombobox({
                    data: { 'rows': 100 },
                    url: ajaxUrlListJobTitle,
                    searchparam: 'Search_title_name',
                    labelField: 'title_name',
                    valueField: 'id',
                    searchField: 'title_name'
                });

            }
        } else {
            DangerNotif(response.Message);
        }
    });

}

function clearModalDetialBudgeted(id) {
    var xformdetil = "HiringRequestFormDetail";
    ajaxGetJquery(ajaxUrlMppDetailGet + "/" + id, {}, function (response) {
        if (response.data === null) {
            DangerNotif("No Data Avaliable");
        } else {
            parseJson(response);
            console.log(response.data);
            var optionsForm = GetOptionsForm(function () {
                var xret = $("#" + xform).valid();
                if (xret) startprocess();
                return xret;
            });
            InitForm(xformdetil, optionsForm);
            $("#time_to_fullfill").val(response.data.ttf);
            $("#employment_status").val(response.data.employment_status);
            $("#job_title_nm").val(response.data.job_title_name);
            $("#budget_source").val(response.data.budget_souce);
            $("#Job_Grade").val(response.data.job_grade_name);
            $("#job_grade_idu").val(response.data.job_grade_id);
            $("#mppBudget").val(response.data.mpp_budged);
            $("#detail_spesification").val(response.data.desc);
            $("#job_title_id").val(response.data.job_tittle_id);
            $("#porject_name").val(response.data.porject_name);
            $("#mpp_id").val(response.data.mpp_id);
            $("#Id").val(id);
        }
    });
}

function clearModalDetialReplacement(id) {
    var xformdetil = "HiringRequestFormDetailReplacement";
    ajaxGetJquery(ajaxUrlHiringRequestDetailGet + "/" + id, {}, function (response) {
        if (response.data === null) {
            DangerNotif("No Data Avaliable");
        } else {
            parseJson(response);
            console.log(response.data);
            var optionsForm = GetOptionsForm(function () {
                var xret = $("#" + xform).valid();
                if (xret) startprocess();
                return xret;
            });
            InitForm(xformdetil, optionsForm);
            if (response.data.employement_status_name == null || response.data.employement_status_name == "") {
                $("#employment_statusr").val("Unset Status Employee");
            }
            $("#employment_statusr").val(response.data.employement_status_name);
            $("#job_title_nmr").val(response.data.bussinesUnitJobLevel.job_title_name);
            $("#Job_Grader").val(response.data.bussinesUnitJobLevel.job_grade_name);
            $("#job_grade_idur").val(response.data.bussinesUnitJobLevel.job_grade_id);
            $("#job_title_idr").val(response.data.bussinesUnitJobLevel.job_title_id);
           
            $("#employee_name_replace").val(response.data.name_employee);
            $("#employee_id_replace").val(response.data.id);

            $("#mppBudgetr").val("90000000");
            $("#porject_namer").val("");

        }
    });
}

function randomIntFromInterval(min, max) // min and max included
{
    return Math.floor(Math.random() * (max - min + 1) + min);
}
function getDateNow() {
    var d = new Date();
    var strDate = d.getFullYear() + (d.getMonth() + 1) + d.getDate();

    return strDate;
}

function SaveDetilReplacement() {
    $('#tmpmppDetail').empty();

    if ($('#dffr').val() !== "" && $('#detail_requirementr').val() !== "" ) {
       
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][jobtitle]" value="' + $('#job_title_idr').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][status]" value="' + $('#budget_sourcer').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][mppbudget]" value="' + $('#mppBudgetr').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][grade]" value="' + $('#job_grade_idur').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][project]" value="' + $('#porject_namer').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][desc]" value="' + $('#detail_spesificationr').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][jobtitle_name]" value="' + $('#job_title_nmr').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][budget_souce]" value="' + $('#budget_sourcer').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][dff]" value="' + $('#dffr').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][Id]" value="' + $('#Idr').val() + '"  >');
        $('#replacement_employee_basic_info_idr').val($('#employee_id_replace').val());
        $('#HiringRequestModalDetailReplacement').modal('toggle');

        //console.log($('#tmpmppDetail').val());
   
    } else {
        alert('Please Insert Date of fullfillment or detail requirement first!');
        return false;
    }
}

function SaveDetilBudget() {
    $('#tmpmppDetail').empty();

    if ($('#dff').val() !== "" && $('#detail_requirement').val() !== "") {

        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][jobtitle]" value="' + $('#job_title_id').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][ttf]" value="' + $('#time_to_fullfill').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][status]" value="' + $('#budget_source').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][mppbudget]" value="' + $('#mppBudget').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][grade]" value="' + $('#job_grade_idu').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][project]" value="' + $('#porject_name').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][desc]" value="' + $('#detail_spesification').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][descresponsibilites]" value="' + $('#detail_requirement').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][jobtitle_name]" value="' + $('#job_title_nm').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][budget_souce]" value="' + $('#budget_source').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][dff]" value="' + $('#dff').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][Id]" value="' + $('#Id').val() + '"  >');
        $('#HiringRequestModalDetail').modal('toggle');

        //console.log($('#tmpmppDetail').val());

    } else {
        alert('Please Insert Date of fullfillment or detail requirement first!');
        return false;
    }
}


function SaveDetilUnBudget() {
    $('#tmpmppDetail').empty();

    if ($('#dff_ub').val() !== "" && $('#detail_requirement_ub').val() !== "") {

        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][jobtitle]" value="' + $('#job_title_id_ub').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][ttf]" value="' + $('#time_to_fullfill_ub').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][status]" value="' + $('#budget_source_ub').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][mppbudget]" value="' + $('#mppBudget_ub').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][grade]" value="' + $('#job_grade_id_ub').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][project]" value="' + $('#porject_name_ub').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][desc]" value="' + $('#detail_spesification_ub').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][descresponsibilites]" value="' + $('#detail_requirement_ub').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][jobtitle_name]" value="' + $('#job_title_name_ub').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][budget_souce]" value="' + $('#budget_source_ub').val() + '"  >');
        $('#tmpmppDetail').append('<input type="hidden" name="dataDetailMpp[0][dff]" value="' + $('#dff_ub').val() + '"  >');
        $('#HiringRequestModalDetailUnbugeted').modal('toggle');

        

    } else {
        alert('Please Insert Date of fullfillment or detail requirement first!');
        return false;
    }
}

function setDataUnbudeted() {
    var xformdetil = "HiringRequestFormDetailUnbugeted";
    ajaxPost(ajaxUrlGetGradeData, { business_unit_id: $('#BusinessUnitId').val(), job_tittle_id: $('#select_job_title').val() }, function (response) {
        if (response.data === null) {
            DangerNotif("No Position Avalible, Please Create Position First");       
            return false;
        } else {
            $('#HiringRequestModalDetailUnbugeted').modal('show');
            parseJson(response);
            var optionsForm = GetOptionsForm(function () {
                var xret = $("#" + xform).valid();
                if (xret) startprocess();
                return xret;
            });
            InitForm(xformdetil, optionsForm);
            $('#job_title_id_ub').val($('#select_job_title').val());
            $('#job_title_name_ub').val($("#select_job_title option:selected").text());
            $('#Job_Grade_ub').val(response.data.job_grade_name);
            $('#job_grade_id_ub').val(response.data.job_grade_id);
        }
        
    });

}

function AddRowTable(no, title, nama) {
    var acak = randomIntFromInterval(1, 999);
    if (title == null) {
        title = "Undefined Jobtitle";
    }
    var markup = "<tr id=" + acak + "><td>" + no + "</td><td>" + title + "</td><td>" + nama + "</td></tr>";
    $("#HiringRequestListApprover tbody").append(markup);
}


function Deleteaja(id) {
    $('table#HiringRequestListApprover tr#' + id + '').remove();
}

