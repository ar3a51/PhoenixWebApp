var ajaxUrl = API + "/Hris/CandidateRecruitmentInterview";
var ajaxUrlList = ajaxUrl + "/List";
var ajaxUrlUpdate = ajaxUrl + "/Update";
var ajaxUrlCreate = ajaxUrl + "/Create";
var ajaxUrlDelete = ajaxUrl + "/Delete";
var ajaxUrlGet = ajaxUrl + "/Get";
var ajaxUrlGetCentral = API + "/Hris/JobVacancy/Get";
var ajaxUrlGetCandidateReq = API + "/Hris/CandidateRecruitment/Get";
var ajaxUrlGetHiringRequest = API + "Hris/HiringRequest/GetListHiringMppWithID";
var ajaxUrlGetEducationInfo = API + "/Hris/CandidateRecruitmentEducationInfo/Get";
var tableList = "EducationInfoList";
var formSearch = "ApplicantProgressFormSearch";
var modalSearch = "ApplicantProgressModalSearch";
var ajaxUrlListJobTitle = API + "Hris/JobTitle/List/";
var pageheader = "";
var paramsSearchIndex = function () {
    var searchdata = $("#" + formSearch).serializeArray();
    var data = { Search_status: 1 };
    $(searchdata).each(function (index, obj) {
        if (obj.name !== "__RequestVerificationToken") {
            data[obj.name] = obj.value;
        }
    });
   
    return data;
}


$(function () {

    initlanguage(lang_hr);

    var xid = $('#id').val();
    var candidate_recruitment_id = $('#candidate_recruitment_id').val();
    var job_vacancy_id = $('#job_vacancy_id').val();

    var xform = "EducationInfoUpdateFormUpdate";
    var optionsForm = GetOptionsForm(function () {
        var xret = $("#" + xform).valid();
        if (xret) startprocess();
        return xret;
    }, function (response, statusText, xhr, $form) {
        endprocess();
        response = parseJson(response);
        if (response.success) {
            SuccessNotif(t_createsuccess);
            window.location.href = WEB + "/hris/ApplicantDetail/EducationInfo/" + candidate_recruitment_id + "/" + job_vacancy_id;
        } else {
            DangerNotif(response.Message);
        }
    });
    InitForm(xform, optionsForm);
    setFormAction(xform, ajaxUrlUpdate);

    ajaxGet(ajaxUrlGetCentral + "/" + job_vacancy_id, {}, function (response) {
        response = parseJson(response);
        //console.log(response);
        $("#vacancy_name").val(response.data.vacancy_name);
        $("#posting_code").val(response.data.code);
       
        var dataraw = '';
        if (response.data.posting_status === "0") {
            dataraw = "DRAFT";
        } else if (response.data.posting_status === "1") {
            dataraw = "APPROVED";
        } else if (response.data.posting_status === "2") {
            dataraw = "REJECT";
        } else if (response.data.posting_status === "3") {
            dataraw = "UNPOSTING";
        } else if (response.data.posting_status === "4") {
            dataraw = "POSTED";
        } else {
            dataraw = "CLOSED";
        }
        $("#posting_status").val(dataraw);
        ajaxGet(ajaxUrlGetCandidateReq + "/" + candidate_recruitment_id, {}, function (response) {
            response2 = parseJson(response);
            //console.log(response2);

            var dataraw = '';
            if (response2.data.applicant_progress_id.trim() === "0") {
                dataraw = "New Applicant";
            } else if (response2.data.applicant_progress_id.trim() === "1") {
                dataraw = "Interview";
            } else if (response2.data.applicant_progress_id.trim() === "2") {
                dataraw = "Job Offered";
            } else if (response2.data.applicant_progress_id.trim() === "3") {
                dataraw = "Approval";
            } else if (response2.data.applicant_progress_id.trim() === "4") {
                dataraw = "Accepted";
            } else if (response2.data.applicant_progress_id.trim() === "5") {
                dataraw = "KIV / Tallent Pool";
            } else {
                dataraw = "Rejected";
            }

            $("#recruitment_status").val(dataraw);
            $("#applicant_code").val(response2.data.code);

        });
        ajaxGet(ajaxUrlGetEducationInfo + "/" + xid, {}, function (response) {
            response2 = parseJson(response);
            //console.log(response2);
            $("#last_education").val(response2.data.last_education);
            $("#major").val(response2.data.major);
            $("#institution_name").val(response2.data.institution_name);
            $("#location").val(response2.data.location);
            $("#year").val(response2.data.year);
            if (response2.data.year !== "undefined" && response2.data.year!==null) {
                var tmp = response2.data.year;
                var result = tmp.split('-');
                $("#year_from").val(parseInt(result[0]));
                $("#year_to").val(parseInt(result[1]));
            }
            
        });


    });
});

function SubmitEditDataEducation() {
    $('#year').val($('#year_from').val() + " - " + $('#year_to').val());
    $('#EducationInfoUpdateFormUpdate').submit();
}


 

