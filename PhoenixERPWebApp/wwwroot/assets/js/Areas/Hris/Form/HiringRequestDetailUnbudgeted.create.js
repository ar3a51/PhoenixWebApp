 
var webUrl = WEB + "/Hris/HiringRequestDetailUnbudgeted";
var ajaxUrl = API + "/Hris/HiringRequestDetailUnbudgeted";
var ajaxUrlCreate = ajaxUrl + "/Create";
var xform = "HiringRequestDetailUnbudgetedFormCreate";
 

$(function () {
    var optionsForm = GetOptionsForm(function () {
        var xret = $("#" + xform).valid();
        if (xret) startprocess();
        return xret;
    }, function (response, statusText, xhr, $form) {
        endprocess();
            response = parseJson(response);
            if (response.success) {
                SuccessNotif(t_createsuccess);
                redirecttolink(webUrl)
            } else {
                DangerNotif(response.Message);
            }
        });
    InitForm(xform, optionsForm);
    setFormAction(xform, ajaxUrlCreate);
    initlanguage(lang_hr);
});
