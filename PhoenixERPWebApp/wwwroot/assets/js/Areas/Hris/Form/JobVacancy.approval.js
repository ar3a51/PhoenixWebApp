var ajaxUrl = API + "/Hris/JobVacancy";
var ajaxUrlList = ajaxUrl + "/List";
var ajaxUrlUpdate = ajaxUrl + "/UpdatePostingStatus";
var ajaxUrlCreate = ajaxUrl + "/Create";
var ajaxUrlDelete = ajaxUrl + "/Delete";
var ajaxUrlGet = ajaxUrl + "/Get";
var ajaxUrlGetHiringRequest = API + "Hris/HiringRequest/GetListHiringMppWithID";




$(function () {
    initlanguage(lang_hr);
    var xid = $('#id').val();
    ajaxGet(ajaxUrlGet + "/" + xid, {}, function (response) {
        response = parseJson(response);
        $('#code').val(response.data.code);
        $('#vacancy_name').val(response.data.vacancy_name);
        $('#master_location_id').val(response.data.master_location_id);
        $('#master_location_id').val(response.data.master_location_name);
        $('#posting_status').val(response.data.posting_status);
        var dataraw = '';
        if (response.data.posting_status === "0") {
            dataraw = "DRAFT";
        } else if (response.data.posting_status === "1") {
            dataraw = "APPROVED";
        } else if (response.data.posting_status === "2") {
            dataraw = "REJECT";
        } else if (response.data.posting_status === "3") {
            dataraw = "UNPOSTING";
        } else if (response.data.posting_status === "4") {
            dataraw = "POSTED";
        } else {
            dataraw = "CLOSED";
        }
        $('#posting_status_name').val(dataraw);
        ajaxGet(ajaxUrlGetHiringRequest + "/" + response.data.hiring_request_id, {}, function (response) {
            response2 = parseJson(response);
            console.log(response2);
            $('#hiring_request_id').val(response2.data.id);
            $('#hiring_request_code').val(response2.data.code);
            $('#hiring_request_id').val(response2.data.id);
            $('#hiring_request_code').val(response2.data.code);
            $('#requester_id').val(response2.data.requester_employee_basic_info_id);
            $('#requester_name').val(response2.data.requester_name);
            $('#job_title_name').val(response.data.job_title_name + "(" + response.data.job_grade_name+")");
            $('#job_title_id').val(response2.data.job_title_id);
            $('#business_unit_id').val(response2.data.business_unit_id);
            $('#business_unit_name').val(response2.data.business_unit_name);

            budgted_table = "<tr id=" + response2.data.id + "><td>" + response2.data.job_title_name + "</td><td>" + response2.data.job_grade_name + "</td><td>" + response2.data.ttf + "</td><td>" + response2.data.employment_status + "</td><td>" + response2.data.budget_souce + "</td><td> Rp. " + response2.data.mpp_budged + "</td><td style=\"text-align:center;vertical-align:middle\">" + response2.data.date_fulfilment + "</td></tr>";
            $("#dt_basic tbody").append(budgted_table);
        });
    });
});

function ApprovedData() {
    var text = "Do you want to Approved this Data";
    swal({
        title: "Confirmation",
        text: text,
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        reverseButtons: !0
    }).then(function (e) {
        if (e.value) {
            ajaxPost(ajaxUrlUpdate, { id: $('#id').val(), posting_status: 4 }, function (response) {
                response = parseJson(response);
                if (response.success) {
                    SuccessNotif("Approval Sucess");
                    window.location.href = WEB + "/hris/JobVacancy";
                } else {
                    DangerNotif(response.Message);
                }
            });
        }
    });

}

function RejectData() {
    $('#JobVacancyApprovedModalSearch').modal('toggle');

    var reason1 = $('#reject_reason').val();
    ajaxPost(ajaxUrlUpdate, { id: $('#id').val(), posting_status: 2}, function (response) {
        response = parseJson(response);
        if (response.success) {
            SuccessNotif("Reject Sucess");
            window.location.href = WEB + "/hris/JobVacancy";
        } else {
            DangerNotif(response.Message);
        }
    });

}

 

