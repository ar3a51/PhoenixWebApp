 var webUrl = WEB + "/Hris/JobVacancy";
var ajaxUrl = API + "/Hris/JobVacancy";
var ajaxUrlUpdate = ajaxUrl + "/UpdatePostingStatus";
var xform = "JobVacancyFormUpdate";
var ajaxUrlGet = ajaxUrl + "/GetWithDetial"; 
var ajaxUrlGetHiringRequest = API + "Hris/HiringRequest/GetListHiringMppWithID";

$(function () {
    var xid = $('#id').val();
    ajaxGet(ajaxUrlGet + "/" + xid, {}, function (response) {
        response = parseJson(response);
        $('#code').val(response.data.code);
        $('#vacancy_name').val(response.data.vacancy_name);
        $('#master_location_id').val(response.data.master_location_id);
        $('#master_location_name').val(response.data.location.location_name);
        $('#posting_status option[value="' + response.data.posting_status + '"]').prop('selected', true);
        ajaxGet(ajaxUrlGetHiringRequest + "/" + response.data.hiring_request_id, {}, function (response) {
            response2 = parseJson(response);
            console.log(response2);
            $('#hiring_request_id').val(response2.data.id);
            $('#hiring_request_code').val(response2.data.code);
            $('#hiring_request_id').val(response2.data.id);
            $('#hiring_request_code').val(response2.data.code);
            $('#requester_id').val(response2.data.requester_employee_basic_info_id);
            $('#requester_name').val(response2.data.requester_name);
            $('#job_title_name').val(response.data.job_title_name + "(" + response.data.job_grade_name + ")");
            $('#job_title_id').val(response2.data.job_title_id);
            $('#business_unit_id').val(response2.data.business_unit_id);
            $('#business_unit_name').val(response2.data.business_unit_name);

            budgted_table = "<tr id=" + response2.data.id + "><td>" + response2.data.job_title_name + "</td><td>" + response2.data.job_grade_name + "</td><td>" + response2.data.ttf + "</td><td>" + response2.data.employment_status + "</td><td>" + response2.data.budget_souce + "</td><td> Rp. " + response2.data.mpp_budged + "</td><td style=\"text-align:center;vertical-align:middle\">" + response2.data.date_fulfilment + "</td></tr>";
            $("#dt_basic tbody").append(budgted_table);
        });
    });
    
    
});


function UpdateData() {
    var text = "Do you want to Update this Data";
    swal({
        title: "Confirmation",
        text: text,
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        reverseButtons: !0
    }).then(function (e) {
        if (e.value) {
            ajaxPost(ajaxUrlUpdate, { id: $('#id').val(), posting_status: $('#posting_status').val() }, function (response) {
                response = parseJson(response);
                if (response.success) {
                    SuccessNotif("Update Sucess");
                    window.location.href = WEB + "/hris/JobVacancy";
                } else {
                    DangerNotif(response.Message);
                }
            });
        }
    });

}
  
