var webUrl = WEB + "/Hris/CandidateRecruitment";
var ajaxUrl = API + "/Hris/CandidateRecruitment";
var ajaxUrlCreate = ajaxUrl + "/CreateDetil";
var xform = "TrackingNewApplicantFormCreate";


$(function () {
    var optionsForm = GetOptionsForm(function () {
        var xret = $("#" + xform).valid();
        if (xret) startprocess();
        return xret;
    }, function (response, statusText, xhr, $form) {
        endprocess();
        response = parseJson(response);
        if (response.success) {
            SuccessNotif(t_createsuccess);
            window.location.href = WEB + "/hris/Applicant/TrackingNewApplicant/" + $('#job_vacancy_id').val();
        } else {
            DangerNotif(response.Message);
        }
    });
    InitForm(xform, optionsForm);
    setFormAction(xform, ajaxUrlCreate);
    initlanguage(lang_hr);

    $('#cv').setuploadbox({});
    $('#photo').setuploadbox({});
    $('#portfolio').setuploadbox({});
});
