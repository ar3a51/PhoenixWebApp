var webUrl = WEB + "/Hris/CandidateRecruitment";
var ajaxUrl = API + "/Hris/CandidateRecruitment";
var ajaxUrlCreate = ajaxUrl + "/Update";
var ajaxUrlGet = ajaxUrl + "/Get";
var xform = "TrackingNewApplicantFormUpdate";


$(function () {
    var xid = $('#id').val();
    var vacancy_id = $('#job_vacancy_id').val();
    var optionsForm = GetOptionsForm(function () {
        var xret = $("#" + xform).valid();
        if (xret) startprocess();
        return xret;
    }, function (response, statusText, xhr, $form) {
        endprocess();
        response = parseJson(response);
        if (response.success) {
            SuccessNotif(t_createsuccess);
            window.location.href = WEB + "/hris/Applicant/TrackingNewApplicant/" + $('#job_vacancy_id').val();
        } else {
            DangerNotif(response.Message);
        }
    });
    InitForm(xform, optionsForm);
    setFormAction(xform, ajaxUrlCreate);
    initlanguage(lang_hr);

    $('#cv').setuploadbox({});
    $('#photo').setuploadbox({});
    $('#portfolio').setuploadbox({});

    ajaxGet(ajaxUrlGet + "/" + xid, {}, function (response) {
        response = parseJson(response);
        $('#first_name').val(response.data.first_name);
        $('#last_name').val(response.data.last_name);
        $('#nick_name').val(response.data.nick_name);
        $('#code').val(response.data.code);
        $('#applicant_progress_id').val(response.data.applicant_progress_id); 
        var $radiosgender = $('input:radio[name=gender]');
        var cekgender = response.data.gender;
        if (cekgender === "male") {
            $radiosgender.filter('[value=male]').prop('checked', true);
        } else {
            $radiosgender.filter('[value=female]').prop('checked', true);
        }
        $('#birth_place').val(response.data.birth_place);
        $('#address').val(response.data.address);
        $('#email').val(response.data.email);
        $('#date_birth').val(response.data.date_birth);
        $('#education_level_id').val(response.data.education_level_id);
        $('#year_experience').val(response.data.year_experience);
        $('#phone_number').val(response.data.phone_number);
        $('#source').val(response.data.source);
        var $radioscigarete = $('input:radio[name=cigaret_willing]');
        var cekcigarete = response.data.cigaret_willing;
        if (cekcigarete === true) {
            $radioscigarete.filter('[value=true]').prop('checked', true);
        } else {
            $radiosgender.filter('[value=false]').prop('checked', true);
        }
        var $radiosalcohol = $('input:radio[name=alcohol_willing]');
        var cekalcohol = response.data.alcohol_willing;
        if (cekalcohol === true) {
            $radiosalcohol.filter('[value=true]').prop('checked', true);
        } else {
            $radiosalcohol.filter('[value=false]').prop('checked', true);
        }
    });
});
