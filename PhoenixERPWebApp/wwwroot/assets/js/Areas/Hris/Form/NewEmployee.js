var ajaxUrl = API + "Hris/CandidateRecruitment";
var ajaxUrlList = ajaxUrl + "/List";
var ajaxUrlUpdate = ajaxUrl + "/Update";
var ajaxUrlCreate = API + "Hris/EmployeeBasicInfo/CreateEmployeeFromCandidate";
var ajaxUrlDelete = ajaxUrl + "/Delete";
var ajaxUrlGetVacancy = API + "Hris/JobVacancy/GetWithDetial";
var ajaxUrlGetCandidateReq = API + "/Hris/CandidateRecruitment/Get";
var ajaxUrllocationGet = API + "Hris/JobVacancy/GetAllLocation";
var ajaxUrllistCategory = API + "Hris/EmployeeChecklistCategory/List";
var ajaxUrlBusinessUnitJobLevel = API + "Hris/BussinesUnitJobLevel/List";
var ajaxUrlGetBusinessUnitJobLevel = API + "Hris/BussinesUnitJobLevel/GetDetail";
var ajaxUrlLegalEntity = API + "General/LegalEntity/List";
var ajaxUrlCandidateRecruitmentEmploymentHistory = API + "Hris/CandidateRecruitmentEmploymentHistory/GetListWithIDCandidate";
var ajaxUrlCandidateRecruitmentEducationInfo = API + "Hris/CandidateRecruitmentEducationInfo/GetListWithIDCandidate";
var tableList = "ApplicantProgressList";
var formSearch = "ApplicantProgressFormSearch";
var modalSearch = "ApplicantProgressModalSearch";
var pageheader = "";
var NewEmployeeChecklist = [];

fnApplicantProgress = {
    reloadlist: function () {
        TABLES[tableList].ajax.reload(null, false);
    },
    search: function () {
        $("#" + modalSearch).show();
    },
    initsearch: function (x) {
        if (x == "reset") {
            $("#" + formSearch)[0].reset();
        }
        TABLES[tableList].ajax.reload();
        $("#" + modalSearch).modal('hide');
    },
    delete: function (id) {
        var text = t_delete;
        swal({
            title: "Confirmation",
            text: t_delete,
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            reverseButtons: !0
        }).then(function (e) {
            if (e.value) {
                ajaxDelete(ajaxUrlDelete + "/" + id, {}, function (response) {
                    response = parseJson(response);
                    if (response.success) {
                        SuccessNotif(t_deletesuccess);
                        fnApplicantProgress.reloadlist();
                        hideAllModal();
                    } else {
                        DangerNotif(response.Message);
                    }
                });
            }


        });

    }
};

$(function () {
    initlanguage(lang_hr);
    var formCreate = "NewEmployeeFormNewApplicant";
    var optionsForm = GetOptionsForm(function () {
        return $("#" + formCreate).valid();
    }, function (response, statusText, xhr, $form) {
        response = parseJson(response);
        if (response.success) {
            SuccessNotif(t_createsuccess);
            window.location.href = WEB + "/hris/employeebasicinfo";
        } else {

            DangerNotif(response.Message);
        }
    });
    InitForm(formCreate, optionsForm);
    setFormAction(formCreate, ajaxUrlCreate);

    $('#candicate_recruitment_name').setcombobox({
        data: { 'rows': 1000, Search_applicant_progress_id: 4, Search_applicant_move_stage_id: 7 },
        url: ajaxUrlList,
        searchparam: 'Search_title_name',
        labelField: 'first_name',
        valueField: 'job_vacancy_id',
        searchField: 'first_name',
        render: function (data) {
            var datanya = '';
            if (data.odata) {
                var dataraw = '';
                //console.log(data.odata);
                dataraw = data.odata.code + " - " + data.odata.first_name + " " + data.odata.last_name;
                return dataraw;
            }
        }
    });

    $('#checklist_category').setcombobox({
        data: { 'rows': 100 },
        url: ajaxUrllistCategory,
        searchparam: 'Search_checklist_category',
        labelField: 'checklist_category',
        valueField: 'id',
        searchField: 'checklist_category'
    });
    $('#upload_sign_contract').setuploadbox({});

    $('#candicate_recruitment_name').on('select2:select', function (e) {
        //console.log(e.params.data.odata);
        var id = e.params.data.odata.id;
        $('#candicate_recruitment_id').val(id);
        //var code_data = e.params.data.odata.code;
        //var applicant_progress_id = e.params.data.odata.applicant_progress_id;
        //var applicant_move_stage_id = e.params.data.odata.applicant_move_stage_id;

        if (id != null) {
            ajaxGet(ajaxUrlGetCandidateReq + "/" + id, {}, function (response2) {
                response2 = parseJson(response2);
                $('#first_name').val(response2.data.first_name);
                $('#last_name').val(response2.data.last_name);
                $('#nick_name').val(response2.data.nick_name);
                $('#code').val(response2.data.code);
                $('#applicant_progress_id').val(response2.data.applicant_progress_id);
                var $radiosgender = $('input:radio[name=gender]');
                var cekgender = response2.data.gender;
                if (cekgender === "male") {
                    $radiosgender.filter('[value=male]').prop('checked', true);
                } else {
                    $radiosgender.filter('[value=female]').prop('checked', true);
                }
                $('#birth_place').val(response2.data.birth_place);
                $('#address').val(response2.data.address);
                $('#email').val(response2.data.email);
                $('#date_birth').val(response2.data.date_birth);
                $('#education_level_id').val(response2.data.education_level_id);
                $('#year_experience').val(response2.data.year_experience);
                $('#phone_number').val(response2.data.phone_number);
                $('#source').val(response2.data.source);
                var $radioscigarete = $('input:radio[name=cigaret_willing]');
                var cekcigarete = response2.data.cigaret_willing;
                if (cekcigarete === true) {
                    $radioscigarete.filter('[value=true]').prop('checked', true);
                } else {
                    $radiosgender.filter('[value=false]').prop('checked', true);
                }
                var $radiosalcohol = $('input:radio[name=alcohol_willing]');
                var cekalcohol = response2.data.alcohol_willing;
                if (cekalcohol === true) {
                    $radiosalcohol.filter('[value=true]').prop('checked', true);
                } else {
                    $radiosalcohol.filter('[value=false]').prop('checked', true);
                }
                $('#cv').setuploadbox({});
                $('#photo').setuploadbox({});
                $('#portfolio').setuploadbox({});

                $('#location_id').setcombobox({
                    data: { 'rows': 100 },
                    url: ajaxUrllocationGet,
                    searchparam: 'Search_location_name',
                    labelField: 'location_name',
                    valueField: 'id',
                    searchField: 'location_name'
                });

                $('#legal_entity_id').setcombobox({
                    data: { 'rows': 100 },
                    url: ajaxUrlLegalEntity,
                    searchparam: 'Search_legal_entity_name',
                    labelField: 'legal_entity_name',
                    valueField: 'id',
                    searchField: 'legal_entity_name'
                });


               

                $('#business_unit_job_level_id').setcombobox({
                    data: { 'rows': 100 },
                    url: ajaxUrlBusinessUnitJobLevel,
                    searchparam: 'Search_job_title_name',
                    labelField: 'job_title_name',
                    valueField: 'id',
                    searchField: 'job_title_name',
                    render: function (data) {
                        var datanya = '';
                        if (data.odata) {
                            var Division = '';
                            var Departement = '';
                            var Subgroup = '';
                            var Group = '';

                            if (data.odata.businessUnitDivision.name != "") {
                                Division = data.odata.businessUnitDivision.name;
                            } else {
                                Division = '';
                            }

                            if (data.odata.businessUnitDepartement.name != "") {
                                Departement = data.odata.businessUnitDepartement.name;
                            } else {
                                Departement = '';
                            }

                            if (data.odata.businessUnitSubgroup.name != "") {
                                Subgroup = data.odata.businessUnitSubgroup.name;
                            } else {
                                Subgroup = '';
                            }

                            if (data.odata.businessUnitGroup.name != "") {
                                Group = data.odata.businessUnitGroup.name;
                            } else {
                                Group = '';
                            }


                            if (data.odata.businessUnitGroup.name != null && data.odata.businessUnitSubgroup.name != null && data.odata.businessUnitDivision.name != null && data.odata.businessUnitDepartement.name != null) {

                                datanya = Group + " => " + Subgroup + " => " + Division + " => " + Departement + " -- (" + data.odata.job_title_name + ")";

                            } else if (data.odata.businessUnitGroup.name != null && data.odata.businessUnitSubgroup.name != null && data.odata.businessUnitDivision.name != null) {

                                datanya = Group + " => " + Subgroup + " => " + Division + " -- (" + data.odata.job_title_name + ")";

                            } else if (data.odata.businessUnitGroup.name != null && data.odata.businessUnitSubgroup.name != null) {
                                datanya = Group + " => " + Subgroup + " --  (" + data.odata.job_title_name + ")";
                            } else {
                                datanya = Group + " --  (" + data.odata.job_title_name + ")";
                            }
                            return datanya;
                        }
                    } 
                });
               
            });
            

        } else {
            DangerNotif("No Data Candidate");
            return false;
        }
    });

    $('#business_unit_job_level_id').on('select2:select', function (e) {
        console.log(e.params.data.odata);
        var id2 = e.params.data.odata.parent_id;
        if (id2 != null) {
            ajaxGet(ajaxUrlGetBusinessUnitJobLevel + "/" + id2, {}, function (response3) {
                response3 = parseJson(response3);

                            var Division = '';
                            var Departement = '';
                            var Subgroup = '';
                            var Group = '';

                            if (response3.data.businessUnitDivision.name != "") {
                                Division = response3.data.businessUnitDivision.name;
                            } else {
                                Division = '';
                            }

                            if (response3.data.businessUnitDepartement.name != "") {
                                Departement = response3.data.businessUnitDepartement.name;
                            } else {
                                Departement = '';
                            }

                            if (response3.data.businessUnitSubgroup.name != "") {
                                Subgroup = response3.data.businessUnitSubgroup.name;
                            } else {
                                Subgroup = '';
                            }

                            if (response3.data.businessUnitGroup.name != "") {
                                Group = response3.data.businessUnitGroup.name;
                            } else {
                                Group = '';
                            }

                            if (response3.data.businessUnitGroup.name != null && response3.data.businessUnitSubgroup.name != null && response3.data.businessUnitDivision.name != null && response3.data.businessUnitDepartement.name != null) {

                                datanya = Group + " => " + Subgroup + " => " + Division + " => " + Departement + " -- (" + response3.data.job_title_name + ")";

                            } else if (response3.data.businessUnitGroup.name != null && response3.data.businessUnitSubgroup.name != null && response3.data.businessUnitDivision.name != null) {

                                datanya = Group + " => " + Subgroup + " => " + Division + " -- (" + response3.data.job_title_name + ")";

                            } else if (response3.data.businessUnitGroup.name != null && response3.data.businessUnitSubgroup.name != null) {
                                datanya = Group + " => " + Subgroup + " --  (" + response3.data.job_title_name + ")";
                            } else {
                                datanya = Group + " --  (" + response3.data.job_title_name + ")";
                            }
                           
                       
                $('#parent_id').val(id2);
                $('#parent_name').val(datanya);

            });
            $('#business_unit_division_id').val(e.params.data.odata.businessUnitDivision.id);
            $('#business_unit_division_name').val(e.params.data.odata.businessUnitDivision.name);
            $('#business_unit_departement_id').val(e.params.data.odata.businessUnitDepartement.id);
            $('#business_unit_departement_name').val(e.params.data.odata.businessUnitDepartement.name);
        }
       
    });
});

function DeleteLogic(id) {
    getData();
    if (NewEmployeeChecklist.length === 1) {
        AddRowNoData();
    } else {
        Deleteaja(id);
        getData();
        var ttl = 0;
        $('#tmpNewEmployeeChecklist').empty();
        var tmpmpno = 0;
        $.each(NewEmployeeChecklist, function (index, value) {
            $('#tmpNewEmployeeChecklist').append('<input type="hidden" name="listDataNewEmployeeChecklist[' + tmpmpno + '][checklist_category]" value="' + value.checklist_category + '"  >');
            $('#tmpNewEmployeeChecklist').append('<input type="hidden" name="listDataNewEmployeeChecklist[' + tmpmpno + '][checklistitem]" value="' + value.checklistitem + '"  >');
            $('#tmpNewEmployeeChecklist').append('<input type="hidden" name="listDataNewEmployeeChecklist[' + tmpmpno + '][description]" value="' + value.description + '"  >');
            $('#tmpNewEmployeeChecklist').append('<input type="hidden" name="listDataNewEmployeeChecklist[' + tmpmpno + '][whentoalert]" value="' + value.whentoalert + '"  >');
              tmpmpno++;
        });

    }
    $('table#dt_employee_checklist tr#' + id + '').remove();
}

function Deleteaja(id) {
    $('table#dt_employee_checklist tr#' + id + '').remove();
}

function AddRowNoData() {
    var markup = "<tr id='no_data'><td colspan='5'>No Data</td></td></tr>";
    $("table#dt_employee_checklist tbody").append(markup);

}

function randomIntFromInterval(min, max) // min and max included
{
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function AddRowTable(checklistcategoryid,checklistcategoryname,checklistitem, description, whentoalert) {
    var acak = randomIntFromInterval(1, 9999);
    var markup = "<tr id=" + acak + "><td style='display: none;'>" + checklistcategoryid + "</td><td>" + checklistcategoryname + "</td><td>" + checklistitem + "</td><td>" + description + "</td><td>" + whentoalert + "</td><td><center><a href='#' class='btn btn-danger' onClick='DeleteLogic(" + acak + ")'>Delete</a></center></td></tr>";
    $("table#dt_employee_checklist tbody").append(markup);
}
function clearModal() {

    var formCreateDetail = "NewEmployeeChecklistModalFormDetail";
    var optionsForm = GetOptionsForm(function () {
        return $("#" + formCreateDetail).valid();
    });
    InitForm(formCreateDetail, optionsForm);
}
function CheckAndAddRow(checklistcategoryid,checklistcategoryname,checklistitem, description, whentoalert) {
   
    if ($('#checklist_item').val() === "") {
        alert('Please Enter Checklist item');
        return false;
    } else if ($('#desc').val() === "") {
        alert('Please Enter Description');
        return false;
    } else if ($('#when_to_alert').val() === "") {
        alert('Please Enter When to Alert First');
        return false;
    } else { 
            Deleteaja('no_data');
            getData();
            // if (RealData.length > 0) {
        tmpData = {
                "checklist_category": checklistcategoryid,
                "checklistitem": checklistitem,
                "description": description,
                "whentoalert": whentoalert
            };
            NewEmployeeChecklist.push(tmpData);
            $('#tmpNewEmployeeChecklist').empty();
            var tmpmpno = 0;
            $.each(NewEmployeeChecklist, function (index, value) {
                $('#tmpNewEmployeeChecklist').append('<input type="hidden" name="listDataNewEmployeeChecklist[' + tmpmpno + '][checklist_category]" value="' + value.checklist_category + '"  >');
                $('#tmpNewEmployeeChecklist').append('<input type="hidden" name="listDataNewEmployeeChecklist[' + tmpmpno + '][checklistitem]" value="' + value.checklistitem + '"  >');
                $('#tmpNewEmployeeChecklist').append('<input type="hidden" name="listDataNewEmployeeChecklist[' + tmpmpno + '][description]" value="' + value.description + '"  >');
                $('#tmpNewEmployeeChecklist').append('<input type="hidden" name="listDataNewEmployeeChecklist[' + tmpmpno + '][whentoalert]" value="' + value.whentoalert + '"  >');
                tmpmpno++;
            });

            AddRowTable(checklistcategoryid,checklistcategoryname,checklistitem, description, whentoalert);
            $('#NewEmployeeChecklistModalDetail').modal('hide');
      
    }
}

function getData() {
    var TableData = '';
    var tmpData = '';
    NewEmployeeChecklist = [];
    var rowCount = $('#dt_employee_checklist >tbody >tr').length;
    // alert(rowCount);
    $('#dt_employee_checklist tr').each(function (row, tr) {
        //console.log(row);
        if ($(tr).find('td:eq(0)').text() !== "") {
            tmpData = {
                "checklist_category": $(tr).find('td:eq(0)').text(),
                "checklistitem": $(tr).find('td:eq(2)').text(),
                "description": $(tr).find('td:eq(3)').text(),
                "whentoalert": $(tr).find('td:eq(4)').text()
            };
            NewEmployeeChecklist.push(tmpData);
        }
        //TableData = TableData
        //    + $(tr).find('td:eq(0)').text() + ' '  // jobtitle
        //    + $(tr).find('td:eq(1)').text() + ' '  // ttf
        //    + $(tr).find('td:eq(2)').text() + ' '  // status
        //    + $(tr).find('td:eq(3)').text() + ' '  // mppbudget
        //    + $(tr).find('td:eq(4)').text() + ' '  // grade
        //    + $(tr).find('td:eq(5)').text() + ' '  // project
        //    + $(tr).find('td:eq(7)').text() + ' '  // desc
        //    + $(tr).find('td:eq(8)').text() + ' '  // jobtitle_name
        //    + $(tr).find('td:eq(6)').text() + ' '  // Task
        //     + '\n';

    });
    //console.log(NewEmployeeChecklist);
    $('#dataDetailMpp').val(JSON.stringify(NewEmployeeChecklist));
}

 

