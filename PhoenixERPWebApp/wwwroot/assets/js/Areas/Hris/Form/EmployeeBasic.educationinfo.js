var pageheader = "";

$(function () {
 
    $('#education_level_id').setcombobox({
        data: { 'rows': 100 },
        url: educationLevelURL,
        maxItems: 0,
        searchparam: 'Search_education_name',
        labelField: 'education_name',
        valueField: 'id'
    });

    $('#education_institusion_id').setcombobox({
        data: { 'rows': 100, 'Search_institution_type': 1 },
        url: InstitusionURL,
        maxItems: 0,
        searchparam: 'Search_institution_name',
        labelField: 'institution_name',
        valueField: 'id'
    });


    
});
