var ajaxUrl = API + "/ProjectManagement/ClientBrief";
var ajaxUrlList = ajaxUrl + "/List";
var ajaxUrlGet = ajaxUrl + "/Get";
var ajaxUrlUpdate = ajaxUrl + "/Update";
var ajaxUrlCreate = ajaxUrl + "/Create";
var ajaxUrlDelete = ajaxUrl + "/Delete";

var tableList = "ClientBriefList";
var formCreate = "ClientBriefFormCreate";
var formUpdate = "ClientBriefFormUpdate";
var formSearch = "ClientBriefFormSearch";
var modalCreate = "ClientBriefModalCreate";
var modalUpdate = "ClientBriefModalUpdate";
var modalSearch = "ClientBriefModalSearch";
var pageheader = "";
var paramsSearchIndex = function () {
    //var searchdata = $("#" + formSearch).serializeArray();

    var search_id = $("#search_id").val();
    var search_val = '';
    if (search_id === 'Search_campaign_name') search_val = $("#search_val").val();
    else search_val = $('#ddl_isdeleted').val()  
    var data = {};
    if (search_val) {
        data[search_id] = search_val;
    }
    /*$(searchdata).each(function (index, obj) {
        if (obj.name !== "__RequestVerificationToken") {
            data[obj.name] = obj.value;
        }
    });*/

    return data;
}
var columnsIndex = [
	{
		"data": "Id",
		"title": "",
		"sClass": "actioncol3x",
		orderable: false,
        render: function (data, type, row, meta) {


            retval = '<div class="btn-group">';
            retval += '<a class="m-btn btn btn-sm btn-primary fakelink" href="/projectmanagement/clientbrief/update/' + row.id + '"><i class="la la-edit"></i></a>';
            retval += '<a class="m-btn btn btn-sm btn-danger" onClick="fnClientBrief.delete(\'' + row.id + '\')" href="javascript:void(0)"><i class="la la-trash"></i></a>';
            retval += '<a class="m-btn btn btn-sm btn-success fakelink" href="/projectmanagement/clientbrief/job/' + row.id + '' + row.id + '"><i class="la la-tasks"></i></a>';
            retval += '</div>';
            return retval;

		}
		
	},
	{
		"data": "brand_name",
		"title": "Brand",
		"sClass": "lcol",
		orderable: true,
	},
	
	{
		"data": "campaign_name",
		"title": "Campaign Name",
		"sClass": "rcol",
        orderable: true
	},
	{
		"data": "job_name",
		"title": "Job Name",
		"sClass": "rcol",
		orderable: false,
		"render": function (data, type, row) {
            rdata = '';
            if (data) {
                //rdata += '<ul>';
                xmn = 0;
                $.each(data, function (key, value) {
                    //rdata += '<li>' + value.app_username + '</li>';
                    if (xmn > 0) rdata += ', ';
                    rdata += value;
                    xmn++;
                });
                //rdata += '</<ul>';
            }
            return rdata;
		}
	},
    {
        "data": "job_number",
        "title": "Job Number",
        "sClass": "rcol",
        orderable: false,
        "render": function (data, type, row) {
            rdata = '';
            if (data) {
                //rdata += '<ul>';
                xmn = 0;
                $.each(data, function (key, value) {
                    //rdata += '<li>' + value.app_username + '</li>';
                    if (xmn > 0) rdata += ', ';
                    rdata += '<a href="' + WEB + '/projectmanagement/job/detail/' + value+'" class="fakelink">'+value+'</a>';
                    xmn++;
                });
                //rdata += '</<ul>';
            }
            return rdata;
        }
    },
	{
		"data": "business_unit_name",
		"title": "Business Unit",
		"sClass": "rcol",
		orderable: false
	},
	{
		"data": "deadline",
		"title": "Date Start",
		"sClass": "rcol",
		orderable: true,
		"render": function (data) {
			var date = new Date(data);
			var month = date.getMonth() + 1;
			return date.getDate() + "/" + month + "/" + date.getFullYear();
		}
	},
	{
		"data": "final_delivery",
		"title": "Final Delivery",
		"sClass": "rcol",
		orderable: true,
		"render": function (data) {
			var date = new Date(data);
			var month = date.getMonth() + 1;
			return date.getDate() + "/" + month + "/" + date.getFullYear();
		}
	}

];


fnClientBrief = {
    generatelist: function () {
        InitTable(tableList, ajaxUrlList, paramsSearchIndex, columnsIndex, 'id', 'desc');
        setcolvis(tableList, columnsIndex)
    },
    reloadlist: function () {
        TABLES[tableList].ajax.reload(null, false);
    },
    search: function () {
        UIkit.modal("#" + modalSearch).show();
    },
    initsearch: function (x) {
        if (x === "reset") {
            $("#" + formSearch)[0].reset();
        }
        console.log(x);
        TABLES[tableList].ajax.reload();
        UIkit.modal("#" + modalSearch).hide();
    },

    create: function () {
        var optionsForm = GetOptionsForm(function () {
            return $("#" + formCreate).parsley().isValid();
        }, function (response, statusText, xhr, $form) {
            response = parseJson(response);
            if (response.success) {
                SuccessNotif(t_createsuccess);
                fnClientBrief.reloadlist();
                UIkit.modal("#" + modalCreate).hide();
            } else {
                DangerNotif(response.Message);
            }
        });
        InitForm(formCreate, optionsForm);
        setFormAction(formCreate, ajaxUrlCreate);
        UIkit.modal("#" + modalCreate).show();
    },
    update: function (id) {
        ajaxGet(ajaxUrlGet + "/" + id, {}, function (response) {
            response = parseJson(response);

            UIkit.modal("#" + modalUpdate).show();
            setFormAction(formUpdate, ajaxUrlUpdate);
            var optionsForm = GetOptionsForm(function () {
                return $("#" + formUpdate).parsley().isValid();
            }, function (response, statusText, xhr, $form) {
                response = parseJson(response);
                if (response.success) {
                    SuccessNotif(t_updatesuccess);
                    fnClientBrief.reloadlist();
                    UIkit.modal("#" + modalUpdate).hide();
                } else {
                    DangerNotif(response.Message);
                }
            });
            InitForm(formUpdate, optionsForm);
            FormLoadByDataUsingName(response.data, formUpdate);
        });
    },
    delete: function (id) {
        var text = t_delete;
        confirmDialog(text, function () {
            ajaxDelete(ajaxUrlDelete + "/" + id, {}, function (response) {
                response = parseJson(response);
                if (response.success) {
                    SuccessNotif(t_deletesuccess);
                    fnClientBrief.reloadlist();
                    hideAllModal();
                } else {
                    DangerNotif(response.Message);
                }
            });
        });
    },

};

$(function () {
    fnClientBrief.generatelist();
    initlanguage(lang_pm);
    $('#ddl_isdeleted').hide();
    $('#search_val').show();
});


function funcCondition(obj) {
    if ($(obj).val() === 'Search_is_deleted') {
        $('#ddl_isdeleted').show();
        $('#search_val').hide();
    }
    else {
        $('#ddl_isdeleted').hide();
        $('#search_val').show();
    }
}

