var ajaxUrl = API + "General/File/Upload";
var ajaxUrlOneDrive = API + "General/File/UploadOneDrive";

var ajaxUrlGetFile = API + "General/File/Getfile"; 
var uploadform = "damuploadform";
var onedriveform = "damonedriveform";
 

$(function () {
    var optionsForm = GetOptionsForm(function () {
        var xret = $("#" + onedriveform).valid();
        if (xret) startprocess();
        return xret;
    }, function (response, statusText, xhr, $form) {
        endprocess();
            response = parseJson(response);
            if (response.success) {
                SuccessNotif(t_createsuccess);
                swal({
                    title: "Confirmation",
                    text: t_useasfile,
                    type: "success",
                    showCancelButton: !0,
                    confirmButtonText: "Yes",
                    cancelButtonText: "No",
                    reverseButtons: !0
                }).then(function (e) {
                    if (e.value) {

                        $("#lblx" + currentuploadbox).html('<a href="' + ajaxUrlGetFile + '/' + response.data.id+'" target="_blank">' + response.data.id + '</a>');
                        $('#' + currentuploadbox).val(response.data.id);
                        $('#File_Upload').modal('hide');
                    }


                });

            } else {
                DangerNotif(response.Message);
            }
        });
    
    //InitForm(uploadform, optionsForm);
    //setFormAction(uploadform, ajaxUrl);
    
    InitForm(onedriveform, optionsForm);
    setFormAction(onedriveform, ajaxUrlOneDrive);

    initlanguage(lang_gn);
});

$(document).ready(function () {
    $('input[type="file"]').change(function (e) {
        $('#lblcustomFile').text(e.target.files[0].name);
    });
});
