using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PhoenixERPWebApp.Models
{
    public class CostSharingAllocation : FinanceEntityBase
    {
        [DisplayName("Id")]
        public string Id { get; set; }

        [DisplayName("Cost Allocation Name")]
        public string CostSharingAllocationName { get; set; }

        [DisplayName("Total Cost Allocation Ratio")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? TotalCostAllocationSharing { get; set; }

        [DisplayName("VAT 100% Full change to")]
        public string BusinessUnitId { get; set; }

        [DisplayName("COA Name Level 5")]
        public string CoaId { get; set; }

        //Not Mapped
        [DisplayName("COA ID")]
        public string CodeRec { get; set; }

        [DisplayName("COA Level 5")]
        public string CoaName { get; set; }
        public bool? IsMonthly { get; set; }

        public List<CostSharingAllocationDetail> CostSharingAllocationDetails { get; set; }
    }
}