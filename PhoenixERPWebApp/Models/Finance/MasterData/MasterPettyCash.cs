﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PhoenixERPWebApp.Models.Finance.MasterData
{
    public class MasterPettyCash : FinanceEntityBase
    {
        [DisplayName("Id")]
        public string Id { get; set; }

        [Required]
        [DisplayName("Bank ID")]
        public string BankId { get; set; }

        [DisplayName("Bank Account Name")]
        public string BankAccountName { get; set; }

        [DisplayName("Bank Account Number")]
        public string BankAccountNumber { get; set; }

        [DisplayName("Currency Id")]
        public string CurrencyId { get; set; }

        [DisplayName("Chart Account ID")]
        public string ChartAccountId { get; set; }

        [DisplayName("Chart Account Name")]
        public string ChartAccountName { get; set; }

        [DisplayName("Chart Account Number")]
        public string ChartAccountNumber { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Status")]
        public string Status { get; set; }

        [DisplayName("Action Id")]
        public string ActionId { get; set; }

        [DisplayName("Legal Entity Id")]
        public string LegalEntityId { get; set; }

        [DisplayName("Begining Balance")]
        public decimal? BeginingBalance { get; set; }

        public bool IsSettle { get; set; }

        //Not Mapped
        [DisplayName("Currency")]
        public string CurrencyCode { get; set; }

        [DisplayName("Bank Code")]
        public string BankCode { get; set; }
        
        [DisplayName("COA Level 5 ID")]
        public string CoaCodeRecName { get { return ChartAccountNumber + " - " + ChartAccountName; } }

        [DisplayName("Legal Entity")]
        public string LegalEntity { get; set; }

        [NotMapped]
        public string BussinessUnitId { get; set; }
        [NotMapped]
        public string BussinessUnit { get; set; }
        [NotMapped]
        public string AffiliationId { get; set; }
        [NotMapped]
        public string Affiliation { get; set; }
    }
}