using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PhoenixERPWebApp.Models
{
    public class FinancePeriod : FinanceEntityBase
    {
        [DisplayName("Id")]
        public string Id { get; set; }

        [DisplayName("Fiscal Year Id")]
        public string FiscalYearId { get; set; }

        [DisplayName("Period Name")]
        public string FinancePeriodName { get; set; }

        [DisplayName("Code")]
        public string FinancePeriodNumber { get; set; }

        [DisplayName("Legal Entity Id")]
        public string LegalEntityId { get; set; }

        [DisplayName("Start Period")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? StartDate { get; set; }

        [DisplayName("End Period")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EndDate { get; set; }

        [DisplayName("Status")]
        public string Status { get; set; }

        [DisplayName("Is Close")]
        public bool? IsClose { get; set; }

        [DisplayName("Ap")]
        public bool? Ap { get; set; }

        [DisplayName("Ar")]
        public bool? Ar { get; set; }

        [DisplayName("Cas")]
        public bool? Cas { get; set; }

        [DisplayName("Pcs")]
        public bool? Pcs { get; set; }

        [DisplayName("Bp")]
        public bool? Bp { get; set; }

        [DisplayName("Br")]
        public bool? Br { get; set; }

        [DisplayName("Je")]
        public bool? Je { get; set; }

        [DisplayName("Ja")]
        public bool? Ja { get; set; }

        [DisplayName("Amor")]
        public bool? Amor { get; set; }

        [DisplayName("Dep")]
        public bool? Dep { get; set; }

        [DisplayName("Sa")]
        public bool? Sa { get; set; }


        //Not Mapped
        [DisplayName("Legal Entity")]
        public string LegalEntityName { get; set; }

        [DisplayName("Remarks")]
        public string RemarksProcess { get; set; }

        [DisplayName("Status")]
        public int? StatusApproval { get; set; }
    }
}