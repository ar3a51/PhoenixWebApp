﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PhoenixERPWebApp.Models
{
    public class BudgetMaster : FinanceEntityBase
    {
        [DisplayName("Id")]
        public string Id { get; set; }
        public string OrganizationId { get; set; }
        public string BudgetId { get; set; }
        public string FinancialYearId { get; set; }
        public string LegalEntityId { get; set; }
        public string DivisionId { get; set; }
        public string BusinessUnitId { get; set; }
        public string AccountId { get; set; }
        public string CurrencyId { get; set; }
        public decimal? ExchangeRate { get; set; }
        public string BudgetAllocationRatioId { get; set; }
        public int? BudgetVersion { get; set; }
        public string BudgetOwnerId { get; set; }
        public string BudgetStatus { get; set; }
        public decimal? TotalBudget { get; set; }
        public string BudgetYear { get; set; }
        public DateTime? StartPeriode { get; set; }
        public DateTime? EndPeriode { get; set; }
        public decimal? Budget01 { get; set; }
        public decimal? Budget02 { get; set; }
        public decimal? Budget03 { get; set; }
        public decimal? Budget04 { get; set; }
        public decimal? Budget05 { get; set; }
        public decimal? Budget06 { get; set; }
        public decimal? Budget07 { get; set; }
        public decimal? Budget08 { get; set; }
        public decimal? Budget09 { get; set; }
        public decimal? Budget10 { get; set; }
        public decimal? Budget11 { get; set; }
        public decimal? Budget12 { get; set; }

        //Not Mapping
        public string BudgetName { get; set; }
    }
}
