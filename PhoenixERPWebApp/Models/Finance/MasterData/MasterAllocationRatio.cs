using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PhoenixERPWebApp.Models
{
    public class MasterAllocationRatio : FinanceEntityBase
    {
        [DisplayName("Id")]
        public string Id { get; set; }

        [DisplayName("Cost Allocation Name")]
        public string CostAllocationName { get; set; }

        [DisplayName("Vat Full Charged")]
        public string VatFullCharged { get; set; }

        [DisplayName("Year")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Year { get; set; }

        [DisplayName("Chart Account Name")]
        public string ChartAccountName { get; set; }

        [DisplayName("Chart Account Number")]
        public string ChartAccountNumber { get; set; }

        [DisplayName("Business Unit 1")]
        public string BusinessUnit1 { get; set; }

        [DisplayName("Business Unit 2")]
        public string BusinessUnit2 { get; set; }

        [DisplayName("Business Unit 3")]
        public string BusinessUnit3 { get; set; }

        [DisplayName("Business Unit 4")]
        public string BusinessUnit4 { get; set; }

        [DisplayName("Business Unit 5")]
        public string BusinessUnit5 { get; set; }

        [DisplayName("Business Unit 6")]
        public string BusinessUnit6 { get; set; }

        [DisplayName("Business Unit 7")]
        public string BusinessUnit7 { get; set; }

        [DisplayName("Percentage 1")]
        public string Percentage1 { get; set; }

        [DisplayName("Percentage 2")]
        public string Percentage2 { get; set; }

        [DisplayName("Percentage 3")]
        public string Percentage3 { get; set; }

        [DisplayName("Percentage 4")]
        public string Percentage4 { get; set; }

        [DisplayName("Percentage 5")]
        public string Percentage5 { get; set; }

        [DisplayName("Percentage 6")]
        public string Percentage6 { get; set; }

        [DisplayName("Percentage 7")]
        public string Percentage7 { get; set; }

        [DisplayName("Status")]
        public string Status { get; set; }

        [DisplayName("Legal Entity Id")]
        public string LegalEntityId { get; set; }
    }
}