﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models
{
    public class TrialBalance
    {
        public string code_rec { get; set; }
        public string name2 { get; set; }
        //public string name3 { get; set; }
        public string name5  { get; set; }
        public decimal previous_year_debit { get; set; }
        public decimal previous_year_credit { get; set; }
        public decimal mutasi_debit { get; set; }
        public decimal mutasi_credit { get; set; }
        public decimal tbl_debit { get; set; }
        public decimal tbl_credit { get; set; }
        public decimal blc_debit { get; set; }
        public decimal blc_credit { get; set; }
        public decimal pnl_debit { get; set; }
        public decimal pnl_credit { get; set; }
    }
}
