﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models
{
    public class TrialBalanceFilter
    {
        public string LegalEntityId { get; set; }
        public string BusinessUnitId { get; set; }
        public string FiscalYearsId { get; set; }
        public string StartPeriodId { get; set; }
        public string EndPeriodId { get; set; }
    }
}
