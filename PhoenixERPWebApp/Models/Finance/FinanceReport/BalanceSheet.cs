﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models
{
    public class BalanceSheet
    {
        //public string name2 { get; set; }

        [DisplayName("Account")]
        public string name5 { get; set; }

        [DisplayName("Iris")]
        public decimal iris { get; set; }

        [DisplayName("Advice")]
        public decimal advis { get; set; }

        [DisplayName("Interface")]
        public decimal Interface { get; set; }

        [DisplayName("Total")]
        public decimal Total { get; set; }
    }
}
