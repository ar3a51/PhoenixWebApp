﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance.FinanceReport
{
    public class SearchReportTimeSheet
    {
        [DisplayName("Job Number")]
        public string JobId { get; set; }

        public string ClientId { get;set; }
    }
}
