﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoenixERPWebApp.Models.Finance
{
    public class BastDTO
    {
        public Bast Header;
        public List<BastDetail> Details;
    }
}
