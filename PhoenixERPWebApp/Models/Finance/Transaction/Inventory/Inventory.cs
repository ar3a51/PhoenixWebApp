﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PhoenixERPWebApp.Models.Finance.Transaction.Inventory
{
    public class Inventory : FinanceEntityBase
    {
        public string Id { get; set; }

        public string OrganizationId { get; set; }

        public string InventoryInNumber { get; set; }

        public DateTime? InventoryInDateTime { get; set; }

        public string JobId { get; set; }

        public string InventoryGoodsReceiptId { get; set; }

        public string WarehouseId { get; set; }

        public bool? IsRefurbished { get; set; }

        public string InventoryOutId { get; set; }

        public string BusinessUnitId { get; set; }

        public string LegalEntityId { get; set; }

        public string AffiliationId { get; set; }

        public string ApprovalId { get; set; }

        public string Remark { get; set; }

        public string SKUCode { get; set; }

        public string InventoryName { get; set; }

        public string Brand { get; set; }

        public string LegalEntity { get; set; }

        public string BusinessUnit { get; set; }

        public decimal? TotalQty { get; set; }
        public decimal? QtyOnHand { get; set; }
        public decimal? QtyOutReturn { get; set; }
        public decimal? QtyBroken { get; set; }
        public List<InventoryDetail> InventoryDetails { get; set; }

        public InventoryDetail InventoryDetail { get; set; }
    }
}
