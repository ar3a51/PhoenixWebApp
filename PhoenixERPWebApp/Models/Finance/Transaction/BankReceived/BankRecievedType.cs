﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance.Transaction.BankReceived
{
    public class BankReceivedType : FinanceEntityBase
    {
        public string Id { get; set; }
   
        public string organization_id { get; set; }

        public string ReceivedType { get; set; }

    }
}
