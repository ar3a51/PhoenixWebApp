﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance.Transaction.BankReceived
{
    public class BankReceivedFilter
    {
        public string FilterInvoice { get; set; }

        public string FilterReferenceNumber { get; set; }

        public string FilterLegalEntity { get; set; }

        public string FilterStartDate { get; set; }

        public string FilterEndDate { get; set; }

        public string FilterBusinessUnitId { get; set; }

        public string FilterStatus { get; set; }
        public string FilterPaymentRequestType { get; set; }

    }
}
