﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance.Transaction.BankReceived
{
    public class BankReceivedDTO
    {
        public BankReceivedList Header { get; set; }
        public List<BankReceivedDetail> Details { get; set; }
    }
}
