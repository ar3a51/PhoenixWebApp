﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance.Transaction.FnCashAdvance
{
    public class FnCashAdvanceDetail
    {
        public string Id { get; set; }
        public string CANumber { get; set; }
        public DateTime CADate { get; set; }
        public string Revision { get; set; }
        public DateTime Date { get; set; }
        public string RequestorName { get; set; }
        public string BusinessUnit { get; set; }
        public string LegalEntity { get; set; }
        public string ClientName { get; set; }
        public string POVendor { get; set; }
        public string JobNumber { get; set; }
        public string ProjectActivity { get; set; }
        public string ProjectArea { get; set; }
        public string VendorName { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public string BankName { get; set; }
        public string BankBranch { get; set; }
        public DateTime PeriodFrom { get; set; }
        public DateTime PeriodTo { get; set; }
    }
}
