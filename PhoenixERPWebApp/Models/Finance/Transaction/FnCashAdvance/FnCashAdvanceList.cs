﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance.Transaction.FnCashAdvance
{
    public class FnCashAdvanceList
    {
        public string Id { get; set; }
        public string CANumber { get; set; }
        public string JobNumber { get; set; }
        public string TaskDetail { get; set; }
        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Amount { get; set; }
        public DateTime Date { get; set; }
        public DateTime DueDate { get; set; }
        public string Status { get; set; }
    }
}
