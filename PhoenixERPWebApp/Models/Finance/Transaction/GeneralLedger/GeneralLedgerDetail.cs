﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models
{
    public class GeneralLedgerDetail
    {
        [DisplayName("Id")]
        public Guid? Id { get; set; }

        [DisplayName("Report")]
        public string Report { get; set; }

        [DisplayName("RPOTYPE")]
        public string Rpotype { get; set; }

        [DisplayName("DESC1")]
        public string Desc1 { get; set; }

        [DisplayName("FDESC1")]
        public string Fdesc1 { get; set; }

        [DisplayName("ACC NAME")]
        public string Accname { get; set; }

        [DisplayName("ACC DESC")]
        public string Accdesc { get; set; }

        [DisplayName("CLIENT")]
        public int? ClientId { get; set; }

        [DisplayName("REMARK")]
        public string Description { get; set; }

        [DisplayName("Periode From")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? PeriodeFrom { get; set; }

        [DisplayName("Periode To")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? PeriodeTo { get; set; }

        [DisplayName("YOUR REFF")]
        public int? YourReff { get; set; }

        [DisplayName("Reference No")]
        public string EvdNo { get; set; }

        [DisplayName("Reference Date")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Referencedate { get; set; }

        [DisplayName("JOB NO")]
        public string JobId { get; set; }

        [DisplayName("Mainservice Category")]
        public string MainserviceCategory { get; set; }

        [DisplayName("PCE ID")]
        public string PceId { get; set; }

        [DisplayName("PCA ID")]
        public string PcaId { get; set; }

        [DisplayName("DESC")]
        public string TrxDescription { get; set; }

        [DisplayName("Division")]
        public string Division { get; set; }

        [DisplayName("Currency")]
        public string CurrCode { get; set; }

        [DisplayName("Exchange")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal? ExchangeRate { get; set; }

        [DisplayName("Debit")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal? Debit { get; set; }

        [DisplayName("Credit")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal? Credit { get; set; }

        [DisplayName("Adjustment")]
        public decimal? Adjustment { get; set; }

        [DisplayName("Currency")]
        public string CurrencyId { get; set; }

        [DisplayName("Legal Entity")]
        public string LegalEntityId { get; set; }

        [DisplayName("PERIOD")]
        public string period { get { return Convert.ToDateTime(PeriodeFrom).ToString("dd-MMM-yyyy") + " to " + Convert.ToDateTime(PeriodeTo).ToString("dd-MMM-yyyy"); } }
    }
    
    public class GeneralLedgerDetailSearch
    {
        [DisplayName("Legal Entity")]
        public string LegalEntityId { get; set; }

        [DisplayName("Periode From")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? PeriodeFrom { get; set; }

        [DisplayName("Periode To")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? PeriodeTo { get; set; }

        [DisplayName("Reference No")]
        public string EvdNo { get; set; }

        [DisplayName("Currency")]
        public string CurrencyId { get; set; }

        [DisplayName("Exchange")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal? ExchangeRate { get; set; }

        [DisplayName("REMARK")]
        public string Description { get; set; }

        [DisplayName("Adjustment")]
        public decimal? Adjustment { get; set; }
    }
}
