﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance
{
    public class CashAdvanceSattlement : FinanceEntityBase
    {

        [DisplayName("Id")]
        public string Id { get; set; }

        [DisplayName("CA Number")]
        public string CaNumber { get; set; }

        public DateTime? CashAdvanceSettleDate { get; set; }

        [DisplayName("Date")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CaDate { get; set; }

        [DisplayName("Revision")]
        public int? Revision { get; set; }

        [DisplayName("Employee ID")]
        public string RequestedBy { get; set; }

        [DisplayName("Requested On")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? RequestedOn { get; set; }

        [DisplayName("Business Unit Id")]
        public string BusinessUnitId { get; set; }

        [DisplayName("Legal Entity")]
        public string LegalEntityId { get; set; }

        [DisplayName("Is Project Cost")]
        public bool? IsProjectCost { get; set; }

        [DisplayName("Purchase Order Id")]
        public string PurchaseOrderId { get; set; }

        [DisplayName("PCE No.")]
        public string PurchaseOrderNumber { get; set; }

        public string ClientName { get; set; }
        public string ProjectActivity { get; set; }
        public string CaStatusId { get; set; }
        public string RequestBusinessUnitId { get; set; }
        public string ToAccount { get; set; }
        public string ToAccountNumber { get; set; }
        //tambahan

        public string JobId { get; set; }
        public string JobNumber { get; set; }
        public string MainServiceCategoryId { get; set; }
        public decimal? Vat { get; set; }
        public double? VatPercent { get; set; }
        public decimal? VatOn { get; set; }
        public decimal? TaxPayable { get; set; }
        public decimal? TaxPayablePercent { get; set; }
        public decimal? TaxPayableOn { get; set; }
        public string TaxPayCoaId { get; set; }
        public string RequestEmployeeName { get; set; }
        public string RequestEmployeeId { get; set; }
        public string RequestJobTitleId { get; set; }
        public string RequestJobTitleName { get; set; }
        public string RequestJobGradeId { get; set; }
        public string RequestJobGradeName { get; set; }
        public string BankId { get; set; }
        public string AccountNumber { get; set; }
        public string SattlementNumber { get; set; }
        public decimal? Balance { get; set; }
        public decimal? Amount { get; set; }
        public decimal? TotalAmount { get; set; }
        public decimal? CashAdvanceCa { get; set; }
        public string CashAdvanceId { get; set; }
        public string RemarkRejected { get; set; }
        public string FileReceipt { get; set; }
        public string LogDescription { get; set; }

        public string FileReceiptName { get; set; }
        public int? Status
        {
            get
            {
                int res = 0;
                if (int.TryParse(CaStatusId, out res))
                {
                    return res;
                }
                else
                {
                    return null;
                }

            }
            set
            {
                CaStatusId = value.HasValue ? value.Value.ToString() : "";
            }
        }

        public string VendorName { get; set; }
        public string PayeeAccountName { get; set; }
        public string PayeeAccountNumber { get; set; }
        public string PayeeBankId { get; set; }
        public string PayeeBankBranch { get; set; }
        public DateTime? CaPeriodFrom { get; set; }
        public DateTime? CaPeriodTo { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public bool? IsApprove { get; set; }
        public string VendorId { get; set; }
        public string VendorPIC { get; set; }
        public string VendorTaxNumber { get; set; }
        public string VendorAddress { get; set; }

        public string Ca_pceId { get; set; }
        public string Ca_PCETypeId { get; set; }
        public string Ca_BrandId { get; set; }
        public string Ca_ShareservicesId { get; set; }
        public string JobName { get; set; }
        
        public string BusinessUnitName { get; set; }
        public string RequestorDivision { get; set; }
        public string LegalEntityName { get; set; }
        public string MainServiceVategoryName { get; set; }
        

        public List<CashAdvanceSettlementDetail> CashAdvanceSettlementDetail { get; set; }

    }
}
