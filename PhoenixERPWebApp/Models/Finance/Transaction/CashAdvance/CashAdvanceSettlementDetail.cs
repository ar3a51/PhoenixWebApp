﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance
{
	public class CashAdvanceSettlementDetail : CashAdvanceDetail
	{
		[DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
		public decimal? ActualUnitPrice { get; set; }

		[DisplayName("Purchased Date")]
		public DateTime? PurchaseDate { get; set; }

		[DisplayName("Receipt")]
		public string Receipt { get; set; }

        public string CashAdvanceSettleId { get; set; }

        public string CashAdvanceDetailId { get; set; }
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? ActualQty { get; set; }
       
        public decimal? ActualAmount { get; set; }

        public string AccountId { get; set; }
        public string AccountCode { get; set; }

        public string FileReceiptNameDetail { get; set; }
        public string TempFileIdDetail { get; set; }

        public decimal? TaxPayable { get; set; }

        public decimal? TaxPayablePercent { get; set; }
        public string TaxPayableCoaId { get; set; }

        public string TaskName { get; set; }
        public string AccountName { get; set; }

        public string AccountIdView { get; set; }


    }
}
