﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance
{
    public class CashAdvance : FinanceEntityBase
    {

        [DisplayName("Id")]
        public string Id { get; set; }

        [DisplayName("CA request ID")]
        public string CaNumber { get; set; }

        [DisplayName("Date Request")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CaDate { get; set; }

        [DisplayName("Revision")]
        public int? Revision { get; set; }

        [DisplayName("Employee")]
        public string RequestedBy { get; set; }

        [DisplayName("Requested On")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? RequestedOn { get; set; }

        [DisplayName("Business Unit Id")]
        public string BusinessUnitId { get; set; }
        public string BusinessUnitName { get; set; }

        [DisplayName("Legal Entity")]
        public string LegalEntityId { get; set; }

        public string LegalEntityName { get; set; }

        [DisplayName("Is Project Cost")]
        public bool? IsProjectCost { get; set; }

        [DisplayName("Purchase Order Id")]
        public string PurchaseOrderId { get; set; }

        [DisplayName("PCE No.")]
        public string PurchaseOrderNumber { get; set; }

        [DisplayName("Job Id")]
        public string JobId { get; set; }

        [DisplayName("PCA No.")]
        public string JobNumber { get; set; }

        [DisplayName("Client Name")]
        public string ClientName { get; set; }


        public string ClientId { get; set; }

        [DisplayName("Main Service Category")]
        public string ProjectActivity { get; set; }

        [DisplayName("Project Area")]
        public string ProjectArea { get; set; }

        [DisplayName("Vendor")]
        public string VendorId { get; set; }

        [DisplayName("Vendor Name")]
        public string VendorName { get; set; }

        [DisplayName("Employee Id")]
        public string EmployeeId { get; set; }

        [DisplayName("Notes")]
        public string Notes { get; set; }

        [DisplayName("Payee Bank Id")]
        public string PayeeBankId { get; set; }

        [DisplayName("Payee Account Number")]
        public string PayeeAccountNumber { get; set; }

        [DisplayName("Payee Account Name")]
        public string PayeeAccountName { get; set; }

        [DisplayName("Payee Bank Branch")]
        public string PayeeBankBranch { get; set; }

        [DisplayName("Period From")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CaPeriodFrom { get; set; }

        [DisplayName("Period To")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CaPeriodTo { get; set; }

        [DisplayName("Approval Id")]
        public string ApprovalId { get; set; }

        [DisplayName("Status")]
        public string CaStatusId { get; set; }

        [DisplayName("Status")]
        public string StatusDesc { get; set; }

        [DisplayName("Division")]
        public string RequestBusinessUnitId { get; set; }

        public string RequestBusinessUnitName { get; set; }

        [DisplayName("Share services")]
        public string ShareservicesId { get; set; }

        [DisplayName("PCE Type")]
        public string PCETypeId { get; set; }

        [DisplayName("Client Brand")]
        public string ClientBrandId { get; set; }

        public string VendorTaxNumber { get; set; }
        public string VendorBrandId { get; set; }
        public string VendorPIC { get; set; }
        public string VendorAddress { get; set; }
        public string VendorTermOfPayment { get; set; }
        public string MainServiceCategoryName { get; set; }

        public string PaytoBank { get; set; }

        public bool IsApprove { get; set; }
        public string MainserviceCategoryId { get; set; }
        public string SatlementNumber { get; set; }

        public string RemarkRejected { get; set; }

        public int? Status
        {
            get
            {
                int res = 0;
                if (int.TryParse(CaStatusId, out res))
                {
                    return res;
                }
                else
                {
                    return null;
                }

            }
            set
            {
                CaStatusId = value.HasValue ? value.Value.ToString() : "";
            }
        }

        public InfoEmployee RequestEmployee { get; set; }

        public List<CashAdvanceDetail> CashAdvanceDetails { get; set; }

        public string RequestEmployeeId { get; set; }

        public string RequestEmployeeName { get; set; }

        public string RequestJobTitleId { get; set; }

        public string RequestJobTitleName { get; set; }

        public string RequestJobGradeId { get; set; }

        public string RequestJobGradeName { get; set; }

        public decimal? Amount { get; set; }


        //Sattement Cash Advance 
        public decimal? SubTotalCa { get; set; }
        public decimal? CashAdvanceCa { get; set; }
        public decimal? BalanceCa { get; set; }
        public decimal? ValueAddedTaxCa { get; set; }
        public decimal? ValueAddedTaxPercentCa { get; set; }
        public decimal? ValueAddedTaxOnCa { get; set; }
        public decimal? TaxPayableCa { get; set; }
        public decimal? TaxPayablePercentCa { get; set; }
        public decimal? TaxPayableOnCa { get; set; }
        public decimal? TotalCa { get; set; }
        public string LogDescription { get; set; }
        public string JobName { get; set; }
    }
}
