﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance.Transaction
{
    public class CashAdvanceSattlementDTO
    {
        public CashAdvanceSattlement Header { get; set; }
        public List<CashAdvanceSettlementDetail> Details { get; set; }
        
        public IFormFile fileUpload { get; set; }
    }
}
