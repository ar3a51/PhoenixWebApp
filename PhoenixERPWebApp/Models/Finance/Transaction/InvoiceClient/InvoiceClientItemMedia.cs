﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance.Transaction
{
    public class InvoiceClientItemMedia
    {
        [DisplayName("Id")]
        public string Id { get; set; }

        [DisplayName("Parent Id")]
        public string ParentId { get; set; }

        [DisplayName("Invoice Id")]
        public string InvoiceId { get; set; }

        [DisplayName("Amount")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal Amount { get; set; }

        [DisplayName("Sequence")]
        public short? Sequence { get; set; }

        [DisplayName("Media")]
        public string Media { get; set; }

        [DisplayName("Version")]
        public string Version { get; set; }

        [DisplayName("Date")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Date { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Rate")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? Rate { get; set; }

        [DisplayName("Ins")]
        public short? Ins { get; set; }

        [DisplayName("Gross")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? Gross { get; set; }

        [DisplayName("Disc")]
        public short? Disc { get; set; }

        [DisplayName("Total")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? Total { get; set; }
    }
}
