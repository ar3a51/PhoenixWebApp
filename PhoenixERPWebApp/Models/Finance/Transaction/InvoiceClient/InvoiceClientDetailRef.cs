﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance.Transaction
{
    public class InvoiceClientDetailRef 
    {
        [DisplayName("Id")]
        public string Id { get; set; }

        [DisplayName("PCE No")]
        public string PceCode { get; set; }


        [DisplayName("Brand Name")]
        public string BrandName { get; set; }

         [DisplayName("Sub Brand")]
        public string SubBrandName { get; set; }

         [DisplayName("Task Detail")]
        public string TaskDetail { get; set; }

        

         [DisplayName("Category")]
        public string Category { get; set; }


           [DisplayName("PCE Value")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? PceValue { get; set; }

           [DisplayName("Balance")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? Balance { get; set; }



          [DisplayName("Grand Total")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? GrandTotal { get; set; }


    }
}
