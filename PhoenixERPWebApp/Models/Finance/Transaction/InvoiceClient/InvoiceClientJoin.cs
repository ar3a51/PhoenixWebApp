﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance.Transaction
{
    public class InvoiceClientJoin 
    {
        [DisplayName("Id")]
        public string Id { get; set; }

        [DisplayName("Amount")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? Amount { get; set; }

        [DisplayName("PO Date")]
        public DateTime? PoDate { get; set; }

        [DisplayName("Pce")]
        public string PceId { get; set; }

        [DisplayName("Remarks")]
        public string Remarks { get; set; }

        [DisplayName("PCE No")]
        public string PceCode { get; set; }

        [DisplayName("Brand")]
        public string BrandId { get; set; }

        [DisplayName("Brand Name")]
        public string BrandName { get; set; }

        [DisplayName("Client")]
        public string ClientId { get; set; }

        [DisplayName("Client Name")]
        public string ClientName { get; set; }

        [DisplayName("Currency")]
        public string CurrencyId { get; set; }

        [DisplayName("Currency Name")]
        public string CurrencyName { get; set; }

        [DisplayName("InvoiceDate")]
        public DateTime? InvoiceDate { get; set; }

        [DisplayName("InvoiceName")]
        public string InvoiceName { get; set; }

        [DisplayName("Status")]
        public string Status { get; set; }

        [DisplayName("Due Date")]
        public DateTime? DueDate { get; set; }

         [DisplayName("Alternate Client")]
        public string AlternateClientId { get; set; }

        [DisplayName("Vat")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? Vat { get; set; }


        [DisplayName("Exchange Rate")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? ExchangeRate { get; set; }

        [DisplayName("Grand Total")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? GrandTotal { get; set; }


        [DisplayName("Job Name")]
        public string JobName { get; set; }

        [DisplayName("Job Status")]
        public string JobStatus { get; set; }
    }
}
