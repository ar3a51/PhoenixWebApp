﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance.Transaction
{
    public class InvoiceClient : FinanceEntityBase
    {
        [DisplayName("Id")]
        public string Id { get; set; }

        [DisplayName("Pce Id")]
        public string PceId { get; set; }

        [DisplayName("Pce Seq")]
        public int? PceSeq { get; set; }

        [DisplayName("Brand Id")]
        public string BrandId { get; set; }

        [DisplayName("Client Id")]
        public string ClientId { get; set; }

        [DisplayName("Alternate Client Id")]
        public string AlternateClientId { get; set; }

        [DisplayName("Currency Id")]
        public string CurrencyId { get; set; }

        [DisplayName("Invoice Date")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? InvoiceDate { get; set; }

        [DisplayName("Invoice Number")]
        public string InvoiceNumber { get; set; }

        [DisplayName("Start Period")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? StartPeriod { get; set; }

        [DisplayName("End Period")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EndPeriod { get; set; }

        //[DisplayName("Due Date")]
        //[DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        //public DateTime? DueDate { get; set; }

        [DisplayName("Vat")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? Vat { get; set; }

        [DisplayName("Sub Total Amount")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? TotalAmount { get; set; }

        [DisplayName("Exchange Rate")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? ExchangeRate { get; set; }

        [DisplayName("Job Name")]
        public string JobName { get; set; }

        [DisplayName("Description")]
        public string Remarks { get; set; }

        [DisplayName("Estimate Number")]
        public string EstimateNumber { get; set; }

        [DisplayName("Ref Number")]
        public string RefNumber { get; set; }

        [DisplayName("Your Ref")]
        public string YourRef { get; set; }

        [DisplayName("Status")]
        public string Status { get; set; }

        [DisplayName("Status")]
        public string ARStatus { get; set; }

        [DisplayName("Account")]
        public string AccountId { get; set; }

        [DisplayName("ASF")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? Asf { get; set; }

        [DisplayName("ASF Amount")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? AsfAmount { get; set; }

        public string PoDocFileId { get; set; }
        public string VatDocFileId { get; set; }




        //------------
        //Not Mapped
        //------------
        [DisplayName("Invoice Delivery Date")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? InvoiceDeliveryDate { get; set; }
        public string InvoiceDeliveryNo { get; set; }
        public string PoDocFileName { get; set; }
        public string VatDocFileName { get; set; }
        [DisplayName("Pce Balance")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? PceBalance { get; set; }

        [DisplayName("Subtotal")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? PceSubtotal { get; set; }

        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        [DisplayName("ASF")]
        public decimal? PceOtherFee { get; set; }

        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        [DisplayName("Total")]
        public decimal? PceTotal { get; set; }

        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? PceVat { get; set; }

        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? PceVatPercentage { get; set; }
        public decimal? totalamtplusasf { get; set; }

        [DisplayName("PCE Id")]
        public string PceCode { get; set; }

        [DisplayName("Grand Total")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? PceGrandTotal { get; set; }

        [DisplayName("Currency")]
        public string CurrencyCode { get; set; }
        [DisplayName("Main Service Category")]
        public string MainServiceCategory { get; set; }
        public string MainServiceCategoryName { get; set; }
        public string JobId { get; set; }

        [DisplayName("Job Status")]
        public string JobStatus { get; set; }
        public string LegalId { get; set; }
        public string LegalEntity { get; set; }
        public string Brand { get; set; }

        [DisplayName("PO Client No")]
        public string POClientNo { get; set; }
        public string ClientName { get; set; }
        public string ClientBrandName { get; set; }
        public string ClientInvAddress { get; set; }
        public string ClientTaxNumber { get; set; }
        public int? TermOfPayment { get; set; }
        public string Division { get; set; }
        public string BusinessUnitId { get; set; }
        public string AffiliationId { get; set; }

        [DisplayName("Affiliation")]
        public string AffiliationName { get; set; }

        [DisplayName("VAT No")]
        public string VatNo { get; set; }

        [DisplayName("VAT (%)")]
        public decimal? VAtPercentage { get; set; }

        [DisplayName("Grand Total Amount")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? GrandTotal { get; set; }

        [DisplayName("PIC")]
        public string ClientPIC { get; set; }
        public List<InvoiceClientItemOther> items_other { get; set; }
        public string AccountBankName { get; set; }
        public string AccountBankNumber { get; set; }

        [DisplayName("Billed")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? InvoiceBilled { get; set; }

        public bool? isApprove { get; set; }

        //used in AR for description jurnal
        [DisplayName("Description")]
        public string PayDescription { get; set; }
        public string MultiPceId { get; set; }
        public string PceIdComaSeparated { get; set; }
        
        public string LegalEntityName { get; set; }
        public string DivisionName { get; set; }
        public string SourceOption { get; set; }

        // Other source
        public string Other_TaskName { get; set; }
        public decimal Other_Quantity { get; set; }
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal Other_UnitPrice { get; set; }
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal Other_Total { get; set; }
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal Other_Amount { get; set; }
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal Other_AmountBilled { get; set; }
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal Other_Balance { get; set; }
        //
        public string IdFilter
        {
            get
            {
                return string.Format("{0}{1}{2}{3}{4}", LegalId, BusinessUnitId, MainServiceCategory, ClientId, CurrencyId);
            }
        }

        public List<InvoiceClient> PceListMultiple { get; set; }

        public List<InvoiceClientItemOther> InvoiceMultipleListOther { get; set; }

        public decimal? BillPercent { get; set; }
        public string StatusName { get; set; }

        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? SubtotalPlusAsf { get; set; }
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? BilledAsfPercent { get; set; }
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? Subtotals { get; set; }
    }
}
