﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PhoenixERPWebApp.Models.Finance.Transaction
{
    public class InvPCE
    {
        [DisplayName("Invoice No")]
        public string InvoiceNumber { get; set; }
        [DisplayName("Pce ID")]
        public string PceId { get; set; }
        [DisplayName("Pce Code")]
        public string PceCode { get; set; }
        [DisplayName("Subtotal")]
        public decimal? PceSubtotal { get; set; }
        [DisplayName("Other Fee")]
        public decimal? PceOtherFee { get; set; }
        [DisplayName("VAT")]
        public decimal? PceVat { get; set; }
        [DisplayName("Grand Total")]
        public decimal? PceGrandTotal { get; set; }
        [DisplayName("Currency")]
        public string Currency { get; set; }
        public string CurrencyId { get; set; }
        
        [DisplayName("Job Id")]
        public string JobId { get; set; }
        [DisplayName("Job Name")]
        public string JobName { get; set; }
        [DisplayName("Legal Id")]
        public string LegalId { get; set; }
        [DisplayName("Entity")]
        public string LegalEntity { get; set; }

        [DisplayName("PO Client No")]
        public string POClientNo { get; set; }

        [DisplayName("Status Job")]
        public string JobStatus { get; set; }
        [DisplayName("Brand Id")]
        public string BrandId { get; set; }
        [DisplayName("Brand Name")]
        public string BrandName { get; set; }
        [DisplayName("Company Id")]
        public string CompanyId { get; set; }
        [DisplayName("Company Name")]
        public string CompanyName { get; set; }
        [DisplayName("Job Status")]
        public string JobStatusId { get; set; }
        [DisplayName("Status")]
        public string InvStatus { get; set; }        
        public string JobRemark { get; set; }
        public string ServiceType { get; set; }

        public DateTime? UpdateDate { get; set; }

    }
}
