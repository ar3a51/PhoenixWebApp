﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PhoenixERPWebApp.Models.Finance.Transaction
{
    public class InvPCEDtlView
    {
        public InvPCE pce { get; set; }
        public PCECompany pceCompany { get; set; }

        public InvoiceClient invCl { get; set; }
        public List<InvoiceClientItemOther> items { get; set; }
    }

    public class PCECompany
    {
        public string CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string BrandId { get; set; }
        public string BrandName { get; set; }
        public string AddressInv { get; set; }
        public string TaxNumber { get; set; }
    }
}
