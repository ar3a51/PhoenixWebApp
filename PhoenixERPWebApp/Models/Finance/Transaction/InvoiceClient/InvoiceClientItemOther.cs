﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance.Transaction
{
    public class InvoiceClientItemOther
    {
        [DisplayName("Id")]
        public string Id { get; set; }

        [DisplayName("Parent Id")]
        public string ParentId { get; set; }

        [DisplayName("Invoice Id")]
        public string InvoiceId { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Remark")]
        public string Remark { get; set; }

        [DisplayName("Bill Amount")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal Amount { get; set; }

        [DisplayName("Sequence")]
        public short? Sequence { get; set; }

        [DisplayName("Account Id")]
        public string AccountID { get; set; }

        [DisplayName("Account Name")]
        public string AccountName { get; set; }

        public string PceId { get; set; }
        public string JobId { get; set; }

        [DisplayName("Bill Percent")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal AmountPercentage { get; set; }

        //--- PCE TASK ---
        public string TaskId { get; set; }
        public string TaskName { get; set; }
        [DisplayName("Qty")]
        public int? Quantity { get; set; }
        public string UomId { get; set; }

        [DisplayName("Price")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? Price { get; set; }
        public decimal? Asf { get; set; }
        public decimal? AsfPercent { get; set; }
        public decimal? Vat { get; set; }
        public decimal? VatPercent { get; set; }

        [DisplayName("Unit Price")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? UnitPrice { get; set; }

        [DisplayName("Total Price")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? Total { get; set; }

        [DisplayName("Billed")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? AmountBilled { get; set; }

        [DisplayName("Balance")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal? Balance { get; set; }
        public decimal? TempBalance { get; set; }

        public bool IsSelected { get; set; }

        [DisplayName("Total Percent")]
        public decimal TotalBillPercent { get; set; }

        [DisplayName("Grand Total")]
        public decimal? GrandTotal { get; set; }

        [DisplayName("Invoice Date")]
        public DateTime? InvoiceDate { get; set; }
    }
}
