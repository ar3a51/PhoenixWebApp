﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance.Transaction.PettyCashSettlement
{
    public class PettyCashSettlementDetailList : FinanceEntityBase
    {
        public string Id { get; set; }
        public DateTime Date { get; set; }
        public string Nota { get; set; }
        public string Reference { get; set; }
        public string Transaction { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
    }
}
