﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance.Transaction.PettyCashSettlement
{
    public class PettyCashSettlementList : FinanceEntityBase
    {
        public string Id { get; set; }
        public string PettyCashSettlementNo { get; set; }
        public DateTime SettlementDate { get; set; }
        public string CashHolder { get; set; }
        public string Division { get; set; }
        public DateTime BeginningDate { get; set; }
        public DateTime EndingDate { get; set; }
        public string AccountPayableNo { get; set; }
        public string Currency { get; set; }
        public decimal Amount { get; set; }
        public string Remarks { get; set; }
        public string Status { get; set; }
    }
}
