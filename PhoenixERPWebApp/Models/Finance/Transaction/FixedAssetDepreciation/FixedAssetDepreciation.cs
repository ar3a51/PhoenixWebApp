﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PhoenixERPWebApp.Models
{   
    public class FixedAssetDepreciation : FinanceEntityBase
    {   
        public string Id { get; set; }
        
        public string FixedAssetID { get; set; }
        
        public DateTime? DepreciationDate { get; set; }
        
        public decimal? DepreciationValue { get; set; }
        
        public decimal? AccumulatedDepreciation { get; set; }
        
        public decimal? BookValue { get; set; }
        
        public decimal? Residual { get; set; }
        
        public string Status { get; set; }
      
        public string DepreciationName { get; set; }
     
        public string DepreciationId { get; set; }
    }
}
