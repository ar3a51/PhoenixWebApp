﻿using System.Collections.Generic;

namespace PhoenixERPWebApp.Models.Finance.Transaction.FixedAsset
{
    public class FixedAssetDTO
    {
        public FixedAsset Header { get; set; }
        public List<FixedAssetMovement> Details { get; set; }
    }
}
