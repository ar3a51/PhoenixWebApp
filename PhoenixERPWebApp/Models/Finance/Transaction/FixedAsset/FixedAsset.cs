﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PhoenixERPWebApp.Models.Finance.Transaction.FixedAsset
{
    public class FixedAsset : FinanceEntityBase
    {

        public string Id { get; set; }

        public string ItemId { get; set; }

        public string FixedAssetName { get; set; }

        public string FixedAssetNumber { get; set; }

        public decimal? UnitCount { get; set; }

        public string PurchaseOrderId { get; set; }

        public string InvoiceId { get; set; }

        public string EvidentNumber { get; set; }

        public decimal? CurrentPrice { get; set; }

        public string CurrencyId { get; set; }

        public decimal? ExchangeRate { get; set; }

        public string CurrentLocation { get; set; }

        public string AccountId { get; set; }

        public string CommercialDepreciationId { get; set; }

        public int? EconomicPeriod { get; set; }

        public string Description { get; set; }

        public DateTime? RegistrationDate { get; set; }

        public string ItemTypeId { get; set; }

        public string ItemGroupId { get; set; }

        public string ItemCategoryId { get; set; }

        public string ItemConditionId { get; set; }

        public string SerialNumber { get; set; }

        public decimal? GrossValue { get; set; }

        public decimal? Residue { get; set; }

        public decimal? AcquisitionCost { get; set; }

        public string Rounding { get; set; }

        public string CoaIdAkumulasi { get; set; }

        public string CoaIdDepresiai { get; set; }

        public string GoodReceiptId { get; set; }

        public string GoodReceiptDetailId { get; set; }
        public string Manufacture { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public string ProductNumber { get; set; }
        public string SkuCode { get; set; }
        public string Color { get; set; }
        public decimal? Qty { get; set; }
        public string Uom { get; set; }
        public string Condition { get; set; }
        public string BusinessUnit { get; set; }
        public string LegalEntity { get; set; }
        public string Affiliation { get; set; }
        public decimal? Lifetime { get; set; }
        public string MasterDepreciationId { get; set; }
        public string StatusDepreciation { get; set; }
        public string RemarksDepreciation { get; set; }
        [NotMapped]
        public string Status { get; set; }
        public string ConditionName { get; set; }
        public string LocationName { get; set; }
    }
}
