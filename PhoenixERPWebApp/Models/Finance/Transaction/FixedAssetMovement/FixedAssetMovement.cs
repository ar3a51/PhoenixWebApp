﻿using PhoenixERPWebApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PhoenixERPWebApp.Models
{
    public class FixedAssetMovement : FinanceEntityBase
    {   
        public string Id { get; set; }
     
        public string FixedAssetId { get; set; }
     
        public string FaaNumber { get; set; }
     
        public DateTime? FaaDateTime { get; set; }
     
        public string FromLocationID { get; set; }
     
        public string ToLocationID { get; set; }
     
        public string FromBusinessUnitID { get; set; }
     
        public string ToBusinessUnitID { get; set; }
     
        public DateTime? ExecutionDate { get; set; }
     
        public string BusinessUnitID { get; set; }
     
        public string LegalEntityID { get; set; }
     
        public string AffiliationID { get; set; }
     
        public string FixedAssetMovementStatusID { get; set; }
     
        public string Remark { get; set; }
 
        public string ApprovalID { get; set; }

        public string Condition { get; set; }

        public string Employee { get; set; }

        [NotMapped]
        public string LocationName { get; set; }
        [NotMapped]
        public string ConditionName { get; set; }
        [NotMapped]
        public string EmployeeName { get; set; }
        [NotMapped]
        public string Division { get; set; }
        [NotMapped]
        public string LegalEntityName { get; set; }
        [NotMapped]
        public string AffiliationName { get; set; }
        [NotMapped]
        public string StatusName { get; set; }
    }
}
