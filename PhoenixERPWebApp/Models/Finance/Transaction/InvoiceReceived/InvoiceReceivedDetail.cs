﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance.Transaction.InvoiceReceived
{
    public class InvoiceReceivedDetail : FinanceEntityBase
    {
        public string Id { get; set; }
        public DateTime InvoiceReceivedDate { get; set; }
        public string InvoiceReceivedNo { get; set; }
        public string Description { get; set; }
        public string PurchaseOrderNo { get; set; }
        public string ReferenceFromVendor { get; set; }
        public string Ref { get; set; }
        public string RefNumber { get; set; }
        public DateTime ReferenceDate { get; set; }
        public string Dept { get; set; }
        public string Currency { get; set; }
        public decimal Amount { get; set; }
        public string TaxNo { get; set; }
        public string Exchange { get; set; }
        public DateTime TaxDate { get; set; }
        public bool PoCheckList { get; set; }
        public bool TaxCheckList { get; set; }
        public bool ContractCheckList { get; set; }
        public bool DoCheckList { get; set; }
        public bool GrCheckList { get; set; }
        public string VendorName { get; set; }
        public string VendorNPWP { get; set; }
        public string VendorAddress { get; set; }
        public string VendorCity { get; set; }
        public string VendorZipCode { get; set; }
        public string VendorContactPerson { get; set; }
        public string VendorContactNumber { get; set; }
        public bool Approval { get; set; }
        public string Notes { get; set; }


        public decimal? AmountPaid { get; set; }
        public decimal? Discount { get; set; }
        public decimal? Tax { get; set; }
        public decimal? EndingBalance { get; set; }
        public int? RowIndex { get; set; }

        
        public string TaskDetail { get; set; }

        public string ItemName { get; set; }

        public string ItemDescription { get; set; }

        public decimal? qty { get; set; }

        public decimal? unit_price { get; set; }

        public decimal? amountTotal { get; set; }

        public string UomId { get; set; }

        public string Item_type_id { get; set; }

        public string ItemTypeName { get; set; }

        public string uom_name { get; set; }
        public string AccountId { get; set; }
        public string AccountCode { get; set; }
        public string AccountName { get; set; }

        public string TaxPayableCoaId { get; set; }
    
        public decimal? TaxPayablePercent { get; set; }
  
        public string TaxPayableCoaName { get; set; }

    }
}
