﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PhoenixERPWebApp.Models.Finance.Transaction.InvoiceReceived;

namespace PhoenixERPWebApp.Models.Finance.Transaction.InvoiceReceived
{
    public class InvoiceReceivedDTO
    {
        public InvoiceReceivedList Header { get; set; }
        public List<InvoiceReceivedDetail> Details { get; set; }
    }
}
