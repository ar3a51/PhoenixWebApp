﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance.Transaction.InvoiceReceived
{
    public class InvoiceReceivedDetailList
    {
        public string Id { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? TaxDate { get; set; }
        public decimal? AmountPaid { get; set; }
        public decimal? Discount { get; set; }
        public decimal? Tax { get; set; }
        public decimal? EndingBalance { get; set; }
        public int? RowIndex { get; set; }    
        public string TaskDetail { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public decimal? qty { get; set; }
        public decimal? unit_price { get; set; }
        public decimal? amountTotal { get; set; }
        public string ItemTypeId { get; set; }
        public string ItemTypeName { get; set; }
        public string UomId { get; set; }
        public string uom_name { get; set; }
        public string ItemId { get; set; }
        public string AccountId { get; set; }
        public string AccountName { get; set; }
        public string AccountCode { get; set; }
        public string TaxPayableCoaId { get; set; }
        public string TaxPayableCoaName { get; set; }
        public decimal? TaxPayablePercent { get; set; }

    }
}
