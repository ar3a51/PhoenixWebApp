﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance.Transaction.InvoiceReceived
{
    public class InvoiceReceivedList : FinanceEntityBase
    {
        public string Id { get; set; }

        public DateTime? InvoiceDate { get; set; }

        public string InvoiceNumber { get; set; }

        public string ClientId { get; set; }

        public string PoId { get; set; }

        public string JobId { get; set; }
        public string JobName { get; set; }

        public string PceId { get; set; }

        public string InvoiceVendor { get; set; }

        public string InvoiceVendorText { get; set; }

        public DateTime? InvoiceVendorDate { get; set; }

        public decimal Value { get; set; }

        public string CurrencyId { get; set; }

        public string Termin { get; set; }

        public decimal ExchangeRate { get; set; }

        public decimal Amount { get; set; }

        public decimal Vat { get; set; }

        public decimal TotalAmount { get; set; }
        public decimal? TotalCostVendor { get; set; }

        public string TaxNumber { get; set; }
        public string GoodReceivedNumber { get; set; }
        public string ServiceReceivedNumber { get; set; }

        public DateTime? TaxDate { get; set; }

        public bool? PoCheckList { get; set; }
        public bool? PoCheckListData { get; set; }

        public string PoObject { get; set; }

        public bool? TaxCheckList { get; set; }
        public bool? diagio { get; set; }
        public bool? TaxCheckListData { get; set; }

        public string TaxObject { get; set; }

        public bool? ContractCheckList { get; set; }
        public bool? ContractCheckListData { get; set; }


        public string ContractObject { get; set; }

        public bool? DoCheckList { get; set; }
        public bool? DoCheckListData { get; set; }

        public string DoObject { get; set; }

        public bool? GrCheckList { get; set; }
        public bool? GrCheckListData { get; set; }


        public string GrObject { get; set; }

        public bool? InvCheckList { get; set; }
        public bool? InvCheckListData { get; set; }
        public string InvObject { get; set; }

        public bool? ApprovalFrontDesk { get; set; }

        public string FrontDeskNotes { get; set; }

        public bool? ApprovalAdminDesk { get; set; }

        public string AdminNotes { get; set; }

        public string MainServiceId { get; set; }
        public string MainServiceCategoryId { get; set; }
        
        public string ServiceCenterId { get; set; }

        public string DivisionId { get; set; }
        public string DivisionName { get; set; }

        public string LegalId { get; set; }

        public string AffiliationId { get; set; }
        [DisplayName("InvoiceStatusId")]
        public string InvoiceStatusId { get; set; }

        public string Description { get; set; }

        public string VendorName { get; set; }

        public string VendorNPWP { get; set; }

        public string VendorAddress { get; set; }

        public string VendorCity { get; set; }

        public string VendorZipCode { get; set; }

        public string VendorContactPerson { get; set; }

        public string VendorContactNumber { get; set; }

        public string PoNumber { get; set; }

        public string ClientName { get; set; }

        public string StatusName { get; set; }

        public string CurrencyName { get; set; }

        //public Pce PCE { get; set; }
        public string purchase_request_number { get; set; }
        public PurchaseOrder purchaseOrder { get; set; }
        public decimal? DueDays { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? Periode { get; set; }

        //inv detail tambahan untuk detail tap dan ap
        public decimal? NetAmount { get; set; }
        public decimal? PromptPayment { get; set; }
        public decimal? PromptPayment2 { get; set; }
        public decimal? OtherDiscount { get; set; }
        public decimal? ValueAddedTax { get; set; }
        public decimal? ValueAddedTaxPercent { get; set; }
        public decimal? ValueAddedTaxOn { get; set; }
        public decimal? AdvertisingTax { get; set; }
        public decimal? TaxPayable { get; set; }
        public decimal? TaxPayablePercent { get; set; }
        public decimal? TaxPayableOn { get; set; }
        public decimal? AllocationRatio { get; set; }

        public string InvoicingTypeId { get; set; }
        public string InvoicingType { get; set; }
        public string PaymentMethodId { get; set; }
        public string PoDescription { get; set; }
        public string PercentPaymentTerm1 { get; set; }
        public string PercentPaymentTerm2 { get; set; }
        public string PercentPaymentTerm3 { get; set; }

        public string PaymentTerm1 { get; set; }
        public string PaymentTerm2 { get; set; }
        public string PaymentTerm3 { get; set; }

        public string RequestForQuotationId { get; set; }
        public string BusinessUnitId { get; set; }
        public string LegalEntityId { get; set; }
        public string LegalEntityName { get; set; }
        public string PCANumber { get; set; }
        public string PCENumber { get; set; }
        public string TypeOfExpenseId { get; set; }
    }
}
