﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance.Transaction.AccountPayableAging
{
    public class AccountPayableAgingList : FinanceEntityBase
    {
        public string Id { get; set; }
        public string ClientName { get; set; }
        public string Invoice { get; set; }
        public DateTime Date { get; set; }
        public string APNumber { get; set; }
        public DateTime APDate { get; set; }
        public DateTime Received { get; set; }
        public DateTime DueDate { get; set; }
        public int Day { get; set; }
        public string YourRef { get; set; }
        public string Remarks { get; set; }
        public string JobId { get; set; }
        public string Divisi { get; set; }
        public string Legal { get; set; }
        public int P0130 { get; set; }
        public int P3160 { get; set; }
        public int P6190 { get; set; }
        public int P90 { get; set; }
        public int NotDue { get; set; }
        public int OverDue { get; set; }
        public int May0107 { get; set; }
        public int May0814 { get; set; }
        public int May1522 { get; set; }
        public int May2330 { get; set; }
    }
}
