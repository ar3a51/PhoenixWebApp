﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models
{
    public class ViewItem
    {
        public string Id { get; set; }
        public string ItemNumber { get; set; }
        public string ItemName { get; set; }
    }
}
