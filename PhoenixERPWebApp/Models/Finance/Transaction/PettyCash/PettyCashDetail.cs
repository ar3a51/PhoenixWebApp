﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance
{
	public class PettyCashDetail : FinanceEntityBase
	{
		[DisplayName("Id")]
		public string Id { get; set; }

		[DisplayName("Petty Cash Id")]
		public string PettyCashId { get; set; }

		[DisplayName("Row Id")]
		public int? RowId { get; set; }

		[DisplayName("Reference")]
		public string Reference { get; set; }

		[DisplayName("Account Id")]
		public string AccountId { get; set; }

		[DisplayName("Date")]
		[DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
		public DateTime? TransactionDate { get; set; }

		[DisplayName("Description")]
		public string TransactionDetail { get; set; }

		[DisplayName("Debit")]
		[DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
		public decimal? Debit { get; set; }

		[DisplayName("Credit")]
		[DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
		public decimal? Credit { get; set; }

		[DisplayName("Filemaster Id")]
		public string FilemasterId { get; set; }
		public string FilemasterName { get; set; }
		public IFormFile File { get; set; }

		[DisplayName("Job Name")]
		public string JobName { get; set; }
		public string JobId { get; set; }
		public string PceId { get; set; }
		public string PcaId { get; set; }
		public string PcaTaskId { get; set; }
		public string PcaTask { get; set; }
		public bool IsFromJE { get; set; }
		public string AccountName { get; set; }
		public string CodeRec { get; set; }
		//public PettyCash PettyCash { get; set; }
		//public Account Account { get; set; }
	}
}
