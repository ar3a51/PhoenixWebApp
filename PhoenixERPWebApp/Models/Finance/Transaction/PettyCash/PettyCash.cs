﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance
{
	public class PettyCash : FinanceEntityBase
	{
		[DisplayName("Petty Cash ID")]
		public string Id { get; set; }

		[DisplayName("Settlement Date")]
		[DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
		public DateTime? SettlementDate { get; set; }

		[DisplayName("Legal Entity")]
		public string LegalEntityId { get; set; }

		[DisplayName("Bussines Unit")]
		public string BusinessUnitId { get; set; }

		[DisplayName("Affiliation Id")]
		public string AffiliationId { get; set; }

		[DisplayName("Cash Holder Id")]
		public string CashHolderId { get; set; }

		[DisplayName("Date")]
		[DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
		public DateTime? MsPettyCashDate { get; set; }

		[DisplayName("Currency")]
		public string CurrencyId { get; set; }

		[DisplayName("Exchange Rate")]
		public decimal? ExchangeRate { get; set; }

		[DisplayName("Remarks")]
		public string Remarks { get; set; }

		[DisplayName("Approval Id")]
		public string ApprovalId { get; set; }

		[DisplayName("Bank Account")]
		public string BankAccountNumber { get; set; }

		[DisplayName("Bank Name")]
		public string BankName { get; set; }

		[DisplayName("Legal Entity")]
		public string LegalEntity { get; set; }

		[DisplayName("Bussiness Unit")]
		public string BusinessUnit { get; set; }

		[DisplayName("Affiliation")]
		public string Affiliation { get; set; }

		[DisplayName("Status")]
		public int Status { get; set; }

		public string StatusName { get; set; }
		public int? TrnStatus { get; set; }

		public bool IsOVerDueSettlement { get; set; }

		[DisplayName("Employee")]
		public string RequestorName { get; set; }

		[DisplayName("Bank Account")]
		public string PettyCashNumber { get; set; }

		public List<PettyCashDetail> Detail { get; set; }

		public bool? IsDraft { get; set; }

		public decimal? EndingBalance { get; set; }
		public string DescJournalEndingBalance { get; set; }
		public string GroupName { get; set; }
	}
}
