﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PhoenixERPWebApp.Models.Finance
{   
    public class GoodRequest : FinanceEntityBase
    {   
        public string Id { get; set; }
        
        public string OrganizationId { get; set; }
        
        public string GoodRequestNumber { get; set; }
        
        public DateTime? GoodRequestDatetime { get; set; }
        
        public string CompletionDatetime { get; set; }
        
        public string JobId { get; set; }
        
        public string RequestedBy { get; set; }
        
        public DateTime? RequestedOn { get; set; }
        
        public string BusinessUnitId { get; set; }
        
        public string LegalEntityId { get; set; }
        
        public string AffiliationId { get; set; }
        
        public string Remark { get; set; }
        
        public DateTime? DUeDate { get; set; }
        
        public int? Status { get; set; }
        
        public string Type { get; set; }
        
        public string InvFaId { get; set; }
        
        public string InvFaName { get; set; }
        
        public string JobName { get; set; }
        
        public string EmployeeName { get; set; }
        
        public string BusinessUnitName { get; set; }
        
        public string LegalEntityName { get; set; }

        public decimal? TotalQty { get; set; }
        public DateTime? ReturnDate { get; set; }
        public string OrderReceipt { get; set; }
        [NotMapped]
        public string LocationTransfer { get; set; }
        [NotMapped]
        public string Purpose { get; set; }
    }
}
