﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance.Transaction.PaymentRequest
{
    public class PaymentRequestDTO
    {
        public PaymentRequestList Header { get; set; }
        public List<PaymentRequestDetail> Details { get; set; }
    }
}
