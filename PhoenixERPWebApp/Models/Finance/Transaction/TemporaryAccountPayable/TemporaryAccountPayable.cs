﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance.Transaction.TemporaryAccountPayable
{
    public class TemporaryAccountPayable : FinanceEntityBase
    {
        public string Id { get; set; }
        public string LegalEntityId { get; set; }
        public string BusinessUnitId { get; set; }
        public string AffiliationId { get; set; }
        public string AccountId { get; set; }
        public string VendorId { get; set; }
        public string JobId { get; set; }
        public string JobName { get; set; }
        public DateTime TransactionDate { get; set; }
        public DateTime? DueDate { get; set; }
        public string CurrencyId { get; set; }
        public decimal? ExchangeRate { get; set; }
        public string BankAccountDestination { get; set; }
        public decimal? Charges { get; set; }
        public string Remarks { get; set; }
      
        public string InvoiceReceivableId { get; set; }
        
        public string Type { get; set; }

        public string InvoiceReceivedId { get; set; }
        public string InvoiceReceivedNumber { get; set; }
        public DateTime? InvoiceReceivedDate { get; set; }
        public string OurRef { get; set; }
        public string BrandId { get; set; }
        public DateTime? Periode { get; set; }
        [DisplayName("Your Ref")]
        public string InvoiceVendorNumber { get; set; }
        public string DivisionId { get; set; }
        public DateTime? Received { get; set; }
        public string TaxNumber { get; set; }
        public DateTime? TaxDate { get; set; }
        public decimal? DueDays { get; set; }
        public string AllocationRatio { get; set; }
        [DisplayName("Total Net")]
        public decimal? Amount { get; set; }
        public decimal? PromptPayment { get; set; }
        public decimal? PromptPayment2 { get; set; }
        public decimal? OtherDiscount { get; set; }
        public decimal? ValueAddedTax { get; set; }
        public decimal? ValueAddedTaxPercent { get; set; }
        public decimal? ValueAddedTaxOn { get; set; }
        public decimal? AdvertisingTax { get; set; }
        public decimal? TotalCostVendor { get; set; }
        public decimal? TaxPayable { get; set; }
        public decimal? TaxPayablePercent { get; set; }
        public decimal? TaxPayableOn { get; set; }
        public string VATNumber { get; set; }
        public DateTime? ReceivedDate { get; set; }
        public string POId { get; set; }
        public string PONumber { get; set; }
        public string status { get; set; }

        public int APType { get; set; }
        public string InvoicePayableId { get; set; }
        public string OurRefId { get; set; }
        public string InvoiceStatusId { get; set; }
        public InvoiceReceived.InvoiceReceivedList invoiceReceivable { get; set; }
        public List<TemporaryAccountPayableDetailList> Details { get; set; }


        public string AdminNotes { get; set; }
        public string FrontDeskNotes { get; set; }
        public string DevisionName { get; set; }
        public string Termin { get; set; }
        public decimal? Value { get; set; }
        public decimal? Vat { get; set; }
        public decimal? TotalAmount { get; set; }
    }
}
