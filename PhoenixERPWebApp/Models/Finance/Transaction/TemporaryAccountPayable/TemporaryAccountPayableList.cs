﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance.Transaction.TemporaryAccountPayable
{
    public class TemporaryAccountPayableList : FinanceEntityBase
    {
        public string Id { get; set; }
        public string TAPNumber { get; set; }
        public string LegalEntityId { get; set; }
        public string BusinessUnitId { get; set; }
        public string AffiliationId { get; set; }
        public string AccountId { get; set; }
        public string VendorId { get; set; }
        public string VendorName { get; set; }
        public string JobId { get; set; }
        public DateTime TransactionDate { get; set; }
        public DateTime? DueDate { get; set; }
        public string CurrencyId { get; set; }
        public string CurrencyName { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Value { get; set; }
        public decimal? ExchangeRate { get; set; }
        public string BankAccountDestination { get; set; }
        public decimal? Charges { get; set; }
        public string Remarks { get; set; }
        
        public string Status { get; set; }
        public string StatusName { get; set; }
        public string InvoiceReceivedNumber { get; set; }
        public DateTime? InvoiceReceivedDate { get; set; }
        public string PONumber { get; set; }
        public string Department { get; set; }
        public string Divisi { get; set; }
        public string OurRefId { get; set; }
        public decimal? DueDays { get; set; }
        public string JobName { get; set; }

        public string OurRef { get; set; }
        public string APType { get; set; }
        public decimal? AllocationRatio { get; set; }
        public DateTime? Received { get; set; }
        public string  InvoiceStatusId { get; set; }
    }
}
