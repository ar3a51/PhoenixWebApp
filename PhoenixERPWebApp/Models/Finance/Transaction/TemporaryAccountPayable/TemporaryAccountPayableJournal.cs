﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance.Transaction.TemporaryAccountPayable
{
    public class TemporayAccountPayableJournal : FinanceEntityBase
    {
        public string Id { get; set; }
        public DateTime? transaction_date { get; set; }
        public string reference_id { get; set; }
        public DateTime? referencedate { get; set; }
        public string referenceto { get; set; }
        public string AccountId { get; set; }
        public string AccountName { get; set; }
        public string Description { get; set; }
        public string currency_id { get; set; }
        public decimal? qty { get; set; }
        public string unit { get; set; }
        public decimal? exchange_rate { get; set; }
        public decimal? price { get; set; }
        public string pce_id { get; set; }
        public string JobId { get; set; }
        public string JobIdName { get; set; }
        public string internal_id { get; set; }
        public string invoice_id { get; set; }
        public string po_id { get; set; }
        public decimal? budgetdebit { get; set; }
        public decimal? budgetcredit { get; set; }
        public decimal? budgetgballance { get; set; }
        public decimal? beginningdebit { get; set; }
        public decimal? beginningcredit { get; set; }
        public decimal? beginningballance { get; set; }
        public decimal? Debit { get; set; }
        public decimal? Credit { get; set; }
        public decimal? balance { get; set; }
        public decimal? endingdebet { get; set; }
        public decimal? endingcredit { get; set; }
        public decimal? endingbalance { get; set; }
        public string rcondition { get; set; }
        public bool? flag { get; set; }
        public DateTime? ftime { get; set; }
        public string mainservice_category_id { get; set; }
        public string shareservice_id { get; set; }
        public string DivisiId { get; set; }
        public string DivisiName { get; set; }
        public string legal_id { get; set; }
        public string affiliate_id { get; set; }
        public string is_posted { get; set; }
        public string AccountCode { get; set; }
        public string LineCode { get; set; }
    }
}
