﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance.Transaction.TemporaryAccountPayable
{
    public class TemporaryAccountPayableDTO
    {
        public TemporaryAccountPayable Header { get; set; }
        public List<TemporaryAccountPayableDetailList> Details { get; set; }
    }
}
