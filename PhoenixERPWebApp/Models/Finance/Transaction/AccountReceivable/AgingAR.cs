﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance.Transaction
{
    public class AgingAR
    {
        [DisplayName("Id")]
        public string Id { get; set; }

        [DisplayName("Client")]
        public string CompanyName { get; set; }

        [DisplayName("Brand")]
        public string CompanyBrand { get; set; }

        [DisplayName("Invoice Number")]
        public string InvoiceNumber { get; set; }

        [DisplayName("Date")]
        public DateTime? TransactionDate { get; set; }

        [DisplayName("Due Date")]
        public DateTime? DueDate { get; set; }

        [DisplayName("Day")]
        public Int32 Day { get; set; }

        [DisplayName("PO Number")]
        public string PONumber { get; set; }

        [DisplayName("Job Id")]
        public string JobId { get; set; }

        [DisplayName("Job Name")]
        public string JobName { get; set; }

        [DisplayName("Division")]
        public string Division { get; set; }

        [DisplayName("Legal Entity")]
        public string LegalEntity { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Coa Code")]
        public string CoaCode { get; set; }

        [DisplayName("Aging")]
        public int Aging { get; set; }

        public string I01_30 { get { return ((Aging >= 1 && Aging <= 30) ? 1 : 0).ToString(); } }
        public string I31_60 { get { return ((Aging >= 31 && Aging <= 60) ? 1 : 0).ToString(); } }
        public string I61_90 { get { return ((Aging >= 61 && Aging <= 90) ? 1 : 0).ToString(); } }
        public string IGt90 { get { return (Aging > 90 ? 1 : 0).ToString(); } }

        public string LegalEntityId { get; set; }
        public string DivisionId { get; set; }
        public string CompanyId { get; set; }
    }
}
