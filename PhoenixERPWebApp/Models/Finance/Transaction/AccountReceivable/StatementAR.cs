﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance.Transaction
{
    public class StatementAR
    {
        [DisplayName("Id")]
        public string Id { get; set; }

        [DisplayName("Client")]
        public string CompanyName { get; set; }

        [DisplayName("Brand")]
        public string CompanyBrand { get; set; }

        [DisplayName("Invoice Number")]
        public string InvoiceNumber { get; set; }

        [DisplayName("Transaction Date")]
        public DateTime? TransactionDate { get; set; }

        [DisplayName("Job Id")]
        public string JobId { get; set; }

        [DisplayName("Job Name")]
        public string JobName { get; set; }

        [DisplayName("Division")]
        public string Division { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Main Service Category")]
        public string MainServiceCategory { get; set; }

        [DisplayName("Debet")]
        public decimal Debet { get; set; }

        [DisplayName("Credit")]
        public decimal Credit { get; set; }

        [DisplayName("Balance")]
        public decimal Balance { get; set; }
        
    }
}
