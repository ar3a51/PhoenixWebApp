﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance.Transaction
{
    public class AccountPayableDTO
    {
        public AccountPayable Header { get; set; }
        public List<AccountPayableDetail> Details { get; set; }
    }
}
