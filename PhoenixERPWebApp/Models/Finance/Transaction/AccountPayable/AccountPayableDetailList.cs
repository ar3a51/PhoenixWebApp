﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance.Transaction
{
    public class AccountPayableDetailList : FinanceEntityBase
    {
        public string Id { get; set; }
        public string AccountPayableId { get; set; }
        public string Lte { get; set; }
        public string TaskDetail { get; set; }
        [DisplayName("Amount")]
        public decimal? AmountPaid { get; set; }
        public decimal? Discount { get; set; }
        public decimal? Tax { get; set; }
        public decimal? EndingBalance { get; set; }
        public int? RowIndex { get; set; }

        public string Ite { get; set; }

        public string ItemName { get; set; }

        public string ItemDescription { get; set; }

        public decimal? qty { get; set; }

        public decimal? unit_price { get; set; }

        public decimal? amountTotal { get; set; }

        public string uom_id { get; set; }

        public string Item_type_id { get; set; }

        public string ItemTypeName { get; set; }

        public string uom_name { get; set; }
        public string AccountId { get; set; }
        public string AccountCode { get; set; }
        public string AccountName { get; set; }
    }
}
