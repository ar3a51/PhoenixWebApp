﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Finance.Transaction
{
    public class AccountPayableJournalList : FinanceEntityBase
    {
        public string Id { get; set; }
        public string AccountId { get; set; }
        public string AccountCode { get; set; }
        public string AccountName { get; set; }
        public string Description { get; set; }
        public string JobId { get; set; }
        public string JobIdName { get; set; }
        public string Department { get; set; }
        public string DivisiId { get; set; }
        public string DivisiName { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
    }
}
