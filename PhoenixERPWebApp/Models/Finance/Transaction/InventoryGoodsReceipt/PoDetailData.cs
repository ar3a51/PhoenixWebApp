﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoenixERPWebApp.Models.Finance.Transaction.InventoryGoodsReceipt
{
    public class PoDetailData
    {
        public string IdPO { get; set; }
        public string JobId { get; set; }
        public string JobName { get; set; }
        public string ClientId { get; set; }
        public string ClientName { get; set; }
        public string BrandId { get; set; }
        public string BrandName { get; set; }
        public string LegalEntityId { get; set; }
        public string LegalEntityName { get; set; }
        public string DivisionId { get; set; }
        public string DivisionName { get; set; }
        //public decimal? Value { get; set; }
        //public decimal? Amount { get; set; }
        //public decimal? ExchangeRate { get; set; }
        public string purchase_request_id { get; set; }
        public string termin { get; set; }
        public string purchase_request_number { get; set; }
        public string InvoicingType { get; set; }
        public string InvoicingTypeId { get; set; }
        public string PaymentMethodId { get; set; }
        public string PoDescription { get; set; }
        public decimal? PercentPaymentTerm1 { get; set; }
        public decimal? PercentPaymentTerm2 { get; set; }
        public decimal? PercentPaymentTerm3 { get; set; }
        public decimal? PaymentTerm1 { get; set; }
        public decimal? PaymentTerm2 { get; set; }
        public decimal? PaymentTerm3 { get; set; }
        public decimal? SubTotalPo { get; set; }
        public decimal? VatPo { get; set; }
        public decimal? VatPercentPo { get; set; }
        public decimal? GrandTotalPo { get; set; }
        //public string PCENumber { get; set; }
        //public string PCANUmber { get; set; }
        public string BusneissUnitId { get; set; }
        
        public string MainServiceCategoryId { get; set; }
    }
}
