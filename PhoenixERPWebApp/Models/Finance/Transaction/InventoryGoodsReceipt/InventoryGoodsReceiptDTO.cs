﻿using System.Collections.Generic;

namespace PhoenixERPWebApp.Models.Finance.Transaction.InventoryGoodsReceipt
{
    public class InventoryGoodsReceiptDTO
    {
        public InventoryGoodsReceipt Header { get; set; }
        public List<InventoryGoodsReceiptDetail> Details { get; set; }
    }
}
