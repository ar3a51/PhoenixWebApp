﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models
{
    public class Uom
    {
        public string Id { get; set; }
        
        public string unit_name { get; set; }
        
        public string unit_symbol { get; set; }

    }
}
