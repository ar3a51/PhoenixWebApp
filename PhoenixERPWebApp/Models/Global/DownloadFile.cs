﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models
{
    public class DownloadFile
    {
        public string FileName { get; set; }
        public byte[] File { get; set; }
    }
}
