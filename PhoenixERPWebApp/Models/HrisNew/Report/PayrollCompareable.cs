﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PhoenixERPWebApp.Models
{
    public class PayrollCompareable
    {
        [DisplayName("Employee Basic Info Id")]
        public string EmployeeBasicInfoId { get; set; }

        [DisplayName("Current")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal? CurrentBasicSalary { get; set; }

        [DisplayName("Current")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal? CurrentThp { get; set; }

        [DisplayName("1st")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal? OneMonthBeforeBasicSalary { get; set; }

        [DisplayName("1st")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal? OneMonthBeforeThp { get; set; }

        [DisplayName("2nd")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal? TwoMonthBeforeBasicSalary { get; set; }

        [DisplayName("2nd")]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal? TwoMonthBeforeThp { get; set; }

        [DisplayName("Employee Name")]
        public string NameEmployee { get; set; }
    }
}
