using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PhoenixERPWebApp.Models
{
    public class TmTerminationType : EntityBase
    {
        [DisplayName("Id")]
        public string Id { get; set; }

        [DisplayName("Type Name")]
        public string TypeName { get; set; }
    }
}