using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PhoenixERPWebApp.Models
{
    public class TrainingRequisition : HrisEntityBase
    {
        public string Id { get; set; }
        public string TrainingId { get; set; }
        public string RequesterEmployeeBasicInfoId { get; set; }
        public DateTime? PurposeStartDate { get; set; }
        public DateTime? PurposeEndDate { get; set; }
        public string Venue { get; set; }
        public string Notes { get; set; }
        public string Status { get; set; }
        public string Code { get; set; }
        public string AddRecomendation { get; set; }
        public string BusinessUnitId { get; set; }

        //not mapped
        public string GroupId { get; set; }
        public string SubgroupId { get; set; }
        public string DivisionId { get; set; }
        public string DepartementId { get; set; }

        //notmapped
        public string RequesterEmployeeBasicInfoName { get; set; }
        public string BusinessUnitName { get; set; }
        public string TrainingName { get; set; }
        public string TrainingType { get; set; }
        public int? Participant { get; set; }
        public List<TrainingRequisitionDetail> ParticipantList { get; set; }
        public bool? isApprove { get; set; }
        public string RemarkRejected { get; set; }
        
    }
}