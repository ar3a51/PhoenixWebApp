using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PhoenixERPWebApp.Models
{
    public class LeaveBalanceInfo : HrisEntityBase
    {
        public string Id { get; set; }
        public string EmployeeBasicInfoId { get; set; }

        [DisplayName("Type Leave")]
        public string LeaveType { get; set; }

        [DisplayName("Total")]
        public int? Balance { get; set; }

        [DisplayName("Expired Date")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ExpiredDate { get; set; }
    }
}