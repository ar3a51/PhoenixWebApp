using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PhoenixERPWebApp.Models
{
    public class Overtime : HrisEntityBase
    {
        [DisplayName("Id")]
        public string Id { get; set; }

        [DisplayName("Employee Basic Info Id")]
        public string EmployeeBasicInfoId { get; set; }

        [DisplayName("Date")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime OvertimeDate { get; set; }

        [DisplayName("Time")]
        public TimeSpan HourOvertime { get; set; }

        [DisplayName("Reason for Overtime Required")]
        public string ReasonOvertime { get; set; }

        [DisplayName("Request Date")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? RequestDate { get; set; }

        [DisplayName("Request Code")]
        public string RequestCode { get; set; }

        [DisplayName("Leave Balance Id")]
        public string LeaveBalanceId { get; set; }

        //[NotMapped]

        [DisplayName("Status")]
        public int? Status { get; set; }

        [DisplayName("Status")]
        public string StatusDesc { get; set; }

        [DisplayName("Remarks")]
        public string RemarkRejected { get; set; }


        public bool? IsApprove { get; set; }

        public InfoEmployee requestEmployee { get; set; }

        //[NotMapped]
        [DisplayName("Hour Overtime")]
        public string HourOver { get { return ((TimeSpan)HourOvertime).ToString(@"hh\:mm"); } }

    }
}