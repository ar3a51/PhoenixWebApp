﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Enum
{
    public static class AccountPayableTypeConst
    {
        public const int Normal = 1;
        public const int OtherForm = 2;
        public const int CashAdvance = 3;
        public const int PettyCash = 4;
    }
}
