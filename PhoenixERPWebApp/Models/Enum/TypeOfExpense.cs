﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoenixERPWebApp.Models
{
    public class TypeOfExpenseOutlet
    {
        //Ambil data Outlet pada table pm.type_of_expense
        public const string TypeOfExpenseId = "1128498563668643843";
        public const string TypeOfExpenseName = "Outlet";
    }

    public class TypeOfExpenseCrossBilling
    { 
        public const string TypeOfExpenseId = "1128498563668643849";
        public const string TypeOfExpenseName = "Cross Billing";
    }
}
