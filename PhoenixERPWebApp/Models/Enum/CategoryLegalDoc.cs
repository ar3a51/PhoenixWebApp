﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models
{
    public static class CategoryLegalDoc
    {
        public const string Employee = "Employee";
        public const string Client = "Client";
        public const string Vendor = "Vendor";
    }
}
