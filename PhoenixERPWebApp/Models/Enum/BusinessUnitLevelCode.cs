﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models
{
    public static class BusinessUnitLevelCode
    {
        public const int HoldingGroup = 1000;
        public const int BusinessUnitSubgroup = 900;
        public const int Division = 800;
        public const int Department = 700;
    }
}
