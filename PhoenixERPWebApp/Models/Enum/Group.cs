﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models
{
    public class GroupAcess
    {
        //Finance
        //Proses Invoice Received, TAP dan AP
        public const string FrontDesk = "22c28111-4751-4e88-8d89-375dcbb61187";
        public const string AdminFinance = "3c622ae9-e7f2-4c4b-bdc9-0addd3083ae4";
        public const string AccountingSupervisor = "c0f9fc8b-b2e7-432d-8399-aebd13c6e57b";
        public const string TaxSupervisor = "bf49a049-67a1-4173-a42f-d95a14f89d29";
        public const string FinanceManager = "ba1c39a1-603b-4985-a2a9-b4271a226ee4";
    }
}
