﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models
{
    public static class Grades
    {
        public const string Poor = "df9a8e9e-db00-47d9-9b1e-5555f0fb6d04";
        public const string Fair = "5a6afd8e-6b7f-4685-a46a-589d18b662e3";
        public const string Good = "7ff4934d-de56-4179-b347-00962b971119";
        public const string VeryGood = "431fddf3-cdca-4970-8a78-f93c1e03c4db";
        public const string Excellent = "11f789f5-1117-45b9-bfc7-92427e3a1ab0";
    }
}
