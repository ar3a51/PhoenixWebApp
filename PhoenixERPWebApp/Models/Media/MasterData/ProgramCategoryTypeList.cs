﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Media
{
    public class ProgramCategoryTypeList : MediaEntityBase
    {
        public string Id { get; set; }
        public string ProgramCategoryName { get; set; }
    }
}
