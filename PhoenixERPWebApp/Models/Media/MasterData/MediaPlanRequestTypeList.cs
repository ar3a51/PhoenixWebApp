﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoenixERPWebApp.Models.Media
{
    public class MediaPlanRequestTypeList : MediaEntityBase
    {
        public string Id { get; set; }
        public string RequestTypeName { get; set; }
    }
}
